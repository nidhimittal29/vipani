/*
MongoDB Script for inserting the PaymentTerms
*/

// Remove usergroups Data
db.usergroups.remove({});

db.usergroups.insert({
    'name': 'Admin',
    'deleted':false,
    'disabled':false,
    'acls': [{'name': 'User', 'add': true, 'edit': true, 'delete': true, 'view': true, 'disabled': false},
        {'name': 'Business Unit', 'add': true, 'edit': true, 'delete': true, 'view': true, 'disabled': false},
        {
            'name': 'Customers, Suppliers and Groups',
            'add': true,
            'edit': true,
            'delete': true,
            'view': true,
            'disabled': false
        },
        {'name': 'Offers', 'add': true, 'edit': true, 'delete': true, 'view': true, 'disabled': false},
        {'name': 'Orders', 'add': true, 'edit': true, 'delete': true, 'view': true, 'disabled': false},
        {'name': 'Payments and Receipts', 'add': true, 'edit': true, 'delete': true, 'view': true, 'disabled': false},
        {'name': 'Profile and Settings', 'add': true, 'edit': true, 'delete': true, 'view': true, 'disabled': false},
        {
            'name': 'Subscription and Billing ',
            'add': true,
            'edit': true,
            'delete': true,
            'view': true,
            'disabled': false
        },
        {'name': 'Upload Items Masters', 'add': true, 'edit': true, 'delete': true, 'view': true, 'disabled': false},
        {'name': 'Setup Inventory', 'add': true, 'edit': true, 'delete': true, 'view': true, 'disabled': false},
        {'name': 'Mediators', 'add': true, 'edit': true, 'delete': true, 'view': true, 'disabled': false},
        {'name': 'Service Providers', 'add': true, 'edit': true, 'delete': true, 'view': true, 'disabled': false}]
});

db.usergroups.insert({
    'name': 'Manager',
    'deleted':false,
    'disabled':false,
    'roles': [{'name': 'User', 'add': false, 'edit': false, 'delete': false, 'view': true, 'disabled': false},
        {'name': 'Business Unit', 'add': false, 'edit': true, 'delete': false, 'view': true, 'disabled': false},
        {
            'name': 'Customers, Suppliers and Groups ',
            'add': true,
            'edit': true,
            'delete': true,
            'view': true,
            'disabled': false
        },
        {'name': 'Business Unit Offers', 'add': true, 'edit': true, 'delete': true, 'view': true, 'disabled': false},
        {'name': 'Business Unit Orders', 'add': true, 'edit': true, 'delete': true, 'view': true, 'disabled': false},
        {
            'name': 'Business Unit Payments and Receipts',
            'add': true,
            'edit': true,
            'delete': true,
            'view': true,
            'disabled': false
        },
        {'name': 'Profile and Settings', 'add': false, 'edit': false, 'delete': false, 'view': true, 'disabled': false},
        {
            'name': 'Subscription and Billing ',
            'add': false,
            'edit': false,
            'delete': false,
            'view': true,
            'disabled': false
        },
        {
            'name': 'Setup Business Unit Inventory',
            'add': true,
            'edit': true,
            'delete': true,
            'view': true,
            'disabled': false
        }]
});

db.usergroups.insert({
    'name': 'Employee',
    'deleted':false,
    'disabled':false,
    'roles': [{'name': 'User', 'add': false, 'edit': false, 'delete': false, 'view': true, 'disabled': false},
        {'name': 'Business Unit', 'add': false, 'edit': false, 'delete': false, 'view': true, 'disabled': false},
        {
            'name': 'Customers, Suppliers and Groups',
            'add': true,
            'edit': true,
            'delete': true,
            'view': true,
            'disabled': false
        },
        {'name': 'Business Unit Offers', 'add': false, 'edit': false, 'delete': false, 'view': true, 'disabled': false},
        {'name': 'Business Unit Orders', 'add': true, 'edit': true, 'delete': true, 'view': true, 'disabled': false},
        {
            'name': 'Business Unit Payments and Receipts',
            'add': false,
            'edit': false,
            'delete': false,
            'view': true,
            'disabled': false
        },
        {'name': 'Profile and Settings', 'add': false, 'edit': false, 'delete': false, 'view': true, 'disabled': false},
        {
            'name': 'Subscription and Billing',
            'add': false,
            'edit': false,
            'delete': false,
            'view': true,
            'disabled': false
        },
        {
            'name': 'Setup Business Unit Inventory',
            'add': false,
            'edit': false,
            'delete': false,
            'view': true,
            'disabled': false
        }]
});

/* End */
