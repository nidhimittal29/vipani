/*
MongoDB Script for inserting the PaymentTerms
*/

// Remove paymentterms Data
db.paymentterms.remove({});

db.paymentterms.insert({'name': 'Net 30', 'type': 'NetTerm', netTerm: 30,
    disabled:false,
    deleted:false
});
db.paymentterms.insert({'name': 'Net 45', 'type': 'NetTerm', netTerm: 45,
    disabled:false,
    deleted:false
});
db.paymentterms.insert({'name': 'Net 60', 'type': 'NetTerm', netTerm: 60, disabled:false, deleted:false
});

db.paymentterms.insert({
    'name': '4/8 Net 60',
    'type': 'NetTermDiscount',
    netTerm: 60,
    discountTerm: 8,
    termDiscount: 4,
    disabled:false,
    deleted:false
});

db.paymentterms.insert({
    'name': '3/25 Net 60',
    'type': 'NetTermDiscount',
    netTerm: 60,
    discountTerm: 25,
    termDiscount: 3,
    disabled:false,
    deleted:false
});

db.paymentterms.insert({
    'name': '2/35 Net 60',
    'type': 'NetTermDiscount',
    netTerm: 60,
    discountTerm: 35,
    termDiscount: 2
});

db.paymentterms.insert({
    'name': '1/45 Net 60',
    'type': 'NetTermDiscount',
    netTerm: 60,
    discountTerm: 45,
    termDiscount: 1,
    disabled:false,
    deleted:false
});

db.paymentterms.insert({'name': 'Pay Now', 'type': 'PayNow',
    disabled:false,
    deleted:false
});
db.paymentterms.insert({'name': 'Pay On Delivery', 'type': 'PayOnDelivery',
    disabled:false,
    deleted:false
});
db.paymentterms.insert({
    'name': 'Installments',
    'type': 'Installments',
    'installments': [{'description': 'Advance Payment', 'days': 0, 'percentage': 40}, {
        'description': 'First Payment',
        'days': 30,
        'percentage': 40
    }, {'description': 'Final Payment', 'days': 45, 'percentage': 20}],
    disabled:false,
    deleted:false
});

/* End */
