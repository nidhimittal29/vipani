/*
MongoDB Script for inserting the PaymentTerms
*/

// Remove subscriptions Data
db.subscriptions.remove({});

db.subscriptions.insert({
    'name': 'Free', 'type': 'Free', 'scope': 'User',
    'termType': 'Monthly', 'billingTermType': 'Monthly', 'discount': 0, 'price': 0, 'netPrice': 0,
    'freeTerm': 0,
    'features': [{'name': 'Inventory Management', 'chargeType': 'Unlimited', 'limit': 0, 'limitCount': 0},
        {'name': 'Offer Management', 'chargeType': 'Limited', 'limit': 0, 'limitCount': 10},
        {'name': 'Order Management', 'chargeType': 'Limited', 'limit': 0, 'limitCount': 100},
        {'name': 'Contact Management', 'chargeType': 'Limited', 'limit': 0, 'limitCount': 15},
        {'name': 'User Management', 'chargeType': 'Limited', 'limit': 0, 'limitCount': 3}]
});

db.subscriptions.insert({
    'name': 'Free', 'type': 'Free', 'scope': 'User',
    'termType': 'Monthly', 'billingTermType': 'Monthly', 'discount': 0, 'price': 0, 'netPrice': 0,
    'freeTerm': 0,
    'features': [{'name': 'Inventory Management', 'chargeType': 'Unlimited', 'limit': 0, 'limitCount': 0},
        {'name': 'Offer Management', 'chargeType': 'Limited', 'limit': 0, 'limitCount': 10},
        {'name': 'Order Management', 'chargeType': 'Limited', 'limit': 0, 'limitCount': 100},
        {'name': 'Contact Management', 'chargeType': 'Limited', 'limit': 0, 'limitCount': 15},
        {'name': 'User Management', 'chargeType': 'Limited', 'limit': 0, 'limitCount': 3}]
});

db.subscriptions.insert({
    'name': 'Free', 'type': 'Free', 'scope': 'User',
    'termType': 'Monthly', 'billingTermType': 'Monthly', 'discount': 0, 'price': 0, 'netPrice': 0,
    'freeTerm': 0,
    'features': [{'name': 'Inventory Management', 'chargeType': 'Unlimited', 'limit': 0, 'limitCount': 0},
        {'name': 'Offer Management', 'chargeType': 'Limited', 'limit': 0, 'limitCount': 10},
        {'name': 'Order Management', 'chargeType': 'Limited', 'limit': 0, 'limitCount': 100},
        {'name': 'Contact Management', 'chargeType': 'Limited', 'limit': 0, 'limitCount': 15},
        {'name': 'User Management', 'chargeType': 'Limited', 'limit': 0, 'limitCount': 3}]
});

db.subscriptions.insert({
    'name': 'Free', 'type': 'Free', 'scope': 'User',
    'termType': 'Monthly', 'billingTermType': 'Monthly', 'discount': 0, 'price': 0, 'netPrice': 0,
    'freeTerm': 0,
    'features': [{'name': 'Inventory Management', 'chargeType': 'Unlimited', 'limit': 0, 'limitCount': 0},
        {'name': 'Offer Management', 'chargeType': 'Limited', 'limit': 0, 'limitCount': 10},
        {'name': 'Order Management', 'chargeType': 'Limited', 'limit': 0, 'limitCount': 100},
        {'name': 'Contact Management', 'chargeType': 'Limited', 'limit': 0, 'limitCount': 15},
        {'name': 'User Management', 'chargeType': 'Limited', 'limit': 0, 'limitCount': 3}]
});


/* End */
