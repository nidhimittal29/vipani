// Remove categories Data
db.categories.remove({});

// Agriculture Main Category
db.categories.insert({
    'name': 'Agriculture',
    'code': 'AGRIC',
    'type': 'MainCategory',
    'categoryImageURL1': 'modules/categories/img/maincategory/agriculture.png',
    'croppedCategoryImageURL1': 'modules/categories/img/maincategory/mobileagriculture.png'
});

// Sub Categories of Agriculture
db.categories.insert({
    'name': 'Cereals',
    'code': 'CERLS',
    'type': 'SubCategory1',
    'categoryImageURL1': 'modules/categories/img/subcategory1/cereals.png',
    'croppedCategoryImageURL1': 'modules/categories/img/subcategory1/mobilecereals.png',
    'parent': db.categories.findOne({'name': 'Agriculture', 'type': 'MainCategory'})._id
});
// Adding Sub Category Cereals to Agriculture Category
db.categories.update({
    'name': 'Agriculture',
    'type': 'MainCategory'
}, {$addToSet: {'children': db.categories.findOne({'name': 'Cereals', 'type': 'SubCategory1'})._id}});

// Sub Categories of Agriculture
db.categories.insert({
    'name': 'Vegetable',
    'code': 'VGTBL',
    'type': 'SubCategory1',
    'categoryImageURL1': 'modules/categories/img/subcategory1/vegetables.png',
    'croppedCategoryImageURL1': 'modules/categories/img/subcategory1/mobilevegetables.png',
    'parent': db.categories.findOne({'name': 'Agriculture', 'type': 'MainCategory'})._id
});

db.categories.update({
    'name': 'Agriculture',
    'type': 'MainCategory'
}, {$addToSet: {'children': db.categories.findOne({'name': 'Vegetable', 'type': 'SubCategory1'})._id}});

// Sub Categories of Vegetables
db.categories.insert({
    'name': 'Raw Corn',
    'code': 'RCORN',
    'type': 'SubCategory2',
    'categoryImageURL1': 'modules/categories/img/subcategory2/rawcorn.png',
    'croppedCategoryImageURL1': 'modules/categories/img/subcategory2/mobilerawcorn.png',
    'productCategory': true,
    'productAttributes': {
        brand: true,
        grade: {enabled: true},
        quality: {enabled: true},
        sampleNumber: true
    },
    'inventoryAttributes': {
        barcode: true,
        batchNumber: true,
        manufactureDate: true,
        packagingDate: true,
        bestBefore: true,
        unitMeasure:['Kg', 'gram', 'tonne', 'Bag', 'Case', 'Pack']
    },
    'parent': db.categories.findOne({'name': 'Vegetable', 'type': 'SubCategory1'})._id
});


db.categories.update({
    'name': 'Vegetable',
    'type': 'SubCategory1'
}, {$addToSet: {'children': db.categories.findOne({'name': 'Raw Corn', 'type': 'SubCategory2'})._id}});

db.categories.insert({
    'name': 'Lemon',
    'code': 'LEMON',
    'type': 'SubCategory2',
    'categoryImageURL1': 'modules/categories/img/subcategory2/lemon.png',
    'croppedCategoryImageURL1': 'modules/categories/img/subcategory2/mobilelemon.png',
    'productCategory': true,
    'productAttributes': {
        brand: true,
        grade: {enabled: true},
        quality: {enabled: true},
        sampleNumber: true
    },
    'inventoryAttributes': {
        barcode: true,
        batchNumber: true,
        manufactureDate: true,
        packagingDate: true,
        bestBefore: true,
        unitMeasure: ['Kg', 'gram', 'tonne', 'Bag', 'Case', 'Pack']
    },
    'parent': db.categories.findOne({'name': 'Vegetable', 'code': 'VGTBL', 'type': 'SubCategory1'})._id
});

db.categories.update({
    'name': 'Vegetable',
    'type': 'SubCategory1'
}, {$addToSet: {'children': db.categories.findOne({'name': 'Lemon', 'code': 'LEMON', 'type': 'SubCategory2'})._id}});

db.categories.insert({
    'name': 'Onion',
    'code': 'ONION',
    'type': 'SubCategory2',
    'categoryImageURL1': 'modules/categories/img/subcategory2/onion.png',
    'croppedCategoryImageURL1': 'modules/categories/img/subcategory2/mobileonion.png',
    'productCategory': true,
    'productAttributes': {
        brand: true,
        grade: {enabled: true},
        quality: {enabled: true},
        sampleNumber: true
    },
    'inventoryAttributes': {
        barcode: true,
        batchNumber: true,
        manufactureDate: true,
        packagingDate: true,
        bestBefore: true,
        unitMeasure: ['Kg', 'gram', 'tonne', 'Bag', 'Case', 'Pack']
    },
    'parent': db.categories.findOne({'name': 'Vegetable', 'code': 'VGTBL', 'type': 'SubCategory1'})._id
});


db.categories.update({
    'name': 'Vegetable',
    'type': 'SubCategory1'
}, {$addToSet: {'children': db.categories.findOne({'name': 'Onion', 'code': 'ONION', 'type': 'SubCategory2'})._id}});

db.categories.insert({
    'name': 'Garlic',
    'code': 'GARLC',
    'type': 'SubCategory2',
    'categoryImageURL1': 'modules/categories/img/subcategory2/garlic.png',
    'croppedCategoryImageURL1': 'modules/categories/img/subcategory2/mobilegarlic.png',
    'productCategory': true,
    'productAttributes': {
        brand: true,
        grade: {enabled: true},
        quality: {enabled: true},
        sampleNumber: true
    },
    'inventoryAttributes': {
        barcode: true,
        batchNumber: true,
        manufactureDate: true,
        packagingDate: true,
        bestBefore: true,
        unitMeasure: ['Kg', 'gram', 'tonne', 'Bag', 'Case', 'Pack']
    },
    'parent': db.categories.findOne({'name': 'Vegetable', 'code': 'VGTBL', 'type': 'SubCategory1'})._id
});


db.categories.update({
    'name': 'Vegetable',
    'type': 'SubCategory1'
}, {$addToSet: {'children': db.categories.findOne({'name': 'Garlic', 'code': 'GARLC', 'type': 'SubCategory2'})._id}});

db.categories.insert({
    'name': 'Potato',
    'code': 'POTAT',
    'type': 'SubCategory2',
    'categoryImageURL1': 'modules/categories/img/subcategory2/potato.png',
    'croppedCategoryImageURL1': 'modules/categories/img/subcategory2/mobilepotato.png',
    'productCategory': true,
    'productAttributes': {
        brand: true,
        grade: {enabled: true},
        quality: {enabled: true},
        sampleNumber: true
    },
    'inventoryAttributes': {
        barcode: true,
        batchNumber: true,
        manufactureDate: true,
        packagingDate: true,
        bestBefore: true,
        unitMeasure: ['Kg', 'gram', 'tonne', 'Bag', 'Case', 'Pack']
    },
    'parent': db.categories.findOne({'name': 'Vegetable', 'code': 'VGTBL', 'type': 'SubCategory1'})._id
});


db.categories.update({
    'name': 'Vegetable',
    'type': 'SubCategory1'
}, {$addToSet: {'children': db.categories.findOne({'name': 'Potato', 'code': 'POTAT', 'type': 'SubCategory2'})._id}});


// Sub Categories of Cereals
db.categories.insert({
    'name': 'Rice',
    'code': 'RICEG',
    'type': 'SubCategory2',
    'categoryImageURL1': 'modules/categories/img/subcategory2/rice.png',
    'croppedCategoryImageURL1': 'modules/categories/img/subcategory2/mobilerice.png',
    'productCategory': true,
    'productAttributes': {
        brand: true,
        grade: {enabled: true},
        quality: {enabled: true},
        sampleNumber: true,
        fssaiLicenceNumber: true
    },
    'inventoryAttributes': {
        barcode: true,
        batchNumber: true,
        manufactureDate: true,
        packagingDate: true,
        bestBefore: true,
        unitMeasure: ['Kg', 'gram', 'tonne', 'Bag', 'Case', 'Pack']
    },
    'parent': db.categories.findOne({'name': 'Cereals', 'type': 'SubCategory1'})._id
});
db.categories.insert({
    'name': 'Wheat',
    'code': 'WHEAT',
    'type': 'SubCategory2',
    'categoryImageURL1': 'modules/categories/img/subcategory2/wheat.png',
    'croppedCategoryImageURL1': 'modules/categories/img/subcategory2/mobilewheat.png',
    'productCategory': true,
    'productAttributes': {
        brand: true,
        grade: {enabled: true},
        quality: {enabled: true},
        sampleNumber: true,
        fssaiLicenceNumber: true
    },
    'inventoryAttributes': {
        barcode: true,
        batchNumber: true,
        manufactureDate: true,
        packagingDate: true,
        bestBefore: true,
        unitMeasure: ['Kg', 'gram', 'tonne', 'Bag', 'Case', 'Pack']
    },
    'parent': db.categories.findOne({'name': 'Cereals', 'type': 'SubCategory1'})._id
});
db.categories.insert({
    'name': 'Pearl Millet',
    'code': 'PRMLT',
    'type': 'SubCategory2',
    'categoryImageURL1': 'modules/categories/img/subcategory2/pearlmillet.png',
    'productCategory': true,
    'productAttributes': {
        brand: true,
        grade: {enabled: true},
        quality: {enabled: true},
        sampleNumber: true,
        fssaiLicenceNumber: true
    },
    'inventoryAttributes': {
        barcode: true,
        batchNumber: true,
        manufactureDate: true,
        packagingDate: true,
        bestBefore: true,
        unitMeasure: ['Kg', 'gram', 'tonne', 'Bag', 'Case', 'Pack']
    },
    'parent': db.categories.findOne({'name': 'Cereals', 'type': 'SubCategory1'})._id
});
db.categories.insert({
    'name': 'Sorghum Bicolor',
    'code': 'SGMBI',
    'type': 'SubCategory2',
    'categoryImageURL1': 'modules/categories/img/subcategory2/sorghumbicolor.png',
    'croppedCategoryImageURL1': 'modules/categories/img/subcategory2/mobilesorghumbicolor.png',
    'productCategory': true,
    'productAttributes': {
        brand: true,
        grade: {enabled: true},
        quality: {enabled: true},
        sampleNumber: true,
        fssaiLicenceNumber: true
    },
    'inventoryAttributes': {
        barcode: true,
        batchNumber: true,
        manufactureDate: true,
        packagingDate: true,
        bestBefore: true,
        unitMeasure: ['Kg', 'gram', 'tonne', 'Bag', 'Case', 'Pack']
    },
    'parent': db.categories.findOne({'name': 'Cereals', 'type': 'SubCategory1'})._id
});
db.categories.insert({
    'name': 'Corn',
    'code': 'CORNG',
    'type': 'SubCategory2',
    'categoryImageURL1': 'modules/categories/img/subcategory2/corn.png',
    'croppedCategoryImageURL1': 'modules/categories/img/subcategory2/mobilecorn.png',
    'productCategory': true,
    'productAttributes': {
        brand: true,
        grade: {enabled: true},
        quality: {enabled: true},
        sampleNumber: true,
        fssaiLicenceNumber: true
    },
    'inventoryAttributes': {
        barcode: true,
        batchNumber: true,
        manufactureDate: true,
        packagingDate: true,
        bestBefore: true,
        unitMeasure: ['Kg', 'gram', 'tonne', 'Bag', 'Case', 'Pack']
    },
    'parent': db.categories.findOne({'name': 'Cereals', 'type': 'SubCategory1'})._id
});
db.categories.insert({
    'name': 'Finger Millet',
    'code': 'FGMLT',
    'type': 'SubCategory2',
    'croppedCategoryImageURL1': 'modules/categories/img/subcategory2/mobilefingermillet.png',
    'productCategory': true,
    'productAttributes': {
        brand: true,
        grade: {enabled: true},
        quality: {enabled: true},
        sampleNumber: true,
        fssaiLicenceNumber: true
    },
    'inventoryAttributes': {
        barcode: true,
        batchNumber: true,
        manufactureDate: true,
        packagingDate: true,
        bestBefore: true,
        unitMeasure: ['Kg', 'gram', 'tonne', 'Bag', 'Case', 'Pack']
    },
    'parent': db.categories.findOne({'name': 'Cereals', 'type': 'SubCategory1'})._id
});

// Adding Sub Categories to Cereals category
db.categories.update({
    'name': 'Cereals',
    'type': 'SubCategory1'
}, {$addToSet: {'children': db.categories.findOne({'name': 'Rice', 'type': 'SubCategory2'})._id}});

db.categories.update({
    'name': 'Cereals',
    'type': 'SubCategory1'
}, {$addToSet: {'children': db.categories.findOne({'name': 'Wheat', 'type': 'SubCategory2'})._id}});

db.categories.update({
    'name': 'Cereals',
    'type': 'SubCategory1'
}, {$addToSet: {'children': db.categories.findOne({'name': 'Pearl Millet', 'type': 'SubCategory2'})._id}});

db.categories.update({
    'name': 'Cereals',
    'type': 'SubCategory1'
}, {$addToSet: {'children': db.categories.findOne({'name': 'Sorghum Bicolor', 'type': 'SubCategory2'})._id}});

db.categories.update({
    'name': 'Cereals',
    'type': 'SubCategory1'
}, {$addToSet: {'children': db.categories.findOne({'name': 'Corn', 'type': 'SubCategory2'})._id}});

db.categories.update({
    'name': 'Cereals',
    'type': 'SubCategory1'
}, {$addToSet: {'children': db.categories.findOne({'name': 'Finger Millet', 'type': 'SubCategory2'})._id}});

// **********************

// Sub Categories of Agriculture
db.categories.insert({
    'name': 'Pulses',
    'code': 'PULSS',
    'type': 'SubCategory1',
    'categoryImageURL1': 'modules/categories/img/subcategory1/pulses.png',
    'croppedCategoryImageURL1': 'modules/categories/img/subcategory1/mobilepulses.png',
    'parent': db.categories.findOne({'name': 'Agriculture', 'type': 'MainCategory'})._id
});
// Adding Sub Category Pulses to Agriculture Category
db.categories.update({
    'name': 'Agriculture',
    'type': 'MainCategory'
}, {$addToSet: {'children': db.categories.findOne({'name': 'Pulses', 'type': 'SubCategory1'})._id}});

// Sub Categories of Pulses
db.categories.insert({
    'name': 'Toor Dal',
    'code': 'TRDAL',
    'type': 'SubCategory2',
    'categoryImageURL1': 'modules/categories/img/subcategory2/toordal.png',
    'croppedCategoryImageURL1': 'modules/categories/img/subcategory2/mobiletoordal.png',
    'productCategory': true,
    'productAttributes': {
        brand: true,
        grade: {enabled: true},
        quality: {enabled: true},
        sampleNumber: true,
        fssaiLicenceNumber: true
    },
    'inventoryAttributes': {
        barcode: true,
        batchNumber: true,
        manufactureDate: true,
        packagingDate: true,
        bestBefore: true,
        unitMeasure: ['Kg', 'gram', 'tonne', 'Bag', 'Case', 'Pack']
    },
    'parent': db.categories.findOne({'name': 'Pulses', 'type': 'SubCategory1'})._id
});

db.categories.insert({
    'name': 'Rajma Beans',
    'code': 'RAJBE',
    'type': 'SubCategory2',
    'categoryImageURL1': 'modules/categories/img/subcategory2/rajmabeans.png',
    'croppedCategoryImageURL1': 'modules/categories/img/subcategory2/mobilerajmabeans.png',
    'productCategory': true,
    'productAttributes': {
        brand: true,
        grade: {enabled: true},
        quality: {enabled: true},
        sampleNumber: true,
        fssaiLicenceNumber: true
    },
    'inventoryAttributes': {
        barcode: true,
        batchNumber: true,
        manufactureDate: true,
        packagingDate: true,
        bestBefore: true,
        unitMeasure: ['Kg', 'gram', 'tonne', 'Bag', 'Case', 'Pack']
    },
    'parent': db.categories.findOne({'name': 'Pulses', 'type': 'SubCategory1'})._id
});
db.categories.insert({
    'name': 'Black Urad Dal Whole',
    'code': 'BUDLW',
    'type': 'SubCategory2',
    'categoryImageURL1': 'modules/categories/img/subcategory2/uraddal.png',
    'croppedCategoryImageURL1': 'modules/categories/img/subcategory2/mobileuraddal.png',
    'productCategory': true,
    'productAttributes': {
        brand: true,
        grade: {enabled: true},
        quality: {enabled: true},
        sampleNumber: true,
        fssaiLicenceNumber: true
    },
    'inventoryAttributes': {
        barcode: true,
        batchNumber: true,
        manufactureDate: true,
        packagingDate: true,
        bestBefore: true,
        unitMeasure: ['Kg', 'gram', 'tonne', 'Bag', 'Case', 'Pack']
    },
    'parent': db.categories.findOne({'name': 'Pulses', 'type': 'SubCategory1'})._id
});
db.categories.insert({
    'name': 'Black Urad Dal Split',
    'code': 'BUDLS',
    'type': 'SubCategory2',
    'categoryImageURL1': 'modules/categories/img/subcategory2/blackuraddalsplit.png',
    'croppedCategoryImageURL1': 'modules/categories/img/subcategory2/mobileblackuraddalsplit.png',
    'productCategory': true,
    'productAttributes': {
        brand: true,
        grade: {enabled: true},
        quality: {enabled: true},
        sampleNumber: true,
        fssaiLicenceNumber: true
    },
    'inventoryAttributes': {
        barcode: true,
        batchNumber: true,
        manufactureDate: true,
        packagingDate: true,
        bestBefore: true,
        unitMeasure: ['Kg', 'gram', 'tonne', 'Bag', 'Case', 'Pack']
    },
    'parent': db.categories.findOne({'name': 'Pulses', 'type': 'SubCategory1'})._id
});

db.categories.insert({
    'name': 'White Urad Dal Whole',
    'code': 'WUDLW',
    'type': 'SubCategory2',
    'categoryImageURL1': 'modules/categories/img/subcategory2/uraddalwhole.png',
    'croppedCategoryImageURL1': 'modules/categories/img/subcategory2/mobileuraddalwhole.png',
    'productCategory': true,
    'productAttributes': {
        brand: true,
        grade: {enabled: true},
        quality: {enabled: true},
        sampleNumber: true,
        fssaiLicenceNumber: true
    },
    'inventoryAttributes': {
        barcode: true,
        batchNumber: true,
        manufactureDate: true,
        packagingDate: true,
        bestBefore: true,
        unitMeasure: ['Kg', 'gram', 'tonne', 'Bag', 'Case', 'Pack']
    },
    'parent': db.categories.findOne({'name': 'Pulses', 'type': 'SubCategory1'})._id
});

db.categories.insert({
    'name': 'White Urad Dal Split',
    'code': 'WUDLS',
    'type': 'SubCategory2',
    'categoryImageURL1': 'modules/categories/img/subcategory2/uraddalsplit.png',
    'croppedCategoryImageURL1': 'modules/categories/img/subcategory2/mobileuraddalsplit.png',
    'productCategory': true,
    'productAttributes': {
        brand: true,
        grade: {enabled: true},
        quality: {enabled: true},
        sampleNumber: true,
        fssaiLicenceNumber: true
    },
    'inventoryAttributes': {
        barcode: true,
        batchNumber: true,
        manufactureDate: true,
        packagingDate: true,
        bestBefore: true,
        unitMeasure: ['Kg', 'gram', 'tonne', 'Bag', 'Case', 'Pack']
    },
    'parent': db.categories.findOne({'name': 'Pulses', 'type': 'SubCategory1'})._id
});

db.categories.insert({
    'name': 'Masoor Dal',
    'code': 'MRDAL',
    'type': 'SubCategory2',
    'categoryImageURL1': 'modules/categories/img/subcategory2/masoordal.png',
    'croppedCategoryImageURL1': 'modules/categories/img/subcategory2/mobilemasoordal.png',
    'productCategory': true,
    'productAttributes': {
        brand: true,
        grade: {enabled: true},
        quality: {enabled: true},
        sampleNumber: true,
        fssaiLicenceNumber: true
    },
    'inventoryAttributes': {
        barcode: true,
        batchNumber: true,
        manufactureDate: true,
        packagingDate: true,
        bestBefore: true,
        unitMeasure: ['Kg', 'gram', 'tonne', 'Bag', 'Case', 'Pack']
    },
    'parent': db.categories.findOne({'name': 'Pulses', 'type': 'SubCategory1'})._id
});
db.categories.insert({
    'name': 'Green Moong Dal Whole',
    'code': 'MGDLW',
    'type': 'SubCategory2',
    'categoryImageURL1': 'modules/categories/img/subcategory2/moongdal.png',
    'croppedCategoryImageURL1': 'modules/categories/img/subcategory2/mobilemoongdal.png',
    'productCategory': true,
    'productAttributes': {
        brand: true,
        grade: {enabled: true},
        quality: {enabled: true},
        sampleNumber: true,
        fssaiLicenceNumber: true
    },
    'inventoryAttributes': {
        barcode: true,
        batchNumber: true,
        manufactureDate: true,
        packagingDate: true,
        bestBefore: true,
        unitMeasure: ['Kg', 'gram', 'tonne', 'Bag', 'Case', 'Pack']
    },
    'parent': db.categories.findOne({'name': 'Pulses', 'type': 'SubCategory1'})._id
});

db.categories.insert({
    'name': 'Green Moong Dal Split',
    'code': 'MGDLS',
    'type': 'SubCategory2',
    'categoryImageURL1': 'modules/categories/img/subcategory2/greenmoongsplit.png',
    'croppedCategoryImageURL1': 'modules/categories/img/subcategory2/mobilegreenmoongsplit.png',
    'productCategory': true,
    'productAttributes': {
        brand: true,
        grade: {enabled: true},
        quality: {enabled: true},
        sampleNumber: true,
        fssaiLicenceNumber: true
    },
    'inventoryAttributes': {
        barcode: true,
        batchNumber: true,
        manufactureDate: true,
        packagingDate: true,
        bestBefore: true,
        unitMeasure: ['Kg', 'gram', 'tonne', 'Bag', 'Case', 'Pack']
    },
    'parent': db.categories.findOne({'name': 'Pulses', 'type': 'SubCategory1'})._id
});

db.categories.insert({
    'name': 'Yellow Moong Dal Split',
    'code': 'MGDLY',
    'type': 'SubCategory2',
    'categoryImageURL1': 'modules/categories/img/subcategory2/yellowmoongdal.png',
    'croppedCategoryImageURL1': 'modules/categories/img/subcategory2/mobileyellowmoongdal.png',
    'productCategory': true,
    'productAttributes': {
        brand: true,
        grade: {enabled: true},
        quality: {enabled: true},
        sampleNumber: true,
        fssaiLicenceNumber: true
    },
    'inventoryAttributes': {
        barcode: true,
        batchNumber: true,
        manufactureDate: true,
        packagingDate: true,
        bestBefore: true,
        unitMeasure: ['Kg', 'gram', 'tonne', 'Bag', 'Case', 'Pack']
    },
    'parent': db.categories.findOne({'name': 'Pulses', 'type': 'SubCategory1'})._id
});

db.categories.insert({
    'name': 'Channa Dal',
    'code': 'CHDAL',
    'type': 'SubCategory2',
    'categoryImageURL1': 'modules/categories/img/subcategory2/channa.png',
    'croppedCategoryImageURL1': 'modules/categories/img/subcategory2/mobilechanna.png',
    'productCategory': true,
    'productAttributes': {
        brand: true,
        grade: {enabled: true},
        quality: {enabled: true},
        sampleNumber: true,
        fssaiLicenceNumber: true
    },
    'inventoryAttributes': {
        barcode: true,
        batchNumber: true,
        manufactureDate: true,
        packagingDate: true,
        bestBefore: true,
        unitMeasure: ['Kg', 'gram', 'tonne', 'Bag', 'Case', 'Pack']
    },
    'parent': db.categories.findOne({'name': 'Pulses', 'type': 'SubCategory1'})._id
});
db.categories.insert({
    'name': 'Kabuli Chana Dal White',
    'code': 'KWDAL',
    'type': 'SubCategory2',
    'categoryImageURL1': 'modules/categories/img/subcategory2/kabulichana.png',
    'croppedCategoryImageURL1': 'modules/categories/img/subcategory2/mobilekabulichana.png',
    'productCategory': true,
    'productAttributes': {
        brand: true,
        grade: {enabled: true},
        quality: {enabled: true},
        sampleNumber: true,
        fssaiLicenceNumber: true
    },
    'inventoryAttributes': {
        barcode: true,
        batchNumber: true,
        manufactureDate: true,
        packagingDate: true,
        bestBefore: true,
        unitMeasure: ['Kg', 'gram', 'tonne', 'Bag', 'Case', 'Pack']
    },
    'parent': db.categories.findOne({'name': 'Pulses', 'type': 'SubCategory1'})._id
});

db.categories.insert({
    'name': 'Kabuli Chana Dal Brown',
    'code': 'KBDAL',
    'type': 'SubCategory2',
    'categoryImageURL1': 'modules/categories/img/subcategory2/kabulichanabrown.png',
    'croppedCategoryImageURL1': 'modules/categories/img/subcategory2/mobilekabulichanabrown.png',
    'productCategory': true,
    'productAttributes': {
        brand: true,
        grade: {enabled: true},
        quality: {enabled: true},
        sampleNumber: true,
        fssaiLicenceNumber: true
    },
    'inventoryAttributes': {
        barcode: true,
        batchNumber: true,
        manufactureDate: true,
        packagingDate: true,
        bestBefore: true,
        unitMeasure: ['Kg', 'gram', 'tonne', 'Bag', 'Case', 'Pack']
    },
    'parent': db.categories.findOne({'name': 'Pulses', 'type': 'SubCategory1'})._id
});

db.categories.insert({
    'name': 'Horse Gram',
    'code': 'HGDAL',
    'type': 'SubCategory2',
    'categoryImageURL1': 'modules/categories/img/subcategory2/horsegram.png',
    'croppedCategoryImageURL1': 'modules/categories/img/subcategory2/mobilehorsegram.png',
    'productCategory': true,
    'productAttributes': {
        brand: true,
        grade: {enabled: true},
        quality: {enabled: true},
        sampleNumber: true,
        fssaiLicenceNumber: true
    },
    'inventoryAttributes': {
        barcode: true,
        batchNumber: true,
        manufactureDate: true,
        packagingDate: true,
        bestBefore: true,
        unitMeasure: ['Kg', 'gram', 'tonne', 'Bag', 'Case', 'Pack']
    },
    'parent': db.categories.findOne({'name': 'Pulses', 'type': 'SubCategory1'})._id
});
db.categories.insert({
    'name': 'Soya Bean',
    'code': 'SOYBN',
    'type': 'SubCategory2',
    'categoryImageURL1': 'modules/categories/img/subcategory2/soyabean.png',
    'croppedCategoryImageURL1': 'modules/categories/img/subcategory2/mobilesoyabean.png',
    'productCategory': true,
    'productAttributes': {
        brand: true,
        grade: {enabled: true},
        quality: {enabled: true},
        sampleNumber: true,
        fssaiLicenceNumber: true
    },
    'inventoryAttributes': {
        barcode: true,
        batchNumber: true,
        manufactureDate: true,
        packagingDate: true,
        bestBefore: true,
        unitMeasure: ['Kg', 'gram', 'tonne', 'Bag', 'Case', 'Pack']
    },
    'parent': db.categories.findOne({'name': 'Pulses', 'type': 'SubCategory1'})._id
});
db.categories.insert({
    'name': 'Cowpea',
    'code': 'COWPE',
    'type': 'SubCategory2',
    'categoryImageURL1': 'modules/categories/img/subcategory2/cowpea.png',
    'croppedCategoryImageURL1': 'modules/categories/img/subcategory2/mobilecowpea.png',
    'productCategory': true,
    'productAttributes': {
        brand: true,
        grade: {enabled: true},
        quality: {enabled: true},
        sampleNumber: true,
        fssaiLicenceNumber: true
    },
    'inventoryAttributes': {
        barcode: true,
        batchNumber: true,
        manufactureDate: true,
        packagingDate: true,
        bestBefore: true,
        unitMeasure: ['Kg', 'gram', 'tonne', 'Bag', 'Case', 'Pack']
    },
    'parent': db.categories.findOne({'name': 'Pulses', 'type': 'SubCategory1'})._id
});
db.categories.insert({
    'name': 'Sesame',
    'code': 'SESAM',
    'type': 'SubCategory2',
    'categoryImageURL1': 'modules/categories/img/subcategory2/sesame.png',
    'croppedCategoryImageURL1': 'modules/categories/img/subcategory2/mobilesesame.png',
    'productCategory': true,
    'productAttributes': {
        brand: true,
        grade: {enabled: true},
        quality: {enabled: true},
        sampleNumber: true,
        fssaiLicenceNumber: true
    },
    'inventoryAttributes': {
        barcode: true,
        batchNumber: true,
        manufactureDate: true,
        packagingDate: true,
        bestBefore: true,
        unitMeasure: ['Kg', 'gram', 'tonne', 'Bag', 'Case', 'Pack']
    },
    'parent': db.categories.findOne({'name': 'Pulses', 'type': 'SubCategory1'})._id
});
db.categories.insert({
    'name': 'Peas',
    'code': 'PEAS',
    'type': 'SubCategory2',
    'categoryImageURL1': 'modules/categories/img/subcategory2/peas.png',
    'croppedCategoryImageURL1': 'modules/categories/img/subcategory2/mobilepeas.png',
    'productCategory': true,
    'productAttributes': {
        brand: true,
        grade: {enabled: true},
        quality: {enabled: true},
        sampleNumber: true,
        fssaiLicenceNumber: true
    },
    'inventoryAttributes': {
        barcode: true,
        batchNumber: true,
        manufactureDate: true,
        packagingDate: true,
        bestBefore: true,
        unitMeasure: ['Kg', 'gram', 'tonne', 'Bag', 'Case', 'Pack']
    },
    'parent': db.categories.findOne({'name': 'Pulses', 'type': 'SubCategory1'})._id
});
db.categories.insert({
    'name': 'Red Gram',
    'code': 'REDGM',
    'type': 'SubCategory2',
    'categoryImageURL1': 'modules/categories/img/subcategory2/redgram.png',
    'croppedCategoryImageURL1': 'modules/categories/img/subcategory2/mobileredgram.png',
    'productCategory': true,
    'productAttributes': {
        brand: true,
        grade: {enabled: true},
        quality: {enabled: true},
        sampleNumber: true,
        fssaiLicenceNumber: true
    },
    'inventoryAttributes': {
        barcode: true,
        batchNumber: true,
        manufactureDate: true,
        packagingDate: true,
        bestBefore: true,
        unitMeasure: ['Kg', 'gram', 'tonne', 'Bag', 'Case', 'Pack']
    },
    'parent': db.categories.findOne({'name': 'Pulses', 'type': 'SubCategory1'})._id
});
db.categories.insert({
    'name': 'Groundnut',
    'code': 'GRDNT',
    'type': 'SubCategory2',
    'categoryImageURL1': 'modules/categories/img/subcategory2/groundnut.png',
    'croppedCategoryImageURL1': 'modules/categories/img/subcategory2/mobilegroundnut.png',
    'productCategory': true,
    'productAttributes': {
        brand: true,
        grade: {enabled: true},
        quality: {enabled: true},
        sampleNumber: true,
        fssaiLicenceNumber: true
    },
    'inventoryAttributes': {
        barcode: true,
        batchNumber: true,
        manufactureDate: true,
        packagingDate: true,
        bestBefore: true,
        unitMeasure: ['Kg', 'gram', 'tonne', 'Bag', 'Case', 'Pack']
    },
    'parent': db.categories.findOne({'name': 'Pulses', 'type': 'SubCategory1'})._id
});
// Adding Sub Categories to Pulses category
db.categories.update({
    'name': 'Pulses',
    'type': 'SubCategory1'
}, {$addToSet: {'children': db.categories.findOne({'name': 'Toor Dal', 'type': 'SubCategory2'})._id}});

db.categories.update({
    'name': 'Pulses',
    'type': 'SubCategory1'
}, {$addToSet: {'children': db.categories.findOne({'name': 'Black Urad Dal Whole', 'type': 'SubCategory2'})._id}});

db.categories.update({
    'name': 'Pulses',
    'type': 'SubCategory1'
}, {$addToSet: {'children': db.categories.findOne({'name': 'Black Urad Dal Split', 'type': 'SubCategory2'})._id}});

db.categories.update({
    'name': 'Pulses',
    'type': 'SubCategory1'
}, {$addToSet: {'children': db.categories.findOne({'name': 'White Urad Dal Whole', 'type': 'SubCategory2'})._id}});

db.categories.update({
    'name': 'Pulses',
    'type': 'SubCategory1'
}, {$addToSet: {'children': db.categories.findOne({'name': 'White Urad Dal Split', 'type': 'SubCategory2'})._id}});

db.categories.update({
    'name': 'Pulses',
    'type': 'SubCategory1'
}, {$addToSet: {'children': db.categories.findOne({'name': 'Green Moong Dal Whole', 'type': 'SubCategory2'})._id}});

db.categories.update({
    'name': 'Pulses',
    'type': 'SubCategory1'
}, {$addToSet: {'children': db.categories.findOne({'name': 'Green Moong Dal Split', 'type': 'SubCategory2'})._id}});

db.categories.update({
    'name': 'Pulses',
    'type': 'SubCategory1'
}, {$addToSet: {'children': db.categories.findOne({'name': 'Yellow Moong Dal Split', 'type': 'SubCategory2'})._id}});

db.categories.update({
    'name': 'Pulses',
    'type': 'SubCategory1'
}, {$addToSet: {'children': db.categories.findOne({'name': 'Masoor Dal', 'type': 'SubCategory2'})._id}});

db.categories.update({
    'name': 'Pulses',
    'type': 'SubCategory1'
}, {$addToSet: {'children': db.categories.findOne({'name': 'Channa Dal', 'type': 'SubCategory2'})._id}});

db.categories.update({
    'name': 'Pulses',
    'type': 'SubCategory1'
}, {$addToSet: {'children': db.categories.findOne({'name': 'Kabuli Chana Dal White', 'type': 'SubCategory2'})._id}});

db.categories.update({
    'name': 'Pulses',
    'type': 'SubCategory1'
}, {$addToSet: {'children': db.categories.findOne({'name': 'Kabuli Chana Dal Brown', 'type': 'SubCategory2'})._id}});

db.categories.update({
    'name': 'Pulses',
    'type': 'SubCategory1'
}, {$addToSet: {'children': db.categories.findOne({'name': 'Horse Gram', 'type': 'SubCategory2'})._id}});

db.categories.update({
    'name': 'Pulses',
    'type': 'SubCategory1'
}, {$addToSet: {'children': db.categories.findOne({'name': 'Soya Bean', 'type': 'SubCategory2'})._id}});

db.categories.update({
    'name': 'Pulses',
    'type': 'SubCategory1'
}, {$addToSet: {'children': db.categories.findOne({'name': 'Cowpea', 'type': 'SubCategory2'})._id}});

db.categories.update({
    'name': 'Pulses',
    'type': 'SubCategory1'
}, {$addToSet: {'children': db.categories.findOne({'name': 'Sesame', 'type': 'SubCategory2'})._id}});

db.categories.update({
    'name': 'Pulses',
    'type': 'SubCategory1'
}, {$addToSet: {'children': db.categories.findOne({'name': 'Peas', 'type': 'SubCategory2'})._id}});

db.categories.update({
    'name': 'Pulses',
    'type': 'SubCategory1'
}, {$addToSet: {'children': db.categories.findOne({'name': 'Red Gram', 'type': 'SubCategory2'})._id}});

db.categories.update({
    'name': 'Pulses',
    'type': 'SubCategory1'
}, {$addToSet: {'children': db.categories.findOne({'name': 'Groundnut', 'type': 'SubCategory2'})._id}});
db.categories.update({
    'name': 'Pulses',
    'type': 'SubCategory1'
}, {$addToSet: {'children': db.categories.findOne({'name': 'Rajma Beans', 'type': 'SubCategory2'})._id}});

// **********************

// Sub Categories of Agriculture
db.categories.insert({
    'name': 'Spices',
    'code': 'SPICE',
    'type': 'SubCategory1',
    'categoryImageURL1': 'modules/categories/img/subcategory1/spices.png',
    'croppedCategoryImageURL1': 'modules/categories/img/subcategory1/mobilespices.png',
    'parent': db.categories.findOne({'name': 'Agriculture', 'type': 'MainCategory'})._id
});
// Adding Sub Category Spices to Agriculture Category
db.categories.update({
    'name': 'Agriculture',
    'type': 'MainCategory'
}, {$addToSet: {'children': db.categories.findOne({'name': 'Spices', 'type': 'SubCategory1'})._id}});

// Sub Categories of Spices
db.categories.insert({
    'name': 'Coriander',
    'code': 'CRNDR',
    'type': 'SubCategory2',
    'categoryImageURL1': 'modules/categories/img/subcategory2/coriander.png',
    'croppedCategoryImageURL1': 'modules/categories/img/subcategory2/mobilecoriander.png',
    'productCategory': true,
    'productAttributes': {
        brand: true,
        grade: {enabled: true},
        quality: {enabled: true},
        sampleNumber: true,
        fssaiLicenceNumber: true
    },
    'inventoryAttributes': {
        barcode: true,
        batchNumber: true,
        manufactureDate: true,
        packagingDate: true,
        bestBefore: true,
        unitMeasure: ['Kg', 'gram', 'tonne', 'Bag', 'Case', 'Pack']
    },
    'parent': db.categories.findOne({'name': 'Spices', 'type': 'SubCategory1'})._id
});
db.categories.insert({
    'name': 'Jeera',
    'code': 'JEERA',
    'type': 'SubCategory2',
    'categoryImageURL1': 'modules/categories/img/subcategory2/jeera.png',
    'croppedCategoryImageURL1': 'modules/categories/img/subcategory2/mobilejeera.png',
    'productCategory': true,
    'productAttributes': {
        brand: true,
        grade: {enabled: true},
        quality: {enabled: true},
        sampleNumber: true,
        fssaiLicenceNumber: true
    },
    'inventoryAttributes': {
        barcode: true,
        batchNumber: true,
        manufactureDate: true,
        packagingDate: true,
        bestBefore: true,
        unitMeasure: ['Kg', 'gram', 'tonne', 'Bag', 'Case', 'Pack']
    },
    'parent': db.categories.findOne({'name': 'Spices', 'type': 'SubCategory1'})._id
});
db.categories.insert({
    'name': 'Mustard',
    'code': 'MUSTD',
    'type': 'SubCategory2',
    'categoryImageURL1': 'modules/categories/img/subcategory2/mustard.png',
    'croppedCategoryImageURL1': 'modules/categories/img/subcategory2/mobilemustard.png',
    'productCategory': true,
    'productAttributes': {
        brand: true,
        grade: {enabled: true},
        quality: {enabled: true},
        sampleNumber: true,
        fssaiLicenceNumber: true
    },
    'inventoryAttributes': {
        barcode: true,
        batchNumber: true,
        manufactureDate: true,
        packagingDate: true,
        bestBefore: true,
        unitMeasure: ['Kg', 'gram', 'tonne', 'Bag', 'Case', 'Pack']
    },
    'parent': db.categories.findOne({'name': 'Spices', 'type': 'SubCategory1'})._id
});
db.categories.insert({
    'name': 'Methi',
    'code': 'METHI',
    'type': 'SubCategory2',
    'categoryImageURL1': 'modules/categories/img/subcategory2/methi.png',
    'croppedCategoryImageURL1': 'modules/categories/img/subcategory2/mobilemethi.png',
    'productCategory': true,
    'productAttributes': {
        brand: true,
        grade: {enabled: true},
        quality: {enabled: true},
        sampleNumber: true,
        fssaiLicenceNumber: true
    },
    'inventoryAttributes': {
        barcode: true,
        batchNumber: true,
        manufactureDate: true,
        packagingDate: true,
        bestBefore: true,
        unitMeasure: ['Kg', 'gram', 'tonne', 'Bag', 'Case', 'Pack']
    },
    'parent': db.categories.findOne({'name': 'Spices', 'type': 'SubCategory1'})._id
});
db.categories.insert({
    'name': 'Dry Chilli',
    'code': 'DRCHL',
    'type': 'SubCategory2',
    'categoryImageURL1': 'modules/categories/img/subcategory2/drychilli.png',
    'croppedCategoryImageURL1': 'modules/categories/img/subcategory2/mobiledrychilli.png',
    'productCategory': true,
    'productAttributes': {
        brand: true,
        grade: {enabled: true},
        quality: {enabled: true},
        sampleNumber: true,
        fssaiLicenceNumber: true
    },
    'inventoryAttributes': {
        barcode: true,
        batchNumber: true,
        manufactureDate: true,
        packagingDate: true,
        bestBefore: true,
        unitMeasure: ['Kg', 'gram', 'tonne', 'Bag', 'Case', 'Pack']
    },
    'parent': db.categories.findOne({'name': 'Spices', 'type': 'SubCategory1'})._id
});
db.categories.insert({
    'name': 'Tamarind',
    'code': 'TMRND',
    'type': 'SubCategory2',
    'categoryImageURL1': 'modules/categories/img/subcategory2/tamarind.png',
    'croppedCategoryImageURL1': 'modules/categories/img/subcategory2/mobiletamarind.png',
    'productCategory': true,
    'productAttributes': {
        brand: true,
        grade: {enabled: true},
        quality: {enabled: true},
        sampleNumber: true,
        fssaiLicenceNumber: true
    },
    'inventoryAttributes': {
        barcode: true,
        batchNumber: true,
        manufactureDate: true,
        packagingDate: true,
        bestBefore: true,
        unitMeasure: ['Kg', 'gram', 'tonne', 'Bag', 'Case', 'Pack']
    },
    'parent': db.categories.findOne({'name': 'Spices', 'type': 'SubCategory1'})._id
});
db.categories.insert({
    'name': 'Clove',
    'code': 'CLOVE',
    'type': 'SubCategory2',
    'categoryImageURL1': 'modules/categories/img/subcategory2/clove.png',
    'croppedCategoryImageURL1': 'modules/categories/img/subcategory2/mobileclove.png',
    'productCategory': true,
    'productAttributes': {
        brand: true,
        grade: {enabled: true},
        quality: {enabled: true},
        sampleNumber: true,
        fssaiLicenceNumber: true
    },
    'inventoryAttributes': {
        barcode: true,
        batchNumber: true,
        manufactureDate: true,
        packagingDate: true,
        bestBefore: true,
        unitMeasure: ['Kg', 'gram', 'tonne', 'Bag', 'Case', 'Pack']
    },
    'parent': db.categories.findOne({'name': 'Spices', 'type': 'SubCategory1'})._id
});
db.categories.insert({
    'name': 'Black Pepper',
    'code': 'BLKPR',
    'type': 'SubCategory2',
    'categoryImageURL1': 'modules/categories/img/subcategory2/blackpepper.png',
    'productCategory': true,
    'productAttributes': {
        brand: true,
        grade: {enabled: true},
        quality: {enabled: true},
        sampleNumber: true,
        fssaiLicenceNumber: true
    },
    'inventoryAttributes': {
        barcode: true,
        batchNumber: true,
        manufactureDate: true,
        packagingDate: true,
        bestBefore: true,
        unitMeasure: ['Kg', 'gram', 'tonne', 'Bag', 'Case', 'Pack']
    },
    'parent': db.categories.findOne({'name': 'Spices', 'type': 'SubCategory1'})._id
});
db.categories.insert({
    'name': 'Ginger',
    'code': 'GINGR',
    'type': 'SubCategory2',
    'categoryImageURL1': 'modules/categories/img/subcategory2/ginger.png',
    'croppedCategoryImageURL1': 'modules/categories/img/subcategory2/mobileginger.png',
    'productCategory': true,
    'productAttributes': {
        brand: true,
        grade: {enabled: true},
        quality: {enabled: true},
        sampleNumber: true,
        fssaiLicenceNumber: true
    },
    'inventoryAttributes': {
        barcode: true,
        batchNumber: true,
        manufactureDate: true,
        packagingDate: true,
        bestBefore: true,
        unitMeasure: ['Kg', 'gram', 'tonne', 'Bag', 'Case', 'Pack']
    },
    'parent': db.categories.findOne({'name': 'Spices', 'type': 'SubCategory1'})._id
});
db.categories.insert({
    'name': 'Carom Seed',
    'code': 'CRMSD',
    'type': 'SubCategory2',
    'categoryImageURL1': 'modules/categories/img/subcategory2/caromseed.png',
    'croppedCategoryImageURL1': 'modules/categories/img/subcategory2/mobilecaromseed.png',
    'productCategory': true,
    'productAttributes': {
        brand: true,
        grade: {enabled: true},
        quality: {enabled: true},
        sampleNumber: true,
        fssaiLicenceNumber: true
    },
    'inventoryAttributes': {
        barcode: true,
        batchNumber: true,
        manufactureDate: true,
        packagingDate: true,
        bestBefore: true,
        unitMeasure: ['Kg', 'gram', 'tonne', 'Bag', 'Case', 'Pack']
    },
    'parent': db.categories.findOne({'name': 'Spices', 'type': 'SubCategory1'})._id
});
db.categories.insert({
    'name': 'Bay Leaf',
    'code': 'BAYLF',
    'type': 'SubCategory2',
    'categoryImageURL1': 'modules/categories/img/subcategory2/bayleaf.png',
    'croppedCategoryImageURL1': 'modules/categories/img/subcategory2/mobilebayleaf.png',
    'productCategory': true,
    'productAttributes': {
        brand: true,
        grade: {enabled: true},
        quality: {enabled: true},
        sampleNumber: true,
        fssaiLicenceNumber: true
    },
    'inventoryAttributes': {
        barcode: true,
        batchNumber: true,
        manufactureDate: true,
        packagingDate: true,
        bestBefore: true,
        unitMeasure: ['Kg', 'gram', 'tonne', 'Bag', 'Case', 'Pack']
    },
    'parent': db.categories.findOne({'name': 'Spices', 'type': 'SubCategory1'})._id
});
db.categories.insert({
    'name': 'Green Cardamom',
    'code': 'GRMCM',
    'type': 'SubCategory2',
    'categoryImageURL1': 'modules/categories/img/subcategory2/greencardamom.png',
    'croppedCategoryImageURL1': 'modules/categories/img/subcategory2/mobilegreencardamom.png',
    'productCategory': true,
    'productAttributes': {
        brand: true,
        grade: {enabled: true},
        quality: {enabled: true},
        sampleNumber: true,
        fssaiLicenceNumber: true
    },
    'inventoryAttributes': {
        barcode: true,
        batchNumber: true,
        manufactureDate: true,
        packagingDate: true,
        bestBefore: true,
        unitMeasure: ['Kg', 'gram', 'tonne', 'Bag', 'Case', 'Pack']
    },
    'parent': db.categories.findOne({'name': 'Spices', 'type': 'SubCategory1'})._id
});
db.categories.insert({
    'name': 'Dry Coconut',
    'code': 'DRYCN',
    'type': 'SubCategory2',
    'categoryImageURL1': 'modules/categories/img/subcategory2/drycoconut.png',
    'croppedCategoryImageURL1': 'modules/categories/img/subcategory2/mobiledrycoconut.png',
    'productCategory': true,
    'productAttributes': {
        brand: true,
        grade: {enabled: true},
        quality: {enabled: true},
        sampleNumber: true,
        fssaiLicenceNumber: true
    },
    'inventoryAttributes': {
        barcode: true,
        batchNumber: true,
        manufactureDate: true,
        packagingDate: true,
        bestBefore: true,
        unitMeasure: ['Kg', 'gram', 'tonne', 'Bag', 'Case', 'Pack']
    },
    'parent': db.categories.findOne({'name': 'Spices', 'type': 'SubCategory1'})._id
});
db.categories.insert({
    'name': 'Salt',
    'code': 'SALTS',
    'type': 'SubCategory2',
    'categoryImageURL1': 'modules/categories/img/subcategory2/salt.png',
    'croppedCategoryImageURL1': 'modules/categories/img/subcategory2/mobilesalt.png',
    'productCategory': true,
    'productAttributes': {
        brand: true,
        grade: {enabled: true},
        quality: {enabled: true},
        sampleNumber: true,
        fssaiLicenceNumber: true
    },
    'inventoryAttributes': {
        barcode: true,
        batchNumber: true,
        manufactureDate: true,
        packagingDate: true,
        bestBefore: true,
        unitMeasure: ['Kg', 'gram', 'tonne', 'Bag', 'Case', 'Pack']
    },
    'parent': db.categories.findOne({'name': 'Spices', 'type': 'SubCategory1'})._id
});
db.categories.insert({
    'name': 'Cassia',
    'code': 'CASSI',
    'type': 'SubCategory2',
    'categoryImageURL1': 'modules/categories/img/subcategory2/cassia.png',
    'croppedCategoryImageURL1': 'modules/categories/img/subcategory2/mobilecassia.png',
    'productCategory': true,
    'productAttributes': {
        brand: true,
        grade: {enabled: true},
        quality: {enabled: true},
        sampleNumber: true,
        fssaiLicenceNumber: true
    },
    'inventoryAttributes': {
        barcode: true,
        batchNumber: true,
        manufactureDate: true,
        packagingDate: true,
        bestBefore: true,
        unitMeasure: ['Kg', 'gram', 'tonne', 'Bag', 'Case', 'Pack']
    },
    'parent': db.categories.findOne({'name': 'Spices', 'type': 'SubCategory1'})._id
});
db.categories.insert({
    'name': 'Turmeric',
    'code': 'TURMC',
    'type': 'SubCategory2',
    'categoryImageURL1': 'modules/categories/img/subcategory2/turmeric.png',
    'croppedCategoryImageURL1': 'modules/categories/img/subcategory2/mobileturmeric.png',
    'productCategory': true,
    'productAttributes': {
        brand: true,
        grade: {enabled: true},
        quality: {enabled: true},
        sampleNumber: true,
        fssaiLicenceNumber: true
    },
    'inventoryAttributes': {
        barcode: true,
        batchNumber: true,
        manufactureDate: true,
        packagingDate: true,
        bestBefore: true,
        unitMeasure: ['Kg', 'gram', 'tonne', 'Bag', 'Case', 'Pack']
    },
    'parent': db.categories.findOne({'name': 'Spices', 'type': 'SubCategory1'})._id
});
db.categories.insert({
    'name': 'Jaggery Powder',
    'code': 'JGYPD',
    'type': 'SubCategory2',
    'categoryImageURL1': 'modules/categories/img/subcategory2/jaggerypowder.png',
    'croppedCategoryImageURL1': 'modules/categories/img/subcategory2/mobilejaggerypowder.png',
    'productCategory': true,
    'productAttributes': {
        brand: true,
        grade: {enabled: true},
        quality: {enabled: true},
        sampleNumber: true,
        fssaiLicenceNumber: true
    },
    'inventoryAttributes': {
        barcode: true,
        batchNumber: true,
        manufactureDate: true,
        packagingDate: true,
        bestBefore: true,
        unitMeasure: ['Kg', 'gram', 'tonne', 'Bag', 'Case', 'Pack']
    },
    'parent': db.categories.findOne({'name': 'Spices', 'type': 'SubCategory1'})._id
});
///
db.categories.insert({
    'name': 'Black Till',
    'code': 'BAKTL',
    'type': 'SubCategory2',
    'categoryImageURL1': 'modules/categories/img/subcategory2/blacktill.png',
    'croppedCategoryImageURL1': 'modules/categories/img/subcategory2/mobileblacktill.png',
    'productCategory': true,
    'productAttributes': {
        brand: true,
        grade: {enabled: true},
        quality: {enabled: true},
        sampleNumber: true,
        fssaiLicenceNumber: true
    },
    'inventoryAttributes': {
        barcode: true,
        batchNumber: true,
        manufactureDate: true,
        packagingDate: true,
        bestBefore: true,
        unitMeasure: ['Kg', 'gram', 'tonne', 'Bag', 'Case', 'Pack']
    },
    'parent': db.categories.findOne({'name': 'Spices', 'type': 'SubCategory1'})._id
});
db.categories.insert({
    'name': 'White Till',
    'code': 'WIETL',
    'type': 'SubCategory2',
    'categoryImageURL1': 'modules/categories/img/subcategory2/whitetill.png',
    'croppedCategoryImageURL1': 'modules/categories/img/subcategory2/mobilewhitetill.png',
    'productCategory': true,
    'productAttributes': {
        brand: true,
        grade: {enabled: true},
        quality: {enabled: true},
        sampleNumber: true,
        fssaiLicenceNumber: true
    },
    'inventoryAttributes': {
        barcode: true,
        batchNumber: true,
        manufactureDate: true,
        packagingDate: true,
        bestBefore: true,
        unitMeasure: ['Kg', 'gram', 'tonne', 'Bag', 'Case', 'Pack']
    },
    'parent': db.categories.findOne({'name': 'Spices', 'type': 'SubCategory1'})._id
});
db.categories.insert({
    'name': 'Poppy Seeds',
    'code': 'POYSD',
    'type': 'SubCategory2',
    'categoryImageURL1': 'modules/categories/img/subcategory2/poppyseeds.png',
    'croppedCategoryImageURL1': 'modules/categories/img/subcategory2/mobilepoppyseeds.png',
    'productCategory': true,
    'productAttributes': {
        brand: true,
        grade: {enabled: true},
        quality: {enabled: true},
        sampleNumber: true,
        fssaiLicenceNumber: true
    },
    'inventoryAttributes': {
        barcode: true,
        batchNumber: true,
        manufactureDate: true,
        packagingDate: true,
        bestBefore: true,
        unitMeasure: ['Kg', 'gram', 'tonne', 'Bag', 'Case', 'Pack']
    },
    'parent': db.categories.findOne({'name': 'Spices', 'type': 'SubCategory1'})._id
});
db.categories.insert({
    'name': 'Javitri',
    'code': 'JAVTI',
    'type': 'SubCategory2',
    'categoryImageURL1': 'modules/categories/img/subcategory2/javitri.png',
    'croppedCategoryImageURL1': 'modules/categories/img/subcategory2/mobilejavitri.png',
    'productCategory': true,
    'productAttributes': {
        brand: true,
        grade: {enabled: true},
        quality: {enabled: true},
        sampleNumber: true,
        fssaiLicenceNumber: true
    },
    'inventoryAttributes': {
        barcode: true,
        batchNumber: true,
        manufactureDate: true,
        packagingDate: true,
        bestBefore: true,
        unitMeasure: ['Kg', 'gram', 'tonne', 'Bag', 'Case', 'Pack']
    },
    'parent': db.categories.findOne({'name': 'Spices', 'type': 'SubCategory1'})._id
});
db.categories.insert({
    'name': 'Sabja',
    'code': 'SABJA',
    'type': 'SubCategory2',
    'categoryImageURL1': 'modules/categories/img/subcategory2/sabja.png',
    'croppedCategoryImageURL1': 'modules/categories/img/subcategory2/mobilesabja.png',
    'productCategory': true,
    'productAttributes': {
        brand: true,
        grade: {enabled: true},
        quality: {enabled: true},
        sampleNumber: true,
        fssaiLicenceNumber: true
    },
    'inventoryAttributes': {
        barcode: true,
        batchNumber: true,
        manufactureDate: true,
        packagingDate: true,
        bestBefore: true,
        unitMeasure: ['Kg', 'gram', 'tonne', 'Bag', 'Case', 'Pack']
    },
    'parent': db.categories.findOne({'name': 'Spices', 'type': 'SubCategory1'})._id
});
db.categories.insert({
    'name': 'Sara Pappu',
    'code': 'SARPU',
    'type': 'SubCategory2',
    'categoryImageURL1': 'modules/categories/img/subcategory2/sarapappu.png',
    'croppedCategoryImageURL1': 'modules/categories/img/subcategory2/mobilesarapappu.png',
    'productCategory': true,
    'productAttributes': {
        brand: true,
        grade: {enabled: true},
        quality: {enabled: true},
        sampleNumber: true,
        fssaiLicenceNumber: true
    },
    'inventoryAttributes': {
        barcode: true,
        batchNumber: true,
        manufactureDate: true,
        packagingDate: true,
        bestBefore: true,
        unitMeasure: ['Kg', 'gram', 'tonne', 'Bag', 'Case', 'Pack']
    },
    'parent': db.categories.findOne({'name': 'Spices', 'type': 'SubCategory1'})._id
});
db.categories.insert({
    'name': 'Melon Seeds',
    'code': 'MLNSD',
    'type': 'SubCategory2',
    'categoryImageURL1': 'modules/categories/img/subcategory2/melonseeds.png',
    'croppedCategoryImageURL1': 'modules/categories/img/subcategory2/mobilemelonseeds.png',
    'productCategory': true,
    'productAttributes': {
        brand: true,
        grade: {enabled: true},
        quality: {enabled: true},
        sampleNumber: true,
        fssaiLicenceNumber: true
    },
    'inventoryAttributes': {
        barcode: true,
        batchNumber: true,
        manufactureDate: true,
        packagingDate: true,
        bestBefore: true,
        unitMeasure: ['Kg', 'gram', 'tonne', 'Bag', 'Case', 'Pack']
    },
    'parent': db.categories.findOne({'name': 'Spices', 'type': 'SubCategory1'})._id
});
db.categories.insert({
    'name': 'Jakayi',
    'code': 'JAKYI',
    'type': 'SubCategory2',
    'categoryImageURL1': 'modules/categories/img/subcategory2/jakayi.png',
    'croppedCategoryImageURL1': 'modules/categories/img/subcategory2/mobilejakayi.png',
    'productCategory': true,
    'productAttributes': {
        brand: true,
        grade: {enabled: true},
        quality: {enabled: true},
        sampleNumber: true,
        fssaiLicenceNumber: true
    },
    'inventoryAttributes': {
        barcode: true,
        batchNumber: true,
        manufactureDate: true,
        packagingDate: true,
        bestBefore: true,
        unitMeasure: ['Kg', 'gram', 'tonne', 'Bag', 'Case', 'Pack']
    },
    'parent': db.categories.findOne({'name': 'Spices', 'type': 'SubCategory1'})._id
});
// Adding Sub Categories to Spices category
db.categories.update({
    'name': 'Spices',
    'type': 'SubCategory1'
}, {$addToSet: {'children': db.categories.findOne({'name': 'Coriander', 'type': 'SubCategory2'})._id}});

db.categories.update({
    'name': 'Spices',
    'type': 'SubCategory1'
}, {$addToSet: {'children': db.categories.findOne({'name': 'Jeera', 'type': 'SubCategory2'})._id}});

db.categories.update({
    'name': 'Spices',
    'type': 'SubCategory1'
}, {$addToSet: {'children': db.categories.findOne({'name': 'Mustard', 'type': 'SubCategory2'})._id}});

db.categories.update({
    'name': 'Spices',
    'type': 'SubCategory1'
}, {$addToSet: {'children': db.categories.findOne({'name': 'Methi', 'type': 'SubCategory2'})._id}});

db.categories.update({
    'name': 'Spices',
    'type': 'SubCategory1'
}, {$addToSet: {'children': db.categories.findOne({'name': 'Dry Chilli', 'type': 'SubCategory2'})._id}});

db.categories.update({
    'name': 'Spices',
    'type': 'SubCategory1'
}, {$addToSet: {'children': db.categories.findOne({'name': 'Tamarind', 'type': 'SubCategory2'})._id}});

db.categories.update({
    'name': 'Spices',
    'type': 'SubCategory1'
}, {$addToSet: {'children': db.categories.findOne({'name': 'Clove', 'type': 'SubCategory2'})._id}});

db.categories.update({
    'name': 'Spices',
    'type': 'SubCategory1'
}, {$addToSet: {'children': db.categories.findOne({'name': 'Black Pepper', 'type': 'SubCategory2'})._id}});

db.categories.update({
    'name': 'Spices',
    'type': 'SubCategory1'
}, {$addToSet: {'children': db.categories.findOne({'name': 'Ginger', 'type': 'SubCategory2'})._id}});

db.categories.update({
    'name': 'Spices',
    'type': 'SubCategory1'
}, {$addToSet: {'children': db.categories.findOne({'name': 'Carom Seed', 'type': 'SubCategory2'})._id}});

db.categories.update({
    'name': 'Spices',
    'type': 'SubCategory1'
}, {$addToSet: {'children': db.categories.findOne({'name': 'Bay Leaf', 'type': 'SubCategory2'})._id}});

db.categories.update({
    'name': 'Spices',
    'type': 'SubCategory1'
}, {$addToSet: {'children': db.categories.findOne({'name': 'Green Cardamom', 'type': 'SubCategory2'})._id}});

db.categories.update({
    'name': 'Spices',
    'type': 'SubCategory1'
}, {$addToSet: {'children': db.categories.findOne({'name': 'Dry Coconut', 'type': 'SubCategory2'})._id}});

db.categories.update({
    'name': 'Spices',
    'type': 'SubCategory1'
}, {$addToSet: {'children': db.categories.findOne({'name': 'Salt', 'type': 'SubCategory2'})._id}});

db.categories.update({
    'name': 'Spices',
    'type': 'SubCategory1'
}, {$addToSet: {'children': db.categories.findOne({'name': 'Cassia', 'type': 'SubCategory2'})._id}});

db.categories.update({
    'name': 'Spices',
    'type': 'SubCategory1'
}, {$addToSet: {'children': db.categories.findOne({'name': 'Turmeric', 'type': 'SubCategory2'})._id}});

db.categories.update({
    'name': 'Spices',
    'type': 'SubCategory1'
}, {$addToSet: {'children': db.categories.findOne({'name': 'Jaggery Powder', 'type': 'SubCategory2'})._id}});

db.categories.update({
    'name': 'Spices',
    'type': 'SubCategory1'
}, {$addToSet: {'children': db.categories.findOne({'name': 'Black Till', 'type': 'SubCategory2'})._id}});

db.categories.update({
    'name': 'Spices',
    'type': 'SubCategory1'
}, {$addToSet: {'children': db.categories.findOne({'name': 'White Till', 'type': 'SubCategory2'})._id}});

db.categories.update({
    'name': 'Spices',
    'type': 'SubCategory1'
}, {$addToSet: {'children': db.categories.findOne({'name': 'Poppy Seeds', 'type': 'SubCategory2'})._id}});

db.categories.update({
    'name': 'Spices',
    'type': 'SubCategory1'
}, {$addToSet: {'children': db.categories.findOne({'name': 'Javitri', 'type': 'SubCategory2'})._id}});

db.categories.update({
    'name': 'Spices',
    'type': 'SubCategory1'
}, {$addToSet: {'children': db.categories.findOne({'name': 'Sabja','type': 'SubCategory2'})._id}});

db.categories.update({
    'name': 'Spices',
    'type': 'SubCategory1'
}, {$addToSet: {'children': db.categories.findOne({'name': 'Sara Pappu','type': 'SubCategory2'})._id}});

db.categories.update({
    'name': 'Spices',
    'type': 'SubCategory1'
}, {$addToSet: {'children': db.categories.findOne({'name': 'Melon Seeds','type': 'SubCategory2'})._id}});

db.categories.update({
    'name': 'Spices',
    'type': 'SubCategory1'
}, {$addToSet: {'children': db.categories.findOne({'name': 'Jakayi','type': 'SubCategory2'})._id}});


db.categories.insert({
    'name': 'Cotton',
    'code': 'COTTN',
    'type': 'SubCategory1',
    'categoryImageURL1': 'modules/categories/img/subcategory1/cottons.png',
    'croppedCategoryImageURL1': 'modules/categories/img/subcategory1/mobilecottons.png',
    'parent': db.categories.findOne({'name': 'Agriculture', 'type': 'MainCategory'})._id
});
db.categories.update({
    'name': 'Agriculture',
    'type': 'MainCategory'
}, {$addToSet: {'children': db.categories.findOne({'name': 'Cotton', 'type': 'SubCategory1'})._id}});

db.categories.insert({
    'name': 'Cotton Bales',
    'code': 'COTBL',
    'type': 'SubCategory2',
    'categoryImageURL1': 'modules/categories/img/subcategory2/cotton.png',
    'croppedCategoryImageURL1': 'modules/categories/img/subcategory2/mobilecotton.png',
    'productCategory': true,
    'productAttributes': {
        brand: true,
        grade: {enabled: true},
        quality: {enabled: true},
        testCertifcate: true,
        sampleNumber: true
    },
    'inventoryAttributes': {
        barcode: true,
        batchNumber: true,
        manufactureDate: true,
        packagingDate: true,
        inventoryTestCertifcate: true,
        bestBefore: true,
        unitMeasure: ['Kg', 'Bale', 'Candy', 'tonne', 'Bag', 'Case', 'Pack']
    },
    'parent': db.categories.findOne({'name': 'Cotton', 'type': 'SubCategory1'})._id
});
db.categories.update({
    'name': 'Cotton',
    'type': 'SubCategory1'
}, {$addToSet: {'children': db.categories.findOne({'name': 'Cotton Bales', 'type': 'SubCategory2'})._id}});

db.categories.insert({
    'name': 'Cotton Seeds',
    'code': 'COTSD',
    'type': 'SubCategory2',
    'categoryImageURL1': 'modules/categories/img/subcategory2/cottonseeds.png',
    'croppedCategoryImageURL1': 'modules/categories/img/subcategory2/mobilecottonseeds.png',
    'productCategory': true,
    'productAttributes': {
        brand: true,
        grade: {enabled: true},
        quality: {enabled: true},
        sampleNumber: true
    },
    'inventoryAttributes': {
        barcode: true,
        batchNumber: true,
        manufactureDate: true,
        packagingDate: true,
        bestBefore: true,
        unitMeasure: ['Kg', 'gram', 'tonne', 'Bag', 'Case', 'Pack']
    },
    'parent': db.categories.findOne({'name': 'Cotton', 'type': 'SubCategory1'})._id
});
db.categories.update({
    'name': 'Cotton',
    'type': 'SubCategory1'
}, {$addToSet: {'children': db.categories.findOne({'name': 'Cotton Seeds', 'type': 'SubCategory2'})._id}});

// **********************

// Textiles Main Category

db.categories.insert({'name': 'Textiles', 'code': 'TEXTL', 'type': 'MainCategory'});

db.categories.insert({
    'name': 'Yarn',
    'code': 'YARNT',
    'type': 'SubCategory1',
    'categoryImageURL1': 'modules/categories/img/subcategory1/yarn.png',
    'croppedCategoryImageURL1': 'modules/categories/img/subcategory1/mobileyarn.png',
    'parent': db.categories.findOne({'name': 'Textiles', 'type': 'MainCategory'})._id
});

db.categories.update({
    'name': 'Textiles',
    'type': 'MainCategory'
}, {$addToSet: {'children': db.categories.findOne({'name': 'Yarn', 'type': 'SubCategory1'})._id}});

db.categories.insert({
    'name': 'Cotton Yarn',
    'code': 'COTYN',
    'type': 'SubCategory2',
    'categoryImageURL1': 'modules/categories/img/subcategory2/cottonyarn.png',
    'croppedCategoryImageURL1': 'modules/categories/img/subcategory2/mobilecottonyarn.png',
    'productCategory': true,
    'productAttributes': {
        brand: true,
        grade: {enabled: true},
        quality: {enabled: true},
        testCertifcate: true,
        sampleNumber: true
    },
    'inventoryAttributes': {
        barcode: true,
        batchNumber: true,
        manufactureDate: true,
        packagingDate: true,
        inventoryTestCertifcate: true,
        bestBefore: true,
        unitMeasure: ['Kg', 'Bundle', 'tonne', 'Bag', 'Case', 'Pack']
    },
    'parent': db.categories.findOne({'name': 'Yarn', 'type': 'SubCategory1'})._id
});

db.categories.insert({
    'name': 'Silk Yarn',
    'code': 'SLKYN',
    'type': 'SubCategory2',
    'categoryImageURL1': 'modules/categories/img/subcategory2/silkyarn.png',
    'croppedCategoryImageURL1': 'modules/categories/img/subcategory2/mobilesilkyarn.png',
    'productCategory': true,
    'productAttributes': {
        brand: true,
        grade: {enabled: true},
        quality: {enabled: true},
        testCertifcate: true,
        sampleNumber: true
    },
    'inventoryAttributes': {
        barcode: true,
        batchNumber: true,
        manufactureDate: true,
        packagingDate: true,
        inventoryTestCertifcate: true,
        bestBefore: true,
        unitMeasure: ['Kg', 'Bundle', 'tonne', 'Bag', 'Case', 'Pack']
    },
    'parent': db.categories.findOne({'name': 'Yarn', 'type': 'SubCategory1'})._id
});

db.categories.insert({
    'name': 'Wool Yarn',
    'code': 'WOLYN',
    'type': 'SubCategory2',
    'croppedCategoryImageURL1': 'modules/categories/img/subcategory2/mobilewoolyarn.png',
    'productCategory': true,
    'productAttributes': {
        brand: true,
        grade: {enabled: true},
        quality: {enabled: true},
        testCertifcate: true,
        sampleNumber: true
    },
    'inventoryAttributes': {
        barcode: true,
        batchNumber: true,
        manufactureDate: true,
        packagingDate: true,
        inventoryTestCertifcate: true,
        bestBefore: true,
        unitMeasure: ['Kg', 'Bundle', 'tonne', 'Bag', 'Case', 'Pack']
    }   ,
    'parent': db.categories.findOne({'name': 'Yarn', 'type': 'SubCategory1'})._id
});

// Adding Sub Categories to Yarn category
db.categories.update({
    'name': 'Yarn',
    'type': 'SubCategory1'
}, {$addToSet: {'children': db.categories.findOne({'name': 'Cotton Yarn', 'type': 'SubCategory2'})._id}});

db.categories.update({
    'name': 'Yarn',
    'type': 'SubCategory1'
}, {$addToSet: {'children': db.categories.findOne({'name': 'Silk Yarn', 'type': 'SubCategory2'})._id}});

db.categories.update({
    'name': 'Yarn',
    'type': 'SubCategory1'
}, {$addToSet: {'children': db.categories.findOne({'name': 'Wool Yarn', 'type': 'SubCategory2'})._id}});

// Adding Fabric SubCategory1
db.categories.insert({
    'name': 'Fabric',
    'code': 'FABRC',
    'type': 'SubCategory1',
    'categoryImageURL1': 'modules/categories/img/subcategory1/fabric.png',
    'croppedCategoryImageURL1': 'modules/categories/img/subcategory1/mobilefabric.png',
    'parent': db.categories.findOne({'name': 'Textiles', 'type': 'MainCategory'})._id
});

db.categories.update({
    'name': 'Textiles',
    'type': 'MainCategory'
}, {$addToSet: {'children': db.categories.findOne({'name': 'Fabric', 'type': 'SubCategory1'})._id}});

db.categories.insert({
    'name': 'Cotton Fabric',
    'code': 'COTFB',
    'type': 'SubCategory2',
    'categoryImageURL1': 'modules/categories/img/subcategory2/cottonfabric.png',
    'croppedCategoryImageURL1': 'modules/categories/img/subcategory2/mobilecottonfabric.png',
    'productCategory': true,
    'productAttributes': {
        brand: true,
        grade: {enabled: true},
        quality: {enabled: true},
        testCertifcate: true,
        sampleNumber: true
    },
    'inventoryAttributes': {
        barcode: true,
        batchNumber: true,
        manufactureDate: true,
        packagingDate: true,
        inventoryTestCertifcate: true,
        bestBefore: true,
        unitMeasure: ['Kg', 'Bundle', 'tonne', 'Bag', 'Case', 'Pack']
    },
    'parent': db.categories.findOne({'name': 'Fabric', 'type': 'SubCategory1'})._id
});

db.categories.insert({
    'name': 'Silk Fabric',
    'code': 'SLKFB',
    'type': 'SubCategory2',
    'categoryImageURL1': 'modules/categories/img/subcategory2/silkfabric.png',
    'croppedCategoryImageURL1': 'modules/categories/img/subcategory2/mobilesilkfabric.png',
    'productCategory': true,
    'productAttributes': {
        brand: true,
        grade: {enabled: true},
        quality: {enabled: true},
        testCertifcate: true,
        sampleNumber: true
    },
    'inventoryAttributes': {
        barcode: true,
        batchNumber: true,
        manufactureDate: true,
        packagingDate: true,
        inventoryTestCertifcate: true,
        bestBefore: true,
        unitMeasure: ['Kg', 'Bundle', 'tonne', 'Bag', 'Case', 'Pack']
    },
    'parent': db.categories.findOne({'name': 'Fabric', 'type': 'SubCategory1'})._id
});

db.categories.insert({
    'name': 'Wool Fabric',
    'code': 'WOLFB',
    'type': 'SubCategory2',
    'categoryImageURL1': 'modules/categories/img/subcategory2/woolfabric.png',
    'croppedCategoryImageURL1': 'modules/categories/img/subcategory2/mobilewoolfabric.png',
    'productCategory': true,
    'productAttributes': {
        brand: true,
        grade: {enabled: true},
        quality: {enabled: true},
        testCertifcate: true,
        sampleNumber: true
    },
    'inventoryAttributes': {
        barcode: true,
        batchNumber: true,
        manufactureDate: true,
        packagingDate: true,
        inventoryTestCertifcate: true,
        bestBefore: true,
        unitMeasure: ['Kg', 'Bundle', 'tonne', 'Bag', 'Case', 'Pack']
    },
    'parent': db.categories.findOne({'name': 'Fabric', 'type': 'SubCategory1'})._id
});

// Adding Sub Categories to Fabric category
db.categories.update({
    'name': 'Fabric',
    'type': 'SubCategory1'
}, {$addToSet: {'children': db.categories.findOne({'name': 'Cotton Fabric', 'type': 'SubCategory2'})._id}});

db.categories.update({
    'name': 'Fabric',
    'type': 'SubCategory1'
}, {$addToSet: {'children': db.categories.findOne({'name': 'Silk Fabric', 'type': 'SubCategory2'})._id}});

db.categories.update({
    'name': 'Fabric',
    'type': 'SubCategory1'
}, {$addToSet: {'children': db.categories.findOne({'name': 'Wool Fabric', 'type': 'SubCategory2'})._id}});


// Adding Apparel SubCategory1
db.categories.insert({
    'name': 'Apparel',
    'code': 'APPRL',
    'type': 'SubCategory1',
    'categoryImageURL1': 'modules/categories/img/subcategory1/apparel.png',
    'croppedCategoryImageURL1': 'modules/categories/img/subcategory1/mobileapparel.png',
    'parent': db.categories.findOne({'name': 'Textiles', 'type': 'MainCategory'})._id
});

db.categories.update({
    'name': 'Textiles',
    'type': 'MainCategory'
}, {$addToSet: {'children': db.categories.findOne({'name': 'Apparel', 'type': 'SubCategory1'})._id}});

db.categories.insert({
    'name': 'Cotton Apparel',
    'code': 'COTAP',
    'type': 'SubCategory2',
    'categoryImageURL1': 'modules/categories/img/subcategory2/cottonapparel.png',
    'croppedCategoryImageURL1': 'modules/categories/img/subcategory2/mobilecottonapparel.png',
    'productCategory': true,
    'productAttributes': {
        brand: true,
        grade: {enabled: true},
        quality: {enabled: true},
        testCertifcate: true,
        sampleNumber: true
    },
    'inventoryAttributes': {
        barcode: true,
        batchNumber: true,
        manufactureDate: true,
        packagingDate: true,
        inventoryTestCertifcate: true,
        bestBefore: true,
        unitMeasure: ['Kg', 'Bundle', 'tonne', 'Bag', 'Case', 'Pack']
    },
    'parent': db.categories.findOne({'name': 'Apparel', 'type': 'SubCategory1'})._id
});

db.categories.insert({
    'name': 'Silk Apparel',
    'code': 'SLKAP',
    'type': 'SubCategory2',
    'categoryImageURL1': 'modules/categories/img/subcategory2/silkapparel.png',
    'croppedCategoryImageURL1': 'modules/categories/img/subcategory2/mobilesilkapparel.png',
    'productCategory': true,
    'productAttributes': {
        brand: true,
        grade: {enabled: true},
        quality: {enabled: true},
        testCertifcate: true,
        sampleNumber: true
    },
    'inventoryAttributes': {
        barcode: true,
        batchNumber: true,
        manufactureDate: true,
        packagingDate: true,
        inventoryTestCertifcate: true,
        bestBefore: true,
        unitMeasure: ['Kg', 'Bundle', 'tonne', 'Bag', 'Case', 'Pack']
    },
    'parent': db.categories.findOne({'name': 'Apparel', 'type': 'SubCategory1'})._id
});

db.categories.insert({
    'name': 'Wool Apparel',
    'code': 'WOLAP',
    'type': 'SubCategory2',
    'categoryImageURL1': 'modules/categories/img/subcategory2/woolapparel.png',
    'croppedCategoryImageURL1': 'modules/categories/img/subcategory2/mobilewoolapparel.png',
    'productCategory': true,
    'productAttributes': {
        brand: true,
        grade: {enabled: true},
        quality: {enabled: true},
        testCertifcate: true,
        sampleNumber: true
    },
    'inventoryAttributes': {
        barcode: true,
        batchNumber: true,
        manufactureDate: true,
        packagingDate: true,
        inventoryTestCertifcate: true,
        bestBefore: true,
        unitMeasure: ['Kg', 'Bundle', 'tonne', 'Bag', 'Case', 'Pack']
    },
    'parent': db.categories.findOne({'name': 'Apparel', 'type': 'SubCategory1'})._id
});

// Adding Sub Categories to Apparel category
db.categories.update({
    'name': 'Apparel',
    'type': 'SubCategory1'
}, {$addToSet: {'children': db.categories.findOne({'name': 'Cotton Apparel', 'type': 'SubCategory2'})._id}});

db.categories.update({
    'name': 'Apparel',
    'type': 'SubCategory1'
}, {$addToSet: {'children': db.categories.findOne({'name': 'Silk Apparel', 'type': 'SubCategory2'})._id}});

db.categories.update({
    'name': 'Apparel',
    'type': 'SubCategory1'
}, {$addToSet: {'children': db.categories.findOne({'name': 'Wool Apparel', 'type': 'SubCategory2'})._id}});

// Add millets sub category

db.categories.insert({
    'name': 'Barnyard Millet',
    'code': 'BYMLT',
    'type': 'SubCategory2',
    'categoryImageURL1': 'modules/categories/img/subcategory2/barnyardmillet.png',
    'croppedCategoryImageURL1': 'modules/categories/img/subcategory2/mobilebarnyardmillet.png',
    'productCategory': true,
    'productAttributes': {
        brand: true,
        grade: {enabled: true},
        quality: {enabled: true},
        sampleNumber: true,
        fssaiLicenceNumber: true
    },
    'inventoryAttributes': {
        barcode: true,
        batchNumber: true,
        manufactureDate: true,
        packagingDate: true,
        bestBefore: true,
        unitMeasure: ['Kg', 'gram', 'tonne', 'Bag', 'Case', 'Pack']
    },
    'parent': db.categories.findOne({'name': 'Cereals', 'type': 'SubCategory1'})._id
});

db.categories.insert({
    'name': 'Foxtail Millet',
    'code': 'FTMLT',
    'type': 'SubCategory2',
    'categoryImageURL1': 'modules/categories/img/subcategory2/foxtailmillet.png',
    'croppedCategoryImageURL1': 'modules/categories/img/subcategory2/mobilefoxtailmillet.png',
    'productCategory': true,
    'productAttributes': {
        brand: true,
        grade: {enabled: true},
        quality: {enabled: true},
        sampleNumber: true,
        fssaiLicenceNumber: true
    },
    'inventoryAttributes': {
        barcode: true,
        batchNumber: true,
        manufactureDate: true,
        packagingDate: true,
        bestBefore: true,
        unitMeasure: ['Kg', 'gram', 'tonne', 'Bag', 'Case', 'Pack']
    },
    'parent': db.categories.findOne({'name': 'Cereals', 'type': 'SubCategory1'})._id
});

db.categories.insert({
    'name': 'Kodo Millet',
    'code': 'KOMLT',
    'type': 'SubCategory2',
    'categoryImageURL1': 'modules/categories/img/subcategory2/kodomillet.png',
    'croppedCategoryImageURL1': 'modules/categories/img/subcategory2/mobilekodomillet.png',
    'productCategory': true,
    'productAttributes': {
        brand: true,
        grade: {enabled: true},
        quality: {enabled: true},
        sampleNumber: true,
        fssaiLicenceNumber: true
    },
    'inventoryAttributes': {
        barcode: true,
        batchNumber: true,
        manufactureDate: true,
        packagingDate: true,
        bestBefore: true,
        unitMeasure: ['Kg', 'gram', 'tonne', 'Bag', 'Case', 'Pack']
    },
    'parent': db.categories.findOne({'name': 'Cereals', 'type': 'SubCategory1'})._id
});

db.categories.insert({
    'name': 'Little Millet',
    'code': 'LTMLT',
    'type': 'SubCategory2',
    'categoryImageURL1': 'modules/categories/img/subcategory2/littlemillet.png',
    'croppedCategoryImageURL1': 'modules/categories/img/subcategory2/mobilelittlemillet.png',
    'productCategory': true,
    'productAttributes': {
        brand: true,
        grade: {enabled: true},
        quality: {enabled: true},
        sampleNumber: true,
        fssaiLicenceNumber: true
    },
    'inventoryAttributes': {
        barcode: true,
        batchNumber: true,
        manufactureDate: true,
        packagingDate: true,
        bestBefore: true,
        unitMeasure: ['Kg', 'gram', 'tonne', 'Bag', 'Case', 'Pack']
    },
    'parent': db.categories.findOne({'name': 'Cereals', 'type': 'SubCategory1'})._id
});

db.categories.insert({
    'name': 'Proso Millet',
    'code': 'PSMLT',
    'type': 'SubCategory2',
    'categoryImageURL1': 'modules/categories/img/subcategory2/prosomillet.png',
    'croppedCategoryImageURL1': 'modules/categories/img/subcategory2/mobileprosomillet.png',
    'productCategory': true,
    'productAttributes': {
        brand: true,
        grade: {enabled: true},
        quality: {enabled: true},
        sampleNumber: true,
        fssaiLicenceNumber: true
    },
    'inventoryAttributes': {
        barcode: true,
        batchNumber: true,
        manufactureDate: true,
        packagingDate: true,
        bestBefore: true,
        unitMeasure: ['Kg', 'gram', 'tonne', 'Bag', 'Case', 'Pack']
    },
    'parent': db.categories.findOne({'name': 'Cereals', 'type': 'SubCategory1'})._id
});

// Adding Millets Sub Categories to Cereals category
db.categories.update({
    'name': 'Cereals',
    'type': 'SubCategory1'
}, {$addToSet: {'children': db.categories.findOne({'name': 'Barnyard Millet', 'type': 'SubCategory2'})._id}});

db.categories.update({
    'name': 'Cereals',
    'type': 'SubCategory1'
}, {$addToSet: {'children': db.categories.findOne({'name': 'Foxtail Millet', 'type': 'SubCategory2'})._id}});

db.categories.update({
    'name': 'Cereals',
    'type': 'SubCategory1'
}, {$addToSet: {'children': db.categories.findOne({'name': 'Kodo Millet', 'type': 'SubCategory2'})._id}});

db.categories.update({
    'name': 'Cereals',
    'type': 'SubCategory1'
}, {$addToSet: {'children': db.categories.findOne({'name': 'Little Millet', 'type': 'SubCategory2'})._id}});

db.categories.update({
    'name': 'Cereals',
    'type': 'SubCategory1'
}, {$addToSet: {'children': db.categories.findOne({'name': 'Proso Millet', 'type': 'SubCategory2'})._id}});


// FMCG Start

db.categories.insert({
    'name': 'FMCG',
    'code': 'FMCGC',
    'type': 'MainCategory'
});

// Sub Categories of FMCG

// Start - Health Care
db.categories.insert({
    'name': 'Health Care',
    'code': 'HCARE',
    'type': 'SubCategory1',
    'categoryImageURL1': 'modules/categories/img/subcategory1/healthcare.png',
    'croppedCategoryImageURL1': 'modules/categories/img/subcategory1/mobilehealthcare.png',
    'parent': db.categories.findOne({'name': 'FMCG', 'code': 'FMCGC', 'type': 'MainCategory'})._id
});

db.categories.insert({
    'name': 'Digestives',
    'code': 'DGSTV',
    'type': 'SubCategory2',
    'categoryImageURL1': 'modules/categories/img/subcategory2/digestive.png',
    'croppedCategoryImageURL1': 'modules/categories/img/subcategory2/mobiledigestive.png',
    'productCategory': true,
    'productAttributes': {
        brand: true,
        grade: {enabled: true},
        quality: {enabled: true},
        sampleNumber: true,
        fssaiLicenceNumber: true
    },
    'inventoryAttributes': {
        barcode: true,
        batchNumber: true,
        manufactureDate: true,
        packagingDate: true,
        bestBefore: true,
        unitMeasure: ['Kg', 'gram', 'litre', 'ml', 'count', 'Bag', 'Case', 'Pack']
    },
    'parent': db.categories.findOne({'name': 'Health Care', 'code': 'HCARE', 'type': 'SubCategory1'})._id
});

db.categories.update({
    'name': 'Health Care',
    'code': 'HCARE',
    'type': 'SubCategory1'
}, {
    $addToSet: {
        'children': db.categories.findOne({
            'name': 'Digestives',
            'code': 'DGSTV',
            'type': 'SubCategory2'
        })._id
    }
});

db.categories.insert({
    'name': 'Health Drinks',
    'code': 'HDRNK',
    'type': 'SubCategory2',
    'categoryImageURL1': 'modules/categories/img/subcategory2/healthdrinks.png',
    'croppedCategoryImageURL1': 'modules/categories/img/subcategory2/mobilehealthdrinks.png',
    'productCategory': true,
    'productAttributes': {
        brand: true,
        grade: {enabled: true},
        quality: {enabled: true},
        sampleNumber: true,
        fssaiLicenceNumber: true
    },
    'inventoryAttributes': {
        barcode: true,
        batchNumber: true,
        manufactureDate: true,
        packagingDate: true,
        bestBefore: true,
        unitMeasure: ['litre', 'ml', 'count', 'Bag', 'Case', 'Pack']
    },
    'parent': db.categories.findOne({'name': 'Health Care', 'code': 'HCARE', 'type': 'SubCategory1'})._id
});

db.categories.update({
    'name': 'Health Care',
    'code': 'HCARE',
    'type': 'SubCategory1'
}, {
    $addToSet: {
        'children': db.categories.findOne({
            'name': 'Health Drinks',
            'code': 'HDRNK',
            'type': 'SubCategory2'
        })._id
    }
});

db.categories.insert({
    'name': 'Honey',
    'code': 'HONEY',
    'type': 'SubCategory2',
    'categoryImageURL1': 'modules/categories/img/subcategory2/honey.png',
    'croppedCategoryImageURL1': 'modules/categories/img/subcategory2/mobilehoney.png',
    'productCategory': true,
    'productAttributes': {
        brand: true,
        grade: {enabled: true},
        quality: {enabled: true},
        sampleNumber: true,
        fssaiLicenceNumber: true
    },
    'inventoryAttributes': {
        barcode: true,
        batchNumber: true,
        manufactureDate: true,
        packagingDate: true,
        bestBefore: true,
        unitMeasure: ['Kg', 'gram', 'litre', 'ml', 'count', 'Bag', 'Case', 'Pack']
    },
    'parent': db.categories.findOne({'name': 'Health Care', 'code': 'HCARE', 'type': 'SubCategory1'})._id
});

db.categories.update({
    'name': 'Health Care',
    'code': 'HCARE',
    'type': 'SubCategory1'
}, {$addToSet: {'children': db.categories.findOne({'name': 'Honey', 'code': 'HONEY', 'type': 'SubCategory2'})._id}});

db.categories.insert({
    'name': 'Health and Wellness',
    'code': 'HWELL',
    'type': 'SubCategory2',
    'categoryImageURL1': 'modules/categories/img/subcategory2/healthandwellness.png',
    'croppedCategoryImageURL1': 'modules/categories/img/subcategory2/mobilehealthandwellness.png',
    'productCategory': true,
    'productAttributes': {
        brand: true,
        grade: {enabled: true},
        quality: {enabled: true},
        sampleNumber: true,
        fssaiLicenceNumber: true
    },
    'inventoryAttributes': {
        barcode: true,
        batchNumber: true,
        manufactureDate: true,
        packagingDate: true,
        bestBefore: true,
        unitMeasure: ['Kg', 'gram', 'litre', 'ml', 'count', 'Bag', 'Case', 'Pack']
    },
    'parent': db.categories.findOne({'name': 'Health Care', 'code': 'HCARE', 'type': 'SubCategory1'})._id
});

db.categories.update({
    'name': 'Health Care',
    'code': 'HCARE',
    'type': 'SubCategory1'
}, {
    $addToSet: {
        'children': db.categories.findOne({
            'name': 'Health and Wellness',
            'code': 'HWELL',
            'type': 'SubCategory2'
        })._id
    }
});

db.categories.insert({
    'name': 'Immune Enhancers',
    'code': 'IMMUN',
    'type': 'SubCategory2',
    'categoryImageURL1': 'modules/categories/img/subcategory2/immuneenhancer.png',
    'croppedCategoryImageURL1': 'modules/categories/img/subcategory2/croppedimmuneenhancer.png',
    'productCategory': true,
    'productAttributes': {
        brand: true,
        grade: {enabled: true},
        quality: {enabled: true},
        sampleNumber: true,
        fssaiLicenceNumber: true
    },
    'inventoryAttributes': {
        barcode: true,
        batchNumber: true,
        manufactureDate: true,
        packagingDate: true,
        bestBefore: true,
        unitMeasure: ['Kg', 'gram', 'litre', 'ml', 'count', 'Bag', 'Case', 'Pack']
    },
    'parent': db.categories.findOne({'name': 'Health Care', 'code': 'HCARE', 'type': 'SubCategory1'})._id
});

db.categories.update({
    'name': 'Health Care',
    'code': 'HCARE',
    'type': 'SubCategory1'
}, {
    $addToSet: {
        'children': db.categories.findOne({
            'name': 'Immune Enhancers',
            'code': 'IMMUN',
            'type': 'SubCategory2'
        })._id
    }
});

// END - Health Care

// Start - Food Products
db.categories.insert({
    'name': 'Food Products',
    'code': 'FOODP',
    'type': 'SubCategory1',
    'categoryImageURL1': 'modules/categories/img/subcategory1/foodproducts.png',
    'croppedCategoryImageURL1': 'modules/categories/img/subcategory1/mobilefoodproducts.png',
    'parent': db.categories.findOne({'name': 'FMCG', 'code': 'FMCGC', 'type': 'MainCategory'})._id
});


db.categories.insert({
    'name': 'Biscuits And Cookies',
    'code': 'BISCK',
    'type': 'SubCategory2',
    'categoryImageURL1': 'modules/categories/img/subcategory2/biscuitsandcookies.png',
    'croppedCategoryImageURL1': 'modules/categories/img/subcategory2/mobilebiscuitsandcookies.png',
    'productCategory': true,
    'productAttributes': {
        brand: true,
        grade: {enabled: true},
        quality: {enabled: true},
        sampleNumber: true,
        fssaiLicenceNumber: true
    },
    'inventoryAttributes': {
        barcode: true,
        batchNumber: true,
        manufactureDate: true,
        packagingDate: true,
        bestBefore: true,
        unitMeasure: ['Kg', 'gram', 'count', 'Bag', 'Case', 'Pack']
    },
    'parent': db.categories.findOne({'name': 'Food Products', 'code': 'FOODP', 'type': 'SubCategory1'})._id
});

db.categories.update({
    'name': 'Food Products',
    'code': 'FOODP',
    'type': 'SubCategory1'
}, {
    $addToSet: {
        'children': db.categories.findOne({
            'name': 'Biscuits And Cookies',
            'code': 'BISCK',
            'type': 'SubCategory2'
        })._id
    }
});


db.categories.insert({
    'name': 'Dairy',
    'code': 'DAIRY',
    'type': 'SubCategory2',
    'categoryImageURL1': 'modules/categories/img/subcategory2/dairy.png',
    'croppedCategoryImageURL1': 'modules/categories/img/subcategory2/mobiledairy.png',
    'productCategory': true,
    'productAttributes': {
        brand: true,
        grade: {enabled: true},
        quality: {enabled: true},
        sampleNumber: true,
        fssaiLicenceNumber: true
    },
    'inventoryAttributes': {
        barcode: true,
        batchNumber: true,
        manufactureDate: true,
        packagingDate: true,
        bestBefore: true,
        unitMeasure: ['Kg', 'gram', 'litre', 'ml', 'count', 'Bag', 'Case', 'Pack']
    },
    'parent': db.categories.findOne({'name': 'Food Products', 'code': 'FOODP', 'type': 'SubCategory1'})._id
});

db.categories.update({
    'name': 'Food Products',
    'code': 'FOODP',
    'type': 'SubCategory1'
}, {
    $addToSet: {
        'children': db.categories.findOne({
            'name': 'Dairy',
            'code': 'DAIRY',
            'type': 'SubCategory2'
        })._id
    }
});

db.categories.insert({
    'name': 'Masalas and Spices',
    'code': 'MSPIC',
    'type': 'SubCategory2',
    'categoryImageURL1': 'modules/categories/img/subcategory2/masalasandspices.png',
    'croppedCategoryImageURL1': 'modules/categories/img/subcategory2/mobilemasalasandspices.png',
    'productCategory': true,
    'productAttributes': {
        brand: true,
        grade: {enabled: true},
        quality: {enabled: true},
        sampleNumber: true,
        fssaiLicenceNumber: true
    },
    'inventoryAttributes': {
        barcode: true,
        batchNumber: true,
        manufactureDate: true,
        packagingDate: true,
        bestBefore: true,
        unitMeasure: ['Kg', 'gram', 'litre', 'ml', 'count', 'Bag', 'Case', 'Pack']
    },
    'parent': db.categories.findOne({'name': 'Food Products', 'code': 'FOODP', 'type': 'SubCategory1'})._id
});

db.categories.update({
    'name': 'Food Products',
    'code': 'FOODP',
    'type': 'SubCategory1'
}, {
    $addToSet: {
        'children': db.categories.findOne({
            'name': 'Masalas and Spices',
            'code': 'MSPIC',
            'type': 'SubCategory2'
        })._id
    }
});

db.categories.insert({
    'name': 'Wheat Atta',
    'code': 'WATTA',
    'type': 'SubCategory2',
    'categoryImageURL1': 'modules/categories/img/subcategory2/wheatatta.png',
    'croppedCategoryImageURL1': 'modules/categories/img/subcategory2/mobilewheatatta.png',
    'productCategory': true,
    'productAttributes': {
        brand: true,
        grade: {enabled: true},
        quality: {enabled: true},
        sampleNumber: true,
        fssaiLicenceNumber: true
    },
    'inventoryAttributes': {
        barcode: true,
        batchNumber: true,
        manufactureDate: true,
        packagingDate: true,
        bestBefore: true,
        unitMeasure: ['Kg', 'gram', 'count', 'Bag', 'Case', 'Pack']
    },
    'parent': db.categories.findOne({'name': 'Food Products', 'code': 'FOODP', 'type': 'SubCategory1'})._id
});

db.categories.update({
    'name': 'Food Products',
    'code': 'FOODP',
    'type': 'SubCategory1'
}, {
    $addToSet: {
        'children': db.categories.findOne({
            'name': 'Wheat Atta',
            'code': 'WATTA',
            'type': 'SubCategory2'
        })._id
    }
});

db.categories.insert({
    'name': 'Flours',
    'code': 'FLOUR',
    'type': 'SubCategory2',
    'categoryImageURL1': 'modules/categories/img/subcategory2/flours.png',
    'croppedCategoryImageURL1': 'modules/categories/img/subcategory2/mobileflours.png',
    'productCategory': true,
    'productAttributes': {
        brand: true,
        grade: {enabled: true},
        quality: {enabled: true},
        sampleNumber: true,
        fssaiLicenceNumber: true
    },
    'inventoryAttributes': {
        barcode: true,
        batchNumber: true,
        manufactureDate: true,
        packagingDate: true,
        bestBefore: true,
        unitMeasure: ['Kg', 'gram', 'count', 'Bag', 'Case', 'Pack']
    },
    'parent': db.categories.findOne({'name': 'Food Products', 'code': 'FOODP', 'type': 'SubCategory1'})._id
});

db.categories.update({
    'name': 'Food Products',
    'code': 'FOODP',
    'type': 'SubCategory1'
}, {$addToSet: {'children': db.categories.findOne({'name': 'Flours', 'code': 'FLOUR', 'type': 'SubCategory2'})._id}});


db.categories.insert({
    'name': 'Oils',
    'code': 'FOILS',
    'type': 'SubCategory2',
    'categoryImageURL1': 'modules/categories/img/subcategory2/oils.png',
    'croppedCategoryImageURL1': 'modules/categories/img/subcategory2/mobileoils.png',
    'productCategory': true,
    'productAttributes': {
        brand: true,
        grade: {enabled: true},
        quality: {enabled: true},
        sampleNumber: true,
        fssaiLicenceNumber: true
    },
    'inventoryAttributes': {
        barcode: true,
        batchNumber: true,
        manufactureDate: true,
        packagingDate: true,
        bestBefore: true,
        unitMeasure: ['Kg', 'gram', 'litre', 'ml', 'count', 'Bag', 'Case', 'Pack']
    },
    'parent': db.categories.findOne({'name': 'Food Products', 'code': 'FOODP', 'type': 'SubCategory1'})._id
});

db.categories.update({
    'name': 'Food Products',
    'code': 'FOODP',
    'type': 'SubCategory1'
}, {$addToSet: {'children': db.categories.findOne({'name': 'Oils', 'code': 'FOILS', 'type': 'SubCategory2'})._id}});

db.categories.insert({
    'name': 'Sooji and Dalia',
    'code': 'SOOJI',
    'type': 'SubCategory2',
    'categoryImageURL1': 'modules/categories/img/subcategory2/sooji.png',
    'croppedCategoryImageURL1': 'modules/categories/img/subcategory2/mobilesooji.png',
    'productCategory': true,
    'productAttributes': {
        brand: true,
        grade: {enabled: true},
        quality: {enabled: true},
        sampleNumber: true,
        fssaiLicenceNumber: true
    },
    'inventoryAttributes': {
        barcode: true,
        batchNumber: true,
        manufactureDate: true,
        packagingDate: true,
        bestBefore: true,
        unitMeasure: ['Kg', 'gram', 'count', 'Bag', 'Case', 'Pack']
    },
    'parent': db.categories.findOne({'name': 'Food Products', 'code': 'FOODP', 'type': 'SubCategory1'})._id
});

db.categories.update({
    'name': 'Food Products',
    'code': 'FOODP',
    'type': 'SubCategory1'
}, {
    $addToSet: {
        'children': db.categories.findOne({
            'name': 'Sooji and Dalia',
            'code': 'SOOJI',
            'type': 'SubCategory2'
        })._id
    }
});

db.categories.insert({
    'name': 'Corn Flakes',
    'code': 'CORNF',
    'type': 'SubCategory2',
    'categoryImageURL1': 'modules/categories/img/subcategory2/cornflakes.png',
    'croppedCategoryImageURL1': 'modules/categories/img/subcategory2/mobilecornflakes.png',
    'productCategory': true,
    'productAttributes': {
        brand: true,
        grade: {enabled: true},
        quality: {enabled: true},
        sampleNumber: true,
        fssaiLicenceNumber: true
    },
    'inventoryAttributes': {
        barcode: true,
        batchNumber: true,
        manufactureDate: true,
        packagingDate: true,
        bestBefore: true,
        unitMeasure: ['Kg', 'gram', 'count', 'Bag', 'Case', 'Pack']
    },
    'parent': db.categories.findOne({'name': 'Food Products', 'code': 'FOODP', 'type': 'SubCategory1'})._id
});

db.categories.update({
    'name': 'Food Products',
    'code': 'FOODP',
    'type': 'SubCategory1'
}, {
    $addToSet: {
        'children': db.categories.findOne({
            'name': 'Corn Flakes',
            'code': 'CORNF',
            'type': 'SubCategory2'
        })._id
    }
});

db.categories.insert({
    'name': 'Noodles',
    'code': 'NOODL',
    'type': 'SubCategory2',
    'categoryImageURL1': 'modules/categories/img/subcategory2/noodles.png',
    'croppedCategoryImageURL1': 'modules/categories/img/subcategory2/mobilenoodles.png',
    'productCategory': true,
    'productAttributes': {
        brand: true,
        grade: {enabled: true},
        quality: {enabled: true},
        sampleNumber: true,
        fssaiLicenceNumber: true
    },
    'inventoryAttributes': {
        barcode: true,
        batchNumber: true,
        manufactureDate: true,
        packagingDate: true,
        bestBefore: true,
        unitMeasure: ['Kg', 'gram', 'count', 'Bag', 'Case', 'Pack']
    },
    'parent': db.categories.findOne({'name': 'Food Products', 'code': 'FOODP', 'type': 'SubCategory1'})._id
});

db.categories.update({
    'name': 'Food Products',
    'code': 'FOODP',
    'type': 'SubCategory1'
}, {$addToSet: {'children': db.categories.findOne({'name': 'Noodles', 'code': 'NOODL', 'type': 'SubCategory2'})._id}});

db.categories.insert({
    'name': 'Candies and Chocolates',
    'code': 'CANCH',
    'type': 'SubCategory2',
    'categoryImageURL1': 'modules/categories/img/subcategory2/candiesandchacolates.png',
    'croppedCategoryImageURL1': 'modules/categories/img/subcategory2/mobilecandiesandchacolates.png',
    'productCategory': true,
    'productAttributes': {
        brand: true,
        grade: {enabled: true},
        quality: {enabled: true},
        sampleNumber: true,
        fssaiLicenceNumber: true
    },
    'inventoryAttributes': {
        barcode: true,
        batchNumber: true,
        manufactureDate: true,
        packagingDate: true,
        bestBefore: true,
        unitMeasure: ['Kg', 'gram', 'count', 'Bag', 'Case', 'Pack']
    },
    'parent': db.categories.findOne({'name': 'Food Products', 'code': 'FOODP', 'type': 'SubCategory1'})._id
});

db.categories.update({
    'name': 'Food Products',
    'code': 'FOODP',
    'type': 'SubCategory1'
}, {
    $addToSet: {
        'children': db.categories.findOne({
            'name': 'Candies and Chocolates',
            'code': 'CANCH',
            'type': 'SubCategory2'
        })._id
    }
});

db.categories.insert({
    'name': 'Sugar',
    'code': 'SUGAR',
    'type': 'SubCategory2',
    'categoryImageURL1': 'modules/categories/img/subcategory2/sugar.png',
    'croppedCategoryImageURL1': 'modules/categories/img/subcategory2/mobilesugar.png',
    'productCategory': true,
    'productAttributes': {
        brand: true,
        grade: {enabled: true},
        quality: {enabled: true},
        sampleNumber: true,
        fssaiLicenceNumber: true
    },
    'inventoryAttributes': {
        barcode: true,
        batchNumber: true,
        manufactureDate: true,
        packagingDate: true,
        bestBefore: true,
        unitMeasure: ['Kg', 'gram', 'count', 'Bag', 'Case', 'Pack']
    },
    'parent': db.categories.findOne({'name': 'Food Products', 'code': 'FOODP', 'type': 'SubCategory1'})._id
});

db.categories.update({
    'name': 'Food Products',
    'code': 'FOODP',
    'type': 'SubCategory1'
}, {$addToSet: {'children': db.categories.findOne({'name': 'Sugar', 'code': 'SUGAR', 'type': 'SubCategory2'})._id}});

db.categories.insert({
    'name': 'Maida',
    'code': 'MAIDA',
    'type': 'SubCategory2',
    'categoryImageURL1': 'modules/categories/img/subcategory2/maida.png',
    'croppedCategoryImageURL1': 'modules/categories/img/subcategory2/mobilemaida.png',
    'productCategory': true,
    'productAttributes': {
        brand: true,
        grade: {enabled: true},
        quality: {enabled: true},
        sampleNumber: true,
        fssaiLicenceNumber: true
    },
    'inventoryAttributes': {
        barcode: true,
        batchNumber: true,
        manufactureDate: true,
        packagingDate: true,
        bestBefore: true,
        unitMeasure: ['Kg', 'gram', 'count', 'Bag', 'Case', 'Pack']
    },
    'parent': db.categories.findOne({'name': 'Food Products', 'code': 'FOODP', 'type': 'SubCategory1'})._id
});

db.categories.update({
    'name': 'Food Products',
    'code': 'FOODP',
    'type': 'SubCategory1'
}, {$addToSet: {'children': db.categories.findOne({'name': 'Maida', 'code': 'MAIDA', 'type': 'SubCategory2'})._id}});

db.categories.insert({
    'name': 'Jaggery',
    'code': 'JAGRY',
    'type': 'SubCategory2',
    'categoryImageURL1': 'modules/categories/img/subcategory2/jaggerypowder.png',
    'croppedCategoryImageURL1': 'modules/categories/img/subcategory2/mobilejaggerypowder.png',
    'productCategory': true,
    'productAttributes': {
        brand: true,
        grade: {enabled: true},
        quality: {enabled: true},
        sampleNumber: true,
        fssaiLicenceNumber: true
    },
    'inventoryAttributes': {
        barcode: true,
        batchNumber: true,
        manufactureDate: true,
        packagingDate: true,
        bestBefore: true,
        unitMeasure: ['Kg', 'gram', 'count', 'Bag', 'Case', 'Pack']
    },
    'parent': db.categories.findOne({'name': 'Food Products', 'code': 'FOODP', 'type': 'SubCategory1'})._id
});

db.categories.update({
    'name': 'Food Products',
    'code': 'FOODP',
    'type': 'SubCategory1'
}, {$addToSet: {'children': db.categories.findOne({'name': 'Jaggery', 'code': 'JAGRY', 'type': 'SubCategory2'})._id}});

db.categories.insert({
    'name': 'Ice Creams',
    'code': 'ICCRM',
    'type': 'SubCategory2',
    'categoryImageURL1': 'modules/categories/img/subcategory2/icecreams.png',
    'croppedCategoryImageURL1': 'modules/categories/img/subcategory2/mobileicecreams.png',
    'productCategory': true,
    'productAttributes': {
        brand: true,
        grade: {enabled: true},
        quality: {enabled: true},
        sampleNumber: true,
        fssaiLicenceNumber: true
    },
    'inventoryAttributes': {
        barcode: true,
        batchNumber: true,
        manufactureDate: true,
        packagingDate: true,
        bestBefore: true,
        unitMeasure: ['Kg', 'gram', 'litre', 'ml', 'count', 'Bag', 'Case', 'Pack']
    },
    'parent': db.categories.findOne({'name': 'Food Products', 'code': 'FOODP', 'type': 'SubCategory1'})._id
});

db.categories.update({
    'name': 'Food Products',
    'code': 'FOODP',
    'type': 'SubCategory1'
}, {
    $addToSet: {
        'children': db.categories.findOne({
            'name': 'Ice Creams',
            'code': 'ICCRM',
            'type': 'SubCategory2'
        })._id
    }
});

db.categories.insert({
    'name': 'Sweets',
    'code': 'SWEET',
    'type': 'SubCategory2',
    'categoryImageURL1': 'modules/categories/img/subcategory2/sweets.png',
    'croppedCategoryImageURL1': 'modules/categories/img/subcategory2/mobilesweets.png',
    'productCategory': true,
    'productAttributes': {
        brand: true,
        grade: {enabled: true},
        quality: {enabled: true},
        sampleNumber: true,
        fssaiLicenceNumber: true
    },
    'inventoryAttributes': {
        barcode: true,
        batchNumber: true,
        manufactureDate: true,
        packagingDate: true,
        bestBefore: true,
        unitMeasure: ['Kg', 'gram', 'count', 'Bag', 'Case', 'Pack']
    },
    'parent': db.categories.findOne({'name': 'Food Products', 'code': 'FOODP', 'type': 'SubCategory1'})._id
});

db.categories.update({
    'name': 'Food Products',
    'code': 'FOODP',
    'type': 'SubCategory1'
}, {$addToSet: {'children': db.categories.findOne({'name': 'Sweets', 'code': 'SWEET', 'type': 'SubCategory2'})._id}});

db.categories.insert({
    'name': 'Oats',
    'code': 'OATSS',
    'type': 'SubCategory2',
    'categoryImageURL1': 'modules/categories/img/subcategory2/oats.png',
    'croppedCategoryImageURL1': 'modules/categories/img/mobilesubcategory2/oats.png',
    'productCategory': true,
    'productAttributes': {
        brand: true,
        grade: {enabled: true},
        quality: {enabled: true},
        sampleNumber: true,
        fssaiLicenceNumber: true
    },
    'inventoryAttributes': {
        barcode: true,
        batchNumber: true,
        manufactureDate: true,
        packagingDate: true,
        bestBefore: true,
        unitMeasure: ['Kg', 'gram', 'count', 'Bag', 'Case', 'Pack']
    },
    'parent': db.categories.findOne({'name': 'Food Products', 'code': 'FOODP', 'type': 'SubCategory1'})._id
});

db.categories.update({
    'name': 'Food Products',
    'code': 'FOODP',
    'type': 'SubCategory1'
}, {$addToSet: {'children': db.categories.findOne({'name': 'Oats', 'code': 'OATSS', 'type': 'SubCategory2'})._id}});

db.categories.insert({
    'name': 'Namkeen',
    'code': 'NAMKN',
    'type': 'SubCategory2',
    'categoryImageURL1': 'modules/categories/img/subcategory2/namkeen.png',
    'croppedCategoryImageURL1': 'modules/categories/img/subcategory2/mobilenamkeen.png',
    'productCategory': true,
    'productAttributes': {
        brand: true,
        grade: {enabled: true},
        quality: {enabled: true},
        sampleNumber: true,
        fssaiLicenceNumber: true
    },
    'inventoryAttributes': {
        barcode: true,
        batchNumber: true,
        manufactureDate: true,
        packagingDate: true,
        bestBefore: true,
        unitMeasure: ['Kg', 'gram', 'count', 'Bag', 'Case', 'Pack']
    },
    'parent': db.categories.findOne({'name': 'Food Products', 'code': 'FOODP', 'type': 'SubCategory1'})._id
});

db.categories.update({
    'name': 'Food Products',
    'code': 'FOODP',
    'type': 'SubCategory1'
}, {$addToSet: {'children': db.categories.findOne({'name': 'Namkeen', 'code': 'NAMKN', 'type': 'SubCategory2'})._id}});

db.categories.insert({
    'name': 'Papads',
    'code': 'PAPAD',
    'type': 'SubCategory2',
    'categoryImageURL1': 'modules/categories/img/subcategory2/papads.png',
    'croppedCategoryImageURL1': 'modules/categories/img/subcategory2/mobilepapads.png',
    'productCategory': true,
    'productAttributes': {
        brand: true,
        grade: {enabled: true},
        quality: {enabled: true},
        sampleNumber: true,
        fssaiLicenceNumber: true
    },
    'inventoryAttributes': {
        barcode: true,
        batchNumber: true,
        manufactureDate: true,
        packagingDate: true,
        bestBefore: true,
        unitMeasure: ['Kg', 'gram', 'count', 'Bag', 'Case', 'Pack']
    },
    'parent': db.categories.findOne({'name': 'Food Products', 'code': 'FOODP', 'type': 'SubCategory1'})._id
});

db.categories.update({
    'name': 'Food Products',
    'code': 'FOODP',
    'type': 'SubCategory1'
}, {$addToSet: {'children': db.categories.findOne({'name': 'Papads', 'code': 'PAPAD', 'type': 'SubCategory2'})._id}});

db.categories.insert({
    'name': 'Pickles',
    'code': 'PICKL',
    'type': 'SubCategory2',
    'categoryImageURL1': 'modules/categories/img/subcategory2/pickles.png',
    'croppedCategoryImageURL1': 'modules/categories/img/subcategory2/mobilepickles.png',
    'productCategory': true,
    'productAttributes': {
        brand: true,
        grade: {enabled: true},
        quality: {enabled: true},
        sampleNumber: true,
        fssaiLicenceNumber: true
    },
    'inventoryAttributes': {
        barcode: true,
        batchNumber: true,
        manufactureDate: true,
        packagingDate: true,
        bestBefore: true,
        unitMeasure: ['Kg', 'gram', 'count', 'Bag', 'Case', 'Pack']
    },
    'parent': db.categories.findOne({'name': 'Food Products', 'code': 'FOODP', 'type': 'SubCategory1'})._id
});

db.categories.update({
    'name': 'Food Products',
    'code': 'FOODP',
    'type': 'SubCategory1'
}, {$addToSet: {'children': db.categories.findOne({'name': 'Pickles', 'code': 'PICKL', 'type': 'SubCategory2'})._id}});

db.categories.insert({
    'name': 'Jams',
    'code': 'JAMSS',
    'type': 'SubCategory2',
    'categoryImageURL1': 'modules/categories/img/subcategory2/jams.png',
    'croppedCategoryImageURL1': 'modules/categories/img/subcategory2/mobilejams.png',
    'productCategory': true,
    'productAttributes': {
        brand: true,
        grade: {enabled: true},
        quality: {enabled: true},
        sampleNumber: true,
        fssaiLicenceNumber: true
    },
    'inventoryAttributes': {
        barcode: true,
        batchNumber: true,
        manufactureDate: true,
        packagingDate: true,
        bestBefore: true,
        unitMeasure: ['Kg', 'gram', 'count', 'Bag', 'Case', 'Pack']
    },
    'parent': db.categories.findOne({'name': 'Food Products', 'code': 'FOODP', 'type': 'SubCategory1'})._id
});

db.categories.update({
    'name': 'Food Products',
    'code': 'FOODP',
    'type': 'SubCategory1'
}, {$addToSet: {'children': db.categories.findOne({'name': 'Jams', 'code': 'JAMSS', 'type': 'SubCategory2'})._id}});

db.categories.insert({
    'name': 'Sauces',
    'code': 'SAUCE',
    'type': 'SubCategory2',
    'categoryImageURL1': 'modules/categories/img/subcategory2/sauces.png',
    'croppedCategoryImageURL1': 'modules/categories/img/subcategory2/mobilesauces.png',
    'productCategory': true,
    'productAttributes': {
        brand: true,
        grade: {enabled: true},
        quality: {enabled: true},
        sampleNumber: true,
        fssaiLicenceNumber: true
    },
    'inventoryAttributes': {
        barcode: true,
        batchNumber: true,
        manufactureDate: true,
        packagingDate: true,
        bestBefore: true,
        unitMeasure: ['Kg', 'gram', 'count', 'Bag', 'Case', 'Pack']
    },
    'parent': db.categories.findOne({'name': 'Food Products', 'code': 'FOODP', 'type': 'SubCategory1'})._id
});

db.categories.update({
    'name': 'Food Products',
    'code': 'FOODP',
    'type': 'SubCategory1'
}, {$addToSet: {'children': db.categories.findOne({'name': 'Sauces', 'code': 'SAUCE', 'type': 'SubCategory2'})._id}});

db.categories.insert({
    'name': 'Cashew',
    'code': 'CASHW',
    'type': 'SubCategory2',
    'categoryImageURL1': 'modules/categories/img/subcategory2/cashew.png',
    'croppedCategoryImageURL1': 'modules/categories/img/subcategory2/mobilecashew.png',
    'productCategory': true,
    'productAttributes': {
        brand: true,
        grade: {enabled: true},
        quality: {enabled: true},
        sampleNumber: true,
        fssaiLicenceNumber: true
    },
    'inventoryAttributes': {
        barcode: true,
        batchNumber: true,
        manufactureDate: true,
        packagingDate: true,
        bestBefore: true,
        unitMeasure: ['Kg', 'gram', 'count', 'Bag', 'Case', 'Pack']
    },
    'parent': db.categories.findOne({'name': 'Food Products', 'code': 'FOODP', 'type': 'SubCategory1'})._id
});

db.categories.update({
    'name': 'Food Products',
    'code': 'FOODP',
    'type': 'SubCategory1'
}, {$addToSet: {'children': db.categories.findOne({'name': 'Cashew', 'code': 'CASHW', 'type': 'SubCategory2'})._id}});

db.categories.insert({
    'name': 'Dry Grapes (Raisins)',
    'code': 'DGRPS',
    'type': 'SubCategory2',
    'categoryImageURL1': 'modules/categories/img/subcategory2/drygrapes.png',
    'croppedCategoryImageURL1': 'modules/categories/img/subcategory2/mobiledrygrapes.png',
    'productCategory': true,
    'productAttributes': {
        brand: true,
        grade: {enabled: true},
        quality: {enabled: true},
        sampleNumber: true,
        fssaiLicenceNumber: true
    },
    'inventoryAttributes': {
        barcode: true,
        batchNumber: true,
        manufactureDate: true,
        packagingDate: true,
        bestBefore: true,
        unitMeasure: ['Kg', 'gram', 'count', 'Bag', 'Case', 'Pack']
    },
    'parent': db.categories.findOne({'name': 'Food Products', 'code': 'FOODP', 'type': 'SubCategory1'})._id
});

db.categories.update({
    'name': 'Food Products',
    'code': 'FOODP',
    'type': 'SubCategory1'
}, {
    $addToSet: {
        'children': db.categories.findOne({
            'name': 'Dry Grapes (Raisins)',
            'code': 'DGRPS',
            'type': 'SubCategory2'
        })._id
    }
});

db.categories.insert({
    'name': 'Almonds (Badam)',
    'code': 'ALMND',
    'type': 'SubCategory2',
    'categoryImageURL1': 'modules/categories/img/subcategory2/almonds.png',
    'croppedCategoryImageURL1': 'modules/categories/img/subcategory2/mobilealmonds.png',
    'productCategory': true,
    'productAttributes': {
        brand: true,
        grade: {enabled: true},
        quality: {enabled: true},
        sampleNumber: true,
        fssaiLicenceNumber: true
    },
    'inventoryAttributes': {
        barcode: true,
        batchNumber: true,
        manufactureDate: true,
        packagingDate: true,
        bestBefore: true,
        unitMeasure: ['Kg', 'gram', 'count', 'Bag', 'Case', 'Pack']
    },
    'parent': db.categories.findOne({'name': 'Food Products', 'code': 'FOODP', 'type': 'SubCategory1'})._id
});

db.categories.update({
    'name': 'Food Products',
    'code': 'FOODP',
    'type': 'SubCategory1'
}, {
    $addToSet: {
        'children': db.categories.findOne({
            'name': 'Almonds (Badam)',
            'code': 'ALMND',
            'type': 'SubCategory2'
        })._id
    }
});

db.categories.insert({
    'name': 'Dates',
    'code': 'DATES',
    'type': 'SubCategory2',
    'categoryImageURL1': 'modules/categories/img/subcategory2/dates.png',
    'croppedCategoryImageURL1': 'modules/categories/img/subcategory2/mobiledates.png',
    'productCategory': true,
    'productAttributes': {
        brand: true,
        grade: {enabled: true},
        quality: {enabled: true},
        sampleNumber: true,
        fssaiLicenceNumber: true
    },
    'inventoryAttributes': {
        barcode: true,
        batchNumber: true,
        manufactureDate: true,
        packagingDate: true,
        bestBefore: true,
        unitMeasure: ['Kg', 'gram', 'count', 'Bag', 'Case', 'Pack']
    },
    'parent': db.categories.findOne({'name': 'Food Products', 'code': 'FOODP', 'type': 'SubCategory1'})._id
});

db.categories.update({
    'name': 'Food Products',
    'code': 'FOODP',
    'type': 'SubCategory1'
}, {$addToSet: {'children': db.categories.findOne({'name': 'Dates', 'code': 'DATES', 'type': 'SubCategory2'})._id}});

db.categories.insert({
    'name': 'Apricot',
    'code': 'APRCT',
    'type': 'SubCategory2',
    'categoryImageURL1': 'modules/categories/img/subcategory2/apricot.png',
    'croppedCategoryImageURL1': 'modules/categories/img/subcategory2/mobileapricot.png',
    'productCategory': true,
    'productAttributes': {
        brand: true,
        grade: {enabled: true},
        quality: {enabled: true},
        sampleNumber: true,
        fssaiLicenceNumber: true
    },
    'inventoryAttributes': {
        barcode: true,
        batchNumber: true,
        manufactureDate: true,
        packagingDate: true,
        bestBefore: true,
        unitMeasure: ['Kg', 'gram', 'count', 'Bag', 'Case', 'Pack']
    },
    'parent': db.categories.findOne({'name': 'Food Products', 'code': 'FOODP', 'type': 'SubCategory1'})._id
});

db.categories.update({
    'name': 'Food Products',
    'code': 'FOODP',
    'type': 'SubCategory1'
}, {$addToSet: {'children': db.categories.findOne({'name': 'Apricot', 'code': 'APRCT', 'type': 'SubCategory2'})._id}});

db.categories.insert({
    'name': 'Anjeer (Fig)',
    'code': 'ANJER',
    'type': 'SubCategory2',
    'categoryImageURL1': 'modules/categories/img/subcategory2/anjeer.png',
    'croppedCategoryImageURL1': 'modules/categories/img/subcategory2/mobileanjeer.png',
    'productCategory': true,
    'productAttributes': {
        brand: true,
        grade: {enabled: true},
        quality: {enabled: true},
        sampleNumber: true,
        fssaiLicenceNumber: true
    },
    'inventoryAttributes': {
        barcode: true,
        batchNumber: true,
        manufactureDate: true,
        packagingDate: true,
        bestBefore: true,
        unitMeasure: ['Kg', 'gram', 'count', 'Bag', 'Case', 'Pack']
    },
    'parent': db.categories.findOne({'name': 'Food Products', 'code': 'FOODP', 'type': 'SubCategory1'})._id
});

db.categories.update({
    'name': 'Food Products',
    'code': 'FOODP',
    'type': 'SubCategory1'
}, {
    $addToSet: {
        'children': db.categories.findOne({
            'name': 'Anjeer (Fig)',
            'code': 'ANJER',
            'type': 'SubCategory2'
        })._id
    }
});

db.categories.insert({
    'name': 'Pistachios',
    'code': 'PSTCH',
    'type': 'SubCategory2',
    'categoryImageURL1': 'modules/categories/img/subcategory2/pistachios.png',
    'croppedCategoryImageURL1': 'modules/categories/img/subcategory2/mobilepistachios.png',
    'productCategory': true,
    'productAttributes': {
        brand: true,
        grade: {enabled: true},
        quality: {enabled: true},
        sampleNumber: true,
        fssaiLicenceNumber: true
    },
    'inventoryAttributes': {
        barcode: true,
        batchNumber: true,
        manufactureDate: true,
        packagingDate: true,
        bestBefore: true,
        unitMeasure: ['Kg', 'gram', 'count', 'Bag', 'Case', 'Pack']
    },
    'parent': db.categories.findOne({'name': 'Food Products', 'code': 'FOODP', 'type': 'SubCategory1'})._id
});

db.categories.update({
    'name': 'Food Products',
    'code': 'FOODP',
    'type': 'SubCategory1'
}, {
    $addToSet: {
        'children': db.categories.findOne({
            'name': 'Pistachios',
            'code': 'PSTCH',
            'type': 'SubCategory2'
        })._id
    }
});


db.categories.insert({
    'name': 'Walnuts',
    'code': 'WLNTS',
    'type': 'SubCategory2',
    'categoryImageURL1': 'modules/categories/img/subcategory2/walnuts.png',
    'croppedCategoryImageURL1': 'modules/categories/img/subcategory2/mobilewalnuts.png',
    'productCategory': true,
    'productAttributes': {
        brand: true,
        grade: {enabled: true},
        quality: {enabled: true},
        sampleNumber: true,
        fssaiLicenceNumber: true
    },
    'inventoryAttributes': {
        barcode: true,
        batchNumber: true,
        manufactureDate: true,
        packagingDate: true,
        bestBefore: true,
        unitMeasure: ['Kg', 'gram', 'count', 'Bag', 'Case', 'Pack']
    },
    'parent': db.categories.findOne({'name': 'Food Products', 'code': 'FOODP', 'type': 'SubCategory1'})._id
});

db.categories.update({
    'name': 'Food Products',
    'code': 'FOODP',
    'type': 'SubCategory1'
}, {$addToSet: {'children': db.categories.findOne({'name': 'Walnuts', 'code': 'WLNTS', 'type': 'SubCategory2'})._id}});

db.categories.insert({
    'name': 'Prunes',
    'code': 'PRUNS',
    'type': 'SubCategory2',
    'categoryImageURL1': 'modules/categories/img/subcategory2/prunes.png',
    'croppedCategoryImageURL1': 'modules/categories/img/subcategory2/mobileprunes.png',
    'productCategory': true,
    'productAttributes': {
        brand: true,
        grade: {enabled: true},
        quality: {enabled: true},
        sampleNumber: true,
        fssaiLicenceNumber: true
    },
    'inventoryAttributes': {
        barcode: true,
        batchNumber: true,
        manufactureDate: true,
        packagingDate: true,
        bestBefore: true,
        unitMeasure: ['Kg', 'gram', 'count', 'Bag', 'Case', 'Pack']
    },
    'parent': db.categories.findOne({'name': 'Food Products', 'code': 'FOODP', 'type': 'SubCategory1'})._id
});

db.categories.update({
    'name': 'Food Products',
    'code': 'FOODP',
    'type': 'SubCategory1'
}, {$addToSet: {'children': db.categories.findOne({'name': 'Prunes', 'code': 'PRUNS', 'type': 'SubCategory2'})._id}});

db.categories.insert({
    'name': 'Other Dry Fruits',
    'code': 'ODFRT',
    'type': 'SubCategory2',
    'categoryImageURL1': 'modules/categories/img/subcategory2/otherdryfruits.png',
    'croppedCategoryImageURL1': 'modules/categories/img/subcategory2/mobileotherdryfruits.png',
    'productCategory': true,
    'productAttributes': {
        brand: true,
        grade: {enabled: true},
        quality: {enabled: true},
        sampleNumber: true,
        fssaiLicenceNumber: true
    },
    'inventoryAttributes': {
        barcode: true,
        batchNumber: true,
        manufactureDate: true,
        packagingDate: true,
        bestBefore: true,
        unitMeasure: ['Kg', 'gram', 'count', 'Bag', 'Case', 'Pack']
    },
    'parent': db.categories.findOne({'name': 'Food Products', 'code': 'FOODP', 'type': 'SubCategory1'})._id
});

db.categories.update({
    'name': 'Food Products',
    'code': 'FOODP',
    'type': 'SubCategory1'
}, {
    $addToSet: {
        'children': db.categories.findOne({
            'name': 'Other Dry Fruits',
            'code': 'ODFRT',
            'type': 'SubCategory2'
        })._id
    }
});
// END - Food Products

// Start - Beverages

db.categories.insert({
    'name': 'Beverages',
    'code': 'BVRGS',
    'type': 'SubCategory1',
    'categoryImageURL1': 'modules/categories/img/subcategory1/beverages.png',
    'croppedCategoryImageURL1': 'modules/categories/img/subcategory1/mobilebeverages.png',
    'parent': db.categories.findOne({'name': 'FMCG', 'code': 'FMCGC', 'type': 'MainCategory'})._id
});

db.categories.insert({
    'name': 'Tea',
    'code': 'TEAPD',
    'type': 'SubCategory2',
    'categoryImageURL1': 'modules/categories/img/subcategory2/teaandcoffee.png',
    'croppedCategoryImageURL1': 'modules/categories/img/subcategory2/mobileteaandcoffee.png',
    'productCategory': true,
    'productAttributes': {
        brand: true,
        grade: {enabled: true},
        quality: {enabled: true},
        sampleNumber: true,
        fssaiLicenceNumber: true
    },
    'inventoryAttributes': {
        barcode: true,
        batchNumber: true,
        manufactureDate: true,
        packagingDate: true,
        bestBefore: true,
        unitMeasure: ['Kg', 'gram', 'count', 'Bag', 'Case', 'Pack']
    },
    'parent': db.categories.findOne({'name': 'Beverages', 'code': 'BVRGS', 'type': 'SubCategory1'})._id
});

db.categories.update({
    'name': 'Beverages',
    'code': 'BVRGS',
    'type': 'SubCategory1'
}, {
    $addToSet: {
        'children': db.categories.findOne({
            'name': 'Tea',
            'code': 'TEAPD',
            'type': 'SubCategory2'
        })._id
    }
});

db.categories.insert({
    'name': 'Juices',
    'code': 'JUICE',
    'type': 'SubCategory2',
    'categoryImageURL1': 'modules/categories/img/subcategory2/juices.png',
    'croppedCategoryImageURL1': 'modules/categories/img/subcategory2/mobilejuices.png',
    'productCategory': true,
    'productAttributes': {
        brand: true,
        grade: {enabled: true},
        quality: {enabled: true},
        sampleNumber: true,
        fssaiLicenceNumber: true
    },
    'inventoryAttributes': {
        barcode: true,
        batchNumber: true,
        manufactureDate: true,
        packagingDate: true,
        bestBefore: true,
        unitMeasure: ['Kg', 'gram', 'litre', 'ml', 'count', 'Bag', 'Case', 'Pack']
    },
    'parent': db.categories.findOne({'name': 'Beverages', 'code': 'BVRGS', 'type': 'SubCategory1'})._id
});

db.categories.update({
    'name': 'Beverages',
    'code': 'BVRGS',
    'type': 'SubCategory1'
}, {$addToSet: {'children': db.categories.findOne({'name': 'Juices', 'code': 'JUICE', 'type': 'SubCategory2'})._id}});

db.categories.insert({
    'name': 'Softdrinks',
    'code': 'SOFTD',
    'type': 'SubCategory2',
    'categoryImageURL1': 'modules/categories/img/subcategory2/softdrinks.png',
    'croppedCategoryImageURL1': 'modules/categories/img/subcategory2/mobilesoftdrinks.png',
    'productCategory': true,
    'productAttributes': {
        brand: true,
        grade: {enabled: true},
        quality: {enabled: true},
        sampleNumber: true,
        fssaiLicenceNumber: true
    },
    'inventoryAttributes': {
        barcode: true,
        batchNumber: true,
        manufactureDate: true,
        packagingDate: true,
        bestBefore: true,
        unitMeasure: ['litre', 'ml', 'count', 'Bag', 'Case', 'Pack']
    },
    'parent': db.categories.findOne({'name': 'Beverages', 'code': 'BVRGS', 'type': 'SubCategory1'})._id
});

db.categories.update({
    'name': 'Beverages',
    'code': 'BVRGS',
    'type': 'SubCategory1'
}, {
    $addToSet: {
        'children': db.categories.findOne({
            'name': 'Softdrinks',
            'code': 'SOFTD',
            'type': 'SubCategory2'
        })._id
    }
});

// END - Beverages
// Start - Home Care

db.categories.insert({
    'name': 'Home Care',
    'code': 'HOMEC',
    'type': 'SubCategory1',
    'categoryImageURL1': 'modules/categories/img/subcategory1/homecare.png',
    'croppedCategoryImageURL1': 'modules/categories/img/subcategory1/mobilehomecare.png',
    'parent': db.categories.findOne({'name': 'FMCG', 'code': 'FMCGC', 'type': 'MainCategory'})._id
});

db.categories.insert({
    'name': 'Laundry',
    'code': 'LNDRY',
    'type': 'SubCategory2',
    'categoryImageURL1': 'modules/categories/img/subcategory2/laundry.png',
    'croppedCategoryImageURL1': 'modules/categories/img/subcategory2/mobilelaundry.png',
    'productCategory': true,
    'productAttributes': {
        brand: true,
        grade: {enabled: true},
        quality: {enabled: true},
        sampleNumber: true,
        fssaiLicenceNumber: true
    },
    'inventoryAttributes': {
        barcode: true,
        batchNumber: true,
        manufactureDate: true,
        packagingDate: true,
        bestBefore: true,
        unitMeasure: ['Kg', 'gram', 'litre', 'ml', 'count', 'Bag', 'Case', 'Pack']
    },
    'parent': db.categories.findOne({'name': 'Home Care', 'code': 'HOMEC', 'type': 'SubCategory1'})._id
});

db.categories.update({
    'name': 'Home Care',
    'code': 'HOMEC',
    'type': 'SubCategory1'
}, {$addToSet: {'children': db.categories.findOne({'name': 'Laundry', 'code': 'LNDRY', 'type': 'SubCategory2'})._id}});


db.categories.insert({
    'name': 'Kitchen and Dining',
    'code': 'KTDNG',
    'type': 'SubCategory2',
    'categoryImageURL1': 'modules/categories/img/subcategory2/kitchenanddining.png',
    'croppedCategoryImageURL1': 'modules/categories/img/subcategory2/mobilekitchenanddining.png',
    'productCategory': true,
    'productAttributes': {
        brand: true,
        grade: {enabled: true},
        quality: {enabled: true},
        sampleNumber: true,
        fssaiLicenceNumber: true
    },
    'inventoryAttributes': {
        barcode: true,
        batchNumber: true,
        manufactureDate: true,
        packagingDate: true,
        bestBefore: true,
        unitMeasure: ['Kg', 'gram', 'litre', 'ml', 'count', 'Bag', 'Case', 'Pack']
    },
    'parent': db.categories.findOne({'name': 'Home Care', 'code': 'HOMEC', 'type': 'SubCategory1'})._id
});

db.categories.update({
    'name': 'Home Care',
    'code': 'HOMEC',
    'type': 'SubCategory1'
}, {
    $addToSet: {
        'children': db.categories.findOne({
            'name': 'Kitchen and Dining',
            'code': 'KTDNG',
            'type': 'SubCategory2'
        })._id
    }
});

db.categories.insert({
    'name': 'Repellents',
    'code': 'RPLNS',
    'type': 'SubCategory2',
    'categoryImageURL1': 'modules/categories/img/subcategory2/repellents.png',
    'croppedCategoryImageURL1': 'modules/categories/img/subcategory2/mobilerepellents.png',
    'productCategory': true,
    'productAttributes': {
        brand: true,
        grade: {enabled: true},
        quality: {enabled: true},
        sampleNumber: true,
        fssaiLicenceNumber: true
    },
    'inventoryAttributes': {
        barcode: true,
        batchNumber: true,
        manufactureDate: true,
        packagingDate: true,
        bestBefore: true,
        unitMeasure: ['Kg', 'gram', 'litre', 'ml', 'count', 'Bag', 'Case', 'Pack']
    },
    'parent': db.categories.findOne({'name': 'Home Care', 'code': 'HOMEC', 'type': 'SubCategory1'})._id
});

db.categories.update({
    'name': 'Home Care',
    'code': 'HOMEC',
    'type': 'SubCategory1'
}, {
    $addToSet: {
        'children': db.categories.findOne({
            'name': 'Repellents',
            'code': 'RPLNS',
            'type': 'SubCategory2'
        })._id
    }
});


db.categories.insert({
    'name': 'Toilets and Surface Cleaners',
    'code': 'TOASC',
    'type': 'SubCategory2',
    'categoryImageURL1': 'modules/categories/img/subcategory2/toiletsandsurfacecleaners.png',
    'croppedCategoryImageURL1': 'modules/categories/img/subcategory2/mobiletoiletsandsurfacecleaners.png',
    'productCategory': true,
    'productAttributes': {
        brand: true,
        grade: {enabled: true},
        quality: {enabled: true},
        sampleNumber: true,
        fssaiLicenceNumber: true
    },
    'inventoryAttributes': {
        barcode: true,
        batchNumber: true,
        manufactureDate: true,
        packagingDate: true,
        bestBefore: true,
        unitMeasure: ['Kg', 'gram', 'litre', 'ml', 'count', 'Bag', 'Case', 'Pack']
    },
    'parent': db.categories.findOne({'name': 'Home Care', 'code': 'HOMEC', 'type': 'SubCategory1'})._id
});

db.categories.update({
    'name': 'Home Care',
    'code': 'HOMEC',
    'type': 'SubCategory1'
}, {
    $addToSet: {
        'children': db.categories.findOne({
            'name': 'Toilets and Surface Cleaners',
            'code': 'TOASC',
            'type': 'SubCategory2'
        })._id
    }
});


db.categories.insert({
    'name': 'Air Care and Cleaning',
    'code': 'ACACL',
    'type': 'SubCategory2',
    'categoryImageURL1': 'modules/categories/img/subcategory2/aircareandcleaning.png',
    'croppedCategoryImageURL1': 'modules/categories/img/subcategory2/mobileaircareandcleaning.png',
    'productCategory': true,
    'productAttributes': {
        brand: true,
        grade: {enabled: true},
        quality: {enabled: true},
        sampleNumber: true,
        fssaiLicenceNumber: true
    },
    'inventoryAttributes': {
        barcode: true,
        batchNumber: true,
        manufactureDate: true,
        packagingDate: true,
        bestBefore: true,
        unitMeasure: ['Kg', 'gram', 'litre', 'ml', 'count', 'Bag', 'Case', 'Pack']
    },
    'parent': db.categories.findOne({'name': 'Home Care', 'code': 'HOMEC', 'type': 'SubCategory1'})._id
});

db.categories.update({
    'name': 'Home Care',
    'code': 'HOMEC',
    'type': 'SubCategory1'
}, {
    $addToSet: {
        'children': db.categories.findOne({
            'name': 'Air Care and Cleaning',
            'code': 'ACACL',
            'type': 'SubCategory2'
        })._id
    }
});


db.categories.insert({
    'name': 'Detergents',
    'code': 'DTGNT',
    'type': 'SubCategory2',
    'categoryImageURL1': 'modules/categories/img/subcategory2/detergents.png',
    'croppedCategoryImageURL1': 'modules/categories/img/subcategory2/mobiledetergents.png',
    'productCategory': true,
    'productAttributes': {
        brand: true,
        grade: {enabled: true},
        quality: {enabled: true},
        sampleNumber: true,
        fssaiLicenceNumber: true
    },
    'inventoryAttributes': {
        barcode: true,
        batchNumber: true,
        manufactureDate: true,
        packagingDate: true,
        bestBefore: true,
        unitMeasure: ['Kg', 'gram', 'litre', 'ml', 'count', 'Bag', 'Case', 'Pack']
    },
    'parent': db.categories.findOne({'name': 'Home Care', 'code': 'HOMEC', 'type': 'SubCategory1'})._id
});

db.categories.update({
    'name': 'Home Care',
    'code': 'HOMEC',
    'type': 'SubCategory1'
}, {
    $addToSet: {
        'children': db.categories.findOne({
            'name': 'Detergents',
            'code': 'DTGNT',
            'type': 'SubCategory2'
        })._id
    }
});


db.categories.insert({
    'name': 'Pet Care',
    'code': 'PETCR',
    'type': 'SubCategory2',
    'categoryImageURL1': 'modules/categories/img/subcategory2/petcare.png',
    'croppedCategoryImageURL1': 'modules/categories/img/subcategory2/mobilepetcare.png',
    'productCategory': true,
    'productAttributes': {
        brand: true,
        grade: {enabled: true},
        quality: {enabled: true},
        sampleNumber: true,
        fssaiLicenceNumber: true
    },
    'inventoryAttributes': {
        barcode: true,
        batchNumber: true,
        manufactureDate: true,
        packagingDate: true,
        bestBefore: true,
        unitMeasure: ['Kg', 'gram', 'litre', 'ml', 'count', 'Bag', 'Case', 'Pack']
    },
    'parent': db.categories.findOne({'name': 'Home Care', 'code': 'HOMEC', 'type': 'SubCategory1'})._id
});

db.categories.update({
    'name': 'Home Care',
    'code': 'HOMEC',
    'type': 'SubCategory1'
}, {$addToSet: {'children': db.categories.findOne({'name': 'Pet Care', 'code': 'PETCR', 'type': 'SubCategory2'})._id}});


db.categories.insert({
    'name': 'Paper and Disposable',
    'code': 'PPRDL',
    'type': 'SubCategory2',
    'categoryImageURL1': 'modules/categories/img/subcategory2/paperanddisposable.png',
    'croppedCategoryImageURL1': 'modules/categories/img/subcategory2/mobilepaperanddisposable.png',
    'productCategory': true,
    'productAttributes': {
        brand: true,
        grade: {enabled: true},
        quality: {enabled: true},
        sampleNumber: true,
        fssaiLicenceNumber: true
    },
    'inventoryAttributes': {
        barcode: true,
        batchNumber: true,
        manufactureDate: true,
        packagingDate: true,
        bestBefore: true,
        unitMeasure: ['Kg', 'gram', 'count', 'Bag', 'Case', 'Pack']
    },
    'parent': db.categories.findOne({'name': 'Home Care', 'code': 'HOMEC', 'type': 'SubCategory1'})._id
});

db.categories.update({
    'name': 'Home Care',
    'code': 'HOMEC',
    'type': 'SubCategory1'
}, {
    $addToSet: {
        'children': db.categories.findOne({
            'name': 'Paper and Disposable',
            'code': 'PPRDL',
            'type': 'SubCategory2'
        })._id
    }
});


db.categories.insert({
    'name': 'Pooja Needs',
    'code': 'PJNDS',
    'type': 'SubCategory2',
    'categoryImageURL1': 'modules/categories/img/subcategory2/poojaneeds.png',
    'croppedCategoryImageURL1': 'modules/categories/img/subcategory2/mobilepoojaneeds.png',
    'productCategory': true,
    'productAttributes': {
        brand: true,
        grade: {enabled: true},
        quality: {enabled: true},
        sampleNumber: true,
        fssaiLicenceNumber: true
    },
    'inventoryAttributes': {
        barcode: true,
        batchNumber: true,
        manufactureDate: true,
        packagingDate: true,
        bestBefore: true,
        unitMeasure: ['Kg', 'gram', 'litre', 'ml', 'count', 'Bag', 'Case', 'Pack']
    },
    'parent': db.categories.findOne({'name': 'Home Care', 'code': 'HOMEC', 'type': 'SubCategory1'})._id
});

db.categories.update({
    'name': 'Home Care',
    'code': 'HOMEC',
    'type': 'SubCategory1'
}, {
    $addToSet: {
        'children': db.categories.findOne({
            'name': 'Pooja Needs',
            'code': 'PJNDS',
            'type': 'SubCategory2'
        })._id
    }
});

// END - Home Care

// Start - Personal Care
db.categories.insert({
    'name': 'Personal Care',
    'code': 'PRSNL',
    'type': 'SubCategory1',
    'categoryImageURL1': 'modules/categories/img/subcategory1/personalcare.png',
    'croppedCategoryImageURL1': 'modules/categories/img/subcategory1/mobilepersonalcare.png',
    'parent': db.categories.findOne({'name': 'FMCG', 'code': 'FMCGC', 'type': 'MainCategory'})._id
});

db.categories.insert({
    'name': 'Cosmetics and Toiletries',
    'code': 'COSMT',
    'type': 'SubCategory2',
    'categoryImageURL1': 'modules/categories/img/subcategory2/cosmeticsandtoiletries.png',
    'croppedCategoryImageURL1': 'modules/categories/img/subcategory2/mobilecosmeticsandtoiletries.png',
    'productCategory': true,
    'productAttributes': {
        brand: true,
        grade: {enabled: true},
        quality: {enabled: true},
        sampleNumber: true,
        fssaiLicenceNumber: true
    },
    'inventoryAttributes': {
        barcode: true,
        batchNumber: true,
        manufactureDate: true,
        packagingDate: true,
        bestBefore: true,
        unitMeasure: ['Kg', 'gram', 'litre', 'ml', 'count', 'Bag', 'Case', 'Pack']
    },
    'parent': db.categories.findOne({'name': 'Personal Care', 'code': 'PRSNL', 'type': 'SubCategory1'})._id
});

db.categories.update({
    'name': 'Personal Care',
    'code': 'PRSNL',
    'type': 'SubCategory1'
}, {
    $addToSet: {
        'children': db.categories.findOne({
            'name': 'Cosmetics and Toiletries',
            'code': 'COSMT',
            'type': 'SubCategory2'
        })._id
    }
});

db.categories.insert({
    'name': 'Skin Care',
    'code': 'SKINC',
    'type': 'SubCategory2',
    'categoryImageURL1': 'modules/categories/img/subcategory2/skincare.png',
    'croppedCategoryImageURL1': 'modules/categories/img/subcategory2/mobileskincare.png',
    'productCategory': true,
    'productAttributes': {
        brand: true,
        grade: {enabled: true},
        quality: {enabled: true},
        sampleNumber: true,
        fssaiLicenceNumber: true
    },
    'inventoryAttributes': {
        barcode: true,
        batchNumber: true,
        manufactureDate: true,
        packagingDate: true,
        bestBefore: true,
        unitMeasure: ['Kg', 'gram', 'litre', 'ml', 'count', 'Bag', 'Case', 'Pack']
    },
    'parent': db.categories.findOne({'name': 'Personal Care', 'code': 'PRSNL', 'type': 'SubCategory1'})._id
});

db.categories.update({
    'name': 'Personal Care',
    'code': 'PRSNL',
    'type': 'SubCategory1'
}, {
    $addToSet: {
        'children': db.categories.findOne({
            'name': 'Skin Care',
            'code': 'SKINC',
            'type': 'SubCategory2'
        })._id
    }
});

db.categories.insert({
    'name': 'Oral Care',
    'code': 'ORALC',
    'type': 'SubCategory2',
    'categoryImageURL1': 'modules/categories/img/subcategory2/oralcare.png',
    'croppedCategoryImageURL1': 'modules/categories/img/subcategory2/mobileoralcare.png',
    'productCategory': true,
    'productAttributes': {
        brand: true,
        grade: {enabled: true},
        quality: {enabled: true},
        sampleNumber: true,
        fssaiLicenceNumber: true
    },
    'inventoryAttributes': {
        barcode: true,
        batchNumber: true,
        manufactureDate: true,
        packagingDate: true,
        bestBefore: true,
        unitMeasure: ['Kg', 'gram', 'litre', 'ml', 'count', 'Bag', 'Case', 'Pack']
    },
    'parent': db.categories.findOne({'name': 'Personal Care', 'code': 'PRSNL', 'type': 'SubCategory1'})._id
});

db.categories.update({
    'name': 'Personal Care',
    'code': 'PRSNL',
    'type': 'SubCategory1'
}, {
    $addToSet: {
        'children': db.categories.findOne({
            'name': 'Oral Care',
            'code': 'ORALC',
            'type': 'SubCategory2'
        })._id
    }
});

db.categories.insert({
    'name': 'Hair Care',
    'code': 'HAIRC',
    'type': 'SubCategory2',
    'categoryImageURL1': 'modules/categories/img/subcategory2/haircare.png',
    'croppedCategoryImageURL1': 'modules/categories/img/subcategory2/mobilehaircare.png',
    'productCategory': true,
    'productAttributes': {
        brand: true,
        grade: {enabled: true},
        quality: {enabled: true},
        sampleNumber: true,
        fssaiLicenceNumber: true
    },
    'inventoryAttributes': {
        barcode: true,
        batchNumber: true,
        manufactureDate: true,
        packagingDate: true,
        bestBefore: true,
        unitMeasure: ['Kg', 'gram', 'litre', 'ml', 'count', 'Bag', 'Case', 'Pack']
    },
    'parent': db.categories.findOne({'name': 'Personal Care', 'code': 'PRSNL', 'type': 'SubCategory1'})._id
});

db.categories.update({
    'name': 'Personal Care',
    'code': 'PRSNL',
    'type': 'SubCategory1'
}, {
    $addToSet: {
        'children': db.categories.findOne({
            'name': 'Hair Care',
            'code': 'HAIRC',
            'type': 'SubCategory2'
        })._id
    }
});


db.categories.insert({
    'name': 'Perfumes and Deodrants',
    'code': 'PERDT',
    'type': 'SubCategory2',
    'categoryImageURL1': 'modules/categories/img/subcategory2/perfumesanddeodrants.png',
    'croppedCategoryImageURL1': 'modules/categories/img/subcategory2/mobileperfumesanddeodrants.png',
    'productCategory': true,
    'productAttributes': {
        brand: true,
        grade: {enabled: true},
        quality: {enabled: true},
        sampleNumber: true,
        fssaiLicenceNumber: true
    },
    'inventoryAttributes': {
        barcode: true,
        batchNumber: true,
        manufactureDate: true,
        packagingDate: true,
        bestBefore: true,
        unitMeasure: ['Kg', 'gram', 'litre', 'ml', 'count', 'Bag', 'Case', 'Pack']
    },
    'parent': db.categories.findOne({'name': 'Personal Care', 'code': 'PRSNL', 'type': 'SubCategory1'})._id
});

db.categories.update({
    'name': 'Personal Care',
    'code': 'PRSNL',
    'type': 'SubCategory1'
}, {
    $addToSet: {
        'children': db.categories.findOne({
            'name': 'Perfumes and Deodrants',
            'code': 'PERDT',
            'type': 'SubCategory2'
        })._id
    }
});
// END - Personal Care


// Adding sub categories to FMCG
db.categories.update({
    'name': 'FMCG',
    'code': 'FMCGC',
    'type': 'MainCategory'
}, {
    $addToSet: {
        'children': db.categories.findOne({
            'name': 'Health Care',
            'code': 'HCARE',
            'type': 'SubCategory1'
        })._id
    }
});

db.categories.update({
    'name': 'FMCG',
    'code': 'FMCGC',
    'type': 'MainCategory'
}, {
    $addToSet: {
        'children': db.categories.findOne({
            'name': 'Food Products',
            'code': 'FOODP',
            'type': 'SubCategory1'
        })._id
    }
});

db.categories.update({
    'name': 'FMCG',
    'code': 'FMCGC',
    'type': 'MainCategory'
}, {
    $addToSet: {
        'children': db.categories.findOne({
            'name': 'Beverages',
            'code': 'BVRGS',
            'type': 'SubCategory1'
        })._id
    }
});

db.categories.update({
    'name': 'FMCG',
    'code': 'FMCGC',
    'type': 'MainCategory'
}, {
    $addToSet: {
        'children': db.categories.findOne({
            'name': 'Home Care',
            'code': 'HOMEC',
            'type': 'SubCategory1'
        })._id
    }
});

db.categories.update({
    'name': 'FMCG',
    'code': 'FMCGC',
    'type': 'MainCategory'
}, {
    $addToSet: {
        'children': db.categories.findOne({
            'name': 'Personal Care',
            'code': 'PRSNL',
            'type': 'SubCategory1'
        })._id
    }
});


// Sub Categories of Agriculture
db.categories.insert({
    'name': 'Coffee',
    'code': 'COFFE',
    'type': 'SubCategory1',
    'categoryImageURL1': 'modules/categories/img/subcategory1/coffee.png',
    'croppedCategoryImageURL1': 'modules/categories/img/subcategory1/mobilecoffee.png',
    'parent': db.categories.findOne({'name': 'Agriculture', 'type': 'MainCategory'})._id
});

db.categories.update({
    'name': 'Agriculture',
    'type': 'MainCategory'
}, {$addToSet: {'children': db.categories.findOne({'name': 'Coffee', 'type': 'SubCategory1'})._id}});

// Sub Categories of Coffee
db.categories.insert({
    'name': 'Coffee Berries',
    'code': 'COBRS',
    'type': 'SubCategory2',
    'categoryImageURL1': 'modules/categories/img/subcategory2/coffeeberries.png',
    'croppedCategoryImageURL1': 'modules/categories/img/subcategory2/mobilecoffeeberries.png',
    'productCategory': true,
    'productAttributes': {
        brand: true,
        grade: {enabled: true},
        quality: {enabled: true},
        sampleNumber: true
    },
    'inventoryAttributes': {
        barcode: true,
        batchNumber: true,
        manufactureDate: true,
        packagingDate: true,
        bestBefore: true,
        unitMeasure:['Kg', 'gram', 'tonne', 'Bag', 'Case', 'Pack']
    },
    'parent': db.categories.findOne({'name': 'Coffee','code':'COFFE', 'type': 'SubCategory1'})._id
});
db.categories.update({
    'name': 'Coffee',
    'type': 'SubCategory1'
}, {$addToSet: {'children': db.categories.findOne({'name': 'Coffee Berries', 'type': 'SubCategory2'})._id}});


// Sub Categories of Coffee
db.categories.insert({
    'name': 'Green Coffee Beans',
    'code': 'GCBNS',
    'type': 'SubCategory2',
    'categoryImageURL1': 'modules/categories/img/subcategory2/greencoffeebeans.png',
    'croppedCategoryImageURL1': 'modules/categories/img/subcategory2/mobilegreencoffeebeans.png',
    'productCategory': true,
    'productAttributes': {
        brand: true,
        grade: {enabled: true},
        quality: {enabled: true},
        sampleNumber: true
    },
    'inventoryAttributes': {
        barcode: true,
        batchNumber: true,
        manufactureDate: true,
        packagingDate: true,
        bestBefore: true,
        unitMeasure: ['Kg', 'gram', 'tonne', 'Bag', 'Case', 'Pack']
    },
    'parent': db.categories.findOne({'name': 'Coffee', 'code': 'COFFE', 'type': 'SubCategory1'})._id
});
db.categories.update({
    'name': 'Coffee',
    'type': 'SubCategory1'
}, {$addToSet: {'children': db.categories.findOne({'name': 'Green Coffee Beans', 'type': 'SubCategory2'})._id}});


// Sub Categories of Coffee
db.categories.insert({
    'name': 'Roasted Coffee Beans',
    'code': 'RCBNS',
    'type': 'SubCategory2',
    'categoryImageURL1': 'modules/categories/img/subcategory2/roastedcoffeebeans.png',
    'croppedCategoryImageURL1': 'modules/categories/img/subcategory2/mobileroastedcoffeebeans.png',
    'productCategory': true,
    'productAttributes': {
        brand: true,
        grade: {enabled: true},
        quality: {enabled: true},
        sampleNumber: true
    },
    'inventoryAttributes': {
        barcode: true,
        batchNumber: true,
        manufactureDate: true,
        packagingDate: true,
        bestBefore: true,
        unitMeasure: ['Kg', 'gram', 'tonne', 'Bag', 'Case', 'Pack']
    },
    'parent': db.categories.findOne({'name': 'Coffee', 'code': 'COFFE', 'type': 'SubCategory1'})._id
});
db.categories.update({
    'name': 'Coffee',
    'type': 'SubCategory1'
}, {$addToSet: {'children': db.categories.findOne({'name': 'Roasted Coffee Beans', 'type': 'SubCategory2'})._id}});


// Sub Categories of Beverages
db.categories.insert({
    'name': 'Coffee Beans',
    'code': 'COFBN',
    'type': 'SubCategory2',
    'categoryImageURL1': 'modules/categories/img/subcategory2/coffeebeans.png',
    'croppedCategoryImageURL1': 'modules/categories/img/subcategory2/mobilecoffeebeans.png',
    'productCategory': true,
    'productAttributes': {
        brand: true,
        grade: {enabled: true},
        quality: {enabled: true},
        sampleNumber: true
    },
    'inventoryAttributes': {
        barcode: true,
        batchNumber: true,
        manufactureDate: true,
        packagingDate: true,
        bestBefore: true,
        unitMeasure:['Kg', 'gram', 'tonne', 'Bag', 'Case', 'Pack']
    },
    'parent': db.categories.findOne({'name': 'Beverages', 'type': 'SubCategory1'})._id
});


db.categories.update({
    'name': 'Beverages',
    'type': 'SubCategory1'
}, {$addToSet: {'children': db.categories.findOne({'name': 'Coffee Beans', 'type': 'SubCategory2'})._id}});

// Sub Categories of Beverages
db.categories.insert({
    'name': 'Coffee Powder',
    'code': 'COPDR',
    'type': 'SubCategory2',
    'categoryImageURL1': 'modules/categories/img/subcategory2/coffeepowder.png',
    'croppedCategoryImageURL1': 'modules/categories/img/subcategory2/mobilecoffeepowder.png',
    'productCategory': true,
    'productAttributes': {
        brand: true,
        grade: {enabled: true},
        quality: {enabled: true},
        sampleNumber: true
    },
    'inventoryAttributes': {
        barcode: true,
        batchNumber: true,
        manufactureDate: true,
        packagingDate: true,
        bestBefore: true,
        unitMeasure:['Kg', 'gram', 'tonne', 'Bag', 'Case', 'Pack']
    },
    'parent': db.categories.findOne({'name': 'Beverages', 'type': 'SubCategory1'})._id
});


db.categories.update({
    'name': 'Beverages',
    'type': 'SubCategory1'
}, {$addToSet: {'children': db.categories.findOne({'name': 'Coffee Powder', 'type': 'SubCategory2'})._id}});


// FMCG - End
//Agri Category
// Sub Categories of Agriculture
/*db.categories.insert({
    'name': 'Coffee',
    'code': 'COFFE',
    'type': 'SubCategory1',
    'categoryImageURL1': 'modules/categories/img/subcategory1/coffee.png',
    'croppedCategoryImageURL1': 'modules/categories/img/subcategory1/mobilecoffee.png',
    'parent': db.categories.findOne({'name': 'Agriculture', 'type': 'MainCategory'})._id
});

db.categories.update({
    'name': 'Agriculture',
    'type': 'MainCategory'
}, {$addToSet: {'children': db.categories.findOne({'name': 'Coffee', 'type': 'SubCategory1'})._id}});*/

// Sub Categories of Coffee
db.categories.insert({
    'name': 'Coffee Berries',
    'code': 'COBRS',
    'type': 'SubCategory2',
    'categoryImageURL1': 'modules/categories/img/subcategory2/coffeeberries.png',
    'croppedCategoryImageURL1': 'modules/categories/img/subcategory2/mobilecoffeeberries.png',
    'productCategory': true,
    'productAttributes': {
        brand: true,
        grade: {enabled: true},
        quality: {enabled: true},
        sampleNumber: true
    },
    'inventoryAttributes': {
        barcode: true,
        batchNumber: true,
        manufactureDate: true,
        packagingDate: true,
        bestBefore: true,
        unitMeasure:['Kg', 'gram', 'tonne', 'Bag', 'Case', 'Pack']
    },
    'parent': db.categories.findOne({'name': 'Coffee','code':'COFFE', 'type': 'SubCategory1'})._id
});
db.categories.update({
    'name': 'Coffee',
    'type': 'SubCategory1'
}, {$addToSet: {'children': db.categories.findOne({'name': 'Coffee Berries', 'type': 'SubCategory2'})._id}});


// Sub Categories of Coffee
db.categories.insert({
    'name': 'Green Coffee Beans',
    'code': 'GCBNS',
    'type': 'SubCategory2',
    'categoryImageURL1': 'modules/categories/img/subcategory2/greencoffeebeans.png',
    'croppedCategoryImageURL1': 'modules/categories/img/subcategory2/mobilegreencoffeebeans.png',
    'productCategory': true,
    'productAttributes': {
        brand: true,
        grade: {enabled: true},
        quality: {enabled: true},
        sampleNumber: true
    },
    'inventoryAttributes': {
        barcode: true,
        batchNumber: true,
        manufactureDate: true,
        packagingDate: true,
        bestBefore: true,
        unitMeasure: ['Kg', 'gram', 'tonne', 'Bag', 'Case', 'Pack']
    },
    'parent': db.categories.findOne({'name': 'Coffee', 'code': 'COFFE', 'type': 'SubCategory1'})._id
});
db.categories.update({
    'name': 'Coffee',
    'type': 'SubCategory1'
}, {$addToSet: {'children': db.categories.findOne({'name': 'Green Coffee Beans', 'type': 'SubCategory2'})._id}});


// Sub Categories of Coffee
db.categories.insert({
    'name': 'Roasted Coffee Beans',
    'code': 'RCBNS',
    'type': 'SubCategory2',
    'categoryImageURL1': 'modules/categories/img/subcategory2/roastedcoffeebeans.png',
    'croppedCategoryImageURL1': 'modules/categories/img/subcategory2/mobileroastedcoffeebeans.png',
    'productCategory': true,
    'productAttributes': {
        brand: true,
        grade: {enabled: true},
        quality: {enabled: true},
        sampleNumber: true
    },
    'inventoryAttributes': {
        barcode: true,
        batchNumber: true,
        manufactureDate: true,
        packagingDate: true,
        bestBefore: true,
        unitMeasure: ['Kg', 'gram', 'tonne', 'Bag', 'Case', 'Pack']
    },
    'parent': db.categories.findOne({'name': 'Coffee', 'code': 'COFFE', 'type': 'SubCategory1'})._id
});
db.categories.update({
    'name': 'Coffee',
    'type': 'SubCategory1'
}, {$addToSet: {'children': db.categories.findOne({'name': 'Roasted Coffee Beans', 'type': 'SubCategory2'})._id}});


//

// Sub Categories of Spices
db.categories.insert({
    'name': 'Sunflower Seeds',
    'code': 'SUNSD',
    'type': 'SubCategory2',
    'categoryImageURL1': 'modules/categories/img/subcategory2/sunflowerseeds.png',
    'croppedCategoryImageURL1': 'modules/categories/img/subcategory2/mobilesunflowerseeds.png',
    'productCategory': true,
    'productAttributes': {
        brand: true,
        grade: {enabled: true},
        quality: {enabled: true},
        sampleNumber: true
    },
    'inventoryAttributes': {
        barcode: true,
        batchNumber: true,
        manufactureDate: true,
        packagingDate: true,
        bestBefore: true,
        unitMeasure:['Kg', 'gram', 'tonne', 'Bag', 'Pack']
    },
    'parent': db.categories.findOne({'name': 'Spices', 'type': 'SubCategory1'})._id
});


db.categories.update({
    'name': 'Spices',
    'type': 'SubCategory1'
}, {$addToSet: {'children': db.categories.findOne({'name': 'Sunflower Seeds', 'type': 'SubCategory2'})._id}});


// Sub Categories of Spices
db.categories.insert({
    'name': 'Flax Seeds',
    'code': 'FLXSD',
    'type': 'SubCategory2',
    'categoryImageURL1': 'modules/categories/img/subcategory2/flaxseeds.png',
    'croppedCategoryImageURL1': 'modules/categories/img/subcategory2/mobileflaxseeds.png',
    'productCategory': true,
    'productAttributes': {
        brand: true,
        grade: {enabled: true},
        quality: {enabled: true},
        sampleNumber: true
    },
    'inventoryAttributes': {
        barcode: true,
        batchNumber: true,
        manufactureDate: true,
        packagingDate: true,
        bestBefore: true,
        unitMeasure:['Kg', 'gram', 'tonne', 'Bag', 'Pack']
    },
    'parent': db.categories.findOne({'name': 'Spices', 'type': 'SubCategory1'})._id
});


db.categories.update({
    'name': 'Spices',
    'type': 'SubCategory1'
}, {$addToSet: {'children': db.categories.findOne({'name': 'Flax Seeds', 'type': 'SubCategory2'})._id}});
db.categories.insert({
    'name': 'Husk',
    'code': 'HUSK',
    'type': 'SubCategory2',
    'categoryImageURL1': 'modules/categories/img/subcategory2/husk.png',
    'croppedCategoryImageURL1': 'modules/categories/img/subcategory2/mobilehusk.png',
    'productCategory': true,
    'productAttributes': {
        brand: true,
        grade: {enabled: true},
        quality: {enabled: true},
        sampleNumber: true
    },
    'inventoryAttributes': {
        barcode: true,
        batchNumber: true,
        manufactureDate: true,
        packagingDate: true,
        bestBefore: true,
        unitMeasure:['Kg', 'gram', 'tonne', 'Bag', 'Pack']
    },
    'parent': db.categories.findOne({'name': 'Cereals', 'type': 'SubCategory1'})._id
});


db.categories.update({
    'name': 'Cereals',
    'type': 'SubCategory1'
}, {$addToSet: {'children': db.categories.findOne({'name': 'Husk', 'type': 'SubCategory2'})._id}});

db.hsncodes.insert({
    'name': 'Rice (RICEG)',
    'hsncode': '10063020',
    'description': 'Basmati Rice - White',
    'CGST':0,
    'SGST':0,
    'IGST':0
});
db.categories.update({
    'name': 'Rice',
    'code': 'RICEG',
    'type': 'SubCategory2'
}, {$addToSet: {'hsncodes':{ hsncode: db.hsncodes.findOne({'hsncode': '10063020'})._id}}});


db.hsncodes.insert({
    'name': 'Rice (RICEG)',
    'hsncode': '10062000',
    'description': 'Basmati Rice - Brown',
    'CGST':0,
    'SGST':0,
    'IGST':0
});
db.categories.update({
    'name': 'Rice',
    'code': 'RICEG',
    'type': 'SubCategory2'
}, {$addToSet: {'hsncodes':{ hsncode: db.hsncodes.findOne({'hsncode': '10062000'})._id}}});


db.hsncodes.insert({
    'name': 'Rice (RICEG)',
    'hsncode': '10061090',
    'description': 'Basmati Paddy(taraori)',
    'CGST':0,
    'SGST':0,
    'IGST':0
});
db.categories.update({
    'name': 'Rice',
    'code': 'RICEG',
    'type': 'SubCategory2'
}, {$addToSet: {'hsncodes':{ hsncode: db.hsncodes.findOne({'hsncode': '10061090'})._id}}});


db.hsncodes.insert({
    'name': 'Corn(CORNG)',
    'hsncode': '07104000',
    'description': 'Sweet Corn',
    'CGST':0,
    'SGST':0,
    'IGST':0
});
db.categories.update({
    'name': 'Corn',
    'code': 'CORNG',
    'type': 'SubCategory2'
}, {$addToSet: {'hsncodes':{ hsncode: db.hsncodes.findOne({'hsncode': '07104000'})._id}}});



db.hsncodes.insert({
    'name': 'Rice(RICEG)',
    'hsncode': '10062000',
    'description': 'Husked(Brown) Rice',
    'CGST':0,
    'SGST':0,
    'IGST':0
});
db.categories.update({
    'name': 'Rice',
    'code': 'RICEG',
    'type': 'SubCategory2'
}, {$addToSet: {'hsncodes':{ hsncode: db.hsncodes.findOne({'hsncode': '10062000'})._id}}});


db.hsncodes.insert({
    'name': 'Rice(RICEG)',
    'hsncode': '10061090',
    'description': 'Rice in husk (paddy or rough)',
    'CGST':0,
    'SGST':0,
    'IGST':0
});
db.categories.update({
    'name': 'Rice',
    'code': 'RICEG',
    'type': 'SubCategory2'
}, {$addToSet: {'hsncodes':{ hsncode: db.hsncodes.findOne({'hsncode': '10061090'})._id}}});


db.hsncodes.insert({
    'name': 'Turmeric(TURMC)',
    'hsncode': '09103010',
    'description': 'Neither Crushed Nor Ground,Fresh',
    'CGST':0,
    'SGST':0,
    'IGST':0
});
db.categories.update({
    'name': 'Turmeric',
    'code': 'TURMC',
    'type': 'SubCategory2'
}, {$addToSet: {'hsncodes':{ hsncode: db.hsncodes.findOne({'hsncode': '09103010'})._id}}});



db.hsncodes.insert({
    'name': 'Turmeric(TURMC)',
    'hsncode': '09101120',
    'description': 'Neither Crushed Nor Ground,Dried',
    'CGST':0,
    'SGST':0,
    'IGST':0
});
db.categories.update({
    'name': 'Turmeric',
    'code': 'TURMC',
    'type': 'SubCategory2'
}, {$addToSet: {'hsncodes':{ hsncode: db.hsncodes.findOne({'hsncode': '09101120'})._id}}});


db.hsncodes.insert({
    'name': 'Bay Leaf(BAYLF)',
    'hsncode': '09101110',
    'description': 'Neither Crushed,Nor Ground,Fresh',
    'CGST':0,
    'SGST':0,
    'IGST':0
});
db.categories.update({
    'name': 'Bay Leaf',
    'code': 'BAYLF',
    'type': 'SubCategory2'
}, {$addToSet: {'hsncodes':{ hsncode: db.hsncodes.findOne({'hsncode': '09101110'})._id}}});



db.hsncodes.insert({
    'name': 'Bay Leaf(BAYLF)',
    'hsncode': '09101120',
    'description': 'Neither Crushed,Nor Ground,Dried,UnBleached',
    'CGST':0,
    'SGST':0,
    'IGST':0
});
db.categories.update({
    'name': 'Bay Leaf',
    'code': 'BAYLF',
    'type': 'SubCategory2'
}, {$addToSet: {'hsncodes':{ hsncode: db.hsncodes.findOne({'hsncode': '09101120'})._id}}});



db.hsncodes.insert({
    'name': 'Bay Leaf(BAYLF)',
    'hsncode': '09101130',
    'description': 'Neither Crushed,Nor Ground,Dried,Bleached',
    'CGST':0,
    'SGST':0,
    'IGST':0
});
db.categories.update({
    'name': 'Bay Leaf',
    'code': 'BAYLF',
    'type': 'SubCategory2'
}, {$addToSet: {'hsncodes':{ hsncode: db.hsncodes.findOne({'hsncode': '09101130'})._id}}});



db.hsncodes.insert({
    'name': 'Ginger(GINGR)',
    'hsncode': '09101110',
    'description': 'Neither Crushed Nor Ground,Fresh',
    'CGST':0,
    'SGST':0,
    'IGST':0
});
db.categories.update({
    'name': 'Ginger',
    'code': 'GINGR',
    'type': 'SubCategory2'
}, {$addToSet: {'hsncodes':{ hsncode: db.hsncodes.findOne({'hsncode': '09101110'})._id}}});



db.hsncodes.insert({
    'name': 'Ginger(GINGR)',
    'hsncode': '09101120',
    'description': 'Neither Crushed Nor Ground,Dried & unBleached',
    'CGST':0,
    'SGST':0,
    'IGST':0
});
db.categories.update({
    'name': 'Ginger',
    'code': 'GINGR',
    'type': 'SubCategory2'
}, {$addToSet: {'hsncodes':{ hsncode: db.hsncodes.findOne({'hsncode': '09101120'})._id}}});



db.hsncodes.insert({
    'name': 'Ginger(GINGR)',
    'hsncode': '09101130',
    'description': 'Neither Crushed Nor Ground,Dried & Bleached',
    'CGST':0,
    'SGST':0,
    'IGST':0
});
db.categories.update({
    'name': 'Ginger',
    'code': 'GINGR',
    'type': 'SubCategory2'
}, {$addToSet: {'hsncodes':{ hsncode: db.hsncodes.findOne({'hsncode': '09101130'})._id}}});


db.hsncodes.insert({
    'name': 'Salt(SALTS)',
    'hsncode': '25010010',
    'description': 'Salt',
    'CGST':0,
    'SGST':0,
    'IGST':0
});
db.categories.update({
    'name': 'Salt',
    'code': 'SALTS',
    'type': 'SubCategory2'
}, {$addToSet: {'hsncodes':{ hsncode: db.hsncodes.findOne({'hsncode': '25010010'})._id}}});



db.hsncodes.insert({
    'name': 'Tamarind(TMRND)',
    'hsncode': '08109020',
    'description': 'Tamarind,Fresh',
    'CGST':0,
    'SGST':0,
    'IGST':0
});
db.categories.update({
    'name': 'Tamarind',
    'code': 'TMRND',
    'type': 'SubCategory2'
}, {$addToSet: {'hsncodes':{ hsncode: db.hsncodes.findOne({'hsncode': '08109020'})._id}}});



db.hsncodes.insert({
    'name': 'Tamarind(TMRND)',
    'hsncode': '08134010',
    'description': 'Tamarind,Dried',
    'CGST':0,
    'SGST':0,
    'IGST':0
});
db.categories.update({
    'name': 'Tamarind',
    'code': 'TMRND',
    'type': 'SubCategory2'
}, {$addToSet: {'hsncodes':{ hsncode: db.hsncodes.findOne({'hsncode': '08134010'})._id}}});



db.hsncodes.insert({
    'name': 'Sunflower seeds(SUNSD)',
    'hsncode': '12060010',
    'description': 'Sunflower Seeds',
    'CGST':0,
    'SGST':0,
    'IGST':0
});
db.categories.update({
    'name': 'Sunflower Seeds',
    'code': 'SUNSD',
    'type': 'SubCategory2'
}, {$addToSet: {'hsncodes':{ hsncode: db.hsncodes.findOne({'hsncode': '12060010'})._id}}});



db.hsncodes.insert({
    'name': 'Sabja(SABJA)',
    'hsncode': '12119092',
    'description': 'Sabja seeds',
    'CGST':0,
    'SGST':0,
    'IGST':0
});
db.categories.update({
    'name': 'Sabja',
    'code': 'SABJA',
    'type': 'SubCategory2'
}, {$addToSet: {'hsncodes':{ hsncode: db.hsncodes.findOne({'hsncode': '12119092'})._id}}});





db.hsncodes.insert({
    'name': 'Jeera(JEERA)',
    'hsncode': '09093129',
    'description': 'Jeera,other than black',
    'CGST':0,
    'SGST':0,
    'IGST':0
});
db.categories.update({
    'name': 'Jeera',
    'code': 'JEERA',
    'type': 'SubCategory2'
}, {$addToSet: {'hsncodes':{ hsncode: db.hsncodes.findOne({'hsncode': '09093129'})._id}}});



db.hsncodes.insert({
    'name': 'Proso Millet(PSMLT)',
    'hsncode': '10083090',
    'description': 'Proso Millet',
    'CGST':0,
    'SGST':0,
    'IGST':0
});
db.categories.update({
    'name': 'Proso Millet',
    'code': 'PSMLT',
    'type': 'SubCategory2'
}, {$addToSet: {'hsncodes':{ hsncode: db.hsncodes.findOne({'hsncode': '10083090'})._id}}});



db.hsncodes.insert({
    'name': 'Pearl Millet (PRMLT)',
    'hsncode': '10082120',
    'description': 'Pearl Millet',
    'CGST':0,
    'SGST':0,
    'IGST':0
});
db.categories.update({
    'name': 'Pearl Millet',
    'code': 'PRMLT',
    'type': 'SubCategory2'
}, {$addToSet: {'hsncodes':{ hsncode: db.hsncodes.findOne({'hsncode':'10082120'})._id}}});



db.hsncodes.insert({
    'name': 'Barnyard Millet(BYMLT)',
    'hsncode': '10089090',
    'description': 'Barnyard millet',
    'CGST':0,
    'SGST':0,
    'IGST':0
});
db.categories.update({
    'name': 'Barnyard Millet',
    'code': 'BYMLT',
    'type': 'SubCategory2'
}, {$addToSet: {'hsncodes':{ hsncode: db.hsncodes.findOne({'hsncode': '10089090'})._id}}});



db.hsncodes.insert({
    'name': 'Kodo Millet (KOMLT)',
    'hsncode': '10089090',
    'description': 'Kodo millet',
    'CGST':0,
    'SGST':0,
    'IGST':0
});
db.categories.update({
    'name': 'Kodo Millet',
    'code': 'KOMLT',
    'type': 'SubCategory2'
}, {$addToSet: {'hsncodes':{hsncode: db.hsncodes.findOne({'name':'Kodo Millet (KOMLT)','hsncode': '10089090'})._id}}});




db.hsncodes.insert({
    'name': 'Foxtail Millet (FTMLT)',
    'hsncode': '10089090',
    'description': 'Foxtail millet',
    'CGST':0,
    'SGST':0,
    'IGST':0
});
db.categories.update({
    'name': 'Foxtail Millet',
    'code': 'FTMLT',
    'type': 'SubCategory2'
}, {$addToSet: {'hsncodes':{ hsncode: db.hsncodes.findOne({'name':'Foxtail Millet (FTMLT)','hsncode': '10089090' })._id}}});




db.hsncodes.insert({
    'name': 'Sorghum bicolor(SGMBI)',
    'hsncode': '10079000',
    'description': 'Sorghum bicolor ',
    'CGST':0,
    'SGST':0,
    'IGST':0
});
db.categories.update({
    'name': 'Sorghum Bicolor',
    'code': 'SGMBI',
    'type': 'SubCategory2'
}, {$addToSet: {'hsncodes':{ hsncode: db.hsncodes.findOne({'hsncode': '10079000'})._id}}});



db.hsncodes.insert({
    'name': 'Finger Millet (FGMLT)',
    'hsncode': '10089090',
    'description': 'Finger millet',
    'CGST':0,
    'SGST':0,
    'IGST':0
});
db.categories.update({
    'name': 'Finger Millet',
    'code': 'FGMLT',
    'type': 'SubCategory2'
}, {$addToSet: {'hsncodes':{ hsncode: db.hsncodes.findOne({'name':'Finger Millet (FGMLT)','hsncode': '10089090'})._id}}});




db.hsncodes.insert({
    'name': 'Little Millet (LTMLT)',
    'hsncode': '10089090',
    'description': 'Little millet',
    'CGST':0,
    'SGST':0,
    'IGST':0
});
db.categories.update({
    'name': 'Little Millet',
    'code': 'LTMLT',
    'type': 'SubCategory2'
}, {$addToSet: {'hsncodes':{ hsncode: db.hsncodes.findOne({'name':'Little Millet (LTMLT)','hsncode': '10089090'})._id}}});


db.hsncodes.insert({
    'name': 'Groundnut(GRDNT)',
    'hsncode': '12024210',
    'description': 'Ground nut',
    'CGST':0,
    'SGST':0,
    'IGST':0
});
db.categories.update({
    'name': 'Groundnut',
    'code': 'GRDNT',
    'type': 'SubCategory2'
}, {$addToSet: {'hsncodes':{ hsncode: db.hsncodes.findOne({'hsncode': '12024210'})._id}}});


db.hsncodes.insert({
    'name': 'Cowpea(COWPE)',
    'hsncode': '07133500',
    'description': 'cowpea',
    'CGST':0,
    'SGST':0,
    'IGST':0
});
db.categories.update({
    'name': 'Cowpea',
    'code': 'COWPE',
    'type': 'SubCategory2'
}, {$addToSet: {'hsncodes':{ hsncode: db.hsncodes.findOne({'hsncode': '07133500'})._id}}});


db.hsncodes.insert({
    'name': 'Horse Gram (HGDAL)',
    'hsncode': '07132000',
    'description': 'Horse gram',
    'CGST':0,
    'SGST':0,
    'IGST':0
});
db.categories.update({
    'name': 'Horse Gram',
    'code': 'HGDAL',
    'type': 'SubCategory2'
}, {$addToSet: {'hsncodes':{ hsncode: db.hsncodes.findOne({'name':'Horse Gram (HGDAL)','hsncode': '07132000'})._id}}});



db.hsncodes.insert({
    'name': 'Kabuli Chana Dal Brown (KBDAL)',
    'hsncode': '07132000',
    'description': 'Kabuli Chana dal',
    'CGST':0,
    'SGST':0,
    'IGST':0
});
db.categories.update({
    'name': 'Kabuli Chana Dal Brown',
    'code': 'KBDAL',
    'type': 'SubCategory2'
}, {$addToSet: {'hsncodes':{ hsncode: db.hsncodes.findOne({'name': 'Kabuli Chana Dal Brown (KBDAL)','hsncode': '07132000'})._id}}});




db.hsncodes.insert({
    'name': 'Green Moong Dal split(MGDLS)',
    'hsncode': '07133100',
    'description': 'Moong dal(green)',
    'CGST':0,
    'SGST':0,
    'IGST':0
});
db.categories.update({
    'name': 'Green Moong Dal Split',
    'code': 'MGDLS',
    'type': 'SubCategory2'
}, {$addToSet: {'hsncodes':{ hsncode: db.hsncodes.findOne({'hsncode': '07133100'})._id}}});



db.hsncodes.insert({
    'name': 'Green moong dal whole(MGDLW)',
    'hsncode': '07139090',
    'description': 'Green Moong dal(whole)',
    'CGST':0,
    'SGST':0,
    'IGST':0
});
db.categories.update({
    'name': 'Green Moong Dal Whole',
    'code': 'MGDLW',
    'type': 'SubCategory2'
}, {$addToSet: {'hsncodes':{ hsncode: db.hsncodes.findOne({'hsncode': '07139090'})._id}}});




db.hsncodes.insert({
    'name': 'Black Urad dal split(BUDLS)',
    'hsncode': '07133990',
    'description': 'Urad Dal Black Split',
    'CGST':0,
    'SGST':0,
    'IGST':0
});
db.categories.update({
    'name': 'Black Urad Dal Split',
    'code': 'BUDLS',
    'type': 'SubCategory2'
}, {$addToSet: {'hsncodes':{ hsncode: db.hsncodes.findOne({'hsncode': '07133990'})._id}}});



db.hsncodes.insert({
    'name': 'Soya Beans(SOYBN)',
    'hsncode': '12019000',
    'description': 'Soya Beans',
    'CGST':0,
    'SGST':0,
    'IGST':0
});
db.categories.update({
    'name': 'Soya Bean',
    'code': 'SOYBN',
    'type': 'SubCategory2'
}, {$addToSet: {'hsncodes':{ hsncode: db.hsncodes.findOne({'hsncode': '12019000'})._id}}});



db.hsncodes.insert({
    'name': 'Sesame seeds(SESAM)',
    'hsncode': '12074090',
    'description': 'Sesame seeds',
    'CGST':0,
    'SGST':0,
    'IGST':0
});
db.categories.update({
    'name': 'Sesame',
    'code': 'SESAM',
    'type': 'SubCategory2'
}, {$addToSet: {'hsncodes':{ hsncode: db.hsncodes.findOne({'name':'Sesame seeds(SESAM)','hsncode': '12074090'})._id}}});



db.hsncodes.insert({
    'name': 'Toor dal(TRDAL)',
    'hsncode': '07136000',
    'description': 'Toor dal',
    'CGST':0,
    'SGST':0,
    'IGST':0
});
db.categories.update({
    'name': 'Toor Dal',
    'code': 'TRDAL',
    'type': 'SubCategory2'
}, {$addToSet: {'hsncodes':{ hsncode: db.hsncodes.findOne({'hsncode': '07136000'})._id}}});



db.hsncodes.insert({
    'name': 'Masoor Dal split(MRDAL)',
    'hsncode': '07139010',
    'description': 'Masoor Dal split',
    'CGST':0,
    'SGST':0,
    'IGST':0
});
db.categories.update({
    'name': 'Masoor Dal',
    'code': 'MRDAL',
    'type': 'SubCategory2'
}, {$addToSet: {'hsncodes':{ hsncode: db.hsncodes.findOne({'hsncode': '07139010'})._id}}});



db.hsncodes.insert({
    'name': 'Rajma Beans(RAJBE)',
    'hsncode': '12074090',
    'description': 'Rajma beans',
    'CGST':0,
    'SGST':0,
    'IGST':0
});
db.categories.update({
    'name': 'Rajma Beans',
    'code': 'RAJBE',
    'type': 'SubCategory2'
}, {$addToSet: {'hsncodes':{ hsncode: db.hsncodes.findOne({'name':'Rajma Beans(RAJBE)','hsncode': '12074090'})._id}}});



db.hsncodes.insert({
    'name': 'Coffee Berries(COBRS)',
    'hsncode': '09011124',
    'description': 'Coffee Berries(Arabica cherry B/b/b grade)',
    'CGST':0,
    'SGST':0,
    'IGST':0
});
db.categories.update({
    'name': 'Coffee Berries',
    'code': 'COBRS',
    'type': 'SubCategory2'
}, {$addToSet: {'hsncodes':{ hsncode: db.hsncodes.findOne({'hsncode': '09011124'})._id}}});



db.hsncodes.insert({
    'name': 'Coffee Berries(COBRS)',
    'hsncode': '09011129',
    'description': 'Coffee Berries(Arabica cherry other )',
    'CGST':0,
    'SGST':0,
    'IGST':0
});
db.categories.update({
    'name': 'Coffee Berries',
    'code': 'COBRS',
    'type': 'SubCategory2'
}, {$addToSet: {'hsncodes':{ hsncode: db.hsncodes.findOne({'hsncode': '09011129'})._id}}});



db.hsncodes.insert({
    'name': 'Coffee Berries(COBRS)',
    'hsncode': '09011141',
    'description': 'Coffee Berries(Rob cherry AB grade )',
    'CGST':0,
    'SGST':0,
    'IGST':0
});
db.categories.update({
    'name': 'Coffee Berries',
    'code': 'COBRS',
    'type': 'SubCategory2'
}, {$addToSet: {'hsncodes':{ hsncode: db.hsncodes.findOne({'hsncode': '09011141'})._id}}});



db.hsncodes.insert({
    'name': 'Coffee Berries(COBRS)',
    'hsncode': '09011142',
    'description': 'Coffee Berries(Rob cherry PB grade )',
    'CGST':0,
    'SGST':0,
    'IGST':0
});
db.categories.update({
    'name': 'Coffee Berries',
    'code': 'COBRS',
    'type': 'SubCategory2'
}, {$addToSet: {'hsncodes':{ hsncode: db.hsncodes.findOne({'hsncode': '09011142'})._id}}});


db.hsncodes.insert({
    'name': 'Coffee Berries (COBRS)',
    'hsncode': '09011143',
    'description': 'Coffee Berries(Rob cherry C grade )',
    'CGST':0,
    'SGST':0,
    'IGST':0
});
db.categories.update({
    'name': 'Coffee Berries',
    'code': 'COBRS',
    'type': 'SubCategory2'
}, {$addToSet: {'hsncodes':{ hsncode: db.hsncodes.findOne({'hsncode': '09011143'})._id}}});



db.hsncodes.insert({
    'name': 'Coffee Berries (COBRS)',
    'hsncode': '09011144',
    'description': 'Coffee Berries(Rob cherry B/b/b grade )',
    'CGST':0,
    'SGST':0,
    'IGST':0
});
db.categories.update({
    'name': 'Coffee Berries',
    'code': 'COBRS',
    'type': 'SubCategory2'
}, {$addToSet: {'hsncodes':{ hsncode: db.hsncodes.findOne({'hsncode': '09011144'})._id}}});


db.hsncodes.insert({
    'name': 'Coffee Berries (COBRS)',
    'hsncode': '09011145',
    'description': 'Coffee Berries(Rob cherry Bulk )',
    'CGST':0,
    'SGST':0,
    'IGST':0
});
db.categories.update({
    'name': 'Coffee Berries',
    'code': 'COBRS',
    'type': 'SubCategory2'
}, {$addToSet: {'hsncodes':{ hsncode: db.hsncodes.findOne({'hsncode': '09011145'})._id}}});


db.hsncodes.insert({
    'name': 'Green Coffee Beans(GCBNS)',
    'hsncode': '09011190',
    'description': 'Green Coffee Beans(Not decaffeinated; other )',
    'CGST':0,
    'SGST':0,
    'IGST':0
});
db.categories.update({
    'name': 'Green Coffee Beans',
    'code': 'GCBNS',
    'type': 'SubCategory2'
}, {$addToSet: {'hsncodes':{ hsncode: db.hsncodes.findOne({'hsncode': '09011190'})._id}}});



db.hsncodes.insert({
    'name': 'Green Coffee Beans(GCBNS)',
    'hsncode': '09011200',
    'description': 'Green Coffee Beans(Decaffeinated)',
    'CGST':0,
    'SGST':0,
    'IGST':0
});
db.categories.update({
    'name': 'Green Coffee Beans',
    'code': 'GCBNS',
    'type': 'SubCategory2'
}, {$addToSet: {'hsncodes':{ hsncode: db.hsncodes.findOne({'hsncode': '09011200'})._id}}});


db.hsncodes.insert({
    'name': 'Roasted Coffee Beans(RCBNS)',
    'hsncode': '09012110',
    'description': 'Roasted Coffee Beans(Not Decaffeinated : In Bulk Packing)',
    'CGST':0,
    'SGST':0,
    'IGST':0
});
db.categories.update({
    'name': 'Roasted Coffee Beans',
    'code': 'RCBNS',
    'type': 'SubCategory2'
}, {$addToSet: {'hsncodes':{ hsncode: db.hsncodes.findOne({'hsncode': '09012110'})._id}}});




db.hsncodes.insert({
    'name': 'Roasted Coffee Beans(RCBNS)',
    'hsncode': '09012190',
    'description': 'Roasted Coffee Beans(Not Decaffeinated : Other)',
    'CGST':0,
    'SGST':0,
    'IGST':0
});
db.categories.update({
    'name': 'Roasted Coffee Beans',
    'code': 'RCBNS',
    'type': 'SubCategory2'
}, {$addToSet: {'hsncodes':{ hsncode: db.hsncodes.findOne({'hsncode': '09012190'})._id}}});



db.hsncodes.insert({
    'name': 'Roasted Coffee Beans(RCBNS)',
    'hsncode': '09012210',
    'description': 'Roasted Coffee Beans(Decaffeinated : In Bulk Packing)',
    'CGST':0,
    'SGST':0,
    'IGST':0
});
db.categories.update({
    'name': 'Roasted Coffee Beans',
    'code': 'RCBNS',
    'type': 'SubCategory2'
}, {$addToSet: {'hsncodes':{ hsncode: db.hsncodes.findOne({'hsncode': '09012210'})._id}}});




db.hsncodes.insert({
    'name': 'Garlic (GARLC)',
    'hsncode': '07032000',
    'description': 'Fresh Or Chilled Garlic',
    'CGST':0,
    'SGST':0,
    'IGST':0
});
db.categories.update({
    'name': 'Garlic',
    'code': 'GARLC',
    'type': 'SubCategory2'
}, {$addToSet: {'hsncodes':{ hsncode: db.hsncodes.findOne({'hsncode': '07032000'})._id}}});



db.hsncodes.insert({
    'name': 'Garlic (GARLC)',
    'hsncode': '07039000',
    'description': 'Garlic (Fresh Or Chilled Leeks And Other Alliaceous Vegetables)',
    'CGST':0,
    'SGST':0,
    'IGST':0
});
db.categories.update({
    'name': 'Garlic',
    'code': 'GARLC',
    'type': 'SubCategory2'
}, {$addToSet: {'hsncodes':{ hsncode: db.hsncodes.findOne({'hsncode': '07039000'})._id}}});



db.hsncodes.insert({
    'name': 'Onion (ONION)',
    'hsncode': '07031010',
    'description': 'Onion(Fresh Or Chilled Onions And Shallots Onions)',
    'CGST':0,
    'SGST':0,
    'IGST':0
});
db.categories.update({
    'name': 'Onion',
    'code': 'ONION',
    'type': 'SubCategory2'
}, {$addToSet: {'hsncodes':{ hsncode: db.hsncodes.findOne({'hsncode': '07031010'})._id}}});



db.hsncodes.insert({
    'name': 'Onion (ONION)',
    'hsncode': '07031020',
    'description': 'Onion(Fresh Or Chilled Onions And Shallots Shallots)',
    'CGST':0,
    'SGST':0,
    'IGST':0
});
db.categories.update({
    'name': 'Onion',
    'code': 'ONION',
    'type': 'SubCategory2'
}, {$addToSet: {'hsncodes':{ hsncode: db.hsncodes.findOne({'hsncode': '07031020'})._id}}});



db.hsncodes.insert({
    'name': 'Potato (POTAT)',
    'hsncode': '07011000',
    'description': 'Potatoes, Fresh Or Chilled Seed',
    'CGST':0,
    'SGST':0,
    'IGST':0
});
db.categories.update({
    'name': 'Potato',
    'code': 'POTAT',
    'type': 'SubCategory2'
}, {$addToSet: {'hsncodes':{ hsncode: db.hsncodes.findOne({'hsncode': '07011000'})._id}}});



db.hsncodes.insert({
    'name': 'Potato (POTAT)',
    'hsncode': '07019000',
    'description': 'Potatoes, Fresh Or Chilled Other',
    'CGST':0,
    'SGST':0,
    'IGST':0
});
db.categories.update({
    'name': 'Potato',
    'code': 'POTAT',
    'type': 'SubCategory2'
}, {$addToSet: {'hsncodes':{ hsncode: db.hsncodes.findOne({'hsncode': '07019000'})._id}}});


db.hsncodes.insert({
    'name': 'Lemon (LEMON)',
    'hsncode': '08055000',
    'description': '(Citrus fruit Fresh or Dried)',
    'CGST':0,
    'SGST':0,
    'IGST':0
});
db.categories.update({
    'name': 'Lemon',
    'code': 'LEMON',
    'type': 'SubCategory2'
}, {$addToSet: {'hsncodes':{ hsncode: db.hsncodes.findOne({'hsncode': '08055000'})._id}}});



db.hsncodes.insert({
    'name': 'Raw Corn (RCORN)',
    'hsncode': '11042300',
    'description': 'Raw Corn',
    'CGST':0,
    'SGST':0,
    'IGST':0
});
db.categories.update({
    'name': 'Raw Corn',
    'code': 'RCORN',
    'type': 'SubCategory2'
}, {$addToSet: {'hsncodes':{ hsncode: db.hsncodes.findOne({'hsncode': '11042300'})._id}}});

db.hsncodes.insert({
    'name': 'Almonds Badam(ALMND)',
    'hsncode':  '08021100',
    'description': 'Almonds(in shell)',
    'CGST':0,
    'SGST':0,
    'IGST':0
});
db.categories.update({
    'name': 'Almonds (Badam)',
    'code': 'ALMND',
    'type': 'SubCategory2'
}, {$addToSet: {'hsncodes':{ hsncode: db.hsncodes.findOne({'hsncode': '08021100'})._id}}});



db.hsncodes.insert({
    'name': 'Almond (Badam)(ALMND)',
    'hsncode': '08021200',
    'description': 'Almonds(shelled)',
    'CGST':0,
    'SGST':0,
    'IGST':0
});
db.categories.update({
    'name': 'Almonds (Badam)',
    'code': 'ALMND',
    'type': 'SubCategory2'
}, {$addToSet: {'hsncodes':{ hsncode: db.hsncodes.findOne({'hsncode': '08021200'})._id}}});


db.hsncodes.insert({
    'name': 'Anjeer (Fig) (ANJER)',
    'hsncode': '08042010',
    'description': 'Anjeer(Fresh or dried)',
    'CGST':0,
    'SGST':0,
    'IGST':0
});
db.categories.update({
    'name': 'Anjeer (Fig)',
    'code': 'ANJER',
    'type': 'SubCategory2'
}, {$addToSet: {'hsncodes':{ hsncode: db.hsncodes.findOne({'hsncode': '08042010'})._id}}});



db.hsncodes.insert({
    'name': 'Apricot (APRCT)',
    'hsncode': '08091000',
    'description': 'Apricot',
    'CGST':0,
    'SGST':0,
    'IGST':0
});
db.categories.update({
    'name': 'Apricot',
    'code': 'APRCT',
    'type': 'SubCategory2'
}, {$addToSet: {'hsncodes':{ hsncode: db.hsncodes.findOne({'hsncode': '08091000'})._id}}});




db.hsncodes.insert({
    'name': 'Biscuits and cookies(salt)(BISCK)',
    'hsncode': '19059020',
    'description': 'Biscuits and Cookies(salt)',
    'CGST':0,
    'SGST':0,
    'IGST':0
});
db.categories.update({
    'name': 'Biscuits and Cookies',
    'code': 'BISCK',
    'type': 'SubCategory2'
}, {$addToSet: {'hsncodes':{ hsncode: db.hsncodes.findOne({'hsncode': '19059020'})._id}}});



db.hsncodes.insert({
    'name': 'Biscuits and Cookies(BISCK)',
    'hsncode': '19053100',
    'description': 'Biscuits and cookies(sweet)',
    'CGST':0,
    'SGST':0,
    'IGST':0
});
db.categories.update({
    'name': 'Biscuits and Cookies',
    'code': 'BISCK',
    'type': 'SubCategory2'
}, {$addToSet: {'hsncodes':{ hsncode: db.hsncodes.findOne({'hsncode': '19053100'})._id}}});



db.hsncodes.insert({
    'name': 'Candies and Chocolates(CANCH)',
    'hsncode': '17049090',
    'description': 'Candy- Duty Free Shop',
    'CGST':0,
    'SGST':0,
    'IGST':0
});
db.categories.update({
    'name': 'Candies and Chocolates',
    'code': 'CANCH',
    'type': 'SubCategory2'
}, {$addToSet: {'hsncodes':{ hsncode: db.hsncodes.findOne({'hsncode': '17049090'})._id}}});



db.hsncodes.insert({
    'name': 'Candies and Chocolates(CANCH)',
    'hsncode': '18069090',
    'description': 'Candy- Not For Sale At Local Market',
    'CGST':0,
    'SGST':0,
    'IGST':0
});
db.categories.update({
    'name': 'Candies and Chocolates',
    'code': 'CANCH',
    'type': 'SubCategory2'
}, {$addToSet: {'hsncodes':{ hsncode: db.hsncodes.findOne({'hsncode': '18069090'})._id}}});



db.hsncodes.insert({
    'name': 'Cashew (CASHW)',
    'hsncode': '08013100',
    'description': 'Cashew',
    'CGST':0,
    'SGST':0,
    'IGST':0
});
db.categories.update({
    'name': 'Cashew',
    'code':'CASHW',
    'type': 'SubCategory2'
}, {$addToSet: {'hsncodes':{ hsncode: db.hsncodes.findOne({'name':'Cashew (CASHW)','hsncode': '08013100'})._id}}});



db.hsncodes.insert({
    'name': 'Corn Flakes(CORNF)',
    'hsncode': '19041010',
    'description': 'Corn Flakes',
    'CGST':0,
    'SGST':0,
    'IGST':0
});
db.categories.update({
    'name': 'Corn Flakes',
    'code':'CORNF',
    'type': 'SubCategory2'
}, {$addToSet: {'hsncodes':{ hsncode: db.hsncodes.findOne({'name':'Corn Flakes(CORNF)','hsncode': '19041010'})._id}}});


db.hsncodes.insert({
    'name': 'Dairy(DAIRY)',
    'hsncode': '23099010',
    'description': 'Dairy',
    'CGST':0,
    'SGST':0,
    'IGST':0
});
db.categories.update({
    'name': 'Dairy',
    'code':'DAIRY',
    'type': 'SubCategory2'
}, {$addToSet: {'hsncodes':{ hsncode: db.hsncodes.findOne({'hsncode': '23099010'})._id}}});



db.hsncodes.insert({
    'name': 'Dates (DATES)',
    'hsncode': '08041020',
    'description': 'Dates',
    'CGST':0,
    'SGST':0,
    'IGST':0
});
db.categories.update({
    'name': 'Dates',
    'code':'DATES',
    'type': 'SubCategory2'
}, {$addToSet: {'hsncodes':{ hsncode: db.hsncodes.findOne({'hsncode': '08041020'})._id}}});




db.hsncodes.insert({
    'name': 'Dry grapes(DGRPS)',
    'hsncode': '08062010',
    'description': 'Dry Grapes',
    'CGST':0,
    'SGST':0,
    'IGST':0
});
db.categories.update({
    'name': 'Dry Grapes (Raisins)',
    'code':'DGRPS',
    'type': 'SubCategory2'
}, {$addToSet: {'hsncodes':{ hsncode: db.hsncodes.findOne({'hsncode': '08062010'})._id}}});



db.hsncodes.insert({
    'name': 'Flours (FLOUR)',
    'hsncode': '11021000',
    'description': 'Flours(Rye flour)',
    'CGST':0,
    'SGST':0,
    'IGST':0
});
db.categories.update({
    'name': 'Flours',
    'code':'FLOUR',
    'type': 'SubCategory2'
}, {$addToSet: {'hsncodes':{ hsncode: db.hsncodes.findOne({'hsncode': '11021000'})._id}}});



db.hsncodes.insert({
    'name': 'Flours(FLOUR)',
    'hsncode': '11022000',
    'description': 'MAIZE(Corn flour)',
    'CGST':0,
    'SGST':0,
    'IGST':0
});
db.categories.update({
    'name': 'Flours',
    'code':'FLOUR',
    'type': 'SubCategory2'
}, {$addToSet: {'hsncodes':{ hsncode: db.hsncodes.findOne({'hsncode': '11022000'})._id}}});



db.hsncodes.insert({
    'name': 'Flours (FLOUR)',
    'hsncode': '11023000',
    'description': 'RICE flour',
    'CGST':0,
    'SGST':0,
    'IGST':0
});
db.categories.update({
    'name': 'Flours',
    'code':'FLOUR',
    'type': 'SubCategory2'
}, {$addToSet: {'hsncodes':{ hsncode: db.hsncodes.findOne({'hsncode': '11023000'})._id}}});




db.hsncodes.insert({
    'name': 'Ice Creams(ICCRM)',
    'hsncode': '21050000',
    'description': 'Ice Creams',
    'CGST':0,
    'SGST':0,
    'IGST':0
});
db.categories.update({
    'name': 'Ice Creams',
    'code':'ICCRM',
    'type': 'SubCategory2'
}, {$addToSet: {'hsncodes':{ hsncode: db.hsncodes.findOne({'hsncode': '21050000'})._id}}});


db.hsncodes.insert({
    'name': 'Jaggery (JAGRY)',
    'hsncode': '17049030',
    'description': 'Jaggery powder and Cubes',
    'CGST':0,
    'SGST':0,
    'IGST':0
});
db.categories.update({
    'name': 'Jaggery',
    'code':'JAGRY',
    'type': 'SubCategory2'
}, {$addToSet: {'hsncodes':{ hsncode: db.hsncodes.findOne({'name':'Jaggery (JAGRY)','hsncode':'17049030'})._id}}});



db.hsncodes.insert({
    'name': 'Jams(JAMSS)',
    'hsncode': '20071000',
    'description': 'Jams(Homogenised Preparations)',
    'CGST':0,
    'SGST':0,
    'IGST':0
});
db.categories.update({
    'name': 'Jams',
    'code':'JAMSS',
    'type': 'SubCategory2'
}, {$addToSet: {'hsncodes':{ hsncode: db.hsncodes.findOne({'hsncode': '20071000'})._id}}});



db.hsncodes.insert({
    'name': 'Jams(JAMSS)',
    'hsncode': '20079100',
    'description': 'Jams(Citrus Fruit)',
    'CGST':0,
    'SGST':0,
    'IGST':0
});
db.categories.update({
    'name': 'Jams',
    'code':'JAMSS',
    'type': 'SubCategory2'
}, {$addToSet: {'hsncodes':{ hsncode: db.hsncodes.findOne({'hsncode': '20079100'})._id}}});


db.hsncodes.insert({
    'name': 'Jams(JAMSS)',
    'hsncode': '20079910',
    'description': 'Jams(Mango)',
    'CGST':0,
    'SGST':0,
    'IGST':0
});
db.categories.update({
    'name': 'Jams',
    'code':'JAMSS',
    'type': 'SubCategory2'
}, {$addToSet: {'hsncodes':{ hsncode: db.hsncodes.findOne({'hsncode': '20079910'})._id}}});


db.hsncodes.insert({
    'name': 'Jams(JAMSS)',
    'hsncode': '20079920',
    'description': 'Jams(Guava)',
    'CGST':0,
    'SGST':0,
    'IGST':0
});
db.categories.update({
    'name': 'Jams',
    'code':'JAMSS',
    'type': 'SubCategory2'
}, {$addToSet: {'hsncodes':{ hsncode: db.hsncodes.findOne({'hsncode': '20079920'})._id}}});


db.hsncodes.insert({
    'name': 'Jams(JAMSS)',
    'hsncode': '20079930',
    'description': 'Jams(Pine Apple)',
    'CGST':0,
    'SGST':0,
    'IGST':0
});
db.categories.update({
    'name': 'Jams',
    'code':'JAMSS',
    'type': 'SubCategory2'
}, {$addToSet: {'hsncodes':{ hsncode: db.hsncodes.findOne({'hsncode': '20079930'})._id}}});



db.hsncodes.insert({
    'name': 'Jams(JAMSS)',
    'hsncode': '20079940',
    'description': 'Jams(Apple)',
    'CGST':0,
    'SGST':0,
    'IGST':0
});
db.categories.update({
    'name': 'Jams',
    'code':'JAMSS',
    'type': 'SubCategory2'
}, {$addToSet: {'hsncodes':{ hsncode: db.hsncodes.findOne({'hsncode': '20079940'})._id}}});



db.hsncodes.insert({
    'name': 'Oats(OATSS)',
    'hsncode': '10040090',
    'description': 'Oats',
    'CGST':0,
    'SGST':0,
    'IGST':0
});
db.categories.update({
    'name': 'Oats',
    'code':'OATSS',
    'type': 'SubCategory2'
}, {$addToSet: {'hsncodes':{ hsncode: db.hsncodes.findOne({'hsncode': '10040090'})._id}}});



db.hsncodes.insert({
    'name': 'Namkeen (NAMKN)',
    'hsncode': '21069099',
    'description': 'Namkeen',
    'CGST':0,
    'SGST':0,
    'IGST':0
});
db.categories.update({
    'name': 'Namkeen',
    'code':'NAMKN',
    'type': 'SubCategory2'
}, {$addToSet: {'hsncodes':{ hsncode: db.hsncodes.findOne({'name':'Namkeen (NAMKN)','hsncode':'21069099'})._id}}});



db.hsncodes.insert({
    'name': 'Noodles(NOODL)',
    'hsncode': '19020000',
    'description': 'Noodles',
    'CGST':0,
    'SGST':0,
    'IGST':0
});
db.categories.update({
    'name': 'Noodles',
    'code':'NOODL',
    'type': 'SubCategory2'
}, {$addToSet: {'hsncodes':{ hsncode: db.hsncodes.findOne({'hsncode': '19020000'})._id}}});



db.hsncodes.insert({
    'name': 'Sugar (SUGAR)',
    'hsncode': '17011120',
    'description': 'Sugar',
    'CGST':0,
    'SGST':0,
    'IGST':0
});
db.categories.update({
    'name': 'Sugar',
    'code':'SUGAR',
    'type': 'SubCategory2'
}, {$addToSet: {'hsncodes':{ hsncode: db.hsncodes.findOne({'name':'Sugar (SUGAR)','hsncode': '17011120'})._id}}});



db.hsncodes.insert({
    'name': 'Papads (PAPAD)',
    'hsncode': '17011120',
    'description': 'Papads',
    'CGST':0,
    'SGST':0,
    'IGST':0
});
db.categories.update({
    'name': 'Papads',
    'code':'PAPAD',
    'type': 'SubCategory2'
}, {$addToSet: {'hsncodes':{ hsncode: db.hsncodes.findOne({'name':'Papads (PAPAD)','hsncode':'17011120'})._id}}});



db.hsncodes.insert({
    'name': 'Walnuts(WLNTS)',
    'hsncode': '08023200',
    'description': 'walnuts',
    'CGST':0,
    'SGST':0,
    'IGST':0
});
db.categories.update({
    'name': 'Walnuts',
    'code':'WLNTS',
    'type': 'SubCategory2'
}, {$addToSet: {'hsncodes':{ hsncode: db.hsncodes.findOne({'hsncode': '08023200'})._id}}});


db.hsncodes.insert({
    'name': 'Pistachios(PSTCH)',
    'hsncode': '08025200',
    'description': 'pistachios',
    'CGST':0,
    'SGST':0,
    'IGST':0
});
db.categories.update({
    'name': 'Pistachios',
    'code': 'PSTCH',
    'type': 'SubCategory2'
}, {$addToSet: {'hsncodes':{ hsncode: db.hsncodes.findOne({'hsncode': '08025200'})._id}}});


db.hsncodes.insert({
    'name': 'Prunes(PRUNS)',
    'hsncode': '08132000',
    'description': 'prunes',
    'CGST':0,
    'SGST':0,
    'IGST':0
});
db.categories.update({
    'name': 'Prunes',
    'code': 'PRUNS',
    'type': 'SubCategory2'
}, {$addToSet: {'hsncodes':{ hsncode: db.hsncodes.findOne({'hsncode': '08132000'})._id}}});


db.hsncodes.insert({
    'name': 'Sauces(SAUCE)',
    'hsncode': '21031000',
    'description': 'SOYA Sauce',
    'CGST':0,
    'SGST':0,
    'IGST':0
});
db.categories.update({
    'name': 'Sauces',
    'code': 'SAUCE',
    'type': 'SubCategory2'
}, {$addToSet: {'hsncodes':{ hsncode: db.hsncodes.findOne({'hsncode': '21031000'})._id}}});


db.hsncodes.insert({
    'name': 'Sauces (SAUCE)',
    'hsncode': '21039020',
    'description': 'CHILLI sauce',
    'CGST':0,
    'SGST':0,
    'IGST':0
});
db.categories.update({
    'name': 'Sauces',
    'code': 'SAUCE',
    'type': 'SubCategory2'
}, {$addToSet: {'hsncodes':{ hsncode: db.hsncodes.findOne({'hsncode': '21039020'})._id}}});



db.hsncodes.insert({
    'name': 'Sauces(SAUCE)',
    'hsncode': '21032000',
    'description': 'TOMATO sauce',
    'CGST':0,
    'SGST':0,
    'IGST':0
});
db.categories.update({
    'name': 'Sauces',
    'code': 'SAUCE',
    'type': 'SubCategory2'
}, {$addToSet: {'hsncodes':{ hsncode: db.hsncodes.findOne({'hsncode': '21032000'})._id}}});



db.hsncodes.insert({
    'name': 'Sauces(SAUCE)',
    'hsncode': '21039010',
    'description': 'Curry paste',
    'CGST':0,
    'SGST':0,
    'IGST':0
});
db.categories.update({
    'name': 'Sauces',
    'code': 'SAUCE',
    'type': 'SubCategory2'
}, {$addToSet: {'hsncodes':{ hsncode: db.hsncodes.findOne({'hsncode': '21039010'})._id}}});



db.hsncodes.insert({
    'name': 'Pickles(PICKL)',
    'hsncode': '20010000',
    'description': 'pickles',
    'CGST':0,
    'SGST':0,
    'IGST':0
});
db.categories.update({
    'name': 'Pickles',
    'code': 'PICKL',
    'type': 'SubCategory2'
}, {$addToSet: {'hsncodes':{ hsncode: db.hsncodes.findOne({'hsncode': '20010000'})._id}}});



db.hsncodes.insert({
    'name': 'Wheat Atta (WATTA)',
    'hsncode': '11010000',
    'description': 'Wheat atta',
    'CGST':0,
    'SGST':0,
    'IGST':0
});
db.categories.update({
    'name': 'Wheat Atta',
    'code': 'WATTA',
    'type': 'SubCategory2'
}, {$addToSet: {'hsncodes':{ hsncode: db.hsncodes.findOne({'name':'Wheat Atta (WATTA)','hsncode': '11010000'})._id}}});



db.hsncodes.insert({
    'name': 'Sooji and Dalia (SOOJI)',
    'hsncode': '11010000',
    'description': 'Sooji and Dalia',
    'CGST':0,
    'SGST':0,
    'IGST':0
});
db.categories.update({
    'name': 'Sooji and Dalia',
    'code': 'SOOJI',
    'type': 'SubCategory2'
}, {$addToSet: {'hsncodes':{ hsncode: db.hsncodes.findOne({'name':'Sooji and Dalia (SOOJI)','hsncode': '11010000'})._id}}});



db.hsncodes.insert({
    'name': 'Other Dry Fruits (ODFRT)',
    'hsncode': '11010000',
    'description': 'Other dry fruits',
    'CGST':0,
    'SGST':0,
    'IGST':0
});
db.categories.update({
    'name': 'Other Dry Fruits',
    'code': 'ODFRT',
    'type': 'SubCategory2'
}, {$addToSet: {'hsncodes':{ hsncode: db.hsncodes.findOne({'name':'Other Dry Fruits (ODFRT)','hsncode': '11010000'})._id}}});



db.hsncodes.insert({
    'name': 'Masala and Spices(MSPIC)',
    'hsncode': '09030000',
    'description': 'Masalas and spices(mate)',
    'CGST':0,
    'SGST':0,
    'IGST':0
});
db.categories.update({
    'name': 'Masala and Spices',
    'code': 'MSPIC',
    'type': 'SubCategory2'
}, {$addToSet: {'hsncodes':{ hsncode: db.hsncodes.findOne({'hsncode': '09030000'})._id}}});




db.hsncodes.insert({
    'name': 'masalas and spices(MSPIC)',
    'hsncode': '09042020',
    'description': 'Masalas and spices(chilli powder)',
    'CGST':0,
    'SGST':0,
    'IGST':0
});
db.categories.update({
    'name': 'Masalas and Spices',
    'code': 'MSPIC',
    'type': 'SubCategory2'
}, {$addToSet: {'hsncodes':{ hsncode: db.hsncodes.findOne({'hsncode': '09042020'})._id}}});



db.hsncodes.insert({
    'name': 'Masalas and Spices(MSPIC)',
    'hsncode': '09109100',
    'description': 'Masalas and spices(curry powder)',
    'CGST':0,
    'SGST':0,
    'IGST':0
});
db.categories.update({
    'name': 'Masalas and Spices',
    'code': 'MSPIC',
    'type': 'SubCategory2'
}, {$addToSet: {'hsncodes':{ hsncode: db.hsncodes.findOne({'hsncode': '09109100'})._id}}});



db.hsncodes.insert({
    'name': 'Masalas and Spices(MSPIC)',
    'hsncode': '09109911',
    'description': 'Masalas and spices(celery)',
    'CGST':0,
    'SGST':0,
    'IGST':0
});
db.categories.update({
    'name': 'Masalas and Spices',
    'code': 'MSPIC',
    'type': 'SubCategory2'
}, {$addToSet: {'hsncodes':{ hsncode: db.hsncodes.findOne({'hsncode': '09109911'})._id}}});


db.hsncodes.insert({
    'name': 'Masalas and spices(MSPIC)',
    'hsncode': '09109912',
    'description': 'Fenugreek',
    'CGST':0,
    'SGST':0,
    'IGST':0
});
db.categories.update({
    'name': 'Masala and Spices',
    'code': 'MSPIC',
    'type': 'SubCategory2'
}, {$addToSet: {'hsncodes':{ hsncode: db.hsncodes.findOne({'hsncode': '09109912'})._id}}});



db.hsncodes.insert({
    'name': 'Masalas and Spices(MSPIC)',
    'hsncode': '09109913',
    'description': 'Dill',
    'CGST':0,
    'SGST':0,
    'IGST':0
});
db.categories.update({
    'name': 'Masalas and Spices',
    'code': 'MSPIC',
    'type': 'SubCategory2'
}, {$addToSet: {'hsncodes':{ hsncode: db.hsncodes.findOne({'hsncode': '09109913'})._id}}});




db.hsncodes.insert({
    'name': 'Masalas and Spices(MSPIC)',
    'hsncode': '09109914',
    'description': 'Ajwain',
    'CGST':0,
    'SGST':0,
    'IGST':0
});
db.categories.update({
    'name': 'Masala and Spices',
    'code': 'MSPIC',
    'type': 'SubCategory2'
}, {$addToSet: {'hsncodes':{ hsncode: db.hsncodes.findOne({'hsncode': '09109914'})._id}}});



db.hsncodes.insert({
    'name': 'Masalas and Spices(MSPIC)',
    'hsncode': '09109922',
    'description': 'Cumin powder',
    'CGST':0,
    'SGST':0,
    'IGST':0
});
db.categories.update({
    'name': 'Masalas and Spices',
    'code': 'MSPIC',
    'type': 'SubCategory2'
}, {$addToSet: {'hsncodes':{ hsncode: db.hsncodes.findOne({'hsncode': '09109922'})._id}}});


db.hsncodes.insert({
    'name': 'Masala and Spices(MSPIC)',
    'hsncode': '09109923',
    'description': 'Celery powder',
    'CGST':0,
    'SGST':0,
    'IGST':0
});
db.categories.update({
    'name': 'Masalas and Spices',
    'code': 'MSPIC',
    'type': 'SubCategory2'
}, {$addToSet: {'hsncodes':{ hsncode: db.hsncodes.findOne({'hsncode': '09109923'})._id}}});





db.hsncodes.insert({
    'name': 'Masala and spices(MSPIC)',
    'hsncode': '09109925',
    'description': 'Dill powder',
    'CGST':0,
    'SGST':0,
    'IGST':0
});
db.categories.update({
    'name': 'Masalas  and Spices',
    'code': 'MSPIC',
    'type': 'SubCategory2'
}, {$addToSet: {'hsncodes':{ hsncode: db.hsncodes.findOne({'hsncode': '09109925'})._id}}});




db.hsncodes.insert({
    'name': 'Masalas and Spices(MSPIC)',
    'hsncode': '09109926',
    'description': 'Poppy powder',
    'CGST':0,
    'SGST':0,
    'IGST':0
});
db.categories.update({
    'name': 'Masalas and Spices',
    'code': 'MSPIC',
    'type': 'SubCategory2'
}, {$addToSet: {'hsncodes':{ hsncode: db.hsncodes.findOne({'hsncode': '09109926'})._id}}});



db.hsncodes.insert({
    'name': 'Masalas and Spices(MSPIC)',
    'hsncode': '09109927',
    'description': 'Mustard powder',
    'CGST':0,
    'SGST':0,
    'IGST':0
});
db.categories.update({
    'name': 'Masalas and Spices',
    'code': 'MSPIC',
    'type': 'SubCategory2'
}, {$addToSet: {'hsncodes':{ hsncode: db.hsncodes.findOne({'hsncode': '09109927'})._id}}});



db.hsncodes.insert({
    'name': 'Oils(FOILS)',
    'hsncode': '15141900',
    'description': 'Mustard Oil and Refined',
    'CGST':0,
    'SGST':0,
    'IGST':0
});
db.categories.update({
    'name': 'Oils',
    'code': 'FOILS',
    'type': 'SubCategory2'
}, {$addToSet: {'hsncodes':{ hsncode: db.hsncodes.findOne({'hsncode': '15141900'})._id}}});



db.hsncodes.insert({
    'name': 'Oils(FOILS)',
    'hsncode': '15089091',
    'description': 'Ground Nut Oil and Refined',
    'CGST':0,
    'SGST':0,
    'IGST':0
});
db.categories.update({
    'name': 'Oils',
    'code': 'FOILS',
    'type': 'SubCategory2'
}, {$addToSet: {'hsncodes':{ hsncode: db.hsncodes.findOne({'hsncode': '15089091'})._id}}});




db.hsncodes.insert({
    'name': 'Oils(FOILS)',
    'hsncode': '15121910',
    'description': 'Sunflower Oil and Refined',
    'CGST':0,
    'SGST':0,
    'IGST':0
});
db.categories.update({
    'name': 'Oils',
    'code': 'FOILS',
    'type': 'SubCategory2'
}, {$addToSet: {'hsncodes':{ hsncode: db.hsncodes.findOne({'hsncode': '15121910'})._id}}});




db.hsncodes.insert({
    'name': 'Oils(FOILS)',
    'hsncode': '15079010',
    'description': 'Soya bean Oil and Refined',
    'CGST':0,
    'SGST':0,
    'IGST':0
});
db.categories.update({
    'name': 'Oils',
    'code': 'FOILS',
    'type': 'SubCategory2'
}, {$addToSet: {'hsncodes':{ hsncode: db.hsncodes.findOne({'hsncode': '15079010'})._id}}});



db.hsncodes.insert({
    'name': 'Oils(FOILS)',
    'hsncode': '15110000',
    'description': 'Palm Oil and Refined',
    'CGST':0,
    'SGST':0,
    'IGST':0
});
db.categories.update({
    'name': 'Oils',
    'code': 'FOILS',
    'type': 'SubCategory2'
}, {$addToSet: {'hsncodes':{ hsncode: db.hsncodes.findOne({'hsncode': '15110000'})._id}}});




db.hsncodes.insert({
    'name': 'Oils(FOILS)',
    'hsncode': '15130000',
    'description': 'Coconut Oil and Refined',
    'CGST':0,
    'SGST':0,
    'IGST':0
});
db.categories.update({
    'name': 'Oils',
    'code': 'FOILS',
    'type': 'SubCategory2'
}, {$addToSet: {'hsncodes':{ hsncode: db.hsncodes.findOne({'hsncode': '15130000'})._id}}});




db.hsncodes.insert({
    'name': 'Oils(FOILS)',
    'hsncode': '15170000',
    'description': 'Margrine oil',
    'CGST':0,
    'SGST':0,
    'IGST':0
});
db.categories.update({
    'name': 'Oils',
    'code': 'FOILS',
    'type': 'SubCategory2'
}, {$addToSet: {'hsncodes':{ hsncode: db.hsncodes.findOne({'hsncode': '15170000'})._id}}});


db.hsncodes.insert({
    'name': 'Oils(FOILS)',
    'hsncode': '15099010',
    'description': 'olive oil ',
    'CGST':0,
    'SGST':0,
    'IGST':0
});
db.categories.update({
    'name': 'Oils',
    'code': 'FOILS',
    'type': 'SubCategory2'
}, {$addToSet: {'hsncodes':{ hsncode: db.hsncodes.findOne({'hsncode': '15099010'})._id}}});



db.hsncodes.insert({
    'name': 'Oils(FOILS)',
    'hsncode': '15162000',
    'description': 'vanaspati',
    'CGST':0,
    'SGST':0,
    'IGST':0
});
db.categories.update({
    'name': 'Oils',
    'code': 'FOILS',
    'type': 'SubCategory2'
}, {$addToSet: {'hsncodes':{ hsncode: db.hsncodes.findOne({'hsncode': '15162000'})._id}}});



db.hsncodes.insert({
    'name': 'Sweets (SWEET)',
    'hsncode': '17049020',
    'description': 'sweets(boiled)',
    'CGST':0,
    'SGST':0,
    'IGST':0
});
db.categories.update({
    'name': 'Sweets',
    'code': 'SWEET',
    'type': 'SubCategory2'
}, {$addToSet: {'hsncodes':{ hsncode: db.hsncodes.findOne({'hsncode': '17049020'})._id}}});



db.hsncodes.insert({
    'name': 'Sweets (SWEET)',
    'hsncode': '17049030',
    'description': 'sweets(Toffees, Caramels And Similar Sweets)',
    'CGST':0,
    'SGST':0,
    'IGST':0
});
db.categories.update({
    'name': 'Sweets',
    'code': 'SWEET',
    'type': 'SubCategory2'
}, {$addToSet: {'hsncodes':{ hsncode: db.hsncodes.findOne({'hsncode': '17049030'})._id}}});



db.hsncodes.insert({
    'name': 'Honey (HONEY)',
    'hsncode': '04090000',
    'description': 'Honey',
    'CGST':0,
    'SGST':0,
    'IGST':0
});
db.categories.update({
    'name': 'Honey',
    'code': 'HONEY',
    'type': 'SubCategory2'
}, {$addToSet: {'hsncodes':{ hsncode: db.hsncodes.findOne({'hsncode': '04090000'})._id}}});




db.hsncodes.insert({
    'name': 'Health Drinks(HDRNK)',
    'hsncode': '19011090',
    'description': 'Health drink',
    'CGST':0,
    'SGST':0,
    'IGST':0
});
db.categories.update({
    'name': 'Health Drinks',
    'code': 'HDRNK',
    'type': 'SubCategory2'
}, {$addToSet: {'hsncodes':{ hsncode: db.hsncodes.findOne({'hsncode': '19011090'})._id}}});


db.hsncodes.insert({
    'name': 'Digestives(DGSTV)',
    'hsncode': '39209939',
    'description': 'Digestives(parle)',
    'CGST':0,
    'SGST':0,
    'IGST':0
});
db.categories.update({
    'name': 'Digestives',
    'code': 'DGSTV',
    'type': 'SubCategory2'
}, {$addToSet: {'hsncodes':{ hsncode: db.hsncodes.findOne({'hsncode': '39209939'})._id}}});


db.hsncodes.insert({
    'name': 'Digestives(DGSTV)',
    'hsncode': '30049011',
    'description': 'Digestives(Medicants of Ayurvedic)',
    'CGST':0,
    'SGST':0,
    'IGST':0
});
db.categories.update({
    'name': 'Digestives',
    'code': 'DGSTV',
    'type': 'SubCategory2'
}, {$addToSet: {'hsncodes':{ hsncode: db.hsncodes.findOne({'hsncode': '30049011'})._id}}});



db.hsncodes.insert({
    'name': 'Digestives (DGSTV)',
    'hsncode': '21069099',
    'description': 'Digestives(Satmola)',
    'CGST':0,
    'SGST':0,
    'IGST':0
});
db.categories.update({
    'name': 'Digestives',
    'code': 'DGSTV',
    'type': 'SubCategory2'
}, {$addToSet: {'hsncodes':{ hsncode: db.hsncodes.findOne({'name':'Digestives (DGSTV)','hsncode': '21069099'})._id}}});


db.hsncodes.insert({
    'name': 'Health Wellness (HWELL)',
    'hsncode': '49011010',
    'description': 'Health and Wellness',
    'CGST':0,
    'SGST':0,
    'IGST':0
});
db.categories.update({
    'name': 'Health and Wellness',
    'code': 'HWELL',
    'type': 'SubCategory2'
}, {$addToSet: {'hsncodes':{ hsncode: db.hsncodes.findOne({'hsncode': '49011010'})._id}}});




db.hsncodes.insert({
    'name': 'Immune Enhancers (IMMUN)',
    'hsncode': '30042019',
    'description': 'Immune enhancers(pieces)',
    'CGST':0,
    'SGST':0,
    'IGST':0
});
db.categories.update({
    'name': 'Immune Enhancers',
    'code': 'IMMUN',
    'type': 'SubCategory2'
}, {$addToSet: {'hsncodes':{ hsncode: db.hsncodes.findOne({'hsncode': '30042019'})._id}}});


db.hsncodes.insert({
    'name': 'Immune Enhancers(IMMUN)',
    'hsncode': '30039011',
    'description': 'Immune enhancers(boxes)',
    'CGST':0,
    'SGST':0,
    'IGST':0
});
db.categories.update({
    'name': 'Immune Enhancers',
    'code': 'IMMUN',
    'type': 'SubCategory2'
}, {$addToSet: {'hsncodes':{ hsncode: db.hsncodes.findOne({'hsncode': '30039011'})._id}}});




db.hsncodes.insert({
    'name': 'Hair Care(HAIRC)',
    'hsncode': '33051010',
    'description': 'Hair care(Containing spirt)',
    'CGST':0,
    'SGST':0,
    'IGST':0
});
db.categories.update({
    'name': 'Hair Care',
    'code': 'HAIRC',
    'type': 'SubCategory2'
}, {$addToSet: {'hsncodes':{ hsncode: db.hsncodes.findOne({'hsncode': '33051010'})._id}}});




db.hsncodes.insert({
    'name': 'Hair Care(HAIRC)',
    'hsncode': '33052000',
    'description': 'Hair care(Preparations For Permanent Waving Or Straightening)',
    'CGST':0,
    'SGST':0,
    'IGST':0
});
db.categories.update({
    'name': 'Hair Care',
    'code': 'HAIRC',
    'type': 'SubCategory2'
}, {$addToSet: {'hsncodes':{ hsncode: db.hsncodes.findOne({'hsncode': '33052000'})._id}}});



db.hsncodes.insert({
    'name': 'Hair Care (HAIRC)',
    'hsncode': '33053000',
    'description': 'Hair care (Hair lacquers)',
    'CGST':0,
    'SGST':0,
    'IGST':0
});
db.categories.update({
    'name': 'Hair Care',
    'code': 'HAIRC',
    'type': 'SubCategory2'
}, {$addToSet: {'hsncodes':{ hsncode: db.hsncodes.findOne({'hsncode': '33053000'})._id}}});




db.hsncodes.insert({
    'name': 'Hair Care(HAIRC)',
    'hsncode': '33059011',
    'description': 'Hair care(Perfumed Hair Oil)',
    'CGST':0,
    'SGST':0,
    'IGST':0
});
db.categories.update({
    'name': 'Hair Care',
    'code': 'HAIRC',
    'type': 'SubCategory2'
}, {$addToSet: {'hsncodes':{ hsncode: db.hsncodes.findOne({'hsncode': '33059011'})._id}}});



db.hsncodes.insert({
    'name': 'Hair Care(HAIRC)',
    'hsncode': '33059020',
    'description': 'Hair care(Brilliantines(spirituous))',
    'CGST':0,
    'SGST':0,
    'IGST':0
});
db.categories.update({
    'name': 'Hair Care',
    'code': 'HAIRC',
    'type': 'SubCategory2'
}, {$addToSet: {'hsncodes':{ hsncode: db.hsncodes.findOne({'hsncode': '33059020'})._id}}});




db.hsncodes.insert({
    'name': 'Hair Care(HAIRC)',
    'hsncode': '33059030',
    'description': 'Hair care(Haircreams)',
    'CGST':0,
    'SGST':0,
    'IGST':0
});
db.categories.update({
    'name': 'Hair Care',
    'code': 'HAIRC',
    'type': 'SubCategory2'
}, {$addToSet: {'hsncodes':{ hsncode: db.hsncodes.findOne({'hsncode': '33059030'})._id}}});




db.hsncodes.insert({
    'name': 'Hair Care(HAIRC)',
    'hsncode': '33059040',
    'description': 'Hair care(Hairdyes)',
    'CGST':0,
    'SGST':0,
    'IGST':0
});
db.categories.update({
    'name': 'Hair Care',
    'code': 'HAIRC',
    'type': 'SubCategory2'
}, {$addToSet: {'hsncodes':{ hsncode: db.hsncodes.findOne({'hsncode': '33059040'})._id}}});




db.hsncodes.insert({
    'name': 'Perfumes and Deodrants(PERDT)',
    'hsncode': '33030040',
    'description': 'Perfumes and Deodrants(Perfumes And Perfumery Compounds Containing Spirit (excluding Aqueous))',
    'CGST':0,
    'SGST':0,
    'IGST':0
});
db.categories.update({
    'name': 'Perfumes and Deodrants',
    'code': 'PERDT',
    'type': 'SubCategory2'
}, {$addToSet: {'hsncodes':{ hsncode: db.hsncodes.findOne({'hsncode': '33030040'})._id}}});



db.hsncodes.insert({
    'name': 'Perfume and Deodrants(PERDT)',
    'hsncode': '33030050',
    'description': 'Perfumes and Deodrants(Perfumes Containing Spirit)',
    'CGST':0,
    'SGST':0,
    'IGST':0
});
db.categories.update({
    'name': 'Perfumes and Deodrants',
    'code': 'PERDT',
    'type': 'SubCategory2'
}, {$addToSet: {'hsncodes':{ hsncode: db.hsncodes.findOne({'hsncode': '33030050'})._id}}});



db.hsncodes.insert({
    'name': 'Detergents(DTGNT)',
    'hsncode': '34029012',
    'description': 'Detergents(Cleaning or degreasing preparations not having a basis of soap or other organic surface active agents)',
    'CGST':0,
    'SGST':0,
    'IGST':0
});
db.categories.update({
    'name': 'Detergents',
    'code': 'DTGNT',
    'type': 'SubCategory2'
}, {$addToSet: {'hsncodes':{ hsncode: db.hsncodes.findOne({'hsncode': '34029012'})._id}}});



db.hsncodes.insert({
    'name': 'Detergents(DTGNT)',
    'hsncode': '34029011',
    'description': 'Detergents(Cleaning or degreasing preparations having a basis of soap or other organic surface active agents)',
    'CGST':0,
    'SGST':0,
    'IGST':0
});
db.categories.update({
    'name': 'Detergents',
    'code': 'DTGNT',
    'type': 'SubCategory2'
}, {$addToSet: {'hsncodes':{ hsncode: db.hsncodes.findOne({'hsncode': '34029011'})._id}}});



db.hsncodes.insert({
    'name': 'Cotton Yarn(COTYN)',
    'hsncode': '520511',
    'description': 'cotton yarn((Uncombed Cotton 85% or More; 714.29 Decitex or More)',
    'CGST':0,
    'SGST':0,
    'IGST':0
});
db.categories.update({
    'name': 'Cotton Yarn',
    'code': 'COTYN',
    'type': 'SubCategory2'
}, {$addToSet: {'hsncodes':{ hsncode: db.hsncodes.findOne({'hsncode': '520511'})._id}}});




db.hsncodes.insert({
    'name': 'Cotton Yarn(COTYN)',
    'hsncode':'520512',
    'description': '(Cotton Yarn Uncombed Cotton 85% or More; 714.29-232.56decitex)',
    'CGST':0,
    'SGST':0,
    'IGST':0
});
db.categories.update({
    'name': 'Cotton Yarn',
    'code': 'COTYN',
    'type': 'SubCategory2'
}, {$addToSet: {'hsncodes':{ hsncode: db.hsncodes.findOne({'hsncode': '520512'})._id}}});



db.hsncodes.insert({
    'name': 'Cotton Yarn(COTYN)',
    'hsncode': '520513',
    'description': '(Cotton yarn Uncombed Cotton 85% or More; 232.56-192.31 Decitex(not sewing))',
    'CGST':0,
    'SGST':0,
    'IGST':0
});
db.categories.update({
    'name': 'Cotton Yarn',
    'code': 'COTYN',
    'type': 'SubCategory2'
}, {$addToSet: {'hsncodes':{ hsncode: db.hsncodes.findOne({'hsncode': '520513'})._id}}});


db.hsncodes.insert({
    'name': 'Cotton Yarn(COTYN)',
    'hsncode': '520514',
    'description': '(Cotton Yarn Uncombed Cotton 85% or More; 192.31-125 Decitex(not sewing))',
    'CGST':0,
    'SGST':0,
    'IGST':0
});
db.categories.update({
    'name': 'Cotton Yarn',
    'code': 'COTYN',
    'type': 'SubCategory2'
}, {$addToSet: {'hsncodes':{ hsncode: db.hsncodes.findOne({'hsncode': '520514'})._id}}});

db.hsncodes.insert({
    'name': 'Cotton Yarn(COTYN)',
    'hsncode': '520515',
    'description': '(Cotton Yarn Uncombed Cotton 85% or More; Less than 125 Decitex(not sewing))',
    'CGST':0,
    'SGST':0,
    'IGST':0
});
db.categories.update({
    'name': 'Cotton Yarn',
    'code': 'COTYN',
    'type': 'SubCategory2'
}, {$addToSet: {'hsncodes':{ hsncode: db.hsncodes.findOne({'hsncode': '520515'})._id}}});


db.hsncodes.insert({
    'name': 'Cotton Yarn(COTYN)',
    'hsncode': '520521',
    'description': '(Cotton Yarn Combed Cotton 85% or More; 714.29 Decitex or More(not sewing))',
    'CGST':0,
    'SGST':0,
    'IGST':0
});
db.categories.update({
    'name': 'Cotton Yarn',
    'code': 'COTYN',
    'type': 'SubCategory2'
}, {$addToSet: {'hsncodes':{ hsncode: db.hsncodes.findOne({'hsncode': '520521'})._id}}});


db.hsncodes.insert({
    'name': 'Cotton Yarn (COTYN)',
    'hsncode': '520522',
    'description': '(Cotton Yarn Combed Cotton 85% or More; 714.29-232.56 Decitex(not sewing))',
    'CGST':0,
    'SGST':0,
    'IGST':0
});
db.categories.update({
    'name': 'Cotton Yarn',
    'code': 'COTYN',
    'type': 'SubCategory2'
}, {$addToSet: {'hsncodes':{ hsncode: db.hsncodes.findOne({'hsncode': '520522'})._id}}});


db.hsncodes.insert({
    'name': 'Cotton Yarn(COTYN)',
    'hsncode': '520523',
    'description': '(Cotton Yarn Combed Cotton 85 or More; 232.56-192.31 Decitex(not sewing))',
    'CGST':0,
    'SGST':0,
    'IGST':0
});
db.categories.update({
    'name': 'Cotton Yarn',
    'code': 'COTYN',
    'type': 'SubCategory2'
}, {$addToSet: {'hsncodes':{ hsncode: db.hsncodes.findOne({'hsncode': '520523'})._id}}});


db.hsncodes.insert({
    'name': 'Cotton Yarn(COTYN)',
    'hsncode': '520524',
    'description': '(Cotton Yarn Combed Cotton 85% or More; 192.31-125 Decitex(not sewing))',
    'CGST':0,
    'SGST':0,
    'IGST':0
});
db.categories.update({
    'name': 'Cotton Yarn',
    'code': 'COTYN',
    'type': 'SubCategory2'
}, {$addToSet: {'hsncodes':{ hsncode: db.hsncodes.findOne({'hsncode':'520524'})._id}}});



db.hsncodes.insert({
    'name': 'Cotton Yarn(COTYN)',
    'hsncode': '520525',
    'description': '(Cotton Yarn Combed Cotton 85% or More; Less than 125 Decitex(not sewing))',
    'CGST':0,
    'SGST':0,
    'IGST':0
});
db.categories.update({
    'name': 'Cotton Yarn',
    'code': 'COTYN',
    'type': 'SubCategory2'
}, {$addToSet: {'hsncodes':{ hsncode: db.hsncodes.findOne({'hsncode': '520525'})._id}}});


db.hsncodes.insert({
    'name': 'Cotton yarn(COTYN)',
    'hsncode': '520531',
    'description': '(Cotton Yarn (Multiple Yarn, Per Single Yarn 714.29 Decitex or More(not sewing))',
    'CGST':0,
    'SGST':0,
    'IGST':0
});
db.categories.update({
    'name': 'Cotton Yarn',
    'code': 'COTYN',
    'type': 'SubCategory2'
}, {$addToSet: {'hsncodes':{ hsncode: db.hsncodes.findOne({'hsncode': '520531'})._id}}});




db.hsncodes.insert({
    'name': 'Cotton Yarn(COTYN)',
    'hsncode': '520532',
    'description': '(Cotton Yarn (Multiple Yarn, Per Single Yarn 714.29-232.56 Decitex(not sewing))',
    'CGST':0,
    'SGST':0,
    'IGST':0
});
db.categories.update({
    'name': 'Cotton Yarn',
    'code': 'COTYN',
    'type': 'SubCategory2'
}, {$addToSet: {'hsncodes':{ hsncode: db.hsncodes.findOne({'hsncode': '520532'})._id}}});


db.hsncodes.insert({
    'name': 'Cotton Yarn(COTYN)',
    'hsncode': '520533',
    'description': '(Cotton Yarn (Multiple Yarn, Per Single Yarn 714.29-232.56 Decitex)(not sewing))',
    'CGST':0,
    'SGST':0,
    'IGST':0
});
db.categories.update({
    'name': 'Cotton Yarn',
    'code': 'COTYN',
    'type': 'SubCategory2'
}, {$addToSet: {'hsncodes':{ hsncode: db.hsncodes.findOne({'hsncode': '520533'})._id}}});


db.hsncodes.insert({
    'name': 'Cotton Yarn(COYTN)',
    'hsncode': '520534',
    'description': '(Cotton Yarn (Multiple Yarn, Per Single Yarn 192.31~125 Decitex)(not sewing))',
    'CGST':0,
    'SGST':0,
    'IGST':0
});
db.categories.update({
    'name': 'Cotton Yarn',
    'code': 'COTYN',
    'type': 'SubCategory2'
}, {$addToSet: {'hsncodes':{ hsncode: db.hsncodes.findOne({'hsncode': '520534'})._id}}});

db.hsncodes.insert({
    'name': 'Cotton Yarn(COTYN)',
    'hsncode': '520535',
    'description': '(Cotton Yarn Multiple Yarn, Per Single Yarn Less than 125 Decitex(not sewing))',
    'CGST':0,
    'SGST':0,
    'IGST':0
});
db.categories.update({
    'name': 'Cotton Yarn',
    'code': 'COTYN',
    'type': 'SubCategory2'
}, {$addToSet: {'hsncodes':{ hsncode: db.hsncodes.findOne({'hsncode': '520535'})._id}}});


db.hsncodes.insert({
    'name': 'Cotton Yarn(COYTN)',
    'hsncode': '520541',
    'description': '(Cotton Yarn Multiple Yarn, Per Single Yarn 714.29 Decitex or More(not sewing))',
    'CGST':0,
    'SGST':0,
    'IGST':0
});
db.categories.update({
    'name': 'Cotton Yarn',
    'code': 'COTYN',
    'type': 'SubCategory2'
}, {$addToSet: {'hsncodes':{ hsncode: db.hsncodes.findOne({'hsncode': '520541'})._id}}});


db.hsncodes.insert({
    'name': 'Cotton Yarn(COYTN)',
    'hsncode': '520542',
    'description': '(Cotton Yarn Multiple Yarn, Per Single Yarn 714.29-232.56 Decitex(not sewing))',
    'CGST':0,
    'SGST':0,
    'IGST':0
});
db.categories.update({
    'name': 'Cotton Yarn',
    'code': 'COTYN',
    'type': 'SubCategory2'
}, {$addToSet: {'hsncodes':{ hsncode: db.hsncodes.findOne({'hsncode': '520542'})._id}}});


db.hsncodes.insert({
    'name': 'Cotton Yarn(COTYN)',
    'hsncode': '520543',
    'description': '(Cotton Yarn Multiple Yarn, Per Single Yarn 714.29-232.56 Decitex(not sewing))',
    'CGST':0,
    'SGST':0,
    'IGST':0
});
db.categories.update({
    'name': 'Cotton Yarn',
    'code': 'COTYN',
    'type': 'SubCategory2'
}, {$addToSet: {'hsncodes':{ hsncode: db.hsncodes.findOne({'hsncode': '520543'})._id}}});


db.hsncodes.insert({
    'name': 'Cotton Yarn(COTYN)',
    'hsncode': '520544',
    'description': '(Cotton Yarn Multiple Yarn, Per Single Yarn 192.31-125 Decitex(not sewing))',
    'CGST':0,
    'SGST':0,
    'IGST':0
});
db.categories.update({
    'name': 'Cotton Yarn',
    'code': 'COTYN',
    'type': 'SubCategory2'
}, {$addToSet: {'hsncodes':{ hsncode: db.hsncodes.findOne({'hsncode': '520544'})._id}}});




db.hsncodes.insert({
    'name': 'Cotton Yarn(COYTN)',
    'hsncode': '520545',
    'description': '(Cotton Yarn Multiple Yarn, Per Single Yarn Less than 125 Decitex(not sewing))',
    'CGST':0,
    'SGST':0,
    'IGST':0
});
db.categories.update({
    'name': 'Cotton Yarn',
    'code': 'COTYN',
    'type': 'SubCategory2'
}, {$addToSet: {'hsncodes':{ hsncode: db.hsncodes.findOne({'hsncode': '520545'})._id}}});


db.hsncodes.insert({
    'name': 'Cotton Yarn(COTYN)',
    'hsncode': '520411',
    'description': '(Cotton Sewing Thread Cotton Content 85% or More; Not Put up for Retail Sale(sewing))',
    'CGST':0,
    'SGST':0,
    'IGST':0
});
db.categories.update({
    'name': 'Cotton Yarn',
    'code': 'COTYN',
    'type': 'SubCategory2'
}, {$addToSet: {'hsncodes':{ hsncode: db.hsncodes.findOne({'hsncode': '520411'})._id}}});


db.hsncodes.insert({
    'name': 'Cotton Yarn(COYTN)',
    'hsncode': '520419',
    'description': '(Other Cotton Sewing Thread Not Put up for Retail Sale(sewing))',
    'CGST':0,
    'SGST':0,
    'IGST':0
});
db.categories.update({
    'name': 'Cotton Yarn',
    'code': 'COTYN',
    'type': 'SubCategory2'
}, {$addToSet: {'hsncodes':{ hsncode: db.hsncodes.findOne({'hsncode': '520419'})._id}}});


db.hsncodes.insert({
    'name': 'Cotton Yarn(COYTN)',
    'hsncode': '520420',
    'description': '(Cotton Sewing Thread Put up for Retail Sale(sewing))',
    'CGST':0,
    'SGST':0,
    'IGST':0
});
db.categories.update({
    'name': 'Cotton Yarn',
    'code': 'COTYN',
    'type': 'SubCategory2'
}, {$addToSet: {'hsncodes':{ hsncode: db.hsncodes.findOne({'hsncode': '520420'})._id}}});


db.hsncodes.insert({
    'name': 'Cotton yarn(COYTN)',
    'hsncode': '520300',
    'description': '(Cotton , Carded Or Combed)',
    'CGST':0,
    'SGST':0,
    'IGST':0
});
db.categories.update({
    'name': 'Cotton Yarn',
    'code': 'COTYN',
    'type': 'SubCategory2'
}, {$addToSet: {'hsncodes':{ hsncode: db.hsncodes.findOne({'hsncode': '520300'})._id}}});



db.hsncodes.insert({
    'name': 'Cotton Fabric(COTFB)',
    'hsncode': '520811',
    'description': 'Plain Woven Fabrics of Cotton (Cotton 85% or More; Not More than 100g/m2; Unbleached)',
    'CGST':0,
    'SGST':0,
    'IGST':0
});
db.categories.update({
    'name': 'Cotton Fabric',
    'code': 'COTFB',
    'type': 'SubCategory2'
}, {$addToSet: {'hsncodes':{ hsncode: db.hsncodes.findOne({'hsncode': '520811'})._id}}});

db.hsncodes.insert({
    'name': 'Cotton Fabric(COTFB)',
    'hsncode': '520812',
    'description': 'Plain Woven Fabrics of Cotton (Cotton 85% or More, 100g-200g/m2; Unbleached)',
    'CGST':0,
    'SGST':0,
    'IGST':0
});
db.categories.update({
    'name': 'Cotton Fabric',
    'code': 'COTFB',
    'type': 'SubCategory2'
}, {$addToSet: {'hsncodes':{ hsncode: db.hsncodes.findOne({'hsncode': '520812'})._id}}});


db.hsncodes.insert({
    'name': 'Cotton Fabric(COTFB)',
    'hsncode': '520813',
    'description': 'Twill Woven Fabrics of Cotton (Cotton 85% or More; Not More than 200g/m2; Unbleached)',
    'CGST':0,
    'SGST':0,
    'IGST':0
});
db.categories.update({
    'name': 'Cotton Fabric',
    'code': 'COTFB',
    'type': 'SubCategory2'
}, {$addToSet: {'hsncodes':{ hsncode: db.hsncodes.findOne({'hsncode': '520813'})._id}}});


db.hsncodes.insert({
    'name': 'Cotton Fabric(COTFB)',
    'hsncode': '520819',
    'description': 'Other Woven Fabrics of Cotton (Cotton 85% or More; Not More than 200g/m2; Unbleached)',
    'CGST':0,
    'SGST':0,
    'IGST':0
});
db.categories.update({
    'name': 'Cotton Fabric',
    'code': 'COTFB',
    'type': 'SubCategory2'
}, {$addToSet: {'hsncodes':{ hsncode: db.hsncodes.findOne({'hsncode': '520819'})._id}}});

db.hsncodes.insert({
    'name': 'Cotton Fabric(COTFB)',
    'hsncode': '520821',
    'description': 'Plain Woven Fabrics of Cotton (Cotton 85% or More; Not More than 100g/m2; Bleached)',
    'CGST':0,
    'SGST':0,
    'IGST':0
});
db.categories.update({
    'name': 'Cotton Fabric',
    'code': 'COTFB',
    'type': 'SubCategory2'
}, {$addToSet: {'hsncodes':{ hsncode: db.hsncodes.findOne({'hsncode': '520821'})._id}}});



db.hsncodes.insert({
    'name': 'Cotton Fabric(COTFB)',
    'hsncode': '520822',
    'description': 'Plain Woven Fabrics of Cotton (Cotton 85% or More; 100g-200g/m2; Bleached)',
    'CGST':0,
    'SGST':0,
    'IGST':0
});
db.categories.update({
    'name': 'Cotton Fabric',
    'code': 'FABRC',
    'type': 'SubCategory2'
}, {$addToSet: {'hsncodes':{ hsncode: db.hsncodes.findOne({'hsncode': '520822'})._id}}});


db.hsncodes.insert({
    'name': 'Cotton Fabric(COTFB)',
    'hsncode': '520823',
    'description': 'Twill Woven Fabrics of Cotton (Cotton 85% or More; Not More than 200g/m2; Bleached)',
    'CGST':0,
    'SGST':0,
    'IGST':0
});
db.categories.update({
    'name': 'Cotton Fabric',
    'code': 'COTFB',
    'type': 'SubCategory2'
}, {$addToSet: {'hsncodes':{ hsncode: db.hsncodes.findOne({'hsncode': '520823'})._id}}});


db.hsncodes.insert({
    'name': 'Cotton Fabric(COTFB)',
    'hsncode': '520829',
    'description': 'Other Woven Fabrics of Cotton(Cotton 85% or More; Not More than 200g/m2; Bleached)',
    'CGST':0,
    'SGST':0,
    'IGST':0
});
db.categories.update({
    'name': 'Cotton Fabric',
    'code': 'COTFB',
    'type': 'SubCategory2'
}, {$addToSet: {'hsncodes':{ hsncode: db.hsncodes.findOne({'hsncode': '520829'})._id}}});



db.hsncodes.insert({
    'name': 'Cotton Fabric(COTFB)',
    'hsncode': '520831',
    'description': 'Plain Woven Fabrics of Cotton (Cotton 85% or More; Dyed; Not More than 100g/m2)',
    'CGST':0,
    'SGST':0,
    'IGST':0
});
db.categories.update({
    'name': 'Cotton Fabric',
    'code': 'COTFB',
    'type': 'SubCategory2'
}, {$addToSet: {'hsncodes':{ hsncode: db.hsncodes.findOne({'hsncode': '520831'})._id}}});


db.hsncodes.insert({
    'name': 'Cotton Fabric(COTFB)',
    'hsncode': '520832',
    'description': 'Plain Woven Fabrics of Cotton (Cotton 85% or More; Dyed; Not More than 100g/m2)',
    'CGST':0,
    'SGST':0,
    'IGST':0
});
db.categories.update({
    'name': 'Cotton Fabric',
    'code': 'COTFB',
    'type': 'SubCategory2'
}, {$addToSet: {'hsncodes':{ hsncode: db.hsncodes.findOne({'hsncode': '520832'})._id}}});


db.hsncodes.insert({
    'name': 'Cotton Fabric(COTFB)',
    'hsncode': '520833',
    'description': 'Twill Woven Fabrics of Cotton (Cotton 85% or More; Dyed; Not More than 200g/m2)',
    'CGST':0,
    'SGST':0,
    'IGST':0
});
db.categories.update({
    'name': 'Cotton Fabric',
    'code': 'COTFB',
    'type': 'SubCategory2'
}, {$addToSet: {'hsncodes':{ hsncode: db.hsncodes.findOne({'hsncode': '520833'})._id}}});


db.hsncodes.insert({
    'name': 'Cotton Fabric(COTFB)',
    'hsncode': '520839',
    'description': 'Other Woven Fabrics of Cotton (Cotton 85% or More; Dyed; Not More than 200g/m2)',
    'CGST':0,
    'SGST':0,
    'IGST':0
});
db.categories.update({
    'name': 'Cotton Fabric',
    'code': 'COTFB',
    'type': 'SubCategory2'
}, {$addToSet: {'hsncodes':{ hsncode: db.hsncodes.findOne({'hsncode': '520839'})._id}}});



db.hsncodes.insert({
    'name': 'Cotton Fabric(COTFB)',
    'hsncode': '520841',
    'description': 'Plain Woven Fabrics of Cotton (Cotton 85% or More; Yarn of Different Color; Not More than 100g/m2)',
    'CGST':0,
    'SGST':0,
    'IGST':0
});
db.categories.update({
    'name': 'Cotton Fabric',
    'code': 'COTFB',
    'type': 'SubCategory2'
}, {$addToSet: {'hsncodes':{ hsncode: db.hsncodes.findOne({'hsncode': '520841'})._id}}});


db.hsncodes.insert({
    'name': 'Cotton Fabric(COTFB)',
    'hsncode': '520843',
    'description': '(Twill Woven Fabrics of Cotton (Cotton 85% or More; Yarn of Different Color; Not More than 200g/m2)',
    'CGST':0,
    'SGST':0,
    'IGST':0
});
db.categories.update({
    'name': 'Cotton Fabric',
    'code': 'COTFB',
    'type': 'SubCategory2'
}, {$addToSet: {'hsncodes':{ hsncode: db.hsncodes.findOne({'hsncode': '520843'})._id}}});



db.hsncodes.insert({
    'name': 'Cotton Fabric(COTFB)',
    'hsncode': '520849',
    'description': 'Other Woven Fabrics of Cotton (Cotton 85% or More; Yarn of Different Color; Not More than 200g/m2',
    'CGST':0,
    'SGST':0,
    'IGST':0
});
db.categories.update({
    'name': 'Cotton Fabric',
    'code': 'COTFB',
    'type': 'SubCategory2'
}, {$addToSet: {'hsncodes':{ hsncode: db.hsncodes.findOne({'hsncode': '520849'})._id}}});

db.hsncodes.insert({
    'name': 'Cotton Fabric(COTFB)',
    'hsncode': '520851',
    'description': 'Plain Woven Fabrics of Cotton (Cotton 85% or More; Printed; Not More than 100g/m2)',
    'CGST':0,
    'SGST':0,
    'IGST':0
});
db.categories.update({
    'name': 'Cotton Fabric',
    'code': 'COTFB',
    'type': 'SubCategory2'
}, {$addToSet: {'hsncodes':{ hsncode: db.hsncodes.findOne({'hsncode': '520851'})._id}}});


db.hsncodes.insert({
    'name': 'Cotton Fabric(COTFB)',
    'hsncode': '520852',
    'description': 'Plain Woven Fabrics of Cotton Cotton 85% or More; Printed; 100g-200g/m2)',
    'CGST':0,
    'SGST':0,
    'IGST':0
});
db.categories.update({
    'name': 'Cotton Fabric',
    'code': 'COTFB',
    'type': 'SubCategory2'
}, {$addToSet: {'hsncodes':{ hsncode: db.hsncodes.findOne({'hsncode': '520852'})._id}}});


db.hsncodes.insert({
    'name': 'Cotton Fabric(COTFB)',
    'hsncode': '520853',
    'description': 'Twill Woven Fabrics of Cotton (Cotton 85% or More; Printed; Not More than 200g/m2)',
    'CGST':0,
    'SGST':0,
    'IGST':0
});
db.categories.update({
    'name': 'Cotton Fabric',
    'code': 'COTFB',
    'type': 'SubCategory2'
}, {$addToSet: {'hsncodes':{ hsncode: db.hsncodes.findOne({'hsncode': '520853'})._id}}});


db.hsncodes.insert({
    'name': 'Cotton Fabric(COTFB)',
    'hsncode': '520859',
    'description': 'Other Woven Fabrics of Cotton (Cotton 85% or More; Printed; Not More than 200g/m2)',
    'CGST':0,
    'SGST':0,
    'IGST':0
});
db.categories.update({
    'name': 'Cotton Fabric',
    'code': 'COTFB',
    'type': 'SubCategory2'
}, {$addToSet: {'hsncodes':{ hsncode: db.hsncodes.findOne({'hsncode': '520859'})._id}}});


db.hsncodes.insert({
    'name': 'Silk Fabric(SLKFB)',
    'hsncode': '500710',
    'description': 'Fabrics of Noil Silk',
    'CGST':0,
    'SGST':0,
    'IGST':0
});
db.categories.update({
    'name': 'Silk Fabric',
    'code': 'SLKFB',
    'type': 'SubCategory2'
}, {$addToSet: {'hsncodes':{ hsncode: db.hsncodes.findOne({'hsncode': '500710'})._id}}});



db.hsncodes.insert({
    'name': 'Silk Fabric(SLKFB)',
    'hsncode': '500720',
    'description': 'Other Woven Fabrics of Silk (Silk Content 85% or More)',
    'CGST':0,
    'SGST':0,
    'IGST':0
});
db.categories.update({
    'name': 'Silk Fabric',
    'code': 'SLKFB',
    'type': 'SubCategory2'
}, {$addToSet: {'hsncodes':{ hsncode: db.hsncodes.findOne({'hsncode': '500720'})._id}}});


db.hsncodes.insert({
    'name': 'Silk Fabric(SLKFB)',
    'hsncode': '500790',
    'description': 'Other Woven Fabrics of Silk or of Silk Waste',
    'CGST':0,
    'SGST':0,
    'IGST':0
});
db.categories.update({
    'name': 'Silk Fabric',
    'code': 'SLKFB',
    'type': 'SubCategory2'
}, {$addToSet: {'hsncodes':{ hsncode: db.hsncodes.findOne({'hsncode': '500790'})._id}}});


db.hsncodes.insert({
    'name': 'Cotton Bales(COTBL)',
    'hsncode': '52010011',
    'description': 'Cotton Bales(Bengal Deshi)',
    'CGST':0,
    'SGST':0,
    'IGST':0
});
db.categories.update({
    'name': 'Cotton Bales',
    'code': 'COTBL',
    'type': 'SubCategory2'
}, {$addToSet: {'hsncodes':{ hsncode: db.hsncodes.findOne({'hsncode': '52010011'})._id}}});


db.hsncodes.insert({
    'name': 'Cotton Bales(COTBL)',
    'hsncode': '52010012',
    'description': 'Cotton Bales(Lengths 20.5 Mm (25/32") And Below)',
    'CGST':0,
    'SGST':0,
    'IGST':0
});
db.categories.update({
    'name': 'Cotton Bales',
    'code': 'COTBL',
    'type': 'SubCategory2'
}, {$addToSet: {'hsncodes':{ hsncode: db.hsncodes.findOne({'hsncode': '52010012'})._id}}});


db.hsncodes.insert({
    'name': 'Cotton Bales(COTBL)',
    'hsncode': '52010013',
    'description': 'Cotton Bales(Length Exceeding 20.5mm (26/32") But Not Exceeding 24.5mm (30/32"))',
    'CGST':0,
    'SGST':0,
    'IGST':0
});
db.categories.update({
    'name': 'Cotton Bales',
    'code': 'COTBL',
    'type': 'SubCategory2'
}, {$addToSet: {'hsncodes':{ hsncode: db.hsncodes.findOne({'hsncode': '52010013'})._id}}});


db.hsncodes.insert({
    'name': 'Cotton Bales(COTBL)',
    'hsncode': '52010014',
    'description': 'Cotton Bales(Length Over 24.5 Mm(31/32) To 28 Mm',
    'CGST':0,
    'SGST':0,
    'IGST':0
});
db.categories.update({
    'name': 'Cotton Bales',
    'code': 'COTBL',
    'type': 'SubCategory2'
}, {$addToSet: {'hsncodes':{ hsncode: db.hsncodes.findOne({'hsncode': '52010014'})._id}}});


db.hsncodes.insert({
    'name': 'Cotton Bales(COTBL)',
    'hsncode': '52010015',
    'description': 'Cotton Bales(Length 28.5 (14/32) And Above But Below 34.5 Mm Mm)',
    'CGST':0,
    'SGST':0,
    'IGST':0
});
db.categories.update({
    'name': 'Cotton Bales',
    'code': 'COTBL',
    'type': 'SubCategory2'
}, {$addToSet: {'hsncodes':{ hsncode: db.hsncodes.findOne({'hsncode': '52010015'})._id}}});


db.hsncodes.insert({
    'name': 'Cotton Bales',
    'hsncode': '52010015',
    'description': 'Cotton Bales(Length 28.5 (14/32) And Above But Below 34.5 Mm Mm)',
    'CGST':0,
    'SGST':0,
    'IGST':0
});
db.categories.update({
    'name': 'Cotton Bales',
    'code': 'COTBL',
    'type': 'SubCategory2'
}, {$addToSet: {'hsncodes':{ hsncode: db.hsncodes.findOne({'hsncode': '52010015'})._id}}});


db.hsncodes.insert({
    'name': 'Melon Seeds (MLNSD)',
    'hsncode': '12099190',
    'description': 'Melon seeds',
    'CGST':0,
    'SGST':0,
    'IGST':0
});
db.categories.update({
    'name': 'Melon Seeds',
    'code': 'MLNSD',
    'type': 'SubCategory2'
}, {$addToSet: {'hsncodes':{ hsncode: db.hsncodes.findOne({'hsncode': '12099190'})._id}}});


db.hsncodes.insert({
    'name': 'Corn(CORNG)',
    'hsncode': '10059000',
    'description': 'Maize(Corn)',
    'CGST':0,
    'SGST':0,
    'IGST':0
});
db.categories.update({
    'name': 'Corn',
    'code': 'CORNG',
    'type': 'SubCategory2'
}, {$addToSet: {'hsncodes':{ hsncode: db.hsncodes.findOne({'hsncode': '10059000'})._id}}});


db.hsncodes.insert({
    'name': 'Jaggery (JGYPD)',
    'hsncode': '17049030',
    'description': 'Jaggery powder and Cubes',
    'CGST':0,
    'SGST':0,
    'IGST':0
});
db.categories.update({
    'name': 'Jaggery Powder',
    'code':'JGYPD',
    'type': 'SubCategory2'
}, {$addToSet: {'hsncodes':{ hsncode: db.hsncodes.findOne({'hsncode':'17049030'})._id}}});


db.hsncodes.insert({
    'name': 'Tea(TEAPD)',
    'hsncode': '09024040',
    'description': 'Tea Bags',
    'CGST':0,
    'SGST':0,
    'IGST':0
});
db.categories.update({
    'name': 'Tea',
    'code': 'TEAPD',
    'type': 'SubCategory2'
}, {$addToSet: {'hsncodes':{ hsncode: db.hsncodes.findOne({'hsncode': '09024040'})._id}}});

db.hsncodes.insert({
    'name':' Tea(TEAPD)',
    'hsncode': '09021010',
    'description': 'Green Tea(not exceeding 25 kg)',
    'CGST':0,
    'SGST':0,
    'IGST':0
});
db.categories.update({
    'name': 'Tea',
    'code': 'TEAPD',
    'type': 'SubCategory2'
}, {$addToSet: {'hsncodes':{ hsncode: db.hsncodes.findOne({'hsncode': '09021010'})._id}}});


db.hsncodes.insert({
    'name': 'Tea(TEAPD)',
    'hsncode': '09021020',
    'description': 'Green Tea( 25 kg to not exceeding 1 kg)',
    'CGST':0,
    'SGST':0,
    'IGST':0
});
db.categories.update({
    'name': 'Tea',
    'code': 'TEAPD',
    'type': 'SubCategory2'
}, {$addToSet: {'hsncodes':{ hsncode: db.hsncodes.findOne({'hsncode': '09021020'})._id}}});


db.hsncodes.insert({
    'name': 'Tea(TEAPD)',
    'hsncode': '09021030',
    'description': 'Green Tea( 1 kg to not exceeding 3 kg)',
    'CGST':0,
    'SGST':0,
    'IGST':0
});
db.categories.update({
    'name': 'Tea',
    'code': 'TEAPD',
    'type': 'SubCategory2'
}, {$addToSet: {'hsncodes':{ hsncode: db.hsncodes.findOne({'hsncode': '09021030'})._id}}});



db.hsncodes.insert({
    'name': 'Tea(TEAPD)',
    'hsncode': '09021090',
    'description': 'Green Tea( other)',
    'CGST':0,
    'SGST':0,
    'IGST':0
});
db.categories.update({
    'name': 'Tea',
    'code': 'TEAPD',
    'type': 'SubCategory2'
}, {$addToSet: {'hsncodes':{ hsncode: db.hsncodes.findOne({'hsncode': '09021090'})._id}}});


db.hsncodes.insert({
    'name': 'Tea(TEAPD)',
    'hsncode': '09023010',
    'description': 'Black Tea(not exceeding 25 kg)',
    'CGST':0,
    'SGST':0,
    'IGST':0
});
db.categories.update({
    'name': 'Tea',
    'code': 'TEAPD',
    'type': 'SubCategory2'
}, {$addToSet: {'hsncodes':{ hsncode: db.hsncodes.findOne({'hsncode': '09023010'})._id}}});


db.hsncodes.insert({
    'name': 'Tea(TEAPD)',
    'hsncode': '09023020',
    'description': 'Black Tea( 25 kg to not exceeding 1 kg)',
    'CGST':0,
    'SGST':0,
    'IGST':0
});
db.categories.update({
    'name': 'Tea',
    'code': 'TEAPD',
    'type': 'SubCategory2'
}, {$addToSet: {'hsncodes':{ hsncode: db.hsncodes.findOne({'hsncode': '09023020'})._id}}});


db.hsncodes.insert({
    'name': 'Tea(TEAPD)',
    'hsncode': '09023030',
    'description': 'BlackTea( 1 kg to not exceeding 3 kg)',
    'CGST':0,
    'SGST':0,
    'IGST':0
});
db.categories.update({
    'name': 'Tea',
    'code': 'TEAPD',
    'type': 'SubCategory2'
}, {$addToSet: {'hsncodes':{ hsncode: db.hsncodes.findOne({'hsncode': '09023030'})._id}}});


db.hsncodes.insert({
    'name': 'Tea(TEAPD)',
    'hsncode': '09023090',
    'description': 'Black Tea( other)',
    'CGST':0,
    'SGST':0,
    'IGST':0
});
db.categories.update({
    'name': 'Tea',
    'code': 'TEAPD',
    'type': 'SubCategory2'
}, {$addToSet: {'hsncodes':{ hsncode: db.hsncodes.findOne({'hsncode': '09023090'})._id}}});



db.hsncodes.insert({
    'name': 'Coriander(CRNDR)',
    'hsncode': '09092110',
    'description': 'of seed quality',
    'CGST':0,
    'SGST':0,
    'IGST':0
});
db.categories.update({
    'name': 'Coriander',
    'code': 'CRNDR',
    'type': 'SubCategory2'
}, {$addToSet: {'hsncodes':{ hsncode: db.hsncodes.findOne({'hsncode': '09092110'})._id}}});


db.hsncodes.insert({
    'name': 'Coriander(CRNDR)',
    'hsncode': '09092190',
    'description': 'other than seed quality',
    'CGST':0,
    'SGST':0,
    'IGST':0
});
db.categories.update({
    'name': 'Coriander',
    'code': 'CRNDR',
    'type': 'SubCategory2'
}, {$addToSet: {'hsncodes':{ hsncode: db.hsncodes.findOne({'hsncode': '09092190'})._id}}});




db.hsncodes.insert({
    'name': 'Dry Coconut (DRYCN)',
    'hsncode': '12030000',
    'description': 'COPRA',
    'CGST':0,
    'SGST':0,
    'IGST':0
});
db.categories.update({
    'name': 'Dry Coconut',
    'code': 'DRYCN',
    'type': 'SubCategory2'
}, {$addToSet: {'hsncodes':{ hsncode: db.hsncodes.findOne({'hsncode': '12030000'})._id}}});


db.hsncodes.insert({
    'name': 'Sunflower seeds(SUNSD)',
    'hsncode': '12060090',
    'description': 'Sunflower Seeds(other than seed quality)',
    'CGST':0,
    'SGST':0,
    'IGST':0
});
db.categories.update({
    'name': 'Sunflower Seeds',
    'code': 'SUNSD',
    'type': 'SubCategory2'
}, {$addToSet: {'hsncodes':{ hsncode: db.hsncodes.findOne({'hsncode': '12060090'})._id}}});



db.hsncodes.insert({
    'name': 'Groundnut(GRDNT)',
    'hsncode': '12021091',
    'description': 'Ground nut(other than seed quality)',
    'CGST':0,
    'SGST':0,
    'IGST':0
});
db.categories.update({
    'name': 'Groundnut',
    'code': 'GRDNT',
    'type': 'SubCategory2'
}, {$addToSet: {'hsncodes':{ hsncode: db.hsncodes.findOne({'hsncode': '12021091'})._id}}});

db.hsncodes.insert({
    'name': 'Soya Beans(SOYBN)',
    'hsncode': '12011000',
    'description': 'Soya Beans(seed quality)',
    'CGST':0,
    'SGST':0,
    'IGST':0
});
db.categories.update({
    'name': 'Soya Bean',
    'code': 'SOYBN',
    'type': 'SubCategory2'
}, {$addToSet: {'hsncodes':{ hsncode: db.hsncodes.findOne({'hsncode': '12011000'})._id}}});



db.hsncodes.insert({
    'name': 'Sesame seeds(SESAM)',
    'hsncode': '12074010',
    'description': 'Sesame seeds(seed quality)',
    'CGST':0,
    'SGST':0,
    'IGST':0
});
db.categories.update({
    'name': 'Sesame',
    'code': 'SESAM',
    'type': 'SubCategory2'
}, {$addToSet: {'hsncodes':{ hsncode: db.hsncodes.findOne({'name':'Sesame seeds(SESAM)','hsncode': '12074010'})._id}}});

db.hsncodes.insert({
    'name': 'Wheat (WHEAT)',
    'hsncode': '10019910',
    'description': 'Wheat',
    'CGST':0,
    'SGST':0,
    'IGST':0
});
db.categories.update({
    'name': 'Wheat',
    'code': 'WHEAT',
    'type': 'SubCategory2'
}, {$addToSet: {'hsncodes':{ hsncode: db.hsncodes.findOne({'hsncode': '10019910'})._id}}});


db.hsncodes.insert({
    'name': 'Cotton Seeds (COTSD)',
    'hsncode': '12072010',
    'description': 'seed quality',
    'CGST':0,
    'SGST':0,
    'IGST':0
});
db.categories.update({
    'name': 'Cotton Seeds',
    'code': 'COTSD',
    'type': 'SubCategory2'
}, {$addToSet: {'hsncodes':{ hsncode: db.hsncodes.findOne({'hsncode': '12072010'})._id}}});


db.hsncodes.insert({
    'name': 'Cotton Seeds (COTSD)',
    'hsncode': '12072090',
    'description': 'other than seed quality',
    'CGST':0,
    'SGST':0,
    'IGST':0
});
db.categories.update({
    'name': 'Cotton Seeds',
    'code': 'COTSD',
    'type': 'SubCategory2'
}, {$addToSet: {'hsncodes':{ hsncode: db.hsncodes.findOne({'hsncode': '12072090'})._id}}});


db.hsncodes.insert({
    'name': 'Peas (PEAS)',
    'hsncode': '07102100',
    'description': 'Shelled Or Unshelled',
    'CGST':0,
    'SGST':0,
    'IGST':0
});
db.categories.update({
    'name': 'Peas',
    'code': 'PEAS',
    'type': 'SubCategory2'
}, {$addToSet: {'hsncodes':{ hsncode: db.hsncodes.findOne({'hsncode': '07102100'})._id}}});


db.hsncodes.insert({
    'name': 'Black Pepper (BLKPR)',
    'hsncode': '09041120',
    'description': 'Light Black Pepper',
    'CGST':0,
    'SGST':0,
    'IGST':0
});
db.categories.update({
    'name': 'Black Pepper',
    'code': 'BLKPR',
    'type': 'SubCategory2'
}, {$addToSet: {'hsncodes':{ hsncode: db.hsncodes.findOne({'hsncode': '09041120'})._id}}});



db.hsncodes.insert({
    'name': 'Black Pepper (BLKPR)',
    'hsncode': '09041130',
    'description': ' Black Pepper Garbled',
    'CGST':0,
    'SGST':0,
    'IGST':0
});
db.categories.update({
    'name': 'Black Pepper',
    'code': 'BLKPR',
    'type': 'SubCategory2'
}, {$addToSet: {'hsncodes':{ hsncode: db.hsncodes.findOne({'hsncode': '09041130'})._id}}});



db.hsncodes.insert({
    'name': 'Black Pepper (BLKPR)',
    'hsncode': '09041140',
    'description': 'Black Pepper UnGarbled',
    'CGST':0,
    'SGST':0,
    'IGST':0
});
db.categories.update({
    'name': 'Black Pepper',
    'code': 'BLKPR',
    'type': 'SubCategory2'
}, {$addToSet: {'hsncodes':{ hsncode: db.hsncodes.findOne({'hsncode': '09041140'})._id}}});


db.hsncodes.insert({
    'name': 'Cassia (CASSI)',
    'hsncode': '09061010',
    'description': 'Cassia',
    'CGST':0,
    'SGST':0,
    'IGST':0
});
db.categories.update({
    'name': 'Cassia',
    'code': 'CASSI',
    'type': 'SubCategory2'
}, {$addToSet: {'hsncodes':{ hsncode: db.hsncodes.findOne({'hsncode': '09061010'})._id}}});


db.hsncodes.insert({
    'name': 'Clove (CLOVE)',
    'hsncode': '09071030',
    'description': 'clove stem',
    'CGST':0,
    'SGST':0,
    'IGST':0
});
db.categories.update({
    'name': 'Clove',
    'code': 'CLOVE',
    'type': 'SubCategory2'
}, {$addToSet: {'hsncodes':{ hsncode: db.hsncodes.findOne({'hsncode': '09071030'})._id}}});


db.hsncodes.insert({
    'name': 'Clove (CLOVE)',
    'hsncode': '09072000',
    'description': ' Crushed or Ground',
    'CGST':0,
    'SGST':0,
    'IGST':0
});
db.categories.update({
    'name': 'Clove',
    'code': 'CLOVE',
    'type': 'SubCategory2'
}, {$addToSet: {'hsncodes':{ hsncode: db.hsncodes.findOne({'hsncode': '09072000'})._id}}});


db.hsncodes.insert({
    'name': 'Clove (CLOVE)',
    'hsncode': '09072010',
    'description': 'Extracted',
    'CGST':0,
    'SGST':0,
    'IGST':0
});
db.categories.update({
    'name': 'Clove',
    'code': 'CLOVE',
    'type': 'SubCategory2'
}, {$addToSet: {'hsncodes':{ hsncode: db.hsncodes.findOne({'hsncode': '09072010'})._id}}});


db.hsncodes.insert({
    'name': 'Methi (METHI)',
    'hsncode': '09109912',
    'description': 'Fenugreek',
    'CGST':0,
    'SGST':0,
    'IGST':0
});
db.categories.update({
    'name': 'Methi',
    'code': 'METHI',
    'type': 'SubCategory2'
}, {$addToSet: {'hsncodes':{ hsncode: db.hsncodes.findOne({'hsncode': '09109912'})._id}}});



db.hsncodes.insert({
    'name': 'Masala and Spices (MSPIC)',
    'hsncode': '09109924',
    'description': 'Fenugreek powder',
    'CGST':0,
    'SGST':0,
    'IGST':0
});
db.categories.update({
    'name': 'Masalas and Spices',
    'code': 'MSPIC',
    'type': 'SubCategory2'
}, {$addToSet: {'hsncodes':{ hsncode: db.hsncodes.findOne({'hsncode': '09109924'})._id}}});



db.hsncodes.insert({
    'name': 'Mustard (MUSTD)',
    'hsncode': '12075010',
    'description': 'seed quality',
    'CGST':0,
    'SGST':0,
    'IGST':0
});
db.categories.update({
    'name': 'Mustard',
    'code': 'MUSTD',
    'type': 'SubCategory2'
}, {$addToSet: {'hsncodes':{ hsncode: db.hsncodes.findOne({'hsncode': '12075010'})._id}}});


db.hsncodes.insert({
    'name': 'Mustard (MUSTD)',
    'hsncode': '12075090',
    'description': ' other than seed quality',
    'CGST':0,
    'SGST':0,
    'IGST':0
});
db.categories.update({
    'name': 'Mustard',
    'code': 'MUSTD',
    'type': 'SubCategory2'
}, {$addToSet: {'hsncodes':{ hsncode: db.hsncodes.findOne({'hsncode': '12075090'})._id}}});


db.hsncodes.insert({
    'name': 'Poppy Seeds (POYSD)',
    'hsncode': '12079100',
    'description': 'Poppy Seeds',
    'CGST':0,
    'SGST':0,
    'IGST':0
});
db.categories.update({
    'name': 'Poppy Seeds',
    'code': 'POYSD',
    'type': 'SubCategory2'
}, {$addToSet: {'hsncodes':{ hsncode: db.hsncodes.findOne({'hsncode': '12079100'})._id}}});


db.hsncodes.insert({
    'name': 'Skin Care (SKINC)',
    'hsncode': '33041000',
    'description': ' Lip make up Preparations',
    'CGST':0,
    'SGST':0,
    'IGST':0
});
db.categories.update({
    'name': 'Skin Care',
    'code': 'SKINC',
    'type': 'SubCategory2'
}, {$addToSet: {'hsncodes':{ hsncode: db.hsncodes.findOne({'hsncode': '33041000'})._id}}});



db.hsncodes.insert({
    'name': 'Skin Care (SKINC)',
    'hsncode': '33043000',
    'description': ' Pedicure and Manicure Preparations',
    'CGST':0,
    'SGST':0,
    'IGST':0
});
db.categories.update({
    'name': 'Skin Care',
    'code': 'SKINC',
    'type': 'SubCategory2'
}, {$addToSet: {'hsncodes':{ hsncode: db.hsncodes.findOne({'hsncode': '33043000'})._id}}});



db.hsncodes.insert({
    'name': 'Skin Care (SKINC)',
    'hsncode': '33042000',
    'description': ' Eye make up Preparations',
    'CGST':0,
    'SGST':0,
    'IGST':0
});
db.categories.update({
    'name': 'Skin Care',
    'code': 'SKINC',
    'type': 'SubCategory2'
}, {$addToSet: {'hsncodes':{ hsncode: db.hsncodes.findOne({'hsncode': '33042000'})._id}}});



db.hsncodes.insert({
    'name': 'Skin Care (SKINC)',
    'hsncode': '33049110',
    'description': ' Face Powders',
    'CGST':0,
    'SGST':0,
    'IGST':0
});
db.categories.update({
    'name': 'Skin Care',
    'code': 'SKINC',
    'type': 'SubCategory2'
}, {$addToSet: {'hsncodes':{ hsncode: db.hsncodes.findOne({'hsncode': '33049110'})._id}}});


db.hsncodes.insert({
    'name': 'Husk (HUSK)',
    'hsncode': '10061010',
    'description': 'Rice In The Husk (Paddy Or Rough) : Of Seed Quality',
    'CGST':0,
    'SGST':0,
    'IGST':0
});
db.categories.update({
    'name': 'Husk',
    'code': 'HUSK',
    'type': 'SubCategory2'
}, {$addToSet: {'hsncodes':{ hsncode: db.hsncodes.findOne({'hsncode': '10061010'})._id}}});


db.hsncodes.insert({
    'name': 'Husk (HUSK)',
    'hsncode': '10062000',
    'description': 'Rice Husked (Brown) Rice',
    'CGST':0,
    'SGST':0,
    'IGST':0
});
db.categories.update({
    'name': 'Husk',
    'code': 'HUSK',
    'type': 'SubCategory2'
}, {$addToSet: {'hsncodes':{ hsncode: db.hsncodes.findOne({'name':'Husk (HUSK)','hsncode': '10062000'})._id}}});


db.hsncodes.insert({
    'name': 'Husk (HUSK)',
    'hsncode': '10061090',
    'description': 'Rice In The Husk (Paddy Or Rough) : Other',
    'CGST':0,
    'SGST':0,
    'IGST':0
});
db.categories.update({
    'name': 'Husk',
    'code': 'HUSK',
    'type': 'SubCategory2'
}, {$addToSet: {'hsncodes':{ hsncode: db.hsncodes.findOne({'name':'Husk (HUSK)','hsncode': '10061090'})._id}}});

db.hsncodes.insert({
    'name': 'Kabuli Chana Dal White (KWDAL)',
    'hsncode': '07132000',
    'description': 'Kabuli Chana dal',
    'CGST':0,
    'SGST':0,
    'IGST':0
});
db.categories.update({
    'name': 'Kabuli Chana Dal White',
    'code': 'KWDAL',
    'type': 'SubCategory2'
}, {$addToSet: {'hsncodes':{ hsncode: db.hsncodes.findOne({'name': 'Kabuli Chana Dal White (KWDAL)','hsncode': '07132000'})._id}}});


db.hsncodes.insert({
    'name': 'Juices (JUICE)',
    'hsncode': '20091100',
    'description': 'Orange Juice : Frozen',
    'CGST':0,
    'SGST':0,
    'IGST':0
});
db.categories.update({
    'name': 'Juices',
    'code': 'JUICE',
    'type': 'SubCategory2'
}, {$addToSet: {'hsncodes':{ hsncode: db.hsncodes.findOne({'hsncode': '20091100'})._id}}});




db.hsncodes.insert({
    'name': 'Juices (JUICE)',
    'hsncode': '20091200',
    'description': 'Orange Juice : Not Frozen',
    'CGST':0,
    'SGST':0,
    'IGST':0
});
db.categories.update({
    'name': 'Juices',
    'code': 'JUICE',
    'type': 'SubCategory2'
}, {$addToSet: {'hsncodes':{ hsncode: db.hsncodes.findOne({'hsncode': '20091200'})._id}}});
db.hsncodes.insert({
    'name': 'Juices (JUICE)',
    'hsncode': '20096100',
    'description': 'Grape Juice (Including Grape Must) : Of A Brix Value Not Exceeding 30',
    'CGST':0,
    'SGST':0,
    'IGST':0
});
db.categories.update({
    'name': 'Juices',
    'code': 'JUICE',
    'type': 'SubCategory2'
}, {$addToSet: {'hsncodes':{ hsncode: db.hsncodes.findOne({'hsncode': '20096100'})._id}}});



db.hsncodes.insert({
    'name': 'Juices (JUICE)',
    'hsncode': '20096900',
    'description': 'Grape Juice (other)',
    'CGST':0,
    'SGST':0,
    'IGST':0
});
db.categories.update({
    'name': 'Juices',
    'code': 'JUICE',
    'type': 'SubCategory2'
}, {$addToSet: {'hsncodes':{ hsncode: db.hsncodes.findOne({'hsncode': '20096900'})._id}}});



db.hsncodes.insert({
    'name': 'Juices (JUICE)',
    'hsncode': '20097100',
    'description': 'Apple Juice : Of A Brix Value Not Exceeding 20',
    'CGST':0,
    'SGST':0,
    'IGST':0
});
db.categories.update({
    'name': 'Juices',
    'code': 'JUICE',
    'type': 'SubCategory2'
}, {$addToSet: {'hsncodes':{ hsncode: db.hsncodes.findOne({'hsncode': '20097100'})._id}}});


db.hsncodes.insert({
    'name': 'Juices (JUICE)',
    'hsncode': '20097900',
    'description': 'Apple Juice : Other',
    'CGST':0,
    'SGST':0,
    'IGST':0
});
db.categories.update({
    'name': 'Juices',
    'code': 'JUICE',
    'type': 'SubCategory2'
}, {$addToSet: {'hsncodes':{ hsncode: db.hsncodes.findOne({'hsncode': '20097900'})._id}}});

db.hsncodes.insert({
    'name': 'Juices (JUICE)',
    'hsncode': '20099000',
    'description': 'Mixtures Of Juices',
    'CGST':0,
    'SGST':0,
    'IGST':0
});
db.categories.update({
    'name': 'Juices',
    'code': 'JUICE',
    'type': 'SubCategory2'
}, {$addToSet: {'hsncodes':{ hsncode: db.hsncodes.findOne({'hsncode': '20099000'})._id}}});


db.hsncodes.insert({
    'name': 'Air Care and Cleaning (ACACL)',
    'hsncode': '85091000',
    'description': 'Self-Contained Electric Motor Vacuum Cleaners, Including Dry And Wet Vacuum Cleaners',
    'CGST':0,
    'SGST':0,
    'IGST':0
});
db.categories.update({
    'name': 'Air Care and Cleaning',
    'code': 'ACACL',
    'type': 'SubCategory2'
}, {$addToSet: {'hsncodes':{ hsncode: db.hsncodes.findOne({'hsncode': '85091000'})._id}}});



db.hsncodes.insert({
    'name': 'Air Care and Cleaning (ACACL)',
    'hsncode': '85094010',
    'description': 'Self-Contained Electric Motor (Food Grinders And Mixers; Fruit Or Vegetable Juice Extractors)',
    'CGST':0,
    'SGST':0,
    'IGST':0
});
db.categories.update({
    'name': 'Air Care and Cleaning',
    'code': 'ACACL',
    'type': 'SubCategory2'
}, {$addToSet: {'hsncodes':{ hsncode: db.hsncodes.findOne({'hsncode': '85094010'})._id}}});


db.hsncodes.insert({
    'name': 'Air Care and Cleaning (ACACL)',
    'hsncode': '85098000',
    'description': 'Self-Contained Electric Motor Appliances(other than vaccum cleaners)',
    'CGST':0,
    'SGST':0,
    'IGST':0
});
db.categories.update({
    'name': 'Air Care and Cleaning',
    'code': 'ACACL',
    'type': 'SubCategory2'
}, {$addToSet: {'hsncodes':{ hsncode: db.hsncodes.findOne({'hsncode': '85098000'})._id}}});


db.hsncodes.insert({
    'name': 'Cosmetics and Toiletries (COSMT)',
    'hsncode': '33030010',
    'description': 'Perfumes And Toilet Waters - Perfumes And Toilet Waters: Eau-De-Cologne',
    'CGST':0,
    'SGST':0,
    'IGST':0
});
db.categories.update({
    'name': 'Cosmetics and Toiletries',
    'code': 'COSMT',
    'type': 'SubCategory2'
}, {$addToSet: {'hsncodes':{ hsncode: db.hsncodes.findOne({'hsncode': '33030010'})._id}}});



db.hsncodes.insert({
    'name': 'Cosmetics and Toiletries (COSMT)',
    'hsncode': '33030020',
    'description': 'Perfumes And Toilet Waters - Perfumes And Toilet Waters: Rose Water',
    'CGST':0,
    'SGST':0,
    'IGST':0
});
db.categories.update({
    'name': 'Cosmetics and Toiletries',
    'code': 'COSMT',
    'type': 'SubCategory2'
}, {$addToSet: {'hsncodes':{ hsncode: db.hsncodes.findOne({'hsncode': '33030020'})._id}}});


db.hsncodes.insert({
    'name': 'Cosmetics and Toiletries (COSMT)',
    'hsncode': '33030030',
    'description': 'Perfumes And Toilet Waters - Perfumes And Toilet Waters: Keora Water',
    'CGST':0,
    'SGST':0,
    'IGST':0
});
db.categories.update({
    'name': 'Cosmetics and Toiletries',
    'code': 'COSMT',
    'type': 'SubCategory2'
}, {$addToSet: {'hsncodes':{ hsncode: db.hsncodes.findOne({'hsncode': '33030030'})._id}}});



db.hsncodes.insert({
    'name': 'Cosmetics and Toiletries (COSMT)',
    'hsncode': '33071010',
    'description': '(Pre-Shave, Shaving Or After-Shave Preparations, Personal Deodorants, Bath Preparations, Depilatories And Other Perfumery)',
    'CGST':0,
    'SGST':0,
    'IGST':0
});
db.categories.update({
    'name': 'Cosmetics and Toiletries',
    'code': 'COSMT',
    'type': 'SubCategory2'
}, {$addToSet: {'hsncodes':{ hsncode: db.hsncodes.findOne({'hsncode': '33071010'})._id}}});


db.hsncodes.insert({
    'name': 'Wool Fabric (WOLFB)',
    'hsncode': '51111910',
    'description': 'Containing 85 Percent Or More By Weight Of Wool Or Of Fine Animal Hair : Other : Unbleached',
    'CGST':0,
    'SGST':0,
    'IGST':0
});
db.categories.update({
    'name': 'Wool Fabric',
    'code': 'WOLFB',
    'type': 'SubCategory2'
}, {$addToSet: {'hsncodes':{ hsncode: db.hsncodes.findOne({'hsncode': '51111910'})._id}}});



db.hsncodes.insert({
    'name': 'Wool Fabric (WOLFB)',
    'hsncode': '51111920',
    'description': 'Containing 85 Percent Or More By Weight Of Wool Or Of Fine Animal Hair : Other : bleached',
    'CGST':0,
    'SGST':0,
    'IGST':0
});
db.categories.update({
    'name': 'Wool Fabric',
    'code': 'WOLFB',
    'type': 'SubCategory2'
}, {$addToSet: {'hsncodes':{ hsncode: db.hsncodes.findOne({'hsncode': '51111920'})._id}}});


db.hsncodes.insert({
    'name': 'Wool Fabric (WOLFB)',
    'hsncode': '51111930',
    'description': 'Containing 85 Percent Or More By Weight Of Wool Or Of Fine Animal Hair : Other : Dyed',
    'CGST':0,
    'SGST':0,
    'IGST':0
});
db.categories.update({
    'name': 'Wool Fabric',
    'code': 'WOLFB',
    'type': 'SubCategory2'
}, {$addToSet: {'hsncodes':{ hsncode: db.hsncodes.findOne({'hsncode': '51111930'})._id}}});



db.hsncodes.insert({
    'name': 'Wool Fabric (WOLFB)',
    'hsncode': '51111940',
    'description': 'Containing 85 Percent Or More By Weight Of Wool Or Of Fine Animal Hair : Other : Printed',
    'CGST':0,
    'SGST':0,
    'IGST':0
});
db.categories.update({
    'name': 'Wool Fabric',
    'code': 'WOLFB',
    'type': 'SubCategory2'
}, {$addToSet: {'hsncodes':{ hsncode: db.hsncodes.findOne({'hsncode': '51111940'})._id}}});


db.hsncodes.insert({
    'name': 'Wool Yarn (WOLYN)',
    'hsncode': '51031010',
    'description': 'Noils of Wool',
    'CGST':0,
    'SGST':0,
    'IGST':0
});
db.categories.update({
    'name': 'Wool Yarn',
    'code': 'WOLYN',
    'type': 'SubCategory2'
}, {$addToSet: {'hsncodes':{ hsncode: db.hsncodes.findOne({'hsncode': '51031010'})._id}}});


db.hsncodes.insert({
    'name': 'Wool Yarn (WOLYN)',
    'hsncode': '51031090',
    'description': 'other than Noils of Wool',
    'CGST':0,
    'SGST':0,
    'IGST':0
});
db.categories.update({
    'name': 'Wool Yarn',
    'code': 'WOLYN',
    'type': 'SubCategory2'
}, {$addToSet: {'hsncodes':{ hsncode: db.hsncodes.findOne({'hsncode': '51031090'})._id}}});

db.hsncodes.insert({
    'name': 'Kitchen and Dining (KTDNG)',
    'hsncode': '44190010',
    'description': 'Tableware And Kitchenware, Of Wood - Tableware And Kitchenware, Of Wood: Tableware',
    'CGST':0,
    'SGST':0,
    'IGST':0
});
db.categories.update({
    'name': 'Kitchen and Dining',
    'code': 'KTDNG',
    'type': 'SubCategory2'
}, {$addToSet: {'hsncodes':{ hsncode: db.hsncodes.findOne({'hsncode': '44190010'})._id}}});



db.hsncodes.insert({
    'name': 'Kitchen and Dining (KTDNG)',
    'hsncode': '69119090',
    'description': 'Tableware, Kitchenware, Other Household Articles And Toilet Articles, Of Porcelain Or China - Other: Other',
    'CGST':0,
    'SGST':0,
    'IGST':0
});
db.categories.update({
    'name': 'Kitchen and Dining',
    'code': 'KTDNG',
    'type': 'SubCategory2'
}, {$addToSet: {'hsncodes':{ hsncode: db.hsncodes.findOne({'hsncode': '69119090'})._id}}});


db.hsncodes.insert({
    'name': 'Repellents (RPLNS)',
    'hsncode': '38081091',
    'description': 'Repellents For Insects Such As Flies, Mosquito',
    'CGST':0,
    'SGST':0,
    'IGST':0
});
db.categories.update({
    'name': 'Repellents',
    'code': 'RPLNS',
    'type': 'SubCategory2'
}, {$addToSet: {'hsncodes':{ hsncode: db.hsncodes.findOne({'hsncode': '38081091'})._id}}});


db.hsncodes.insert({
    'name': 'Pooja Needs (PJNDS)',
    'hsncode': '73239390',
    'description': 'Kunkum',
    'CGST':0,
    'SGST':0,
    'IGST':0
});
db.categories.update({
    'name': 'Pooja Needs',
    'code': 'PJNDS',
    'type': 'SubCategory2'
}, {$addToSet: {'hsncodes':{ hsncode: db.hsncodes.findOne({'hsncode': '73239390'})._id}}});



db.hsncodes.insert({
    'name': 'Pooja Needs (PJNDS)',
    'hsncode': '33074100',
    'description': 'Agarbatti',
    'CGST':0,
    'SGST':0,
    'IGST':0
});
db.categories.update({
    'name': 'Pooja Needs',
    'code': 'PJNDS',
    'type': 'SubCategory2'
}, {$addToSet: {'hsncodes':{ hsncode: db.hsncodes.findOne({'hsncode': '33074100'})._id}}});

db.hsncodes.insert({
    'name': 'Pooja Needs (PJNDS)',
    'hsncode': '29142921',
    'description': 'Camphor',
    'CGST':0,
    'SGST':0,
    'IGST':0
});
db.categories.update({
    'name': 'Pooja Needs',
    'code': 'PJNDS',
    'type': 'SubCategory2'
}, {$addToSet: {'hsncodes':{ hsncode: db.hsncodes.findOne({'hsncode': '29142921'})._id}}});


db.hsncodes.insert({
    'name': 'Pooja Needs (PJNDS)',
    'hsncode': '29142921',
    'description': 'Camphor',
    'CGST':0,
    'SGST':0,
    'IGST':0
});
db.categories.update({
    'name': 'Pooja Needs',
    'code': 'PJNDS',
    'type': 'SubCategory2'
}, {$addToSet: {'hsncodes':{ hsncode: db.hsncodes.findOne({'hsncode': '29142921'})._id}}});


db.hsncodes.insert({
    'name': 'Black Urad Dal Whole (BUDLW)',
    'hsncode': '07133100',
    'description': 'Urad Dal Black Whole',
    'CGST':0,
    'SGST':0,
    'IGST':0
});
db.categories.update({
    'name': 'Black Urad Dal Whole',
    'code': 'BUDLW',
    'type': 'SubCategory2'
}, {$addToSet: {'hsncodes':{ hsncode: db.hsncodes.findOne({'name': 'Black Urad Dal Whole (BUDLW)','hsncode': '07133100'})._id}}});


db.hsncodes.insert({
    'name': 'Channa Dal (CHDAL)',
    'hsncode': '07132000',
    'description': 'Channa dal',
    'CGST':0,
    'SGST':0,
    'IGST':0
});
db.categories.update({
    'name': 'Channa Dal',
    'code': 'CHDAL',
    'type': 'SubCategory2'
}, {$addToSet: {'hsncodes':{ hsncode: db.hsncodes.findOne({'name': 'Channa Dal (CHDAL)','hsncode': '07132000'})._id}}});


db.hsncodes.insert({
    'name': 'Red Gram (REDGM)',
    'hsncode': '07139090',
    'description': 'Red Gram',
    'CGST':0,
    'SGST':0,
    'IGST':0
});
db.categories.update({
    'name':'Red Gram',
    'code': 'REDGM',
    'type': 'SubCategory2'
}, {$addToSet: {'hsncodes':{ hsncode: db.hsncodes.findOne({'name': 'Red Gram (REDGM)','hsncode': '07139090'})._id}}});



db.hsncodes.insert({
    'name': 'White Urad Dal Split (WUDLS)',
    'hsncode': '07133910',
    'description': 'White Urad Dal Split',
    'CGST':0,
    'SGST':0,
    'IGST':0
});
db.categories.update({
    'name': 'White Urad Dal Split',
    'code': 'WUDLS',
    'type': 'SubCategory2'
}, {$addToSet: {'hsncodes':{ hsncode: db.hsncodes.findOne({'hsncode': '07133910'})._id}}});



db.hsncodes.insert({
    'name': 'White Urad Dal Whole (WUDLW)',
    'hsncode': '07139090',
    'description': 'White Urad Dal Whole',
    'CGST':0,
    'SGST':0,
    'IGST':0
});
db.categories.update({
    'name': 'White Urad Dal Whole',
    'code': 'WUDLW',
    'type': 'SubCategory2'
}, {$addToSet: {'hsncodes':{ hsncode: db.hsncodes.findOne({'name': 'White Urad Dal Whole (WUDLW)','hsncode': '07139090'})._id}}});


db.hsncodes.insert({
    'name': 'Yellow Moong Dal Split (MGDLY)',
    'hsncode': '07133910',
    'description': ' Yellow Moong Dal split',
    'CGST':0,
    'SGST':0,
    'IGST':0
});
db.categories.update({
    'name': 'Yellow Moong Dal Split',
    'code': 'MGDLY',
    'type': 'SubCategory2'
}, {$addToSet: {'hsncodes':{ hsncode: db.hsncodes.findOne({'name': 'Yellow Moong Dal Split (MGDLY)','hsncode': '07133910'})._id}}});



db.hsncodes.insert({
    'name': 'Black Till (BAKTL)',
    'hsncode': '12074090',
    'description': 'Black Till',
    'CGST':0,
    'SGST':0,
    'IGST':0
});
db.categories.update({
    'name': 'Black Till',
    'code': 'BAKTL',
    'type': 'SubCategory2'
}, {$addToSet: {'hsncodes':{ hsncode: db.hsncodes.findOne({'name': 'Black Till (BAKTL)','hsncode': '12074090'})._id}}});


db.hsncodes.insert({
    'name': 'Carom Seed (CRMSD)',
    'hsncode': '09109914',
    'description': 'Ajwain',
    'CGST':0,
    'SGST':0,
    'IGST':0
});
db.categories.update({
    'name': 'Carom Seed',
    'code': 'CRMSD',
    'type': 'SubCategory2'
}, {$addToSet: {'hsncodes':{ hsncode: db.hsncodes.findOne({'name': 'Carom Seed (CRMSD)','hsncode': '09109914'})._id}}});


db.hsncodes.insert({
    'name': 'Dry Chilli (DRCHL)',
    'hsncode': '09042110',
    'description': 'Dry Chilli',
    'CGST':0,
    'SGST':0,
    'IGST':0
});
db.categories.update({
    'name': 'Dry Chilli',
    'code': 'DRCHL',
    'type': 'SubCategory2'
}, {$addToSet: {'hsncodes':{ hsncode: db.hsncodes.findOne({'hsncode': '09042110'})._id}}});


db.hsncodes.insert({
    'name': 'Flax Seeds (FLXSD)',
    'hsncode': '12040090',
    'description': 'Brown Flax Seeds',
    'CGST':0,
    'SGST':0,
    'IGST':0
});
db.categories.update({
    'name': 'Flax Seeds',
    'code': 'FLXSD',
    'type': 'SubCategory2'
}, {$addToSet: {'hsncodes':{ hsncode: db.hsncodes.findOne({'hsncode': '12040090'})._id}}});


db.hsncodes.insert({
    'name': 'Green Cardamom (GRMCM)',
    'hsncode': '09083120',
    'description': 'Green Cardamom',
    'CGST':0,
    'SGST':0,
    'IGST':0
});
db.categories.update({
    'name': 'Green Cardamom',
    'code': 'GRMCM',
    'type': 'SubCategory2'
}, {$addToSet: {'hsncodes':{ hsncode: db.hsncodes.findOne({'hsncode': '09083120'})._id}}});



db.hsncodes.insert({
    'name': 'Jakayi (JAKYI)',
    'hsncode': '33019017',
    'description': 'Nutmeg',
    'CGST':0,
    'SGST':0,
    'IGST':0
});
db.categories.update({
    'name': 'Jakayi',
    'code': 'JAKYI',
    'type': 'SubCategory2'
}, {$addToSet: {'hsncodes':{ hsncode: db.hsncodes.findOne({'name': 'Jakayi (JAKYI)','hsncode': '33019017'})._id}}});




db.hsncodes.insert({
    'name': 'Javitri (JAVTI)',
    'hsncode': '33019017',
    'description': 'Skin of Nutmeg/Jatipatri',
    'CGST':0,
    'SGST':0,
    'IGST':0
});
db.categories.update({
    'name': 'Jakayi',
    'code': 'JAKYI',
    'type': 'SubCategory2'
}, {$addToSet: {'hsncodes':{ hsncode: db.hsncodes.findOne({'hsncode': '33019017'})._id}}});


db.hsncodes.insert({
    'name': 'Sara Pappu (SARPU)',
    'hsncode': '08135020',
    'description': 'charoli/chironji',
    'CGST':0,
    'SGST':0,
    'IGST':0
});
db.categories.update({
    'name': 'Sara Pappu',
    'code': 'SARPU',
    'type': 'SubCategory2'
}, {$addToSet: {'hsncodes':{ hsncode: db.hsncodes.findOne({'hsncode': '08135020'})._id}}});


db.hsncodes.insert({
    'name': 'White Till (WIETL)',
    'hsncode': '21069099',
    'description': 'White Till',
    'CGST':0,
    'SGST':0,
    'IGST':0
});
db.categories.update({
    'name': 'White Till',
    'code': 'WIETL',
    'type': 'SubCategory2'
}, {$addToSet: {'hsncodes':{ hsncode: db.hsncodes.findOne({'name': 'White Till (WIETL)','hsncode': '21069099'})._id}}});

db.hsncodes.insert({
    'name': 'Coffee Beans (COFBN)',
    'hsncode': '09011149',
    'description': 'Coffee Beans',
    'CGST':0,
    'SGST':0,
    'IGST':0
});
db.categories.update({
    'name': 'Coffee Beans',
    'code': 'COFBN',
    'type': 'SubCategory2'
}, {$addToSet: {'hsncodes':{ hsncode: db.hsncodes.findOne({'hsncode': '09011149'})._id}}});


db.hsncodes.insert({
    'name': 'Coffee Powder (COPDR)',
    'hsncode': '21011120',
    'description': 'Coffee Powder',
    'CGST':0,
    'SGST':0,
    'IGST':0
});
db.categories.update({
    'name': 'Coffee Powder',
    'code': 'COPDR',
    'type': 'SubCategory2'
}, {$addToSet: {'hsncodes':{ hsncode: db.hsncodes.findOne({'hsncode': '21011120'})._id}}});


db.hsncodes.insert({
    'name': 'Softdrinks (SOFTD)',
    'hsncode': '21069019',
    'description': 'Softdrinks',
    'CGST':0,
    'SGST':0,
    'IGST':0
});
db.categories.update({
    'name': 'Softdrinks',
    'code': 'SOFTD',
    'type': 'SubCategory2'
}, {$addToSet: {'hsncodes':{ hsncode: db.hsncodes.findOne({'hsncode': '21069019'})._id}}});


db.hsncodes.insert({
    'name': 'Biscuits And Cookies (BISCK)',
    'hsncode': '19053100',
    'description': 'Biscuits and cookies',
    'CGST':0,
    'SGST':0,
    'IGST':0
});
db.categories.update({
    'name': 'Biscuits And Cookies',
    'code': 'BISCK',
    'type': 'SubCategory2'
}, {$addToSet: {'hsncodes':{ hsncode: db.hsncodes.findOne({'name': 'Biscuits And Cookies (BISCK)','hsncode': '19053100'})._id}}});


db.hsncodes.insert({
    'name': 'Maida (MAIDA)',
    'hsncode': '11010000',
    'description': 'MAIDA',
    'CGST':0,
    'SGST':0,
    'IGST':0
});
db.categories.update({
    'name': 'Maida',
    'code': 'MAIDA',
    'type': 'SubCategory2'
}, {$addToSet: {'hsncodes':{ hsncode: db.hsncodes.findOne({'name': 'Maida (MAIDA)','hsncode': '11010000'})._id}}});


db.hsncodes.insert({
    'name': 'Laundry (LNDRY)',
    'hsncode': '39269099',
    'description': 'Laundry Items',
    'CGST':0,
    'SGST':0,
    'IGST':0
});
db.categories.update({
    'name': 'Laundry',
    'code': 'LNDRY',
    'type': 'SubCategory2'
}, {$addToSet: {'hsncodes':{ hsncode: db.hsncodes.findOne({'hsncode': '39269099'})._id}}});






db.hsncodes.insert({
    'name': 'Paper and Disposable (PPRDL)',
    'hsncode': '84099990',
    'description': 'Disposable Items',
    'CGST':0,
    'SGST':0,
    'IGST':0
});
db.categories.update({
    'name': 'Paper and Disposable',
    'code': 'PPRDL',
    'type': 'SubCategory2'
}, {$addToSet: {'hsncodes':{ hsncode: db.hsncodes.findOne({'hsncode': '84099990'})._id}}});


db.hsncodes.insert({
    'name': 'Pet Care (PETCR)',
    'hsncode': '49011010',
    'description': 'Pet Care',
    'CGST':0,
    'SGST':0,
    'IGST':0
});
db.categories.update({
    'name': 'Pet Care',
    'code': 'PETCR',
    'type': 'SubCategory2'
}, {$addToSet: {'hsncodes':{ hsncode: db.hsncodes.findOne({'name': 'Pet Care (PETCR)','hsncode': '49011010'})._id}}});

db.hsncodes.insert({
    'name': 'Toilets and Surface Cleaners (TOASC)',
    'hsncode': '38101090',
    'description': 'Surface cleaners',
    'CGST':0,
    'SGST':0,
    'IGST':0
});
db.categories.update({
    'name': 'Toilets and Surface Cleaners',
    'code': 'TOASC',
    'type': 'SubCategory2'
}, {$addToSet: {'hsncodes':{ hsncode: db.hsncodes.findOne({'hsncode': '38101090'})._id}}});




db.hsncodes.insert({
    'name': 'Oral Care (ORALC)',
    'hsncode': '33029011',
    'description': 'Oral Care',
    'CGST':0,
    'SGST':0,
    'IGST':0
});
db.categories.update({
    'name': 'Oral Care',
    'code': 'ORALC',
    'type': 'SubCategory2'
}, {$addToSet: {'hsncodes':{ hsncode: db.hsncodes.findOne({'hsncode': '33029011'})._id}}});



db.hsncodes.insert({
    'name': 'Cotton Apparel (COTAP)',
    'hsncode': '62052000',
    'description': 'Cotton Apparel',
    'CGST':0,
    'SGST':0,
    'IGST':0
});
db.categories.update({
    'name': 'Cotton Apparel',
    'code': 'COTAP',
    'type': 'SubCategory2'
}, {$addToSet: {'hsncodes':{ hsncode: db.hsncodes.findOne({'hsncode': '62052000'})._id}}});


db.hsncodes.insert({
    'name': 'Silk Apparel (SLKAP)',
    'hsncode': '62044290',
    'description': 'Silk Apparel',
    'CGST':0,
    'SGST':0,
    'IGST':0
});
db.categories.update({
    'name': 'Silk Apparel',
    'code': 'SLKAP',
    'type': 'SubCategory2'
}, {$addToSet: {'hsncodes':{ hsncode: db.hsncodes.findOne({'hsncode': '62044290'})._id}}});


db.hsncodes.insert({
    'name': 'Wool Apparel (WOLAP)',
    'hsncode': '62019100',
    'description': 'wool Apparel',
    'CGST':0,
    'SGST':0,
    'IGST':0
});
db.categories.update({
    'name': 'Wool Apparel',
    'code': 'WOLAP',
    'type': 'SubCategory2'
}, {$addToSet: {'hsncodes':{ hsncode: db.hsncodes.findOne({'hsncode': '62019100'})._id}}});


db.hsncodes.insert({
    'name': 'Silk Yarn (SLKYN)',
    'hsncode': '57019090',
    'description': 'silk yarn',
    'CGST':0,
    'SGST':0,
    'IGST':0
});
db.categories.update({
    'name': 'Silk Yarn',
    'code': 'SLKYN',
    'type': 'SubCategory2'
}, {$addToSet: {'hsncodes':{ hsncode: db.hsncodes.findOne({'hsncode': '57019090'})._id}}});


//Manufacturer
//Buy 1,
// Sell 2,
// Mediate 3,
// Buy,Sell 4,
//Buy, Mediate 5
//Sell ,Mediate 6
//Sell ,Buy ,Mediate 7

db.registrationcategories.insert({
    'name': 'Manufacturer',
    'type': 1,
    'category':1,
    'enabled':false
});
db.registrationcategories.insert({
    'name': 'Manufacturer',
    'type': 2,
    'category':1,
    'enabled':true,
    'isDefault':true
});

db.registrationcategories.insert({
    'name': 'Manufacturer',
    'type': 3,
    'category':1,
    'enabled':false
});
db.registrationcategories.insert({
    'name': 'Manufacturer',
    'type': 4,
    'category':1,
    'enabled':false
});
db.registrationcategories.insert({
    'name': 'Manufacturer',
    'type': 5,
    'category':1,
    'enabled':false
});
db.registrationcategories.insert({
    'name': 'Manufracturer',
    'type': 6,
    'category':1,
    'enabled':false
});
db.registrationcategories.insert({
    'name': 'Manufacturer',
    'type': 7,
    'category':1,
    'enabled':false
});

//Distributor
//Buy 1,
// Sell 2,
// Mediate 3,
// Buy,Sell 4,
//Buy, Mediate 5
//Sell ,Mediate 6
//Sell ,Buy ,Mediate 7


db.registrationcategories.insert({
    'name': 'Distributor',
    'type': 1,
    'category':1,
    'enabled':true,
    'isDefault':true
});
db.registrationcategories.insert({
    'name': 'Distributor',
    'type': 2,
    'category':1,
    'enabled':true
});

db.registrationcategories.insert({
    'name': 'Distributor',
    'type': 3,
    'category':1,
    'enabled':false
});
db.registrationcategories.insert({
    'name': 'Distributor',
    'type': 4,
    'category':1,
    'enabled':false
});
db.registrationcategories.insert({
    'name': 'Distributor',
    'type': 5,
    'category':1,
    'enabled':false
});
db.registrationcategories.insert({
    'name': 'Distributor',
    'type': 6,
    'category':1,
    'enabled':false
});
db.registrationcategories.insert({
    'name': 'Distributor',
    'type': 7,
    'category':1,
    'enabled':false
});

//Retailer
//Buy 1,
// Sell 2,
// Mediate 3,
// Buy,Sell 4,
//Buy, Mediate 5
//Sell ,Mediate 6
//Sell ,Buy ,Mediate 7


db.registrationcategories.insert({
    'name': 'Retailer',
    'type': 1,
    'category':2,
    'enabled':true,
    'isDefault':true
});
db.registrationcategories.insert({
    'name': 'Retailer',
    'type': 2,
    'category':2,
    'enabled':false
});

db.registrationcategories.insert({
    'name': 'Retailer',
    'type': 3,
    'category':2,
    'enabled':false
});
db.registrationcategories.insert({
    'name': 'Retailer',
    'type': 4,
    'category':2,
    'enabled':false
});
db.registrationcategories.insert({
    'name': 'Retailer',
    'type': 5,
    'category':2,
    'enabled':false
});
db.registrationcategories.insert({
    'name': 'Retailer',
    'type': 6,
    'category':2,
    'enabled':false
});
db.registrationcategories.insert({
    'name': 'Retailer',
    'type': 7,
    'category':2,
    'enabled':false
});

//Commission Agent
//Buy 1,
// Sell 2,
// Mediate 3,
// Buy,Sell 4,
//Buy, Mediate 5
//Sell ,Mediate 6
//Sell ,Buy ,Mediate 7


db.registrationcategories.insert({
    'name': 'Commission Agent',
    'type': 1,
    'category':2,
    'enabled':true,
    'isDefault':true
});
db.registrationcategories.insert({
    'name': 'Commission Agent',
    'type': 2,
    'category':2,
    'enabled':false
});

db.registrationcategories.insert({
    'name': 'Commission Agent',
    'type': 3,
    'category':2,
    'enabled':false
});
db.registrationcategories.insert({
    'name': 'Commission Agent',
    'type': 4,
    'category':2,
    'enabled':false
});
db.registrationcategories.insert({
    'name': 'Commission Agent',
    'type': 5,
    'category':2,
    'enabled':false
});
db.registrationcategories.insert({
    'name': 'Commission Agent',
    'type': 6,
    'category':2,
    'enabled':false
});
db.registrationcategories.insert({
    'name': 'Commission Agent',
    'type': 7,
    'category':2,
    'enabled':false
});

//Trader
//Buy 1,
// Sell 2,
// Mediate 3,
// Buy,Sell 4,
//Buy, Mediate 5
//Sell ,Mediate 6
//Sell ,Buy ,Mediate 7


db.registrationcategories.insert({
    'name': 'Trader',
    'type': 1,
    'category':2,
    'enabled':false
});
db.registrationcategories.insert({
    'name': 'Trader',
    'type': 2,
    'category':2,
    'enabled':false
});

db.registrationcategories.insert({
    'name': 'Trader',
    'type': 3,
    'category':2,
    'enabled':false
});
db.registrationcategories.insert({
    'name': 'Trader',
    'type': 4,
    'category':2,
    'enabled':false
});
db.registrationcategories.insert({
    'name': 'Trader',
    'type': 5,
    'category':2,
    'enabled':false
});
db.registrationcategories.insert({
    'name': 'Trader',
    'type': 6,
    'category':2,
    'enabled':false
});
db.registrationcategories.insert({
    'name': 'Trader',
    'type': 7,
    'category':2,
    'enabled':false
});

//Exporter
//Buy 1,
// Sell 2,
// Mediate 3,
// Buy,Sell 4,
//Buy, Mediate 5
//Sell ,Mediate 6
//Sell ,Buy ,Mediate 7


db.registrationcategories.insert({
    'name': 'Exporter',
    'type': 1,
    'category':2,
    'enabled':false
});
db.registrationcategories.insert({
    'name': 'Exporter',
    'type': 2,
    'category':2,
    'enabled':false
});

db.registrationcategories.insert({
    'name': 'Exporter',
    'type': 3,
    'category':2,
    'enabled':false
});
db.registrationcategories.insert({
    'name': 'Exporter',
    'type': 4,
    'category':2,
    'enabled':false
});
db.registrationcategories.insert({
    'name': 'Exporter',
    'type': 5,
    'category':2,
    'enabled':false
});
db.registrationcategories.insert({
    'name': 'Exporter',
    'type': 6,
    'category':2,
    'enabled':false
});
db.registrationcategories.insert({
    'name': 'Exporter',
    'type': 7,
    'category':2,
    'enabled':false
});


//Importer
//Buy 1,
// Sell 2,
// Mediate 3,
// Buy,Sell 4,
//Buy, Mediate 5
//Sell ,Mediate 6
//Sell ,Buy ,Mediate 7


db.registrationcategories.insert({
    'name': 'Importer',
    'type': 1,
    'category':2,
    'enabled':false
});
db.registrationcategories.insert({
    'name': 'Importer',
    'type': 2,
    'category':2,
    'enabled':false
});

db.registrationcategories.insert({
    'name': 'Importer',
    'type': 3,
    'category':2,
    'enabled':false
});
db.registrationcategories.insert({
    'name': 'Importer',
    'type': 4,
    'category':2,
    'enabled':false
});
db.registrationcategories.insert({
    'name': 'Importer',
    'type': 5,
    'category':2,
    'enabled':false
});
db.registrationcategories.insert({
    'name': 'Importer',
    'type': 6,
    'category':2,
    'enabled':false
});
db.registrationcategories.insert({
    'name': 'Importer',
    'type': 7,
    'category':2,
    'enabled':false
});

//FPO/FPC
//Buy 1,
// Sell 2,
// Mediate 3,
// Buy,Sell 4,
//Buy, Mediate 5
//Sell ,Mediate 6
//Sell ,Buy ,Mediate 7


db.registrationcategories.insert({
    'name': 'FPO/FPC',
    'type': 1,
    'category':2,
    'enabled':false
});
db.registrationcategories.insert({
    'name': 'FPO/FPC',
    'type': 2,
    'category':2,
    'enabled':false
});

db.registrationcategories.insert({
    'name': 'FPO/FPC',
    'type': 3,
    'category':2,
    'enabled':false
});
db.registrationcategories.insert({
    'name': 'FPO/FPC',
    'type': 4,
    'category':2,
    'enabled':false
});
db.registrationcategories.insert({
    'name': 'FPO/FPC',
    'type': 5,
    'category':2,
    'enabled':false
});
db.registrationcategories.insert({
    'name': 'FPO/FPC',
    'type': 6,
    'category':2,
    'enabled':false
});
db.registrationcategories.insert({
    'name': 'FPO/FPC',
    'type': 7,
    'category':2,
    'enabled':false
});
var rice={ 'name': 'Rice',categories:[],segmentImageURL1:'modules/categories/img/segments/rice.jpg'};
db.categories.find({name: /rice/i,type:'SubCategory2'}).forEach(function (categoryDoc) {
    rice.categories.push({category:categoryDoc._id});

});
db.businesssegments.insert(rice);
var coffee={ 'name': 'Coffee',categories:[],segmentImageURL1:'modules/categories/img/segments/coffee.jpg'};
db.categories.find({name: /Coffee/i,type:'SubCategory2'}).forEach(function (categoryDoc) {
    coffee.categories.push({category:categoryDoc._id});
});
db.businesssegments.insert(coffee);

var others={ 'name': 'Other',categories:[],isSpecific:true};
db.businesssegments.insert(others);

db.taxgroups.insert({
    'name':'0% GST',
    'CGST':0,
    'SGST': 0,
    'IGST':0
});
db.categories.update({
    'name': 'Corn',
    'code':'CORNG',
    'type': 'SubCategory2'
},{$addToSet: {'taxGroups': db.taxgroups.findOne({'name':'0% GST'})._id}});

db.categories.update({
    'name': 'Wheat',
    'code':'WHEAT',
    'type': 'SubCategory2'
},{$addToSet: {'taxGroups': db.taxgroups.findOne({'name':'0% GST'})._id}});

db.categories.update({
    'name': 'Coffee Berries',
    'code':'COBRS',
    'type': 'SubCategory2'
},{$addToSet: {'taxGroups': db.taxgroups.findOne({'name':'0% GST'})._id}});

db.categories.update({
    'name': 'Dry Coconut',
    'code':'DRYCN',
    'type': 'SubCategory2'
},{$addToSet: {'taxGroups': db.taxgroups.findOne({'name':'0% GST'})._id}});

db.categories.update({
    'name': 'Honey',
    'code':'HONEY',
    'type': 'SubCategory2'
},{$addToSet: {'taxGroups': db.taxgroups.findOne({'name':'0% GST'})._id}});

db.categories.update({
    'name': 'Oats',
    'code':'OATSS',
    'type': 'SubCategory2'
},{$addToSet: {'taxGroups': db.taxgroups.findOne({'name':'0% GST'})._id}});

db.categories.update({
    'name': 'Turmeric',
    'code':'TURMC',
    'type': 'SubCategory2'
},{$addToSet: {'taxGroups': db.taxgroups.findOne({'name':'0% GST'})._id}});

db.categories.update({
    'name': 'Ginger',
    'code': 'GINGR',
    'type': 'SubCategory2'
},{$addToSet: {'taxGroups': db.taxgroups.findOne({'name':'0% GST'})._id}});

db.categories.update({
    'name': 'Salt',
    'code': 'SALTS',
    'type': 'SubCategory2'
},{$addToSet: {'taxGroups': db.taxgroups.findOne({'name':'0% GST'})._id}});

db.categories.update({
    'name': 'Tamarind',
    'code': 'TMRND',
    'type': 'SubCategory2'
},{$addToSet: {'taxGroups': db.taxgroups.findOne({'name':'0% GST'})._id}});

db.categories.update({
    'name': 'Sorghum Bicolor',
    'code': 'SGMBI',
    'type': 'SubCategory2'
},{$addToSet: {'taxGroups': db.taxgroups.findOne({'name':'0% GST'})._id}});

db.categories.update({
    'name': 'Green Coffee Beans',
    'code': 'GCBNS',
    'type': 'SubCategory2'
},{$addToSet: {'taxGroups': db.taxgroups.findOne({'name':'0% GST'})._id}});

db.categories.update({
    'name': 'Garlic',
    'code': 'GARLC',
    'type': 'SubCategory2'
},{$addToSet: {'taxGroups': db.taxgroups.findOne({'name':'0% GST'})._id}});

db.categories.update({
    'name': 'Onion',
    'code': 'ONION',
    'type': 'SubCategory2'
},{$addToSet: {'taxGroups': db.taxgroups.findOne({'name':'0% GST'})._id}});

db.categories.update({
    'name': 'Potato',
    'code': 'POTAT',
    'type': 'SubCategory2'
},{$addToSet: {'taxGroups': db.taxgroups.findOne({'name':'0% GST'})._id}});

db.categories.update({
    'name': 'Lemon',
    'code': 'LEMON',
    'type': 'SubCategory2'
},{$addToSet: {'taxGroups': db.taxgroups.findOne({'name':'0% GST'})._id}});

db.categories.update({
    'name': 'Anjeer (Fig)',
    'code': 'ANJER',
    'type': 'SubCategory2'
},{$addToSet: {'taxGroups': db.taxgroups.findOne({'name':'0% GST'})._id}});

db.categories.update({
    'name': 'Apricot',
    'code': 'APRCT',
    'type': 'SubCategory2'
},{$addToSet: {'taxGroups': db.taxgroups.findOne({'name':'0% GST'})._id}});

db.categories.update({
    'name': 'Dates',
    'code':'DATES',
    'type': 'SubCategory2'
},{$addToSet: {'taxGroups': db.taxgroups.findOne({'name':'0% GST'})._id}});

db.categories.update({
    'name': 'Melon Seeds',
    'code': 'MLNSD',
    'type': 'SubCategory2'
},{$addToSet: {'taxGroups': db.taxgroups.findOne({'name':'0% GST'})._id}});

db.categories.update({
    'name': 'Coriander',
    'code': 'CRNDR',
    'type': 'SubCategory2'
},{$addToSet: {'taxGroups': db.taxgroups.findOne({'name':'0% GST'})._id}});

db.categories.update({
    'name': 'Sunflower Seeds',
    'code': 'SUNSD',
    'type': 'SubCategory2'
},{$addToSet: {'taxGroups': db.taxgroups.findOne({'name':'0% GST'})._id}});

db.categories.update({
    'name': 'Groundnut',
    'code': 'GRDNT',
    'type': 'SubCategory2'
},{$addToSet: {'taxGroups': db.taxgroups.findOne({'name':'0% GST'})._id}});

db.categories.update({
    'name': 'Soya Bean',
    'code': 'SOYBN',
    'type': 'SubCategory2'
},{$addToSet: {'taxGroups': db.taxgroups.findOne({'name':'0% GST'})._id}});

db.categories.update({
    'name': 'Sesame',
    'code': 'SESAM',
    'type': 'SubCategory2'
},{$addToSet: {'taxGroups': db.taxgroups.findOne({'name':'0% GST'})._id}});

db.categories.update({
    'name': 'Cotton Seeds',
    'code': 'COTSD',
    'type': 'SubCategory2'
},{$addToSet: {'taxGroups': db.taxgroups.findOne({'name':'0% GST'})._id}});

db.categories.update({
    'name': 'Poppy Seeds',
    'code': 'POYSD',
    'type': 'SubCategory2'
},{$addToSet: {'taxGroups': db.taxgroups.findOne({'name':'0% GST'})._id}});

db.categories.update({
    'name': 'Rice',
    'code': 'RICEG',
    'type': 'SubCategory2'
},{$addToSet: {'taxGroups': db.taxgroups.findOne({'name':'0% GST'})._id}});

db.categories.update({
    'name': 'Channa Dal',
    'code': 'CHDAL',
    'type': 'SubCategory2'
},{$addToSet: {'taxGroups': db.taxgroups.findOne({'name':'0% GST'})._id}});

db.categories.update({
    'name': 'Kabuli Chana Dal Brown',
    'code': 'KBDAL',
    'type': 'SubCategory2'
},{$addToSet: {'taxGroups': db.taxgroups.findOne({'name':'0% GST'})._id}});

db.categories.update({
    'name': 'Kabuli Chana Dal White',
    'code': 'KWDAL',
    'type': 'SubCategory2'
},{$addToSet: {'taxGroups': db.taxgroups.findOne({'name':'0% GST'})._id}});

db.categories.update({
    'name': 'Cowpea',
    'code': 'COWPE',
    'type': 'SubCategory2'
},{$addToSet: {'taxGroups': db.taxgroups.findOne({'name':'0% GST'})._id}});

db.categories.update({
    'name': 'Black Urad Dal Split',
    'code': 'BUDLS',
    'type': 'SubCategory2'
},{$addToSet: {'taxGroups': db.taxgroups.findOne({'name':'0% GST'})._id}});

db.categories.update({
    'name': 'Black Urad Dal Whole',
    'code': 'BUDLW',
    'type': 'SubCategory2'
},{$addToSet: {'taxGroups': db.taxgroups.findOne({'name':'0% GST'})._id}});

db.categories.update({
    'name': 'Finger Millet',
    'code': 'FGMLT',
    'type': 'SubCategory2'
},{$addToSet: {'taxGroups': db.taxgroups.findOne({'name':'0% GST'})._id}});


db.categories.update({
    'name': 'Foxtail Millet',
    'code': 'FTMLT',
    'type': 'SubCategory2'
},{$addToSet: {'taxGroups': db.taxgroups.findOne({'name':'0% GST'})._id}});

db.categories.update({
    'name': 'Kodo Millet',
    'code': 'KOMLT',
    'type': 'SubCategory2'
},{$addToSet: {'taxGroups': db.taxgroups.findOne({'name':'0% GST'})._id}});

db.categories.update({
    'name': 'Little Millet',
    'code': 'LTMLT',
    'type': 'SubCategory2'
},{$addToSet: {'taxGroups': db.taxgroups.findOne({'name':'0% GST'})._id}});

db.categories.update({
    'name': 'Pearl Millet',
    'code': 'PRMLT',
    'type': 'SubCategory2'
},{$addToSet: {'taxGroups': db.taxgroups.findOne({'name': '0% GST'})._id}});

db.categories.update({
    'name': 'Proso Millet',
    'code': 'PSMLT',
    'type': 'SubCategory2'
},{$addToSet: {'taxGroups': db.taxgroups.findOne({'name': '0% GST'})._id}});

db.taxgroups.insert({
    'name':'5% GST',
    'CGST':2.5,
    'SGST':2.5,
    'IGST': 5
});
db.categories.update({
    'name': 'Corn',
    'code':'CORNG',
    'type': 'SubCategory2'
},{$addToSet: {'taxGroups': db.taxgroups.findOne({'name':'5% GST'})._id}});

db.categories.update({
    'name': 'Wheat',
    'code':'WHEAT',
    'type': 'SubCategory2'
},{$addToSet: {'taxGroups': db.taxgroups.findOne({'name':'5% GST'})._id}});

db.categories.update({
    'name': 'Dry Coconut',
    'code':'DRYCN',
    'type': 'SubCategory2'
},{$addToSet: {'taxGroups': db.taxgroups.findOne({'name':'5% GST'})._id}});

db.categories.update({
    'name': 'Roasted Coffee Beans',
    'code':'RCBNS',
    'type': 'SubCategory2'
},{$addToSet: {'taxGroups': db.taxgroups.findOne({'name':'5% GST'})._id}});

db.categories.update({
    'name': 'Honey',
    'code':'HONEY',
    'type': 'SubCategory2'
},{$addToSet: {'taxGroups': db.taxgroups.findOne({'name':'5% GST'})._id}});

db.categories.update({
    'name': 'Oats',
    'code':'OATSS',
    'type': 'SubCategory2'
},{$addToSet: {'taxGroups': db.taxgroups.findOne({'name':'5% GST'})._id}});

db.categories.update({
    'name': 'Turmeric',
    'code': 'TURMC',
    'type': 'SubCategory2'
},{$addToSet: {'taxGroups': db.taxgroups.findOne({'name':'5% GST'})._id}});

db.categories.update({
    'name': 'Ginger',
    'code': 'GINGR',
    'type': 'SubCategory2'
},{$addToSet: {'taxGroups': db.taxgroups.findOne({'name':'5% GST'})._id}});

db.categories.update({
    'name': 'Cashew',
    'code':'CASHW',
    'type': 'SubCategory2'
},{$addToSet: {'taxGroups': db.taxgroups.findOne({'name':'5% GST'})._id}});

db.categories.update({
    'name': 'Cotton Yarn',
    'code': 'COTYN',
    'type': 'SubCategory2'
},{$addToSet: {'taxGroups': db.taxgroups.findOne({'name':'5% GST'})._id}});

db.categories.update({
    'name': 'Cotton Fabric',
    'code': 'COTFB',
    'type': 'SubCategory2'
},{$addToSet: {'taxGroups': db.taxgroups.findOne({'name':'5% GST'})._id}});

db.categories.update({
    'name': 'Silk Fabric',
    'code': 'SLKFB',
    'type': 'SubCategory2'
},{$addToSet: {'taxGroups': db.taxgroups.findOne({'name':'5% GST'})._id}});

db.categories.update({
    'name': 'Cotton Bales',
    'code': 'COTBL',
    'type': 'SubCategory2'
},{$addToSet: {'taxGroups': db.taxgroups.findOne({'name':'5% GST'})._id}});

db.categories.update({
    'name': 'Tea',
    'code': 'TEAPD',
    'type': 'SubCategory2'
},{$addToSet: {'taxGroups': db.taxgroups.findOne({'name':'5% GST'})._id}});

db.categories.update({
    'name': 'Coriander',
    'code': 'CRNDR',
    'type': 'SubCategory2'
},{$addToSet: {'taxGroups': db.taxgroups.findOne({'name':'5% GST'})._id}});

db.categories.update({
    'name': 'Sunflower Seeds',
    'code': 'SUNSD',
    'type': 'SubCategory2'
},{$addToSet: {'taxGroups': db.taxgroups.findOne({'name':'5% GST'})._id}});

db.categories.update({
    'name': 'Groundnut',
    'code': 'GRDNT',
    'type': 'SubCategory2'
},{$addToSet: {'taxGroups': db.taxgroups.findOne({'name':'5% GST'})._id}});

db.categories.update({
    'name': 'Sesame',
    'code': 'SESAM',
    'type': 'SubCategory2'
},{$addToSet: {'taxGroups': db.taxgroups.findOne({'name':'5% GST'})._id}});

db.categories.update({
    'name': 'Cotton Seeds',
    'code': 'COTSD',
    'type': 'SubCategory2'
},{$addToSet: {'taxGroups': db.taxgroups.findOne({'name':'5% GST'})._id}});
db.categories.update({
    'name': 'Black Pepper',
    'code': 'BLKPR',
    'type': 'SubCategory2'
},{$addToSet: {'taxGroups': db.taxgroups.findOne({'name':'5% GST'})._id}});

db.categories.update({
    'name': 'Rice',
    'code': 'RICEG',
    'type': 'SubCategory2'
},{$addToSet: {'taxGroups': db.taxgroups.findOne({'name':'5% GST'})._id}});


db.categories.update({
    'name': 'Channa Dal',
    'code': 'CHDAL',
    'type': 'SubCategory2'
},{$addToSet: {'taxGroups': db.taxgroups.findOne({'name':'5% GST'})._id}});

db.categories.update({
    'name': 'Kabuli Chana Dal Brown',
    'code': 'KBDAL',
    'type': 'SubCategory2'
},{$addToSet: {'taxGroups': db.taxgroups.findOne({'name':'5% GST'})._id}});


db.categories.update({
    'name': 'Kabuli Chana Dal White',
    'code': 'KWDAL',
    'type': 'SubCategory2'
},{$addToSet: {'taxGroups': db.taxgroups.findOne({'name':'5% GST'})._id}});

db.categories.update({
    'name': 'Green Moong Dal Split',
    'code': 'MGDLS',
    'type': 'SubCategory2'
},{$addToSet: {'taxGroups': db.taxgroups.findOne({'name':'5% GST'})._id}});

db.categories.update({
    'name': 'Green Moong Dal Whole',
    'code': 'MGDLW',
    'type': 'SubCategory2'
},{$addToSet: {'taxGroups': db.taxgroups.findOne({'name':'5% GST'})._id}});


db.categories.update({
    'name': 'Cowpea',
    'code': 'COWPE',
    'type': 'SubCategory2'
},{$addToSet: {'taxGroups': db.taxgroups.findOne({'name':'5% GST'})._id}});

db.categories.update({
    'name': 'Black Urad Dal Split',
    'code': 'BUDLS',
    'type': 'SubCategory2'
},{$addToSet: {'taxGroups': db.taxgroups.findOne({'name':'5% GST'})._id}});

db.categories.update({
    'name': 'Black Urad Dal Whole',
    'code': 'BUDLW',
    'type': 'SubCategory2'
},{$addToSet: {'taxGroups': db.taxgroups.findOne({'name':'5% GST'})._id}});

db.categories.update({
    'name': 'Finger Millet',
    'code': 'FGMLT',
    'type': 'SubCategory2'
},{$addToSet: {'taxGroups': db.taxgroups.findOne({'name':'5% GST'})._id}});

db.categories.update({
    'name': 'Wool Fabric',
    'code': 'WOLFB',
    'type': 'SubCategory2'
},{$addToSet: {'taxGroups': db.taxgroups.findOne({'name':'5% GST'})._id}});

db.categories.update({
    'name': 'Kodo Millet',
    'code': 'KOMLT',
    'type': 'SubCategory2'
},{$addToSet: {'taxGroups': db.taxgroups.findOne({'name':'5% GST'})._id}});


db.categories.update({
    'name': 'Little Millet',
    'code': 'LTMLT',
    'type': 'SubCategory2'
},{$addToSet: {'taxGroups': db.taxgroups.findOne({'name':'5% GST'})._id}});

db.categories.update({
    'name': 'Pearl Millet',
    'code': 'PRMLT',
    'type': 'SubCategory2'
},{$addToSet: {'taxGroups': db.taxgroups.findOne({'name':'5% GST'})._id}});

db.categories.update({
    'name': 'Proso Millet',
    'code': 'PSMLT',
    'type': 'SubCategory2'
},{$addToSet: {'taxGroups': db.taxgroups.findOne({'name':'5% GST'})._id}});

db.categories.update({
    'name': 'Silk Yarn',
    'code': 'SLKYN',
    'type': 'SubCategory2'
},{$addToSet: {'taxGroups': db.taxgroups.findOne({'name':'5% GST'})._id}});

db.categories.update({
    'name': 'Wool Yarn',
    'code': 'WOLYN',
    'type': 'SubCategory2'
},{$addToSet: {'taxGroups': db.taxgroups.findOne({'name':'5% GST'})._id}});
db.taxgroups.insert({
    'name':'12% GST',
    'CGST':6,
    'SGST':6,
    'IGST': 12
});
db.categories.update({
    'name': 'Oral Care',
    'code': 'ORALC',
    'type': 'SubCategory2'
},{$addToSet: {'taxGroups': db.taxgroups.findOne({'name':'12% GST'})._id}});

db.categories.update({
    'name': 'Husk',
    'code':'HUSK',
    'type': 'SubCategory2'
},{$addToSet: {'taxGroups': db.taxgroups.findOne({'name':'12% GST'})._id}});

db.categories.update({
    'name': 'Dairy',
    'code':'DAIRY',
    'type': 'SubCategory2'
},{$addToSet: {'taxGroups': db.taxgroups.findOne({'name':'12% GST'})._id}});

db.categories.update({
    'name': 'Dry Grapes (Raisins)',
    'code':'DGRPS',
    'type': 'SubCategory2'
},{$addToSet: {'taxGroups': db.taxgroups.findOne({'name':'12% GST'})._id}});

db.categories.update({
    'name': 'Jams',
    'code':'JAMSS',
    'type': 'SubCategory2'
},{$addToSet: {'taxGroups': db.taxgroups.findOne({'name':'12% GST'})._id}});

db.categories.update({
    'name': 'Namkeen',
    'code':'NAMKN',
    'type': 'SubCategory2'
},{$addToSet: {'taxGroups': db.taxgroups.findOne({'name':'12% GST'})._id}});

db.categories.update({
    'name': 'Almonds (Badam)',
    'code': 'ALMND',
    'type': 'SubCategory2'
},{$addToSet: {'taxGroups': db.taxgroups.findOne({'name':'12% GST'})._id}});

db.categories.update({
    'name': 'Sauces',
    'code': 'SAUCE',
    'type': 'SubCategory2'
},{$addToSet: {'taxGroups': db.taxgroups.findOne({'name':'12% GST'})._id}});

db.categories.update({
    'name': 'Oral Care',
    'code': 'ORALC',
    'type': 'SubCategory2'
},{$addToSet: {'taxGroups': db.taxgroups.findOne({'name':'12% GST'})._id}});

db.taxgroups.insert({
    'name':'18% GST',
    'CGST':9,
    'SGST':9,
    'IGST': 18
});
db.categories.update({
    'name': 'Honey',
    'code':'HONEY',
    'type': 'SubCategory2'
},{$addToSet: {'taxGroups': db.taxgroups.findOne({'name':'18% GST'})._id}});

db.categories.update({
    'name': 'Ice Creams',
    'code':'ICCRM',
    'type': 'SubCategory2'
},{$addToSet: {'taxGroups': db.taxgroups.findOne({'name':'18% GST'})._id}});


db.categories.update({
    'name': 'Biscuits And Cookies',
    'code': 'BISCK',
    'type': 'SubCategory2'
},{$addToSet: {'taxGroups': db.taxgroups.findOne({'name':'18% GST'})._id}});

db.categories.update({
    'name': 'Noodles',
    'code':'NOODL',
    'type': 'SubCategory2'
},{$addToSet: {'taxGroups': db.taxgroups.findOne({'name':'18% GST'})._id}});

db.categories.update({
    'name': 'Hair Care',
    'code': 'HAIRC',
    'type': 'SubCategory2'
},{$addToSet: {'taxGroups': db.taxgroups.findOne({'name':'18% GST'})._id}});


db.categories.update({
    'name': 'Laundry',
    'code': 'LNDRY',
    'type': 'SubCategory2'
},{$addToSet: {'taxGroups': db.taxgroups.findOne({'name':'18% GST'})._id}});

db.categories.update({
    'name': 'Kitchen and Dining',
    'code': 'KTDNG',
    'type': 'SubCategory2'
},{$addToSet: {'taxGroups': db.taxgroups.findOne({'name':'18% GST'})._id}});


db.taxgroups.insert({
    'name':'28% GST',
    'CGST':14,
    'SGST':14,
    'IGST': 28
});
db.categories.update({
    'name': 'Candies and Chocolates',
    'code': 'CANCH',
    'type': 'SubCategory2'
},{$addToSet: {'taxGroups': db.taxgroups.findOne({'name':'28% GST'})._id}});

db.categories.update({
    'name': 'Laundry',
    'code': 'LNDRY',
    'type': 'SubCategory2'
},{$addToSet: {'taxGroups': db.taxgroups.findOne({'name':'28% GST'})._id}});

db.categories.update({
    'name': 'Perfumes and Deodrants',
    'code': 'PERDT',
    'type': 'SubCategory2'
},{$addToSet: {'taxGroups': db.taxgroups.findOne({'name':'28% GST'})._id}});

db.categories.update({
    'name': 'Toilets and Surface Cleaners',
    'code': 'TOASC',
    'type': 'SubCategory2'
},{$addToSet: {'taxGroups': db.taxgroups.findOne({'name':'28% GST'})._id}});

db.categories.update({
    'name': 'Detergents',
    'code': 'DTGNT',
    'type': 'SubCategory2'
},{$addToSet: {'taxGroups': db.taxgroups.findOne({'name':'28% GST'})._id}});

db.categories.update({
    'name': 'Skin Care',
    'code': 'SKINC',
    'type': 'SubCategory2'
},{$addToSet: {'taxGroups': db.taxgroups.findOne({'name':'28% GST'})._id}});
