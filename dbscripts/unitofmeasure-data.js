/*
MongoDB Script for inserting the UnitOfMeasures
*/

// Remove unitofmeasures Data
db.unitofmeasures.remove({});

db.unitofmeasures.insert({
    'name': 'Ampules',
    'symbol': 'Amp',
    'uqcCode': 'AMP',
    'description': 'AMPULES',
    'quantityType': 'Measure',
    'type': 'Simple',
    'disabled': false,
    'deleted': false
});

db.unitofmeasures.insert({
    'name': 'Bags',
    'symbol': 'Bag',
    'uqcCode': 'BAG',
    'description': 'BAGS',
    'quantityType': 'Measure',
    'type': 'Simple',
    'disabled': false,
    'deleted': false,
    'disabled': false,
    'deleted': false
});
db.unitofmeasures.insert({
    'name': 'Bale',
    'symbol': 'Bal',
    'uqcCode': 'BAL',
    'description': 'BALE',
    'quantityType': 'Measure',
    'type': 'Simple',
    'disabled': false,
    'deleted': false
});
db.unitofmeasures.insert({
    'name': 'Bundles',
    'symbol': 'Bdl',
    'uqcCode': 'BDL',
    'description': 'BUNDLES',
    'quantityType': 'Measure',
    'type': 'Simple',
    'disabled': false,
    'deleted': false
});
db.unitofmeasures.insert({
    'name': 'Buckles',
    'symbol': 'Bkl',
    'uqcCode': 'BKL',
    'description': 'BUCKLES',
    'quantityType': 'Measure',
    'type': 'Simple',
    'disabled': false,
    'deleted': false
});
db.unitofmeasures.insert({
    'name': 'Billion Of Units',
    'symbol': 'Bou',
    'uqcCode': 'BOU',
    'description': 'BILLIONS OF UNITS',
    'quantityType': 'Measure',
    'type': 'Simple',
    'disabled': false,
    'deleted': false
});
db.unitofmeasures.insert({
    'name': 'Box',
    'symbol': 'Box',
    'uqcCode': 'BOX',
    'description': 'BOX',
    'quantityType': 'Measure',
    'type': 'Simple',
    'disabled': false,
    'deleted': false
});
db.unitofmeasures.insert({
    'name': 'Bottles',
    'symbol': 'Btl',
    'uqcCode': 'BTL',
    'description': 'BOTTLES',
    'quantityType': 'Measure',
    'type': 'Simple',
    'disabled': false,
    'deleted': false
});
db.unitofmeasures.insert({
    'name': 'Bunches',
    'symbol': 'Bun',
    'uqcCode': 'BUN',
    'description': 'BUNCHES',
    'quantityType': 'Measure',
    'type': 'Simple',
    'disabled': false,
    'deleted': false
});
db.unitofmeasures.insert({
    'name': 'Cans',
    'symbol': 'Can',
    'uqcCode': 'CAN',
    'description': 'CANS',
    'quantityType': 'Measure',
    'type': 'Simple',
    'disabled': false,
    'deleted': false
});
db.unitofmeasures.insert({
    'name': 'Cubic Meter',
    'symbol': 'Cbm',
    'uqcCode': 'CBM',
    'description': 'CUBIC METER',
    'quantityType': 'Volume',
    'type': 'Simple',
    'disabled': false,
    'deleted': false
});
db.unitofmeasures.insert({
    'name': 'Cubic Centimeter',
    'symbol': 'Ccm',
    'uqcCode': 'CCM',
    'description': 'CUBIC CENTIMETER',
    'quantityType': 'Volume',
    'type': 'Simple',
    'disabled': false,
    'deleted': false
});
db.unitofmeasures.insert({
    'name': 'Centimeter',
    'symbol': 'Cms',
    'uqcCode': 'CMS',
    'description': 'CENTIMETER',
    'quantityType': 'Length',
    'type': 'Simple',
    'disabled': false,
    'deleted': false
});
db.unitofmeasures.insert({
    'name': 'Carat',
    'symbol': 'Crt',
    'uqcCode': 'CRT',
    'description': 'CARAT',
    'quantityType': 'Measure',
    'type': 'Simple',
    'disabled': false,
    'deleted': false
});

db.unitofmeasures.insert({
    'name': 'Cartons',
    'symbol': 'Ctn',
    'uqcCode': 'CTN',
    'description': 'CARTONS',
    'quantityType': 'Measure',
    'type': 'Simple',
    'disabled': false,
    'deleted': false
});
db.unitofmeasures.insert({
    'name': 'Dozen',
    'symbol': 'Doz',
    'uqcCode': 'DOZ',
    'description': 'DOZEN',
    'quantityType': 'Measure',
    'type': 'Simple',
    'disabled': false,
    'deleted': false
});
db.unitofmeasures.insert({
    'name': 'Drum',
    'symbol': 'Drm',
    'uqcCode': 'DRM',
    'description': 'DRUM',
    'quantityType': 'Measure',
    'type': 'Simple',
    'disabled': false,
    'deleted': false
});
db.unitofmeasures.insert({
    'name': 'Feet',
    'symbol': 'Fts',
    'uqcCode': 'FTS',
    'description': 'FEET',
    'quantityType': 'Length',
    'type': 'Simple',
    'disabled': false,
    'deleted': false
});
db.unitofmeasures.insert({
    'name': 'Great Gross',
    'symbol': 'Ggr',
    'uqcCode': 'GGR',
    'description': 'GREAT GROSS',
    'quantityType': 'Measure',
    'type': 'Simple',
    'disabled': false,
    'deleted': false
});
db.unitofmeasures.insert({
    'name': 'Grams',
    'symbol': 'Gms',
    'uqcCode': 'GMS',
    'description': 'GRAMS',
    'quantityType': 'Weight',
    'type': 'Simple',
    'disabled': false,
    'deleted': false
});
db.unitofmeasures.insert({
    'name': 'Gross',
    'symbol': 'Grs',
    'uqcCode': 'GRS',
    'description': 'GROSS',
    'quantityType': 'Measure',
    'type': 'Simple',
    'disabled': false,
    'deleted': false
});
db.unitofmeasures.insert({
    'name': 'Gross Yards',
    'symbol': 'Gyd',
    'uqcCode': 'GYD',
    'description': 'GROSS YARDS',
    'quantityType': 'Length',
    'type': 'Simple',
    'disabled': false,
    'deleted': false
});
db.unitofmeasures.insert({
    'name': 'Kilogram Activity',
    'symbol': 'Kga',
    'uqcCode': 'KGA',
    'description': 'KILOGRAM ACTIVITY',
    'quantityType': 'Measure',
    'type': 'Simple',
    'disabled': false,
    'deleted': false
});
db.unitofmeasures.insert({
    'name': 'Kilogram Base',
    'symbol': 'Kgb',
    'uqcCode': 'KGB',
    'description': 'KILOGRAM BASE',
    'quantityType': 'Measure',
    'type': 'Simple',
    'disabled': false,
    'deleted': false
});
db.unitofmeasures.insert({
    'name': 'Kilograms',
    'symbol': 'Kgs',
    'uqcCode': 'KGS',
    'description': 'KILOGRAMS',
    'quantityType': 'Weight',
    'type': 'Simple',
    'disabled': false,
    'deleted': false
});
db.unitofmeasures.insert({
    'name': 'Kits',
    'symbol': 'Kit',
    'uqcCode': 'KIT',
    'description': 'KITS',
    'quantityType': 'Measure',
    'type': 'Simple',
    'disabled': false,
    'deleted': false
});
db.unitofmeasures.insert({
    'name': 'Kiloliter',
    'symbol': 'Klr',
    'uqcCode': 'KLR',
    'description': 'KILOLITER',
    'quantityType': 'Volume',
    'type': 'Simple',
    'disabled': false,
    'deleted': false
});
db.unitofmeasures.insert({
    'name': 'Kilometeres',
    'symbol': 'Kme',
    'uqcCode': 'KME',
    'description': 'KILOMETERS',
    'quantityType': 'Length',
    'type': 'Simple',
    'disabled': false,
    'deleted': false
});
db.unitofmeasures.insert({
    'name': 'Pounds',
    'symbol': 'Lbs',
    'uqcCode': 'LBS',
    'description': 'POUNDS',
    'quantityType': 'Weight',
    'type': 'Simple',
    'disabled': false,
    'deleted': false
});
db.unitofmeasures.insert({
    'name': 'Litres',
    'symbol': 'Ltr',
    'uqcCode': 'LTR',
    'description': 'LITERS',
    'quantityType': 'Volume',
    'type': 'Simple',
    'disabled': false,
    'deleted': false
});
db.unitofmeasures.insert({
    'name': 'Miligrams',
    'symbol': 'Mgs',
    'uqcCode': 'MGS',
    'description': 'MILIGRAMS',
    'quantityType': 'Weight',
    'type': 'Simple',
    'disabled': false,
    'deleted': false
});
db.unitofmeasures.insert({
    'name': 'Million Keasergen',
    'symbol': 'Mku',
    'uqcCode': 'MKU',
    'description': 'MILLION KEASERGEN',
    'quantityType': 'Measure',
    'type': 'Simple',
    'disabled': false,
    'deleted': false
});
db.unitofmeasures.insert({
    'name': 'Milliliter',
    'symbol': 'Mlt',
    'uqcCode': 'MLT',
    'description': 'MILLILITER',
    'quantityType': 'Volume',
    'type': 'Simple',
    'disabled': false,
    'deleted': false
});
db.unitofmeasures.insert({
    'name': 'Millions Of Unit',
    'symbol': 'Mou',
    'uqcCode': 'MOU',
    'description': 'MILLIONS OF UNIT',
    'quantityType': 'Measure',
    'type': 'Simple',
    'disabled': false,
    'deleted': false
});
db.unitofmeasures.insert({
    'name': 'Meter',
    'symbol': 'Mtr',
    'uqcCode': 'MTR',
    'description': 'METER',
    'quantityType': 'Length',
    'type': 'Simple',
    'disabled': false,
    'deleted': false
});
db.unitofmeasures.insert({
    'name': 'Metric Ton',
    'symbol': 'Mts',
    'uqcCode': 'MTS',
    'description': 'METRIC TON',
    'quantityType': 'Weight',
    'type': 'Simple',
    'disabled': false,
    'deleted': false
});
db.unitofmeasures.insert({
    'name': 'Million Units',
    'symbol': 'Mus',
    'uqcCode': 'MUS',
    'description': 'MILLION UNITS',
    'quantityType': 'Measure',
    'type': 'Simple',
    'disabled': false,
    'deleted': false
});
db.unitofmeasures.insert({
    'name': 'Number',
    'symbol': 'Nos',
    'uqcCode': 'NOS',
    'description': 'NUMBER',
    'quantityType': 'Measure',
    'type': 'Simple',
    'disabled': false,
    'deleted': false
});
db.unitofmeasures.insert({
    'name': 'Packs',
    'symbol': 'Pac',
    'uqcCode': 'PAC',
    'description': 'PACKS',
    'quantityType': 'Measure',
    'type': 'Simple',
    'disabled': false,
    'deleted': false
});
db.unitofmeasures.insert({
    'name': 'Pieces',
    'symbol': 'Pcs',
    'uqcCode': 'PCS',
    'description': 'PIECES',
    'quantityType': 'Measure',
    'type': 'Simple',
    'disabled': false,
    'deleted': false
});
db.unitofmeasures.insert({
    'name': 'Pairs',
    'symbol': 'Prs',
    'uqcCode': 'PRS',
    'description': 'PAIRS',
    'quantityType': 'Measure',
    'type': 'Simple',
    'disabled': false,
    'deleted': false
});
db.unitofmeasures.insert({
    'name': 'Quintal',
    'symbol': 'Qtl',
    'uqcCode': 'QTL',
    'description': 'QUINTAL',
    'quantityType': 'Weight',
    'type': 'Simple',
    'disabled': false,
    'deleted': false
});
db.unitofmeasures.insert({
    'name': 'Rolls',
    'symbol': 'Rls',
    'uqcCode': 'RLS',
    'description': 'ROLLS',
    'quantityType': 'Measure',
    'type': 'Simple',
    'disabled': false,
    'deleted': false
});
db.unitofmeasures.insert({
    'name': 'Roll',
    'symbol': 'Rol',
    'uqcCode': 'ROL',
    'description': 'ROLL',
    'quantityType': 'Measure',
    'type': 'Simple',
    'disabled': false,
    'deleted': false
});
db.unitofmeasures.insert({
    'name': 'Sets',
    'symbol': 'Set',
    'uqcCode': 'SET',
    'description': 'SETS',
    'quantityType': 'Measure',
    'type': 'Simple',
    'disabled': false,
    'deleted': false
});
db.unitofmeasures.insert({
    'name': 'Square Feet',
    'symbol': 'Sqf',
    'uqcCode': 'SQF',
    'description': 'SQUARE FEET',
    'quantityType': 'Area',
    'type': 'Simple',
    'disabled': false,
    'deleted': false
});
db.unitofmeasures.insert({
    'name': 'Square Yards',
    'symbol': 'Sqy',
    'uqcCode': 'SQY',
    'description': 'SQUARE YARDS',
    'quantityType': 'Area',
    'type': 'Simple',
    'disabled': false,
    'deleted': false
});
db.unitofmeasures.insert({
    'name': 'Square Meter',
    'symbol': 'Sqm',
    'uqcCode': 'SQM',
    'description': 'SQUARE METER',
    'quantityType': 'Area',
    'type': 'Simple',
    'disabled': false,
    'deleted': false
});
db.unitofmeasures.insert({
    'name': 'Tablets',
    'symbol': 'Tbs',
    'uqcCode': 'TBS',
    'description': 'TABLETS',
    'quantityType': 'Measure',
    'type': 'Simple',
    'disabled': false,
    'deleted': false
});

db.unitofmeasures.insert({
    'name': 'Ten Gross',
    'symbol': 'Tgm',
    'uqcCode': 'TGM',
    'description': 'TEN GROSS',
    'quantityType': 'Measure',
    'type': 'Simple',
    'disabled': false,
    'deleted': false
});
db.unitofmeasures.insert({
    'name': 'Thousands',
    'symbol': 'Thd',
    'uqcCode': 'THD',
    'description': 'THOUSANDS',
    'quantityType': 'Measure',
    'type': 'Simple',
    'disabled': false,
    'deleted': false
});
db.unitofmeasures.insert({
    'name': 'Great Britain Ton',
    'symbol': 'Ton',
    'uqcCode': 'TON',
    'description': 'GREAT BRITAIN TON',
    'quantityType': 'Weight',
    'type': 'Simple',
    'disabled': false,
    'deleted': false
});
db.unitofmeasures.insert({
    'name': 'Tubes',
    'symbol': 'Tub',
    'uqcCode': 'TUB',
    'description': 'TUBES',
    'quantityType': 'Measure',
    'type': 'Simple',
    'disabled': false,
    'deleted': false
});
db.unitofmeasures.insert({
    'name': 'US Gallons',
    'symbol': 'Ugs',
    'uqcCode': 'UGS',
    'description': 'US GALLONS',
    'quantityType': 'Volume',
    'type': 'Simple',
    'disabled': false,
    'deleted': false
});
db.unitofmeasures.insert({
    'name': 'Units',
    'symbol': 'Unt',
    'uqcCode': 'UNT',
    'description': 'UNITS',
    'quantityType': 'Measure',
    'type': 'Simple',
    'disabled': false,
    'deleted': false
});
db.unitofmeasures.insert({
    'name': 'Vials',
    'symbol': 'Vls',
    'uqcCode': 'VLS',
    'description': 'VIALS',
    'quantityType': 'Measure',
    'type': 'Simple',
    'disabled': false,
    'deleted': false
});
db.unitofmeasures.insert({
    'name': 'Yards',
    'symbol': 'Yds',
    'uqcCode': 'YDS',
    'description': 'YARDS',
    'quantityType': 'Length',
    'type': 'Simple',
    'disabled': false,
    'deleted': false
});

db.unitofmeasures.insert({
    'name': 'Bag Of 25Kgs',
    'symbol': 'Bag (25Kgs)',
    'conversion': 25,
    'description': 'Bag of 25Kgs',
    'quantityType': 'Weight',
    'type': 'Compound',
    'firstUnitOfMeasure': db.unitofmeasures.findOne({'name': 'Bags', 'symbol': 'Bag', 'type': 'Simple'})._id,
    'secondUnitOfMeasure': db.unitofmeasures.findOne({'name': 'Kilograms', 'symbol': 'Kgs', 'type': 'Simple'})._id,
    'disabled': false,
    'deleted': false
});

db.unitofmeasures.insert({
    'name': 'Bag Of 10Kgs',
    'symbol': 'Bag (10Kgs)',
    'conversion': 10,
    'description': 'Bag of 10Kgs',
    'quantityType': 'Weight',
    'type': 'Compound',
    'firstUnitOfMeasure': db.unitofmeasures.findOne({'name': 'Bags', 'symbol': 'Bag', 'type': 'Simple'})._id,
    'secondUnitOfMeasure': db.unitofmeasures.findOne({'name': 'Kilograms', 'symbol': 'Kgs', 'type': 'Simple'})._id,
    'disabled': false,
    'deleted': false
});

db.unitofmeasures.insert({
    'name': 'Bag Of 5Kgs',
    'symbol': 'Bag (5Kgs)',
    'conversion': 5,
    'description': 'Bag of 5Kgs',
    'quantityType': 'Weight',
    'type': 'Compound',
    'firstUnitOfMeasure': db.unitofmeasures.findOne({'name': 'Bags', 'symbol': 'Bag', 'type': 'Simple'})._id,
    'secondUnitOfMeasure': db.unitofmeasures.findOne({'name': 'Kilograms', 'symbol': 'Kgs', 'type': 'Simple'})._id,
    'disabled': false,
    'deleted': false
});

db.unitofmeasures.insert({
    'name': 'Pack Of 2Kgs',
    'symbol': 'Pack (2Kgs)',
    'conversion': 2,
    'description': 'Pack of 2Kgs',
    'type': 'Compound',
    'quantityType': 'Weight',
    'firstUnitOfMeasure': db.unitofmeasures.findOne({'name': 'Packs', 'symbol': 'Pac', 'type': 'Simple'})._id,
    'secondUnitOfMeasure': db.unitofmeasures.findOne({'name': 'Kilograms', 'symbol': 'Kgs', 'type': 'Simple'})._id,
    'disabled': false,
    'deleted': false
});

db.unitofmeasures.insert({
    'name': 'Pack Of 1Kgs',
    'symbol': 'Pack (1Kgs)',
    'conversion': 1,
    'description': 'Pack of 1Kgs',
    'type': 'Compound',
    'quantityType': 'Weight',
    'firstUnitOfMeasure': db.unitofmeasures.findOne({'name': 'Packs', 'symbol': 'Pac', 'type': 'Simple'})._id,
    'secondUnitOfMeasure': db.unitofmeasures.findOne({'name': 'Kilograms', 'symbol': 'Kgs', 'type': 'Simple'})._id,
    'disabled': false,
    'deleted': false
});

db.unitofmeasures.insert({
    'name': 'Pack Of 0.5Kgs',
    'symbol': 'Pack (0.5Kgs)',
    'conversion': 0.5,
    'description': 'Pack of 0.5Kgs',
    'type': 'Compound',
    'quantityType': 'Weight',
    'firstUnitOfMeasure': db.unitofmeasures.findOne({'name': 'Packs', 'symbol': 'Pac', 'type': 'Simple'})._id,
    'secondUnitOfMeasure': db.unitofmeasures.findOne({'name': 'Kilograms', 'symbol': 'Kgs', 'type': 'Simple'})._id,
    'disabled': false,
    'deleted': false
});
db.unitofmeasures.insert({
    'name': 'Pack Of 1Ltr',
    'symbol': 'Pack (1Ltr)',
    'conversion': 1,
    'description': 'Pack of 1Ltr',
    'type': 'Compound',
    'quantityType': 'Volume',
    'firstUnitOfMeasure': db.unitofmeasures.findOne({'name': 'Packs', 'symbol': 'Pac', 'type': 'Simple'})._id,
    'secondUnitOfMeasure': db.unitofmeasures.findOne({'name': 'Litres', 'symbol': 'Ltr', 'type': 'Simple'})._id,
    'disabled': false,
    'deleted': false
});

db.unitofmeasures.insert({
    'name': 'Bottle Of 1Ltr',
    'symbol': 'Bottle (1Ltr)',
    'conversion': 1,
    'description': 'Bottle Of 1Ltr',
    'type': 'Compound',
    'quantityType': 'Volume',
    'firstUnitOfMeasure': db.unitofmeasures.findOne({'name': 'Bottles', 'symbol': 'Btl', 'type': 'Simple'})._id,
    'secondUnitOfMeasure': db.unitofmeasures.findOne({'name': 'Litres', 'symbol': 'Ltr', 'type': 'Simple'})._id,
    'disabled': false,
    'deleted': false
});

db.unitofmeasures.insert({
    'name': 'Bottle Of 0.5Ltr',
    'symbol': 'Bottle (0.5Ltr)',
    'conversion': 0.5,
    'description': 'Bottle Of 0.5Ltr',
    'type': 'Compound',
    'quantityType': 'Volume',
    'firstUnitOfMeasure': db.unitofmeasures.findOne({'name': 'Bottles', 'symbol': 'Btl', 'type': 'Simple'})._id,
    'secondUnitOfMeasure': db.unitofmeasures.findOne({'name': 'Litres', 'symbol': 'Ltr', 'type': 'Simple'})._id,
    'disabled': false,
    'deleted': false
});

db.unitofmeasures.insert({
    'name': 'Pack Of 0.5Ltr',
    'symbol': 'Pack (0.5Ltr)',
    'conversion': 0.5,
    'description': 'Pack of 0.5Ltr',
    'type': 'Compound',
    'quantityType': 'Volume',
    'firstUnitOfMeasure': db.unitofmeasures.findOne({'name': 'Packs', 'symbol': 'Pac', 'type': 'Simple'})._id,
    'secondUnitOfMeasure': db.unitofmeasures.findOne({'name': 'Litres', 'symbol': 'Ltr', 'type': 'Simple'})._id,
    'disabled': false,
    'deleted': false
});


db.unitofmeasures.insert({
    'name': 'Box Of 40Nos',
    'symbol': 'Box (40Nos)',
    'conversion': 40,
    'description': 'Box of 40Nos',
    'type': 'Compound',
    'quantityType': 'Measure',
    'firstUnitOfMeasure': db.unitofmeasures.findOne({'name': 'Box', 'symbol': 'Box', 'type': 'Simple'})._id,
    'secondUnitOfMeasure': db.unitofmeasures.findOne({'name': 'Number', 'symbol': 'Nos', 'type': 'Simple'})._id,
    'disabled': false,
    'deleted': false
});


db.unitofmeasures.insert({
    'name': 'Box Of 51Nos',
    'symbol': 'Box (51Nos)',
    'conversion': 51,
    'description': 'Box of 51Nos',
    'type': 'Compound',
    'quantityType': 'Measure',
    'firstUnitOfMeasure': db.unitofmeasures.findOne({'name': 'Box', 'symbol': 'Box', 'type': 'Simple'})._id,
    'secondUnitOfMeasure': db.unitofmeasures.findOne({'name': 'Number', 'symbol': 'Nos', 'type': 'Simple'})._id,
    'disabled': false,
    'deleted': false
});

db.unitofmeasures.insert({
    'name': 'Pack Of 25Nos',
    'symbol': 'Pack (25Nos)',
    'conversion': 25,
    'description': 'Pack of 25Nos',
    'type': 'Compound',
    'quantityType': 'Measure',
    'firstUnitOfMeasure': db.unitofmeasures.findOne({'name': 'Packs', 'symbol': 'Pac', 'type': 'Simple'})._id,
    'secondUnitOfMeasure': db.unitofmeasures.findOne({'name': 'Number', 'symbol': 'Nos', 'type': 'Simple'})._id,
    'disabled': false,
    'deleted': false
});

db.unitofmeasures.insert({
    'name': 'Pack Of 20Nos',
    'symbol': 'Pack (20Nos)',
    'conversion': 20,
    'description': 'Pack of 20Nos',
    'type': 'Compound',
    'quantityType': 'Measure',
    'firstUnitOfMeasure': db.unitofmeasures.findOne({'name': 'Packs', 'symbol': 'Pac', 'type': 'Simple'})._id,
    'secondUnitOfMeasure': db.unitofmeasures.findOne({'name': 'Number', 'symbol': 'Nos', 'type': 'Simple'})._id,
    'disabled': false,
    'deleted': false
});

db.unitofmeasures.insert({
    'name': 'Pack Of 10Nos',
    'symbol': 'Pack (10Nos)',
    'conversion': 10,
    'description': 'Pack of 10Nos',
    'type': 'Compound',
    'quantityType': 'Measure',
    'firstUnitOfMeasure': db.unitofmeasures.findOne({'name': 'Packs', 'symbol': 'Pac', 'type': 'Simple'})._id,
    'secondUnitOfMeasure': db.unitofmeasures.findOne({'name': 'Number', 'symbol': 'Nos', 'type': 'Simple'})._id,
    'disabled': false,
    'deleted': false
});
db.unitofmeasures.insert({
    'name': 'Ounces',
    'symbol': 'Oz',
    'uqcCode': 'OZ',
    'description': 'Ounces',
    'quantityType': 'Measure',
    'type': 'Simple',
    'disabled': false,
    'deleted': false
});
db.unitofmeasures.insert({
    'name': 'Pounds',
    'symbol': 'lbs',
    'uqcCode': 'LBS',
    'description': 'Pounds',
    'quantityType': 'Measure',
    'type': 'Simple',
    'disabled': false,
    'deleted': false
});


