/*
MongoDB Script for inserting the PaymentModes
*/

// Remove PaymentModes Data
db.paymentmodes.remove({});

db.paymentmodes.insert({'name': 'Cash', 'type': 'Cash', disabled: false, deleted: false});
//'Cash', 'Cheque', 'DD', 'NEFT', 'RTGS', 'IMPS', 'UPI', 'Wallet', 'Online'
db.paymentmodes.insert({'name': 'Cheque', 'type': 'Cheque', disabled: false, deleted: false});
db.paymentmodes.insert({'name': 'DD', 'type': 'DD', disabled: false, deleted: false});
db.paymentmodes.insert({'name': 'NEFT', 'type': 'NEFT', disabled: false, deleted: false});
db.paymentmodes.insert({'name': 'RTGS', 'type': 'RTGS', disabled: false, deleted: false});
db.paymentmodes.insert({'name': 'IMPS', 'type': 'IMPS', disabled: false, deleted: false});
db.paymentmodes.insert({'name': 'UPI', 'type': 'UPI', disabled: false, deleted: false});
db.paymentmodes.insert({'name': 'Wallet', 'type': 'Wallet', disabled: false, deleted: false});
db.paymentmodes.insert({'name': 'Online', 'type': 'Online', disabled: false, deleted: false});

/* End */
