/*
/*
MongoDB Script for inserting the Business Segments

*/

// Remove businesssegments Data
db.businesssegments.remove({});

var rice = {'name': 'Rice', categories: [], segmentImageURL1: 'modules/categories/img/segments/rice.jpg'};
db.categories.find({name: /rice/i, type: 'SubCategory2'}).forEach(function (categoryDoc) {
    rice.categories.push({category: categoryDoc._id});

});
db.businesssegments.insert(rice);

var coffee = {'name': 'Coffee', categories: [], segmentImageURL1: 'modules/categories/img/segments/coffee.jpg'};
db.categories.find({name: /coffee/i, type: 'SubCategory2'}).forEach(function (categoryDoc) {
    coffee.categories.push({category: categoryDoc._id});

});
db.businesssegments.insert(coffee);

var textiles = {'name': 'Textiles', categories: [], segmentImageURL1: 'modules/categories/img/segments/textiles.png'};

textiles.categories.push({category: db.categories.findOne({'name': 'Cotton Yarn', 'type': 'SubCategory2'})._id});
textiles.categories.push({category: db.categories.findOne({'name': 'Silk Yarn', 'type': 'SubCategory2'})._id});
textiles.categories.push({category: db.categories.findOne({'name': 'Wool Yarn', 'type': 'SubCategory2'})._id});

textiles.categories.push({category: db.categories.findOne({'name': 'Cotton Fabric', 'type': 'SubCategory2'})._id});
textiles.categories.push({category: db.categories.findOne({'name': 'Silk Fabric', 'type': 'SubCategory2'})._id});
textiles.categories.push({category: db.categories.findOne({'name': 'Wool Fabric', 'type': 'SubCategory2'})._id});

textiles.categories.push({category: db.categories.findOne({'name': 'Cotton Apparel', 'type': 'SubCategory2'})._id});
textiles.categories.push({category: db.categories.findOne({'name': 'Silk Sarees', 'type': 'SubCategory2'})._id});
textiles.categories.push({category: db.categories.findOne({'name': 'Wool Apparel', 'type': 'SubCategory2'})._id});


db.businesssegments.insert(textiles);

var fruitsAndVegetables = {
    'name': 'Fruits and Vegetables',
    categories: [],
    segmentImageURL1: 'modules/categories/img/segments/fruits-vegetables.png'
};

fruitsAndVegetables.categories.push({category: db.categories.findOne({'name': 'Lemon', 'type': 'SubCategory2'})._id});
fruitsAndVegetables.categories.push({category: db.categories.findOne({'name': 'Onion', 'type': 'SubCategory2'})._id});

db.businesssegments.insert(fruitsAndVegetables);

var others={ 'name': 'Other',categories:[],isSpecific:true};
db.businesssegments.insert(others);
