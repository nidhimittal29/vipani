/*
/*
MongoDB Script for inserting the TaxGroups

*/


// Remove taxgroups Data
db.taxgroups.remove({});

db.taxgroups.insert({
    'name': 'GST 0%',
    'description': 'GST 0%',
    'CGST': 0,
    'SGST': 0,
    'IGST': 0, 'disabled': false, 'deleted': false
});
db.taxgroups.insert({
    'name': 'GST 3%',
    'description': 'GST 3%',
    'CGST': 1.5,
    'SGST': 1.5,
    'IGST': 3, 'disabled': false, 'deleted': false
});
db.taxgroups.insert({
    'name': 'GST 5%',
    'description': 'GST 5%',
    'CGST': 2.5,
    'SGST': 2.5,
    'IGST': 5, 'disabled': false, 'deleted': false
});
db.taxgroups.insert({
    'name': 'GST 8%',
    'description': 'GST 8%',
    'CGST': 4,
    'SGST': 4,
    'IGST': 8, 'disabled': false, 'deleted': false
});
db.taxgroups.insert({
    'name': 'GST 12%',
    'description': 'GST 12%',
    'CGST': 6,
    'SGST': 6,
    'IGST': 12, 'disabled': false, 'deleted': false
});

db.taxgroups.insert({
    'name': 'GST 18%',
    'description': 'GST 18%',
    'CGST': 9,
    'SGST': 9,
    'IGST': 18, 'disabled': false, 'deleted': false
});

db.taxgroups.insert({
    'name': 'GST 28%',
    'description': 'GST 28%',
    'CGST': 14,
    'SGST': 14,
    'IGST': 28, 'disabled': false, 'deleted': false
});
db.taxgroups.insert({
    'name': 'GST 28% + 1% Cess',
    'description': 'GST 28% + 1% Cess',
    'cess': 1,
    'CGST': 14,
    'SGST': 14,
    'IGST': 28, 'disabled': false, 'deleted': false
});
db.taxgroups.insert({
    'name': 'GST 31%',
    'description': 'GST 31%',
    'CGST': 15.5,
    'SGST': 15.5,
    'IGST': 31, 'disabled': false, 'deleted': false
});
