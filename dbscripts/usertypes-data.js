/*
/*
MongoDB Script for inserting the User Registration Categories

*/


// Remove registrationcategories Data
db.registrationcategories.remove({});

//Manufacturer
//Buy 1,
// Sell 2,
// Mediate 3,
// Buy,Sell 4,
//Buy, Mediate 5
//Sell ,Mediate 6
//Sell ,Buy ,Mediate 7

db.registrationcategories.insert({
    'name': 'Manufacturer',
    'type': 1,
    'category': 1,
    'description':'Manage all Business units & users Engaging mediators & Service Providers realtime insights about products research',
    'rImageURL':'modules/core/img/nvc-img/Manufacturer.png',
    'enabled': false
});
db.registrationcategories.insert({
    'name': 'Manufacturer',
    'type': 2,
    'category': 1,
    'enabled': true,
    'description':'Manage all Business units & users Engaging mediators & Service Providers realtime insights about products research',
    'rImageURL':'modules/core/img/nvc-img/Manufacturer.png',
    'isDefault': true
});

db.registrationcategories.insert({
    'name': 'Manufacturer',
    'type': 3,
    'category': 1,
    'description':'Manage all Business units & users Engaging mediators & Service Providers realtime insights about products research',
    'rImageURL':'modules/core/img/nvc-img/Manufacturer.png',
    'enabled': false
});
db.registrationcategories.insert({
    'name': 'Manufacturer',
    'type': 4,
    'category': 1,
    'description':'Manage all Business units & users Engaging mediators & Service Providers realtime insights about products research',
    'rImageURL':'modules/core/img/nvc-img/Manufacturer.png',
    'enabled': false
});
db.registrationcategories.insert({
    'name': 'Manufacturer',
    'type': 5,
    'category': 1,
    'description':'Manage all Business units & users Engaging mediators & Service Providers realtime insights about products research',
    'rImageURL':'modules/core/img/nvc-img/Manufacturer.png',
    'enabled': false
});
db.registrationcategories.insert({
    'name': 'Manufracturer',
    'type': 6,
    'category': 1,
    'description':'Manage all Business units & users Engaging mediators & Service Providers realtime insights about products research',
    'rImageURL':'modules/core/img/nvc-img/Manufacturer.png',
    'enabled': false
});
db.registrationcategories.insert({
    'name': 'Manufacturer',
    'type': 7,
    'category': 1,
    'description':'Manage all Business units & users Engaging mediators & Service Providers realtime insights about products research',
    'rImageURL':'modules/core/img/nvc-img/Manufacturer.png',
    'enabled': false
});

//Distributor
//Buy 1,
// Sell 2,
// Mediate 3,
// Buy,Sell 4,
//Buy, Mediate 5
//Sell ,Mediate 6
//Sell ,Buy ,Mediate 7


db.registrationcategories.insert({
    'name': 'Distributor',
    'type': 1,
    'category': 1,
    'enabled': true,
    'description':'Creative product showcasing Reach various customers & suppliers realtime analytics about market',
    'rImageURL':'modules/core/img/nvc-img/Distributor.png',
    'isDefault': true
});
db.registrationcategories.insert({
    'name': 'Distributor',
    'type': 2,
    'category': 1,
    'description':'Creative product showcasing Reach various customers & suppliers realtime analytics about market',
    'rImageURL':'modules/core/img/nvc-img/Distributor.png',
    'enabled': true
});

db.registrationcategories.insert({
    'name': 'Distributor',
    'type': 3,
    'category': 1,
    'description':'Creative product showcasing Reach various customers & suppliers realtime analytics about market',
    'rImageURL':'modules/core/img/nvc-img/Distributor.png',
    'enabled': false
});
db.registrationcategories.insert({
    'name': 'Distributor',
    'type': 4,
    'category': 1,
    'description':'Creative product showcasing Reach various customers & suppliers realtime analytics about market',
    'rImageURL':'modules/core/img/nvc-img/Distributor.png',
    'enabled': false
});
db.registrationcategories.insert({
    'name': 'Distributor',
    'type': 5,
    'category': 1,
    'description':'Creative product showcasing Reach various customers & suppliers realtime analytics about market',
    'rImageURL':'modules/core/img/nvc-img/Distributor.png',
    'enabled': false
});
db.registrationcategories.insert({
    'name': 'Distributor',
    'type': 6,
    'category': 1,
    'description':'Creative product showcasing Reach various customers & suppliers realtime analytics about market',
    'rImageURL':'modules/core/img/nvc-img/Distributor.png',
    'enabled': false
});
db.registrationcategories.insert({
    'name': 'Distributor',
    'type': 7,
    'category': 1,
    'description':'Creative product showcasing Reach various customers & suppliers realtime analytics about market',
    'rImageURL':'modules/core/img/nvc-img/Distributor.png',
    'enabled': false
});

//Retailer
//Buy 1,
// Sell 2,
// Mediate 3,
// Buy,Sell 4,
//Buy, Mediate 5
//Sell ,Mediate 6
//Sell ,Buy ,Mediate 7


db.registrationcategories.insert({
    'name': 'Retailer',
    'type': 1,
    'category': 2,
    'enabled': true,
    'description':'Receive offers as required Automate the hectic manual processes Realtime Traceability about Product',
    'rImageURL':'modules/core/img/nvc-img/Retailer.png',
    'isDefault': true
});
db.registrationcategories.insert({
    'name': 'Retailer',
    'type': 2,
    'category': 2,
    'description':'Receive offers as required Automate the hectic manual processes Realtime Traceability about Product',
    'rImageURL':'modules/core/img/nvc-img/Retailer.png',
    'enabled': false
});

db.registrationcategories.insert({
    'name': 'Retailer',
    'type': 3,
    'category': 2,
    'description':'Receive offers as required Automate the hectic manual processes Realtime Traceability about Product',
    'rImageURL':'modules/core/img/nvc-img/Retailer.png',
    'enabled': false
});
db.registrationcategories.insert({
    'name': 'Retailer',
    'type': 4,
    'category': 2,
    'description':'Receive offers as required Automate the hectic manual processes Realtime Traceability about Product',
    'rImageURL':'modules/core/img/nvc-img/Retailer.png',
    'enabled': false
});
db.registrationcategories.insert({
    'name': 'Retailer',
    'type': 5,
    'category': 2,
    'description':'Receive offers as required Automate the hectic manual processes Realtime Traceability about Product',
    'rImageURL':'modules/core/img/nvc-img/Retailer.png',
    'enabled': false
});
db.registrationcategories.insert({
    'name': 'Retailer',
    'type': 6,
    'category': 2,
    'description':'Receive offers as required Automate the hectic manual processes Realtime Traceability about Product',
    'rImageURL':'modules/core/img/nvc-img/Retailer.png',
    'enabled': false
});
db.registrationcategories.insert({
    'name': 'Retailer',
    'type': 7,
    'category': 2,
    'description':'Receive offers as required Automate the hectic manual processes Realtime Traceability about Product',
    'rImageURL':'modules/core/img/nvc-img/Retailer.png',
    'enabled': false
});

//Commission Agent
//Buy 1,
// Sell 2,
// Mediate 3,
// Buy,Sell 4,
//Buy, Mediate 5
//Sell ,Mediate 6
//Sell ,Buy ,Mediate 7


db.registrationcategories.insert({
    'name': 'Commission Agent',
    'type': 1,
    'category': 2,
    'enabled': true
});
db.registrationcategories.insert({
    'name': 'Commission Agent',
    'type': 2,
    'category': 2,
    'enabled': false
});

db.registrationcategories.insert({
    'name': 'Commission Agent',
    'type': 3,
    'category': 2,
    'enabled': false
});
db.registrationcategories.insert({
    'name': 'Commission Agent',
    'type': 4,
    'category': 2,
    'enabled': false
});
db.registrationcategories.insert({
    'name': 'Commission Agent',
    'type': 5,
    'category': 2,
    'enabled': false
});
db.registrationcategories.insert({
    'name': 'Commission Agent',
    'type': 6,
    'category': 2,
    'enabled': false
});
db.registrationcategories.insert({
    'name': 'Commission Agent',
    'type': 7,
    'category': 2,
    'enabled': false
});

//Trader
//Buy 1,
// Sell 2,
// Mediate 3,
// Buy,Sell 4,
//Buy, Mediate 5
//Sell ,Mediate 6
//Sell ,Buy ,Mediate 7


db.registrationcategories.insert({
    'name': 'Trader',
    'type': 1,
    'category': 2,
    'enabled': false
});
db.registrationcategories.insert({
    'name': 'Trader',
    'type': 2,
    'category': 2,
    'enabled': false
});

db.registrationcategories.insert({
    'name': 'Trader',
    'type': 3,
    'category': 2,
    'enabled': false
});
db.registrationcategories.insert({
    'name': 'Trader',
    'type': 4,
    'category': 2,
    'enabled': false
});
db.registrationcategories.insert({
    'name': 'Trader',
    'type': 5,
    'category': 2,
    'enabled': false
});
db.registrationcategories.insert({
    'name': 'Trader',
    'type': 6,
    'category': 2,
    'enabled': false
});
db.registrationcategories.insert({
    'name': 'Trader',
    'type': 7,
    'category': 2,
    'enabled': false
});

//Exporter
//Buy 1,
// Sell 2,
// Mediate 3,
// Buy,Sell 4,
//Buy, Mediate 5
//Sell ,Mediate 6
//Sell ,Buy ,Mediate 7


db.registrationcategories.insert({
    'name': 'Exporter',
    'type': 1,
    'category': 2,
    'enabled': false
});
db.registrationcategories.insert({
    'name': 'Exporter',
    'type': 2,
    'category': 2,
    'enabled': false
});

db.registrationcategories.insert({
    'name': 'Exporter',
    'type': 3,
    'category': 2,
    'enabled': false
});
db.registrationcategories.insert({
    'name': 'Exporter',
    'type': 4,
    'category': 2,
    'enabled': false
});
db.registrationcategories.insert({
    'name': 'Exporter',
    'type': 5,
    'category': 2,
    'enabled': false
});
db.registrationcategories.insert({
    'name': 'Exporter',
    'type': 6,
    'category': 2,
    'enabled': false
});
db.registrationcategories.insert({
    'name': 'Exporter',
    'type': 7,
    'category': 2,
    'enabled': false
});


//Importer
//Buy 1,
// Sell 2,
// Mediate 3,
// Buy,Sell 4,
//Buy, Mediate 5
//Sell ,Mediate 6
//Sell ,Buy ,Mediate 7


db.registrationcategories.insert({
    'name': 'Importer',
    'type': 1,
    'category': 2,
    'enabled': false
});
db.registrationcategories.insert({
    'name': 'Importer',
    'type': 2,
    'category': 2,
    'enabled': false
});

db.registrationcategories.insert({
    'name': 'Importer',
    'type': 3,
    'category': 2,
    'enabled': false
});
db.registrationcategories.insert({
    'name': 'Importer',
    'type': 4,
    'category': 2,
    'enabled': false
});
db.registrationcategories.insert({
    'name': 'Importer',
    'type': 5,
    'category': 2,
    'enabled': false
});
db.registrationcategories.insert({
    'name': 'Importer',
    'type': 6,
    'category': 2,
    'enabled': false
});
db.registrationcategories.insert({
    'name': 'Importer',
    'type': 7,
    'category': 2,
    'enabled': false
});

//FPO/FPC
//Buy 1,
// Sell 2,
// Mediate 3,
// Buy,Sell 4,
//Buy, Mediate 5
//Sell ,Mediate 6
//Sell ,Buy ,Mediate 7


db.registrationcategories.insert({
    'name': 'FPO/FPC',
    'type': 1,
    'category': 2,
    'enabled': false
});
db.registrationcategories.insert({
    'name': 'FPO/FPC',
    'type': 2,
    'category': 2,
    'enabled': false
});

db.registrationcategories.insert({
    'name': 'FPO/FPC',
    'type': 3,
    'category': 2,
    'enabled': false
});
db.registrationcategories.insert({
    'name': 'FPO/FPC',
    'type': 4,
    'category': 2,
    'enabled': false
});
db.registrationcategories.insert({
    'name': 'FPO/FPC',
    'type': 5,
    'category': 2,
    'enabled': false
});
db.registrationcategories.insert({
    'name': 'FPO/FPC',
    'type': 6,
    'category': 2,
    'enabled': false
});
db.registrationcategories.insert({
    'name': 'FPO/FPC',
    'type': 7,
    'category': 2,
    'enabled': false
});

// TODO:
//'Mediator', 'Accountant', 'CertificationAuthority', 'ServiceProvider'
