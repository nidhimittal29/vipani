/*
 * Insert categories
 */
// Remove categories Data
db.categories.remove({});

// Agriculture Main Category
db.categories.insert({
    'name': 'Agriculture',
    'code': 'AGRIC',
    'type': 'MainCategory',
    'categoryImageURL1': 'modules/categories/img/maincategory/agriculture.png',
    'croppedCategoryImageURL1': 'modules/categories/img/maincategory/mobileagriculture.png',
    'disabled': false,
    'deleted': false
});

// Sub Categories of Agriculture
db.categories.insert({
    'name': 'Cereals',
    'code': 'CERLS',
    'type': 'SubCategory1',
    'categoryImageURL1': 'modules/categories/img/subcategory1/cereals.png',
    'croppedCategoryImageURL1': 'modules/categories/img/subcategory1/mobilecereals.png',
    'disabled': false,
    'deleted': false,
    'parent': db.categories.findOne({'name': 'Agriculture', 'type': 'MainCategory'})._id
});
// Adding Sub Category Cereals to Agriculture Category
db.categories.update({
    'name': 'Agriculture',
    'type': 'MainCategory'
}, {$addToSet: {'children': db.categories.findOne({'name': 'Cereals', 'type': 'SubCategory1'})._id}});

// Sub Categories of Cereals
db.categories.insert({
    'name': 'Sona Masoori Rice',
    'code': 'SONMR',
    'type': 'SubCategory2',
    'aliases': ['Sona Masoori', 'Swarna Masoori Rice'],
    'superClassification': ['Rice', 'Grain'],
    'categoryImageURL1': 'modules/categories/img/subcategory2/rice.png',
    'croppedCategoryImageURL1': 'modules/categories/img/subcategory2/mobilerice.png',
    'productCategory': true,
    'productAttributes': {
        brand: true,
        grade: {
            enabled: true,
            definition: [{attributeKey: 'Color', attributeValue: ['White', 'Brown']}, {
                attributeKey: 'Kind',
                attributeValue: ['White Rice', 'Aromatice Rice', 'Brown Rice']
            }, {
                attributeKey: 'Cultivation Type',
                attributeValue: ['Common', 'Natural', 'Organic']
            }, {
                attributeKey: 'Variety',
                attributeValue: ['Medium Grain Rice', 'Short Grain Rice', 'Long Grain Rice']
            }, {attributeKey: 'Style', attributeValue: ['Dried', 'Fresh', 'Steamed']}]
        },
        quality: {
            enabled: true,
            definition: [{
                attributeKey: 'Stickiness',
                attributeValue: ['White Glutinous Rice(milled)', 'Purple Glutinous Rice(unmilled)', 'Black Glutinous Rice(unmilled)']
            }, {
                attributeKey: 'Flavor',
                attributeValue: ['salty', 'sweet', 'sour', 'bitter', 'astringency', 'spice heat', 'cooling', 'bite', 'metallic flavor', 'umami taste']
            }, {
                attributeKey: 'Aroma',
                attributeValue: ['popcorn', 'cooked grain', 'starchy', 'woody', 'smoky', 'grain', 'corn', 'hay-like', 'barny', 'rancid', 'waxy', 'earthy', 'sweet aroma']
            }]
        },
        sampleNumber: true,
        fssaiLicenceNumber: true
    },
    'inventoryAttributes': {
        barcode: true,
        batchNumber: true,
        manufactureDate: true,
        packagingDate: true,
        bestBefore: true
    },
    'disabled': false,
    'deleted': false,
    'parent': db.categories.findOne({'name': 'Cereals', 'type': 'SubCategory1'})._id
});

db.categories.update({
    'name': 'Sona Masoori Rice',
    'type': 'SubCategory2'
}, {
    $addToSet: {
        'unitOfMeasures': db.unitofmeasures.findOne({
            'name': 'Bag Of 25Kgs',
            'symbol': 'Bag (25Kgs)',
            'conversion': 25,
            'quantityType': 'Weight',
            'type': 'Compound'
        })._id
    }
});

db.categories.update({
    'name': 'Sona Masoori Rice',
    'type': 'SubCategory2'
}, {
    $addToSet: {
        'unitOfMeasures': db.unitofmeasures.findOne({
            'name': 'Bag Of 10Kgs',
            'symbol': 'Bag (10Kgs)',
            'conversion': 10,
            'quantityType': 'Weight',
            'type': 'Compound'
        })._id
    }
});

db.categories.update({
    'name': 'Sona Masoori Rice',
    'type': 'SubCategory2'
}, {
    $addToSet: {
        'unitOfMeasures': db.unitofmeasures.findOne({
            'name': 'Bag Of 5Kgs',
            'symbol': 'Bag (5Kgs)',
            'conversion': 5,
            'quantityType': 'Weight',
            'type': 'Compound'
        })._id
    }
});

db.categories.update({
    'name': 'Sona Masoori Rice',
    'type': 'SubCategory2'
}, {
    $addToSet: {
        'unitOfMeasures': db.unitofmeasures.findOne({
            'name': 'Pack Of 2Kgs',
            'symbol': 'Pack (2Kgs)',
            'conversion': 2,
            'quantityType': 'Weight',
            'type': 'Compound'
        })._id
    }
});

db.categories.update({
    'name': 'Sona Masoori Rice',
    'type': 'SubCategory2'
}, {
    $addToSet: {
        'unitOfMeasures': db.unitofmeasures.findOne({
            'name': 'Pack Of 1Kgs',
            'symbol': 'Pack (1Kgs)',
            'conversion': 1,
            'quantityType': 'Weight',
            'type': 'Compound'
        })._id
    }
});

db.categories.update({
    'name': 'Sona Masoori Rice',
    'type': 'SubCategory2'
}, {
    $addToSet: {
        'unitOfMeasures': db.unitofmeasures.findOne({
            'name': 'Pack Of 0.5Kgs',
            'symbol': 'Pack (0.5Kgs)',
            'conversion': 0.5,
            'quantityType': 'Weight',
            'type': 'Compound'
        })._id
    }
});

db.categories.update({
    'name': 'Sona Masoori Rice',
    'type': 'SubCategory2'
}, {$addToSet: {'hsnCodes': db.hsncodes.findOne({'hsncode': '10063010'})._id}});

db.categories.update({
    'name': 'Sona Masoori Rice',
    'type': 'SubCategory2'
}, {$addToSet: {'hsnCodes': db.hsncodes.findOne({'hsncode': '10063090'})._id}});

db.categories.update({
    'name': 'Sona Masoori Rice',
    'type': 'SubCategory2'
}, {$addToSet: {'taxGroups': db.taxgroups.findOne({'name': 'GST 0%'})._id}});

db.categories.update({
    'name': 'Sona Masoori Rice',
    'type': 'SubCategory2'
}, {$addToSet: {'taxGroups': db.taxgroups.findOne({'name': 'GST 5%'})._id}});

// Adding Sub Categories to Cereals category
db.categories.update({
    'name': 'Cereals',
    'type': 'SubCategory1'
}, {$addToSet: {'children': db.categories.findOne({'name': 'Sona Masoori Rice', 'type': 'SubCategory2'})._id}});


// Coffee

db.categories.insert({
    'name': 'Coffee',
    'code': 'COFFE',
    'type': 'SubCategory1',
    'categoryImageURL1': 'modules/categories/img/subcategory1/coffee.png',
    'croppedCategoryImageURL1': 'modules/categories/img/subcategory1/mobilecoffee.png',
    'disabled': false,
    'deleted': false,
    'parent': db.categories.findOne({'name': 'Agriculture', 'type': 'MainCategory'})._id
});

db.categories.update({
    'name': 'Agriculture',
    'type': 'MainCategory'
}, {$addToSet: {'children': db.categories.findOne({'name': 'Coffee', 'type': 'SubCategory1'})._id}});


// Sub Categories of Cereals
db.categories.insert({
    'name': 'Coffee Berries',
    'code': 'COBRS',
    'type': 'SubCategory2',
    'aliases': ['Cherry,Berry,Stone fruit'],
    'superClassification': ['Berry'],
    'categoryImageURL1': 'modules/categories/img/subcategory2/Berries.png',
    'croppedCategoryImageURL1': 'modules/categories/img/subcategory2/mobileCoffee Berries.png',
    'productCategory': true,
    'productAttributes': {
        brand: true,
        grade: {
            enabled: true,
            definition: [
                {attributeKey: 'Color', attributeValue: ['Bright Red']},
                {attributeKey: 'Kind', attributeValue: ['Small', 'Dwarf']},
                {attributeKey: 'Cultivation Type', attributeValue: ['Conventional', 'Natural', 'Organic']},
                {attributeKey: 'Variety', attributeValue: ['Arabica', 'Robusta']},
                {attributeKey: 'Style', attributeValue: ['Fresh', 'Dried']}
            ]
        },
        quality: {
            enabled: true,
            definition: [{
                attributeKey: 'Grades',
                attributeValue: ['PB', 'AA', 'A', 'C ', 'Blacks/Browns ', 'Bits', ' Bulk', 'AB']
            }, {
                attributeKey: 'Flavor',
                attributeValue: ['Dried Fruit taste', 'Bitter taste']
            },
            ]
        },
        sampleNumber: true,
        fssaiLicenceNumber: true
    },
    'inventoryAttributes': {
        barcode: true,
        batchNumber: true,
        manufactureDate: true,
        packagingDate: true,
        bestBefore: true
    },
    'disabled': false,
    'deleted': false,
    'parent': db.categories.findOne({'name': 'Coffee', 'type': 'SubCategory1'})._id
});

db.categories.update({
    'name': 'Coffee Berries',
    'type': 'SubCategory2'
}, {
    $addToSet: {
        'unitOfMeasures': db.unitofmeasures.findOne({
            'name': 'Box Of 40Nos',
            'symbol': 'Box (40Nos)',
            'conversion': 40,
            'quantityType': 'Measure',
            'type': 'Compound'
        })._id
    }
});

db.categories.update({
    'name': 'Coffee Berries',
    'type': 'SubCategory2'
}, {
    $addToSet: {
        'unitOfMeasures': db.unitofmeasures.findOne({
            'name': 'Pack Of 25Nos',
            'symbol': 'Pack (25Nos)',
            'conversion': 25,
            'quantityType': 'Measure',
            'type': 'Compound'
        })._id
    }
});

db.categories.update({
    'name': 'Coffee Berries',
    'type': 'SubCategory2'
}, {
    $addToSet: {
        'unitOfMeasures': db.unitofmeasures.findOne({
            'name': 'Pack Of 20Nos',
            'symbol': 'Pack (20Nos)',
            'conversion': 20,
            'quantityType': 'Measure',
            'type': 'Compound'
        })._id
    }
});

db.categories.update({
    'name': 'Coffee Berries',
    'type': 'SubCategory2'
}, {
    $addToSet: {
        'unitOfMeasures': db.unitofmeasures.findOne({
            'name': 'Pack Of 10Nos',
            'symbol': 'Pack (10Nos)',
            'conversion': 10,
            'quantityType': 'Measure',
            'type': 'Compound'
        })._id
    }
});

db.categories.update({
    'name': 'Coffee Berries',
    'type': 'SubCategory2'
}, {$addToSet: {'hsnCodes': db.hsncodes.findOne({'hsncode': '09011124'})._id}});

db.categories.update({
    'name': 'Coffee Berries',
    'type': 'SubCategory2'
}, {$addToSet: {'hsnCodes': db.hsncodes.findOne({'hsncode': '09011129'})._id}});
db.categories.update({
    'name': 'Coffee Berries',
    'type': 'SubCategory2'
}, {$addToSet: {'hsnCodes': db.hsncodes.findOne({'hsncode': '09011141'})._id}});


db.categories.update({
    'name': 'Coffee Berries',
    'type': 'SubCategory2'
}, {$addToSet: {'hsnCodes': db.hsncodes.findOne({'hsncode': '09011142'})._id}});


db.categories.update({
    'name': 'Coffee Berries',
    'type': 'SubCategory2'
}, {$addToSet: {'hsnCodes': db.hsncodes.findOne({'hsncode': '09011143'})._id}});


db.categories.update({
    'name': 'Coffee Berries',
    'type': 'SubCategory2'
}, {$addToSet: {'hsnCodes': db.hsncodes.findOne({'hsncode': '09011144'})._id}});

db.categories.update({
    'name': 'Coffee Berries',
    'type': 'SubCategory2'
}, {$addToSet: {'hsnCodes': db.hsncodes.findOne({'hsncode': '09011144'})._id}});

db.categories.update({
    'name': 'Coffee Berries',
    'type': 'SubCategory2'
}, {$addToSet: {'hsnCodes': db.hsncodes.findOne({'hsncode': '09011145'})._id}});

db.categories.update({
    'name': 'Coffee Berries',
    'type': 'SubCategory2'
}, {$addToSet: {'hsnCodes': db.hsncodes.findOne({'hsncode': '09011149'})._id}});


db.categories.update({
    'name': 'Coffee Berries',
    'type': 'SubCategory2'
}, {$addToSet: {'taxGroups': db.taxgroups.findOne({'name': 'GST 0%'})._id}});


// Adding Sub Categories to Coffee category
db.categories.update({
    'name': 'Coffee',
    'type': 'SubCategory1'
}, {$addToSet: {'children': db.categories.findOne({'name': 'Coffee Berries', 'type': 'SubCategory2'})._id}});

// Sub Categories of Agriculture
db.categories.insert({
    'name': 'Vegetables',
    'code': 'VGTBL',
    'type': 'SubCategory1',
    'categoryImageURL1': 'modules/categories/img/subcategory1/vegetables.png',
    'croppedCategoryImageURL1': 'modules/categories/img/subcategory1/mobilevegetables.png',
    'disabled': false,
    'deleted': false,
    'parent': db.categories.findOne({'name': 'Agriculture', 'type': 'MainCategory'})._id
});

db.categories.update({
    'name': 'Agriculture',
    'type': 'MainCategory'
}, {$addToSet: {'children': db.categories.findOne({'name': 'Vegetables', 'type': 'SubCategory1'})._id}});


// Sub Categories of Vegetables

db.categories.insert({
    'name': 'Lemon',
    'code': 'LEMON',
    'type': 'SubCategory2',
    'categoryImageURL1': 'modules/categories/img/subcategory2/lemon.png',
    'croppedCategoryImageURL1': 'modules/categories/img/subcategory2/mobilelemon.png',
    'productCategory': true,
    'productAttributes': {
        brand: true,
        grade: {
            enabled: true,
            definition: [{attributeKey: 'Color', attributeValue: ['White', 'Brown']}, {
                attributeKey: 'Kind',
                attributeValue: ['White Rice', 'Aromatice Rice', 'Brown Rice']
            }, {
                attributeKey: 'Cultivation Type',
                attributeValue: ['Common', 'Natural', 'Organic']
            }, {
                attributeKey: 'Variety',
                attributeValue: ['Medium Grain Rice', 'Short Grain Rice', 'Long Grain Rice']
            }, {attributeKey: 'Style', attributeValue: ['Dried', 'Fresh', 'Steamed']}]
        },
        quality: {
            enabled: true,
            definition: [{
                attributeKey: 'Stickiness',
                attributeValue: ['White Glutinous Rice(milled)', 'Purple Glutinous Rice(unmilled)', 'Black Glutinous Rice(unmilled)']
            }, {
                attributeKey: 'Flavor',
                attributeValue: ['salty', 'sweet', 'sour', 'bitter', 'astringency', 'spice heat', 'cooling', 'bite', 'metallic flavor', 'umami taste']
            }, {
                attributeKey: 'Aroma',
                attributeValue: ['popcorn', 'cooked grain', 'starchy', 'woody', 'smoky', 'grain', 'corn', 'hay-like', 'barny', 'rancid', 'waxy', 'earthy', 'sweet aroma']
            }]
        },
        sampleNumber: true,
        fssaiLicenceNumber: true
    },
    'inventoryAttributes': {
        barcode: true,
        batchNumber: true,
        manufactureDate: true,
        packagingDate: true,
        bestBefore: true
    },
    'disabled': false,
    'deleted': false,
    'parent': db.categories.findOne({'name': 'Vegetables', 'code': 'VGTBL', 'type': 'SubCategory1'})._id
});
db.categories.update({
    'name': 'Lemon',
    'type': 'SubCategory2'
}, {
    $addToSet: {
        'unitOfMeasures': db.unitofmeasures.findOne({
            'name': 'Bag Of 25Kgs',
            'symbol': 'Bag (25Kgs)',
            'conversion': 25,
            'quantityType': 'Weight',
            'type': 'Compound'
        })._id
    }
});

db.categories.update({
    'name': 'Lemon',
    'type': 'SubCategory2'
}, {
    $addToSet: {
        'unitOfMeasures': db.unitofmeasures.findOne({
            'name': 'Bag Of 10Kgs',
            'symbol': 'Bag (10Kgs)',
            'conversion': 10,
            'quantityType': 'Weight',
            'type': 'Compound'
        })._id
    }
});

db.categories.update({
    'name': 'Lemon',
    'type': 'SubCategory2'
}, {
    $addToSet: {
        'unitOfMeasures': db.unitofmeasures.findOne({
            'name': 'Bag Of 5Kgs',
            'symbol': 'Bag (5Kgs)',
            'conversion': 5,
            'quantityType': 'Weight',
            'type': 'Compound'
        })._id
    }
});

db.categories.update({
    'name': 'Lemon',
    'type': 'SubCategory2'
}, {
    $addToSet: {
        'unitOfMeasures': db.unitofmeasures.findOne({
            'name': 'Pack Of 2Kgs',
            'symbol': 'Pack (2Kgs)',
            'conversion': 2,
            'quantityType': 'Weight',
            'type': 'Compound'
        })._id
    }
});

db.categories.update({
    'name': 'Lemon',
    'type': 'SubCategory2'
}, {
    $addToSet: {
        'unitOfMeasures': db.unitofmeasures.findOne({
            'name': 'Pack Of 1Kgs',
            'symbol': 'Pack (1Kgs)',
            'conversion': 1,
            'quantityType': 'Weight',
            'type': 'Compound'
        })._id
    }
});

db.categories.update({
    'name': 'Lemon',
    'type': 'SubCategory2'
}, {
    $addToSet: {
        'unitOfMeasures': db.unitofmeasures.findOne({
            'name': 'Pack Of 0.5Kgs',
            'symbol': 'Pack (0.5Kgs)',
            'conversion': 0.5,
            'quantityType': 'Weight',
            'type': 'Compound'
        })._id
    }
});

db.categories.update({
    'name': 'Vegetables',
    'type': 'SubCategory1'
}, {$addToSet: {'children': db.categories.findOne({'name': 'Lemon', 'code': 'LEMON', 'type': 'SubCategory2'})._id}});
db.categories.update({
    'name': 'Lemon',
    'type': 'SubCategory2'
}, {$addToSet: {'hsnCodes': db.hsncodes.findOne({'hsncode': '08055000'})._id}});

db.categories.update({
    'name': 'Lemon',
    'type': 'SubCategory2'
}, {$addToSet: {'taxGroups': db.taxgroups.findOne({'name': 'GST 0%'})._id}});

db.categories.insert({
    'name': 'Onion',
    'code': 'ONION',
    'type': 'SubCategory2',
    'categoryImageURL1': 'modules/categories/img/subcategory2/onion.png',
    'croppedCategoryImageURL1': 'modules/categories/img/subcategory2/mobileonion.png',
    'productCategory': true,
    'productAttributes': {
        brand: true,
        grade: {
            enabled: true,
            definition: [{attributeKey: 'Color', attributeValue: ['White', 'Brown']}, {
                attributeKey: 'Kind',
                attributeValue: ['White Rice', 'Aromatice Rice', 'Brown Rice']
            }, {
                attributeKey: 'Cultivation Type',
                attributeValue: ['Common', 'Natural', 'Organic']
            }, {
                attributeKey: 'Variety',
                attributeValue: ['Medium Grain Rice', 'Short Grain Rice', 'Long Grain Rice']
            }, {attributeKey: 'Style', attributeValue: ['Dried', 'Fresh', 'Steamed']}]
        },
        quality: {
            enabled: true,
            definition: [{
                attributeKey: 'Stickiness',
                attributeValue: ['White Glutinous Rice(milled)', 'Purple Glutinous Rice(unmilled)', 'Black Glutinous Rice(unmilled)']
            }, {
                attributeKey: 'Flavor',
                attributeValue: ['salty', 'sweet', 'sour', 'bitter', 'astringency', 'spice heat', 'cooling', 'bite', 'metallic flavor', 'umami taste']
            }, {
                attributeKey: 'Aroma',
                attributeValue: ['popcorn', 'cooked grain', 'starchy', 'woody', 'smoky', 'grain', 'corn', 'hay-like', 'barny', 'rancid', 'waxy', 'earthy', 'sweet aroma']
            }]
        },
        sampleNumber: true,
        fssaiLicenceNumber: true
    },
    'inventoryAttributes': {
        barcode: true,
        batchNumber: true,
        manufactureDate: true,
        packagingDate: true,
        bestBefore: true
    },
    'disabled': false,
    'deleted': false,
    'parent': db.categories.findOne({'name': 'Vegetables', 'code': 'VGTBL', 'type': 'SubCategory1'})._id,
    'grandParent':  db.categories.findOne({'name': 'Agriculture', 'type': 'MainCategory'})._id
});


db.categories.update({
    'name': 'Onion',
    'type': 'SubCategory2'
}, {
    $addToSet: {
        'unitOfMeasures': db.unitofmeasures.findOne({
            'name': 'Bag Of 25Kgs',
            'symbol': 'Bag (25Kgs)',
            'conversion': 25,
            'quantityType': 'Weight',
            'type': 'Compound'
        })._id
    }
});

db.categories.update({
    'name': 'Onion',
    'type': 'SubCategory2'
}, {
    $addToSet: {
        'unitOfMeasures': db.unitofmeasures.findOne({
            'name': 'Bag Of 10Kgs',
            'symbol': 'Bag (10Kgs)',
            'conversion': 10,
            'quantityType': 'Weight',
            'type': 'Compound'
        })._id
    }
});

db.categories.update({
    'name': 'Onion',
    'type': 'SubCategory2'
}, {
    $addToSet: {
        'unitOfMeasures': db.unitofmeasures.findOne({
            'name': 'Bag Of 5Kgs',
            'symbol': 'Bag (5Kgs)',
            'conversion': 5,
            'quantityType': 'Weight',
            'type': 'Compound'
        })._id
    }
});

db.categories.update({
    'name': 'Onion',
    'type': 'SubCategory2'
}, {
    $addToSet: {
        'unitOfMeasures': db.unitofmeasures.findOne({
            'name': 'Pack Of 2Kgs',
            'symbol': 'Pack (2Kgs)',
            'conversion': 2,
            'quantityType': 'Weight',
            'type': 'Compound'
        })._id
    }
});

db.categories.update({
    'name': 'Onion',
    'type': 'SubCategory2'
}, {
    $addToSet: {
        'unitOfMeasures': db.unitofmeasures.findOne({
            'name': 'Pack Of 1Kgs',
            'symbol': 'Pack (1Kgs)',
            'conversion': 1,
            'quantityType': 'Weight',
            'type': 'Compound'
        })._id
    }
});

db.categories.update({
    'name': 'Onion',
    'type': 'SubCategory2'
}, {
    $addToSet: {
        'unitOfMeasures': db.unitofmeasures.findOne({
            'name': 'Pack Of 0.5Kgs',
            'symbol': 'Pack (0.5Kgs)',
            'conversion': 0.5,
            'quantityType': 'Weight',
            'type': 'Compound'
        })._id
    }
});
db.categories.update({
    'name': 'Onion',
    'type': 'SubCategory2'
}, {$addToSet: {'hsnCodes': db.hsncodes.findOne({'hsncode': '07031010'})._id}});

db.categories.update({
    'name': 'Onion',
    'type': 'SubCategory2'
}, {$addToSet: {'taxGroups': db.taxgroups.findOne({'name': 'GST 0%'})._id}});

db.categories.update({
    'name': 'Vegetables',
    'type': 'SubCategory1'
}, {$addToSet: {'children': db.categories.findOne({'name': 'Onion', 'code': 'ONION', 'type': 'SubCategory2'})._id}});

db.categories.insert({
    'name': 'Garlic',
    'code': 'GARLC',
    'type': 'SubCategory2',
    'categoryImageURL1': 'modules/categories/img/subcategory2/garlic.png',
    'croppedCategoryImageURL1': 'modules/categories/img/subcategory2/mobilegarlic.png',
    'productCategory': true,
    'productAttributes': {
        brand: true,
        grade: {
            enabled: true,
            definition: [{attributeKey: 'Color', attributeValue: ['White', 'Brown']}, {
                attributeKey: 'Kind',
                attributeValue: ['White Rice', 'Aromatice Rice', 'Brown Rice']
            }, {
                attributeKey: 'Cultivation Type',
                attributeValue: ['Common', 'Natural', 'Organic']
            }, {
                attributeKey: 'Variety',
                attributeValue: ['Medium Grain Rice', 'Short Grain Rice', 'Long Grain Rice']
            }, {attributeKey: 'Style', attributeValue: ['Dried', 'Fresh', 'Steamed']}]
        },
        quality: {
            enabled: true,
            definition: [{
                attributeKey: 'Stickiness',
                attributeValue: ['White Glutinous Rice(milled)', 'Purple Glutinous Rice(unmilled)', 'Black Glutinous Rice(unmilled)']
            }, {
                attributeKey: 'Flavor',
                attributeValue: ['salty', 'sweet', 'sour', 'bitter', 'astringency', 'spice heat', 'cooling', 'bite', 'metallic flavor', 'umami taste']
            }, {
                attributeKey: 'Aroma',
                attributeValue: ['popcorn', 'cooked grain', 'starchy', 'woody', 'smoky', 'grain', 'corn', 'hay-like', 'barny', 'rancid', 'waxy', 'earthy', 'sweet aroma']
            }]
        },
        sampleNumber: true,
        fssaiLicenceNumber: true
    },
    'inventoryAttributes': {
        barcode: true,
        batchNumber: true,
        manufactureDate: true,
        packagingDate: true,
        bestBefore: true
    },
    'disabled': false,
    'deleted': false,
    'parent': db.categories.findOne({'name': 'Vegetables', 'code': 'VGTBL', 'type': 'SubCategory1'})._id,
    'grandParent':  db.categories.findOne({'name': 'Agriculture', 'type': 'MainCategory'})._id
});
db.categories.update({
    'name': 'Garlic',
    'type': 'SubCategory2'
}, {
    $addToSet: {
        'unitOfMeasures': db.unitofmeasures.findOne({
            'name': 'Bag Of 25Kgs',
            'symbol': 'Bag (25Kgs)',
            'conversion': 25,
            'quantityType': 'Weight',
            'type': 'Compound'
        })._id
    }
});

db.categories.update({
    'name': 'Garlic',
    'type': 'SubCategory2'
}, {
    $addToSet: {
        'unitOfMeasures': db.unitofmeasures.findOne({
            'name': 'Bag Of 10Kgs',
            'symbol': 'Bag (10Kgs)',
            'conversion': 10,
            'quantityType': 'Weight',
            'type': 'Compound'
        })._id
    }
});

db.categories.update({
    'name': 'Garlic',
    'type': 'SubCategory2'
}, {
    $addToSet: {
        'unitOfMeasures': db.unitofmeasures.findOne({
            'name': 'Bag Of 5Kgs',
            'symbol': 'Bag (5Kgs)',
            'conversion': 5,
            'quantityType': 'Weight',
            'type': 'Compound'
        })._id
    }
});

db.categories.update({
    'name': 'Garlic',
    'type': 'SubCategory2'
}, {
    $addToSet: {
        'unitOfMeasures': db.unitofmeasures.findOne({
            'name': 'Pack Of 2Kgs',
            'symbol': 'Pack (2Kgs)',
            'conversion': 2,
            'quantityType': 'Weight',
            'type': 'Compound'
        })._id
    }
});

db.categories.update({
    'name': 'Garlic',
    'type': 'SubCategory2'
}, {
    $addToSet: {
        'unitOfMeasures': db.unitofmeasures.findOne({
            'name': 'Pack Of 1Kgs',
            'symbol': 'Pack (1Kgs)',
            'conversion': 1,
            'quantityType': 'Weight',
            'type': 'Compound'
        })._id
    }
});

db.categories.update({
    'name': 'Garlic',
    'type': 'SubCategory2'
}, {
    $addToSet: {
        'unitOfMeasures': db.unitofmeasures.findOne({
            'name': 'Pack Of 0.5Kgs',
            'symbol': 'Pack (0.5Kgs)',
            'conversion': 0.5,
            'quantityType': 'Weight',
            'type': 'Compound'
        })._id
    }
});

db.categories.update({
    'name': 'Vegetables',
    'type': 'SubCategory1'
}, {$addToSet: {'children': db.categories.findOne({'name': 'Garlic', 'code': 'GARLC', 'type': 'SubCategory2'})._id}});

db.categories.update({
    'name': 'Garlic',
    'type': 'SubCategory2'
}, {$addToSet: {'hsnCodes': db.hsncodes.findOne({'hsncode': '07032000'})._id}});

db.categories.update({
    'name': 'Garlic',
    'type': 'SubCategory2'
}, {$addToSet: {'taxGroups': db.taxgroups.findOne({'name': 'GST 0%'})._id}});


// START - Textiles
db.categories.insert({
    'name': 'Textiles',
    'code': 'TEXTL',
    'type': 'MainCategory',
    'categoryImageURL1': 'modules/categories/img/maincategory/agriculture.png',
    'croppedCategoryImageURL1': 'modules/categories/img/maincategory/mobileagriculture.png',
    'disabled': false,
    'deleted': false
});

db.categories.insert({
    'name': 'Yarn',
    'code': 'YARNT',
    'type': 'SubCategory1',
    'categoryImageURL1': 'modules/categories/img/subcategory1/yarn.png',
    'croppedCategoryImageURL1': 'modules/categories/img/subcategory1/mobileyarn.png',
    'disabled': false,
    'deleted': false,
    'parent': db.categories.findOne({'name': 'Textiles', 'type': 'MainCategory'})._id
});

db.categories.update({
    'name': 'Textiles',
    'type': 'MainCategory'
}, {$addToSet: {'children': db.categories.findOne({'name': 'Yarn', 'type': 'SubCategory1'})._id}});


db.categories.insert({
    'name': 'Cotton Yarn',
    'code': 'COTYN',
    'type': 'SubCategory2',
    'aliases': ['Combed Cotton Yarn,Cotton Combed Yarn, Cotton Thread'],
    'superClassification': ['Yarn'],
    'categoryImageURL1': 'modules/categories/img/subcategory2/cottonyarn.png',
    'croppedCategoryImageURL1': 'modules/categories/img/subcategory2/mobilecottonyarn.png',
    'productCategory': true,
    'productAttributes': {
        brand: true,
        grade: {
            enabled: true,
            definition: [
                {
                    attributeKey: 'URClassficationGrade',
                    attributeValue: ['50-55 Very Good', '45-50 Good', '40-45 Satisfactory', '35-40 Poor Below', '30 Unusable']
                },
                {attributeKey: 'Cultivation Type', attributeValue: ['Conventional', 'Natural', 'Organic']}
            ]
        },
        quality: {
            enabled: true,
            definition: [{attributeKey: 'StapleClassification', attributeValue: ['Short, Medium, Long, Extra Long']},
                {
                    attributeKey: 'Strength',
                    attributeValue: ['Weak', 'Medium', 'Average', 'Strong', 'Very Strong']
                }
            ]
        },
        sampleNumber: true,
        fssaiLicenceNumber: false
    },
    'inventoryAttributes': {
        barcode: true,
        batchNumber: true,
        manufactureDate: true,
        packagingDate: true,
        inventoryTestCertifcate: true,
        bestBefore: true
    },
    'disabled': false,
    'deleted': false,
    'parent': db.categories.findOne({'name': 'Yarn', 'type': 'SubCategory1'})._id
});

db.categories.update({
    'name': 'Cotton Yarn',
    'type': 'SubCategory2'
}, {
    $addToSet: {
        'unitOfMeasures': db.unitofmeasures.findOne({
            'name': 'Pack Of 10Nos',
            'symbol': 'Pack (10Nos)',
            'conversion': 10,
            'quantityType': 'Measure',
            'type': 'Compound'
        })._id
    }
});

db.categories.update({
    'name': 'Cotton Yarn',
    'type': 'SubCategory2'
}, {$addToSet: {'hsnCodes': db.hsncodes.findOne({'hsncode': '52061100'})._id}});

db.categories.update({
    'name': 'Cotton Yarn',
    'type': 'SubCategory2'
}, {$addToSet: {'taxGroups': db.taxgroups.findOne({'name': 'GST 5%'})._id}});


db.categories.insert({
    'name': 'Silk Yarn',
    'code': 'SLKYN',
    'type': 'SubCategory2',
    'aliases': ['SLK Yarn, Silk Thread'],
    'superClassification': ['Yarn'],
    'categoryImageURL1': 'modules/categories/img/subcategory2/silkyarn.png',
    'croppedCategoryImageURL1': 'modules/categories/img/subcategory2/mobilesilkyarn.png',
    'productCategory': true,
    'productAttributes': {
        brand: true,
        grade: {
            enabled: true,
            definition: [
                {attributeKey: 'Color', attributeValue: ['White', 'Tussar', 'Natural Gold', 'Raw Natural']},
                {attributeKey: 'Grade', attributeValue: ['Premium', 'First Level Grade', 'Second Level Grade']},
                {attributeKey: 'Cultivation Type', attributeValue: ['Conventional', 'Natural', 'Organic']}
            ]
        },
        quality: {
            enabled: true,
            definition: [{attributeKey: 'StapleClassification', attributeValue: ['Short, Medium, Long, Extra Long']},
                {
                    attributeKey: 'Strength',
                    attributeValue: ['Weak', 'Medium', 'Average', 'Strong', 'Very Strong']
                }
            ]
        },
        sampleNumber: true,
        fssaiLicenceNumber: false
    },
    'inventoryAttributes': {
        barcode: true,
        batchNumber: true,
        manufactureDate: true,
        packagingDate: true,
        inventoryTestCertifcate: true,
        bestBefore: true
    },
    'disabled': false,
    'deleted': false,
    'parent': db.categories.findOne({'name': 'Yarn', 'type': 'SubCategory1'})._id
});

db.categories.update({
    'name': 'Silk Yarn',
    'type': 'SubCategory2'
}, {
    $addToSet: {
        'unitOfMeasures': db.unitofmeasures.findOne({
            'name': 'Pack Of 10Nos',
            'symbol': 'Pack (10Nos)',
            'conversion': 10,
            'quantityType': 'Measure',
            'type': 'Compound'
        })._id
    }
});

db.categories.update({
    'name': 'Silk Yarn',
    'type': 'SubCategory2'
}, {$addToSet: {'hsnCodes': db.hsncodes.findOne({'hsncode': '50040010'})._id}});

db.categories.update({
    'name': 'Silk Yarn',
    'type': 'SubCategory2'
}, {$addToSet: {'taxGroups': db.taxgroups.findOne({'name': 'GST 5%'})._id}});

db.categories.insert({
    'name': 'Wool Yarn',
    'code': 'WOLYN',
    'type': 'SubCategory2',
    'aliases': ['Wool Thread'],
    'superClassification': ['Yarn'],
    'categoryImageURL1': 'modules/categories/img/subcategory2/woolyarn.png',
    'croppedCategoryImageURL1': 'modules/categories/img/subcategory2/mobilewoolyarn.png',
    'productCategory': true,
    'productAttributes': {
        brand: true,
        grade: {
            enabled: true,
            definition: [
                {attributeKey: 'Type', attributeValue: ['Fine', 'Medium', 'Coarse', 'Very Coarse']},
                {
                    attributeKey: 'Count',
                    attributeValue: ['80s', '70s', '64s', '62s', '60s', '58s', '56s', '54s', '50s', '48s', '46s', '44s', '40s']
                }
            ]
        },
        quality: {
            enabled: true,
            definition: [{attributeKey: 'StapleClassification', attributeValue: ['Short, Medium, Long, Extra Long']},
                {
                    attributeKey: 'Standard Of Uniformity',
                    attributeValue: ['Excellent', 'Average', 'Poor']
                }
            ]
        },
        sampleNumber: true,
        fssaiLicenceNumber: false
    },
    'inventoryAttributes': {
        barcode: true,
        batchNumber: true,
        manufactureDate: true,
        packagingDate: true,
        inventoryTestCertifcate: true,
        bestBefore: true
    },
    'disabled': false,
    'deleted': false,
    'parent': db.categories.findOne({'name': 'Yarn', 'type': 'SubCategory1'})._id
});

db.categories.update({
    'name': 'Wool Yarn',
    'type': 'SubCategory2'
}, {
    $addToSet: {
        'unitOfMeasures': db.unitofmeasures.findOne({
            'name': 'Pack Of 10Nos',
            'symbol': 'Pack (10Nos)',
            'conversion': 10,
            'quantityType': 'Measure',
            'type': 'Compound'
        })._id
    }
});

db.categories.update({
    'name': 'Wool Yarn',
    'type': 'SubCategory2'
}, {$addToSet: {'hsnCodes': db.hsncodes.findOne({'hsncode': '52064200'})._id}});

db.categories.update({
    'name': 'Wool Yarn',
    'type': 'SubCategory2'
}, {$addToSet: {'taxGroups': db.taxgroups.findOne({'name': 'GST 5%'})._id}});

// Adding Sub Categories to Yarn category
db.categories.update({
    'name': 'Yarn',
    'type': 'SubCategory1'
}, {$addToSet: {'children': db.categories.findOne({'name': 'Cotton Yarn', 'type': 'SubCategory2'})._id}});

db.categories.update({
    'name': 'Yarn',
    'type': 'SubCategory1'
}, {$addToSet: {'children': db.categories.findOne({'name': 'Silk Yarn', 'type': 'SubCategory2'})._id}});

db.categories.update({
    'name': 'Yarn',
    'type': 'SubCategory1'
}, {$addToSet: {'children': db.categories.findOne({'name': 'Wool Yarn', 'type': 'SubCategory2'})._id}});

// Adding Fabric SubCategory1
db.categories.insert({
    'name': 'Fabric',
    'code': 'FABRC',
    'type': 'SubCategory1',
    'categoryImageURL1': 'modules/categories/img/subcategory1/fabric.png',
    'croppedCategoryImageURL1': 'modules/categories/img/subcategory1/mobilefabric.png',
    'disabled': false,
    'deleted': false,
    'parent': db.categories.findOne({'name': 'Textiles', 'type': 'MainCategory'})._id
});

db.categories.update({
    'name': 'Textiles',
    'type': 'MainCategory'
}, {$addToSet: {'children': db.categories.findOne({'name': 'Fabric', 'type': 'SubCategory1'})._id}});

db.categories.insert({
    'name': 'Cotton Fabric',
    'code': 'COTFB',
    'type': 'SubCategory2',
    'aliases': ['Cotton Cloth'],
    'superClassification': ['Fabric'],
    'categoryImageURL1': 'modules/categories/img/subcategory2/cottonfabric.png',
    'croppedCategoryImageURL1': 'modules/categories/img/subcategory2/mobilecottonfabric.png',
    'productCategory': true,
    'productAttributes': {
        brand: true,
        grade: {
            enabled: true,
            definition: [
                {
                    attributeKey: 'Grade',
                    attributeValue: ['Middle Fair', 'Strict Good Middling', 'Good Middling', 'Middling', 'Strict Low Middling', 'Strict Good Ordinary', 'Good Ordinary']
                }
            ]
        },
        quality: {
            enabled: true,
            definition: [
                {
                    attributeKey: 'Standard Of Uniformity',
                    attributeValue: ['Excellent', 'Average', 'Poor']
                }
            ]
        },
        sampleNumber: true,
        fssaiLicenceNumber: false
    },
    'inventoryAttributes': {
        barcode: true,
        batchNumber: true,
        manufactureDate: true,
        packagingDate: true,
        inventoryTestCertifcate: true,
        bestBefore: true
    },
    'disabled': false,
    'deleted': false,
    'parent': db.categories.findOne({'name': 'Fabric', 'type': 'SubCategory1'})._id
});

db.categories.update({
    'name': 'Cotton Fabric',
    'type': 'SubCategory2'
}, {
    $addToSet: {
        'unitOfMeasures': db.unitofmeasures.findOne({
            'name': 'Pack Of 10Nos',
            'symbol': 'Pack (10Nos)',
            'conversion': 10,
            'quantityType': 'Measure',
            'type': 'Compound'
        })._id
    }
});

db.categories.update({
    'name': 'Cotton Fabric',
    'type': 'SubCategory2'
}, {$addToSet: {'hsnCodes': db.hsncodes.findOne({'hsncode': '52081110'})._id}});

db.categories.update({
    'name': 'Cotton Fabric',
    'type': 'SubCategory2'
}, {$addToSet: {'hsnCodes': db.hsncodes.findOne({'hsncode': '52083170'})._id}});

db.categories.update({
    'name': 'Cotton Fabric',
    'type': 'SubCategory2'
}, {$addToSet: {'hsnCodes': db.hsncodes.findOne({'hsncode': '52083280'})._id}});

db.categories.update({
    'name': 'Cotton Fabric',
    'type': 'SubCategory2'
}, {$addToSet: {'taxGroups': db.taxgroups.findOne({'name': 'GST 5%'})._id}});

db.categories.insert({
    'name': 'Silk Fabric',
    'code': 'SLKFB',
    'type': 'SubCategory2',
    'aliases': ['Silk Cloth'],
    'superClassification': ['Fabric'],
    'categoryImageURL1': 'modules/categories/img/subcategory2/silkfabric.png',
    'croppedCategoryImageURL1': 'modules/categories/img/subcategory2/mobilesilkfabric.png',
    'productCategory': true,
    'productAttributes': {
        brand: true,
        grade: {
            enabled: true,
            definition: [
                {attributeKey: 'Color', attributeValue: ['White', 'Tussar', 'Natural Gold', 'Raw Natural']},
                {attributeKey: 'Grade', attributeValue: ['Premium', 'First Level Grade', 'Second Level Grade']}
            ]
        },
        quality: {
            enabled: true,
            definition: [
                {
                    attributeKey: 'Standard Of Uniformity',
                    attributeValue: ['Excellent', 'Average', 'Poor']
                }
            ]
        },
        sampleNumber: true,
        fssaiLicenceNumber: false
    },
    'inventoryAttributes': {
        barcode: true,
        batchNumber: true,
        manufactureDate: true,
        packagingDate: true,
        inventoryTestCertifcate: true,
        bestBefore: true
    },
    'disabled': false,
    'deleted': false,
    'parent': db.categories.findOne({'name': 'Fabric', 'type': 'SubCategory1'})._id
});


db.categories.update({
    'name': 'Silk Fabric',
    'type': 'SubCategory2'
}, {
    $addToSet: {
        'unitOfMeasures': db.unitofmeasures.findOne({
            'name': 'Pack Of 10Nos',
            'symbol': 'Pack (10Nos)',
            'conversion': 10,
            'quantityType': 'Measure',
            'type': 'Compound'
        })._id
    }
});

db.categories.update({
    'name': 'Silk Fabric',
    'type': 'SubCategory2'
}, {$addToSet: {'hsnCodes': db.hsncodes.findOne({'hsncode': '50071000'})._id}});

db.categories.update({
    'name': 'Silk Fabric',
    'type': 'SubCategory2'
}, {$addToSet: {'hsnCodes': db.hsncodes.findOne({'hsncode': '50072010'})._id}});

db.categories.update({
    'name': 'Silk Fabric',
    'type': 'SubCategory2'
}, {$addToSet: {'taxGroups': db.taxgroups.findOne({'name': 'GST 5%'})._id}});

db.categories.insert({
    'name': 'Wool Fabric',
    'code': 'WOLFB',
    'type': 'SubCategory2',
    'aliases': ['Wool Cloth'],
    'superClassification': ['Fabric'],
    'categoryImageURL1': 'modules/categories/img/subcategory2/woolfabric.png',
    'croppedCategoryImageURL1': 'modules/categories/img/subcategory2/mobilewoolfabric.png',
    'productCategory': true,
    'productAttributes': {
        brand: true,
        grade: {
            enabled: true,
            definition: [
                {attributeKey: 'Color', attributeValue: ['White', 'Tussar', 'Natural Gold', 'Raw Natural']},
                {
                    attributeKey: 'Count',
                    attributeValue: ['80s', '70s', '64s', '62s', '60s', '58s', '56s', '54s', '50s', '48s', '46s', '44s', '40s']
                }
            ]
        },
        quality: {
            enabled: true,
            definition: [
                {
                    attributeKey: 'Standard Of Uniformity',
                    attributeValue: ['Excellent', 'Average', 'Poor']
                }
            ]
        },
        sampleNumber: true,
        fssaiLicenceNumber: false
    },
    'inventoryAttributes': {
        barcode: true,
        batchNumber: true,
        manufactureDate: true,
        packagingDate: true,
        inventoryTestCertifcate: true,
        bestBefore: true
    },
    'disabled': false,
    'deleted': false,
    'parent': db.categories.findOne({'name': 'Fabric', 'type': 'SubCategory1'})._id
});

db.categories.update({
    'name': 'Wool Fabric',
    'type': 'SubCategory2'
}, {
    $addToSet: {
        'unitOfMeasures': db.unitofmeasures.findOne({
            'name': 'Pack Of 10Nos',
            'symbol': 'Pack (10Nos)',
            'conversion': 10,
            'quantityType': 'Measure',
            'type': 'Compound'
        })._id
    }
});

db.categories.update({
    'name': 'Wool Fabric',
    'type': 'SubCategory2'
}, {$addToSet: {'hsnCodes': db.hsncodes.findOne({'hsncode': '51121110'})._id}});

db.categories.update({
    'name': 'Wool Fabric',
    'type': 'SubCategory2'
}, {$addToSet: {'hsnCodes': db.hsncodes.findOne({'hsncode': '51111110'})._id}});

db.categories.update({
    'name': 'Wool Fabric',
    'type': 'SubCategory2'
}, {$addToSet: {'taxGroups': db.taxgroups.findOne({'name': 'GST 5%'})._id}});

// Adding Sub Categories to Fabric category
db.categories.update({
    'name': 'Fabric',
    'type': 'SubCategory1'
}, {$addToSet: {'children': db.categories.findOne({'name': 'Cotton Fabric', 'type': 'SubCategory2'})._id}});

db.categories.update({
    'name': 'Fabric',
    'type': 'SubCategory1'
}, {$addToSet: {'children': db.categories.findOne({'name': 'Silk Fabric', 'type': 'SubCategory2'})._id}});

db.categories.update({
    'name': 'Fabric',
    'type': 'SubCategory1'
}, {$addToSet: {'children': db.categories.findOne({'name': 'Wool Fabric', 'type': 'SubCategory2'})._id}});


// Adding Apparel SubCategory1
db.categories.insert({
    'name': 'Apparel',
    'code': 'APPRL',
    'type': 'SubCategory1',
    'aliases': ['Clothing'],
    'superClassification': ['Clothes'],
    'categoryImageURL1': 'modules/categories/img/subcategory1/apparel.png',
    'croppedCategoryImageURL1': 'modules/categories/img/subcategory1/mobileapparel.png',
    'disabled': false,
    'deleted': false,
    'parent': db.categories.findOne({'name': 'Textiles', 'type': 'MainCategory'})._id
});

db.categories.update({
    'name': 'Textiles',
    'type': 'MainCategory'
}, {$addToSet: {'children': db.categories.findOne({'name': 'Apparel', 'type': 'SubCategory1'})._id}});

db.categories.insert({
    'name': 'Cotton Apparel',
    'code': 'COTAP',
    'type': 'SubCategory2',
    'aliases': ['Cotton Clothes', 'Cotton Clothing'],
    'superClassification': ['Clothes', 'Clothing'],
    'categoryImageURL1': 'modules/categories/img/subcategory2/cottonapparel.png',
    'croppedCategoryImageURL1': 'modules/categories/img/subcategory2/mobilecottonapparel.png',
    'productCategory': true,
    'productAttributes': {
        brand: true,
        grade: {
            enabled: true,
            definition: [
                {attributeKey: 'Softness', attributeValue: ['Very Soft', 'Medium Soft', 'Soft']}
            ]
        },
        quality: {
            enabled: true,
            definition: [
                {
                    attributeKey: 'Durability',
                    attributeValue: ['Long', 'Medium', 'Short']
                }
            ]
        },
        sampleNumber: true,
        fssaiLicenceNumber: false
    },
    'inventoryAttributes': {
        barcode: true,
        batchNumber: true,
        manufactureDate: true,
        packagingDate: true,
        inventoryTestCertifcate: true,
        bestBefore: true
    },
    'disabled': false,
    'deleted': false,
    'parent': db.categories.findOne({'name': 'Apparel', 'type': 'SubCategory1'})._id
});

db.categories.update({
    'name': 'Cotton Apparel',
    'type': 'SubCategory2'
}, {
    $addToSet: {
        'unitOfMeasures': db.unitofmeasures.findOne({
            'name': 'Box Of 51Nos',
            'symbol': 'Box (51Nos)',
            'conversion': 51,
            'quantityType': 'Measure',
            'type': 'Compound'
        })._id
    }
});

db.categories.update({
    'name': 'Cotton Apparel',
    'type': 'SubCategory2'
}, {
    $addToSet: {
        'unitOfMeasures': db.unitofmeasures.findOne({
            'name': 'Pack Of 10Nos',
            'symbol': 'Pack (10Nos)',
            'conversion': 10,
            'quantityType': 'Measure',
            'type': 'Compound'
        })._id
    }
});

db.categories.update({
    'name': 'Cotton Apparel',
    'type': 'SubCategory2'
}, {$addToSet: {'hsnCodes': db.hsncodes.findOne({'hsncode': '61121930'})._id}});

db.categories.update({
    'name': 'Cotton Apparel',
    'type': 'SubCategory2'
}, {$addToSet: {'hsnCodes': db.hsncodes.findOne({'hsncode': '61043990'})._id}});

db.categories.update({
    'name': 'Cotton Apparel',
    'type': 'SubCategory2'
}, {$addToSet: {'taxGroups': db.taxgroups.findOne({'name': 'GST 5%'})._id}});

db.categories.update({
    'name': 'Cotton Apparel',
    'type': 'SubCategory2'
}, {$addToSet: {'taxGroups': db.taxgroups.findOne({'name': 'GST 12%'})._id}});

db.categories.insert({
    'name': 'Silk Sarees',
    'code': 'SLKSR',
    'type': 'SubCategory2',
    'aliases': ['Silk Clothes', 'Silk Clothing', 'Women Sarees'],
    'superClassification': ['Sarees', 'Clothing'],
    'categoryImageURL1': 'modules/categories/img/subcategory2/silkapparel.png',
    'croppedCategoryImageURL1': 'modules/categories/img/subcategory2/mobilesilkapparel.png',
    'productCategory': true,
    'productAttributes': {
        brand: true,
        grade: {
            enabled: true,
            definition: [
                {attributeKey: 'Softness', attributeValue: ['Very Soft', 'Medium Soft', 'Soft']}
            ]
        },
        quality: {
            enabled: true,
            definition: [
                {
                    attributeKey: 'Durability',
                    attributeValue: ['Long', 'Medium', 'Short']
                }
            ]
        },
        sampleNumber: true,
        fssaiLicenceNumber: false
    },
    'inventoryAttributes': {
        barcode: true,
        batchNumber: true,
        manufactureDate: true,
        packagingDate: true,
        inventoryTestCertifcate: true,
        bestBefore: true
    },
    'disabled': false,
    'deleted': false,
    'parent': db.categories.findOne({'name': 'Apparel', 'type': 'SubCategory1'})._id
});

db.categories.update({
    'name': 'Silk Sarees',
    'type': 'SubCategory2'
}, {
    $addToSet: {
        'unitOfMeasures': db.unitofmeasures.findOne({
            'name': 'Box Of 51Nos',
            'symbol': 'Box (51Nos)',
            'conversion': 51,
            'quantityType': 'Measure',
            'type': 'Compound'
        })._id
    }
});

db.categories.update({
    'name': 'Silk Sarees',
    'type': 'SubCategory2'
}, {
    $addToSet: {
        'unitOfMeasures': db.unitofmeasures.findOne({
            'name': 'Pack Of 10Nos',
            'symbol': 'Pack (10Nos)',
            'conversion': 10,
            'quantityType': 'Measure',
            'type': 'Compound'
        })._id
    }
});

db.categories.update({
    'name': 'Silk Sarees',
    'type': 'SubCategory2'
}, {$addToSet: {'hsnCodes': db.hsncodes.findOne({'hsncode': '50071000'})._id}});

db.categories.update({
    'name': 'Silk Sarees',
    'type': 'SubCategory2'
}, {$addToSet: {'hsnCodes': db.hsncodes.findOne({'hsncode': '50072010'})._id}});

db.categories.update({
    'name': 'Silk Sarees',
    'type': 'SubCategory2'
}, {$addToSet: {'taxGroups': db.taxgroups.findOne({'name': 'GST 5%'})._id}});

db.categories.update({
    'name': 'Silk Sarees',
    'type': 'SubCategory2'
}, {$addToSet: {'taxGroups': db.taxgroups.findOne({'name': 'GST 12%'})._id}});

db.categories.insert({
    'name': 'Wool Apparel',
    'code': 'WOLAP',
    'type': 'SubCategory2',
    'aliases': ['Woolen Clothes', 'Wool Clothing', 'Woolen Clothing'],
    'superClassification': ['Clothing'],
    'categoryImageURL1': 'modules/categories/img/subcategory2/woolapparel.png',
    'croppedCategoryImageURL1': 'modules/categories/img/subcategory2/mobilewoolapparel.png',
    'productCategory': true,
    'productAttributes': {
        brand: true,
        grade: {
            enabled: true,
            definition: [
                {attributeKey: 'Softness', attributeValue: ['Very Soft', 'Medium Soft', 'Soft']}
            ]
        },
        quality: {
            enabled: true,
            definition: [
                {
                    attributeKey: 'Durability',
                    attributeValue: ['Long', 'Medium', 'Short']
                }
            ]
        },
        sampleNumber: true,
        fssaiLicenceNumber: false
    },
    'inventoryAttributes': {
        barcode: true,
        batchNumber: true,
        manufactureDate: true,
        packagingDate: true,
        inventoryTestCertifcate: true,
        bestBefore: true
    },
    'disabled': false,
    'deleted': false,
    'parent': db.categories.findOne({'name': 'Apparel', 'type': 'SubCategory1'})._id
});

db.categories.update({
    'name': 'Wool Apparel',
    'type': 'SubCategory2'
}, {$addToSet: {'hsnCodes': db.hsncodes.findOne({'hsncode': '61121930'})._id}});

db.categories.update({
    'name': 'Wool Apparel',
    'type': 'SubCategory2'
}, {$addToSet: {'hsnCodes': db.hsncodes.findOne({'hsncode': '61043990'})._id}});

db.categories.update({
    'name': 'Wool Apparel',
    'type': 'SubCategory2'
}, {$addToSet: {'taxGroups': db.taxgroups.findOne({'name': 'GST 5%'})._id}});

db.categories.update({
    'name': 'Wool Apparel',
    'type': 'SubCategory2'
}, {$addToSet: {'taxGroups': db.taxgroups.findOne({'name': 'GST 12%'})._id}});
// Adding Sub Categories to Apparel category
db.categories.update({
    'name': 'Apparel',
    'type': 'SubCategory1'
}, {$addToSet: {'children': db.categories.findOne({'name': 'Cotton Apparel', 'type': 'SubCategory2'})._id}});

db.categories.update({
    'name': 'Apparel',
    'type': 'SubCategory1'
}, {$addToSet: {'children': db.categories.findOne({'name': 'Silk Sarees', 'type': 'SubCategory2'})._id}});

db.categories.update({
    'name': 'Apparel',
    'type': 'SubCategory1'
}, {$addToSet: {'children': db.categories.findOne({'name': 'Wool Apparel', 'type': 'SubCategory2'})._id}});

// END - Textiles


