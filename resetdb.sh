if [ $# -ge 1 ]
then
    db=$1
else
 db=nvipani-dev
fi
mongo $db --eval "db.dropDatabase()"
mongo $db dbscripts/usertypes-data.js
mongo $db dbscripts/unitofmeasure-data.js
mongo $db dbscripts/taxgroups-data.js
mongo $db dbscripts/paymentterms-data.js
mongo $db dbscripts/paymentmodes-data.js
mongo $db dbscripts/hsncodes-data.js
mongo $db dbscripts/categories-data.js
mongo $db dbscripts/businesssegments-data.js
mongo $db dbscripts/usergroups-data.js
