'use strict';

var config = browser.params,path = require('path'),
    user = require('../account/common.util.e2e/signin.common.po.js'),
    global = require('../account/common.util.e2e/global.common.po.js'),
    companyinfotestdata=require('./companyinfo.testdata.json'),
    companyInfoPage = require('./companyinfo.po');

describe('Company Info View', function() {
    var loadPage = function () {
        browser.manage().deleteAllCookies();
        browser.get('http://localhost:3000/#!/signin');

    };

    describe('with local auth', function () {
        beforeAll(function () {
            browser.get('http://localhost:3000/#!/register');
            user.registerProcess(companyinfotestdata[0]);
        });
        beforeEach(function () {
            loadPage();
            user.login(companyinfotestdata[0]);
            browser.sleep('3000');
            browser.findElement(by.id('profilemenu')).click();
            browser.sleep('2000');
            browser.findElement(by.id('companyprofile')).click();
        });

        afterEach(function () {
            user.logout();
        });

        //1.TESTCASE TO UPDATE GSTINNO IN COMPANY INFO
        it('update gstinNumber at company info', function () {
            browser.findElement(by.xpath('//*[text()="Company info"]')).click();
            companyInfoPage.form.gstinNo.sendKeys('12AAAGM0289C1ZK');
            companyInfoPage.form.submit.click();
            browser.sleep('4000');

        });

        //2.TESTCASE TO UPDATE PANNO IN COMPANY INFO
        it('update panNumber at company info', function () {
            browser.findElement(by.xpath('//*[text()="Company info"]')).click();
            companyInfoPage.form.panNo.sendKeys('KYBCA7375C');
            companyInfoPage.form.submit.click();
            browser.sleep('4000');
        });

        //3.TESTCASE TO UPDATE BANK DETAILS IN COMPANY INFO
        it('update bank details at company info', function () {
            browser.findElement(by.xpath('//*[text()="Company info"]')).click();
            companyInfoPage.form.bankAccountNumber.sendKeys('11245678900');
            companyInfoPage.form.bankName.sendKeys('State Bank Of India');
            companyInfoPage.form.ifscCode.sendKeys('SBIN0020550');
            companyInfoPage.form.submit.click();
            browser.sleep('4000');
        });

        //4.TESTCASE TO ADD MULTIPLE MOBILE NUMBERS IN COMPANY INFO
        it('add more mobile numbers at company info', function () {
            browser.findElement(by.xpath('//*[text()="Company info"]')).click();
            browser.driver.executeScript("arguments[0].scrollIntoView(true);", element(by.id('companyAddressTab')).getWebElement());
            browser.findElement(by.id('companyAddPhones')).click();
            browser.findElement(by.id('companyPhoneNumber-0')).sendKeys(companyinfotestdata[1].phoneNumber[0]);
            browser.findElement(by.id('companyAddPhones')).click();
            browser.findElement(by.id('companyPhoneNumber-1')).sendKeys(companyinfotestdata[1].phoneNumber[1]);
            browser.findElement(by.id('companyAddPhones')).click();
            browser.findElement(by.id('companyPhoneNumber-2')).sendKeys(companyinfotestdata[1].phoneNumber[2]);
            companyInfoPage.form.submit.click();
            browser.sleep('4000');
        });

        //5.TESTCASE TO ADD MULTIPLE EMAILS IN COMPANY INFO
        it('add more emails at company info', function () {
            browser.findElement(by.xpath('//*[text()="Company info"]')).click();
            browser.driver.executeScript("arguments[0].scrollIntoView(true);", element(by.id('companyAddressTab')).getWebElement());
            browser.findElement(by.xpath('//*[@id="companyPhoneEmailTab"]/md-tabs/md-tabs-wrapper/md-tabs-canvas/md-pagination-wrapper/md-tab-item[2]')).click();
            browser.driver.executeScript("arguments[0].scrollIntoView(true);", element(by.id('companyAddressTab')).getWebElement());
            browser.findElement(by.id('companyAddEmails')).click();
            browser.findElement(by.id('companyEmail-1')).sendKeys(companyinfotestdata[2].email[0]);
            browser.findElement(by.id('companyAddEmails')).click();
            browser.findElement(by.id('companyEmail-2')).sendKeys(companyinfotestdata[2].email[1]);
            browser.findElement(by.id('companyAddEmails')).click();
            browser.findElement(by.id('companyEmail-3')).sendKeys(companyinfotestdata[2].email[2]);
            companyInfoPage.form.submit.click();
            browser.sleep('4000');
        });

        //6.TESTCASE TO ADD  BILLING,SHIPPING,RECEIVING,INVOICE ADDRESSES AT COMPANY INFO
        it('add  Billing,shipping,Receiving,invoice addresses at company info', function () {
            browser.findElement(by.xpath('//*[text()="Company info"]')).click();
            browser.driver.executeScript("arguments[0].scrollIntoView(true);", element(by.id('companyAddressTab')).getWebElement());
            browser.findElement(by.id('companyAddAddresses-0')).click();
            browser.findElement(by.xpath("//*[@name='addressLine']")).sendKeys(companyinfotestdata[3].billingaddresses[0].addressLine);
            browser.findElement(by.xpath("//*[@name='city']")).sendKeys(companyinfotestdata[3].billingaddresses[0].city);
            browser.findElement(by.xpath("//*[@name='state']")).sendKeys(companyinfotestdata[3].billingaddresses[0].state);
            browser.findElement(by.xpath("//*[@name='country']")).sendKeys(companyinfotestdata[3].billingaddresses[0].country);
            browser.findElement(by.xpath("//*[@name='pinCode']")).sendKeys(companyinfotestdata[3].billingaddresses[0].pinCode);
            browser.findElement(by.xpath('//*[@id="companyAddressTab"]//md-tab-item[2]')).click();
            browser.findElement(by.id('companyAddAddresses-1')).click();
            browser.findElement(by.xpath("//*[@name='addressLine']")).sendKeys(companyinfotestdata[3].shippingaddresses[0].addressLine);
            browser.findElement(by.xpath("//*[@name='city']")).sendKeys(companyinfotestdata[3].shippingaddresses[0].city);
            browser.findElement(by.xpath("//*[@name='state']")).sendKeys(companyinfotestdata[3].shippingaddresses[0].state);
            browser.findElement(by.xpath("//*[@name='country']")).sendKeys(companyinfotestdata[3].shippingaddresses[0].country);
            browser.findElement(by.xpath("//*[@name='pinCode']")).sendKeys(companyinfotestdata[3].shippingaddresses[0].pinCode);
            browser.findElement(by.xpath('//*[@id="companyAddressTab"]//md-tab-item[3]')).click();
            browser.findElement(by.id('companyAddAddresses-2')).click();
            browser.findElement(by.xpath("//*[@name='addressLine']")).sendKeys(companyinfotestdata[3].receivingaddresses[0].addressLine);
            browser.findElement(by.xpath("//*[@name='city']")).sendKeys(companyinfotestdata[3].receivingaddresses[0].city);
            browser.findElement(by.xpath("//*[@name='state']")).sendKeys(companyinfotestdata[3].receivingaddresses[0].state);
            browser.findElement(by.xpath("//*[@name='country']")).sendKeys(companyinfotestdata[3].receivingaddresses[0].country);
            browser.findElement(by.xpath("//*[@name='pinCode']")).sendKeys(companyinfotestdata[3].receivingaddresses[0].pinCode);
            browser.findElement(by.xpath('//*[@id="companyAddressTab"]//md-tab-item[4]')).click();
            browser.findElement(by.id('companyAddAddresses-3')).click();
            browser.findElement(by.xpath("//*[@name='addressLine']")).sendKeys(companyinfotestdata[3].invoiceaddresses[0].addressLine);
            browser.findElement(by.xpath("//*[@name='city']")).sendKeys(companyinfotestdata[3].invoiceaddresses[0].city);
            browser.findElement(by.xpath("//*[@name='state']")).sendKeys(companyinfotestdata[3].invoiceaddresses[0].state);
            browser.findElement(by.xpath("//*[@name='country']")).sendKeys(companyinfotestdata[3].invoiceaddresses[0].country);
            browser.findElement(by.xpath("//*[@name='pinCode']")).sendKeys(companyinfotestdata[3].invoiceaddresses[0].pinCode);
            companyInfoPage.form.submit.click();
            browser.sleep('4000');
        });

        //7.TESTCASE TO ADD MORE BILLING ADDRESSES AT COMPANY INFO
        it('add more Billing addresses at company info', function () {
            browser.findElement(by.xpath('//*[text()="Company info"]')).click();
            browser.driver.executeScript("arguments[0].scrollIntoView(true);", element(by.id('companyAddressTab')).getWebElement());
            browser.findElement(by.id('companyAddAddresses-0')).click();
            browser.findElement(by.xpath("//*[@name='addressLine']")).sendKeys(companyinfotestdata[4].billingaddresses[0].addressLine);
            browser.findElement(by.xpath("//*[@name='city']")).sendKeys(companyinfotestdata[4].billingaddresses[0].city);
            browser.findElement(by.xpath("//*[@name='state']")).sendKeys(companyinfotestdata[4].billingaddresses[0].state);
            browser.findElement(by.xpath("//*[@name='country']")).sendKeys(companyinfotestdata[4].billingaddresses[0].country);
            browser.findElement(by.xpath("//*[@name='pinCode']")).sendKeys(companyinfotestdata[4].billingaddresses[0].pinCode);
            browser.findElement(by.id('companyAddAddresses-0')).click();
            browser.driver.executeScript("arguments[0].scrollIntoView(true);", element(by.xpath("//*[@name='pinCode']")).getWebElement());
            browser.findElement(by.xpath("//*[@name='addressLine']")).sendKeys(companyinfotestdata[4].billingaddresses[1].addressLine);
            browser.findElement(by.xpath("//*[@name='city']")).sendKeys(companyinfotestdata[4].billingaddresses[1].city);
            browser.findElement(by.xpath("//*[@name='state']")).sendKeys(companyinfotestdata[4].billingaddresses[1].state);
            browser.findElement(by.xpath("//*[@name='country']")).sendKeys(companyinfotestdata[4].billingaddresses[1].country);
            browser.findElement(by.xpath("//*[@name='pinCode']")).sendKeys(companyinfotestdata[4].billingaddresses[1].pinCode);
            companyInfoPage.form.submit.click();
            browser.sleep('4000');
        });

        //8.TESTCASE TO ADD MORE SHIPPING ADDRESSES AT COMPANY INFO
        it('add more shipping addresses at company info', function () {
            browser.findElement(by.xpath('//*[text()="Company info"]')).click();
            browser.driver.executeScript("arguments[0].scrollIntoView(true);", element(by.id('companyAddressTab')).getWebElement());
            browser.findElement(by.xpath('//*[@id="companyAddressTab"]//md-tab-item[2]')).click();
            browser.findElement(by.id('companyAddAddresses-1')).click();
            browser.findElement(by.xpath("//*[@name='addressLine']")).sendKeys(companyinfotestdata[5].shippingaddresses[0].addressLine);
            browser.findElement(by.xpath("//*[@name='city']")).sendKeys(companyinfotestdata[5].shippingaddresses[0].city);
            browser.findElement(by.xpath("//*[@name='state']")).sendKeys(companyinfotestdata[5].shippingaddresses[0].state);
            browser.findElement(by.xpath("//*[@name='country']")).sendKeys(companyinfotestdata[5].shippingaddresses[0].country);
            browser.findElement(by.xpath("//*[@name='pinCode']")).sendKeys(companyinfotestdata[5].shippingaddresses[0].pinCode);
            browser.findElement(by.id('companyAddAddresses-1')).click();
            browser.driver.executeScript("arguments[0].scrollIntoView(true);", element(by.xpath("//*[@name='pinCode']")).getWebElement());
            browser.findElement(by.xpath("//*[@name='addressLine']")).sendKeys(companyinfotestdata[5].shippingaddresses[1].addressLine);
            browser.findElement(by.xpath("//*[@name='city']")).sendKeys(companyinfotestdata[5].shippingaddresses[1].city);
            browser.findElement(by.xpath("//*[@name='state']")).sendKeys(companyinfotestdata[5].shippingaddresses[1].state);
            browser.findElement(by.xpath("//*[@name='country']")).sendKeys(companyinfotestdata[5].shippingaddresses[1].country);
            browser.findElement(by.xpath("//*[@name='pinCode']")).sendKeys(companyinfotestdata[5].shippingaddresses[1].pinCode);
            companyInfoPage.form.submit.click();
            browser.sleep('4000');
            });

        //9.TESTCASE TO ADD MORE RECEIVING ADDRESSES AT COMPANY INFO
        it('add more shipping addresses at company info', function () {
            browser.findElement(by.xpath('//*[text()="Company info"]')).click();
            browser.driver.executeScript("arguments[0].scrollIntoView(true);", element(by.id('companyAddressTab')).getWebElement());
            browser.findElement(by.xpath('//*[@id="companyAddressTab"]//md-tab-item[3]')).click();
            browser.findElement(by.id('companyAddAddresses-2')).click();
            browser.findElement(by.xpath("//*[@name='addressLine']")).sendKeys(companyinfotestdata[6].receivingaddresses[0].addressLine);
            browser.findElement(by.xpath("//*[@name='city']")).sendKeys(companyinfotestdata[6].receivingaddresses[0].city);
            browser.findElement(by.xpath("//*[@name='state']")).sendKeys(companyinfotestdata[6].receivingaddresses[0].state);
            browser.findElement(by.xpath("//*[@name='country']")).sendKeys(companyinfotestdata[6].receivingaddresses[0].country);
            browser.findElement(by.xpath("//*[@name='pinCode']")).sendKeys(companyinfotestdata[6].receivingaddresses[0].pinCode);
            browser.findElement(by.id('companyAddAddresses-2')).click();
            browser.driver.executeScript("arguments[0].scrollIntoView(true);", element(by.xpath("//*[@name='pinCode']")).getWebElement());
            browser.findElement(by.xpath("//*[@name='addressLine']")).sendKeys(companyinfotestdata[6].receivingaddresses[1].addressLine);
            browser.findElement(by.xpath("//*[@name='city']")).sendKeys(companyinfotestdata[6].receivingaddresses[1].city);
            browser.findElement(by.xpath("//*[@name='state']")).sendKeys(companyinfotestdata[6].receivingaddresses[1].state);
            browser.findElement(by.xpath("//*[@name='country']")).sendKeys(companyinfotestdata[6].receivingaddresses[1].country);
            browser.findElement(by.xpath("//*[@name='pinCode']")).sendKeys(companyinfotestdata[6].receivingaddresses[1].pinCode);
            companyInfoPage.form.submit.click();
            browser.sleep('4000');
        });

        //10.TESTCASE TO ADD MORE INVOICE ADDRESSES AT COMPANY INFO
        it('add more shipping addresses at company info', function () {
            browser.findElement(by.xpath('//*[text()="Company info"]')).click();
            browser.driver.executeScript("arguments[0].scrollIntoView(true);", element(by.id('companyAddressTab')).getWebElement());
            browser.findElement(by.xpath('//*[@id="companyAddressTab"]//md-tab-item[4]')).click();
            browser.findElement(by.id('companyAddAddresses-3')).click();
            browser.findElement(by.xpath("//*[@name='addressLine']")).sendKeys(companyinfotestdata[7].invoiceaddresses[0].addressLine);
            browser.findElement(by.xpath("//*[@name='city']")).sendKeys(companyinfotestdata[7].invoiceaddresses[0].city);
            browser.findElement(by.xpath("//*[@name='state']")).sendKeys(companyinfotestdata[7].invoiceaddresses[0].state);
            browser.findElement(by.xpath("//*[@name='country']")).sendKeys(companyinfotestdata[7].invoiceaddresses[0].country);
            browser.findElement(by.xpath("//*[@name='pinCode']")).sendKeys(companyinfotestdata[7].invoiceaddresses[0].pinCode);
            browser.findElement(by.id('companyAddAddresses-3')).click();
            browser.driver.executeScript("arguments[0].scrollIntoView(true);", element(by.xpath("//*[@name='pinCode']")).getWebElement());
            browser.findElement(by.xpath("//*[@name='addressLine']")).sendKeys(companyinfotestdata[7].invoiceaddresses[1].addressLine);
            browser.findElement(by.xpath("//*[@name='city']")).sendKeys(companyinfotestdata[7].invoiceaddresses[1].city);
            browser.findElement(by.xpath("//*[@name='state']")).sendKeys(companyinfotestdata[7].invoiceaddresses[1].state);
            browser.findElement(by.xpath("//*[@name='country']")).sendKeys(companyinfotestdata[7].invoiceaddresses[1].country);
            browser.findElement(by.xpath("//*[@name='pinCode']")).sendKeys(companyinfotestdata[7].invoiceaddresses[1].pinCode);
            companyInfoPage.form.submit.click();
            browser.sleep('4000');
        });

        //11.TESTCASE TO CHECK VALIDATION FOR GSTIN NUMBER AT COMPANY INFO
        it('checking validation for GSTIN number at company info',function(){
            browser.findElement(by.xpath('//*[text()="Company info"]')).click();
            companyInfoPage.form.gstinNo.sendKeys('12AAAGM0');
            companyInfoPage.form.gstinNumber.clear().sendKeys(companyinfotestdata[8].gstinNumber);
            companyInfoPage.form.submit.click();
            browser.sleep('4000');
        });

        //12.TESTCASE TO CHECK VALIDATION FOR PAN NUMBER AT COMPANY INFO
        it('checking validation for GSTIN number at company info',function(){
            browser.findElement(by.xpath('//*[text()="Company info"]')).click();
            companyInfoPage.form.panNo.sendKeys('AAA676');
            companyInfoPage.form.panNo.clear().sendKeys(companyinfotestdata[8].panNumber);
            companyInfoPage.form.submit.click();
            browser.sleep('4000');
        });

        //13.TESTCASE TO CHECK VALIDATION FOR PINCODE IN ADDRESS AT COMPANY INFO
        it('checking validation for picode in address at company info',function(){
            browser.findElement(by.xpath('//*[text()="Company info"]')).click();
            browser.driver.executeScript("arguments[0].scrollIntoView(true);", element(by.id('companyAddressTab')).getWebElement());
            browser.findElement(by.id('companyAddAddresses-0')).click();
            companyInfoPage.form.pinCode.sendKeys("51369874252");
            browser.findElement(by.xpath("//*[@name='pinCode']")).getAttribute('ng-minlength').toBe('6');
            browser.findElement(by.xpath("//*[@name='pinCode']")).getAttribute('ng-maxlength').toBe('6');
            browser.findElement(by.xpath("//*[@name='pinCode']")).getAttribute('only-digits').toBe('');
            companyInfoPage.form.pinCode.clear().sendKeys("515661");
            companyInfoPage.form.submit.click();
            browser.sleep('4000');
        });

        //14.TESTCASE TO UPDATE COMPANYINFO WITH ALL FIELDS
        it('update company info with all fields',function(){
            browser.findElement(by.xpath('//*[text()="Company info"]')).click();
            browser.driver.executeScript("arguments[0].scrollIntoView(true);", element(by.id('companyAddressTab')).getWebElement());
            companyInfoPage.form.gstinNo.clear().sendKeys(companyinfotestdata[9].gstinNumber);
            companyInfoPage.form.panNo.clear().sendKeys(companyinfotestdata[9].panNumber);
            browser.driver.executeScript("arguments[0].scrollIntoView(true);", element(by.id('companyAddressTab')).getWebElement());
            companyInfoPage.form.bankAccountNumber.clear().sendKeys(companyinfotestdata[9].bankAccountNumber);
            companyInfoPage.form.bankName.clear().sendKeys(companyinfotestdata[9].bankName);
            companyInfoPage.form.ifscCode.clear().sendKeys(companyinfotestdata[9].ifscCode);
            browser.findElement(by.id('companyAddPhones')).click();
            browser.findElement(by.id('companyPhoneNumber-0')).clear().sendKeys(companyinfotestdata[9].phoneNumber);
            browser.findElement(by.id('companyAddEmails')).click();
            browser.findElement(by.id('companyEmail-1')).clear().sendKeys(companyinfotestdata[9].email);
            browser.findElement(by.id('companyAddAddresses-0')).click();
            browser.findElement(by.xpath("//*[@name='addressLine']")).clear().sendKeys(companyinfotestdata[9].billingaddresses[0].addressLine);
            browser.findElement(by.xpath("//*[@name='city']")).clear().sendKeys(companyinfotestdata[9].billingaddresses[0].city);
            browser.findElement(by.xpath("//*[@name='state']")).clear().sendKeys(companyinfotestdata[9].billingaddresses[0].state);
            browser.findElement(by.xpath("//*[@name='country']")).clear().sendKeys(companyinfotestdata[9].billingaddresses[0].country);
            browser.findElement(by.xpath("//*[@name='pinCode']")).clear().sendKeys(companyinfotestdata[9].billingaddresses[0].pinCode);
            browser.findElement(by.xpath('//*[@id="companyAddressTab"]//md-tab-item[2]')).click();
            browser.findElement(by.id('companyAddAddresses-1')).click();
            browser.findElement(by.xpath("//*[@name='addressLine']")).clear().sendKeys(companyinfotestdata[9].shippingaddresses[0].addressLine);
            browser.findElement(by.xpath("//*[@name='city']")).clear().sendKeys(companyinfotestdata[9].shippingaddresses[0].city);
            browser.findElement(by.xpath("//*[@name='state']")).clear().sendKeys(companyinfotestdata[9].shippingaddresses[0].state);
            browser.findElement(by.xpath("//*[@name='country']")).clear().sendKeys(companyinfotestdata[9].shippingaddresses[0].country);
            browser.findElement(by.xpath("//*[@name='pinCode']")).clear().sendKeys(companyinfotestdata[9].shippingaddresses[0].pinCode);
            browser.findElement(by.xpath('//*[@id="companyAddressTab"]//md-tab-item[3]')).click();
            browser.findElement(by.id('companyAddAddresses-2')).click();
            browser.findElement(by.xpath("//*[@name='addressLine']")).clear().sendKeys(companyinfotestdata[9].receivingaddresses[0].addressLine);
            browser.findElement(by.xpath("//*[@name='city']")).clear().sendKeys(companyinfotestdata[9].receivingaddresses[0].city);
            browser.findElement(by.xpath("//*[@name='state']")).clear().sendKeys(companyinfotestdata[9].receivingaddresses[0].state);
            browser.findElement(by.xpath("//*[@name='country']")).clear().sendKeys(companyinfotestdata[9].receivingaddresses[0].country);
            browser.findElement(by.xpath("//*[@name='pinCode']")).clear().sendKeys(companyinfotestdata[9].receivingaddresses[0].pinCode);
            browser.findElement(by.xpath('//*[@id="companyAddressTab"]//md-tab-item[4]')).click();
            browser.findElement(by.id('companyAddAddresses-3')).click();
            browser.findElement(by.xpath("//*[@name='addressLine']")).clear().sendKeys(companyinfotestdata[9].invoiceaddresses[0].addressLine);
            browser.findElement(by.xpath("//*[@name='city']")).clear().sendKeys(companyinfotestdata[9].invoiceaddresses[0].city);
            browser.findElement(by.xpath("//*[@name='state']")).clear().sendKeys(companyinfotestdata[9].invoiceaddresses[0].state);
            browser.findElement(by.xpath("//*[@name='country']")).clear().sendKeys(companyinfotestdata[9].invoiceaddresses[0].country);
            browser.findElement(by.xpath("//*[@name='pinCode']")).clear().sendKeys(companyinfotestdata[9].invoiceaddresses[0].pinCode);
            companyInfoPage.form.submit.click();
            browser.sleep('4000');
        });
    });
});
