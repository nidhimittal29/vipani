'use strict';

var companyInfoPage = function() {
    var form = this.form = element(by.name('companyForm'));
    form.gstinNo = form.element(by.model('company.gstinNumber'));
    form.panNo = form.element(by.model('company.panNumber'));
    form.bankAccountNumber = form.element(by.model('company.bankAccountDetails.bankAccountNumber'));
    form.accountType = form.element(by.model('company.bankAccountDetails.accountType'));
    form.bankName = form.element(by.model('company.bankAccountDetails.bankName'));
    form.ifscCode = form.element(by.model('company.bankAccountDetails.ifscCode'));
    form.phoneType = form.element(by.model('phone.phoneType'));
    form.phoneNumber = form.element(by.model('phone.phoneNumber'));
    form.emailType = form.element(by.model('email.emailType'));
    form.email = form.element(by.model('email.email'));
    form.addressLine = form.element(by.model('address.addressLine'));
    form.city = form.element(by.model('address.city'));
    form.state = form.element(by.model('address.state'));
    form.country = form.element(by.model('address.country'));
    form.pinCode = form.element(by.model('address.pinCode'));
    form.submit = form.element(by.xpath('//*[@aria-label="Update company info"]'));

    this.companyInfo = function (data) {
        for (var prop in data) {
            var formElem = form[prop];
            if (data.hasOwnProperty(prop) && formElem && typeof formElem.sendKeys === 'function') {
                formElem.sendKeys(data[prop]);
            }
        }
    };
};
module.exports = new companyInfoPage();
