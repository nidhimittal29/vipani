'use strict';

var config = browser.params;
var user = require('../../account/common.util.e2e/signin.common.po');
var global = require('../../account/common.util.e2e/global.common.po');
var BUserTestData=require('./createbusinessuser.testdata'),createBuserForm=require('./createbusinessuser.po');


describe('Create Business User View', function() {
    var loadPage = function () {
        browser.manage().deleteAllCookies();
        browser.get('http://localhost:3000/#!/signin');

    };

    describe('with local auth', function () {
        beforeAll(function(){
            global.loadPage('http://localhost:3000/#!/register');
            user.registerProcess(BUserTestData[0]);
        });
        beforeEach(function () {
            loadPage();
            user.login(BUserTestData[0]);
            browser.sleep('3000');
            browser.findElement(by.id('profilemenu')).click();
            browser.sleep('2000');
            browser.findElement(by.id('companyprofile')).click();
            browser.findElement(by.xpath('//*[@id="business-tabs"]//md-tab-item[2]')).click();
        });

        afterEach(function () {
            user.logout();

        });

        function createNewEmployee(userGroup,userName) {
            browser.findElement(by.id('add-business-user')).click();
            createBuserForm.form.userGroup.click();
            browser.findElement(by.id(userGroup)).click();
            createBuserForm.form.userName.sendKeys(userName);
            createBuserForm.form.submit.click();
            browser.sleep(4000);
        }

        //1.TEST CASE TO CHECK DEFAULT BUSINESS USER
        it('Check default business user',function () {
            expect(browser.findElement(by.xpath('//*[@id="businessUserTable"]/tbody/tr[1]/td[3]')).getText()).toBe(BUserTestData[0].username);
            expect(browser.findElement(by.xpath('//*[@id="businessUserTable"]/tbody/tr[1]/td[4]')).getText()).toBe('Admin');
        });

        //2.TEST CASE TO CREATE  BUSINESS USER AS ADMIN AT COMPANY LEVEL +Ve TEST CASE
        it('Create business user as admin at company level', function () {
            browser.findElement(by.id('add-business-user')).click();
            createBuserForm.form.userGroup.click();
            browser.findElement(by.id('Admin')).click();
            createBuserForm.form.userName.sendKeys(BUserTestData[1].userName);
            createBuserForm.form.submit.click();
            browser.sleep(4000);
        });

        //3.TEST CASE TO VALIDATE FOR  CREATE  BUSINESS USER AT COMPANY LEVEL -Ve TEST CASE
        it('Validation for Create business user at company level', function () {
            browser.findElement(by.id('add-business-user')).click();
            expect(browser.findElement(by.id('create-business-user')).isEnabled()).toBe(false);
            createBuserForm.form.userGroup.click();
            browser.findElement(by.id('Admin')).click();
            expect(browser.findElement(by.id('create-business-user')).isEnabled()).toBe(false);
            createBuserForm.form.userName.sendKeys('hgdahsgdasfd');
            expect(browser.findElement(by.id('create-business-user')).isEnabled()).toBe(false);
            createBuserForm.form.userName.clear().sendKeys(BUserTestData[2].userName);
            expect(browser.findElement(by.id('create-business-user')).isEnabled()).toBe(true);
            createBuserForm.form.submit.click();
            browser.sleep('4000');
        });

        //4.TEST CASE TO CHECK CREATE  BUSINESS USER WITH ALREADY REGISTERED USER AT COMPANY LEVEL -Ve TEST CASE
        it('Should not able to create business user with already registered user', function () {
            browser.findElement(by.id('add-business-user')).click();
            createBuserForm.form.userGroup.click();
            browser.findElement(by.id('Admin')).click();
            createBuserForm.form.userName.sendKeys(BUserTestData[0].username);
            browser.findElement(by.id('create-business-user')).click();
            browser.sleep(3000);
            browser.findElement(by.id('close-add-business-user')).click();
            browser.sleep(1500);
            //expect(browser.findElement(By.css('.ng-scope .md-error-toast-theme .md-bottom .md-right')).getText()).toBe('User is already used for some other company');

        });

        //5.TEST CASE TO CREATE  BUSINESS USER AS MANAGER AT COMPANY LEVEL +Ve TEST CASE
        it('Create business user as manager user at company level', function () {
            browser.findElement(by.id('add-business-user')).click();
            createBuserForm.form.userGroup.click();
            browser.findElement(by.id('Manager')).click();
            createBuserForm.form.userName.sendKeys(BUserTestData[3].userName);
            createBuserForm.form.submit.click();
            browser.sleep('4000');
        });

        //6.TEST CASE TO CREATE  BUSINESS USER AS EMPLOYEE AT COMPANY LEVEL +Ve TEST CASE
        it('Create business user as employee at company level', function () {
            browser.findElement(by.id('add-business-user')).click();
            createBuserForm.form.userGroup.click();
            browser.findElement(by.id('Employee')).click();
            createBuserForm.form.userName.sendKeys(BUserTestData[4].userName);
            createBuserForm.form.submit.click();
            browser.sleep('4000');
        });

        //7.TEST CASE TO CHECK GROUP ACTIONS VISIBILITY -Ve TEST CASE
        it('Group actions should not visible when business users not selected',function () {
            browser.findElement(by.id('add-business-user')).click();
            createBuserForm.form.userGroup.click();
            browser.findElement(by.id('Manager')).click();
            createBuserForm.form.userName.sendKeys('group@check.com');
            createBuserForm.form.submit.click();
            browser.sleep(4000);
            expect(browser.findElement(by.id('user-group-actions')).isDisplayed()).toBe(false);
            browser.findElement(by.xpath('//*[@id="businessUserTable"]/tbody/tr[2]/td[1]/md-checkbox')).click();
            expect(browser.findElement(by.id('user-group-actions')).isDisplayed()).toBe(true);
        });

        //8.DISABLE AND ENABLE SHOULD NOT WORK IF EMPLOYEE STATUS IS REQUEST -Ve TEST CASE
        it('Disable and enable should not work if employee status is request',function () {
            createNewEmployee('Manager','request@test.com');
            browser.findElement(by.xpath('//*[@id="businessUserTable"]/tbody/tr[2]/td[1]/md-checkbox')).click();
            browser.findElement(by.id('user-group-actions')).click();
            browser.findElement(by.id('disable-mass-users')).click();
            expect(browser.findElement(by.xpath('//*[@id="businessUserTable"]//tr[2]/td[5]/span')).getText()).toBe('Request');
            browser.findElement(by.id('user-group-actions')).click();
            var link='';
            browser.findElement(by.id('enable-mass-users')).click();
            expect(browser.findElement(by.xpath('//*[@id="businessUserTable"]/tbody/tr[2]/td[5]/span')).getText()).toBe('Request');
            browser.sleep(2000);
        });

        //9.TEST CASE TO CHECK DELETE IS WORKING FOR ONE BUSINESS USER OR NOT +ve TEST CASE
        it('Delete should work for one business user',function () {
            createNewEmployee('Manager',BUserTestData[5].userName);
            browser.findElement(by.xpath('//*[@id="businessUserTable"]/tbody/tr[2]/td[1]/md-checkbox')).click();
            browser.findElement(by.id('user-group-actions')).click();
            browser.findElement(by.id('delete-mass-users')).click();
            browser.sleep(2000);
        });

        //10.TEST CASE TO CHECK DELETE IS WORKING FOR MULTIPLE BUSINESS USER OR NOT +ve TEST CASE
        it('Delete should work for multiple business user',function () {
            createNewEmployee('Manager',BUserTestData[6].userName);
            createNewEmployee('Manager',BUserTestData[7].userName);
            browser.findElement(by.xpath('//*[@id="businessUserTable"]/tbody/tr[2]/td[1]/md-checkbox')).click();
            browser.findElement(by.xpath('//*[@id="businessUserTable"]/tbody/tr[3]/td[1]/md-checkbox')).click();
            browser.findElement(by.id('user-group-actions')).click();
            browser.findElement(by.id('delete-mass-users')).click();
            browser.sleep(2000);
        });

         //11.SHOULD NOT CHANGE STATUS IF BUSINESS USER IS DEFAULT USER AND IS IN ACTIVE AT LIST VIEW +Ve TEST CASE
        it('Should not Change status if business user is default user and is in active at list view',function () {
            expect( browser.findElement(by.xpath('//*[@id="businessUserTable"]/tbody/tr[1]/td[3]')).getText()).toBe(BUserTestData[0].username);
            expect( browser.findElement(by.xpath('//*[@id="businessUserTable"]/tbody/tr[1]/td[1]/md-checkbox')).isDisplayed()).toBe(false);
            expect(browser.findElement(by.xpath('//*[@id="businessUserTable"]/tbody/tr[1]/td[5]/span')).getText()).toBe('Active');
            expect(browser.findElement(by.id('user-group-actions')).isDisplayed()).toBe(false);
        });

        //12.Check digital signature upload while create business user +ve test
        it('Should upload digital signature while create business user',function () {
            browser.findElement(by.id('add-business-user')).click();
            createBuserForm.form.userGroup.click();
            browser.findElement(by.id('Manager')).click();
            createBuserForm.form.userName.sendKeys(BUserTestData[9].userName);
            var absolutePath = path.resolve(__dirname, '../images/digitalsignature.png');
            createBuserForm.form.digitalSignature.sendKeys(absolutePath);
            browser.sleep(2000);
            expect(browser.findElement(by.xpath('//div[@id="digital-signature"]//!*[@class="fa fa-eye"]')).isDisplayed()).toBe(true);
            expect(browser.findElement(by.xpath('//div[@id="digital-signature"]//!*[@class="fa fa-download"]')).isDisplayed()).toBe(true);
            createBuserForm.form.submit.click();
            browser.sleep(3000);
        });

        //13.CREATE BUSINESS USER AT COMPANY LEVEL AND ACTIVATE +VE TEST CASE
        it('Create business user and should activate ',function () {
            browser.findElement(by.id('add-business-user')).click();
            createBuserForm.form.userGroup.click();
            browser.findElement(by.id('Manager')).click();
            createBuserForm.form.userName.sendKeys(BUserTestData[10].userName);
            createBuserForm.form.submit.click();
            browser.sleep('1000');
            var link='';
            var text1 = browser.findElement(by.binding('success')).getText().then(function (ss1) {

                return ss1.split('. ');
            });
            text1.then(function (slices) {

                var text = slices[1];
                link = text.split(' ');
            });
            browser.findElement(by.id('close-add-business-user')).click();
            user.logout();
            global.loadPage(link);
            expect(browser.findElement(by.xpath('//h4')).getText()).toBe(BUserTestData[0].userName);
            expect(browser.findElement(by.model('credentials.username')).getText()).toBe(BUserTestData[10].userName);
            browser.findElement(by.model('credentials.username')).sendKeys('password');
            browser.findElement(by.id('activate')).click();
        });

        //14.TEST CASE TO CHECK DISABLE AND ENABLE IS WORKING OR NOT +ve TEST CASE
        it('Disable and enable should work',function () {
            browser.findElement(by.xpath('//!*[@id="businessUserTable"]/tbody/tr[2]/td[1]/md-checkbox')).click();
            browser.findElement(by.id('user-group-actions')).click();
            browser.findElement(by.id('disable-mass-users')).click();
            expect(browser.findElement(by.xpath('//!*[@id="businessUserTable"]//tr[2]/td[5]/span')).getText()).toBe('Inactive');
            browser.findElement(by.xpath('//!*[@id="businessUserTable"]/tbody/tr[2]/td[1]')).click();
            browser.findElement(by.id('user-group-actions')).click();
            browser.findElement(by.id('enable-mass-users')).click();
            expect(browser.findElement(by.xpath('//!*[@id="businessUserTable"]//tr[2]/td[5]/span')).getText()).toBe('Active');
        });
    });
});
