'use strict';

var config = browser.params;
var path = require('path');
var user = require('../../account/common.util.e2e/signin.common.po');
var global = require('../../account/common.util.e2e/global.common.po');
var testData=require('./editbusinessuser.testdata'),editBuserForm=require('./editbusinessuser.po'),createBuserForm=require('../createbusinessuser/createbusinessuser.po');
var toast;
describe('Edit Business User View', function() {
    var loadPage = function () {
        browser.manage().deleteAllCookies();
        browser.get('http://localhost:3000/#!/signin');

    };

    describe('with local auth', function () {
        beforeAll(function () {
            global.loadPage('http://localhost:3000/#!/register');
            user.registerProcess(testData[0]);

        });
        beforeEach(function () {
            loadPage();
            user.login(testData[0]);
            browser.sleep('3000');
            browser.findElement(by.id('profilemenu')).click();
            browser.findElement(by.id('companyprofile')).click();
            browser.findElement(by.xpath('//*[@id="business-tabs"]//md-tab-item[2]')).click();
        });

        afterEach(function () {
            user.logout();
        });
        //1.UPDATE SHOULD NOT POSSIBLE UNTIL EDIT BUTTON SELECTED -VE TEST CASE
        //DELETE SHOULD NOT VISIBLE UNTIL EDIT BUTTON SELECTED -VE TEST CASE
        //TEST CASE TO UPDATE BUSINESS USER +Ve TEST CASE
        it('Update business user details',function () {
            browser.findElement(by.id('add-business-user')).click();
            createBuserForm.form.userGroup.click();
            browser.findElement(by.id('Manager')).click();
            createBuserForm.form.userName.sendKeys(testData[1].userName);
            createBuserForm.form.submit.click();
            browser.sleep(4000);
            browser.findElement(by.xpath('//*[@id="businessUserTable"]/tbody/tr[2]/td[3]')).click();
            expect(browser.findElement(by.id('user-delete')).isDisplayed()).toBe(false);
            expect(editBuserForm.form.submit.isDisplayed()).toBe(false);
            browser.findElement(by.id('user-edit')).click();
            expect(browser.findElement(by.id('user-delete')).isDisplayed()).toBe(true);
            expect(editBuserForm.form.submit.isDisplayed()).toBe(true);
            expect(editBuserForm.form.userGroup.isDisplayed()).toBe(true);
            editBuserForm.form.userGroup.click();
            browser.findElement(by.id('Employee')).click();
            editBuserForm.form.submit.click();
            expect(browser.findElement(by.xpath('//*[@id="businessUserTable"]/tbody/tr[2]/td[4]')).getText()).toBe('Employee');
            browser.sleep(4000);
        });

        //2.DELETE SHOULD NOT POSSIBLE IF BUSINESS USER IS DEFAULT USER AT DETAILED VIEW -VE TEST CASE
        it('Delete should not possible if business user is default user at detailed view',function () {
            browser.findElement(by.xpath('//*[@id="businessUserTable"]/tbody/tr[1]/td[3]')).click();
            browser.sleep(3000);
            expect(browser.findElement(by.id('user-delete')).isDisplayed()).toBe(false);
            expect(editBuserForm.form.submit.isDisplayed()).toBe(false);
            browser.findElement(by.id('user-edit')).click();
            expect(browser.findElement(by.id('user-delete')).isDisplayed()).toBe(false);
            expect(editBuserForm.form.submit.isDisplayed()).toBe(true);
            browser.findElement(by.id('user-cancel')).click();
        });
        //3.BUSINESS USER SHOULD DELETE WHILE EDIT OF BUSINESS USER +VE TEST CASE
        it('Business user should delete while edit',function () {
            browser.findElement(by.id('add-business-user')).click();
            createBuserForm.form.userGroup.click();
            browser.findElement(by.id('Manager')).click();
            createBuserForm.form.userName.sendKeys(testData[2].userName);
            createBuserForm.form.submit.click();
            browser.sleep(4000);
            browser.driver.executeScript("arguments[0].scrollIntoView(true);", element(by.xpath('//*[@id="businessUserTable"]/tbody/tr[3]/td[3]')).getWebElement());
            browser.findElement(by.xpath('//*[@id="businessUserTable"]/tbody/tr[3]/td[3]')).click();
            expect(browser.findElement(by.id('user-delete')).isDisplayed()).toBe(false);
            expect(editBuserForm.form.submit.isDisplayed()).toBe(false);
            browser.findElement(by.id('user-edit')).click();
            expect(browser.findElement(by.id('user-delete')).isDisplayed()).toBe(true);
            browser.findElement(by.id('user-delete')).click();
            browser.sleep(2000);
        });
        /**
         * case 4:Upload digital signature while edit business user +ve
         */
        it('Should be able to upload digital signature of business user',function () {
            browser.findElement(by.id('add-business-user')).click();
            createBuserForm.form.userGroup.click();
            browser.findElement(by.id('Manager')).click();
            createBuserForm.form.userName.sendKeys(testData[3].userName);
            createBuserForm.form.submit.click();
            browser.sleep(4000);
            browser.driver.executeScript("arguments[0].scrollIntoView(true);", element(by.xpath('//*[@id="businessUserTable"]/tbody/tr[3]/td[3]')).getWebElement());
            browser.findElement(by.xpath('//*[@id="businessUserTable"]/tbody/tr[3]/td[3]')).click();
            browser.findElement(by.id('user-edit')).click();
            var absolutePath = path.resolve(__dirname, '../images/signature.png');
            editBuserForm.form.digitalSignature.sendKeys(absolutePath);
            browser.sleep(2000);
            expect(browser.findElement(by.xpath('//dive[@id="edit-digital-signature"]//*[@class="fa fa-eye"]')).isDisplayed()).toBe(true);
            expect(browser.findElement(by.xpath('//dive[@id="edit-digital-signature"]//*[@class="fa fa-download"]')).isDisplayed()).toBe(true);
            editBuserForm.form.submit.click();
            browser.sleep(2000);
        });
    });
});
