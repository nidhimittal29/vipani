'use strict';

var createBusinessUserPage = function() {
    var form = this.form = element(by.name('editBusinessUserForm'));
    form.userGroup = form.element(by.model('businessUser.userGroup._id'));
    form.userName = form.element(by.model('businessUser.username'));
    form.digitalSignature = form.element(by.model('businessUser.digitalSignature'));
    form.submit=form.element(by.id('update-business-user'));
    this.createBusinessUser = function(data) {
        for (var prop in data) {
            var formElem = form[prop];
            if (data.hasOwnProperty(prop) && formElem && typeof formElem.sendKeys === 'function') {
                formElem.sendKeys(data[prop]);
            }
        }
    };
};

module.exports = new createBusinessUserPage();
