


'use strict';

var config = browser.params;
var user=require('../../account/common.util.e2e/signin.common.po');
var product=require('../../account/common.util.e2e/product.common.po');

var global = require('../../account/common.util.e2e/global.common.po');
describe('LandingPage View', function() {
    var page;
    var page1;
    var product_testdata;
    var loginDetails=require('./login');
    product_testdata = require('./../product_testdata');
    page1 = require('./../products.po');
    var inventory;
    var loadPage = function () {
        browser.manage().deleteAllCookies();
        browser.get('http://localhost:3000/#!/signin');
        browser.sleep(1000);
    };

    describe('with local auth', function () {
        beforeAll(function(){
            global.loadPage('http://localhost:3000/#!/register');
            user.registerProcess(loginDetails[0]);
        });
        beforeEach(function () {
            loadPage();

            user.signin(loginDetails[0]);
            user.getForm().submit.click();
            browser.sleep(500);


        });

        afterEach(function () {

            user.logout();
        });
        it('should import products first so that a brand is added',function(){
            //no validations here, only for importing and creatng a brand, end of it inventories will be deleted.
            browser.sleep(200);
            product.fileImportProducts('../../products/importtestdata/ImportWithData.csv');
            //select all categories
            element(by.xpath('//md-checkbox[@aria-label=\'Cereals\']')).click();
            browser.sleep(1000);
            element(by.css('button[aria-label=\'Proceed to upload file\']')).click();
            browser.sleep(1000);
            element(by.xpath('//div[@aria-hidden=\'false\']/div[@aria-hidden=\'false\']//md-checkbox[@id=\'all\']')).click();
            browser.sleep(1000);
            // browser.driver.executeScript("arguments[0].scrollIntoView(true);",
            //     element(by.css('button[aria-label="adding inventory"]')));
            element(by.css('button[aria-label=\'Proceed to upload file\']')).click();
            browser.sleep(500);
            element(by.css('button[aria-label=\'Proceed to upload file\']')).click();
            browser.sleep(500);
            product.deleteAllImported();
            browser.sleep(1000);

        });


        //1.TEST CASE FOR CREATE INVENTORY
        it('should login user, Checking Functionalities in Landing Page and redirecting to "/"', function () {

            product.initiateCreateProduct();
            browser.sleep(200);
            element(by.xpath("//*[@aria-label='Category']")).click();
            browser.sleep(200);
            element(by.xpath("//*[contains(text(),'" + product_testdata.category + "')]")).click();
            browser.sleep(200);
            element(by.xpath("//*[@aria-label='Product']")).click();
            browser.sleep(200);
            element(by.xpath("//*[contains(text(),'" + product_testdata.productName + "')]")).click();
            browser.sleep(200);
            element(by.xpath("//*[@aria-label='Brand']")).click();
            browser.sleep(200);
            element(by.xpath("//*[@class=\"md-select-menu-container md-active md-clickable\"]//*[contains(text(),'" + product_testdata.brandName + "')]")).click();
            browser.sleep(200);
            element(by.xpath("//div[@id=\"inventory-0\"]//*[@aria-label=\"Unit Measure\"]")).click();
            browser.sleep(200);
            element(by.xpath("//*[@class=\"md-select-menu-container md-active md-clickable\"]//*[contains(text(),'" + product_testdata.uom + "')]")).click();
            browser.sleep(200);
            page1.form.numberOfUnits.sendKeys(product_testdata.numberOfUnits);
            page1.form.mrp.sendKeys(product_testdata.MRP);
            element(by.xpath("//*[@aria-label='Create Product']")).click();
            browser.sleep(200);
            inventory = element(by.xpath("//td[.//h5[contains(text(),'" + product_testdata.inventoryName + " - " + product_testdata.uom + "')] and .//h6[contains(text(),'" + product_testdata.brandName + "')]]"));
            product.deleteAllImported();
            browser.sleep(1000);



        });



         //2.TEST CASE FOR VALIDATION OF UOM WHILE CREATING INVENTORY.
        it('should login user, uom validation while creating inventory "/"', function () {
            product.initiateCreateProduct();
            browser.sleep(200);
            element(by.xpath("//*[@aria-label='Category']")).click();
            browser.sleep(200);
            element(by.xpath("//*[contains(text(),'" + product_testdata.category + "')]")).click();
            browser.sleep(200);
            element(by.xpath("//*[@aria-label='Product']")).click();
            browser.sleep(200);
            browser.driver.findElement(by.xpath("//*[contains(text(),'" + product_testdata.productName + "')]")).click();
            browser.sleep(200);
            element(by.xpath("//*[@aria-label='Brand']")).click();
            browser.sleep(200);
            element(by.xpath("//*[@class=\"md-select-menu-container md-active md-clickable\"]//*[contains(text(),'" + product_testdata.brandName + "')]")).click();
            browser.sleep(200);
            browser.driver.findElement(by.xpath("//*[@aria-label='Create Product']")).click();
            browser.sleep(200);
            browser.driver.findElement(by.xpath("//*[@aria-label='Create Product']")).click();
            expect(element(by.xpath("//*[@class=\"md-input-message-animation ng-scope\"]")).getText()).toBe('UOM is required');
            browser.driver.findElement(by.xpath("//*[@aria-label='Create Product']")).click();
            inventory = element(by.xpath("//td[.//h5[contains(text(),'" + product_testdata.inventoryName + " - " + product_testdata.uom + "')] and .//h6[contains(text(),'" + product_testdata.brandName + "')]]"));
            browser.sleep(200);
            browser.driver.findElement(by.xpath("//*[@class=\"fa fa-times fa-lg ng-scope material-icons\"]")).click();
            browser.sleep(1000);
            });

       //3.TEST CASE FOR VALIDATION OF NUMBER OF UNITS WHILE CREATING INVENTORY.
       it('should login user, validate number of units while creating inventory "/"', function () {
            product.initiateCreateProduct();
            browser.sleep(200);
            element(by.xpath("//*[@aria-label='Category']")).click();
            browser.sleep(200);
            element(by.xpath("//*[contains(text(),'" + product_testdata.category + "')]")).click();
            browser.sleep(200);
            element(by.xpath("//*[@aria-label='Product']")).click();
            browser.sleep(200);
            browser.driver.findElement(by.xpath("//*[contains(text(),'" + product_testdata.productName + "')]")).click();
            browser.sleep(200);
            element(by.xpath("//*[@aria-label='Brand']")).click();
            browser.sleep(200);
            element(by.xpath("//*[@class=\"md-select-menu-container md-active md-clickable\"]//*[contains(text(),'" + product_testdata.brandName + "')]")).click();
            browser.sleep(200);
            browser.driver.findElement(by.xpath("//*[@aria-label='Create Product']")).click();
            browser.sleep(200);
            browser.driver.findElement(by.xpath("//*[@aria-label='Create Product']")).click();
            expect(element(by.xpath("//*[@class=\"md-input-message-animation ng-scope\"]")).getText()).toBe('UOM is required');
            element(by.xpath("//div[@id=\"inventory-0\"]//*[@aria-label=\"Unit Measure\"]")).click();
            browser.sleep(200);
            element(by.xpath("//*[@class=\"md-select-menu-container md-active md-clickable\"]//*[contains(text(),'" + product_testdata.uom + "')]")).click();
            browser.sleep(200);
            browser.driver.findElement(by.xpath("//*[@aria-label='Create Product']")).click();
            /*expect(page1.form.numofunits.getAttribute('name')).toEqual(browser.driver.switchTo().activeElement().getAttribute('name'));*/
            browser.sleep(200);
           browser.driver.findElement(by.xpath("//*[@class=\"fa fa-times fa-lg ng-scope material-icons\"]")).click();
           browser.sleep(1000);
        });

        //4.TEST CASE FOR VALIDATION OF MRP WHILE CREATING INVENTORY.
        it('should login user, validate mrp while creating inventory"/"', function () {
            product.initiateCreateProduct();
            browser.sleep(200);
            element(by.xpath("//*[@aria-label='Category']")).click();
            browser.sleep(200);
            element(by.xpath("//*[contains(text(),'" + product_testdata.category + "')]")).click();
            browser.sleep(200);
            element(by.xpath("//*[@aria-label='Product']")).click();
            browser.sleep(200);
            browser.driver.findElement(by.xpath("//*[contains(text(),'" + product_testdata.productName + "')]")).click();
            browser.sleep(200);
            element(by.xpath("//*[@aria-label='Brand']")).click();
            browser.sleep(200);
            element(by.xpath("//*[@class=\"md-select-menu-container md-active md-clickable\"]//*[contains(text(),'" + product_testdata.brandName + "')]")).click();
            browser.sleep(200);
            browser.driver.findElement(by.xpath("//*[@aria-label='Create Product']")).click();
            browser.sleep(200);
            browser.driver.findElement(by.xpath("//*[@aria-label='Create Product']")).click();
            expect(element(by.xpath("//*[@class=\"md-input-message-animation ng-scope\"]")).getText()).toBe('UOM is required');
            element(by.xpath("//div[@id=\"inventory-0\"]//*[@aria-label=\"Unit Measure\"]")).click();
            browser.sleep(200);
            element(by.xpath("//*[@class=\"md-select-menu-container md-active md-clickable\"]//*[contains(text(),'" + product_testdata.uom + "')]")).click();
            browser.sleep(200);
            browser.driver.findElement(by.xpath("//*[@aria-label='Create Product']")).click();
            /*expect(page1.form.numberOfUnits.getAttribute('name')).toEqual(browser.driver.switchTo().activeElement().getAttribute('name'));*/
            browser.sleep(200);
            page1.form.numberOfUnits.sendKeys(product_testdata.numberOfUnits);
            browser.sleep(200);
            browser.driver.findElement(by.xpath("//*[@aria-label='Create Product']")).click();
            browser.driver.findElement(by.xpath("//*[@class=\"fa fa-times fa-lg ng-scope material-icons\"]")).click();
            browser.sleep(500);
        });

          //5.TEST CASE FOR CREATE INVENTORY WITH NEW UOM
         it('should login user, create a product with new uom"/"', function () {
             product.initiateCreateProduct();
             browser.sleep(200);
            element(by.xpath("//*[@aria-label='Category']")).click();
            browser.sleep(200);
            element(by.xpath("//*[contains(text(),'" + product_testdata.category + "')]")).click();
            browser.sleep(200);
            element(by.xpath("//*[@aria-label='Product']")).click();
            browser.sleep(200);
            browser.driver.findElement(by.xpath("//*[contains(text(),'" + product_testdata.productName + "')]")).click();
            browser.sleep(200);
            element(by.xpath("//*[@aria-label='Brand']")).click();
            browser.sleep(200);
            element(by.xpath("//*[@class=\"md-select-menu-container md-active md-clickable\"]//*[contains(text(),'" + product_testdata.brandName + "')]")).click();
            browser.sleep(200);
            element(by.xpath("//div[@id=\"inventory-0\"]//*[@aria-label=\"Unit Measure\"]")).click();
            browser.sleep(500);
            element(by.xpath("//*[@class=\"md-select-menu-container md-active md-clickable\"]//*[contains(text(),'" + product_testdata.addUOM + "')]")).click();
            browser.sleep(200);
            element(by.xpath("//*[@aria-label='First Unit Measure']")).click();
            browser.sleep(200);
            browser.driver.findElement(by.xpath("//*[@class=\"md-select-menu-container md-active md-clickable\"]//*[contains(text(),'" + product_testdata.firstUOM + "')]")).click();
            browser.sleep(200);
            page1.form.size.sendKeys(product_testdata.size)
            element(by.xpath("//*[@aria-label='Second Unit Measure']")).click();
            browser.sleep(200);
            browser.driver.findElement(by.xpath("//*[@class=\"md-select-menu-container md-active md-clickable\"]//*[contains(text(),'" + product_testdata.secondUOM + "')]")).click();
            browser.sleep(200);
            page1.form.numberOfUnits.sendKeys(product_testdata.numberOfUnits);
            page1.form.mrp.sendKeys(product_testdata.MRP);
            browser.driver.findElement(by.xpath("//*[@aria-label='Create Product']")).click();
            browser.sleep(200);
            product.deleteAllImported();
            });

         //6.TEST CASE FOR VALIDATION OF FIRST UOM WHILE CREATING NEW UOM
            it('should login user, validate first uom while creating new uom"/"', function () {
            product.initiateCreateProduct();
            browser.sleep(200);
           element(by.xpath("//*[@aria-label='Category']")).click();
           browser.sleep(200);
           element(by.xpath("//*[contains(text(),'" + product_testdata.category + "')]")).click();
           browser.sleep(200);
           element(by.xpath("//*[@aria-label='Product']")).click();
           browser.sleep(200);
           browser.driver.findElement(by.xpath("//*[contains(text(),'" + product_testdata.productName + "')]")).click();
           browser.sleep(200);
           element(by.xpath("//*[@aria-label='Brand']")).click();
           browser.sleep(200);
           element(by.xpath("//*[@class=\"md-select-menu-container md-active md-clickable\"]//*[contains(text(),'" + product_testdata.brandName + "')]")).click();
           browser.sleep(200);
           element(by.xpath("//div[@id=\"inventory-0\"]//*[@aria-label=\"Unit Measure\"]")).click();
           browser.sleep(500);
           element(by.xpath("//*[@class=\"md-select-menu-container md-active md-clickable\"]//*[contains(text(),'" + product_testdata.addUOM + "')]")).click();
           browser.sleep(200);
           browser.driver.findElement(by.xpath("//*[@aria-label='Create Product']")).click();
           browser.sleep(200);
           expect(element(by.xpath("//*[@aria-label='First Unit Measure']")).isDisplayed()).toBe(true);
           browser.sleep(200);
           browser.driver.findElement(by.xpath("//*[@class=\"fa fa-times fa-lg ng-scope material-icons\"]")).click();
           browser.sleep(500);
           });

          //7.TEST CASE FOR VALIDATION OF SIZE WHILE CREATING NEW UOM
            it('should login user, validation of size while creating new uom', function () {
            product.initiateCreateProduct();
            browser.sleep(200);
            element(by.xpath("//*[@aria-label='Category']")).click();
            browser.sleep(200);
            element(by.xpath("//*[contains(text(),'" + product_testdata.category + "')]")).click();
            browser.sleep(200);
            element(by.xpath("//*[@aria-label='Product']")).click();
            browser.sleep(200);
            browser.driver.findElement(by.xpath("//*[contains(text(),'" + product_testdata.productName + "')]")).click();
            browser.sleep(200);
            element(by.xpath("//*[@aria-label='Brand']")).click();
            browser.sleep(200);
            element(by.xpath("//*[@class=\"md-select-menu-container md-active md-clickable\"]//*[contains(text(),'" + product_testdata.brandName + "')]")).click();
            browser.sleep(200);
            element(by.xpath("//div[@id=\"inventory-0\"]//*[@aria-label=\"Unit Measure\"]")).click();
            browser.sleep(500);
            element(by.xpath("//*[@class=\"md-select-menu-container md-active md-clickable\"]//*[contains(text(),'" + product_testdata.addUOM + "')]")).click();
            browser.sleep(200);
            element(by.xpath("//*[@aria-label='First Unit Measure']")).click();
            browser.sleep(200);
            browser.driver.findElement(by.xpath("//*[@class=\"md-select-menu-container md-active md-clickable\"]//*[contains(text(),'" + product_testdata.firstUOM + "')]")).click();
            browser.sleep(200);
            page1.form.numberOfUnits.sendKeys(product_testdata.numberOfUnits);
            browser.sleep(200);
            page1.form.mrp.sendKeys(product_testdata.MRP);
            browser.sleep(200);
            browser.driver.findElement(by.xpath("//*[@aria-label='Create Product']")).click();
            browser.sleep(200);
            expect(page1.form.size.getAttribute('name')).toEqual(browser.driver.switchTo().activeElement().getAttribute('name'));
            browser.sleep(200);
            browser.driver.findElement(by.xpath("//*[@aria-label='Create Product']")).click();
            browser.sleep(200);
            browser.driver.findElement(by.xpath("//*[@class=\"fa fa-times fa-lg ng-scope material-icons\"]")).click();
            browser.sleep(500);
        });


          //8.TEST CASE FOR VALIDATION OF SECOND UOM WHILE CREATING NEW UOM
            it('should login user, validation of second uom while creating new uom', function () {
            product.initiateCreateProduct();
            browser.sleep(200);
            element(by.xpath("//*[@aria-label='Category']")).click();
            browser.sleep(200);
            element(by.xpath("//*[contains(text(),'" + product_testdata.category + "')]")).click();
            browser.sleep(200);
            element(by.xpath("//*[@aria-label='Product']")).click();
            browser.sleep(200);
            browser.driver.findElement(by.xpath("//*[contains(text(),'" + product_testdata.productName + "')]")).click();
            browser.sleep(200);
            element(by.xpath("//*[@aria-label='Brand']")).click();
            browser.sleep(200);
            element(by.xpath("//*[@class=\"md-select-menu-container md-active md-clickable\"]//*[contains(text(),'" + product_testdata.brandName + "')]")).click();
            browser.sleep(200);
            element(by.xpath("//div[@id=\"inventory-0\"]//*[@aria-label=\"Unit Measure\"]")).click();
            browser.sleep(500);
            element(by.xpath("//*[@class=\"md-select-menu-container md-active md-clickable\"]//*[contains(text(),'" + product_testdata.addUOM + "')]")).click();
            browser.sleep(200);
            element(by.xpath("//*[@aria-label='First Unit Measure']")).click();
            browser.sleep(200);
            browser.driver.findElement(by.xpath("//*[@class=\"md-select-menu-container md-active md-clickable\"]//*[contains(text(),'" + product_testdata.firstUOM + "')]")).click();
            browser.sleep(200);
            browser.driver.findElement(by.xpath("//*[@aria-label='Create Product']")).click();
            browser.sleep(200);
            page1.form.numberOfUnits.sendKeys(product_testdata.numberOfUnits);
            page1.form.mrp.sendKeys(product_testdata.MRP);
            page1.form.size.sendKeys(product_testdata.size);
            browser.driver.findElement(by.xpath("//*[@aria-label='Create Product']")).click();
            browser.sleep(200);
            browser.driver.findElement(by.xpath("//*[@aria-label='Create Product']")).click();
            browser.sleep(200);
            expect(element(by.xpath("//*[@aria-label='Second Unit Measure']")).isDisplayed()).toBe(true);
            browser.sleep(200);
            browser.driver.findElement(by.xpath("//*[@class=\"fa fa-times fa-lg ng-scope material-icons\"]")).click();
            browser.sleep(500);
        });

         //9.TEST CASE FOR VALIDATION OF NUMBER OF UNITS WHILE CREATING NEW UOM
        it('should login user, validate number of units while creating new uom', function () {
            product.initiateCreateProduct();
            browser.sleep(200);
            element(by.xpath("//*[@aria-label='Category']")).click();
            browser.sleep(200);
            element(by.xpath("//*[contains(text(),'" + product_testdata.category + "')]")).click();
            browser.sleep(200);
            element(by.xpath("//*[@aria-label='Product']")).click();
            browser.sleep(200);
            browser.driver.findElement(by.xpath("//*[contains(text(),'" + product_testdata.productName + "')]")).click();
            browser.sleep(200);
            element(by.xpath("//*[@aria-label='Brand']")).click();
            browser.sleep(200);
            element(by.xpath("//*[@class=\"md-select-menu-container md-active md-clickable\"]//*[contains(text(),'" + product_testdata.brandName + "')]")).click();
            browser.sleep(200);
            element(by.xpath("//div[@id=\"inventory-0\"]//*[@aria-label=\"Unit Measure\"]")).click();
            browser.sleep(500);
            element(by.xpath("//*[@class=\"md-select-menu-container md-active md-clickable\"]//*[contains(text(),'" + product_testdata.addUOM + "')]")).click();
            browser.sleep(200);
            element(by.xpath("//*[@aria-label='First Unit Measure']")).click();
            browser.sleep(1000);
            browser.driver.findElement(by.xpath("//*[@class=\"md-select-menu-container md-active md-clickable\"]//*[contains(text(),'" + product_testdata.firstUOM + "')]")).click();
            browser.sleep(200);
            browser.driver.findElement(by.xpath("//*[@aria-label='Create Product']")).click();
            browser.sleep(200);
            browser.driver.findElement(by.xpath("//*[@aria-label='Create Product']")).click();
            browser.sleep(200);
            page1.form.mrp.sendKeys(product_testdata.MRP);
            page1.form.size.sendKeys(product_testdata.size);
            element(by.xpath("//*[@aria-label='Second Unit Measure']")).click();
            browser.sleep(1000);
            browser.driver.findElement(by.xpath("//*[@class=\"md-select-menu-container md-active md-clickable\"]//*[contains(text(),'" + product_testdata.secondUOM + "')]")).click();
            browser.sleep(200);
            browser.driver.findElement(by.xpath("//*[@aria-label='Create Product']")).click();
            browser.sleep(200);
           /* expect(page1.form.numberOfUnits.getAttribute('name')).toEqual(browser.driver.switchTo().activeElement().getAttribute('name'));*/
            browser.sleep(200);
            browser.driver.findElement(by.xpath("//*[@class=\"fa fa-times fa-lg ng-scope material-icons\"]")).click();
            browser.sleep(500);
        });

         //10.TEST CASE FOR VALIDATION OF MRP WHILE CREATING NEW UOM
        it('should login user, validation of mrp while creating new uom "/"', function () {

           product.initiateCreateProduct();
           browser.sleep(200);

            element(by.xpath("//*[@aria-label='Category']")).click();
            browser.sleep(200);
            element(by.xpath("//*[contains(text(),'" + product_testdata.category + "')]")).click();
            browser.sleep(200);
            element(by.xpath("//*[@aria-label='Product']")).click();
            browser.sleep(200);
            browser.driver.findElement(by.xpath("//*[contains(text(),'" + product_testdata.productName + "')]")).click();
            browser.sleep(200);
            element(by.xpath("//*[@aria-label='Brand']")).click();
            browser.sleep(200);
            element(by.xpath("//*[@class=\"md-select-menu-container md-active md-clickable\"]//*[contains(text(),'" + product_testdata.brandName + "')]")).click();
            browser.sleep(200);
            element(by.xpath("//div[@id=\"inventory-0\"]//*[@aria-label=\"Unit Measure\"]")).click();
            browser.sleep(500);
            element(by.xpath("//*[@class=\"md-select-menu-container md-active md-clickable\"]//*[contains(text(),'" + product_testdata.addUOM + "')]")).click();
            browser.sleep(200);
            element(by.xpath("//*[@aria-label='First Unit Measure']")).click();
            browser.sleep(200);
            browser.driver.findElement(by.xpath("//*[@class=\"md-select-menu-container md-active md-clickable\"]//*[contains(text(),'" + product_testdata.firstUOM + "')]")).click();
            browser.sleep(200);
            browser.driver.findElement(by.xpath("//*[@aria-label='Create Product']")).click();
            browser.sleep(200);
            browser.driver.findElement(by.xpath("//*[@aria-label='Create Product']")).click();
            browser.sleep(200);
            page1.form.size.sendKeys(product_testdata.size);
            element(by.xpath("//*[@aria-label='Second Unit Measure']")).click();
            browser.sleep(200);
            browser.driver.findElement(by.xpath("//*[@class=\"md-select-menu-container md-active md-clickable\"]//*[contains(text(),'" + product_testdata.secondUOM + "')]")).click();
            browser.sleep(200);
            page1.form.numberOfUnits.sendKeys(product_testdata.numberOfUnits);
            page1.form.size.sendKeys(product_testdata.size);
            browser.driver.findElement(by.xpath("//*[@aria-label='Create Product']")).click();
            browser.sleep(200);
            expect(page1.form.mrp.getAttribute('name')).toEqual(browser.driver.switchTo().activeElement().getAttribute('name'));
            browser.sleep(200);
            browser.driver.findElement(by.xpath("//*[@class=\"fa fa-times fa-lg ng-scope material-icons\"]")).click();
            browser.sleep(500);
        });

         //11.TEST CASE TO CHECK SHOW MORE DETAILS DISPLAYED OR NOT
           it('should login user, check whether more details link is working or not', function () {

               product.initiateCreateProduct();
               browser.sleep(200);

                element(by.xpath("//*[@aria-label='Category']")).click();
                element(by.xpath("//*[contains(text(),'" + product_testdata.category + "')]")).click();
                browser.sleep(200);
                element(by.xpath("//*[@aria-label='Product']")).click();
                browser.sleep(200);
                browser.driver.findElement(by.xpath("//*[contains(text(),'" + product_testdata.productName + "')]")).click();
                browser.sleep(200);
                element(by.xpath("//*[@aria-label='Brand']")).click();
                browser.sleep(200);
                element(by.xpath("//*[@class=\"md-select-menu-container md-active md-clickable\"]//*[contains(text(),'" + product_testdata.brandName + "')]")).click();
                browser.sleep(200);
                element(by.xpath("//*[contains(text(),'" + product_testdata.showMore + "')]")).click();
                browser.sleep(200);
                expect(element(by.xpath("//input[@name=\"batchNo\"]")).isDisplayed()).toBe(true);
                browser.sleep(200);
                page1.form.batchNumber.sendKeys(product_testdata.batchNumber);
                browser.sleep(200);
                browser.driver.findElement(by.xpath("//*[@class=\"fa fa-times fa-lg ng-scope material-icons\"]")).click();
                browser.sleep(500);
            });

         //12.TEST CASE TO CHECK SHOW LESS DETAILS LABEL IS WORKING OR NOT
        it('should login user, check whether less details link is working or not', function () {
            product.initiateCreateProduct();
            browser.sleep(200);
            element(by.xpath("//*[@aria-label='Category']")).click();
            element(by.xpath("//*[contains(text(),'" + product_testdata.category + "')]")).click();
            browser.sleep(200);
            element(by.xpath("//*[@aria-label='Product']")).click();
            browser.sleep(200);
            browser.driver.findElement(by.xpath("//*[contains(text(),'" + product_testdata.productName + "')]")).click();
            browser.sleep(200);
            element(by.xpath("//*[@aria-label='Brand']")).click();
            browser.sleep(200);
            element(by.xpath("//*[@class=\"md-select-menu-container md-active md-clickable\"]//*[contains(text(),'" + product_testdata.brandName + "')]")).click();
            browser.sleep(200);
            element(by.xpath("//*[contains(text(),'" + product_testdata.showMore + "')]")).click();
            browser.sleep(200);
            expect(element(by.xpath("//input[@name=\"batchNo\"]")).isDisplayed()).toBe(true);
            browser.sleep(200);
            page1.form.batchNumber.sendKeys(product_testdata.batchNumber);
            browser.sleep(200);
            element(by.xpath("//*[contains(text(),'" + product_testdata.showLess + "')]")).click();
            browser.sleep(200);
            expect(element(by.xpath("//input[@name=\"batchNo\"]")).isDisplayed()).toBe(false);
            browser.sleep(200);
            browser.driver.findElement(by.xpath("//*[@class=\"fa fa-times fa-lg ng-scope material-icons\"]")).click();
            browser.sleep(500);
        });


        //13.TEST CASE FOR ADD ONE MORE INVENTORY WHILE CREATING INVENTORY
              it('should login user,add one more inventory item while creating inventory', function () {

                  product.initiateCreateProduct();
                  browser.sleep(200);

                element(by.xpath("//*[@aria-label='Category']")).click();
                element(by.xpath("//*[contains(text(),'" + product_testdata.category + "')]")).click();
                browser.sleep(200);
                element(by.xpath("//*[@aria-label='Product']")).click();
                browser.sleep(200);
                browser.driver.findElement(by.xpath("//*[contains(text(),'" + product_testdata.productName + "')]")).click();
                browser.sleep(200);
                element(by.xpath("//*[@aria-label='Brand']")).click();
                browser.sleep(200);
                element(by.xpath("//*[@class=\"md-select-menu-container md-active md-clickable\"]//*[contains(text(),'" + product_testdata.brandName + "')]")).click();
                browser.sleep(200);
                element(by.xpath("//*[@id=\"inventory-0\"]/div/div[1]/md-icon")).click();
                browser.sleep(200);
                element(by.xpath("//div[@id=\"inventory-1\"]//*[@aria-label=\"Unit Measure\"]")).click();
                browser.sleep(200);
                element(by.xpath("//*[@class=\"md-select-menu-container md-active md-clickable\"]//*[contains(text(),'" + product_testdata.uom + "')]")).click();
                browser.sleep(200);
                element(by.xpath("//div[@id=\"inventory-1\"]//*[@ng-model='inventory.numberOfUnits']")).sendKeys("7984795");
                element(by.xpath("//div[@id=\"inventory-1\"]//*[@ng-model='inventory.MRP']")).sendKeys("6678478");
                browser.driver.findElement(by.xpath("//*[@aria-label='Create Product']")).click();
                browser.sleep(200);
                browser.driver.findElement(by.xpath("//*[@class=\"fa fa-times fa-lg ng-scope material-icons\"]")).click();
                browser.sleep(500);
            });

        it('should login user, validation of uom while adding more inventories"/"', function () {
            product.initiateCreateProduct();
            browser.sleep(200);

            element(by.xpath("//*[@aria-label='Category']")).click();
            element(by.xpath("//*[contains(text(),'" + product_testdata.category + "')]")).click();
            browser.sleep(200);
            element(by.xpath("//*[@aria-label='Product']")).click();
            browser.sleep(200);
            browser.driver.findElement(by.xpath("//*[contains(text(),'" + product_testdata.productName + "')]")).click();
            browser.sleep(200);
            element(by.xpath("//*[@aria-label='Brand']")).click();
            browser.sleep(200);
            element(by.xpath("//*[@class=\"md-select-menu-container md-active md-clickable\"]//*[contains(text(),'" + product_testdata.brandName + "')]")).click();
            browser.sleep(200);
            element(by.xpath("//*[@id=\"inventory-0\"]/div/div[1]/md-icon")).click();
            browser.sleep(200);
            element(by.xpath("//div[@id=\"inventory-0\"]//*[@aria-label=\"Unit Measure\"]")).click();
            browser.sleep(200);
            element(by.xpath("//*[@class=\"md-select-menu-container md-active md-clickable\"]//*[contains(text(),'" + product_testdata.uom + "')]")).click();
            browser.sleep(200);
            element(by.xpath("//div[@id=\"inventory-0\"]//*[@ng-model='inventory.numberOfUnits']")).sendKeys("7984795");
            element(by.xpath("//div[@id=\"inventory-0\"]//*[@ng-model='inventory.MRP']")).sendKeys("6678478");
            browser.driver.findElement(by.xpath("//*[@aria-label='Create Product']")).click();
            browser.sleep(200);
            browser.driver.findElement(by.xpath("//*[@aria-label='Create Product']")).click();
            browser.sleep(200);
            expect(element(by.xpath("//*[@id=\"inventory-1\"]//*[@class=\"md-input-message-animation ng-scope\"]")).isDisplayed()).toBe(true);
            browser.sleep(200);
            browser.driver.findElement(by.xpath("//*[@class=\"fa fa-times fa-lg ng-scope material-icons\"]")).click();
            browser.sleep(500);
        });

        //14.TEST CASE FOR VALIDATION OF NUMBER OF UNITS WHILE ADDING MORE INVENTORIES
        it('should login user,validation of number of units while adding more inventories', function () {
            product.initiateCreateProduct();
            browser.sleep(200);

            element(by.xpath("//*[@aria-label='Category']")).click();
            element(by.xpath("//*[contains(text(),'" + product_testdata.category + "')]")).click();
            browser.sleep(200);
            element(by.xpath("//*[@aria-label='Product']")).click();
            browser.sleep(200);
            browser.driver.findElement(by.xpath("//*[contains(text(),'" + product_testdata.productName + "')]")).click();
            browser.sleep(200);
            element(by.xpath("//*[@aria-label='Brand']")).click();
            browser.sleep(200);
            element(by.xpath("//*[@class=\"md-select-menu-container md-active md-clickable\"]//*[contains(text(),'" + product_testdata.brandName + "')]")).click();
            browser.sleep(200);
            //*[@id="inventory-0"]/div/div[1]/md-icon
            element(by.xpath('//*[@id=\'inventory-0\']/div/div[1]/md-icon')).click();
            browser.sleep(200);
            element(by.xpath("//div[@id=\"inventory-0\"]//*[@aria-label=\"Unit Measure\"]")).click();
            browser.sleep(200);
            element(by.xpath("//*[@class=\"md-select-menu-container md-active md-clickable\"]//*[contains(text(),'" + product_testdata.uom + "')]")).click();
            browser.sleep(200);
            element(by.xpath("//div[@id=\"inventory-0\"]//*[@ng-model='inventory.numberOfUnits']")).sendKeys("7984795");
            element(by.xpath("//div[@id=\"inventory-0\"]//*[@ng-model='inventory.MRP']")).sendKeys("6678478");
            element(by.xpath("//div[@id=\"inventory-1\"]//*[@aria-label=\"Unit Measure\"]")).click();
            browser.sleep(200);
            element(by.xpath("//*[@class=\"md-select-menu-container md-active md-clickable\"]//*[contains(text(),'" + product_testdata.secondUom + "')]")).click();
            browser.sleep(200);
            browser.driver.findElement(by.xpath("//*[@aria-label='Create Product']")).click();
            browser.sleep(200);
            expect(  element(by.xpath("//div[@id=\"inventory-1\"]//*[@ng-model='inventory.numberOfUnits']")).getAttribute('name')).toEqual(browser.driver.switchTo().activeElement().getAttribute('name'));
            browser.sleep(200);
            element(by.xpath("//*[@class=\"fa fa-times fa-lg ng-scope material-icons\"]")).click();
            browser.sleep(500);

        });

           //15.TEST CASE FOR VALIDATION OF MRP WHILE ADDING MORE INVENTORIES
            it('should login user, validation of mrp while adding more inventories', function () {
            product.initiateCreateProduct();
            browser.sleep(200);

            element(by.xpath("//*[@aria-label='Category']")).click();
            element(by.xpath("//*[contains(text(),'" + product_testdata.category + "')]")).click();
            browser.sleep(200);
            element(by.xpath("//*[@aria-label='Product']")).click();
            browser.sleep(200);
            browser.driver.findElement(by.xpath("//*[contains(text(),'" + product_testdata.productName + "')]")).click();
            browser.sleep(200);
            element(by.xpath("//*[@aria-label='Brand']")).click();
            browser.sleep(200);
            element(by.xpath("//*[@class=\"md-select-menu-container md-active md-clickable\"]//*[contains(text(),'" + product_testdata.brandName + "')]")).click();
            browser.sleep(200);
            //*[@id="inventory-0"]/div/div[1]/md-icon
            element(by.xpath('//*[@id=\'inventory-0\']/div/div[1]/md-icon')).click();
            browser.sleep(200);
            element(by.xpath("//div[@id=\"inventory-0\"]//*[@aria-label=\"Unit Measure\"]")).click();
            browser.sleep(200);
            element(by.xpath("//*[@class=\"md-select-menu-container md-active md-clickable\"]//*[contains(text(),'" + product_testdata.uom + "')]")).click();
            browser.sleep(200);
            element(by.xpath("//div[@id=\"inventory-0\"]//*[@ng-model='inventory.numberOfUnits']")).sendKeys("7984795");
            element(by.xpath("//div[@id=\"inventory-0\"]//*[@ng-model='inventory.MRP']")).sendKeys("6678478");
            element(by.xpath("//div[@id=\"inventory-1\"]//*[@aria-label=\"Unit Measure\"]")).click();
            browser.sleep(200);
            element(by.xpath("//*[@class=\"md-select-menu-container md-active md-clickable\"]//*[contains(text(),'" + product_testdata.secondUom + "')]")).click();
            browser.sleep(200);
            browser.driver.findElement(by.xpath("//*[@aria-label='Create Product']")).click();
            browser.sleep(200);
            element(by.xpath("//*[@class=\"fa fa-times fa-lg ng-scope material-icons\"]")).click();
            browser.sleep(500);
        });

       //16.TEST CASE FOR VALIDATION FOR ADD INVENTORIES WITH SAME UOM
        it('should login user, validation for add inventories with same uom', function () {
            product.initiateCreateProduct();
            browser.sleep(200);

            element(by.xpath("//*[@aria-label='Category']")).click();
            element(by.xpath("//*[contains(text(),'" + product_testdata.category + "')]")).click();
            browser.sleep(200);
            element(by.xpath("//*[@aria-label='Product']")).click();
            browser.sleep(200);
            browser.driver.findElement(by.xpath("//*[contains(text(),'" + product_testdata.productName + "')]")).click();
            browser.sleep(200);
            element(by.xpath("//*[@aria-label='Brand']")).click();
            browser.sleep(200);
            element(by.xpath("//*[@class=\"md-select-menu-container md-active md-clickable\"]//*[contains(text(),'" + product_testdata.brandName + "')]")).click();
            browser.sleep(200);
            element(by.xpath("//div[@id=\"inventory-0\"]//*[@aria-label=\"Unit Measure\"]")).click();
            browser.sleep(200);
            element(by.xpath("//*[@class=\"md-select-menu-container md-active md-clickable\"]//*[contains(text(),'" + product_testdata.uom + "')]")).click();
            browser.sleep(200);
            element(by.xpath("//div[@id=\"inventory-0\"]//*[@ng-model='inventory.numberOfUnits']")).sendKeys("7984795");
            element(by.xpath("//div[@id=\"inventory-0\"]//*[@ng-model='inventory.MRP']")).sendKeys("6678478");
            element(by.xpath("//*[@id=\"inventory-0\"]")).click();
            browser.sleep(200);
            browser.driver.findElement(by.xpath("//*[@aria-label='Create Product']")).click();
            browser.sleep(200);
            // element(by.xpath("//*[@class=\"fa fa-times fa-lg ng-scope material-icons\"]")).click();
            // browser.sleep(500);
            product.deleteAllImported();

        });

          //17.TEST CASE FOR CREATE INVENTORY WITH MORE INVENTORIES
            it('should login user,create more inventories', function () {

            product.initiateCreateProduct();
            browser.sleep(200);

            element(by.xpath("//*[@aria-label='Category']")).click();
            browser.sleep(200);
            element(by.xpath("//*[contains(text(),'" + product_testdata.category + "')]")).click();
            browser.sleep(200);
            element(by.xpath("//*[@aria-label='Product']")).click();
            browser.sleep(200);
            browser.driver.findElement(by.xpath("//*[contains(text(),'" + product_testdata.productName + "')]")).click();
            browser.sleep(200);
            element(by.xpath("//*[@aria-label='Brand']")).click();
            browser.sleep(200);
            element(by.xpath("//*[@class=\"md-select-menu-container md-active md-clickable\"]//*[contains(text(),'" + product_testdata.brandName + "')]")).click();
            browser.sleep(200);
            element(by.xpath("//div[@id=\"inventory-0\"]//*[@aria-label=\"Unit Measure\"]")).click();
            browser.sleep(200);
            element(by.xpath("//*[@class=\"md-select-menu-container md-active md-clickable\"]//*[contains(text(),'" + product_testdata.uom + "')]")).click();
            browser.sleep(200);
            page1.form.numberOfUnits.sendKeys(product_testdata.numberOfUnits);
            page1.form.mrp.sendKeys(product_testdata.MRP);
            element(by.xpath("//*[@class=\"md-primary fa fa-plus-circle fa-2x ng-scope material-icons\"]")).click();
            browser.sleep(200);
            element(by.xpath("//div[@id=\"inventory-1\"]//*[@aria-label=\"Unit Measure\"]")).click();
            browser.sleep(200);
            element(by.xpath("//*[@class=\"md-select-menu-container md-active md-clickable\"]//*[contains(text(),'" + product_testdata.secondUom + "')]")).click();
            browser.sleep(200);
            element(by.xpath("//div[@id=\"inventory-1\"]//*[@ng-model='inventory.numberOfUnits']")).sendKeys("7984795");
            element(by.xpath("//div[@id=\"inventory-1\"]//*[@ng-model='inventory.MRP']")).sendKeys("6678478");
            browser.driver.findElement(by.xpath("//*[@aria-label='Create Product']")).click();
            browser.sleep(200);
            inventory = browser.driver.findElement(by.xpath("//td[.//h5[contains(text(),'" + product_testdata.inventoryName + " - " + product_testdata.uom + "')] and .//h6[contains(text(),'" + product_testdata.brandName + "')]]"));
            browser.sleep(200);
            product.deleteAllImported();
            });

        //18.TEST CASE FOR CREATE INVENTORY WITH MORE INVENTORIES AND MORE DETAILS
        it('should login user, check for create inventory with more details "/"', function () {
            product.initiateCreateProduct();
            browser.sleep(200);

            element(by.xpath("//*[@aria-label='Category']")).click();
            browser.sleep(200);
            element(by.xpath("//*[contains(text(),'" + product_testdata.category + "')]")).click();
            browser.sleep(200);
            element(by.xpath("//*[@aria-label='Product']")).click();
            browser.sleep(200);
            browser.driver.findElement(by.xpath("//*[contains(text(),'" + product_testdata.productName + "')]")).click();
            browser.sleep(200);
            element(by.xpath("//*[@aria-label='Brand']")).click();
            browser.sleep(200);
            element(by.xpath("//*[@class=\"md-select-menu-container md-active md-clickable\"]//*[contains(text(),'" + product_testdata.brandName + "')]")).click();
            browser.sleep(200);
            element(by.xpath("//div[@id=\"inventory-0\"]//*[@aria-label=\"Unit Measure\"]")).click();
            browser.sleep(200);
            element(by.xpath("//*[@class=\"md-select-menu-container md-active md-clickable\"]//*[contains(text(),'" + product_testdata.uom + "')]")).click();
            browser.sleep(200);
            page1.form.numberOfUnits.sendKeys(product_testdata.numberOfUnits);
            page1.form.mrp.sendKeys(product_testdata.MRP);
            element(by.xpath("//*[contains(text(),'" + product_testdata.showMore + "')]")).click();
            browser.sleep(200);
            page1.form.batchNumber.sendKeys(product_testdata.batchNumber);
            browser.driver.findElement(by.xpath("//*[@aria-label='Create Product']")).click();
            browser.sleep(200);
            inventory = browser.driver.findElement(by.xpath("//td[.//h5[contains(text(),'" + product_testdata.inventoryName + " - " + product_testdata.uom + "')] and .//h6[contains(text(),'" + product_testdata.brandName + "')]]"));
            browser.sleep(200);
            product.deleteAllImported();
            });
      });


    });

