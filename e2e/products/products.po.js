











'use strict';

var product = function() {
    var form = this.form = element(by.name('productForm'));
    form.productname = form.element(by.xpath("//*[@aria-label='Product Name']"));
    form.brandname = form.element(by.model('selectedItem.brandName'));
    form.samplenum = form.element(by.model('selectedItem.sampleNumber'));
    form.fssainum = form.element(by.model('selectedItem.fssaiLicenseNumber'));
    form.fssainumproof = form.element(by.model('selectedItem.fssaiLicenseNumberProof'));
    form.description = form.element(by.model('selectedItem.description'));
    form.gradeKey = form.element(by.model('grade.attributeKey'));
    form.gradeValue = form.element(by.model('grade.attributeValue'));
    form.qualityKey = form.element(by.model('quality.attributeKey'));
    form.qualityValue = form.element(by.model('quality.attributeValue'));
    form.unitSize = form.element(by.model('inventory.unitSize'));
    form.unitMeasure = form.element(by.model('inventory.unitMeasure'));
    form.mrp = form.element(by.model('inventory.MRP'));
    form.size = form.element(by.model('inventory.uom.conversion'));
    form.numberOfUnits = form.element(by.model('inventory.numberOfUnits'));
    form.batchNumber=form.element(by.xpath('//input[@name="batchNo"]'));
    form.maxUnits = form.element(by.model('inventory.maxUnits'));
    form.cgst = form.element(by.model('inventory.tax.CGST'));
    form.sgst = form.element(by.model('inventory.tax.SGST'));
    form.igst = form.element(by.model('inventory.tax.IGST'));
    form.moq = form.element(by.model('salePrice.MOQ'));
    form.margin = form.element(by.model('salePrice.margin'));
    form.moq1 = form.element(by.model('buyPrice.MOQ'));
    form.margin1 = form.element(by.model('buyPrice.margin'));
    form.submit  = form.element(by.css('.md-button'));
    this.login = function(data) {
        for (var prop in data) {
            var formElem1 = form[prop];
            if (data.hasOwnProperty(prop) && formElem1 && typeof formElem1.sendKeys === 'function') {
                formElem1.sendKeys(data[prop]);
            }
        }
    };
};

module.exports = new product();

