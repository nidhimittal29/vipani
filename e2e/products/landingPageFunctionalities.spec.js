


'use strict';

var config = browser.params;


describe('LandingPage asa View', function() {
    var page;

    var loadPage = function () {
        browser.manage().deleteAllCookies();
        browser.get('http://localhost:3000/#!/signin');
    };

    var testUser = {
        username: 'ab@gmail.com',
        password: '12345678'
    };



    describe('with local auth', function() {
        beforeEach(function () {
            loadPage();

        });
//TEST CASE FOR CHECKING THE DISPLAY OF HOME LINK IN THE LANDING PAGE
        it('should login user, Checking Functionalities in Landing Page and redirecting to "/"', function () {
            page = require('./login.po');
            page.login(testUser);
            page.form.submit.click();
            browser.sleep(2000);
            expect(browser.findElement(by.linkText("Home")).isDisplayed()).toBe(true);
            expect(browser.driver.findElement(by.linkText("Home")).getText()).toBe("Home");
            browser.driver.findElement(by.xpath("//*[@aria-label='Logout']")).click();
            browser.sleep(2000);
            expect(browser.getCurrentUrl()).toEqual('http://localhost:3000/#!/');
        });
//TEST CASE FOR CHECKING THE DISPLAY OF PRODUCTS LINK IN THE LANDING PAGE
        it('should login user, Checking Functionalities in Landing Page and redirecting to "/"', function () {
            page = require('./login.po');
            page.login(testUser);
            page.form.submit.click();
            browser.sleep(2000);
            expect(browser.findElement(by.linkText("Products")).isDisplayed()).toBe(true);
            expect(browser.driver.findElement(by.linkText("Products")).getText()).toBe("Products");
            browser.driver.findElement(by.xpath("//*[@aria-label='Logout']")).click();
            browser.sleep(2000);
            expect(browser.getCurrentUrl()).toEqual('http://localhost:3000/#!/');
        });
//TEST CASE FOR CHECKING THE DISPLAY OF CONTACTS LINK IN THE LANDING PAGE
        it('should login user, Checking Functionalities in Landing Page and redirecting to "/"', function () {
            page = require('./login.po');
            page.login(testUser);
            page.form.submit.click();
            browser.sleep(2000);
            expect(browser.findElement(by.linkText("Contacts")).isDisplayed()).toBe(true);
            expect(browser.driver.findElement(by.linkText("Contacts")).getText()).toBe("Contacts");
            browser.driver.findElement(by.xpath("//*[@aria-label='Logout']")).click();
            browser.sleep(2000);
            expect(browser.getCurrentUrl()).toEqual('http://localhost:3000/#!/');
        });
//TEST CASE FOR CHECKING THE DISPLAY OF OFFERS LINK IN THE LANDING PAGE
        it('should login user, Checking Functionalities in Landing Page and redirecting to "/"', function () {
            page = require('./login.po');
            page.login(testUser);
            page.form.submit.click();
            browser.sleep(2000);
            expect(browser.findElement(by.linkText("Offers")).isDisplayed()).toBe(true);
            expect(browser.driver.findElement(by.linkText("Offers")).getText()).toBe("Offers");
            browser.driver.findElement(by.xpath("//*[@aria-label='Logout']")).click();
            browser.sleep(2000);
            expect(browser.getCurrentUrl()).toEqual('http://localhost:3000/#!/');
        });
//TEST CASE FOR CHECKING THE DISPLAY OF ORDERS LINK IN THE LANDING PAGE
        it('should login user, Checking Functionalities in Landing Page and redirecting to "/"', function () {
            page = require('./login.po');
            page.login(testUser);
            page.form.submit.click();
            browser.sleep(2000);
            expect(browser.findElement(by.linkText("Orders")).isDisplayed()).toBe(true);
            expect(browser.driver.findElement(by.linkText("Orders")).getText()).toBe("Orders");
            browser.driver.findElement(by.xpath("//*[@aria-label='Logout']")).click();
            browser.sleep(2000);
            expect(browser.getCurrentUrl()).toEqual('http://localhost:3000/#!/');
        });        
//TEST CASE FOR CHECKING THE DISPLAY OF REPORTS LINK IN THE LANDING PAGE
        it('should login user, Checking Functionalities in Landing Page and redirecting to "/"', function () {
            page = require('./login.po');
            page.login(testUser);
            page.form.submit.click();
            browser.sleep(2000);
            expect(browser.findElement(by.linkText("Reports")).isDisplayed()).toBe(true);
            expect(browser.driver.findElement(by.linkText("Reports")).getText()).toBe("Reports");
            browser.driver.findElement(by.xpath("//*[@aria-label='Logout']")).click();
            browser.sleep(2000);
            expect(browser.getCurrentUrl()).toEqual('http://localhost:3000/#!/');
        });
//TEST CASE FOR CHECKING CLICK EVENT OF MENU PRESENT IN THE HEADER
        it('should login user, Checking Functionalities in Landing Page and redirecting to "/"', function () {
            page = require('./login.po');
            page.login(testUser);
            page.form.submit.click();
            browser.sleep(2000);
            expect(browser.findElement(by.xpath("//*[@aria-label='nav']")).isDisplayed()).toBe(true);
            browser.findElement(by.xpath("//*[@aria-label='nav']")).click();
            browser.sleep(2000);
            browser.findElement(by.css(".bb-content")).getCssValue('marginLeft').then(function (marginLeft) {
                expect(marginLeft).toBe('0px');
            });
            browser.findElement(by.xpath("//*[@aria-label='nav']")).click();
            browser.sleep(2000);
            browser.findElement(by.css(".bb-content")).getCssValue('marginLeft').then(function (marginLeft) {
                expect(marginLeft).toBe('150px');
            });
            expect(browser.findElement(by.linkText("Home")).isDisplayed()).toBe(true);
            expect(browser.findElement(by.linkText("Products")).isDisplayed()).toBe(true);
            expect(browser.findElement(by.linkText("Offers")).isDisplayed()).toBe(true);
            expect(browser.findElement(by.linkText("Orders")).isDisplayed()).toBe(true);
            expect(browser.findElement(by.linkText("Reports")).isDisplayed()).toBe(true);
            browser.driver.findElement(by.xpath("//*[@aria-label='Logout']")).click();
            browser.sleep(2000);
            expect(browser.getCurrentUrl()).toEqual('http://localhost:3000/#!/');
        });
        it('should login user, Checking Functionalities in Landing Page and redirecting to "/"', function () {
            page = require('./login.po');
            page.login(testUser);
            page.form.submit.click();
            browser.sleep(2000);
            expect(browser.findElement(by.xpath("//*[@aria-label='nav']")).isDisplayed()).toBe(true);
            browser.findElement(by.css(".bb-content")).getCssValue('marginLeft').then(function (marginLeft) {
                 expect(marginLeft).toBe('150px');
            });
            browser.driver.findElement(by.xpath("//*[@aria-label='Logout']")).click();
            browser.sleep(2000);
            expect(browser.getCurrentUrl()).toEqual('http://localhost:3000/#!/');
        });
        
//TEST CASE FOR CHECKING WHETHER WE ARE GETTING PRODUCTS PAGE BY CLICKING THE PRODUCTS LINK
        it('should login user, Checking Functionalities in Landing Page and redirecting to "/"', function () {
            page = require('./login.po');
            page.login(testUser);
            page.form.submit.click();
            browser.sleep(2000);
            browser.findElement(by.linkText("Products")).click();
            expect(browser.getCurrentUrl()).toEqual('http://localhost:3000/#!/products');
            browser.driver.findElement(by.xpath("//*[@aria-label='Logout']")).click();
            browser.sleep(2000);
            expect(browser.getCurrentUrl()).toEqual('http://localhost:3000/#!/');
        });

/*//TEST CASE FOR CHECKING WHETHER WE ARE GETTING CONTACTS PAGE BY CLICKING THE CONTACTS LINK
        it('should login user, Checking Functionalities in Landing Page and redirecting to "/"', function () {
            page = require('./login.po');
            page.login(testUser);
            page.form.submit.click();
            browser.sleep(2000);
            browser.findElement(by.linkText("Contacts")).click();
            expect(browser.getCurrentUrl()).toEqual('http://localhost:3000/#!/contacts');
            browser.sleep(1500);
            browser.findElement(by.xpath("html/body/div[1]/section/div/section/md-content/md-card/div[1]/button")).click();
            browser.driver.findElement(by.xpath("//!*[@aria-label='Logout']")).click();
            browser.sleep(2000);
            expect(browser.getCurrentUrl()).toEqual('http://localhost:3000/#!/');
        });*/
//TEST CASE FOR CHECKING WHETHER WE ARE GETTING OFFERS PAGE BY CLICKING THE OFFERS LINK
        it('should login user, Checking Functionalities in Landing Page and redirecting to "/"', function () {
            page = require('./login.po');
            page.login(testUser);
            page.form.submit.click();
            browser.sleep(2000);
            browser.findElement(by.linkText("Offers")).click();
            expect(browser.getCurrentUrl()).toEqual('http://localhost:3000/#!/offers');
            browser.driver.findElement(by.xpath("//*[@aria-label='Logout']")).click();
            browser.sleep(2000);
            expect(browser.getCurrentUrl()).toEqual('http://localhost:3000/#!/');
        });
//TEST CASE FOR CHECKING WHETHER WE ARE GETTING ORDERS PAGE BY CLICKING THE ORDERS LINK
        it('should login user, Checking Functionalities in Landing Page and redirecting to "/"', function () {
            page = require('./login.po');
            page.login(testUser);
            page.form.submit.click();
            browser.sleep(2000);
            browser.findElement(by.linkText("Orders")).click();
            expect(browser.getCurrentUrl()).toEqual('http://localhost:3000/#!/orders');
            browser.driver.findElement(by.xpath("//*[@aria-label='Logout']")).click();
            browser.sleep(2000);
            expect(browser.getCurrentUrl()).toEqual('http://localhost:3000/#!/');
        });
//TEST CASE FOR CHECKING WHETHER WE ARE GETTING REPORTS PAGE BY CLICKING THE REPORTS LINK
        it('should login user, Checking Functionalities in Landing Page and redirecting to "/"', function () {
            page = require('./login.po');
            page.login(testUser);
            page.form.submit.click();
            browser.sleep(2000);
            browser.findElement(by.linkText("Reports")).click();
            expect(browser.getCurrentUrl()).toEqual('http://localhost:3000/#!/reports');
            browser.driver.findElement(by.xpath("//*[@aria-label='Logout']")).click();
            browser.sleep(2000);
            expect(browser.getCurrentUrl()).toEqual('http://localhost:3000/#!/');
        });
//TEST CASE FOR CORRECT POSITION OF ICONS IN THE PAGE
        it('should login user, Checking Functionalities in Landing Page and redirecting to "/"', function () {
            page = require('./login.po');
            page.login(testUser);
            page.form.submit.click();
            browser.sleep(2000);
            
            browser.driver.findElement(by.xpath("//*[@aria-label='Logout']")).click();
            browser.sleep(2000);
            expect(browser.getCurrentUrl()).toEqual('http://localhost:3000/#!/');
        });
    });

});

