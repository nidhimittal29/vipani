


'use strict';
var config = browser.params;
var user=require('../../account/common.util.e2e/signin.common.po');
var product=require('../../account/common.util.e2e/product.common.po');
var loginTestData = require('./login');
var global = require('../../account/common.util.e2e/global.common.po');
describe('LandingPage View', function() {
    var loadPage = function () {
        browser.manage().deleteAllCookies();
        browser.get('http://localhost:3000/#!/signin');
    };

    describe('with local auth', function () {
        beforeAll(function () {
            global.loadPage('http://localhost:3000/#!/register');
            user.registerProcess(loginTestData[0]);
            global.loadPage('http://localhost:3000/#!/register');
            user.registerProcess(loginTestData[1]);
            global.loadPage('http://localhost:3000/#!/register');
            user.registerProcess(loginTestData[2]);
        });
        beforeEach(function () {
            loadPage();
        });
        afterEach(function () {
            user.logout();
            browser.sleep(1000);
        });


        //
        // it('should show errors if header is invalid', function () {
        //     user.signin(loginTestData[0]);
        //     user.getForm().submit.click();
        //     browser.sleep(1000);
        //     product.fileImportProducts('../../Products/importtestdata/ImportWithErrors.csv');
        //     //select all categories
        //     //create-file-import/div/div[1]/div[5]/div[1]/h5[2]/span
        //     expect(element(by.xpath('//create-file-import/div/div[1]/div[5]/div[1]/h5[2]/span')).getText()).
        //     toBe('All mandatory columns required for importing the CSV file is not present. Following fields are missing or are not in the correct position:Category,SubCategory1,ProductName,ProductCode,BrandName,HSNCode,UOM.FirstUOM,UOM.Conversion,UOM.SecondUOM,The mandatory columns are : Category, SubCategory1, ProductName, ProductCode, BrandName, HSNCode, TaxGroup, UOM.FirstUOM, UOM.Conversion and UOM.SecondUOM');;
        //     element(by.css('[aria-label=\'close dialog\']')).click();
        //
        // });
            it('should show error if hsn code is invalid', function () {
                user.signin(loginTestData[0]);
                user.getForm().submit.click();
                browser.sleep(1000);
                product.fileImportProducts('../../products/importtestdata/invalidhsncodes.xlsx');

                element(by.xpath('//md-checkbox[@aria-label=\'Coffee\']')).click();
                element(by.xpath('//md-checkbox[@aria-label=\'Cereals\']')).click();
                browser.sleep(1000);
                element(by.css('button[aria-label=\'Proceed to upload file\']')).click();
                browser.sleep(200);
                //create-file-import/div/div[1]/div[7]/div[1]/div[2]/table/tbody/tr[1]/td[2]/p
                expect(element(by.xpath('//create-file-import/*//table/tbody/tr[1]/td[2]/p')).
                getText()).toBe('No HSN Code with the number :912011149');
                expect(element(by.xpath('//create-file-import/*//table/tbody/tr[2]/td[2]/p')).
                getText()).toBe('No HSN Code with the number :1002363010');

                element(by.css('[aria-label=\'close dialog\']')).click();
            });
        //
            it('should show errors if categories are invalid', function () {
                user.signin(loginTestData[0]);
                user.getForm().submit.click();
                browser.sleep(1000);

                product.fileImportProducts('../../products/importtestdata/ImportWithDataErrors.csv');
                //select all categories
                browser.sleep(2000);
                expect(element(by.xpath('//create-file-import/div/div[1]/div[5]/div[1]/h5[2]/span')).getText()).
                toBe('2 errors were found');
                element(by.css('[aria-label=\'close dialog\']')).click();
            });
            //


            it('should show errors if mandatory fields are blank',function(){
                user.signin(loginTestData[0]);
                user.getForm().submit.click();
                browser.sleep(1000);
                product.fileImportProducts('../../products/importtestdata/ImportWithDataErrors.csv');
                //select all categories
                //create-file-import/div/div[1]/div[5]/div[1]/h5[2]/span
                browser.sleep(2000);
                expect(element(by.xpath('//create-file-import/div/div[1]/div[5]/div[1]/table/tbody/tr[1]/td[2]/p')).
                getText()).toBe('Agriculture,Cereals,Sona Masoori,SONMR,Rice,Grain,Sona Masoori Rice,SuryaTeja,Premium,Produced By Surya Teja with Sataka Sortex Machine,Surya Teja Raw And Boiled Rice Industries Pvt Ltd,Surya Teja Raw And Boiled Rice Industries Pvt Ltd,9293949533,9293949544,suryateja444@gmail.com,Khammam Road, Yadarpally Village, Miryalaguda,Nalgonda,Telangana State,India,508207,Surya Teja Raw And Boiled Rice Industries Pvt Ltd,Murali Mohan,9293949533,9293949544,suryateja444@gmail.com,Khammam Road, Yadarpally Village, Miryalaguda,Nalgonda,Telangana State,India,508207,10063020,GST 5%,Bag,Column UOM.Conversion: The mandatory column UOM.Conversion value is empty');
                expect(element(by.xpath('//create-file-import/div/div[1]/div[5]/div[1]/table/tbody/tr[2]/td[2]/p')).
                getText()).toBe('Agriculture,Cereals,Basmati rice,BASMR,Rice,Grain,Premium quality Basmati Rice,Rasola Basmati Rice,Long-Grain Rice,Rasola Basmati Rice,Rohit Anand (Director),Rohit Anand (Director),8750290481,9773818274,info@rasolabasmatirice.com,B-83 Lajpat nagar-II,New Delhi,Delhi,India,,,,,,,,,,,,10063020,GST 5%,Pac,1,Column UOM.SecondUOM: The mandatory column UOM.SecondUOM value is empty');
                element(by.css('[aria-label=\'close dialog\']')).click();


            });

        //
        //
            it('should enable add items to inventory button only if products are there',function(){
                user.signin(loginTestData[0]);
                user.getForm().submit.click();
                browser.sleep(1000);

                product.fileImportProducts('../../products/importtestdata/ImportWithDataErrors.csv');
                //select all categories
                browser.sleep(2000);
                expect(element(by.xpath('//create-file-import/div/div[1]/div[5]/div[1]/h5[2]/span')).getText()).
                toBe('4 errors were found');
                expect(element(by.css('button[aria-label=\'Proceed to upload file\']')).getAttribute('disabled')).toBe('true');
                browser.sleep(500);
                element(by.css('[aria-label=\'close dialog\']')).click();
            });
           // it('should enable finish only in the last stage',function(){});


            it('should login user, Browse file path for import products from a file and verify all fields are set correctly for a csv file"/"', function () {
                user.signin(loginTestData[0]);
                user.getForm().submit.click();
                browser.sleep(1000);
                //First step: specify the file to import
                product.fileImportProducts('../../products/importtestdata/product-import-template-2-categories-two-products.csv');
                //select all categories
                //decide which category to click
                browser.sleep(2000);
                element(by.xpath('//md-checkbox[@aria-label=\'Coffee\']')).click();
                element(by.xpath('//md-checkbox[@aria-label=\'Cereals\']')).click();
                browser.sleep(1000);
                element(by.css('button[aria-label=\'Proceed to upload file\']')).click();
                browser.sleep(1000);
                //*[@id="all"]
                element(by.xpath('//create-file-import//nv-check-box')).click();
                browser.sleep(200);
                // browser.driver.executeScript("arguments[0].scrollIntoView(true);",
                //     element(by.css('button[aria-label="adding inventory"]')));
                element(by.css('button[aria-label=\'Proceed to upload file\']')).click();
                browser.sleep(500);
                element(by.css('button[aria-label=\'Proceed to upload file\']')).click();
                browser.sleep(500);
                expect(element.all(by.xpath('//view-products/table/tbody/tr')).count()).toBe(2);
                element(by.xpath('//view-products/table/tbody/tr[1]/td[3]')).click();
                browser.sleep(500);
                //specify the new values for the product imported to be validate against
                var valuesArray=[ 'Cereals',
                    'Surya Teja',
                    '1400',
                    'Sona Masoori',
                    '10063010',
                    '4000',
                    'Bag Of 25Kgs ( 25 )',
                    'GST 0%'
                ];
                var editProductForm = element(by.xpath('//form[@name=\'editProductForm\']'));
                product.validateProductEdit(editProductForm,valuesArray);
                browser.sleep(500);
                //check the batch values
                var batchValuesArray=[
                    ['CERSM-02032018-120214','1400','1000','1000','18:02:2017','20:02:2017'],
                    ['CERSM-02032018-120213','1400','2000','2000','18:02:2017','18:02:2017'],
                    ['CERSM-02032018-120212','1400','1000','1000','11:05:2017','13:05:2017']
                ];
                product.validateBatches(element(by.xpath('//md-content[@id=\'batches\']//md-card')),batchValuesArray);
                browser.sleep(500);
                //check the discounts values
                editProductForm.element(by.xpath('//*//md-tab-item[text()=\'Discounts\']')).click();
                browser.sleep(500);
                //var moqValuesArray=[{type:'MOQ',values:['100','5']},{type:'MOQ',values:['100','10']}];
                var moqValuesArray=[{type:'MOQ',values:['100','5']}];
                product.validateMOQDiscounts(element(by.xpath('//md-content[@id=\'discounts\']')),moqValuesArray);
                //Repeat for MOV, but if not provided,then validate it is blank
                product.validateBlankMOQDiscounts(element(by.xpath('//md-content[@id=\'discounts\']')),'MOV');
                editProductForm.element(by.css('[aria-label=\'close dialog\']')).click();
                product.deleteAllImported();
                browser.sleep(1000);

            });

            it('should login user, Browse file path for import products from a file and verify all fields are set correctly using xlsx"/"', function () {
                user.signin(loginTestData[0]);
                user.getForm().submit.click();
                browser.sleep(1000);
                product.fileImportProducts('../../products/importtestdata/product-import-template-2-categories-two-products.xlsx');
                //select all categories
                browser.sleep(2000);
                element(by.xpath('//md-checkbox[@aria-label=\'Select All Categories\']')).click();
              /*  element(by.xpath('//md-checkbox[@aria-label=\'Cereals\']')).click();*/
                browser.sleep(1000);
                element(by.css('button[aria-label=\'Proceed to upload file\']')).click();
                browser.sleep(200);
                element(by.xpath('//create-file-import//nv-check-box')).click();
                browser.sleep(200);
                // browser.driver.executeScript("arguments[0].scrollIntoView(true);",
                //     element(by.css('button[aria-label="adding inventory"]')));
                element(by.css('button[aria-label=\'Proceed to upload file\']')).click();
                browser.sleep(500);
                element(by.css('button[aria-label=\'Proceed to upload file\']')).click();
                browser.sleep(500);
                expect(element.all(by.xpath('//view-products/table/tbody/tr')).count()).toBe(2);
                element(by.xpath('//view-products/table/tbody/tr[1]/td[3]')).click();
                browser.sleep(500);
                var valuesArray=[ 'Cereals',
                    'Surya Teja',
                    '1400',
                    'Sona Masoori',
                    '10063010',
                    '4000',
                    'Bag Of 25Kgs ( 25 )',
                    'GST 0%'
                ];
                var editProductForm = element(by.xpath('//form[@name=\'editProductForm\']'));
                product.validateProductEdit(editProductForm,valuesArray);
                browser.sleep(500);
                var batchValuesArray=[
                    ['CERSM-02032018-120214','1400','1000','1000','18:02:2017','20:02:2017'],
                    ['CERSM-02032018-120213','1400','2000','2000','18:02:2017','18:02:2017'],
                    ['CERSM-02032018-120212','1400','1000','1000','11:05:2017','13:05:2017']
                ];
                product.validateBatches(element(by.xpath('//md-content[@id=\'batches\']//md-card')),batchValuesArray);
                browser.sleep(500);

                editProductForm.element(by.xpath('//*//md-tab-item[text()=\'Discounts\']')).click();
                browser.sleep(500);
                var moqValuesArray=[{type:'MOQ',values:['100','5']}];
                product.validateMOQDiscounts(element(by.xpath('//md-content[@id=\'discounts\']')),moqValuesArray);
                product.validateBlankMOQDiscounts(element(by.xpath('//md-content[@id=\'discounts\']')),'MOV');
                editProductForm.element(by.css('[aria-label=\'close dialog\']')).click();
                product.deleteAllImported();
                browser.sleep(1000);

            });

            it('is a test case for bb-735',function(){
                user.signin(loginTestData[1]);
                user.getForm().submit.click();
                browser.sleep(1000);

                product.fileImportProducts('../../products/importtestdata/new berries1.csv');
                browser.sleep(2000);
                //select all categories
                element(by.xpath('//md-checkbox[@aria-label=\'Coffee\']')).click();
                browser.sleep(1000);
                element(by.css('button[aria-label=\'Proceed to upload file\']')).click();
                browser.sleep(200);
                element(by.xpath('//create-file-import//nv-check-box')).click();
                browser.sleep(200);
                // browser.driver.executeScript("arguments[0].scrollIntoView(true);",
                //     element(by.css('button[aria-label="adding inventory"]')));
                element(by.css('button[aria-label=\'Proceed to upload file\']')).click();
                browser.sleep(500);
                element(by.css('button[aria-label=\'Proceed to upload file\']')).click();
                browser.sleep(500);
                expect(element.all(by.xpath('//view-products/table/tbody/tr')).count()).toBe(1);
                element(by.xpath('//view-products/table/tbody/tr[1]/td[3]')).click();
                browser.sleep(500);
                var valuesArray=[ 'Coffee',
                    'Grower',
                    '14325',
                    'Coffee Berries',
                    '09011149',
                    '0',
                    'Bag Of 75Kgs ( 75 )',
                    'GST 0%'
                ];
                var editProductForm = element(by.xpath('//form[@name=\'editProductForm\']'));
                product.validateProductEdit(editProductForm,valuesArray);
                browser.sleep(500);
                editProductForm.element(by.xpath('//*//md-tab-item[text()=\'Discounts\']')).click();
                browser.sleep(500);
                var moqValuesArray=[{type:'MOQ',values:['15','6']},{type:'MOQ',values:['100','4']}];
                product.validateMOQDiscounts(element(by.xpath('//md-content[@id=\'discounts\']')),moqValuesArray);
                var movValuesArray=[{type:'MOV',values:['50000','12']},{type:'MOV',values:['50000','5']}];
                product.validateMOQDiscounts(element(by.xpath('//md-content[@id=\'discounts\']')),movValuesArray);
                editProductForm.element(by.css('[aria-label=\'close dialog\']')).click();
                product.deleteAllImported();
                browser.sleep(1000);
            });

            it('is one more test case for bb-735',function(){
                user.signin(loginTestData[2]);
                user.getForm().submit.click();
                browser.sleep(1000);

                product.fileImportProducts('../../products/importtestdata/moqmovtest.csv');
                browser.sleep(2000);
                //select all categories
                element(by.xpath('//md-checkbox[@aria-label=\'Cereals\']')).click();
                browser.sleep(1000);
                element(by.css('button[aria-label=\'Proceed to upload file\']')).click();
                browser.sleep(200);
                element(by.xpath('//create-file-import//nv-check-box')).click();
                browser.sleep(200);
                // browser.driver.executeScript("arguments[0].scrollIntoView(true);",
                //     element(by.css('button[aria-label="adding inventory"]')));
                element(by.css('button[aria-label=\'Proceed to upload file\']')).click();
                browser.sleep(500);
                element(by.css('button[aria-label=\'Proceed to upload file\']')).click();
                browser.sleep(500);
                expect(element.all(by.xpath('//view-products/table/tbody/tr')).count()).toBe(2);
                element(by.xpath('//view-products/table/tbody/tr[1]/td[3]')).click();
                browser.sleep(500);
                var valuesArray=[ 'Cereals',
                    'SuryaTeja',
                    '500',
                    'Sona Masoori Rice',
                    '10063020',
                    '0',
                    'Bag Of 25Kgs ( 25 )',
                    'GST 5%'
                ];
                var editProductForm = element(by.xpath('//form[@name=\'editProductForm\']'));
                product.validateProductEdit(editProductForm,valuesArray);
                browser.sleep(500);
                editProductForm.element(by.xpath('//*//md-tab-item[text()=\'Discounts\']')).click();
                browser.sleep(500);
                var moqValuesArray=[{type:'MOQ',values:['20','8']}];
                product.validateMOQDiscounts(element(by.xpath('//md-content[@id=\'discounts\']')),moqValuesArray);
                product.validateBlankMOQDiscounts(element(by.xpath('//md-content[@id=\'discounts\']')),'MOV');
                editProductForm.element(by.css('[aria-label=\'close dialog\']')).click();
                product.deleteAllImported();
                browser.sleep(1000);
            });

        it('should delete multiple records with batch delete', function () {
            user.signin(loginTestData[0]);
            user.getForm().submit.click();
            browser.sleep(1000);
            product.fileImportProducts('../../products/importtestdata/product-import-template-2-categories-two-products.xlsx');
            //select all categories
            browser.sleep(2000);
            element(by.xpath('//md-checkbox[@aria-label=\'Coffee\']')).click();
            element(by.xpath('//md-checkbox[@aria-label=\'Cereals\']')).click();
            browser.sleep(1000);
            element(by.css('button[aria-label=\'Proceed to upload file\']')).click();
            browser.sleep(200);
            element(by.xpath('//create-file-import//nv-check-box')).click();
            browser.sleep(200);
            // browser.driver.executeScript("arguments[0].scrollIntoView(true);",
            //     element(by.css('button[aria-label="adding inventory"]')));
            element(by.css('button[aria-label=\'Proceed to upload file\']')).click();
            browser.sleep(500);
            element(by.css('button[aria-label=\'Proceed to upload file\']')).click();
            browser.sleep(500);
            expect(element.all(by.xpath('//view-products/table/tbody/tr')).count()).toBe(2);
            element(by.xpath('//view-products/table/tbody/tr[1]/td[3]')).click();
            browser.sleep(500);
            var valuesArray = ['Cereals',
                'Surya Teja',
                '1400',
                'Sona Masoori',
                '10063010',
                '4000',
                'Bag Of 25Kgs ( 25 )',
                'GST 0%'
            ];
            var editProductForm = element(by.xpath('//form[@name=\'editProductForm\']'));
            product.validateProductEdit(editProductForm, valuesArray);
            browser.sleep(500);
            var batchValuesArray = [
                ['CERSM-02032018-120214', '1400', '1000', '1000', '18:02:2017', '20:02:2017'],
                ['CERSM-02032018-120213', '1400', '2000', '2000', '18:02:2017', '18:02:2017'],
                ['CERSM-02032018-120212', '1400', '1000', '1000', '11:05:2017', '13:05:2017']
            ];
            product.validateBatches(element(by.xpath('//md-content[@id=\'batches\']//md-card')), batchValuesArray);
            browser.sleep(500);

            editProductForm.element(by.xpath('//*//md-tab-item[text()=\'Discounts\']')).click();
            browser.sleep(500);
            var moqValuesArray = [{type: 'MOQ', values: ['100', '5']}];
            product.validateMOQDiscounts(element(by.xpath('//md-content[@id=\'discounts\']')), moqValuesArray);
            product.validateBlankMOQDiscounts(element(by.xpath('//md-content[@id=\'discounts\']')), 'MOV');
            editProductForm.element(by.css('[aria-label=\'close dialog\']')).click();
            var list = element.all(by.xpath('//view-products/table/tbody/tr/*/md-checkbox'));
            list.each(function (item) {
                item.click();
            });
            browser.sleep(500);
            element(by.css('button[aria-label=\'Group Actions\']')).click();
            browser.sleep(1000);
            element(by.css('button[aria-label=\'Disable\']')).click();
            var list = element.all(by.xpath('//view-products/table/tbody/tr/*/md-checkbox'));
            list.each(function (item) {
                item.click();
            });
            browser.sleep(500);
            element(by.css('button[aria-label=\'Group Actions\']')).click();
            browser.sleep(1000);
            element(by.css('button[aria-label=\'Enable\']')).click();
            product.deleteAllImported();
            browser.sleep(1000);
        });
    });
});

