


'use strict';

var config = browser.params;

describe('Signin View', function() {
    var page,testData;
    var loadPage = function() {
        browser.manage().deleteAllCookies();
        browser.get('http://localhost:3000/#!/signin');

    };

    describe('with local auth', function() {

        beforeEach(function() {
            loadPage();
            page = require('./signin.po');
            testData=require('./../signup/signup.testdata');
        });


        //TEST CASE FOR MISSING BOTH THE CREDENTIALS
        it('should validate authenticated user, and redirecting to "/"', function() {
            page.form.submit.click();
            browser.findById(by.xpath('html/body/div[4]/md-dialog/md-dialog-actions/button')).click();
            expect(browser.getCurrentUrl()).toEqual('http://localhost:3000/#!/signin');
        });
        afterEach(function() {
            loadPage();
            page = require('./signin.po');
            testData=require('./../signup/signup.testdata');
        });
    });
});

