


'use strict';

var config = browser.params;
var user=require('../../account/common.util.e2e/signin.common.po');
var product=require('../../account/common.util.e2e/product.common.po');
var global = require('../../account/common.util.e2e/global.common.po');
var loginDetails=require('./login');

describe('LandingPage View', function() {
    var page;
    var page1;
    var product_testdata;
    var inventory;
    var loadPage = function () {
        browser.manage().deleteAllCookies();
        browser.get('http://localhost:3000/#!/signin');
    };

    describe('with local auth', function () {
        beforeAll(function(){
            global.loadPage('http://localhost:3000/#!/register');
            user.registerProcess(loginDetails[0]);
        });
        beforeEach(function () {
            loadPage();
            product_testdata = require('./../product_testdata');
            page1 = require('./../products.po');

            user.signin(loginDetails[0]);
            user.getForm().submit.click();
            browser.sleep(500);


        });

        afterEach(function () {

            user.logout();
        });

        it('should import products first so that a brand is added',function(){
            //no validations here, only for importing and creatng a brand, end of it inventories will be deleted.
            browser.sleep(200);
            product.fileImportProducts('../../products/importtestdata/ImportWithData.csv');
            //select all categories
            element(by.xpath('//md-checkbox[@aria-label=\'Cereals\']')).click();
            browser.sleep(1000);
            element(by.css('button[aria-label=\'Proceed to upload file\']')).click();
            browser.sleep(200);
            element(by.xpath('//div[@aria-hidden=\'false\']/div[@aria-hidden=\'false\']//md-checkbox[@id=\'all\']')).click();
            browser.sleep(200);
            // browser.driver.executeScript("arguments[0].scrollIntoView(true);",
            //     element(by.css('button[aria-label="adding inventory"]')));
            element(by.css('button[aria-label=\'Proceed to upload file\']')).click();
            browser.sleep(500);
            element(by.css('button[aria-label=\'Proceed to upload file\']')).click();
            browser.sleep(500);
            //product.deleteAllImported();
            browser.sleep(1000);
            console.log('import done');

        });



           //1. TEST CASE FOR ADD BATCH IN EDIT INVENTORY
            it('should login user,test add batch in edit inventory \'/\'', function () {
            product.initiateProducts();
            element(by.xpath('//view-products/table/tbody/tr[2]/td[3]')).click();
            browser.sleep(500);
            var batchValuesArray=[
                ['CERSM-02032018-120214','1400','1000','1000','18:02:2017','20:02:2017'],
                ['CERSM-02032018-120213','1400','2000','2000','18:02:2017','18:02:2017'],
                ['CERSM-02032018-120212','1400','1000','1000','11:05:2017','13:05:2017']
            ];
            product.validateBatches(element(by.xpath('//md-content[@id=\'batches\']//md-card')),batchValuesArray);
            var editProductForm = element(by.xpath('//form[@name=\'editProductForm\']'));
            editProductForm.element(by.css('[aria-label=\'close dialog\']')).click();
            browser.sleep(500);
            console.log('add batch in edit inventory, success');


        });

        //2.TEST CASE FOR EMPTY BATCH IN EDIT INVENTORY
        it('should login user, test for empty batch in edit inventory', function () {
            product.initiateProducts();
            element(by.xpath('//view-products/table/tbody/tr[2]/td[3]')).click();
            browser.sleep(500);
            browser.findElement(by.xpath('//*[@id="batches"]/div/button')).click();
            browser.sleep(500);
            browser.findElement(by.model('stock.batchNumber')).sendKeys('56376387');
            browser.sleep(500);
            browser.findElement(by.model('stock.openingBalance')).sendKeys('5465777');
            browser.sleep(500);
            browser.findElement(by.model('stock.currentBalance')).sendKeys('9867778');
            browser.sleep(500);
            browser.findElement(by.xpath('//button[@aria-label="Save"]')).click();
            browser.sleep(500);
            var editProductForm = element(by.xpath('//form[@name=\'editProductForm\']'));
            editProductForm.element(by.css('[aria-label=\'close dialog\']')).click();
            browser.sleep(500);
            console.log('add empty batch in edit inventory, success');
        });


           //3.TEST CASE FOR DELETE BATCH IN EDIT INVENTORY
            it('should login user,test for delete batch in edit inventory', function () {
            product.initiateProducts();
            element(by.xpath('//view-products/table/tbody/tr[2]/td[3]')).click();
            browser.sleep(1000);
            browser.findElement(by.xpath('//button[@class=\'md-primary md-raised nvc-shape-pill md-button md-ink-ripple\']//span[@class=\'fa fa-plus ng-scope\']')).click();
            browser.sleep(200);
            browser.findElement(by.model('stock.batchNumber')).sendKeys('56376387');
            browser.sleep(200);
            browser.findElement(by.model('stock.openingBalance')).sendKeys('5465777');
            browser.sleep(200);
            browser.findElement(by.model('stock.currentBalance')).sendKeys('9867778');
            browser.sleep(200);
            browser.findElement(by.xpath('//button[@aria-label="Save"]')).click();
            browser.sleep(500);
            browser.findElement(by.xpath('//*[@id=\'stock-0\']/div/div/div[1]/div/div[1]/div[2]/div[@aria-label=\'Delete Batch\']/button')).click();
            browser.sleep(500);
            //*[@id='stock-0']/div/div[2]/div/div[1]/button/md-icon
            var editProductForm = element(by.xpath('//form[@name=\'editProductForm\']'));
            editProductForm.element(by.css('[aria-label=\'close dialog\']')).click();
            browser.sleep(200);
            console.log('delete batch in edit inventory, success');
        });



         //4.TEST CASE FOR UPDATE BATCH IN EDIT INVENTORY
            it('should login user, update batch', function () {
              product.initiateProducts();
            element(by.xpath('//view-products/table/tbody/tr[2]/td[3]')).click();
            browser.sleep(500);
            browser.findElement(by.xpath('//*[@id=\'stock-0\']/div/div/div[1]/div/div[1]/div[2]/div[1]/button[@aria-label=\'Edit-Batch\']')).click();
            browser.sleep(200);
            var stockbatchnum = element(by.model('stock.batchNumber'));
            stockbatchnum.clear().then(function() {
                stockbatchnum.sendKeys('56376387');
            })
            browser.sleep(200);
            var openingbalance = element(by.model('stock.openingBalance'));
            openingbalance.clear().then(function(){
            openingbalance.sendKeys('5465777');
            });
            browser.sleep(200);
            var currentBal = element(by.model('stock.currentBalance'));
            currentBal.clear().then(function(){
                currentBal.sendKeys('9867778')
            });
            browser.sleep(200);

            browser.findElement(by.xpath('//*[@id=\'stock-0\']/div/div/div[1]/div/div[1]/div[1]/div[1]/button[@aria-label=\'Edit-Batch\']')).click();
            browser.sleep(200);
            var editProductForm = element(by.xpath('//form[@name=\'editProductForm\']'));
            editProductForm.element(by.css('[aria-label=\'close dialog\']')).click();
            element(by.xpath('//view-products/table/tbody/tr[2]/td[3]')).click();
            browser.sleep(500);
            var batchValuesArray=[
                ['56376387','35355','5465777','9867778','',''],
                ['CERSM-02032018-121214','1400','1000','1000','18:02:2017','20:02:2017'],
                ['CERSM-02032018-120220','1400','2000','2000','18:02:2017','18:02:2017'],
                ['CERSM-02032018-120211','1400','1000','1000','11:05:2017','13:05:2017']
            ];
            product.validateBatches(element(by.xpath('//md-content[@id=\'batches\']//md-card')),batchValuesArray);
            browser.sleep(500);
            editProductForm.element(by.css('[aria-label=\'close dialog\']')).click();
            console.log('update batch in edit inventory, success');

        });

        //5.TEST CASES FOR CREATING DISCOUNT IN EDIT INVENTORY
       it('should login user, create discount ', function () {
           product.initiateProducts();
           element(by.xpath('//view-products/table/tbody/tr[2]/td[3]')).click();
           browser.sleep(500);
           var editProductForm = element(by.xpath('//*[@name="editProductForm"]'));
           var discountsTab = editProductForm.element(by.xpath('//*//md-tab-item[text()=\'Discounts\']'));
           discountsTab.click();
           browser.sleep(200);
            element(by.xpath('//*[@id="sale-discounts"]/div[1]/div[1]/span')).click();
            browser.findElement(by.model('salePrice.MOQ')).clear();
            browser.findElement(by.model('salePrice.MOQ')).sendKeys('674744');
            browser.sleep(500);

            browser.findElement(by.model('salePrice.margin')).clear();
            browser.findElement(by.model('salePrice.margin')).sendKeys('55');
            browser.sleep(500);
           browser.findElement(by.model('saleValue.MOV')).clear();
           browser.findElement(by.model('saleValue.MOV')).sendKeys('98865');
           browser.sleep(500);

           browser.findElement(by.model('saleValue.margin')).clear();
           browser.findElement(by.model('saleValue.margin')).sendKeys('88');
           browser.sleep(500);
           element(by.xpath('//*[@id="discounts"]//md-tab-item[contains(text(),\'Buy\')]')).click();

           element(by.xpath('//*[@id="buy-discounts"]/div[1]/div[1]/span')).click();
            browser.findElement(by.model('buyPrice.MOQ')).clear();
            browser.findElement(by.model('buyPrice.MOQ')).sendKeys('958858');
            browser.sleep(500);

            browser.findElement(by.model('buyPrice.margin')).clear();
            browser.findElement(by.model('buyPrice.margin')).sendKeys('88');
            browser.sleep(500);

           element(by.xpath('//*[@id="buy-discounts"]/div[2]/div[1]/span')).click();

            browser.findElement(by.model('buyValue.MOV')).clear();
            browser.findElement(by.model('buyValue.MOV')).sendKeys('234421');
            browser.sleep(500);

            browser.findElement(by.model('buyValue.margin')).clear();
            browser.findElement(by.model('buyValue.margin')).sendKeys('76');
            browser.sleep(500);
            browser.findElement(by.xpath('//button[@aria-label=\'Save\']')).click();
            browser.sleep(500);
           var editProductForm = element(by.xpath('//form[@name=\'editProductForm\']'));
           editProductForm.element(by.css('[aria-label=\'close dialog\']')).click();
           console.log('update mov and moq in inventory, success');
           browser.sleep(200);

        });

           //6.TEST CASES FOR CREATING DISCOUNT WITH MULTIPLE MOQ,MOV AND MARGIN IN EDIT INVENTORY
            it('should login user,create discount with multiple moq,mov and margin ', function () {
            product.initiateProducts();
            element(by.xpath('//view-products/table/tbody/tr[2]/td[3]')).click();
            browser.sleep(500);
            var editProductForm = element(by.xpath('//*[@name="editProductForm"]'));
            var discountsTab = editProductForm.element(by.xpath('//*//md-tab-item[text()=\'Discounts\']'));
            discountsTab.click();
            browser.sleep(200);
            element(by.xpath('//*[@id="sale-discounts"]/div[1]/div[1]/span')).click();
            browser.findElement(by.model('salePrice.MOQ')).clear();
            browser.findElement(by.model('salePrice.MOQ')).sendKeys('674744');
            browser.sleep(500);

            element(by.xpath('//*[@id="sale-discounts"]/div[1]/div[1]/span')).click();

            browser.findElement(by.xpath('//*[@id=\'salePrice-1\']//*[@ng-model=\'salePrice.MOQ\']')).clear();
            browser.findElement(by.xpath('//*[@id=\'salePrice-1\']//*[@ng-model=\'salePrice.MOQ\']')).sendKeys('32535');
            browser.sleep(500);


            browser.findElement(by.model('salePrice.margin')).clear();
            browser.findElement(by.model('salePrice.margin')).sendKeys('55');
            browser.sleep(500);

            browser.findElement(by.xpath('//*[@id=\'salePrice-1\']//*[@ng-model=\'salePrice.margin\']')).clear();
            browser.findElement(by.xpath('//*[@id=\'salePrice-1\']//*[@ng-model=\'salePrice.margin\']')).sendKeys('54');
            browser.sleep(500);

            browser.findElement(by.model('saleValue.MOV')).clear();
            browser.findElement(by.model('saleValue.MOV')).sendKeys('98865');
            browser.sleep(500);

            element(by.xpath('//*[@id=\'sale-discounts\']/div[2]/div/span')).click();
            browser.sleep(500);
            browser.findElement(by.xpath('//*[@id=\'saleValue-1\']//*[@ng-model=\'saleValue.MOV\']')).clear();
            browser.findElement(by.xpath('//*[@id=\'saleValue-1\']//*[@ng-model=\'saleValue.MOV\']')).sendKeys('32535');
            browser.sleep(500);

            browser.findElement(by.model('saleValue.margin')).clear();
            browser.findElement(by.model('saleValue.margin')).sendKeys('88');
            browser.sleep(500);

            browser.findElement(by.xpath('//*[@id=\'saleValue-1\']//*[@ng-model=\'saleValue.margin\']')).clear();
            browser.findElement(by.xpath('//*[@id=\'saleValue-1\']//*[@ng-model=\'saleValue.margin\']')).sendKeys('22');
            browser.sleep(500);

            element(by.xpath('//*[@id="discounts"]//md-tab-item[contains(text(),\'Buy\')]')).click();

            element(by.xpath('//*[@id=\'buy-discounts\']/div[1]/div[1]/span')).click();
            browser.findElement(by.model('buyPrice.MOQ')).clear();
            browser.findElement(by.model('buyPrice.MOQ')).sendKeys('958858');
            browser.sleep(500);
            element(by.xpath('//*[@id=\'buy-discounts\']/div[1]/div[1]/span')).click();
            //browser.findElement(by.xpath("//*[@id=\"addBuy\"]")).click();
            browser.sleep(500);
            browser.findElement(by.xpath('//*[@id=\'buyPrice-1\']//*[@ng-model=\'buyPrice.MOQ\']')).clear();
            browser.findElement(by.xpath('//*[@id=\'buyPrice-1\']//*[@ng-model=\'buyPrice.MOQ\']')).sendKeys('732143');
            browser.sleep(500);

            browser.findElement(by.model('buyPrice.margin')).clear();
            browser.findElement(by.model('buyPrice.margin')).sendKeys('78');
            browser.sleep(500);

            browser.findElement(by.xpath('//*[@id=\'buyPrice-1\']//*[@ng-model=\'buyPrice.margin\']')).clear();
            browser.findElement(by.xpath('//*[@id=\'buyPrice-1\']//*[@ng-model=\'buyPrice.margin\']')).sendKeys('91');
            browser.sleep(500);

            element(by.xpath('//*[@id=\'buy-discounts\']/div[2]/div[1]/span')).click();
            browser.findElement(by.model('buyValue.MOV')).clear();
            browser.findElement(by.model('buyValue.MOV')).sendKeys('234421');
            browser.sleep(500);

            element(by.xpath('//*[@id="buy-discounts"]/div[2]/div[1]/span')).click();
            browser.sleep(500);
            browser.findElement(by.xpath('//*[@id=\'buyValue-1\']//*[@ng-model=\'buyValue.MOV\']')).clear();
            browser.findElement(by.xpath('//*[@id=\'buyValue-1\']//*[@ng-model=\'buyValue.MOV\']')).sendKeys('93215');
            browser.sleep(500);

            browser.findElement(by.model('buyValue.margin')).clear();
            browser.findElement(by.model('buyValue.margin')).sendKeys('88');
            browser.sleep(500);
            browser.findElement(by.xpath('//*[@id=\'buyValue-1\']//*[@ng-model=\'buyValue.margin\']')).clear();
            browser.findElement(by.xpath('//*[@id=\'buyValue-1\']//*[@ng-model=\'buyValue.margin\']')).sendKeys('39');
            browser.sleep(500);

            //browser.driver.executeScript('arguments[0].scrollIntoView(true);', element(by.xpath('//div/md-content/div[1]/div[1]/div[1]/button/md-icon')).getWebElement());
            //browser.sleep(500);


            //browser.driver.executeScript('arguments[0].scrollIntoView(true);', element(by.xpath('//div/md-content/div[1]/div[2]/div[1]/button/md-icon')).getWebElement());
            //browser.sleep(500);



            //browser.driver.executeScript('arguments[0].scrollIntoView(true);', element(by.xpath('//div/md-content/div[1]/div[3]/div[1]/button/md-icon')).getWebElement());
            //browser.sleep(500);



            //browser.driver.executeScript('arguments[0].scrollIntoView(true);', element(by.xpath('//div/md-content/div[1]/div[4]/div[1]/button/md-icon')).getWebElement());
            //browser.sleep(500);
            browser.findElement(by.xpath('//button[@aria-label=\'Save\']')).click();

            editProductForm.element(by.css('[aria-label=\'close dialog\']')).click();


        });

         //7.TEST CASES FOR VALIDATION OF MOQ IN SALE PRICE WHILE CREATING DISCOUNT IN EDIT INVENTORY
          it('should login user, validate moq in sale price"/"', function () {
              product.initiateProducts();
              element(by.xpath('//view-products/table/tbody/tr[2]/td[3]')).click();
              browser.sleep(500);
              var editProductForm = element(by.xpath('//*[@name="editProductForm"]'));
              var discountsTab = editProductForm.element(by.xpath('//*//md-tab-item[text()=\'Discounts\']'));
              discountsTab.click();
              browser.sleep(200);
              discountsTab.element(by.xpath("//*[@id=\"salePrice-0\"]//*[@ng-model=\"salePrice.MOQ\"]")).clear();
              discountsTab.element(by.xpath("//*[@id=\"salePrice-0\"]//*[@ng-model=\"salePrice.MOQ\"]")).sendKeys("fjgglj");
              browser.sleep(500);
              expect(discountsTab.element(by.xpath("//*[@id=\"salePrice-0\"]//md-input-container/i[@data='Invalid Input..']")).isDisplayed()).toBe(true);
              browser.sleep(500);
              editProductForm.element(by.css('[aria-label=\'close dialog\']')).click();
        });


            //8.TEST CASES FOR VALIDATION OF MOV IN SALE VALUE WHILE CREATING DISCOUNT IN EDIT INVENTORY
            it('should login user, valide mov in sale price "/"', function () {
              product.initiateProducts();
              element(by.xpath('//view-products/table/tbody/tr[2]/td[3]')).click();
              browser.sleep(500);
              var editProductForm = element(by.xpath('//*[@name="editProductForm"]'));
              var discountsTab = editProductForm.element(by.xpath('//*//md-tab-item[text()=\'Discounts\']'));
              discountsTab.click();
              browser.sleep(200);
              browser.findElement(by.xpath("//*[@id=\"saleValue-0\"]//*[@ng-model=\"saleValue.MOV\"]")).clear();
              browser.findElement(by.xpath("//*[@id=\"saleValue-0\"]//*[@ng-model=\"saleValue.MOV\"]")).sendKeys("gdhjd");
              browser.sleep(500);
              expect(discountsTab.element(by.xpath("//*[@id=\"saleValue-0\"]//md-input-container/i[@data='Invalid Input..']")).isDisplayed()).toBe(true);
              browser.sleep(500);
              editProductForm.element(by.css('[aria-label=\'close dialog\']')).click();


         });

            //9.TEST CASES FOR VALIDATION OF MARGIN IN SALE PRICE  WHILE CREATING DISCOUNT IN EDIT INVENTORY
            it('should login user, validation of margin in sale price', function () {
            product.initiateProducts();
            element(by.xpath('//view-products/table/tbody/tr[2]/td[3]')).click();
            browser.sleep(500);
            var editProductForm = element(by.xpath('//*[@name="editProductForm"]'));
            var discountsTab = editProductForm.element(by.xpath('//*//md-tab-item[text()=\'Discounts\']'));
            discountsTab.click();
            browser.sleep(200);
            element(by.xpath('//*[@id=\'salePrice-0\']//*[@ng-model=\'salePrice.margin\']')).clear();
            element(by.xpath('//*[@id=\'salePrice-0\']//*[@ng-model=\'salePrice.margin\']')).sendKeys('-55');
            browser.sleep(500);
            expect(discountsTab.element(by.xpath('//*[@id=\'salePrice-0\']//md-input-container/i[@data=\'Enter Proper input..\']')).isDisplayed()).toBe(true);
            browser.sleep(500);
            editProductForm.element(by.css('[aria-label=\'close dialog\']')).click();


        });

            //10.TEST CASES FOR VALIDATION OF MARGIN IN SALE VALUE  WHILE CREATING DISCOUNT IN EDIT INVENTORY

            it('should login user, validation of margin in sale value', function () {
            product.initiateProducts();
            element(by.xpath('//view-products/table/tbody/tr[2]/td[3]')).click();
            browser.sleep(500);
            var editProductForm = element(by.xpath('//*[@name="editProductForm"]'));
            var discountsTab = editProductForm.element(by.xpath('//*//md-tab-item[text()="Discounts"]'));
            discountsTab.click();
            browser.sleep(200);
            //*[@id="discounts"]/div/md-tabs/md-tabs-wrapper/md-tabs-canvas/md-pagination-wrapper/md-tab-item[2]
            var discountsElem = element(by.id('discounts'));

            browser.findElement(by.xpath('//*[@id=\'saleValue-0\']//*[@ng-model=\'saleValue.margin\']')).clear();
            browser.findElement(by.xpath('//*[@id=\'saleValue-0\']//*[@ng-model=\'saleValue.margin\']')).sendKeys('-88');
            browser.sleep(500);
            expect(browser.findElement(by.xpath('//*[@id=\'saleValue-0\']//md-input-container/i[@data=\'Enter Proper input..\']')).isDisplayed()).toBe(true);
            browser.sleep(500);

            editProductForm.element(by.css('[aria-label=\'close dialog\']')).click();

        });

            //11.TEST CASES FOR VALIDATION OF MOQ IN BUY PRICE WHILE CREATING DISCOUNT IN EDIT INVENTORY
            it('should login user, validate moq in buy price', function () {
            product.initiateProducts();
            element(by.xpath('//view-products/table/tbody/tr[2]/td[3]')).click();
            browser.sleep(500);
            var editProductForm = element(by.xpath('//*[@name="editProductForm"]'));
            var discountsTab = editProductForm.element(by.xpath('//*//md-tab-item[text()=\'Discounts\']'));
            discountsTab.click();
            element(by.xpath('//*[@id="discounts"]/div/md-tabs/md-tabs-wrapper/md-tabs-canvas/md-pagination-wrapper/md-tab-item[2]')).click();
            browser.sleep(500);
            element(by.xpath('//*[@id="buy-discounts"]/div[1]/div[1]/span')).click();
            browser.findElement(by.xpath('//*[@id=\'buyPrice-0\']//*[@ng-model=\'buyPrice.MOQ\']')).clear();
            browser.findElement(by.xpath('//*[@id=\'buyPrice-0\']//*[@ng-model=\'buyPrice.MOQ\']')).sendKeys('fjgglj');
            browser.sleep(500);
            expect(browser.findElement(by.xpath('//*[@id=\'buyPrice-0\']//md-input-container/i[@data=\'Invalid Input..\']')).isDisplayed()).toBe(true);
            browser.sleep(500);

            editProductForm.element(by.css('[aria-label=\'close dialog\']')).click();

        });

        //12.TEST CASES FOR VALIDATION OF MOV IN BUY VALUE WHILE CREATING DISCOUNT IN EDIT INVENTORY
        it('should login user, validation of mov in buy value', function () {
            product.initiateProducts();
            element(by.xpath('//view-products/table/tbody/tr[2]/td[3]')).click();
            browser.sleep(500);
            var editProductForm = element(by.xpath('//*[@name="editProductForm"]'));
            var discountsTab = editProductForm.element(by.xpath('//*//md-tab-item[text()=\'Discounts\']'));
            discountsTab.click();
            element(by.xpath('//*[@id="discounts"]/div/md-tabs/md-tabs-wrapper/md-tabs-canvas/md-pagination-wrapper/md-tab-item[2]')).click();
            browser.sleep(500);
            element(by.xpath('//*[@id="buy-discounts"]/div[2]/div/span')).click();
            discountsTab.element(by.xpath("//*[@id=\"buyValue-0\"]//*[@ng-model=\"buyValue.MOV\"]")).clear();
            browser.findElement(by.xpath("//*[@id=\"buyValue-0\"]//*[@ng-model=\"buyValue.MOV\"]")).sendKeys("gdhjd");
            browser.sleep(500);
            expect(discountsTab.element(by.xpath("//*[@id=\"buyValue-0\"]//md-input-container/i[@data='Invalid Input..']")).isDisplayed()).toBe(true);
            browser.sleep(500);

            editProductForm.element(by.css('[aria-label=\'close dialog\']')).click();


        });

       //13. TEST CASES FOR VALIDATION OF MARGIN IN BUY PRICE  WHILE CREATING DISCOUNT IN EDIT INVENTORY
        it('should login user, Checking Functionalities in Landing Page and redirecting to "/"', function () {
            product.initiateProducts();
            element(by.xpath('//view-products/table/tbody/tr[2]/td[3]')).click();
            browser.sleep(500);
            var editProductForm = element(by.xpath('//*[@name="editProductForm"]'));
            var discountsTab = editProductForm.element(by.xpath('//*//md-tab-item[text()=\'Discounts\']'));
            discountsTab.click();
            element(by.xpath('//*[@id="discounts"]/div/md-tabs/md-tabs-wrapper/md-tabs-canvas/md-pagination-wrapper/md-tab-item[2]')).click();
            browser.sleep(500);
            element(by.xpath('//*[@id="buy-discounts"]/div[1]/div[1]/span')).click();

            browser.findElement(by.xpath("//*[@id=\"buyPrice-0\"]//*[@ng-model=\"buyPrice.margin\"]")).clear();
            browser.findElement(by.xpath("//*[@id=\"buyPrice-0\"]//*[@ng-model=\"buyPrice.margin\"]")).sendKeys("-55");
            browser.sleep(500);
            expect(browser.findElement(by.xpath("//*[@id=\"buyPrice-0\"]//md-input-container/i[@data='Enter Proper input..']")).isDisplayed()).toBe(true);
            browser.sleep(500);
            editProductForm.element(by.css('[aria-label=\'close dialog\']')).click();

        });

        //14.TEST CASES FOR VALIDATION OF MARGIN IN BUY VALUE  WHILE CREATING DISCOUNT IN EDIT INVENTORY
        it('should login user, Checking Functionalities in Landing Page and redirecting to "/"', function () {
            product.initiateProducts();
            element(by.xpath('//view-products/table/tbody/tr[2]/td[3]')).click();
            browser.sleep(500);
            var editProductForm = element(by.xpath('//*[@name="editProductForm"]'));
            var discountsTab = editProductForm.element(by.xpath('//*//md-tab-item[text()=\'Discounts\']'));
            discountsTab.click();
            element(by.xpath('//*[@id="discounts"]/div/md-tabs/md-tabs-wrapper/md-tabs-canvas/md-pagination-wrapper/md-tab-item[2]')).click();
            browser.sleep(500);
            element(by.xpath('//*[@id="buy-discounts"]/div[2]/div/span')).click();
            browser.findElement(by.xpath("//*[@id=\"buyValue-0\"]//*[@ng-model=\"buyValue.margin\"]")).clear();
            browser.findElement(by.xpath("//*[@id=\"buyValue-0\"]//*[@ng-model=\"buyValue.margin\"]")).sendKeys("-88");
            browser.sleep(500);
            expect(browser.findElement(by.xpath("//*[@id=\"buyValue-0\"]//md-input-container/i[@data='Enter Proper input..']")).isDisplayed()).toBe(true);
            browser.sleep(500);

            editProductForm.element(by.css('[aria-label=\'close dialog\']')).click();

            });

           //15.TEST CASES FOR VALIDATION OF MULTIPLE MARGIN,MOQ,MOV IN EDIT INVENTORY
            it('should login user, validation of multiple margin moq,mov "/"', function () {
            product.initiateProducts();
            element(by.xpath('//view-products/table/tbody/tr[2]/td[3]')).click();
            browser.sleep(500);
            var editProductForm = element(by.xpath('//form[@name=\'editProductForm\']'));
            var discountsTab = editProductForm.element(by.xpath('//*//md-tab-item[text()=\'Discounts\']'));
            discountsTab.click();
            browser.sleep(500);

            browser.findElement(by.xpath("//*[@id=\"salePrice-1\"]//*[@ng-model=\"salePrice.MOQ\"]")).clear();
            browser.findElement(by.xpath("//*[@id=\"salePrice-1\"]//*[@ng-model=\"salePrice.MOQ\"]")).sendKeys("-55");
            browser.sleep(500);
            expect(browser.findElement(by.xpath("//*[@id=\"salePrice-1\"]//md-input-container/i[@data='Invalid Input..']")).isDisplayed()).toBe(true);
            browser.sleep(500);


            browser.findElement(by.xpath("//*[@id=\"salePrice-1\"]//*[@ng-model=\"salePrice.margin\"]")).clear();
            browser.findElement(by.xpath("//*[@id=\"salePrice-1\"]//*[@ng-model=\"salePrice.margin\"]")).sendKeys("-55");
            browser.sleep(500);
            expect(browser.findElement(by.xpath("//*[@id=\"salePrice-1\"]//md-input-container/i[@data='Enter Proper input..']")).isDisplayed()).toBe(true);
            browser.sleep(500);
            element(by.xpath('//*[@id="sale-discounts"]/div[2]/div/span')).click();
            browser.findElement(by.xpath("//*[@id=\"saleValue-1\"]//*[@ng-model=\"saleValue.MOV\"]")).clear();
            browser.findElement(by.xpath("//*[@id=\"saleValue-1\"]//*[@ng-model=\"saleValue.MOV\"]")).sendKeys("gdhjd");
            browser.sleep(500);
            expect(browser.findElement(by.xpath("//*[@id=\"saleValue-1\"]//md-input-container/i[@data='Invalid Input..']")).isDisplayed()).toBe(true);
            browser.sleep(500);

            browser.findElement(by.xpath("//*[@id=\"saleValue-1\"]//*[@ng-model=\"saleValue.margin\"]")).clear();
            browser.findElement(by.xpath("//*[@id=\"saleValue-1\"]//*[@ng-model=\"saleValue.margin\"]")).sendKeys("gdhjd");
            browser.sleep(500);
            expect(browser.findElement(by.xpath("//*[@id=\"saleValue-1\"]//md-input-container/i[@data='Enter Proper input..']")).isDisplayed()).toBe(true);
            browser.sleep(500);

            element(by.xpath('//*[@id="discounts"]//md-tab-item[contains(text(),\'Buy\')]')).click();
            element(by.xpath('//*[@id="buy-discounts"]/div[1]/div/span')).click();
            browser.findElement(by.xpath('//*[@id=\'buyPrice-0\']//*[@ng-model=\'buyPrice.MOQ\']')).clear();
            browser.findElement(by.xpath('//*[@id=\'buyPrice-0\']//*[@ng-model=\'buyPrice.MOQ\']')).sendKeys('-55');
            browser.sleep(500);
            expect(browser.findElement(by.xpath("//*[@id=\"buyPrice-0\"]//md-input-container/i[@data='Invalid Input..']")).isDisplayed()).toBe(true);
            browser.sleep(500);

            browser.findElement(by.xpath('//*[@id=\'buyPrice-0\']//*[@ng-model=\'buyPrice.margin\']')).clear();
            browser.findElement(by.xpath('//*[@id=\'buyPrice-0\']//*[@ng-model=\'buyPrice.margin\']')).sendKeys('-55');
            //
            expect(browser.findElement(by.xpath('//*[@id="buyPrice-0"]//md-input-container/i[@data=\'Invalid Input..\']')).isDisplayed()).toBe(true);
            browser.sleep(500);

            // browser.driver.executeScript("arguments[0].scrollIntoView(true);", element(by.xpath('//div/md-content/div[1]/div[3]/div[1]/button/md-icon')).getWebElement());
            // browser.sleep(500);

            // browser.driver.executeScript("arguments[0].scrollIntoView(true);", element(by.xpath('//div/md-content/div[1]/div[4]/div[1]/button/md-icon')).getWebElement());
            // browser.sleep(500);
            element(by.xpath('//*[@id="buy-discounts"]/div[2]/div/span')).click();
            browser.findElement(by.xpath("//*[@id=\"buyValue-0\"]//*[@ng-model=\"buyValue.MOV\"]")).clear();
            browser.findElement(by.xpath("//*[@id=\"buyValue-0\"]//*[@ng-model=\"buyValue.MOV\"]")).sendKeys("gdhjd");
            browser.sleep(500);
            expect(browser.findElement(by.xpath("//*[@id=\"buyValue-0\"]//md-input-container/i[@data='Invalid Input..']")).isDisplayed()).toBe(true);
            browser.sleep(500);

            browser.findElement(by.xpath("//*[@id=\"buyValue-0\"]//*[@ng-model=\"buyValue.margin\"]")).clear();
            browser.findElement(by.xpath("//*[@id=\"buyValue-0\"]//*[@ng-model=\"buyValue.margin\"]")).sendKeys("gdhjd");
            browser.sleep(500);
            expect(browser.findElement(by.xpath("//*[@id=\"buyValue-0\"]//md-input-container/i[@data='Enter Proper input..']")).isDisplayed()).toBe(true);
            browser.sleep(500);


            editProductForm.element(by.css('[aria-label=\'close dialog\']')).click();

        });

        //    //16.TEST CASES FOR UPLOADING IN EDIT INVENTORY
        //      it('should login user, Checking Functionalities in Landing Page and redirecting to "/"', function () {
        //     product.initiateProducts();
        //     element(by.xpath('//view-products/table/tbody/tr[2]/td[3]')).click();
        //     browser.sleep(500);
        //     var editProductForm = element(by.xpath('//form[@name=\'editProductForm\']'));
        //     var imagesTab = editProductForm.element(by.xpath('//*//md-tab-item[text()=\'Images\']'));
        //     imagesTab.click();
        //     browser.sleep(500);
        //     var path=require('path');
        //     var absolutePath1 = path.resolve(__dirname, "./images/cerels.png");
        //     var absolutePath2 = path.resolve(__dirname, "./images/Coffee.png");
        //     var absolutePath3 = path.resolve(__dirname, "./images/Cotton.png");
        //     var absolutePath4 = path.resolve(__dirname, "./images/Paddyhusk.png");
        //     browser.findElement(by.model("inventory.inventoryImageURL1")).sendKeys(absolutePath1);
        //     browser.sleep(500);
        //     browser.findElement(by.model("inventory.inventoryImageURL2")).sendKeys(absolutePath2);
        //     browser.sleep(500);
        //     browser.findElement(by.model("inventory.inventoryImageURL3")).sendKeys(absolutePath3);
        //     browser.sleep(500);
        //     browser.findElement(by.model("inventory.inventoryImageURL4")).sendKeys(absolutePath4);
        //     browser.sleep(500);
        //
        //     editProductForm.element(by.css('[aria-label=\'close dialog\']')).click();
        //     });
        //
          });


});

