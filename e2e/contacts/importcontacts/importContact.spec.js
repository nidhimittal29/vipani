


'use strict';
var path = require('path');
var user = require('../../account/common.util.e2e/signin.common.po');
var contactsCommon = require('../../account/common.util.e2e/contacts.common.po');
var config = browser.params;
var global = require('../../account/common.util.e2e/global.common.po');
var loginTestData = require('./login');
describe('LandingPage View', function() {
    var signupPage;
    var page;
    var inventory;
    var editContactpage;
    var loadPage = function () {
        browser.manage().deleteAllCookies();
        browser.get('http://localhost:3000/#!/signin');
    };
    describe('with local auth', function () {

        beforeAll(function(){
            global.loadPage('http://localhost:3000/#!/register');
            user.registerProcess(loginTestData[0]);
        });
        beforeEach(function () {
            loadPage();

        });


//1.TESTCASE TO import contacts from xlsx file
        it('should login user, import contacts from xlsx file', function () {
            user.signin(loginTestData[0]);
            user.getForm().submit.click();
            browser.sleep(1000);
            var fileToUpload = '../../contacts/importtestdata/contacts-import-template.xlsx';
            contactsCommon.fileImportContacts(fileToUpload);
            element(by.xpath('//a[@id=\'proceedButton\']')).click();
            browser.sleep(4000);
            browser.executeScript('window.scrollTo(0,0);').then(function () {
                browser.findElement(by.id("finishImport")).click();
                browser.sleep(1000);
            });
            element(by.xpath('//*[@id="contact-tabs"]/md-tabs/*//md-tab-item[2]')).click();
            expect(element(by.xpath("//md-content[@id='contact_Suppliers']/view-contacts/table/tbody/tr")).length,1);
            expect(element(by.xpath("//md-content[@id='contact_Suppliers']/view-contacts/table/tbody/tr/td")).length,6);
            editContactpage = require('../editcontact/editContact.po');
            var editContactForm = contactsCommon.getEditContactsForm();
            element(by.xpath("//md-content[@id='contact_Suppliers']/view-contacts/table/tbody/tr/td[3]")).click();
            browser.sleep(2000);
            expect(element(by.xpath('//form[@name="editContactForm"]//*[contains(text(),"Contact Details")]')).
            isDisplayed()).toBe(true);
            //*[@id="contactdetails"]/div[4]/label
            expect(element(by.xpath('//*[@id="contactdetails"]/div[6]/label')).
            getText()).toBe('Bangalore');
            //*[@id="contactdetails"]/div[7]/label
            //*[@id="contactdetails"]/div[3]/label
            expect(element(by.xpath('//*[@id="contactdetails"]/div[3]/label')).
            getText()).toBe('Supplier');
            expect(element(by.xpath('//*[@id="contactdetails"]/div[5]/label')).
            getText()).toBe('Active');

            browser.driver.executeScript("arguments[0].scrollIntoView(true);", element(by.xpath
            ('//form[@name=\'editContactForm\']//*[@id=\'contact_phone\']//*[@data-ng-model=\'phone.phoneNumber\']')).getWebElement());
            browser.sleep(1000);
            expect(element(by.xpath('//form[@name=\'editContactForm\']//*[@id=\'contact_phone\']//*[@data-ng-model=\'phone.phoneNumber\']')).
            getAttribute('value')).toBe('1231231212');
            element(by.xpath('//form[@name=\'editContactForm\']//md-tabs-canvas//*[contains(text(),\'Email\')]')).click();
            browser.sleep(1000);
            expect(element(by.xpath('//form[@name=\'editContactForm\']//*[@id=\'contact_email\']//*[@ng-model=\'email.email\']')).
            getAttribute('value')).toBe('abc@gmail.com');
            browser.driver.executeScript("arguments[0].scrollIntoView(true);", element(by.xpath
            ('//form[@name=\'editContactForm\']//*[@id="tab_Billing"]')).getWebElement());
            browser.sleep(1000);
            expect(element(by.xpath('//md-content[@id="tab_Billing"]//*[@id="addressLine"]')).
            getAttribute('value')).toBe('Addrline1');
            expect(element(by.xpath('//md-content[@id="tab_Billing"]//*[@id="city"]')).
            getAttribute('value')).toBe('Bangalore');
            expect(element(by.xpath('//md-content[@id="tab_Billing"]//*[@id="state"]')).
            getAttribute('value')).toBe('Karnataka');
            expect(element(by.xpath('//md-content[@id="tab_Billing"]//*[@id="country"]')).
            getAttribute('value')).toBe('India');
            element(by.xpath('//form[@name=\'editContactForm\']/*//md-tab-item[text()=\'Shipping\']')).click();
            browser.sleep(1000);
            expect(element(by.xpath('//md-content[@id="tab_Shipping"]//*[@id="addressLine"]')).
            getAttribute('value')).toBe('AddrLine1');
            expect(element(by.xpath('//md-content[@id="tab_Shipping"]//*[@id="city"]')).
            getAttribute('value')).toBe('Bangalore');
            expect(element(by.xpath('//md-content[@id="tab_Shipping"]//*[@id="state"]')).
            getAttribute('value')).toBe('Karnataka');
            expect(element(by.xpath('//md-content[@id="tab_Shipping"]//*[@id="country"]')).
            getAttribute('value')).toBe('India');
            browser.driver.executeScript("arguments[0].scrollIntoView(true);", element(by.css
            ('button[aria-label="close dialog"')).getWebElement());
            element(by.css('form[name="editContactForm"] button[aria-label="close dialog"]')).click();
            browser.sleep(500);
            contactsCommon.deleteContacts('Suppliers');
        });

        //2.TESTCASE TO import contacts from csv file
        it('should login user, import contacts from csv file', function () {
            user.signin(loginTestData[0]);
            user.getForm().submit.click();
            browser.sleep(1000);
            var fileToUpload = '../../contacts/importtestdata/contacts-import-template.csv';
            contactsCommon.fileImportContacts(fileToUpload);
            browser.findElement(by.id("proceedButton")).click();
            browser.sleep(4000);
            browser.executeScript('window.scrollTo(0,0);').then(function () {
                browser.findElement(by.id("finishImport")).click();
                browser.sleep(1000);
            });
            expect(element(by.xpath("//md-content[@id='contact_Customers']/view-contacts/table/tbody/tr")).length,1);
            expect(element(by.xpath("//md-content[@id='contact_Customers']/view-contacts/table/tbody/tr/td")).length,6);
            editContactpage = require('../editcontact/editContact.po');
            var editContactForm = contactsCommon.getEditContactsForm();
            element(by.xpath("//md-content[@id='contact_Customers']/view-contacts/table/tbody/tr/td[3]")).click();
            browser.sleep(2000);
            expect(element(by.xpath('//form[@name="editContactForm"]//*[contains(text(),"Contact Details")]')).
            isDisplayed()).toBe(true);
            //*[@id="contactdetails"]/div[4]/label
            expect(element(by.xpath('//*[@id="contactdetails"]/div[6]/label')).
            getText()).toBe('Bangalore');
            //*[@id="contactdetails"]/div[7]/label
            //*[@id="contactdetails"]/div[3]/label
            expect(element(by.xpath('//*[@id="contactdetails"]/div[3]/label')).
            getText()).toBe('Customer');
            expect(element(by.xpath('//*[@id="contactdetails"]/div[5]/label')).
            getText()).toBe('Active');

            browser.driver.executeScript("arguments[0].scrollIntoView(true);", element(by.xpath
            ('//form[@name=\'editContactForm\']//*[@id=\'contact_phone\']//*[@data-ng-model=\'phone.phoneNumber\']')).getWebElement());
            browser.sleep(1000);
            expect(element(by.xpath('//form[@name=\'editContactForm\']//*[@id=\'contact_phone\']//*[@data-ng-model=\'phone.phoneNumber\']')).
            getAttribute('value')).toBe('1231231212');
            browser.driver.executeScript("arguments[0].scrollIntoView(true);", element(by.xpath
            ('//form[@name=\'editContactForm\']//*[@id="tab_Billing"]')).getWebElement());
            browser.sleep(1000);
            expect(element(by.xpath('//md-content[@id="tab_Billing"]//*[@id="addressLine"]')).
            getAttribute('value')).toBe('AddrLine');
            expect(element(by.xpath('//md-content[@id="tab_Billing"]//*[@id="city"]')).
            getAttribute('value')).toBe('Bangalore');
            expect(element(by.xpath('//md-content[@id="tab_Billing"]//*[@id="state"]')).
            getAttribute('value')).toBe('bangalore');
            expect(element(by.xpath('//md-content[@id="tab_Billing"]//*[@id="country"]')).
            getAttribute('value')).toBe('India');
            expect(element(by.xpath('//md-content[@id="tab_Billing"]//*[@id="pinCode"]')).
            getAttribute('value')).toBe('560037');

            element(by.xpath('//form[@name=\'editContactForm\']/*//md-tab-item[text()=\'Shipping\']')).click();
            browser.sleep(1000);
            expect(element(by.xpath('//md-content[@id="tab_Shipping"]//*[@id="addressLine"]')).
            getAttribute('value')).toBe('AddrLine1');
            expect(element(by.xpath('//md-content[@id="tab_Shipping"]//*[@id="city"]')).
            getAttribute('value')).toBe('Bangalore1');
            expect(element(by.xpath('//md-content[@id="tab_Shipping"]//*[@id="state"]')).
            getAttribute('value')).toBe('karnataka');
            expect(element(by.xpath('//md-content[@id="tab_Shipping"]//*[@id="country"]')).
            getAttribute('value')).toBe('India');
            expect(element(by.xpath('//md-content[@id="tab_Shipping"]//*[@id="pinCode"]')).
            getAttribute('value')).toBe('560037');
            browser.driver.executeScript("arguments[0].scrollIntoView(true);", element(by.css
            ('button[aria-label="close dialog"')).getWebElement());
            element(by.css('form[name="editContactForm"] button[aria-label="close dialog"]')).click();
            browser.sleep(500);
            contactsCommon.deleteContacts('Customers');
        });


        afterEach(function () {
            //Contacts.remove().exec();

            user.logout();
        });
    });
});
