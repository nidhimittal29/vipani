'use strict';

var config = browser.params;

describe('Edit Contact View', function() {
    var editContactPage, testDataContact, createContactPage;
    var loadPage = function () {
        browser.manage().deleteAllCookies();
        browser.get('http://localhost:3000/#!/signin');

    };

    describe('with local auth', function () {
        beforeEach(function () {
            loadPage();
            testDataContact = require('./../contact.testdata');
            createContactPage = require('../createcontact/createtestcontact.po');
            editContactPage = require('../editcontact/editContact.po');
            var loginPage = require('./../../account/signin/signin.po');
            var loginDetais = require('./../../account/signup/signup.testdata');
            loginPage.signin(loginDetais[10]);
            loginPage.form.submit.click();
            browser.sleep('2000');
            browser.findElement(by.id('nav-contacts')).click();
            browser.sleep('5000');

        });

        afterEach(function () {
            //browser.driver.findElement(by.xpath('//div/md-dialog/span')).click()
            browser.driver.findElement(by.css('.nvc-padding-0')).click();
            browser.findElement(by.css('.fa.fa-fw.fa-sign-out.bb-f20.ng-scope.material-icons')).click();
        });
        //TEST CASE TO CHECK LIST OF CUSTOMER CONTACTS DISPLAYED CORRECTLY OR NOT WHILE LISTING CONTACTS
        it('should login user, Checking list contact and redirecting to "/"', function () {

            for(var i=1;i<=15;i++) {
                var func = (function() {
                    var s = i;
                    return function() {

                        if(browser.findElement(by.xpath('//*[@class="ng-scope md-active md-no-scroll"]//*[@id="contactTable"]/tbody/tr[1]')).isDisplayed()) {
                            browser.findElement(by.css('.md-primary.md-raised.nvc-shape-pill.nvc-margin-right-0.md-button.md-ink-ripple')).click();
                            browser.findElement(by.xpath('//div[@class="md-open-menu-container md-whiteframe-z2 md-active md-clickable"]' +
                                '//md-icon[@class="fa fa-plus ng-scope material-icons"]')).click();
                        }else {
                            browser.findElement(by.xpath('//*[@class="ng-scope md-active md-no-scroll"]//*[@id="create-customer"]')).click();
                        }
                        browser.sleep('2000');
                        expect(browser.findElement(by.css(".nv-text-center.nv-text-bold")).getText()).toBe('Create Contact');
                        createContactPage.form.displayName.sendKeys(testDataContact.listContactDetails[s-1].displayName);
                        createContactPage.form.phoneNumber.sendKeys(testDataContact.listContactDetails[s-1].phoneNumber);
                        createContactPage.form.email.sendKeys(testDataContact.listContactDetails[s-1].email);
                        createContactPage.form.submit.click();
                        browser.sleep(2000);
                        expect(browser.findElement(by.xpath('//*[@class="ng-scope md-active md-no-scroll"]//*[@id="contactTable"]/tbody/tr[1]/td[3]')).getText()).toBe(testDataContact.listContactDetails[s-1].displayName);
                        expect(browser.findElement(by.xpath('//*[@class="ng-scope md-active md-no-scroll"]//*[@id="contactTable"]/tbody/tr[1]/td[5]')).getText()).toBe(testDataContact.listContactDetails[s-1].phoneNumber);
                        expect(browser.findElement(by.xpath('//*[@class="ng-scope md-active md-no-scroll"]//*[@id="contactTable"]/tbody/tr[1]/td[4]')).getText()).toBe(testDataContact.listContactDetails[s-1].email);

                    }
                })();
                func();
            }
        });

        //TEST CASE TO CHECK LOADING CUSTOMER CONTACTS WITH PAGINATION
        it('should login user, Checking list contact and redirecting to "/"', function () {
            if (browser.findElement(by.xpath('//*[@id="contactTable"]/tbody/tr[1]')).isDisplayed()) {

                for (var i = 1, j = 15; i <= 15; i++, j--) {
                    var func = (function() {
                    var q = p,r=j;
                    return function() {
                        expect(browser.findElement(by.xpath('//!*[@id="contactTable"]/tbody/tr[q]/td[3]')).getText()).toBe(testDataContact.listContactDetails[r].displayName);
                        expect(browser.findElement(by.xpath('//!*[@id="contactTable"]/tbody/tr[q]/td[5]')).getText()).toBe(testDataContact.listContactDetails[r].phoneNumber);
                        expect(browser.findElement(by.xpath('//!*[@id="contactTable"]/tbody/tr[q]/td[4]')).getText()).toBe(testDataContact.listContactDetails[r].email);
                        browser.driver.executeScript("arguments[0].scrollIntoView(true);", element(by.xpath('//!*[@id="contactTable"]/tbody/tr[i+1]/td[3]')).getWebElement());

                    }
                })();
                func();
                }
            }
        });

        //TEST CASE TO CHECK DELETION IS WORKING OR NOT FOR CUSTOMER CONTACT
        it('should login user, Checking list contact and redirecting to "/"', function () {
            if (browser.findElement(by.xpath('//*[@id="contactTable"]/tbody/tr[1]')).isDisplayed()) {
                var contactName = browser.findElement(by.xpath('//*[@id="contactTable"]/tbody/tr[1]/td[3]')).getText();
                var contactEmail = browser.findElement(by.xpath('//*[@id="contactTable"]/tbody/tr[1]/td[4]')).getText();
                var contactPhone = browser.findElement(by.xpath('//*[@id="contactTable"]/tbody/tr[1]/td[5]')).getText();

                browser.findElement(by.xpath('//*[@id="contactTable"]/tbody/tr[1]/td[1]/md-checkbox')).click();
                browser.driver.findElement(by.css('//*[@class="ng-scope md-no-scroll md-active"]//*[@class="md-primary md-raised nvc-shape-pill md-button md-ink-ripple"]')).click();
                browser.findElement(by.xpath('//div[@class="md-open-menu-container md-whiteframe-z2 md-active md-clickable"]' +
                    '//md-icon[@class="fa fa-trash ng-scope material-icons"]')).click();
                expect(browser.findElement(by.xpath('//*[@id="contactTable"]/tbody/tr[1]/td[3]')).getText()).not.toEqual(contactName);
                expect(browser.findElement(by.xpath('//*[@id="contactTable"]/tbody/tr[1]/td[4]')).getText()).not.toEqual(contactEmail);
                expect(browser.findElement(by.xpath('//*[@id="contactTable"]/tbody/tr[1]/td[5]')).getText()).not.toEqual(contactPhone);
            }
        });

        //TEST CASE TO CHECK ENABLE AND DISABLE OF CONTACT IS WORKING OR NOT FOR CUSTOMER CONTACT
        it('should login user, Checking list contact and redirecting to "/"', function () {
            if (browser.findElement(by.xpath('//*[@id="contactTable"]/tbody/tr[1]')).isDisplayed()) {

                //check disable
                browser.findElement(by.xpath('//*[@id="contactTable"]/tbody/tr[1]/td[1]/md-checkbox')).click();
                browser.driver.findElement(by.css('//*[@class="ng-scope md-no-scroll md-active"]//*[@class="md-primary md-raised nvc-shape-pill md-button md-ink-ripple"]')).click();
                browser.findElement(by.xpath('//div[@class="md-open-menu-container md-whiteframe-z2 md-active md-clickable"]' +
                    '//md-icon[@class="fa fa-ban ng-scope material-icons"]')).click();
                expect(browser.findElement(by.xpath('//*[@id="contactTable"]/tbody/tr[1]')).isEnabled()).toBe(false);
                //check enable
                browser.findElement(by.xpath('//*[@id="contactTable"]/tbody/tr[1]/td[1]/md-checkbox')).click();
                browser.driver.findElement(by.css('//*[@class="ng-scope md-no-scroll md-active"]//*[@class="md-primary md-raised nvc-shape-pill md-button md-ink-ripple"]')).click();
                browser.findElement(by.xpath('//div[@class="md-open-menu-container md-whiteframe-z2 md-active md-clickable"]' +
                    '//md-icon[@class="fa fa-check ng-scope material-icons"]')).click();
                expect(browser.findElement(by.xpath('//*[@id="contactTable"]/tbody/tr[1]')).isEnabled()).toBe(true);
            }
        });

        //TEST CASE TO CHECK SEARCH IS WORKING OR NOT FOR CUSTOMER CONTACT
        it('should login user, Checking list contact and redirecting to "/"', function () {
            if (browser.findElement(by.xpath('//*[@id="contactTable"]/tbody/tr[1]')).isDisplayed()) {
                var contactName = browser.findElement(by.xpath('//*[@id="contactTable"]/tbody/tr[1]/td[3]')).getText();
                var contactEmail = browser.findElement(by.xpath('//*[@id="contactTable"]/tbody/tr[1]/td[4]')).getText();
                var contactPhone = browser.findElement(by.xpath('//*[@id="contactTable"]/tbody/tr[1]/td[5]')).getText();
                browser.findElement(by.xpath('//*[@class="ng-scope md-active md-no-scroll"]//*[@ng-model="searchText"]')).sendKeys(contactName);

                expect(browser.findElement(by.xpath('//*[@id="contactTable"]/tbody/tr[1]/td[3]')).getText()).not.toEqual(contactName);
                expect(browser.findElement(by.xpath('//*[@id="contactTable"]/tbody/tr[1]/td[4]')).getText()).not.toEqual(contactEmail);
                expect(browser.findElement(by.xpath('//*[@id="contactTable"]/tbody/tr[1]/td[5]')).getText()).not.toEqual(contactPhone);
            }
        });

        //TEST CASE TO CHECK LIST OF SUPPLIER CONTACTS DISPLAYED CORRECTLY OR NOT WHILE LISTING CONTACTS
        it('should login user, Checking list contact and redirecting to "/"', function () {
            browser.findElement(by.xpath('//*[@id="contact-tabs"]//md-tab-item[2]')).click();
            for(var i=1;i<=15;i++){
                if (browser.findElement(by.xpath('//*[@id="contactTable"]/tbody/tr[1]')).isDisplayed()) {
                    browser.driver.findElement(by.xpath('//*[@class="ng-scope md-active md-no-scroll"]//*[@class="md-primary md-raised nvc-shape-pill md-button md-ink-ripple"]')).click();
                    browser.findElement(by.xpath('//div[@class="md-open-menu-container md-whiteframe-z2 md-active md-clickable"]' +
                        '//md-icon[@class="fa fa-plus ng-scope material-icons"]')).click();
                }else{
                    browser.findElement(by.xpath('//*[@class="ng-scope md-active md-no-scroll"]//*[@id="create-supplier"]')).click();
                }
                expect(browser.findElement(by.css(".nv-text-center.nv-text-bold")).getText()).toBe('Create Contact');
                createContactPage.form.displayName.sendKeys(testDataContact.listContactDetails[i-1].displayName);
                browser.findElement(by.id('step-2')).click();
                browser.sleep('2000')
                createContactPage.form.phoneNumber.sendKeys(testDataContact.listContactDetails[i-1].phoneNumber);
                createContactPage.form.email.sendKeys(testDataContact.listContactDetails[i-1].email);
                createContactPage.form.submit.click();
                browser.sleep(2000);
            }
            for(var i=1,j=15;i<=5;i++,j--){
                expect(browser.findElement(by.xpath('//!*[@id="contactTable"]/tbody/tr[i]/td[3]')).getText()).toBe(testDataContact.listContactDetails[j].displayName);
                expect(browser.findElement(by.xpath('//!*[@id="contactTable"]/tbody/tr[i]/td[5]')).getText()).toBe(testDataContact.listContactDetails[j].phoneNumber);
                expect(browser.findElement(by.xpath('//!*[@id="contactTable"]/tbody/tr[1]/td[4]')).getText()).toBe(testDataContact.listContactDetails[j].email);
                browser.driver.executeScript("arguments[0].scrollIntoView(true);", element(by.xpath('//!*[@id="contactTable"]/tbody/tr[i+1]/td[3]')).getWebElement());

            }
        });

        //TEST CASE TO CHECK LOADING SUPPLIER CONTACTS WITH PAGINATION
        it('should login user, Checking list contact and redirecting to "/"', function () {
            browser.findElement(by.xpath('//*[@id="contact-tabs"]//md-tab-item[2]')).click();
            if (browser.findElement(by.xpath('//*[@id="contactTable"]/tbody/tr[1]')).isDisplayed()) {
                for(var i=1,j=15;i<=15;i++,j--) {
                    expect(browser.findElement(by.xpath('//!*[@id="contactTable"]/tbody/tr[i]/td[3]')).getText()).toBe(testDataContact.listContactDetails[j].displayName);
                    expect(browser.findElement(by.xpath('//!*[@id="contactTable"]/tbody/tr[i]/td[5]')).getText()).toBe(testDataContact.listContactDetails[j].phoneNumber);
                    expect(browser.findElement(by.xpath('//!*[@id="contactTable"]/tbody/tr[i]/td[4]')).getText()).toBe(testDataContact.listContactDetails[j].email);
                    browser.driver.executeScript("arguments[0].scrollIntoView(true);", element(by.xpath('//!*[@id="contactTable"]/tbody/tr[i+1]/td[3]')).getWebElement());
                }
            }
        });

        //TEST CASE TO CHECK DELETION IS WORKING OR NOT FOR SUPPLIER CONTACT
        it('should login user, Checking list contact and redirecting to "/"', function () {
            browser.findElement(by.xpath('//*[@id="contact-tabs"]//md-tab-item[2]')).click();
            if (browser.findElement(by.xpath('//*[@id="contactTable"]/tbody/tr[1]')).isDisplayed()) {
                var contactName = browser.findElement(by.xpath('//*[@id="contactTable"]/tbody/tr[1]/td[3]')).getText();
                var contactEmail = browser.findElement(by.xpath('//*[@id="contactTable"]/tbody/tr[1]/td[4]')).getText();
                var contactPhone = browser.findElement(by.xpath('//*[@id="contactTable"]/tbody/tr[1]/td[5]')).getText();

                browser.findElement(by.xpath('//*[@id="contactTable"]/tbody/tr[1]/td[1]/md-checkbox')).click();
                browser.driver.findElement(by.xpath('//*[@class="ng-scope md-active md-no-scroll"]//*[@class="md-primary md-raised nvc-shape-pill md-button md-ink-ripple"]')).click();
                browser.findElement(by.xpath('//div[@class="md-open-menu-container md-whiteframe-z2 md-active md-clickable"]' +
                    '//md-icon[@class="fa fa-trash ng-scope material-icons"]')).click();
                expect(browser.findElement(by.xpath('//*[@id="contactTable"]/tbody/tr[1]/td[3]')).getText()).not.toEqual(contactName);
                expect(browser.findElement(by.xpath('//*[@id="contactTable"]/tbody/tr[1]/td[4]')).getText()).not.toEqual(contactEmail);
                expect(browser.findElement(by.xpath('//*[@id="contactTable"]/tbody/tr[1]/td[5]')).getText()).not.toEqual(contactPhone);
            }
        });

        //TEST CASE TO CHECK ENABLE AND DISABLE OF CONTACT IS WORKING OR NOT FOR SUPPLIER CONTACT
        it('should login user, Checking list contact and redirecting to "/"', function () {
            browser.findElement(by.xpath('//*[@id="contact-tabs"]//md-tab-item[2]')).click();
            if (browser.findElement(by.xpath('//*[@id="contactTable"]/tbody/tr[1]')).isDisplayed()) {
                //check disable
                browser.findElement(by.xpath('//*[@id="contactTable"]/tbody/tr[1]/td[1]/md-checkbox')).click();
                browser.driver.findElement(by.xpath('//*[@class="ng-scope md-active md-no-scroll"]//*[@class="md-primary md-raised nvc-shape-pill md-button md-ink-ripple"]')).click();
                browser.findElement(by.xpath('//div[@class="md-open-menu-container md-whiteframe-z2 md-active md-clickable"]' +
                    '//md-icon[@class="fa fa-ban ng-scope material-icons"]')).click();
                expect(browser.findElement(by.xpath('//*[@id="contactTable"]/tbody/tr[1]')).isEnabled()).toBe(false);
                //check enable
                browser.findElement(by.xpath('//*[@id="contactTable"]/tbody/tr[1]/td[1]/md-checkbox')).click();
                browser.driver.findElement(by.xpath('//*[@class="ng-scope md-active md-no-scroll"]//*[@class="md-primary md-raised nvc-shape-pill md-button md-ink-ripple"]')).click();
                browser.findElement(by.xpath('//div[@class="md-open-menu-container md-whiteframe-z2 md-active md-clickable"]' +
                    '//md-icon[@class="fa fa-check ng-scope material-icons"]')).click();
                expect(browser.findElement(by.xpath('//*[@id="contactTable"]/tbody/tr[1]')).isEnabled()).toBe(true);
            }
        });

        //TEST CASE TO CHECK SEARCH IS WORKING OR NOT FOR SUPPLIER CONTACT
        it('should login user, Checking list contact and redirecting to "/"', function () {
            browser.findElement(by.xpath('//*[@id="contact-tabs"]//md-tab-item[2]')).click();
            if (browser.findElement(by.xpath('//*[@id="contactTable"]/tbody/tr[1]')).isDisplayed()) {
                var contactName = browser.findElement(by.xpath('//*[@id="contactTable"]/tbody/tr[1]/td[3]')).getText();
                var contactEmail = browser.findElement(by.xpath('//*[@id="contactTable"]/tbody/tr[1]/td[4]')).getText();
                var contactPhone = browser.findElement(by.xpath('//*[@id="contactTable"]/tbody/tr[1]/td[5]')).getText();
                browser.findElement(by.xpath('//*[@class="ng-scope md-active md-no-scroll"]//*[@ng-model="searchText"]')).sendKeys(contactName);

                expect(browser.findElement(by.xpath('//*[@id="contactTable"]/tbody/tr[1]/td[3]')).getText()).not.toEqual(contactName);
                expect(browser.findElement(by.xpath('//*[@id="contactTable"]/tbody/tr[1]/td[4]')).getText()).not.toEqual(contactEmail);
                expect(browser.findElement(by.xpath('//*[@id="contactTable"]/tbody/tr[1]/td[5]')).getText()).not.toEqual(contactPhone);
            }
        });

    });

});
