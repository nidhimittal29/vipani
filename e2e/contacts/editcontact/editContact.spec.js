'use strict';

var config = browser.params;
var contacts=require('../../account/common.util.e2e/contacts.common.po');
var user=require('../../account/common.util.e2e/signin.common.po');
var global = require('../../account/common.util.e2e/global.common.po');
var loginDetails = require('./login');
describe('Edit Contact View', function() {
    var editContactPage, testDataContact,createContactPage;
    var loadPage = function () {
        browser.manage().deleteAllCookies();
        browser.get('http://localhost:3000/#!/signin');

    };

    describe('with local auth', function () {

        beforeAll(function(){
            // global.loadPage('http://localhost:3000/#!/register');
            // user.registerProcess(loginDetails[0]);
        });
        beforeEach(function () {
            loadPage();
            testDataContact = require('./../contact.testdata');
            createContactPage=require('../createcontact/createtestcontact.po');
            editContactPage = require('./editContact.po');
            user.login(loginDetails[0]);
            contacts.initiateContacts();
            browser.sleep('2000');
        });

        afterEach(function () {
            user.logout();
        });

        //1.TEST CASE TO CHECK CUSTOMER CONTACT DETAILS ARE DISPLAYED CORRECTLY OR NOT WHILE EDIT CONTACT
        it('should login user, Checking Edit contact and redirecting to "/"', function () {
            contacts.initiateCreateContact('Customers');
            createContactPage.form.displayName.sendKeys(testDataContact.createContactDetails[4].displayName);
            createContactPage.form.element(by.xpath('//*[@aria-label="Show More Or Less Link"]')).click();

            createContactPage.form.firstName.sendKeys(testDataContact.createContactDetails[4].firstName);
            createContactPage.form.middleName.sendKeys(testDataContact.createContactDetails[4].middleName);
            createContactPage.form.lastName.sendKeys(testDataContact.createContactDetails[4].lastName);
            createContactPage.form.companyName.sendKeys(testDataContact.createContactDetails[4].companyName);
            createContactPage.form.gstinNumber.sendKeys(testDataContact.createContactDetails[4].gstinNumber);

            browser.findElement(by.xpath('//*[contains(text(),"Add Communication Info")]')).click();
            createContactPage.form.phoneNumber.sendKeys(testDataContact.createContactDetails[4].phoneNumber);
            browser.driver.executeScript("arguments[0].scrollIntoView(true);", element(by.xpath('//div[@id=\'email-0\']//*[@ng-model=\'email.email\']')).getWebElement());
            createContactPage.form.email.sendKeys(testDataContact.createContactDetails[4].email);
            createContactPage.form.submit.click();
            browser.sleep(3000);
            expect(browser.findElement(by.xpath('//md-content[@id="contact_Customers"]/view-contacts/table/tbody/tr/td[3]')).getText()).toBe(testDataContact.createContactDetails[4].displayName);
            expect(browser.findElement(by.xpath('//md-content[@id="contact_Customers"]/view-contacts/table/tbody/tr/td[5]')).getText()).toBe(testDataContact.createContactDetails[4].phoneNumber);
            expect(browser.findElement(by.xpath('//md-content[@id="contact_Customers"]/view-contacts/table/tbody/tr/td[4]')).getText()).toBe(testDataContact.createContactDetails[4].email);

            browser.findElement(by.xpath('//md-content[@id="contact_Customers"]/view-contacts/table/tbody/tr/td[3]')).click();
            browser.sleep(2000);
            expect(browser.findElement(by.xpath('//form[@name="editContactForm"]//*[contains(text(),"Contact Details")]')).isDisplayed()).toBe(true);
            expect(browser.findElement(by.xpath('//label[contains(text(),'+testDataContact.createContactDetails[4].displayName+')]')).isPresent()).toBe(true);
            expect(browser.findElement(by.xpath('//label[contains(text(),Customer)]')).isPresent()).toBe(true);
            expect(browser.findElement(by.xpath('//label[contains(text(),Active)]')).isPresent()).toBe(true);

            browser.driver.executeScript("arguments[0].scrollIntoView(true);", element(by.xpath
            ('//form[@name=\'editContactForm\']//*[@id=\'contact_phone\']//*[@data-ng-model=\'phone.phoneNumber\']')).getWebElement());
            browser.sleep(1000);
            expect(browser.findElement(by.xpath('//form[@name=\'editContactForm\']//*[@id=\'contact_phone\']//*[@data-ng-model=\'phone.phoneNumber\']')).
            getAttribute('value')).toBe(testDataContact.createContactDetails[4].phoneNumber);
            browser.findElement(by.xpath('//form[@name=\'editContactForm\']//md-tabs-canvas//*[contains(text(),\'Email\')]')).click();
            browser.sleep(1000);
            expect(browser.findElement(by.xpath('//form[@name=\'editContactForm\']//*[@id=\'contact_email\']//*[@ng-model=\'email.email\']')).
            getAttribute('value')).toBe(testDataContact.createContactDetails[4].email);

            browser.findElement(by.css('form[name="editContactForm"] button[aria-label="close dialog"]')).click();
        });

        //2.TEST CASE TO CHECK SUPPLIER CONTACT DETAILS ARE DISPLAYED CORRECTLY OR NOT WHILE EDIT CONTACT
        it('should login user, Checking Edit contact and redirecting to "/"', function () {
            contacts.initiateCreateContact('Suppliers');
            createContactPage.form.displayName.sendKeys(testDataContact.createContactDetails[5].displayName);
            createContactPage.form.element(by.xpath('//*[@aria-label="Show More Or Less Link"]')).click();

            createContactPage.form.firstName.sendKeys(testDataContact.createContactDetails[5].firstName);
            createContactPage.form.middleName.sendKeys(testDataContact.createContactDetails[5].middleName);
            createContactPage.form.lastName.sendKeys(testDataContact.createContactDetails[5].lastName);
            createContactPage.form.companyName.sendKeys(testDataContact.createContactDetails[5].companyName);
            createContactPage.form.gstinNumber.sendKeys(testDataContact.createContactDetails[5].gstinNumber);

            browser.findElement(by.xpath('//*[contains(text(),"Add Communication Info")]')).click();
            createContactPage.form.phoneNumber.sendKeys(testDataContact.createContactDetails[5].phoneNumber);
            browser.driver.executeScript("arguments[0].scrollIntoView(true);", element(by.xpath('//div[@id=\'email-0\']//*[@ng-model=\'email.email\']')).getWebElement());
            createContactPage.form.email.sendKeys(testDataContact.createContactDetails[5].email);
            createContactPage.form.submit.click();
            browser.sleep(3000);
            expect(browser.findElement(by.xpath('//md-content[@id="contact_Suppliers"]/view-contacts/table/tbody/tr/td[3]')).getText()).toBe(testDataContact.createContactDetails[5].displayName);
            expect(browser.findElement(by.xpath('//md-content[@id="contact_Suppliers"]/view-contacts/table/tbody/tr/td[5]')).getText()).toBe(testDataContact.createContactDetails[5].phoneNumber);
            expect(browser.findElement(by.xpath('//md-content[@id="contact_Suppliers"]/view-contacts/table/tbody/tr/td[4]')).getText()).toBe(testDataContact.createContactDetails[5].email);

            browser.findElement(by.xpath('//md-content[@id="contact_Suppliers"]/view-contacts/table/tbody/tr/td[3]')).click();
            browser.sleep(2000);
            expect(browser.findElement(by.xpath('//form[@name="editContactForm"]//*[contains(text(),"Contact Details")]')).isDisplayed()).toBe(true);
            expect(browser.findElement(by.xpath('//label[contains(text(),'+testDataContact.createContactDetails[5].displayName+')]')).isPresent()).toBe(true);
            expect(browser.findElement(by.xpath('//label[contains(text(),Supplier)]')).isPresent()).toBe(true);
            expect(browser.findElement(by.xpath('//label[contains(text(),Active)]')).isPresent()).toBe(true);

            browser.driver.executeScript("arguments[0].scrollIntoView(true);", element(by.xpath
            ('//form[@name=\'editContactForm\']//*[@id=\'contact_phone\']//*[@data-ng-model=\'phone.phoneNumber\']')).getWebElement());
            browser.sleep(1000);
            expect(browser.findElement(by.xpath('//form[@name=\'editContactForm\']//*[@id=\'contact_phone\']//*[@data-ng-model=\'phone.phoneNumber\']')).
            getAttribute('value')).toBe(testDataContact.createContactDetails[5].phoneNumber);
            browser.findElement(by.xpath('//form[@name=\'editContactForm\']//md-tabs-canvas//*[contains(text(),\'Email\')]')).click();
            browser.sleep(1000);
            expect(browser.findElement(by.xpath('//form[@name=\'editContactForm\']//*[@id=\'contact_email\']//*[@ng-model=\'email.email\']')).
            getAttribute('value')).toBe(testDataContact.createContactDetails[5].email);

            browser.findElement(by.css('form[name="editContactForm"] button[aria-label="close dialog"]')).click();
        });

         //3.TEST CASE TO UPDATE CUSTOMER CONTACT WITH ALL THE FIELDS
        it('should login user, Checking Edit contact and redirecting to "/"', function () {
            browser.findElement(by.xpath('//md-content[@id="contact_Customers"]/view-contacts/table/tbody/tr/td[3]')).click();
            browser.sleep(2000);
            expect(browser.findElement(by.xpath('//form[@name="editContactForm"]//*[contains(text(),"Contact Details")]')).isDisplayed()).toBe(true);
            browser.findElement(by.xpath('//form[@name=\'editContactForm\']//button[@aria-label="edit button"]/md-icon')).click();
            editContactPage.form.displayName.clear().sendKeys(testDataContact.createContactDetails[17].displayName);
            editContactPage.form.contactType.click();
            browser.findElement(by.xpath('//div[@class="md-select-menu-container md-active md-clickable"]//*[contains(text(),"'+testDataContact.createContactDetails[17].contactType+'")]')).click();
            editContactPage.form.element(by.xpath('//*[@aria-label="Show More Or Less Link"]')).click();
            editContactPage.form.gstinNumber.clear().sendKeys(testDataContact.createContactDetails[17].gstinNumber);
            editContactPage.form.companyName.clear().sendKeys(testDataContact.createContactDetails[17].companyName);
            editContactPage.form.firstName.sendKeys(testDataContact.createContactDetails[17].firstName);
            editContactPage.form.middleName.sendKeys(testDataContact.createContactDetails[17].middleName);
            editContactPage.form.lastName.sendKeys(testDataContact.createContactDetails[17].lastName);
            browser.driver.executeScript("arguments[0].scrollIntoView(true);", element(by.xpath('//form[@name=\'editContactForm\']//div[@id=\'mobile-0\']//*[@data-ng-model=\'phone.phoneNumber\']')).getWebElement());
            browser.findElement(by.xpath('//form[@name=\'editContactForm\']//div[@id=\'mobile-0\']//*[@data-ng-model=\'phone.phoneNumber\']')).clear().sendKeys(testDataContact.createContactDetails[17].phoneNumber);
            browser.findElement(by.xpath('//form[@name=\'editContactForm\']//md-tabs-canvas//*[contains(text(),\'Email\')]')).click();
            browser.findElement(by.xpath('//form[@name=\'editContactForm\']//div[@id=\'email-0\']//*[@ng-model=\'email.email\']')).clear().sendKeys(testDataContact.createContactDetails[17].email);
            browser.findElement(by.xpath('//form[@name=\'editContactForm\']//md-content[@id=\'tab_Billing\']//*[@aria-label=\'Add Address\']')).click();
            browser.driver.executeScript("arguments[0].scrollIntoView(true);", element(by.xpath('//form[@name=\'editContactForm\']//md-content[@id=\'tab_Billing\']//div[@id=\'address-0\']//*[@id="addressLine"]')).getWebElement());
            browser.findElement(by.xpath('//form[@name=\'editContactForm\']//md-content[@id=\'tab_Billing\']//div[@id=\'address-0\']//*[@id="addressLine"]')).clear().sendKeys(testDataContact.createContactDetails[17].addressLine);
            browser.findElement(by.xpath('//form[@name=\'editContactForm\']//md-content[@id=\'tab_Billing\']//div[@id=\'address-0\']//*[@id="city"]')).clear().sendKeys(testDataContact.createContactDetails[17].city);
            browser.findElement(by.xpath('//form[@name=\'editContactForm\']//md-content[@id=\'tab_Billing\']//div[@id=\'address-0\']//*[@id="state"]')).clear().sendKeys(testDataContact.createContactDetails[17].state);
            browser.findElement(by.xpath('//form[@name=\'editContactForm\']//md-content[@id=\'tab_Billing\']//div[@id=\'address-0\']//*[@id="country"]')).clear().sendKeys(testDataContact.createContactDetails[17].country);
            browser.findElement(by.xpath('//form[@name=\'editContactForm\']//md-content[@id=\'tab_Billing\']//div[@id=\'address-0\']//*[@id="pinCode"]')).clear().sendKeys(testDataContact.createContactDetails[17].pinCode);
            browser.driver.executeScript("arguments[0].scrollIntoView(true);", element(by.xpath('//form[@name=\'editContactForm\']//button[@aria-label=\'Update\']')).getWebElement());
            browser.findElement(by.xpath('//button[@aria-label=\'Update\']')).click();
            browser.sleep(500);


        });
        //4.TEST CASE TO UPDATE SUPPLIER CONTACT WITH ALL THE FIELDS
        it('should login user, Checking Edit contact and redirecting to "/"', function () {
            element(by.xpath('//*[@id="contact-tabs"]/md-tabs/*//md-tab-item[2]')).click();
            element(by.xpath('//md-content[@id="contact_Suppliers"]/view-contacts/table/tbody/tr/td[3]')).click();
            browser.sleep(2000);
            expect(element(by.xpath('//form[@name="editContactForm"]//*[contains(text(),"Contact Details")]')).
            isDisplayed()).toBe(true);
            element(by.xpath('//form[@name=\'editContactForm\']//button[@aria-label="edit button"]/md-icon')).click();
            editContactPage.form.displayName.clear();
            editContactPage.form.displayName.sendKeys(testDataContact.createContactDetails[17].displayName);
            editContactPage.form.contactType.click();
            //editContactPage.form.element(by.xpath('//*[@aria-label="Show More Or Less Link"]')).click();
            element(by.xpath('//div[@class="md-select-menu-container md-active md-clickable"]//*[contains(text(),"'+testDataContact.createContactDetails[17].contactType+'")]')).click();
            editContactPage.form.gstinNumber.clear();
            editContactPage.form.gstinNumber.sendKeys(testDataContact.createContactDetails[17].gstinNumber);
            editContactPage.form.companyName.clear();
            editContactPage.form.companyName.sendKeys(testDataContact.createContactDetails[17].companyName);
            //editContactPage.form.element(by.model('contact.paymentTerm._id')).click();
            //element(by.xpath('//div[@class="md-select-menu-container md-active md-clickable"]//*[contains(text(),"'+testDataContact.createContactDetails[17].paymentterms+'")]')).click();

            editContactPage.form.firstName.sendKeys(testDataContact.createContactDetails[17].firstName);
            editContactPage.form.middleName.sendKeys(testDataContact.createContactDetails[17].middleName);
            editContactPage.form.lastName.sendKeys(testDataContact.createContactDetails[17].lastName);
            browser.driver.executeScript("arguments[0].scrollIntoView(true);", element(by.xpath('//form[@name=\'editContactForm\']//div[@id=\'mobile-0\']//*[@data-ng-model=\'phone.phoneNumber\']')).getWebElement());
            element(by.xpath('//form[@name=\'editContactForm\']//div[@id=\'mobile-0\']//*[@data-ng-model=\'phone.phoneNumber\']')).clear().sendKeys(testDataContact.createContactDetails[17].phoneNumber);
            element(by.xpath('//form[@name=\'editContactForm\']//md-tabs-canvas//*[contains(text(),\'Email\')]')).click();
            element(by.xpath('//form[@name=\'editContactForm\']//div[@id=\'email-0\']//*[@ng-model=\'email.email\']')).clear().sendKeys(testDataContact.createContactDetails[17].email);
            element(by.xpath('//form[@name=\'editContactForm\']//md-content[@id=\'tab_Billing\']//*[@aria-label=\'Add Address\']')).click();
            browser.driver.executeScript("arguments[0].scrollIntoView(true);", element(by.xpath('//form[@name=\'editContactForm\']//md-content[@id=\'tab_Billing\']//div[@id=\'address-0\']//*[@id="addressLine"]')).getWebElement());
            element(by.xpath('//form[@name=\'editContactForm\']//md-content[@id=\'tab_Billing\']//div[@id=\'address-0\']//*[@id="addressLine"]')).clear().sendKeys(testDataContact.createContactDetails[17].addressLine);
            element(by.xpath('//form[@name=\'editContactForm\']//md-content[@id=\'tab_Billing\']//div[@id=\'address-0\']//*[@id="city"]')).clear().sendKeys(testDataContact.createContactDetails[17].city);
            element(by.xpath('//form[@name=\'editContactForm\']//md-content[@id=\'tab_Billing\']//div[@id=\'address-0\']//*[@id="state"]')).clear().sendKeys(testDataContact.createContactDetails[17].state);
            element(by.xpath('//form[@name=\'editContactForm\']//md-content[@id=\'tab_Billing\']//div[@id=\'address-0\']//*[@id="country"]')).clear().sendKeys(testDataContact.createContactDetails[17].country);
            element(by.xpath('//form[@name=\'editContactForm\']//md-content[@id=\'tab_Billing\']//div[@id=\'address-0\']//*[@id="pinCode"]')).clear().sendKeys(testDataContact.createContactDetails[17].pinCode);
            browser.driver.executeScript("arguments[0].scrollIntoView(true);", element(by.xpath('//form[@name=\'editContactForm\']//button[@aria-label=\'Update\']')).getWebElement());
            element(by.xpath('//button[@aria-label=\'Update\']')).click();
            browser.sleep(500);
        });

        //5.TEST CASE TO VALIDATE PHONE NUMBER WHILE EDIT CONTACT
        it('should login user, Checking Edit contact and redirecting to "/"', function () {
            element(by.xpath(' //md-content[@id="contact_Customers"]/view-contacts/table/tbody/tr/td[3]')).click();
            browser.sleep(2000);
            expect(element(by.xpath('//form[@name="editContactForm"]//*[contains(text(),"Contact Details")]')).
            isDisplayed()).toBe(true);
            browser.driver.executeScript("arguments[0].scrollIntoView(true);", element(by.xpath('//form[@name=\'editContactForm\']//div[@id=\'mobile-0\']//*[@data-ng-model=\'phone.phoneNumber\']')).getWebElement());
            element(by.xpath('//form[@name=\'editContactForm\']//div[@id=\'mobile-0\']//*[@data-ng-model=\'phone.phoneNumber\']')).
            clear().sendKeys('224784554');
            expect(editContactPage.form.submit.isEnabled()).toBe(true);
            editContactPage.form.submit.click();
            expect(editContactPage.form.element(by.xpath('//*[@id="mobile-0"]/div[2]/div[2]/md-input-container/i')).getAttribute('data')).toBe('Invalid Input..');
            element(by.css('form[name="editContactForm"] button[aria-label="close dialog"]')).click();
        });

        //6.TEST CASE TO CHECK PHONE NUMBER IS EMPTY WHILE EDIT CONTACT
        it('should login user, Checking Edit contact and redirecting to "/"', function () {
            element(by.xpath("//md-content[@id=\'contact_Customers\']/view-contacts/table/tbody/tr[1]/td[3]")).click();
            browser.sleep(2000);
            expect(element(by.xpath('//form[@name="editContactForm"]//*[contains(text(),"Contact Details")]')).isDisplayed()).toBe(true);
            browser.driver.executeScript("arguments[0].scrollIntoView(true);", element(by.xpath('//form[@name=\'editContactForm\']//div[@id=\'mobile-0\']//*[@data-ng-model=\'phone.phoneNumber\']')).getWebElement());
            element(by.xpath('//form[@name=\'editContactForm\']//div[@id=\'mobile-0\']//*[@data-ng-model=\'phone.phoneNumber\']')).clear();
            expect(editContactPage.form.submit.isEnabled()).toBe(true);
            editContactPage.form.submit.click();
            expect(editContactPage.form.element(by.xpath('//*[@id="mobile-0"]/div[2]/div[2]/md-input-container/i')).getAttribute('data')).toBe('Invalid Input..');
            element(by.css('form[name="editContactForm"] button[aria-label="close dialog"]')).click();
        });

        //7.TEST CASE TO VALIDATE EMAIL ID WHILE EDIT CONTACT
        it('should login user, Checking Edit contact and redirecting to "/"', function () {
            element(by.xpath("//md-content[@id=\'contact_Customers\']/view-contacts/table/tbody/tr[1]/td[3]")).click();
            browser.sleep(2000);
            expect(element(by.xpath('//form[@name="editContactForm"]//*[contains(text(),"Contact Details")]')).isDisplayed()).toBe(true);
            browser.driver.executeScript("arguments[0].scrollIntoView(true);", element(by.xpath('//form[@name=\'editContactForm\']//div[@id=\'mobile-0\']//*[@data-ng-model=\'phone.phoneNumber\']')).getWebElement());
            element(by.xpath('//form[@name=\'editContactForm\']//md-tabs-canvas//*[contains(text(),\'Email\')]')).click();
            element(by.xpath('//form[@name=\'editContactForm\']//div[@id=\'email-0\']//*[@ng-model=\'email.email\']')).
            sendKeys('dsadfsfasd');
            expect(editContactPage.form.submit.isEnabled()).toBe(true);
            editContactPage.form.submit.click();
            expect(editContactPage.form.element(by.xpath('//*[@id="email-0"]/div[2]/div[2]/md-input-container/i')).getAttribute('data')).toBe('Invalid Input..');
            element(by.css('form[name="editContactForm"] button[aria-label="close dialog"]')).click();
        });

        //8.TEST CASE TO VALIDATE PINCODE WHILE EDIT CONTACT
        it('should login user, Checking Edit contact and redirecting to "/"', function () {
            element(by.xpath("//md-content[@id=\'contact_Customers\']/view-contacts/table/tbody/tr[1]/td[3]")).click();
            browser.sleep(2000);
            expect(element(by.xpath('//form[@name="editContactForm"]//*[contains(text(),"Contact Details")]')).isDisplayed()).toBe(true);
            browser.driver.executeScript("arguments[0].scrollIntoView(true);", element(by.xpath('//form[@name=\'editContactForm\']//div[@id=\'mobile-0\']//*[@data-ng-model=\'phone.phoneNumber\']')).getWebElement());
            element(by.xpath('//form[@name=\'editContactForm\']//*[@id="tab_Billing"]/div[1]/span')).click();
            browser.driver.executeScript("arguments[0].scrollIntoView(true);", element(by.xpath('//form[@name=\'editContactForm\']//md-card//md-tab-content[1]//div[@id=\'address-0\']//*[@id="pinCode"]')).getWebElement());
            element(by.xpath('//form[@name=\'editContactForm\']//md-card//md-tab-content[1]//div[@id=\'address-0\']//*[@id="addressLine"]')).
            sendKeys('5784');
            expect(element(by.xpath('//form[@name=\'editContactForm\']//md-card//md-tab-content[1]//div[@id=\'address-0\']//*[@id="pinCode"]')).getAttribute('ng-minlength')).toBe('6');
            expect(element(by.xpath('//form[@name=\'editContactForm\']//md-card//md-tab-content[1]//div[@id=\'address-0\']//*[@id="pinCode"]')).getAttribute('ng-maxlength')).toBe('6');
            expect(element(by.xpath('//form[@name=\'editContactForm\']//md-card//md-tab-content[1]//div[@id=\'address-0\']//*[@id="pinCode"]')).getAttribute('only-digits')).toBe('');
            element(by.css('form[name="editContactForm"] button[aria-label="close dialog"]')).click();
        });

        //9.TEST CASE TO ADD MORE PHONE NUMBERS AND EMAIL ID'S WHILE EDIT CONTACT
        it('should login user, Checking Edit contact and redirecting to "/"', function () {
            element(by.xpath("//md-tabs/md-tabs-content-wrapper/md-tab-content[1]/div/md-content/view-contacts/table/tbody/tr[1]/td[3]")).click();
            browser.sleep(2000);
            expect(element(by.xpath('//form[@name="editContactForm"]//*[contains(text(),"Contact Details")]')).isDisplayed()).toBe(true);
            browser.driver.executeScript("arguments[0].scrollIntoView(true);", element(by.xpath('//form[@name=\'editContactForm\']//div[@id=\'mobile-0\']//*[@data-ng-model=\'phone.phoneNumber\']')).getWebElement());
            element(by.xpath('//form[@name=\'editContactForm\']//div[@id=\'mobile-0\']//*[@data-ng-model=\'phone.phoneNumber\']'))
                .clear().sendKeys(testDataContact.createContactDetails[7].phoneNumbers[0].phoneNumber);
            element(by.xpath('//form[@name=\'editContactForm\']//md-content[@id=\'contact_phone\']/div/span')).click();
            element(by.xpath('//form[@name=\'editContactForm\']//md-content[@id=\'contact_phone\']/div/span')).click();
            browser.driver.executeScript("arguments[0].scrollIntoView(true);", element(by.xpath('//form[@name=\'editContactForm\']//div[@id=\'mobile-1\']//*[@data-ng-model=\'phone.phoneNumber\']')).getWebElement());
            element(by.xpath('//form[@name=\'editContactForm\']//div[@id=\'mobile-1\']//*[@data-ng-model=\'phone.phoneNumber\']'))
                .clear().sendKeys(testDataContact.createContactDetails[7].phoneNumbers[1].phoneNumber);
            browser.driver.executeScript("arguments[0].scrollIntoView(true);", element(by.xpath('//form[@name=\'editContactForm\']//div[@id=\'mobile-2\']//*[@data-ng-model=\'phone.phoneNumber\']')).getWebElement());
            element(by.xpath('//form[@name=\'editContactForm\']//div[@id=\'mobile-1\']//*[@data-ng-model=\'phone.phoneNumber\']'))
                .clear().sendKeys(testDataContact.createContactDetails[7].phoneNumbers[2].phoneNumber);
            element(by.xpath('//form[@name=\'editContactForm\']//md-tabs-canvas//*[contains(text(),\'Email\')]')).click();
            element(by.xpath('//form[@name=\'editContactForm\']//div[@id=\'email-0\']//*[@ng-model=\'email.email\']')).
            clear().sendKeys(testDataContact.createContactDetails[7].emails[0].email);
            element(by.xpath('//form[@name=\'editContactForm\']//md-content[@id=\'contact_email\']/div/span')).click();
            element(by.xpath('//form[@name=\'editContactForm\']//md-content[@id=\'contact_email\']/div/span')).click();
            browser.driver.executeScript("arguments[0].scrollIntoView(true);", element(by.xpath('//form[@name=\'editContactForm\']//div[@id=\'email-1\']//*[@ng-model=\'email.email\']')).getWebElement());
            element(by.xpath('//form[@name=\'editContactForm\']//div[@id=\'email-1\']//*[@ng-model=\'email.email\']')).
            clear().sendKeys(testDataContact.createContactDetails[7].emails[1].email);
            browser.driver.executeScript("arguments[0].scrollIntoView(true);", element(by.xpath('//form[@name=\'editContactForm\']//div[@id=\'email-2\']//*[@ng-model=\'email.email\']')).getWebElement());
            element(by.xpath('//form[@name=\'editContactForm\']//div[@id=\'email-2\']//*[@ng-model=\'email.email\']')).
            clear().sendKeys(testDataContact.createContactDetails[7].emails[2].email);
            expect(editContactPage.form.submit.isEnabled()).toBe(true);
            editContactPage.form.submit.click();
        });

        //10.TEST CASE TO ADD MORE BILLING ADDRESSES WHILE EDIT CONTACT
        it('should login user, Checking Edit contact and redirecting to "/"', function () {
            element(by.xpath('//md-content[@id="contact_Customers"]/view-contacts/table/tbody/tr/td[3]')).click();
            browser.sleep(2000);
            expect(element(by.xpath('//form[@name="editContactForm"]//*[contains(text(),"Contact Details")]')).isDisplayed()).toBe(true);

            browser.driver.executeScript("arguments[0].scrollIntoView(true);", element(by.xpath('//form[@name=\'editContactForm\']//div[@id=\'mobile-0\']//*[@data-ng-model=\'phone.phoneNumber\']')).getWebElement());

            element(by.xpath('//form[@name=\'editContactForm\']//md-content[@id=\'tab_Billing\']/div/span')).click();
            element(by.xpath('//form[@name=\'editContactForm\']//md-content[@id=\'tab_Billing\']/div/span')).click();
            element(by.xpath('//form[@name=\'editContactForm\']//md-content[@id=\'tab_Billing\']/div/span')).click();

            browser.driver.executeScript("arguments[0].scrollIntoView(true);", element(by.xpath('//form[@name=\'editContactForm\']//md-card//md-tab-content[1]//div[@id=\'address-0\']//*[@id="addressLine"]')).getWebElement());
            element(by.xpath('//form[@name=\'editContactForm\']//md-card//md-tab-content[1]//div[@id=\'address-0\']//*[@id="addressLine"]')).
            sendKeys(testDataContact.createContactDetails[17].addressLine);
            element(by.xpath('//form[@name=\'editContactForm\']//md-card//md-tab-content[1]//div[@id=\'address-0\']//*[@id="city"]')).
            sendKeys(testDataContact.createContactDetails[17].city);
            element(by.xpath('//form[@name=\'editContactForm\']//md-card//md-tab-content[1]//div[@id=\'address-0\']//*[@id="state"]')).
            sendKeys(testDataContact.createContactDetails[17].state);
            element(by.xpath('//form[@name=\'editContactForm\']//md-card//md-tab-content[1]//div[@id=\'address-0\']//*[@id="country"]')).
            sendKeys(testDataContact.createContactDetails[17].country);
            element(by.xpath('//form[@name=\'editContactForm\']//md-card//md-tab-content[1]//div[@id=\'address-0\']//*[@id="pinCode"]')).
            sendKeys(testDataContact.createContactDetails[17].pinCode);

            browser.driver.executeScript("arguments[0].scrollIntoView(true);", element(by.xpath('//form[@name=\'editContactForm\']//md-card//md-tab-content[1]//div[@id=\'address-1\']//*[@id="addressLine"]')).getWebElement());
            element(by.xpath('//form[@name=\'editContactForm\']//md-card//md-tab-content[1]//div[@id=\'address-1\']//*[@id="addressLine"]')).
            sendKeys(testDataContact.createContactDetails[17].addressLine);
            element(by.xpath('//form[@name=\'editContactForm\']//md-card//md-tab-content[1]//div[@id=\'address-1\']//*[@id="city"]')).
            sendKeys(testDataContact.createContactDetails[17].city);
            element(by.xpath('//form[@name=\'editContactForm\']//md-card//md-tab-content[1]//div[@id=\'address-1\']//*[@id="state"]')).
            sendKeys(testDataContact.createContactDetails[17].state);
            element(by.xpath('//form[@name=\'editContactForm\']//md-card//md-tab-content[1]//div[@id=\'address-1\']//*[@id="country"]')).
            sendKeys(testDataContact.createContactDetails[17].country);
            element(by.xpath('//form[@name=\'editContactForm\']//md-card//md-tab-content[1]//div[@id=\'address-1\']//*[@id="pinCode"]')).
            sendKeys(testDataContact.createContactDetails[17].pinCode);

            browser.driver.executeScript("arguments[0].scrollIntoView(true);", element(by.xpath('//form[@name=\'editContactForm\']//md-card//md-tab-content[1]//div[@id=\'address-2\']//*[@id="addressLine"]')).getWebElement());
            element(by.xpath('//form[@name=\'editContactForm\']//md-card//md-tab-content[1]//div[@id=\'address-2\']//*[@id="addressLine"]')).
            sendKeys(testDataContact.createContactDetails[17].addressLine);
            element(by.xpath('//form[@name=\'editContactForm\']//md-card//md-tab-content[1]//div[@id=\'address-2\']//*[@id="city"]')).
            sendKeys(testDataContact.createContactDetails[17].city);
            element(by.xpath('//form[@name=\'editContactForm\']//md-card//md-tab-content[1]//div[@id=\'address-2\']//*[@id="state"]')).
            sendKeys(testDataContact.createContactDetails[17].state);
            element(by.xpath('//form[@name=\'editContactForm\']//md-card//md-tab-content[1]//div[@id=\'address-2\']//*[@id="country"]')).
            sendKeys(testDataContact.createContactDetails[17].country);
            element(by.xpath('//form[@name=\'editContactForm\']//md-card//md-tab-content[1]//div[@id=\'address-2\']//*[@id="pinCode"]')).
            sendKeys(testDataContact.createContactDetails[17].pinCode);
            editContactPage.form.submit.click();
        });

        //11.TEST CASE TO ADD MORE SHIPPING ADDRESSES WHILE EDIT CONTACT
        it('should login user, Checking Edit contact and redirecting to "/"', function () {
            element(by.xpath('//md-content[@id="contact_Customers"]/view-contacts/table/tbody/tr/td[3]')).click();
            browser.sleep(2000);
            expect(element(by.xpath('//form[@name="editContactForm"]//*[contains(text(),"Contact Details")]')).isDisplayed()).toBe(true);

            browser.driver.executeScript("arguments[0].scrollIntoView(true);", element(by.xpath('//form[@name=\'editContactForm\']//div[@id=\'mobile-0\']//*[@data-ng-model=\'phone.phoneNumber\']')).getWebElement());
            element(by.xpath('//form[@name=\'editContactForm\']//md-tabs-canvas//*[contains(text(),\'Shipping\')]')).click();
            element(by.xpath('//form[@name=\'editContactForm\']//md-content[@id=\'tab_Shipping\']/div/span')).click();
            element(by.xpath('//form[@name=\'editContactForm\']//md-content[@id=\'tab_Shipping\']/div/span')).click();
            element(by.xpath('//form[@name=\'editContactForm\']//md-content[@id=\'tab_Shipping\']/div/span')).click();

            browser.driver.executeScript("arguments[0].scrollIntoView(true);", element(by.xpath('//form[@name=\'editContactForm\']//md-card//md-tab-content[1]//div[@id=\'address-0\']//*[@id="addressLine"]')).getWebElement());
            element(by.xpath('//form[@name=\'editContactForm\']//md-content[@id=\'tab_Shipping\']//div[@id=\'address-0\']//*[@id="addressLine"]')).
            sendKeys(testDataContact.createContactDetails[17].addressLine);
            element(by.xpath('//form[@name=\'editContactForm\']//md-content[@id=\'tab_Shipping\']//div[@id=\'address-0\']//*[@id="city"]')).
            sendKeys(testDataContact.createContactDetails[17].city);
            element(by.xpath('//form[@name=\'editContactForm\']//md-content[@id=\'tab_Shipping\']//div[@id=\'address-0\']//*[@id="state"]')).
            sendKeys(testDataContact.createContactDetails[17].state);
            element(by.xpath('//form[@name=\'editContactForm\']//md-content[@id=\'tab_Shipping\']//div[@id=\'address-0\']//*[@id="country"]')).
            sendKeys(testDataContact.createContactDetails[17].country);
            element(by.xpath('//form[@name=\'editContactForm\']//md-content[@id=\'tab_Shipping\']//div[@id=\'address-0\']//*[@id="pinCode"]')).
            sendKeys(testDataContact.createContactDetails[17].pinCode);

            browser.driver.executeScript("arguments[0].scrollIntoView(true);", element(by.xpath('//form[@name=\'editContactForm\']//md-card//md-tab-content[1]//div[@id=\'address-1\']//*[@id="addressLine"]')).getWebElement());
            element(by.xpath('//form[@name=\'editContactForm\']//md-content[@id=\'tab_Shipping\']//div[@id=\'address-1\']//*[@id="addressLine"]')).
            sendKeys(testDataContact.createContactDetails[17].addressLine);
            element(by.xpath('//form[@name=\'editContactForm\']//md-content[@id=\'tab_Shipping\']//div[@id=\'address-1\']//*[@id="city"]')).
            sendKeys(testDataContact.createContactDetails[17].city);
            element(by.xpath('//form[@name=\'editContactForm\']//md-content[@id=\'tab_Shipping\']//div[@id=\'address-1\']//*[@id="state"]')).
            sendKeys(testDataContact.createContactDetails[17].state);
            element(by.xpath('//form[@name=\'editContactForm\']//md-content[@id=\'tab_Shipping\']//div[@id=\'address-1\']//*[@id="country"]')).
            sendKeys(testDataContact.createContactDetails[17].country);
            element(by.xpath('//form[@name=\'editContactForm\']//md-content[@id=\'tab_Shipping\']//div[@id=\'address-1\']//*[@id="pinCode"]')).
            sendKeys(testDataContact.createContactDetails[17].pinCode);

            browser.driver.executeScript("arguments[0].scrollIntoView(true);", element(by.xpath('//form[@name=\'editContactForm\']//md-card//md-tab-content[1]//div[@id=\'address-2\']//*[@id="addressLine"]')).getWebElement());
            element(by.xpath('//form[@name=\'editContactForm\']//md-content[@id=\'tab_Shipping\']//div[@id=\'address-2\']//*[@id="addressLine"]')).
            sendKeys(testDataContact.createContactDetails[17].addressLine);
            element(by.xpath('//form[@name=\'editContactForm\']//md-content[@id=\'tab_Shipping\']//div[@id=\'address-2\']//*[@id="city"]')).
            sendKeys(testDataContact.createContactDetails[17].city);
            element(by.xpath('//form[@name=\'editContactForm\']//md-content[@id=\'tab_Shipping\']//div[@id=\'address-2\']//*[@id="state"]')).
            sendKeys(testDataContact.createContactDetails[17].state);
            element(by.xpath('//form[@name=\'editContactForm\']//md-content[@id=\'tab_Shipping\']//div[@id=\'address-2\']//*[@id="country"]')).
            sendKeys(testDataContact.createContactDetails[17].country);
            element(by.xpath('//form[@name=\'editContactForm\']//md-content[@id=\'tab_Shipping\']//div[@id=\'address-2\']//*[@id="pinCode"]')).
            sendKeys(testDataContact.createContactDetails[17].pinCode);
            editContactPage.form.submit.click();
        });

        //12.TEST CASE TO ADD MORE RECEIVING ADDRESSES WHILE EDIT CONTACT
        it('should login user, Checking Edit contact and redirecting to "/"', function () {
            element(by.xpath('//md-content[@id="contact_Customers"]/view-contacts/table/tbody/tr/td[3]')).click();
            browser.sleep(2000);
            expect(element(by.xpath('//form[@name="editContactForm"]//*[contains(text(),"Contact Details")]')).isDisplayed()).toBe(true);

            browser.driver.executeScript("arguments[0].scrollIntoView(true);", element(by.xpath('//form[@name=\'editContactForm\']//div[@id=\'mobile-0\']//*[@data-ng-model=\'phone.phoneNumber\']')).getWebElement());
            element(by.xpath('//form[@name=\'editContactForm\']//md-tabs-canvas//*[contains(text(),\'Receiving\')]')).click();
            element(by.xpath('//form[@name=\'editContactForm\']//md-content[@id=\'tab_Receiving\']/div/span')).click();
            element(by.xpath('//form[@name=\'editContactForm\']//md-content[@id=\'tab_Receiving\']/div/span')).click();
            element(by.xpath('//form[@name=\'editContactForm\']//md-content[@id=\'tab_Receiving\']/div/span')).click();

            browser.driver.executeScript("arguments[0].scrollIntoView(true);", element(by.xpath('//form[@name=\'editContactForm\']//md-card//md-tab-content[1]//div[@id=\'address-0\']//*[@id="addressLine"]')).getWebElement());
            element(by.xpath('//form[@name=\'editContactForm\']//md-content[@id=\'tab_Receiving\']//div[@id=\'address-0\']//*[@id="addressLine"]')).
            sendKeys(testDataContact.createContactDetails[17].addressLine);
            element(by.xpath('//form[@name=\'editContactForm\']//md-content[@id=\'tab_Receiving\']//div[@id=\'address-0\']//*[@id="city"]')).
            sendKeys(testDataContact.createContactDetails[17].city);
            element(by.xpath('//form[@name=\'editContactForm\']//md-content[@id=\'tab_Receiving\']//div[@id=\'address-0\']//*[@id="state"]')).
            sendKeys(testDataContact.createContactDetails[17].state);
            element(by.xpath('//form[@name=\'editContactForm\']//md-content[@id=\'tab_Receiving\']//div[@id=\'address-0\']//*[@id="country"]')).
            sendKeys(testDataContact.createContactDetails[17].country);
            element(by.xpath('//form[@name=\'editContactForm\']//md-content[@id=\'tab_Receiving\']//div[@id=\'address-0\']//*[@id="pinCode"]')).
            sendKeys(testDataContact.createContactDetails[17].pinCode);

            browser.driver.executeScript("arguments[0].scrollIntoView(true);", element(by.xpath('//form[@name=\'editContactForm\']//md-card//md-tab-content[1]//div[@id=\'address-1\']//*[@id="addressLine"]')).getWebElement());
            element(by.xpath('//form[@name=\'editContactForm\']//md-content[@id=\'tab_Receiving\']//div[@id=\'address-1\']//*[@id="addressLine"]')).
            sendKeys(testDataContact.createContactDetails[17].addressLine);
            element(by.xpath('//form[@name=\'editContactForm\']//md-content[@id=\'tab_Receiving\']//div[@id=\'address-1\']//*[@id="city"]')).
            sendKeys(testDataContact.createContactDetails[17].city);
            element(by.xpath('//form[@name=\'editContactForm\']//md-content[@id=\'tab_Receiving\']//div[@id=\'address-1\']//*[@id="state"]')).
            sendKeys(testDataContact.createContactDetails[17].state);
            element(by.xpath('//form[@name=\'editContactForm\']//md-content[@id=\'tab_Receiving\']//div[@id=\'address-1\']//*[@id="country"]')).
            sendKeys(testDataContact.createContactDetails[17].country);
            element(by.xpath('//form[@name=\'editContactForm\']//md-content[@id=\'tab_Receiving\']//div[@id=\'address-1\']//*[@id="pinCode"]')).
            sendKeys(testDataContact.createContactDetails[17].pinCode);

            browser.driver.executeScript("arguments[0].scrollIntoView(true);", element(by.xpath('//form[@name=\'editContactForm\']//md-card//md-tab-content[1]//div[@id=\'address-2\']//*[@id="addressLine"]')).getWebElement());
            element(by.xpath('//form[@name=\'editContactForm\']//md-content[@id=\'tab_Receiving\']//div[@id=\'address-2\']//*[@id="addressLine"]')).
            sendKeys(testDataContact.createContactDetails[17].addressLine);
            element(by.xpath('//form[@name=\'editContactForm\']//md-content[@id=\'tab_Receiving\']//div[@id=\'address-2\']//*[@id="city"]')).
            sendKeys(testDataContact.createContactDetails[17].city);
            element(by.xpath('//form[@name=\'editContactForm\']//md-content[@id=\'tab_Receiving\']//div[@id=\'address-2\']//*[@id="state"]')).
            sendKeys(testDataContact.createContactDetails[17].state);
            element(by.xpath('//form[@name=\'editContactForm\']//md-content[@id=\'tab_Receiving\']//div[@id=\'address-2\']//*[@id="country"]')).
            sendKeys(testDataContact.createContactDetails[17].country);
            element(by.xpath('//form[@name=\'editContactForm\']//md-content[@id=\'tab_Receiving\']//div[@id=\'address-2\']//*[@id="pinCode"]')).
            sendKeys(testDataContact.createContactDetails[17].pinCode);
            editContactPage.form.submit.click();
        });

        //13.TEST CASE TO ADD MORE INVOICE ADDRESSES WHILE EDIT CONTACT
        it('should login user, Checking Edit contact and redirecting to "/"', function () {
            element(by.xpath('//md-content[@id="contact_Customers"]/view-contacts/table/tbody/tr/td[3]')).click();
            browser.sleep(2000);
            expect(element(by.xpath('//form[@name="editContactForm"]//*[contains(text(),"Contact Details")]')).isDisplayed()).toBe(true);

            browser.driver.executeScript("arguments[0].scrollIntoView(true);", element(by.xpath('//form[@name=\'editContactForm\']//div[@id=\'mobile-0\']//*[@data-ng-model=\'phone.phoneNumber\']')).getWebElement());
            element(by.xpath('//form[@name=\'editContactForm\']//md-tabs-canvas//*[contains(text(),\'Invoice\')]')).click();
            element(by.xpath('//form[@name=\'editContactForm\']//md-content[@id=\'tab_Invoice\']/div/span')).click();
            element(by.xpath('//form[@name=\'editContactForm\']//md-content[@id=\'tab_Invoice\']/div/span')).click();
            element(by.xpath('//form[@name=\'editContactForm\']//md-content[@id=\'tab_Invoice\']/div/span')).click();

            browser.driver.executeScript("arguments[0].scrollIntoView(true);", element(by.xpath('//form[@name=\'editContactForm\']//md-card//md-tab-content[1]//div[@id=\'address-0\']//*[@id="addressLine"]')).getWebElement());
            element(by.xpath('//form[@name=\'editContactForm\']//md-content[@id=\'tab_Invoice\']//div[@id=\'address-0\']//*[@id="addressLine"]')).
            sendKeys(testDataContact.createContactDetails[17].addressLine);
            element(by.xpath('//form[@name=\'editContactForm\']//md-content[@id=\'tab_Invoice\']//div[@id=\'address-0\']//*[@id="city"]')).
            sendKeys(testDataContact.createContactDetails[17].city);
            element(by.xpath('//form[@name=\'editContactForm\']//md-content[@id=\'tab_Invoice\']//div[@id=\'address-0\']//*[@id="state"]')).
            sendKeys(testDataContact.createContactDetails[17].state);
            element(by.xpath('//form[@name=\'editContactForm\']//md-content[@id=\'tab_Invoice\']//div[@id=\'address-0\']//*[@id="country"]')).
            sendKeys(testDataContact.createContactDetails[17].country);
            element(by.xpath('//form[@name=\'editContactForm\']//md-content[@id=\'tab_Invoice\']//div[@id=\'address-0\']//*[@id="pinCode"]')).
            sendKeys(testDataContact.createContactDetails[17].pinCode);

            browser.driver.executeScript("arguments[0].scrollIntoView(true);", element(by.xpath('//form[@name=\'editContactForm\']//md-card//md-tab-content[1]//div[@id=\'address-1\']//*[@id="addressLine"]')).getWebElement());
            element(by.xpath('//form[@name=\'editContactForm\']//md-content[@id=\'tab_Invoice\']//div[@id=\'address-1\']//*[@id="addressLine"]')).
            sendKeys(testDataContact.createContactDetails[17].addressLine);
            element(by.xpath('//form[@name=\'editContactForm\']//md-content[@id=\'tab_Invoice\']//div[@id=\'address-1\']//*[@id="city"]')).
            sendKeys(testDataContact.createContactDetails[17].city);
            element(by.xpath('//form[@name=\'editContactForm\']//md-content[@id=\'tab_Invoice\']//div[@id=\'address-1\']//*[@id="state"]')).
            sendKeys(testDataContact.createContactDetails[17].state);
            element(by.xpath('//form[@name=\'editContactForm\']//md-content[@id=\'tab_Invoice\']//div[@id=\'address-1\']//*[@id="country"]')).
            sendKeys(testDataContact.createContactDetails[17].country);
            element(by.xpath('//form[@name=\'editContactForm\']//md-content[@id=\'tab_Invoice\']//div[@id=\'address-1\']//*[@id="pinCode"]')).
            sendKeys(testDataContact.createContactDetails[17].pinCode);

            browser.driver.executeScript("arguments[0].scrollIntoView(true);", element(by.xpath('//form[@name=\'editContactForm\']//md-card//md-tab-content[1]//div[@id=\'address-2\']//*[@id="addressLine"]')).getWebElement());
            element(by.xpath('//form[@name=\'editContactForm\']//md-content[@id=\'tab_Invoice\']//div[@id=\'address-2\']//*[@id="addressLine"]')).
            sendKeys(testDataContact.createContactDetails[17].addressLine);
            element(by.xpath('//form[@name=\'editContactForm\']//md-content[@id=\'tab_Invoice\']//div[@id=\'address-2\']//*[@id="city"]')).
            sendKeys(testDataContact.createContactDetails[17].city);
            element(by.xpath('//form[@name=\'editContactForm\']//md-content[@id=\'tab_Invoice\']//div[@id=\'address-2\']//*[@id="state"]')).
            sendKeys(testDataContact.createContactDetails[17].state);
            element(by.xpath('//form[@name=\'editContactForm\']//md-content[@id=\'tab_Invoice\']//div[@id=\'address-2\']//*[@id="country"]')).
            sendKeys(testDataContact.createContactDetails[17].country);
            element(by.xpath('//form[@name=\'editContactForm\']//md-content[@id=\'tab_Invoice\']//div[@id=\'address-2\']//*[@id="pinCode"]')).
            sendKeys(testDataContact.createContactDetails[17].pinCode);
            editContactPage.form.submit.click();
        });

        //14.TEST CASE TO CHECK MINIMUM ONE EMAIL OR PHONE IS REQUIRED
        it('should login user, Create contact with phone validation and by taking email',function () {
            contacts.initiateCreateContact('Customers');
            createContactPage.form.displayName.sendKeys(testDataContact.createContactDetails[19].displayName);
            element(by.xpath('//*[contains(text(),"Add Communication Info")]')).click();
            createContactPage.form.phoneNumber.click();
            createContactPage.form.element(by.xpath('//*[@id=\'email-0\']/*//*[@ng-model=\'email.email\']')).click();
            createContactPage.form.element(by.xpath('//*[@id=\'email-0\']/*//*[@ng-model=\'email.email\']')).sendKeys(testDataContact.createContactDetails[19].email);
            createContactPage.form.submit.click();
            browser.sleep(3000);
            //*[@id="contactTable"]/tbody/tr
            expect(element(by.xpath('//md-content[@id="contact_Customers"]/view-contacts/table/tbody/tr/td[3]')).getText()).toBe(testDataContact.createContactDetails[19].displayName);
            expect(element(by.xpath('//md-content[@id="contact_Customers"]/view-contacts/table/tbody/tr/td[4]')).getText()).toBe(testDataContact.createContactDetails[19].emial);
            browser.sleep(2000);
            element(by.xpath('//md-content[@id="contact_Customers"]/view-contacts/table/tbody/tr/td[7]')).click()
            browser.sleep(2000);
            expect(element(by.xpath('//form[@name="editContactForm"]//*[contains(text(),"Contact Details")]')).isDisplayed()).toBe(true);
            expect(element(by.xpath('//label[contains(text(),'+testDataContact.createContactDetails[19].displayName+')]')).isPresent()).toBe(true);

            browser.sleep(1000);
            element(by.xpath('//form[@name=\'editContactForm\']//md-tabs-canvas//*[contains(text(),\'Email\')]')).click();
            browser.sleep(1000);
            expect(element(by.xpath('//form[@name=\'editContactForm\']//*[@id=\'contact_email\']//*[@ng-model=\'email.email\']')).
            getAttribute('value')).toBe(testDataContact.createContactDetails[19].email);
            element(by.xpath('//form[@name=\'editContactForm\']//*[@id=\'contact_email\']//*[@ng-model=\'email.email\']')).sendKeys('');
            element(by.xpath('//form[@name=\'editContactForm\']//md-tabs-canvas//*[contains(text(),\'Phone\')]')).click();
            element(by.xpath('//form[@name=\'editContactForm\']//md-content[@id=\'contact_phone\']/div/span')).click();
            element(by.xpath('//form[@name=\'editContactForm\']//*[@id=\'contact_phone\']//*[@data-ng-model=\'phone.phoneNumber\']')).sendKeys(testDataContact.createContactDetails[19].phoneNumber);
            element(by.xpath('//button[@aria-label=\'Update\']')).click();
            browser.sleep(3000);
            //*[@id="contactTable"]/tbody/tr
            expect(element(by.xpath('//md-content[@id="contact_Customers"]/view-contacts/table/tbody/tr/td[3]')).getText()).toBe(testDataContact.createContactDetails[19].displayName);
            expect(element(by.xpath('//md-content[@id="contact_Customers"]/view-contacts/table/tbody/tr/td[5]')).getText()).toBe(testDataContact.createContactDetails[19].phoneNumber);
            browser.sleep(2000);
            contacts.deleteContacts('Customers');
            browser.sleep(2000);
        });

        //15.TEST CASE TO CHECK MINIMUM ONE EMAIL OR PHONE IS REQUIRED
        it('should login user, Create contact with email validation and by taking email',function () {
            contacts.initiateCreateContact('Customers');
            createContactPage.form.displayName.sendKeys(testDataContact.createContactDetails[20].displayName);
            element(by.xpath('//*[contains(text(),"Add Communication Info")]')).click();
            createContactPage.form.phoneNumber.click();
            createContactPage.form.phoneNumber.sendKeys(testDataContact.createContactDetails[20].phoneNumber);
            createContactPage.form.submit.click();
            browser.sleep(3000);
            //*[@id="contactTable"]/tbody/tr
            expect(element(by.xpath('//md-content[@id="contact_Customers"]/view-contacts/table/tbody/tr/td[3]')).getText()).toBe(testDataContact.createContactDetails[20].displayName);
            expect(element(by.xpath('//md-content[@id="contact_Customers"]/view-contacts/table/tbody/tr/td[5]')).getText()).toBe(testDataContact.createContactDetails[20].phoneNumber);
            browser.sleep(2000);
            element(by.xpath('//md-content[@id="contact_Customers"]/view-contacts/table/tbody/tr/td[7]')).click()
            browser.sleep(2000);
            expect(element(by.xpath('//form[@name="editContactForm"]//*[contains(text(),"Contact Details")]')).isDisplayed()).toBe(true);
            expect(element(by.xpath('//label[contains(text(),'+testDataContact.createContactDetails[20].displayName+')]')).isPresent()).toBe(true);

            browser.sleep(1000);
            createContactPage.form.phoneNumber.sendKeys('');
            element(by.xpath('//form[@name=\'editContactForm\']//md-tabs-canvas//*[contains(text(),\'Email\')]')).click();
            browser.sleep(1000);
            element(by.xpath('//form[@name=\'editContactForm\']//*[@id=\'contact_email\']//*[@ng-model=\'email.email\']')).sendKeys(testDataContact.createContactDetails[20].email);
            element(by.xpath('//button[@aria-label=\'Update\']')).click();
            browser.sleep(3000);
            //*[@id="contactTable"]/tbody/tr
            expect(element(by.xpath('//md-content[@id="contact_Customers"]/view-contacts/table/tbody/tr/td[3]')).getText()).toBe(testDataContact.createContactDetails[20].displayName);
            expect(element(by.xpath('//md-content[@id="contact_Customers"]/view-contacts/table/tbody/tr/td[4]')).getText()).toBe(testDataContact.createContactDetails[20].email);
            browser.sleep(2000);
            contacts.deleteContacts('Customers');
            browser.sleep(2000);
        });



    });
});
