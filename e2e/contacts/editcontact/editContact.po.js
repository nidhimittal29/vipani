'use strict';

var editcontactPage = function() {
    var form = this.form = element(by.name('editContactForm'));
    form.displayName=form.element(by.model('contact.displayName'));
    form.contactType=form.element(by.model('contact.customerType'));
    form.firstName=form.element(by.model('contact.firstName'));
    form.middleName=form.element(by.model('contact.middleName'));
    form.lastName=form.element(by.model('contact.lastName'));
    form.companyName=form.element(by.model('contact.companyName'));
    form.gstinNumber=form.element(by.model('contact.gstinNumber'));
    form.submit = form.element(by.xpath('//form[@name="editContactForm"]//*[@aria-label="Update"]'));
    this.editcontact = function(data) {
        for (var prop in data) {
            var formElem = form[prop];
            if (data.hasOwnProperty(prop) && formElem && typeof formElem.sendKeys === 'function') {
                formElem.sendKeys(data[prop]);
            }
        }
    };
};

module.exports = new editcontactPage();
