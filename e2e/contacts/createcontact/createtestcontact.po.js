'use strict';

var createtestcontactPage = function() {
    var form = this.form = element(by.name('addContactsForm'));
    form.customerType = form.element(by.model('customerType'));
    form.displayName = form.element(by.model('displayName'));
    form.firstName=form.element(by.model('firstName'));
    form.middleName=form.element(by.model('middleName'));
    form.lastName=form.element(by.model('lastName'));
    form.companyName=form.element(by.model('companyName'));
    form.gstinNumber=form.element(by.model('gstinNumber'));
    form.phoneType = form.element(by.model('phone.phoneType'));
    form.phoneNumber = form.element(by.model('phone.phoneNumber'));
    form.email = form.element(by.model('email.email'));
    form.emailType = form.element(by.model('email.emailType'));
    form.submit = form.element(by.css('.nvc-accent-background'));
    this.createtestcontact = function(data) {
        for (var prop in data) {
            var formElem = form[prop];
            if (data.hasOwnProperty(prop) && formElem && typeof formElem.sendKeys === 'function') {
                formElem.sendKeys(data[prop]);
            }
        }
    };
};

module.exports = new createtestcontactPage();
