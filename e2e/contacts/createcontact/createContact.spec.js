'use strict';

var config = browser.params;
var user=require('../../account/common.util.e2e/signin.common.po');
var contacts=require('../../account/common.util.e2e/contacts.common.po');
var global = require('../../account/common.util.e2e/global.common.po');
var loginDetails=require('./login');
describe('Create Contact View', function() {
    var loginPage, createContactPage, testDataContact;
    var loadPage = function () {
        browser.manage().deleteAllCookies();
        browser.get('http://localhost:3000/#!/signin');

    };

    describe('with local auth', function () {
        beforeAll(function () {
            global.loadPage('http://localhost:3000/#!/register');
            user.registerProcess(loginDetails[0]);
        });
        beforeEach(function () {
            loadPage();
            testDataContact = require('../contact.testdata.json');
            createContactPage = require('./createtestcontact.po');

            user.login(loginDetails[0]);
            contacts.initiateContacts();
            browser.sleep(2000);

        });

        afterEach(function () {
            browser.sleep(2000);
            user.logout();
        });

        //1. TEST CASE TO CREATE CUSTOMER CONTACT WITH MINIMUM FIELDS
        it('should login user, Checking Create customer with minimum fields', function () {
              contacts.initiateCreateContact('Customers');
              createContactPage.form.displayName.sendKeys(testDataContact.createContactDetails[0].displayName);
              element(by.xpath('//*[contains(text(),"Add Communication Info")]')).click();
              createContactPage.form.phoneNumber.sendKeys(testDataContact.createContactDetails[0].phoneNumber);
              createContactPage.form.submit.click();
              browser.sleep(3000);
              //*[@id="contactTable"]/tbody/tr
              expect(element(by.xpath('//md-content[@id="contact_Customers"]/view-contacts/table/tbody/tr/td[3]')).getText()).toBe(testDataContact.createContactDetails[0].displayName);
              expect(element(by.xpath('//md-content[@id="contact_Customers"]/view-contacts/table/tbody/tr/td[5]')).getText()).toBe(testDataContact.createContactDetails[0].phoneNumber);
              browser.sleep(2000);
              contacts.deleteContacts('Customers');
              browser.sleep(2000);

          });

        //2.TEST CASE TO CREATE SUPPLIER CONTACT WITH MINIMUM FIELDS
        it('should login user, Checking Create supplier with minimum fields', function () {
              contacts.initiateCreateContact('Suppliers');
              browser.sleep(200);
              createContactPage.form.displayName.sendKeys(testDataContact.createContactDetails[1].displayName);
              element(by.xpath('//*[contains(text(),"Add Communication Info")]')).click();
              createContactPage.form.phoneNumber.sendKeys(testDataContact.createContactDetails[1].phoneNumber);
              createContactPage.form.submit.click();
              browser.sleep(3000);
              expect(element(by.xpath('//md-content[@id="contact_Suppliers"]/view-contacts/table/tbody/tr/td[3]')).getText()).toBe(testDataContact.createContactDetails[1].displayName);
              expect(element(by.xpath('//md-content[@id="contact_Suppliers"]/view-contacts/table/tbody/tr/td[5]')).getText()).toBe(testDataContact.createContactDetails[1].phoneNumber);
              contacts.deleteContacts('Suppliers');
              browser.sleep(2000);
          });

        //3.TEST CASE TO CHECK DEFAULT VALUES TO CREATE CUSTOMER CONTACT
        it('should login user, Checking Create Customer contact with def values ', function () {
              contacts.initiateCreateContact('Customers');
              var addContactForm = createContactPage.form;
              expect(addContactForm.element(by.model('customerType')).getText()).toBe('Customer');
              expect(addContactForm.element(by.xpath('//*[@id=\'mobile-0\']/*//*[@ng-model=\'phone.phoneType\']')).getText()).toBe('Mobile');
              expect(addContactForm.element(by.xpath('//*[@id=\'email-0\']/*//*[@ng-model=\'email.emailType\']')).getText()).toBe('Work');
              createContactPage.form.element(by.xpath('//*[@aria-label="Show More Or Less Link"]')).click();
              addContactForm.firstName.sendKeys('ax');
              addContactForm.middleName.sendKeys('ax');
              addContactForm.lastName.sendKeys('ax');
              browser.sleep(2000);
              addContactForm.element(by.css('[aria-label="close dialog"]')).click();

              contacts.initiateCreateContact('Suppliers');
              var addContactForm = createContactPage.form;
              expect(addContactForm.element(by.model('customerType')).getText()).toBe('Supplier');
              expect(addContactForm.element(by.xpath('//*[@id=\'mobile-0\']/*//*[@ng-model=\'phone.phoneType\']')).getText()).toBe('Mobile');
              expect(addContactForm.element(by.xpath('//*[@id=\'email-0\']/*//*[@ng-model=\'email.emailType\']')).getText()).toBe('Work');
              createContactPage.form.element(by.xpath('//*[@aria-label="Show More Or Less Link"]')).click();
              expect(addContactForm.firstName.getText()).toBe('');
              expect(addContactForm.middleName.getText()).toBe('');
              expect(addContactForm.lastName.getText()).toBe('');
              addContactForm.element(by.css('[aria-label="close dialog"]')).click();
              browser.sleep(2000);
          });

        //4.TEST CASE TO VALIDATE REQUIRE FIELDS  TO CREATE CONTACT
        it('should login user, validate required fields in create contact form', function () {
              contacts.initiateCreateContact('Customers');
              browser.sleep(3000);
              expect(createContactPage.form.submit.isEnabled()).toBe(true);
              expect(createContactPage.form.element(by.css('[ng-model="displayName"]')).getAttribute('required')).toBe('true');
              createContactPage.form.submit.click();
              expect(element(by.xpath('//input[@id=\'phoneNumber\']')).getAttribute('required')).toBe('true');

              createContactPage.form.element(by.css('[aria-label="close dialog"]')).click();
              browser.sleep(2000);
          });

        //5.TEST CASE TO PHONE NUMBER VALIDATION WHILE CREATE CONTACT
        it('should login user, Checking phone number validation while creating contact', function () {
              contacts.initiateCreateContact('Customers');
              createContactPage.form.phoneNumber.sendKeys('1245367');
              expect(createContactPage.form.submit.isEnabled()).toBe(true);
              createContactPage.form.submit.click();
              expect(browser.element(by.xpath('//*[@id="mobile-0"]/div[2]/div[2]/md-input-container/i')).getAttribute('data'))
                  .toBe('Invalid Input..');
              createContactPage.form.element(by.css('[aria-label="close dialog"]')).click();
              browser.sleep(2000);
          });

        //6.TEST CASE TO EMAIL VALIDATION WHILE CREATE CONTACT
        it('should login user, Checking email validation contact and redirecting to ', function () {
              contacts.initiateCreateContact('Customers');
              browser.sleep(3000);
              createContactPage.form.email.sendKeys('asddsf');
              expect(createContactPage.form.submit.isEnabled()).toBe(true);
              createContactPage.form.submit.click();
              expect(browser.element(by.xpath('//*[@id="email-0"]/div[2]/div[2]/md-input-container/i')).getAttribute('data'))
                  .toBe('Invalid Input..');
              createContactPage.form.element(by.css('[aria-label="close dialog"]')).click();
              browser.sleep(2000);
          });

        //7.TEST CASE TO PINCODE VALIDATION WHILE CREATE CONTACT
        it('should login user, checking pin code validation', function () {
              contacts.initiateCreateContact('Customers');
              createContactPage.form.phoneNumber.sendKeys('1245367542');
              browser.sleep(3000);
              browser.driver.executeScript("arguments[0].scrollIntoView(true);", element(by.xpath('//*[contains(text(),"Add Address Details")]')).getWebElement());
              element(by.xpath('//*[contains(text(),"Add Address Details")]')).click();
              expect(element(by.xpath('//md-tab-content[1]//div[@id="address-0"]//*[@ng-model="address.pinCode"]')).getAttribute('ng-minlength')).toBe('6');
              expect(element(by.xpath('//md-tab-content[1]//div[@id="address-0"]//*[@ng-model="address.pinCode"]')).getAttribute('ng-maxlength')).toBe('6');
              expect(element(by.xpath('//md-tab-content[1]//div[@id="address-0"]//*[@ng-model="address.pinCode"]')).getAttribute('only-digits')).toBe('');
              //expect(createContactPage.form.submit.isEnabled()).toBe(false);
              createContactPage.form.element(by.css('[aria-label="close dialog"]')).click();
              browser.sleep(2000);
          });

        //8.TEST CASE TO CREATE CUSTOMER CONTACT WITH MOBILE NUMBER AND EMAIL ID
        it('should login user, checking customer contact with mobile and email ', function () {
              contacts.initiateCreateContact('Customers');
              createContactPage.form.displayName.sendKeys(testDataContact.createContactDetails[2].displayName);
              createContactPage.form.phoneNumber.sendKeys(testDataContact.createContactDetails[2].phoneNumber);
              createContactPage.form.email.sendKeys(testDataContact.createContactDetails[2].email);
              createContactPage.form.submit.click();
              browser.sleep(3000);
              expect(element(by.xpath('//md-content[@id=\'contact_Customers\']/view-contacts/table/tbody/tr/td[3]')).getText()).toBe(testDataContact.createContactDetails[2].displayName);
              expect(element(by.xpath('//md-content[@id=\'contact_Customers\']/view-contacts/table/tbody/tr/td[5]')).getText()).toBe(testDataContact.createContactDetails[2].phoneNumber);
              expect(element(by.xpath('//md-content[@id=\'contact_Customers\']/view-contacts/table/tbody/tr/td[4]')).getText()).toBe(testDataContact.createContactDetails[2].email);
              contacts.deleteContacts('Customers');
              browser.sleep(2000);
          });

        //9.TEST CASE TO CREATE SUPPLIER CONTACT WITH MOBILE NUMBER AND EMAIL ID
        it('should login user, checking create supplier with mobile and email', function () {
              contacts.initiateCreateContact('Suppliers');
              createContactPage.form.displayName.sendKeys(testDataContact.createContactDetails[3].displayName);
              createContactPage.form.phoneNumber.sendKeys(testDataContact.createContactDetails[3].phoneNumber);
              createContactPage.form.email.sendKeys(testDataContact.createContactDetails[3].email);
              createContactPage.form.submit.click();
              browser.sleep(3000);
              expect(element(by.xpath("//md-content[@id=\'contact_Suppliers\']//view-contacts/table/tbody/tr/td[3]"))
                  .getText()).toBe(testDataContact.createContactDetails[3].displayName);
              expect(element(by.xpath("//md-content[@id=\'contact_Suppliers\']/view-contacts/table/tbody/tr/td[5]"))
                  .getText()).toBe(testDataContact.createContactDetails[3].phoneNumber);
              expect(element(by.xpath("//md-content[@id=\'contact_Suppliers\']/view-contacts/table/tbody/tr/td[4]"))
                  .getText()).toBe(testDataContact.createContactDetails[3].email);
              contacts.deleteContacts('Suppliers');
              browser.sleep(2000);
          });

        //10.TEST CASE TO CREATE CUSTOMER CONTACT WITH MORE DETAILS
        it('should login user, Checking Create customer with more details', function () {
            contacts.initiateCreateContact('Customers');
            createContactPage.form.displayName.sendKeys(testDataContact.createContactDetails[4].displayName);
            createContactPage.form.element(by.xpath('//*[@aria-label="Show More Or Less Link"]')).click();
            createContactPage.form.firstName.sendKeys(testDataContact.createContactDetails[4].firstName);
            createContactPage.form.middleName.sendKeys(testDataContact.createContactDetails[4].middleName);
            createContactPage.form.lastName.sendKeys(testDataContact.createContactDetails[4].lastName);
            createContactPage.form.companyName.sendKeys(testDataContact.createContactDetails[4].companyName);
            createContactPage.form.gstinNumber.sendKeys(testDataContact.createContactDetails[4].gstinNumber);

            createContactPage.form.phoneNumber.sendKeys(testDataContact.createContactDetails[4].phoneNumber);
            createContactPage.form.email.sendKeys(testDataContact.createContactDetails[4].email);
            createContactPage.form.submit.click();
            browser.sleep(3000);
            expect(element(by.xpath('//md-content[@id=\'contact_Customers\']//view-contacts/table/tbody/tr/td[3]')).getText()).toBe(testDataContact.createContactDetails[4].displayName);
            expect(element(by.xpath('//md-content[@id=\'contact_Customers\']//view-contacts/table/tbody/tr/td[5]')).getText()).toBe(testDataContact.createContactDetails[4].phoneNumber);
            expect(element(by.xpath("//md-content[@id=\'contact_Customers\']//view-contacts/table/tbody/tr/td[4]")).getText()).toBe(testDataContact.createContactDetails[4].email);

            contacts.deleteContacts('Customers');
            browser.sleep(1500);
        });

        //11.TEST CASE TO CREATE SUPPLIER CONTACT WITH MORE DETAILS
        it('should login user, Checking Create supplier with more details ', function () {
            contacts.initiateCreateContact('Suppliers');
            createContactPage.form.displayName.sendKeys(testDataContact.createContactDetails[5].displayName);
            createContactPage.form.element(by.xpath('//*[@aria-label="Show More Or Less Link"]')).click();
            createContactPage.form.firstName.sendKeys(testDataContact.createContactDetails[5].firstName);
            createContactPage.form.middleName.sendKeys(testDataContact.createContactDetails[5].middleName);
            createContactPage.form.lastName.sendKeys(testDataContact.createContactDetails[5].lastName);
            createContactPage.form.companyName.sendKeys(testDataContact.createContactDetails[5].companyName);
            createContactPage.form.gstinNumber.sendKeys(testDataContact.createContactDetails[5].gstinNumber);
            element(by.xpath('//*[contains(text(),"Add Communication Info")]')).click();
            createContactPage.form.phoneNumber.sendKeys(testDataContact.createContactDetails[5].phoneNumber);
            createContactPage.form.email.sendKeys(testDataContact.createContactDetails[5].email);
            createContactPage.form.submit.click();
            browser.sleep(3000);

            expect(element(by.xpath('//md-content[@id=\'contact_Suppliers\']//view-contacts/table/tbody/tr/td[3]')).getText()).toBe(testDataContact.createContactDetails[5].displayName);
            expect(element(by.xpath('//md-content[@id=\'contact_Suppliers\']//view-contacts/table/tbody/tr/td[5]')).getText()).toBe(testDataContact.createContactDetails[5].phoneNumber);
            expect(element(by.xpath("//md-content[@id=\'contact_Suppliers\']//view-contacts/table/tbody/tr/td[4]")).getText()).toBe(testDataContact.createContactDetails[5].email);

            contacts.deleteContacts('Suppliers');
            browser.sleep(1500);

        });

        //12.TEST CASE TO VALIDATION FOR GSTIN NUMBER WHILE CREATE CUSTOMER CONTACT CONTACT
        it('should login user, Checking validation of gstin number for customer', function () {
            contacts.initiateCreateContact('Customers');

            createContactPage.form.displayName.sendKeys(testDataContact.createContactDetails[6].displayName);
            createContactPage.form.element(by.xpath('//*[@aria-label="Show More Or Less Link"]')).click();
            createContactPage.form.firstName.sendKeys(testDataContact.createContactDetails[6].firstName);
            createContactPage.form.middleName.sendKeys(testDataContact.createContactDetails[6].middleName);
            createContactPage.form.lastName.sendKeys(testDataContact.createContactDetails[6].lastName);
            createContactPage.form.companyName.sendKeys(testDataContact.createContactDetails[6].companyName);
            createContactPage.form.gstinNumber.sendKeys(testDataContact.createContactDetails[6].gstinNumber);
            expect(element(by.xpath("//i[@class='fa fa-info-circle pass-icon nv-red ng-scope']")).isDisplayed())
                .toBe(true);
            //expect(createContactPage.form.submit.isEnabled()).toBe(false);
            createContactPage.form.element(by.css('[aria-label="close dialog"]')).click();
            browser.sleep(1500);
        });

        //13.TEST CASE TO CREATE CUSTOMER CONTACT WITH MORE NUMBER OF MOBILE NUMBERS AND EMAIL IDS
        it('should login user, Checking Create customer contact with  more mobile numbers and emails ', function () {
                contacts.initiateCreateContact('Customers');
                createContactPage.form.displayName.sendKeys(testDataContact.createContactDetails[7].displayName);
            browser.findElement(by.xpath('//*[contains(text(),"Add Communication Info")]')).click();

            browser.findElement(by.xpath("//div[@id='mobile-0']//*[@ng-model=\"phone.phoneType\"]")).click();
            browser.findElement(by.xpath("//div[@class='md-select-menu-container md-active md-clickable']" +
                    "//*[contains(text(),'"+testDataContact.createContactDetails[7].phoneNumbers[0].type+"')]")).click();
            browser.findElement(by.xpath("//div[@id='mobile-0']//*[@ng-model='phone.phoneNumber']")).
                sendKeys(testDataContact.createContactDetails[7].phoneNumbers[0].phoneNumber);

            browser.findElement(by.xpath("//*[@id=\"mobile-0\"]/div[1]/md-icon")).click();
                browser.driver.executeScript("arguments[0].scrollIntoView(true);", element(by.xpath('//div[@id=\'mobile-1\']//*[@ng-model=\'phone.phoneNumber\']')).getWebElement());

            browser.findElement(by.xpath("//div[@id='mobile-1']//*[@ng-model=\"phone.phoneType\"]")).click();
            browser.findElement(by.xpath("//div[@class='md-select-menu-container md-active md-clickable']" +
                    "//*[contains(text(),'"+testDataContact.createContactDetails[7].phoneNumbers[1].type+"')]")).click();
            browser.findElement(by.xpath("//div[@id='mobile-1']//*[@ng-model='phone.phoneNumber']")).
                sendKeys(testDataContact.createContactDetails[7].phoneNumbers[1].phoneNumber);

            browser.findElement(by.xpath("//*[@id=\"mobile-0\"]/div[1]/md-icon")).click();
                browser.driver.executeScript("arguments[0].scrollIntoView(true);", element(by.xpath('//div[@id=\'mobile-2\']//*[@ng-model=\'phone.phoneNumber\']')).getWebElement());

            browser.findElement(by.xpath("//div[@id='mobile-2']//*[@ng-model=\"phone.phoneType\"]")).click();
            browser.findElement(by.xpath("//div[@class='md-select-menu-container md-active md-clickable']" +
                    "//*[contains(text(),'"+testDataContact.createContactDetails[7].phoneNumbers[2].type+"')]")).click();
            browser.findElement(by.xpath("//div[@id='mobile-2']//*[@ng-model='phone.phoneNumber']")).
                sendKeys(testDataContact.createContactDetails[7].phoneNumbers[2].phoneNumber);

            browser.findElement(by.xpath("//div[@id='email-0']//*[@ng-model=\"email.emailType\"]")).click();
            browser.findElement(by.xpath("//div[@class='md-select-menu-container md-active md-clickable']" +
                    "//*[contains(text(),'"+testDataContact.createContactDetails[7].emails[0].type+"')]")).click();
            browser.findElement(by.xpath("//div[@id='email-0']//*[@ng-model='email.email']")).
                sendKeys(testDataContact.createContactDetails[7].emails[0].email);

            browser.findElement(by.xpath("//*[@id=\"email-0\"]/div[1]/md-icon")).click();
                browser.driver.executeScript("arguments[0].scrollIntoView(true);", element(by.xpath('//div[@id=\'email-1\']//*[@ng-model=\'email.email\']')).getWebElement());

            browser.findElement(by.xpath("//div[@id='email-1']//*[@ng-model=\"email.emailType\"]")).click();
            browser.findElement(by.xpath("//div[@class='md-select-menu-container md-active md-clickable']" +
                    "//*[contains(text(),'"+testDataContact.createContactDetails[7].emails[1].type+"')]")).click();
            browser.findElement(by.xpath("//div[@id='email-1']//*[@ng-model='email.email']")).
                sendKeys(testDataContact.createContactDetails[7].emails[1].email);

            browser.findElement(by.xpath("//*[@id=\"email-0\"]/div[1]/md-icon")).click();
                browser.driver.executeScript("arguments[0].scrollIntoView(true);", element(by.xpath('//div[@id=\'email-2\']//*[@ng-model=\'email.email\']')).getWebElement());

            browser.findElement(by.xpath("//div[@id='email-2']//*[@ng-model=\"email.emailType\"]")).click();
            browser.findElement(by.xpath("//div[@class='md-select-menu-container md-active md-clickable']" +
                    "//*[contains(text(),'"+testDataContact.createContactDetails[7].emails[2].type+"')]")).click();
            browser.findElement(by.xpath("//div[@id='email-2']//*[@ng-model='email.email']")).
                sendKeys(testDataContact.createContactDetails[7].emails[2].email);
                createContactPage.form.submit.click();
                browser.sleep(300);
                expect(element(by.xpath("//md-tabs/md-tabs-content-wrapper/md-tab-content[1]/div/md-content/" +
                    "view-contacts/table/tbody/tr/td[3]")).getText()).toBe(testDataContact.createContactDetails[7].displayName);
                expect(element(by.xpath("//md-tabs/md-tabs-content-wrapper/md-tab-content[1]/div/md-content/" +
                    "view-contacts/table/tbody/tr/td[5]")).getText()).toBe(testDataContact.createContactDetails[7].phoneNumbers[0].phoneNumber);
                expect(element(by.xpath("//md-tabs/md-tabs-content-wrapper/md-tab-content[1]/div/md-content/" +
                    "view-contacts/table/tbody/tr/td[4]")).getText()).toBe(testDataContact.createContactDetails[7].emails[0].email);
                contacts.deleteContacts('Customers');
                browser.sleep(1500);
            });

        //14.TEST CASE TO CREATE SUPPLIER CONTACT WITH MORE NUMBER OF MOBILE NUMBERS AND EMAIL IDS
        it('should login user, Checking Create supplier contact with more mobiles and emails', function () {
                contacts.initiateCreateContact('Suppliers');
                createContactPage.form.displayName.sendKeys(testDataContact.createContactDetails[8].displayName);
                browser.findElement(by.xpath('//*[contains(text(),"Add Communication Info")]')).click();

                 browser.findElement(by.xpath("//div[@id='mobile-0']//*[@ng-model=\"phone.phoneType\"]")).click();
                 browser.findElement(by.xpath("//div[@class='md-select-menu-container md-active md-clickable']" +
                    "//*[contains(text(),'"+testDataContact.createContactDetails[8].phoneNumbers[0].type+"')]")).click();
                 browser.findElement(by.xpath("//div[@id='mobile-0']//*[@ng-model='phone.phoneNumber']")).
                sendKeys(testDataContact.createContactDetails[8].phoneNumbers[0].phoneNumber);

                 browser.findElement(by.xpath("//*[@id=\"mobile-0\"]/div[1]/md-icon")).click();
                browser.driver.executeScript("arguments[0].scrollIntoView(true);", element(by.xpath('//div[@id=\'mobile-1\']//*[@ng-model="phone.phoneType"]')).getWebElement());

                 browser.findElement(by.xpath("//div[@id='mobile-1']//*[@ng-model=\"phone.phoneType\"]")).click();
                 browser.findElement(by.xpath("//div[@class='md-select-menu-container md-active md-clickable']" +
                    "//*[contains(text(),'"+testDataContact.createContactDetails[8].phoneNumbers[1].type+"')]")).click();
                 browser.findElement(by.xpath("//div[@id='mobile-1']//*[@ng-model='phone.phoneNumber']")).
                sendKeys(testDataContact.createContactDetails[8].phoneNumbers[1].phoneNumber);

                 browser.findElement(by.xpath("//*[@id=\"mobile-0\"]/div[1]/md-icon")).click();
                browser.driver.executeScript("arguments[0].scrollIntoView(true);", element(by.xpath('//div[@id=\'mobile-2\']//*[@ng-model="phone.phoneType"]')).getWebElement());

                 browser.findElement(by.xpath("//div[@id='mobile-2']//*[@ng-model=\"phone.phoneType\"]")).click();
                 browser.findElement(by.xpath("//div[@class='md-select-menu-container md-active md-clickable']" +
                    "//*[contains(text(),'"+testDataContact.createContactDetails[8].phoneNumbers[2].type+"')]")).click();
                 browser.findElement(by.xpath("//div[@id='mobile-2']//*[@ng-model='phone.phoneNumber']")).
                sendKeys(testDataContact.createContactDetails[8].phoneNumbers[2].phoneNumber);

                browser.driver.executeScript("arguments[0].scrollIntoView(true);", element(by.xpath('//div[@id=\'email-0\']//*[@ng-model="email.emailType"]')).getWebElement());
                 browser.findElement(by.xpath("//div[@id='email-0']//*[@ng-model=\"email.emailType\"]")).click();
                 browser.findElement(by.xpath("//div[@class='md-select-menu-container md-active md-clickable']" +
                    "//*[contains(text(),'"+testDataContact.createContactDetails[8].emails[0].type+"')]")).click();
                 browser.findElement(by.xpath("//div[@id='email-0']//*[@ng-model='email.email']")).
                sendKeys(testDataContact.createContactDetails[8].emails[0].email);

                 browser.findElement(by.xpath("//*[@id=\"email-0\"]/div[1]/md-icon")).click();
                browser.driver.executeScript("arguments[0].scrollIntoView(true);", element(by.xpath('//div[@id=\'email-1\']//*[@ng-model="email.emailType"]')).getWebElement());

                 browser.findElement(by.xpath("//div[@id='email-1']//*[@ng-model=\"email.emailType\"]")).click();
                 browser.findElement(by.xpath("//div[@class='md-select-menu-container md-active md-clickable']" +
                    "//*[contains(text(),'"+testDataContact.createContactDetails[8].emails[1].type+"')]")).click();
                 browser.findElement(by.xpath("//div[@id='email-1']//*[@ng-model='email.email']")).
                sendKeys(testDataContact.createContactDetails[8].emails[1].email);

                 browser.findElement(by.xpath("//*[@id=\"email-0\"]/div[1]/md-icon")).click();
                browser.driver.executeScript("arguments[0].scrollIntoView(true);", element(by.xpath('//div[@id=\'email-2\']//*[@ng-model="email.emailType"]')).getWebElement());

                 browser.findElement(by.xpath("//div[@id='email-2']//*[@ng-model=\"email.emailType\"]")).click();
                 browser.findElement(by.xpath("//div[@class='md-select-menu-container md-active md-clickable']" +
                    "//*[contains(text(),'"+testDataContact.createContactDetails[8].emails[2].type+"')]")).click();
                createContactPage.form.submit.click();
                browser.sleep(3000);

            expect(element(by.xpath("//md-content[@id='contact_Suppliers']/view-contacts/table/tbody/tr/td[3]")).getText()).toBe(testDataContact.createContactDetails[8].displayName);
            expect(element(by.xpath("//md-content[@id='contact_Suppliers']/view-contacts/table/tbody/tr/td[5]")).getText()).toBe(testDataContact.createContactDetails[8].phoneNumbers[0].phoneNumber);
            expect(element(by.xpath("//md-content[@id='contact_Suppliers']/view-contacts/table/tbody/tr/td[4]")).getText()).toBe(testDataContact.createContactDetails[8].emails[0].email);
            contacts.deleteContacts('Suppliers');
            browser.sleep(1500);
        });

        //15.TEST CASE TO CREATE CUSTOMER CONTACT WITH BILLING ADDRESS
        it('should login user, Checking customer creation with billing address', function () {
            contacts.initiateCreateContact('Customers');
            createContactPage.form.displayName.sendKeys(testDataContact.createContactDetails[9].displayName);
            browser.findElement(by.xpath('//*[contains(text(),"Add Communication Info")]')).click();
            browser.findElement(by.xpath("//div[@id='mobile-0']//*[@ng-model='phone.phoneNumber']")).
            sendKeys(testDataContact.createContactDetails[9].phoneNumber);
            browser.driver.executeScript("arguments[0].scrollIntoView(true);", element(by.xpath('//*[contains(text(),"Add Address Details")]')).getWebElement());

            browser.findElement(by.xpath('//*[contains(text(),"Add Address Details")]')).click();
            browser.findElement(by.xpath('//div[@id="address-0"]//*[@ng-model="address.addressLine"]'))
                .sendKeys(testDataContact.createContactDetails[9].addressLine);
            browser.findElement(by.xpath('//div[@id="address-0"]//*[@ng-model="address.city"]'))
                .sendKeys(testDataContact.createContactDetails[9].city);
            browser.findElement(by.xpath('//div[@id="address-0"]//*[@ng-model="address.state"]'))
                .sendKeys(testDataContact.createContactDetails[9].state);
            browser.findElement(by.xpath('//div[@id="address-0"]//*[@ng-model="address.country"]'))
                .sendKeys(testDataContact.createContactDetails[9].country);
            browser.findElement(by.xpath('//div[@id="address-0"]//*[@ng-model="address.pinCode"]'))
                .sendKeys(testDataContact.createContactDetails[9].pinCode);

            createContactPage.form.submit.click();
            browser.sleep(3000);
            expect(element(by.xpath("//md-tabs/md-tabs-content-wrapper/md-tab-content[1]/div/md-content/" +
                "view-contacts/table/tbody/tr/td[3]")).getText()).toBe(testDataContact.createContactDetails[9].displayName);
            expect(element(by.xpath("//md-tabs/md-tabs-content-wrapper/md-tab-content[1]/div/md-content/" +
                "view-contacts/table/tbody/tr/td[5]")).getText()).toBe(testDataContact.createContactDetails[9].phoneNumber);
            contacts.deleteContacts('Customers');
            browser.sleep(1500);
        });

        //16.TEST CASE TO CREATE SUPPLIER CONTACT  WITH BILLING ADDRESS
        it('should login user,Checking supplier creation with billing address', function () {
            contacts.initiateCreateContact('Suppliers');
            createContactPage.form.displayName.sendKeys(testDataContact.createContactDetails[10].displayName);
            browser.findElement(by.xpath('//*[contains(text(),"Add Communication Info")]')).click();
            browser.findElement(by.xpath("//div[@id='mobile-0']//*[@ng-model='phone.phoneNumber']")).
            sendKeys(testDataContact.createContactDetails[10].phoneNumber);
            browser.driver.executeScript("arguments[0].scrollIntoView(true);", element(by.xpath('//*[contains(text(),"Add Address Details")]')).getWebElement());
            browser.findElement(by.xpath('//*[contains(text(),"Add Address Details")]')).click();
            browser.findElement(by.xpath('//div[@id="address-0"]//*[@ng-model="address.addressLine"]'))
                .sendKeys(testDataContact.createContactDetails[10].addressLine);
            browser.findElement(by.xpath('//div[@id="address-0"]//*[@ng-model="address.city"]'))
                .sendKeys(testDataContact.createContactDetails[10].city);
            browser.findElement(by.xpath('//div[@id="address-0"]//*[@ng-model="address.state"]'))
                .sendKeys(testDataContact.createContactDetails[10].state);
            browser.findElement(by.xpath('//div[@id="address-0"]//*[@ng-model="address.country"]'))
                .sendKeys(testDataContact.createContactDetails[10].country);
            browser.findElement(by.xpath('//div[@id="address-0"]//*[@ng-model="address.pinCode"]'))
                .sendKeys(testDataContact.createContactDetails[10].pinCode);

            createContactPage.form.submit.click();
            browser.sleep(500);
            expect(element(by.xpath("//md-tab-content[2]/div/md-content/view-contacts/table/tbody/tr/td[3]"))
                .getText()).toBe(testDataContact.createContactDetails[10].displayName);
            expect(element(by.xpath("//md-tab-content[2]/div/md-content/view-contacts/table/tbody/tr/td[5]"))
                .getText()).toBe(testDataContact.createContactDetails[10].phoneNumber);
            contacts.deleteContacts('Suppliers');
            browser.sleep(1000);

        });

        //17.TEST CASE TO CREATE CUSTOMER CONTACT WITH MORE BILLING ADDRESS
        it('should login user, Checking customer with more billing addresses ', function () {
            contacts.initiateCreateContact('Customers');
            createContactPage.form.displayName.sendKeys(testDataContact.createContactDetails[11].displayName);
            browser.findElement(by.xpath('//*[contains(text(),"Add Communication Info")]')).click();
            browser.findElement(by.xpath("//div[@id='mobile-0']//*[@ng-model='phone.phoneNumber']")).
            sendKeys(testDataContact.createContactDetails[11].phoneNumber);
            browser.driver.executeScript("arguments[0].scrollIntoView(true);", element(by.xpath('//*[contains(text(),"Add Address Details")]')).getWebElement());
            browser.findElement(by.xpath('//*[contains(text(),"Add Address Details")]')).click();
            contacts.sendAddressData('Customers','Billing',testDataContact.createContactDetails[11].address[0],0);
            browser.findElement(by.xpath('//md-content[@id="tab_Billing"]/div/span')).click();
            browser.findElement(by.xpath('//md-content[@id="tab_Billing"]/div/span')).click();
            browser.driver.executeScript("arguments[0].scrollIntoView(true);", element(by.xpath('//md-content[@id="tab_Billing"]//div[@id="address-1"]//*[@ng-model="address.addressLine"]')).getWebElement());
            contacts.sendAddressData('Customers','Billing',testDataContact.createContactDetails[11].address[1],1);
            browser.driver.executeScript("arguments[0].scrollIntoView(true);", element(by.xpath('//md-content[@id="tab_Billing"]//div[@id="address-2"]//*[@ng-model="address.addressLine"]')).getWebElement());
            contacts.sendAddressData('Customers','Billing',testDataContact.createContactDetails[11].address[2],2);
            createContactPage.form.submit.click();

            browser.sleep(300);
            expect(element(by.xpath('//md-content[@id="contact_Customers"]/view-contacts/table/tbody/tr/td[3]')).getText()).toBe(testDataContact.createContactDetails[11].displayName);
            expect(element(by.xpath('//md-content[@id="contact_Customers"]/view-contacts/table/tbody/tr/td[5]')).getText()).toBe(testDataContact.createContactDetails[11].phoneNumber);
            contacts.deleteContacts('Customers');
            browser.sleep(1500);
        });

        //18.TEST CASE TO CREATE SUPPLIER CONTACT WITH WITH MORE BILLING ADDRESS
        it('should login user, Checking suppliers with more billing addresses', function () {
            contacts.initiateCreateContact('Suppliers');
            createContactPage.form.displayName.sendKeys(testDataContact.createContactDetails[12].displayName);
            browser.findElement(by.xpath('//*[contains(text(),"Add Communication Info")]')).click();
            browser.findElement(by.xpath("//div[@id='mobile-0']//*[@ng-model='phone.phoneNumber']")).
            sendKeys(testDataContact.createContactDetails[12].phoneNumber);
            browser.driver.executeScript("arguments[0].scrollIntoView(true);", element(by.xpath('//*[contains(text(),"Add Address Details")]')).getWebElement());
            browser.findElement(by.xpath('//*[contains(text(),"Add Address Details")]')).click();
            browser.findElement(by.xpath('//md-content[@id="tab_Billing"]/div/span')).click();
            browser.findElement(by.xpath('//md-content[@id="tab_Billing"]/div/span')).click();
            browser.sleep(200);
            contacts.sendAddressData('Suppliers','Billing',testDataContact.createContactDetails[12].address[0],0);
            browser.driver.executeScript("arguments[0].scrollIntoView(true);", element(by.xpath('//md-content[@id="tab_Billing"]//div[@id="address-1"]//*[@ng-model="address.addressLine"]')).getWebElement());
            contacts.sendAddressData('Suppliers','Billing',testDataContact.createContactDetails[12].address[1],1);
            browser.driver.executeScript("arguments[0].scrollIntoView(true);", element(by.xpath('//md-content[@id="tab_Billing"]//div[@id="address-2"]//*[@ng-model="address.addressLine"]')).getWebElement());

            contacts.sendAddressData('Suppliers','Billing',testDataContact.createContactDetails[12].address[2],2);

            createContactPage.form.submit.click();
            browser.sleep(3000);


            expect(element(by.xpath('//md-content[@id=\'contact_Suppliers\']//view-contacts/table/tbody/tr/td[3]')).getText()).toBe(testDataContact.createContactDetails[12].displayName);
            expect(element(by.xpath('//md-content[@id=\'contact_Suppliers\']//view-contacts/table/tbody/tr/td[5]')).getText()).toBe(testDataContact.createContactDetails[12].phoneNumber);
            contacts.deleteContacts('Suppliers');
            browser.sleep(1500);
        });

        //19.TEST CASE TO CREATE CUSTOMER CONTACT WITH BILLING,SHIPPING,RECEIVING AND INVOICE ADDRESS
        it('should login user, Checking customer creation with billing,shipping,receiving and invoice', function () {
            contacts.initiateCreateContact('Customers');
            createContactPage.form.displayName.sendKeys(testDataContact.createContactDetails[13].displayName);
            browser.findElement(by.xpath('//*[contains(text(),"Add Communication Info")]')).click();
            browser.findElement(by.xpath("//div[@id='mobile-0']//*[@ng-model='phone.phoneNumber']")).
            sendKeys(testDataContact.createContactDetails[13].phoneNumber);
            browser.driver.executeScript("arguments[0].scrollIntoView(true);", element(by.xpath('//*[contains(text(),"Add Address Details")]')).getWebElement());
            browser.findElement(by.xpath('//*[contains(text(),"Add Address Details")]')).click();
            contacts.sendAddressData('Customers','Billing',testDataContact.createContactDetails[13].billingAddress,0);
            browser.findElement(by.xpath('//md-tab-item[contains(text(),"Shipping")]')).click();
            contacts.sendAddressData('Customers','Shipping',testDataContact.createContactDetails[13].shippingAddress,0);
            browser.findElement(by.xpath('//md-tab-item[contains(text(),"Receiving")]')).click();
            contacts.sendAddressData('Customer','Receiving',testDataContact.createContactDetails[13].receivingAddress,0);
            browser.findElement(by.xpath('//md-tab-item[contains(text(),"Invoice")]')).click();
            contacts.sendAddressData('Customer','Invoice',testDataContact.createContactDetails[13].invoiceAddress,0);
            createContactPage.form.submit.click();
            browser.sleep(3000);
            expect(element(by.xpath('//md-content[@id=\'contact_Customers\']//view-contacts/table/tbody/tr/td[3]')).getText()).toBe(testDataContact.createContactDetails[13].displayName);
            expect(element(by.xpath('//md-content[@id=\'contact_Customers\']//view-contacts/table/tbody/tr/td[5]')).getText()).toBe(testDataContact.createContactDetails[13].phoneNumber);

            contacts.deleteContacts('Customers');
            browser.sleep(1500);
        });

        // 20. TEST CASE TO CREATE SUPPLIER CONTACT WITH WITH BILLING,SHIPPING,RECEIVING AND INVOICE ADDRESS
        it('should login user, Checking Create supplier contact with shipping billing, receiving and invoice ', function () {
              contacts.initiateCreateContact('Suppliers');
              createContactPage.form.displayName.sendKeys(testDataContact.createContactDetails[14].displayName);
              browser.findElement(by.xpath('//*[contains(text(),"Add Communication Info")]')).click();
              browser.findElement(by.xpath("//div[@id='mobile-0']//*[@ng-model='phone.phoneNumber']")).
              sendKeys(testDataContact.createContactDetails[14].phoneNumber);
              browser.driver.executeScript("arguments[0].scrollIntoView(true);", element(by.xpath('//*[contains(text(),"Add Address Details")]')).getWebElement());
              browser.findElement(by.xpath('//*[contains(text(),"Add Address Details")]')).click();
              contacts.sendAddressData('Customers','Billing',testDataContact.createContactDetails[14].billingAddress,0);
              browser.findElement(by.xpath('//md-tab-item[contains(text(),"Shipping")]')).click();

              contacts.sendAddressData('Customers','Shipping',testDataContact.createContactDetails[14].shippingAddress,0);
              browser.findElement(by.xpath('//md-tab-item[contains(text(),"Receiving")]')).click();
              contacts.sendAddressData('Customer','Receiving',testDataContact.createContactDetails[14].receivingAddress,0);
              browser.findElement(by.xpath('//md-tab-item[contains(text(),"Invoice")]')).click();
              contacts.sendAddressData('Customer','Invoice',testDataContact.createContactDetails[14].invoiceAddress,0);
              createContactPage.form.submit.click();
              browser.sleep(3000);
              expect(element(by.xpath('//md-content[@id=\'contact_Suppliers\']//view-contacts/table/tbody/tr/td[3]')).getText()).toBe(testDataContact.createContactDetails[14].displayName);
              expect(element(by.xpath('//md-content[@id=\'contact_Suppliers\']//view-contacts/table/tbody/tr/td[5]')).getText()).toBe(testDataContact.createContactDetails[14].phoneNumber);
              contacts.deleteContacts('Suppliers');
              browser.sleep(1500);
          });

        //21.TEST CASE TO CREATE CUSTOMER CONTACT WITH MORE BILLING,SHIPPING,RECEIVING AND INVOICE ADDRESS
        it('should login user, Checking Create customer contactwith more shipping,billing,receiving and invoice addresses', function () {
            contacts.initiateCreateContact('Customers');
            createContactPage.form.displayName.sendKeys(testDataContact.createContactDetails[15].displayName);
            browser.findElement(by.xpath('//*[contains(text(),"Add Communication Info")]')).click();
            browser.findElement(by.xpath("//div[@id='mobile-0']//*[@ng-model='phone.phoneNumber']")).
            sendKeys(testDataContact.createContactDetails[15].phoneNumber);
            browser.driver.executeScript("arguments[0].scrollIntoView(true);", element(by.xpath('//*[contains(text(),"Add Address Details")]')).getWebElement());
            browser.findElement(by.xpath('//*[contains(text(),"Add Address Details")]')).click();
            browser.findElement(by.xpath('//md-content[@id="tab_Billing"]/div/span')).click();
            contacts.sendAddressData('Customers','Billing',testDataContact.createContactDetails[15].billingAddress[0],0);
            contacts.sendAddressData('Customers','Billing',testDataContact.createContactDetails[15].billingAddress[1],1);
            browser.driver.executeScript("arguments[0].scrollIntoView(true);", element(by.xpath('//md-tab-item[contains(text(),\'Shipping\')]')).getWebElement());
            browser.findElement(by.xpath('//md-tab-item[contains(text(),"Shipping")]')).click();
            browser.findElement(by.xpath('//md-content[@id="tab_Shipping"]/div/span')).click();
            contacts.sendAddressData('Customers','Shipping',testDataContact.createContactDetails[15].shippingAddress[0],0);
            contacts.sendAddressData('Customers','Shipping',testDataContact.createContactDetails[15].shippingAddress[1],1);

            browser.driver.executeScript("arguments[0].scrollIntoView(true);", element(by.xpath('//md-tab-item[contains(text(),"Receiving")')).getWebElement());
            browser.findElement(by.xpath('//md-content[@id="tab_Receiving"]/div/span')).click();
            browser.findElement(by.xpath('//md-tab-item[contains(text(),"Receiving")]')).click();
            contacts.sendAddressData('Customers','Receiving',testDataContact.createContactDetails[15].receivingAddress[0]);
            contacts.sendAddressData('Customers','Receiving',testDataContact.createContactDetails[15].receivingAddress[1]);
            browser.driver.executeScript("arguments[0].scrollIntoView(true);", element(by.xpath('//md-tab-item[contains(text(),"Invoice")]')).getWebElement());
            browser.findElement(by.xpath('//md-tab-item[contains(text(),"Invoice")]')).click();
            browser.findElement(by.xpath('//md-content[@id="tab_Invoice"]/div/span')).click();
            contacts.sendAddressData('Customers','Invoice',testDataContact.createContactDetails[15].invoiceAddress[0],0);
            contacts.sendAddressData('Customers','Invoice',testDataContact.createContactDetails[15].invoiceAddress[1],1);

            createContactPage.form.submit.click();
            browser.sleep(3000);
            expect(element(by.xpath("//md-tabs/md-tabs-content-wrapper/md-tab-content[1]/div/md-content/" +
                "view-contacts/table/tbody/tr/td[3]")).getText()).toBe(testDataContact.createContactDetails[15].displayName);
            expect(element(by.xpath("//md-tabs/md-tabs-content-wrapper/md-tab-content[1]/div/md-content/" +
                "view-contacts/table/tbody/tr/td[5]")).getText()).toBe(testDataContact.createContactDetails[15].phoneNumber);

            contacts.deleteContacts('Customers');
            browser.sleep(500);

        });

         //22.TEST CASE TO CREATE SUPPLIER CONTACT WITH WITH MORE BILLING,SHIPPING,RECEIVING AND INVOICE ADDRESS
        it('should login user,  Checking Create supplier contactwith more shipping,billing,receiving and invoice addresses', function () {
            contacts.initiateCreateContact('Suppliers');
            createContactPage.form.displayName.sendKeys(testDataContact.createContactDetails[16].displayName);
            browser.findElement(by.xpath('//*[contains(text(),"Add Communication Info")]')).click();
            browser.findElement(by.xpath("//div[@id='mobile-0']//*[@ng-model='phone.phoneNumber']")).
            sendKeys(testDataContact.createContactDetails[16].phoneNumber);
            browser.driver.executeScript("arguments[0].scrollIntoView(true);", element(by.xpath('//*[contains(text(),"Add Address Details")]')).getWebElement());
            browser.findElement(by.xpath('//*[contains(text(),"Add Address Details")]')).click();
            browser.findElement(by.xpath('//md-content[@id="tab_Billing"]/div/span')).click();
            contacts.sendAddressData('Customers','Billing',testDataContact.createContactDetails[16].billingAddress[0],0);
            contacts.sendAddressData('Customers','Billing',testDataContact.createContactDetails[16].billingAddress[1],1);
            browser.findElement(by.xpath('//md-tab-item[contains(text(),"Shipping")]')).click();
            browser.findElement(by.xpath('//md-content[@id="tab_Shipping"]/div/span')).click();
            contacts.sendAddressData('Customers','Shipping',testDataContact.createContactDetails[16].shippingAddress[0],0);
            contacts.sendAddressData('Customers','Shipping',testDataContact.createContactDetails[16].shippingAddress[1],1);

            browser.findElement(by.xpath('//md-tab-item[contains(text(),"Receiving")]')).click();
            browser.findElement(by.xpath('//md-content[@id="tab_Receiving"]/div/span')).click();
            contacts.sendAddressData('Customers','Receiving',testDataContact.createContactDetails[16].receivingAddress[0],0);
            contacts.sendAddressData('Customers','Receiving',testDataContact.createContactDetails[16].receivingAddress[1],1);

            browser.findElement(by.xpath('//md-tab-item[contains(text(),"Invoice")]')).click();
            browser.findElement(by.xpath('//md-content[@id="tab_Invoice"]/div/span')).click();
            contacts.sendAddressData('Customers','Invoice',testDataContact.createContactDetails[16].invoiceAddress[0],0);
            contacts.sendAddressData('Customers','Invoice',testDataContact.createContactDetails[16].invoiceAddress[1],1);

            createContactPage.form.submit.click();
            expect(element(by.xpath("//md-content[@id=\'tab_Shipping\']/div/md-content/view-contacts/table/tbody/tr/td[3]")).getText()).toBe(testDataContact.createContactDetails[16].displayName);
            expect(element(by.xpath("//md-content[@id=\'tab_Shipping\']/div/md-content/view-contacts/table/tbody/tr/td[5]")).getText()).toBe(testDataContact.createContactDetails[16].phoneNumber);
            contacts.deleteContacts('Suppliers');
            browser.sleep(500);

        });
        //23.TEST CASE TO CHECK REMOVING OF MOBILE NUMBERS AND EMAIL IDS WHILE CREATING CUSTOMER CONTACT
        it('should login user, Checking removing mobile and emails while creating customer', function () {
            contacts.initiateCreateContact('Customers');
            createContactPage.form.displayName.sendKeys(testDataContact.createContactDetails[7].displayName);
            browser.findElement(by.xpath('//*[contains(text(),"Add Communication Info")]')).click();

            browser.findElement(by.xpath("//div[@id='mobile-0']//*[@ng-model=\"phone.phoneType\"]")).click();
            browser.findElement(by.xpath("//div[@class='md-select-menu-container md-active md-clickable']" +
                "//*[contains(text(),'"+testDataContact.createContactDetails[7].phoneNumbers[0].type+"')]")).click();
            browser.findElement(by.xpath("//div[@id='mobile-0']//*[@ng-model='phone.phoneNumber']")).
            sendKeys(testDataContact.createContactDetails[7].phoneNumbers[0].phoneNumber);

            browser.findElement(by.xpath("//*[@id=\"mobile-0\"]/div[1]/md-icon")).click();
            browser.driver.executeScript("arguments[0].scrollIntoView(true);", element(by.xpath('//div[@id=\'mobile-1\']//*[@ng-model=\'phone.phoneNumber\']')).getWebElement());

            browser.findElement(by.xpath("//div[@id='mobile-1']//*[@ng-model=\"phone.phoneType\"]")).click();
            browser.findElement(by.xpath("//div[@class='md-select-menu-container md-active md-clickable']" +
                "//*[contains(text(),'"+testDataContact.createContactDetails[7].phoneNumbers[1].type+"')]")).click();
            browser.findElement(by.xpath("//div[@id='mobile-1']//*[@ng-model='phone.phoneNumber']")).
            sendKeys(testDataContact.createContactDetails[7].phoneNumbers[1].phoneNumber);
            browser.findElement(by.xpath("//*[@id=\"mobile-1\"]/div[1]/md-icon")).click();

            browser.findElement(by.xpath("//div[@id='email-0']//*[@ng-model=\"email.emailType\"]")).click();
            browser.findElement(by.xpath("//div[@class='md-select-menu-container md-active md-clickable']" +
                "//*[contains(text(),'"+testDataContact.createContactDetails[7].emails[0].type+"')]")).click();
            browser.findElement(by.xpath("//div[@id='email-0']//*[@ng-model='email.email']")).
            sendKeys(testDataContact.createContactDetails[7].emails[0].email);

            browser.findElement(by.xpath("//*[@id=\"email-0\"]/div[1]/md-icon")).click();
            browser.driver.executeScript("arguments[0].scrollIntoView(true);", element(by.xpath('//div[@id=\'email-1\']//*[@ng-model=\'email.email\']')).getWebElement());

            browser.findElement(by.xpath("//div[@id='email-1']//*[@ng-model=\"email.emailType\"]")).click();
            browser.findElement(by.xpath("//div[@class='md-select-menu-container md-active md-clickable']" +
                "//*[contains(text(),'"+testDataContact.createContactDetails[7].emails[1].type+"')]")).click();
            browser.findElement(by.xpath("//div[@id='email-1']//*[@ng-model='email.email']")).
            sendKeys(testDataContact.createContactDetails[7].emails[1].email);
            browser.findElement(by.xpath("//*[@id=\"email-1\"]/div[1]/md-icon")).click();
            createContactPage.form.element(by.css('[aria-label="close dialog"]')).click();


        });

        //24.TEST CASE TO CHECK REMOVING OF MOBILE NUMBERS AND EMAIL IDS WHILE CREATING SUPPLIER CONTACT
        it('should login user,  Checking removing mobile and emails while creating supplier', function () {
            contacts.initiateCreateContact('Suppliers');
            createContactPage.form.displayName.sendKeys(testDataContact.createContactDetails[8].displayName);
            browser.findElement(by.xpath('//*[contains(text(),"Add Communication Info")]')).click();

            browser.findElement(by.xpath("//div[@id='mobile-0']//*[@ng-model=\"phone.phoneType\"]")).click();
            browser.findElement(by.xpath("//div[@class='md-select-menu-container md-active md-clickable']" +
                "//*[contains(text(),'"+testDataContact.createContactDetails[7].phoneNumbers[0].type+"')]")).click();
            browser.findElement(by.xpath("//div[@id='mobile-0']//*[@ng-model='phone.phoneNumber']")).
            sendKeys(testDataContact.createContactDetails[7].phoneNumbers[0].phoneNumber);

            browser.findElement(by.xpath("//*[@id=\"mobile-0\"]/div[1]/md-icon")).click();
            browser.driver.executeScript("arguments[0].scrollIntoView(true);", element(by.xpath('//div[@id=\'mobile-1\']//*[@ng-model=\'phone.phoneNumber\']')).getWebElement());

            browser.findElement(by.xpath("//div[@id='mobile-1']//*[@ng-model=\"phone.phoneType\"]")).click();
            browser.findElement(by.xpath("//div[@class='md-select-menu-container md-active md-clickable']" +
                "//*[contains(text(),'"+testDataContact.createContactDetails[7].phoneNumbers[1].type+"')]")).click();
            browser.findElement(by.xpath("//div[@id='mobile-1']//*[@ng-model='phone.phoneNumber']")).
            sendKeys(testDataContact.createContactDetails[7].phoneNumbers[1].phoneNumber);
            browser.findElement(by.xpath("//*[@id=\"mobile-1\"]/div[1]/md-icon")).click();

            browser.findElement(by.xpath("//div[@id='email-0']//*[@ng-model=\"email.emailType\"]")).click();
            browser.findElement(by.xpath("//div[@class='md-select-menu-container md-active md-clickable']" +
                "//*[contains(text(),'"+testDataContact.createContactDetails[7].emails[0].type+"')]")).click();
            browser.findElement(by.xpath("//div[@id='email-0']//*[@ng-model='email.email']")).
            sendKeys(testDataContact.createContactDetails[7].emails[0].email);

            browser.findElement(by.xpath("//*[@id=\"email-0\"]/div[1]/md-icon")).click();
            browser.driver.executeScript("arguments[0].scrollIntoView(true);", element(by.xpath('//div[@id=\'email-1\']//*[@ng-model=\'email.email\']')).getWebElement());

            browser.findElement(by.xpath("//div[@id='email-1']//*[@ng-model=\"email.emailType\"]")).click();
            browser.findElement(by.xpath("//div[@class='md-select-menu-container md-active md-clickable']" +
                "//*[contains(text(),'"+testDataContact.createContactDetails[7].emails[1].type+"')]")).click();
            browser.findElement(by.xpath("//div[@id='email-1']//*[@ng-model='email.email']")).
            sendKeys(testDataContact.createContactDetails[7].emails[1].email);
            browser.findElement(by.xpath("//*[@id=\"email-1\"]/div[1]/md-icon")).click();
            createContactPage.form.element(by.css('[aria-label="close dialog"]')).click();

        });

        //25.TEST CASE TO CHECK REMOVING OF BILLING,SHIPPING,RECEIVING AND INVOICE ADDRESS WHILE CREATING CUSTOMER CONTACT
        it('should login user, Checking removing all addresses while creating customer ', function () {
            contacts.initiateCreateContact('Customers');
            createContactPage.form.displayName.sendKeys(testDataContact.createContactDetails[9].displayName);
            browser.findElement(by.xpath('//*[contains(text(),"Add Communication Info")]')).click();
            browser.findElement(by.xpath("//div[@id='mobile-0']//*[@ng-model='phone.phoneNumber']")).
            sendKeys(testDataContact.createContactDetails[9].phoneNumber);
            browser.driver.executeScript("arguments[0].scrollIntoView(true);", element(by.xpath('//*[contains(text(),"Add Address Details")]')).getWebElement());
            browser.findElement(by.xpath('//*[contains(text(),"Add Address Details")]')).click();
            browser.findElement(by.xpath('//md-content[@id="tab_Billing"]/div/span')).click();
            browser.findElement(by.xpath('//div[@id="address-1"]//*[@ng-model="address.addressLine"]'))
                .sendKeys(testDataContact.createContactDetails[9].addressLine);
            browser.findElement(by.xpath('//div[@id="address-1"]//*[@ng-model="address.city"]'))
                .sendKeys(testDataContact.createContactDetails[9].city);
            browser.findElement(by.xpath('//div[@id="address-1"]//*[@ng-model="address.state"]'))
                .sendKeys(testDataContact.createContactDetails[9].state);
            browser.findElement(by.xpath('//div[@id="address-1"]//*[@ng-model="address.country"]'))
                .sendKeys(testDataContact.createContactDetails[9].country);
            browser.findElement(by.xpath('//div[@id="address-1"]//*[@ng-model="address.pinCode"]'))
                .sendKeys(testDataContact.createContactDetails[9].pinCode);
            browser.findElement(by.xpath('//*[@id="address-1"]/div[1]/span')).click();
            createContactPage.form.element(by.css('[aria-label="close dialog"]')).click();

        });

        //26.TEST CASE TO CHECK REMOVING OF BILLING,SHIPPING,RECEIVING AND INVOICE ADDRESS WHILE CREATING SUPPLER CONTACT
        it('should login user, Checking removing all addresses while creating supplier "', function () {
            contacts.initiateCreateContact('Suppliers');
            createContactPage.form.displayName.sendKeys(testDataContact.createContactDetails[10].displayName);
            browser.findElement(by.xpath('//*[contains(text(),"Add Communication Info")]')).click();
            browser.findElement(by.xpath("//div[@id='mobile-0']//*[@ng-model='phone.phoneNumber']")).
            sendKeys(testDataContact.createContactDetails[10].phoneNumber);
            browser.driver.executeScript("arguments[0].scrollIntoView(true);", element(by.xpath('//*[contains(text(),"Add Address Details")]')).getWebElement());
            browser.findElement(by.xpath('//*[contains(text(),"Add Address Details")]')).click();
            browser.findElement(by.xpath('//md-content[@id="tab_Billing"]/div/span')).click();
            browser.findElement(by.xpath('//div[@id="address-1"]//*[@ng-model="address.addressLine"]'))
                    .sendKeys(testDataContact.createContactDetails[10].addressLine);
            browser.findElement(by.xpath('//div[@id="address-1"]//*[@ng-model="address.city"]'))
                    .sendKeys(testDataContact.createContactDetails[10].city);
            browser.findElement(by.xpath('//div[@id="address-1"]//*[@ng-model="address.state"]'))
                    .sendKeys(testDataContact.createContactDetails[10].state);
            browser.findElement(by.xpath('//div[@id="address-1"]//*[@ng-model="address.country"]'))
                    .sendKeys(testDataContact.createContactDetails[10].country);
            browser.findElement(by.xpath('//div[@id="address-1"]//*[@ng-model="address.pinCode"]'))
                    .sendKeys(testDataContact.createContactDetails[10].pinCode);
            browser.findElement(by.xpath('//*[@id="address-1"]/div[1]/span')).click();
            createContactPage.form.element(by.css('[aria-label="close dialog"]')).click();


        });

        //27.TEST CASE TO CHECK MINIMUM ONE EMAIL OR PHONE IS REQUIRED
        it('should login user, Create contact with phone validation and by taking email',function () {
            contacts.initiateCreateContact('Customers');
            createContactPage.form.displayName.sendKeys(testDataContact.createContactDetails[19].displayName);
            element(by.xpath('//*[contains(text(),"Add Communication Info")]')).click();
            createContactPage.form.phoneNumber.click();
            createContactPage.form.element(by.xpath('//*[@id=\'email-0\']/*//*[@ng-model=\'email.email\']')).click();
            createContactPage.form.element(by.xpath('//*[@id=\'email-0\']/*//*[@ng-model=\'email.email\']')).sendKeys(testDataContact.createContactDetails[19].email);
            createContactPage.form.submit.click();
            browser.sleep(3000);
            //*[@id="contactTable"]/tbody/tr
            expect(element(by.xpath('//md-content[@id="contact_Customers"]/view-contacts/table/tbody/tr/td[3]')).getText()).toBe(testDataContact.createContactDetails[19].displayName);
            expect(element(by.xpath('//md-content[@id="contact_Customers"]/view-contacts/table/tbody/tr/td[4]')).getText()).toBe(testDataContact.createContactDetails[19].email);
            browser.sleep(2000);
            contacts.deleteContacts('Customers');
            browser.sleep(2000);
            contacts.initiateCreateContact('Customers');
            createContactPage.form.displayName.sendKeys(testDataContact.createContactDetails[19].displayName);
            element(by.xpath('//*[contains(text(),"Add Communication Info")]')).click();
            createContactPage.form.element(by.xpath('//*[@id=\'email-0\']/*//*[@ng-model=\'email.email\']')).click();
            createContactPage.form.phoneNumber.click();
            createContactPage.form.phoneNumber.sendKeys(testDataContact.createContactDetails[19].phoneNumber);
            createContactPage.form.submit.click();
            browser.sleep(3000);
            //*[@id="contactTable"]/tbody/tr
            expect(element(by.xpath('//md-content[@id="contact_Customers"]/view-contacts/table/tbody/tr/td[3]')).getText()).toBe(testDataContact.createContactDetails[19].displayName);
            expect(element(by.xpath('//md-content[@id="contact_Customers"]/view-contacts/table/tbody/tr/td[5]')).getText()).toBe(testDataContact.createContactDetails[19].phoneNumber);
            browser.sleep(2000);
            contacts.deleteContacts('Customers');
            browser.sleep(2000);
        });
        //28.TEST CASE TO CHECK MINIMUM ONE EMAIL OR PHONE IS REQUIRED
        it('should login user, Create contact with email validation and by taking email', function () {
            contacts.initiateCreateContact('Customers');
            createContactPage.form.displayName.sendKeys(testDataContact.createContactDetails[20].displayName);
            element(by.xpath('//*[contains(text(),"Add Communication Info")]')).click();
            createContactPage.form.element(by.xpath('//*[@id=\'email-0\']/*//*[@ng-model=\'email.email\']')).click();
            createContactPage.form.phoneNumber.click();
            createContactPage.form.phoneNumber.sendKeys(testDataContact.createContactDetails[20].phoneNumber);
            createContactPage.form.submit.click();
            browser.sleep(3000);
            //*[@id="contactTable"]/tbody/tr
            expect(element(by.xpath('//md-content[@id="contact_Customers"]/view-contacts/table/tbody/tr/td[3]')).getText()).toBe(testDataContact.createContactDetails[20].displayName);
            expect(element(by.xpath('//md-content[@id="contact_Customers"]/view-contacts/table/tbody/tr/td[5]')).getText()).toBe(testDataContact.createContactDetails[20].phoneNumber);
            browser.sleep(2000);
            contacts.deleteContacts('Customers');
            browser.sleep(2000);
            contacts.initiateCreateContact('Customers');
            createContactPage.form.displayName.sendKeys(testDataContact.createContactDetails[20].displayName);
            element(by.xpath('//*[contains(text(),"Add Communication Info")]')).click();
            createContactPage.form.phoneNumber.sendKeys('');
            createContactPage.form.element(by.xpath('//*[@id=\'email-0\']/*//*[@ng-model=\'email.email\']')).click();
            createContactPage.form.element(by.xpath('//*[@id=\'email-0\']/*//*[@ng-model=\'email.email\']')).sendKeys(testDataContact.createContactDetails[20].email);
            createContactPage.form.submit.click();
            browser.sleep(3000);
            //*[@id="contactTable"]/tbody/tr
            expect(element(by.xpath('//md-content[@id="contact_Customers"]/view-contacts/table/tbody/tr/td[3]')).getText()).toBe(testDataContact.createContactDetails[20].displayName);
            expect(element(by.xpath('//md-content[@id="contact_Customers"]/view-contacts/table/tbody/tr/td[4]')).getText()).toBe(testDataContact.createContactDetails[20].email);
            browser.sleep(2000);
            contacts.deleteContacts('Customers');
            browser.sleep(2000);

        });
    });
});
