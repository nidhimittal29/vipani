'use strict';
var path=require('path');
var config = browser.params;
var user=require('../../account/common.util.e2e/signin.common.po');
var contacts=require('../../account/common.util.e2e/contacts.common.po');
var global = require('../../account/common.util.e2e/global.common.po');
var loginDetails=require('./login');
var testDataContact;
describe('Create Contact View', function() {
    var loginPage, createGroupPage, testDataGroup;
    var loadPage = function () {
        browser.manage().deleteAllCookies();
        browser.get('http://localhost:3000/#!/signin');

    };

    describe('with local auth', function () {

        beforeAll(function(){
            global.loadPage('http://localhost:3000/#!/register');
            user.registerProcess(loginDetails[0]);
        });
        beforeEach(function () {
            loadPage();
            user.signin(loginDetails[0]);
            user.getForm().submit.click();
            browser.sleep('3000');
            testDataGroup = require('./../group.testdata');
            testDataContact=require('../contact.testdata.json');
            createGroupPage = require('./createGrooup.po');

        });

        afterEach(function () {
            user.logout();
        });

             //1.TEST CASE TO CREATE GROUP WITH GROUP TYPE IS CUSTOMER WHEN GROUP LIST IS EMPTY AND CONTACTS SELECTION TYPE IS CUSTOM
            it('should login user, Checking Create group and redirecting to "/"', function () {
            contacts.initiateCreateGroup();
            createGroupPage.form.groupType.click();
            element(by.xpath("//div[@class='md-select-menu-container md-active md-clickable']" +
                "//*[contains(text(),'"+testDataGroup.groupDetails[0].groupType+"')]")).click();
            createGroupPage.form.groupName.sendKeys(testDataGroup.groupDetails[0].groupName);
            createGroupPage.form.groupClassificationType.click();
            element(by.xpath("//div[@class='md-select-menu-container md-active md-clickable']" +
                "//*[contains(text(),'"+testDataGroup.groupDetails[0].groupClassificationType+"')]")).click();
            createGroupPage.form.createGroup.click();
            browser.sleep('2000');
            expect(browser.findElement(by.xpath('//*[@class="ng-scope md-no-scroll md-active"]//*[@id="groupTable"]/tbody/tr[1]/td[3]')).getText()).toBe(testDataGroup.groupDetails[0].groupName);
            expect(browser.findElement(by.xpath('//*[@class="ng-scope md-no-scroll md-active"]//*[@id="groupTable"]/tbody/tr[1]/td[4]')).getText()).toBe(testDataGroup.groupDetails[0].groupType);
            contacts.deleteGroups();
        });

        //2.TEST CASE TO CREATE GROUP WITH GROUP TYPE IS SUPPLIER AND CONTACTS SELECTION TYPE IS CUSTOM
        it('should login user, Checking Create group and redirecting to "/"', function () {
            contacts.initiateCreateGroup();
            createGroupPage.form.groupType.click();
            element(by.xpath("//div[@class='md-select-menu-container md-active md-clickable']" +
                "//*[contains(text(),'"+testDataGroup.groupDetails[1].groupType+"')]")).click();
            createGroupPage.form.groupName.sendKeys(testDataGroup.groupDetails[1].groupName);
            createGroupPage.form.groupClassificationType.click();
            element(by.xpath("//div[@class='md-select-menu-container md-active md-clickable']" +
                "//*[contains(text(),'"+testDataGroup.groupDetails[1].groupClassificationType+"')]")).click();
            createGroupPage.form.createGroup.click();
            browser.sleep('2000');
            expect(browser.findElement(by.xpath('//*[@class="ng-scope md-no-scroll md-active"]//*[@id="groupTable"]/tbody/tr[1]/td[3]')).getText()).toBe(testDataGroup.groupDetails[1].groupName);
            expect(browser.findElement(by.xpath('//*[@class="ng-scope md-no-scroll md-active"]//*[@id="groupTable"]/tbody/tr[1]/td[4]')).getText()).toBe(testDataGroup.groupDetails[1].groupType);
            contacts.deleteGroups();
        });

        //3.TEST CASE TO CREATE GROUP WITH GROUP TYPE IS CUSTOM AND CONTACTS SELECTION TYPE IS LOCATION CRITERIA
        it('should login user, Checking Create group and redirecting to "/"', function () {
            contacts.initiateCreateGroup();
            createGroupPage.form.groupType.click();
            element(by.xpath("//div[@class='md-select-menu-container md-active md-clickable']" +
                "//*[contains(text(),'"+testDataGroup.groupDetails[2].groupType+"')]")).click();
            createGroupPage.form.groupName.sendKeys(testDataGroup.groupDetails[2].groupName);
            createGroupPage.form.groupClassificationType.click();
            element(by.xpath("//div[@class='md-select-menu-container md-active md-clickable']" +
                "//*[contains(text(),'"+testDataGroup.groupDetails[2].groupClassificationType+"')]")).click();
            browser.driver.executeScript("arguments[0].scrollIntoView(true);", element(by.model('address.city')).getWebElement());
            createGroupPage.form.city.sendKeys(testDataGroup.groupDetails[2].city);
            createGroupPage.form.state.sendKeys(testDataGroup.groupDetails[2].state);
            createGroupPage.form.country.sendKeys(testDataGroup.groupDetails[2].country);
            createGroupPage.form.pinCode.sendKeys(testDataGroup.groupDetails[2].pinCode);
            createGroupPage.form.createGroup.click();
            browser.sleep('3000');
            expect(browser.findElement(by.xpath('//*[@class="ng-scope md-no-scroll md-active"]//*[@id="groupTable"]/tbody/tr[1]/td[3]')).getText()).toBe(testDataGroup.groupDetails[2].groupName);
            expect(browser.findElement(by.xpath('//*[@class="ng-scope md-no-scroll md-active"]//*[@id="groupTable"]/tbody/tr[1]/td[4]')).getText()).toBe(testDataGroup.groupDetails[2].groupType);
            element(by.xpath('//*[@class="ng-scope md-no-scroll md-active"]//*[@id="groupTable"]/tbody/tr[1]/td[3]')).click();
            browser.sleep(200);
            expect(element(by.xpath('//*[@aria-label=\'Search for Contacts to add to Group\']')).isPresent()).toBe(false);
            element(by.xpath('//form[@name="editGroupForm"]//*[@aria-label="close dialog"]')).click();
            contacts.deleteGroups();
        });

        //4.TEST CASE TO CREATE GROUP WITH GROUP TYPE IS SUPPLIER AND CONTACTS SELECTION TYPE IS LOCATION CRITERIA
        it('should login user, Checking Create group and redirecting to "/"', function () {
            contacts.initiateCreateGroup();
            createGroupPage.form.groupType.click();
            element(by.xpath("//div[@class='md-select-menu-container md-active md-clickable']" +
                "//*[contains(text(),'"+testDataGroup.groupDetails[3].groupType+"')]")).click();
            createGroupPage.form.groupName.sendKeys(testDataGroup.groupDetails[3].groupName);
            createGroupPage.form.groupClassificationType.click();
            element(by.xpath("//div[@class='md-select-menu-container md-active md-clickable']" +
                "//*[contains(text(),'"+testDataGroup.groupDetails[3].groupClassificationType+"')]")).click();
            browser.driver.executeScript("arguments[0].scrollIntoView(true);", element(by.model('address.city')).getWebElement());
            createGroupPage.form.city.sendKeys(testDataGroup.groupDetails[3].city);
            createGroupPage.form.state.sendKeys(testDataGroup.groupDetails[3].state);
            createGroupPage.form.country.sendKeys(testDataGroup.groupDetails[3].country);
            createGroupPage.form.pinCode.sendKeys(testDataGroup.groupDetails[3].pinCode);
            createGroupPage.form.createGroup.click();
            browser.sleep('3000');
            expect(browser.findElement(by.xpath('//*[@class="ng-scope md-no-scroll md-active"]//*[@id="groupTable"]/tbody/tr[1]/td[3]')).getText()).toBe(testDataGroup.groupDetails[3].groupName);
            expect(browser.findElement(by.xpath('//*[@class="ng-scope md-no-scroll md-active"]//*[@id="groupTable"]/tbody/tr[1]/td[4]')).getText()).toBe(testDataGroup.groupDetails[3].groupType);
            contacts.deleteGroups();
        });

        //5.TEST CASE TO CREATE GROUP WITH GROUP TYPE IS CUSTOM AND CONTACTS SELECTION TYPE IS ALL
        it('should login user, Checking Create group and redirecting to "/"', function () {
            contacts.initiateCreateGroup();
            createGroupPage.form.groupType.click();
            element(by.xpath("//div[@class='md-select-menu-container md-active md-clickable']" +
                "//*[contains(text(),'"+testDataGroup.groupDetails[4].groupType+"')]")).click();
            createGroupPage.form.groupName.sendKeys(testDataGroup.groupDetails[4].groupName);
            createGroupPage.form.groupClassificationType.click();
            element(by.xpath("//div[@class='md-select-menu-container md-active md-clickable']" +
                "//*[contains(text(),'"+testDataGroup.groupDetails[4].groupClassificationType+"')]")).click();
            createGroupPage.form.createGroup.click();
            browser.sleep('3000');
            expect(browser.findElement(by.xpath('//*[@class="ng-scope md-no-scroll md-active"]//*[@id="groupTable"]/tbody/tr[1]/td[3]')).getText()).toBe(testDataGroup.groupDetails[4].groupName);
            expect(browser.findElement(by.xpath('//*[@class="ng-scope md-no-scroll md-active"]//*[@id="groupTable"]/tbody/tr[1]/td[4]')).getText()).toBe(testDataGroup.groupDetails[4].groupType);
            contacts.deleteGroups();
        });

        //6.TEST CASE TO CREATE GROUP WITH GROUP TYPE IS SUPPLIER AND CONTACTS SELECTION TYPE IS ALL
        it('should login user, Checking Create group and redirecting to "/"', function () {
            contacts.initiateCreateGroup();
            createGroupPage.form.groupType.click();
            element(by.xpath("//div[@class='md-select-menu-container md-active md-clickable']" +
                "//*[contains(text(),'"+testDataGroup.groupDetails[5].groupType+"')]")).click();
            createGroupPage.form.groupName.sendKeys(testDataGroup.groupDetails[5].groupName);
            createGroupPage.form.groupClassificationType.click();
            element(by.xpath("//div[@class='md-select-menu-container md-active md-clickable']" +
                "//*[contains(text(),'"+testDataGroup.groupDetails[5].groupClassificationType+"')]")).click();
            createGroupPage.form.createGroup.click();
            browser.sleep('3000');
            expect(browser.findElement(by.xpath('//*[@class="ng-scope md-no-scroll md-active"]//*[@id="groupTable"]/tbody/tr[1]/td[3]')).getText()).toBe(testDataGroup.groupDetails[5].groupName);
            expect(browser.findElement(by.xpath('//*[@class="ng-scope md-no-scroll md-active"]//*[@id="groupTable"]/tbody/tr[1]/td[4]')).getText()).toBe(testDataGroup.groupDetails[5].groupType);
            contacts.deleteGroups();
        });

         //7.TEST CASE TO CHECK VALIDATION WITHOUT GROUP NAME FOR CREATE GROUP WITH GROUP TYPE IS CUSTOM
        it('should login user, Checking Create group and redirecting to "/"', function () {
            contacts.initiateCreateGroup();
            createGroupPage.form.groupType.click();
            element(by.xpath("//div[@class='md-select-menu-container md-active md-clickable']" +
                "//*[contains(text(),'"+testDataGroup.groupDetails[0].groupType+"')]")).click();
            createGroupPage.form.createGroup.click();
            browser.sleep('3000');
            expect(browser.findElement(by.xpath('//*[@class="md-input-messages-animation md-auto-hide ng-active"]/div')).getText()).toBe('Group name is required.');
            browser.driver.findElement(by.xpath('//form[@name="addGroupForm"]//md-icon[@class="fa fa-times fa-lg ng-scope material-icons"]')).click();

        });

        //8.TEST CASE TO CHECK VALIDATION WITHOUT GROUP NAME FOR CREATE GROUP WITH GROUP TYPE IS SUPPLIER
        it('should login user, Checking Create group and redirecting to "/"', function () {
            contacts.initiateCreateGroup();
            createGroupPage.form.groupType.click();
            element(by.xpath("//div[@class='md-select-menu-container md-active md-clickable']" +
                "//*[contains(text(),'"+testDataGroup.groupDetails[1].groupType+"')]")).click();
            createGroupPage.form.createGroup.click();
            browser.sleep('3000');
            expect(browser.findElement(by.xpath('//*[@class="md-input-messages-animation md-auto-hide ng-active"]/div')).getText()).toBe('Group name is required.');
            browser.driver.findElement(by.xpath('//form[@name="addGroupForm"]//md-icon[@class="fa fa-times fa-lg ng-scope material-icons"]')).click();

        });

        //9.TEST CASE TO CHECK VALIDATION WITHOUT GROUP TYPE FOR CREATE GROUP WITH GROUP TYPE IS CUSTOM
        // it('should login user, Checking Create group and redirecting to "/"', function () {
        //     contacts.initiateCreateGroup();
        //     createGroupPage.form.createGroup.click();
        //     //TODO testcase for group type validation
        //
        //     browser.driver.findElement(by.xpath('//form[@name="addGroupForm"]//md-icon[@class="fa fa-times fa-lg ng-scope material-icons"]')).click();
        // });

        //10.TEST CASE TO CHECK VALIDATION WITHOUT GROUP TYPE FOR CREATE GROUP WITH GROUP TYPE IS SUPPLIER
        //it('should login user, Checking Create group and redirecting to "/"', function () {
        //     contacts.initiateGroups();
        //     browser.findElement(by.xpath('//*[@class="ng-scope md-no-scroll md-active"]' +
        //         '//button[@class="md-primary md-raised nvc-shape-pill nvc-margin-right-0 md-button md-ink-ripple"]')).click();
        //     browser.findElement(by.xpath('//div[@class="md-open-menu-container md-whiteframe-z2 md-active md-clickable"]' +
        //         '//md-icon[@class="fa fa-file ng-scope material-icons"]')).click();
        //     createGroupPage.form.createGroup.click();
        //     //TODO testcase for group type validation
        //
        //     browser.driver.findElement(by.xpath('//form[@name="addGroupForm"]//md-icon[@class="fa fa-times fa-lg ng-scope material-icons"]')).click();
        // });

        //11.TEST CASE TO CHECK IMAGE UPLOAD WHILE CREATE GROUP WITH GROUP TYPE IS CUSTOM
        it('should login user, Checking Create group and redirecting to "/"', function () {
            contacts.initiateCreateGroup();

            var absolutePath = path.resolve(__dirname, "./images/Apparels.png");
            browser.findElement(by.model("groupImageURL")).sendKeys(absolutePath);
            browser.sleep(2000);
            createGroupPage.form.groupType.click();
            element(by.xpath("//div[@class='md-select-menu-container md-active md-clickable']" +
                "//*[contains(text(),'"+testDataGroup.groupDetails[6].groupType+"')]")).click();
            createGroupPage.form.groupName.sendKeys(testDataGroup.groupDetails[6].groupName);
            createGroupPage.form.groupClassificationType.click();
            element(by.xpath("//div[@class='md-select-menu-container md-active md-clickable']" +
                "//*[contains(text(),'"+testDataGroup.groupDetails[6].groupClassificationType+"')]")).click();
            browser.sleep('3000');
            createGroupPage.form.createGroup.click();
            browser.sleep('3000');
            expect(browser.findElement(by.xpath('//*[@class="ng-scope md-no-scroll md-active"]//*[@id="groupTable"]/tbody/tr[1]/td[3]')).getText()).toBe(testDataGroup.groupDetails[6].groupName);
            expect(browser.findElement(by.xpath('//*[@class="ng-scope md-no-scroll md-active"]//*[@id="groupTable"]/tbody/tr[1]/td[4]')).getText()).toBe(testDataGroup.groupDetails[6].groupType);
            contacts.deleteGroups();

        });

        // it('should add participants, after creating a group',function(){
        //     //create a contact first.
        //     contacts.initiateCreateContact('Customers');
        //     createContactPage.form.displayName.sendKeys(testDataContact.createContactDetails[9].displayName);
        //     element(by.xpath('//*[contains(text(),"Add Communication Info")]')).click();
        //     element(by.xpath("//div[@id='mobile-0']//*[@data-ng-model='phone.phoneNumber']")).
        //     sendKeys(testDataContact.createContactDetails[9].phoneNumber);
        //     browser.driver.executeScript("arguments[0].scrollIntoView(true);", element(by.xpath('//*[contains(text(),"Add Address Details")]')).getWebElement());
        //
        //     element(by.xpath('//*[contains(text(),"Add Address Details")]')).click();
        //     element(by.xpath('//div[@id="address-0"]//*[@ng-model="address.addressLine"]'))
        //         .sendKeys(testDataContact.createContactDetails[9].addressLine);
        //     element(by.xpath('//div[@id="address-0"]//*[@ng-model="address.city"]'))
        //         .sendKeys(testDataContact.createContactDetails[9].city);
        //     element(by.xpath('//div[@id="address-0"]//*[@ng-model="address.state"]'))
        //         .sendKeys(testDataContact.createContactDetails[9].state);
        //     element(by.xpath('//div[@id="address-0"]//*[@ng-model="address.country"]'))
        //         .sendKeys(testDataContact.createContactDetails[9].country);
        //     element(by.xpath('//div[@id="address-0"]//*[@ng-model="address.pinCode"]'))
        //         .sendKeys('452178');
        //
        //     createContactPage.form.submit.click();
        //     browser.sleep(3000);
        //     expect(element(by.xpath("//md-tabs/md-tabs-content-wrapper/md-tab-content[1]/div/md-content/" +
        //         "view-contacts/table/tbody/tr/td[3]")).getText()).toBe(testDataContact.createContactDetails[9].displayName);
        //     expect(element(by.xpath("//md-tabs/md-tabs-content-wrapper/md-tab-content[1]/div/md-content/" +
        //         "view-contacts/table/tbody/tr/td[5]")).getText()).toBe(testDataContact.createContactDetails[9].phoneNumber);
        //     browser.sleep(1500);
        //
        //     //create a group and add participants
        //     contacts.initiateCreateGroup();
        //
        //
        //     createGroupPage.form.groupType.click();
        //     element(by.xpath("//div[@class='md-select-menu-container md-active md-clickable']" +
        //         "//*[contains(text(),'"+testDataGroup.groupDetails[0].groupType+"')]")).click();
        //     createGroupPage.form.groupName.sendKeys(testDataGroup.groupDetails[0].groupName);
        //     createGroupPage.form.groupClassificationType.click();
        //     element(by.xpath("//div[@class='md-select-menu-container md-active md-clickable']" +
        //         "//*[contains(text(),'"+testDataGroup.groupDetails[0].groupClassificationType+"')]")).click();
        //     createGroupPage.form.createGroup.click();
        //     browser.sleep('3000');
        //     expect(element(by.xpath('//*[@class="ng-scope md-no-scroll md-active"]//*[@id="groupTable"]/tbody/tr[1]/td[3]')).getText()).toBe(testDataGroup.groupDetails[0].groupName);
        //     expect(element(by.xpath('//*[@class="ng-scope md-no-scroll md-active"]//*[@id="groupTable"]/tbody/tr[1]/td[4]')).getText()).toBe(testDataGroup.groupDetails[0].groupType);
        //     element(by.xpath('//*[@class="ng-scope md-no-scroll md-active"]//*[@id="groupTable"]/tbody/tr[1]/td[3]')).click();
        //     browser.sleep(200);
        //     element(by.xpath('//*[@aria-label=\'Search for Contacts to add to Group\']')).sendKeys(testDataContact.createContactDetails[9].displayName+"\t");
        //     //*[@id="ul-978"]/li/md-autocomplete-parent-scope/span
        //     element(by.xpath('//*[contains(text(),'+testDataContact.createContactDetails[9].displayName+')]')).click();
        //     expect(element(by.xpath('//md-chip/div[1]/md-chip-template/span/strong')).getText()).toBe(testDataContact.createContactDetails[9].displayName);
        //     element(by.css('[aria-label=\'Update\']')).click();
        //
        //     contacts.deleteGroups();
        //     contacts.deleteContacts('Customers');
        // });




    });
});
