'use strict';

var createGroupPage = function() {
    var form = this.form = element(by.name('addGroupForm'));
    form.groupType = form.element(by.model('groupType'));
    form.groupClassificationType = form.element(by.model('groupClassification.classificationType'));
    form.groupName = form.element(by.model('name'));
    form.groupDes=form.element(by.model('description'));
    form.city=form.element(by.model('address.city'));
    form.state=form.element(by.model('address.state'));
    form.country=form.element(by.model('address.country'));
    form.pinCode=form.element(by.model('address.pinCode'));

    form.createGroup=form.element(by.id('createGroup'));
    this.createGroup = function(data) {
        for (var prop in data) {
            var formElem = form[prop];
            if (data.hasOwnProperty(prop) && formElem && typeof formElem.sendKeys === 'function') {
                formElem.sendKeys(data[prop]);
            }
        }
    };
};

module.exports = new createGroupPage();
