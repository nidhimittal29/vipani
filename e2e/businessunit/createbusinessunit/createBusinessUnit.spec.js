'use strict';

var config = browser.params,path = require('path'),
user = require('../../account/common.util.e2e/signin.common.po'),
 global = require('../../account/common.util.e2e/global.common.po'),
 bunittestdata=require('./createbusinessUnit.testdata'),unitCreatePage=require('./createBusinessUnit.po');

describe('Create Business Unit View', function() {
    var loadPage = function () {
        browser.manage().deleteAllCookies();
         browser.get('http://localhost:3000/#!/signin');

    };

    describe('with local auth', function () {
        beforeAll(function(){
            browser.get('http://localhost:3000/#!/register');
            user.registerProcess(bunittestdata[0]);
        });
        beforeEach(function () {
            loadPage();
            user.login(bunittestdata[0]);
            browser.sleep('3000');
            browser.findElement(by.id('profilemenu')).click();
            browser.sleep('2000');
            browser.findElement(by.id('companyprofile')).click();
        });

        afterEach(function () {
            user.logout();
        });

        /**
         * case 1:Check default business unit +ve test case
         */
        it('Check default business unit',function () {
             expect(browser.findElement(by.xpath('//*[@id="businessUnitTable"]/tbody/tr[1]/td[2]/span')).getText()).toBe('RegisteredOffice (default)');
            expect(browser.findElement(by.xpath('//*[@id="businessUnitTable"]/tbody/tr[1]/td[3]/h5')).getText()).toBe(bunittestdata[0].username);
            expect(browser.findElement(by.xpath('//*[@id="businessUnitTable"]/tbody/tr[1]/td[3]/h6')).getText()).toBe('Admin');
         });

        /**
         * case 2:Create business unit with all required fields +ve test case
         */
        it('Should be able to create business unit',function () {

            browser.findElement(by.id('add-business-unit')).click();
            unitCreatePage.form.incharge.click();
            browser.findElement(by.id(bunittestdata[0].username)).click();
            unitCreatePage.form.businessUnitName.sendKeys(bunittestdata[1].businessUnitName);
            browser.findElement(by.id('createStep-2')).click();
            browser.findElement(by.id('addBunitPhoneType')).click();
            browser.findElement(by.id('addBunitPhone-Office')).click();
            unitCreatePage.form.phoneNumber.sendKeys(bunittestdata[1].phoneNumber);
            browser.findElement(by.xpath('//*[@id="addBunitPhoneNumberTab"]//md-tab-item[2]')).click();
            browser.findElement(by.id('addBunitEmailType')).click();
            browser.findElement(by.id('addBunitType-Work')).click();
            unitCreatePage.form.email.sendKeys(bunittestdata[1].email);
            browser.findElement(by.id('createStep-3')).click();
            unitCreatePage.form.addressLine.sendKeys(bunittestdata[1].addressLine);
            unitCreatePage.form.city.sendKeys(bunittestdata[1].city);
            unitCreatePage.form.state.sendKeys(bunittestdata[1].state);
            unitCreatePage.form.country.sendKeys(bunittestdata[1].country);
            unitCreatePage.form.pinCode.sendKeys(bunittestdata[1].pinCode);
            unitCreatePage.form.submit.click();
            expect(browser.findElement(by.xpath('//*[@id="businessUnitTable"]/tbody/tr[2]/td[2]/span')).getText()).toBe(bunittestdata[1].businessUnitName);
            expect(browser.findElement(by.xpath('//*[@id="businessUnitTable"]/tbody/tr[2]/td[3]/h5')).getText()).toBe(bunittestdata[0].username);
            expect(browser.findElement(by.xpath('//*[@id="businessUnitTable"]/tbody/tr[2]/td[3]/h6')).getText()).toBe('Admin');

        });
        /**
         * case 3:3.1:Check validation for create business unit -ve test case
         * case 3.2:Check validation for phone number to create business unit -ve test case
         * case 3.3:Check validation for email to create business unit -ve test case
         * case 3.4:Check validation for pincode to create business unit -ve test case
         */
        it('Validation for create business unit',function () {
            browser.findElement(by.xpath('.//button[@aria-label="addBunit"]')).click();
            expect(browser.findElement(by.xpath('//add-business-unit/form/div[1]/div[1]/h4')).getText()).toBe('Add Business Unit');
            unitCreatePage.form.submit.click();
            expect(unitCreatePage.form.businessUnitName.getAttribute('required')).toBeDefined();
            unitCreatePage.form.businessUnitName.sendKeys(bunittestdata[1].businessUnitName);
            browser.findElement(by.id('createStep-2')).click();
            browser.findElement(by.id('addBunitPhoneType')).click();
            browser.findElement(by.id('addBunitPhone-Office')).click();
            unitCreatePage.form.phoneNumber.sendKeys("21554564");
            expect(browser.findElement(by.xpath('//form[@name=\'addBusinessUnitForm\']//md-tab-content[@class="ng-scope md-active md-no-scroll"]//i')).isDisplayed()).toBe(true);
            unitCreatePage.form.phoneNumber.clear().sendKeys(bunittestdata[1].phoneNumber);
            browser.findElement(by.xpath('//*[@id="addBunitPhoneNumberTab"]//md-tab-item[2]')).click();
            browser.findElement(by.id('addBunitEmailType')).click();
            browser.findElement(by.id('addBunitType-Work')).click();
            unitCreatePage.form.email.sendKeys('ssafs@sdf');
            expect(browser.findElement(by.xpath('//form[@name=\'addBusinessUnitForm\']//md-tab-content[@class="ng-scope md-no-scroll md-active"]//i')).isDisplayed()).toBe(true);
            unitCreatePage.form.email.clear().sendKeys(bunittestdata[1].email);
            unitCreatePage.form.submit.click();
            expect(browser.findElement(by.xpath('//*[@id="businessUnitTable"]/tbody/tr[3]/td[2]/span')).getText()).toBe(bunittestdata[1].businessUnitName);
            expect(browser.findElement(by.xpath('//*[@id="businessUnitTable"]/tbody/tr[3]/td[3]/h5')).getText()).toBe(bunittestdata[0].username);
            expect(browser.findElement(by.xpath('//*[@id="businessUnitTable"]/tbody/tr[3]/td[3]/h6')).getText()).toBe('Admin');
        });
        /**
         * case 4:Create business unit with more phone numbers +ve test case
         */
        it('Create business unit with more phone numbers',function () {
            browser.findElement(by.xpath('.//button[@aria-label="addBunit"]')).click();
            expect(browser.findElement(by.xpath('//add-business-unit/form/div[1]/div[1]/h4')).getText()).toBe('Add Business Unit');
            unitCreatePage.form.incharge.click();
            browser.findElement(by.id(bunittestdata[0].username)).click();
            unitCreatePage.form.businessUnitName.sendKeys(bunittestdata[1].businessUnitName);
            browser.findElement(by.id('createStep-2')).click();
            unitCreatePage.form.phoneNumber.sendKeys(bunittestdata[1].phoneNumber);
            browser.findElement(by.xpath('//*[@id="addBunitphone-0"]/div[1]/md-icon[2]')).click();
           browser.findElement(by.id('addBunitPhoneNumber-1')).sendKeys(bunittestdata[2].phoneNumber[0]);
            browser.findElement(by.xpath('//*[@id="addBunitphone-0"]/div[1]/md-icon[2]')).click();
            browser.findElement(by.id('addBunitPhoneNumber-2')).sendKeys(bunittestdata[2].phoneNumber[1]);
            browser.findElement(by.xpath('//*[@id="addBunitphone-0"]/div[1]/md-icon[2]')).click();
            browser.findElement(by.id('addBunitPhoneNumber-3')).sendKeys(bunittestdata[2].phoneNumber[2]);
            browser.findElement(by.xpath('//*[@id="addBunitPhoneNumberTab"]//md-tab-item[2]')).click();
            unitCreatePage.form.email.sendKeys(bunittestdata[1].email);
            browser.findElement(by.id('createStep-3')).click();
            unitCreatePage.form.addressLine.sendKeys(bunittestdata[1].addressLine);
            unitCreatePage.form.city.sendKeys(bunittestdata[1].city);
            unitCreatePage.form.state.sendKeys(bunittestdata[1].state);
            unitCreatePage.form.country.sendKeys(bunittestdata[1].country);
            unitCreatePage.form.pinCode.sendKeys(bunittestdata[1].pinCode);
            unitCreatePage.form.submit.click();

        });

        /**
         *  case 5:Create business unit with more email +ve test case
         */
        it('Create business unit with more emails',function () {
            browser.findElement(by.xpath('.//button[@aria-label="addBunit"]')).click();
            expect(browser.findElement(by.xpath('//add-business-unit/form/div[1]/div[1]/h4')).getText()).toBe('Add Business Unit');
            unitCreatePage.form.incharge.click();
            browser.findElement(by.id(bunittestdata[0].username)).click();
            unitCreatePage.form.businessUnitName.sendKeys(bunittestdata[1].businessUnitName);
            browser.findElement(by.id('createStep-2')).click();
            unitCreatePage.form.phoneNumber.sendKeys(bunittestdata[3].phoneNumber);
            browser.findElement(by.xpath('//*[@id="addBunitPhoneNumberTab"]//md-tab-item[2]')).click();
            unitCreatePage.form.email.sendKeys(bunittestdata[1].email);
            browser.findElement(by.xpath('//*[@id="addBunitEmail-0"]/div[1]/md-icon[2]')).click();
            browser.findElement(by.id('addBunitPhoneEmail-1')).sendKeys(bunittestdata[3].email[0]);
            browser.findElement(by.xpath('//*[@id="addBunitEmail-0"]/div[1]/md-icon[2]')).click();
            browser.findElement(by.id('addBunitPhoneEmail-2')).sendKeys(bunittestdata[3].email[1]);
            browser.findElement(by.xpath('//*[@id="addBunitEmail-0"]/div[1]/md-icon[2]')).click();
            browser.findElement(by.id('addBunitPhoneEmail-3')).sendKeys(bunittestdata[3].email[2]);
            browser.findElement(by.id('createStep-3')).click();
            unitCreatePage.form.addressLine.sendKeys(bunittestdata[2].addressLine);
            unitCreatePage.form.city.sendKeys(bunittestdata[2].city);
            unitCreatePage.form.state.sendKeys(bunittestdata[2].state);
            unitCreatePage.form.country.sendKeys(bunittestdata[2].country);
            unitCreatePage.form.pinCode.sendKeys(bunittestdata[2].pinCode);
            unitCreatePage.form.submit.click();
        });
        /*
         * case 6:Create business unit with more address +ve test case
         */
        it('Create business unit with more address',function () {
            browser.findElement(by.xpath('.//button[@aria-label="addBunit"]')).click();
            expect(browser.findElement(by.xpath('//add-business-unit/form/div[1]/div[1]/h4')).getText()).toBe('Add Business Unit');
            unitCreatePage.form.incharge.click();
            browser.findElement(by.id(bunittestdata[0].username)).click();
            unitCreatePage.form.businessUnitName.sendKeys(bunittestdata[1].businessUnitName);
            browser.findElement(by.id('createStep-2')).click();
            unitCreatePage.form.phoneNumber.sendKeys(bunittestdata[4].phoneNumber);
            browser.findElement(by.xpath('//*[@id="addBunitPhoneNumberTab"]//md-tab-item[2]')).click();
            unitCreatePage.form.email.sendKeys(bunittestdata[4].email);
            browser.driver.executeScript("arguments[0].scrollIntoView(true);", element(by.id('createStep-3')).getWebElement());
            browser.findElement(by.id('createStep-3')).click();
            unitCreatePage.form.addressLine.sendKeys(bunittestdata[4].address[0].addressLine);
            unitCreatePage.form.city.sendKeys(bunittestdata[4].address[0].city);
            unitCreatePage.form.state.sendKeys(bunittestdata[4].address[0].state);
            unitCreatePage.form.country.sendKeys(bunittestdata[4].address[0].country);
            unitCreatePage.form.pinCode.sendKeys(bunittestdata[4].address[0].pinCode);
            browser.findElement(by.id('companyAddAddresses-0')).click();
            browser.driver.executeScript("arguments[0].scrollIntoView(true);", element(by.id('createStep-3')).getWebElement());
            unitCreatePage.form.addressLine.sendKeys(bunittestdata[4].address[1].addressLine);
            unitCreatePage.form.city.sendKeys(bunittestdata[4].address[1].city);
            unitCreatePage.form.state.sendKeys(bunittestdata[4].address[1].state);
            unitCreatePage.form.country.sendKeys(bunittestdata[4].address[1].country);
            unitCreatePage.form.pinCode.sendKeys(bunittestdata[4].address[1].pinCode)
            unitCreatePage.form.submit.click();
        });



    });
});
