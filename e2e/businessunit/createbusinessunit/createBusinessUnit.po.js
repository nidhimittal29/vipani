'use strict';

var createBusinessUnitPage = function() {
    var form = this.form = element(by.name('addBusinessUnitForm'));
    form.incharge = form.element(by.model('businessUnit.businessUnitIncharge'));
    form.businessUnitName = form.element(by.model('businessUnit.name'));
    form.gstinNo=form.element(by.model('businessUnit.gstinNumber'));
    form.panNo=form.element(by.model('businessUnit.panNumber'));
    form.emailType=form.element(by.model('email.emailType'));
    form.email=form.element(by.model('email.email'));
    form.phoneType=form.element(by.model('phone.phoneType'));
    form.phoneNumber=form.element(by.model('phone.phoneNumber'));
    form.addressLine = form.element(by.model('address.addressLine'));
    form.city = form.element(by.model('address.city'));
    form.state = form.element(by.model('address.state'));
    form.country = form.element(by.model('address.country'));
    form.pinCode=form.element(by.model('address.pinCode'));
    form.bankAccountNumber=form.element(by.model('businessUnit.bankAccountDetails.bankAccountNumber'));
    form.accountType=form.element(by.model('businessUnit.bankAccountDetails.accountType'));
    form.bankName=form.element(by.model('businessUnit.bankAccountDetails.bankName'));
    form.ifscCode=form.element(by.model('businessUnit.bankAccountDetails.ifscCode'));
    form.submit = form.element(by.id('createBunit'));
    this.createBusinessUnit = function(data) {
        for (var prop in data) {
            var formElem = form[prop];
            if (data.hasOwnProperty(prop) && formElem && typeof formElem.sendKeys === 'function') {
                formElem.sendKeys(data[prop]);
            }
        }
    };
};

module.exports = new createBusinessUnitPage();
