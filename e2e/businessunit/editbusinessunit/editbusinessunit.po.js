'use strict';

var editBusinessUnitPage = function() {
    var form = this.form = element(by.name('editBusinessUnitForm'));
    form.businessUnitName = form.element(by.model('businessUnit.name'));
    form.gstinNumber=form.element(by.model('businessUnit.gstinNumber'));
    form.panNumber=form.element(by.model('businessUnit.panNumber'));
    form.gstinNumberProof=form.element(by.model('businessUnit.gstinNumberProof'));
    form.panNumberProof=form.element(by.model('businessUnit.panNumberProof'));
    form.phoneNumber=form.element(by.model('phone.phoneNumber'));
    form.email=form.element(by.model('email.email'));
    form.addressLine=form.element(by.model('address.addressLine'));
    form.city=form.element(by.model('address.city'));
    form.state=form.element(by.model('address.state'));
    form.country=form.element(by.model('address.country'));
    form.pinCode=form.element(by.model('address.pinCode'));
    form.bankAccountNumber=form.element(by.model('businessUnit.bankAccountDetails.bankAccountNumber'));
    form.accountType=form.element(by.model('businessUnit.bankAccountDetails.accountType'));
    form.bankName=form.element(by.model('businessUnit.bankAccountDetails.bankName'));
    form.ifscCode=form.element(by.model('businessUnit.bankAccountDetails.ifscCode'));
    form.submit = form.element(by.xpath('//*[@aria-label="Update"]'));
    this.editBusinessUnit = function(data) {
        for (var prop in data) {
            var formElem = form[prop];
            if (data.hasOwnProperty(prop) && formElem && typeof formElem.sendKeys === 'function') {
                formElem.sendKeys(data[prop]);
            }
        }
    };
};

module.exports = new editBusinessUnitPage();
