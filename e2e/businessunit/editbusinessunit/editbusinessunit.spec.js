'use strict';

var config = browser.params,path = require('path'),
    user = require('../../account/common.util.e2e/signin.common.po'),
    bunittestdata=require('./editbusinessunit.testdata'),unitCreatePage=require('../createbusinessunit/createBusinessUnit.po'),unitEditPage=require('../editbusinessunit/editbusinessunit.po');

describe('Edit Business Unit View', function() {
    var loadPage = function () {
        browser.manage().deleteAllCookies();
        browser.get('http://localhost:3000/#!/signin');

    };

    describe('with local auth', function () {
        beforeAll(function () {
            browser.get('http://localhost:3000/#!/register');
            user.registerProcess(bunittestdata[0]);
        });
        beforeEach(function () {
            loadPage();
            user.login(bunittestdata[0]);
            browser.sleep('3000');
            browser.findElement(by.id('profilemenu')).click();
            browser.sleep('2000');
            browser.findElement(by.id('businessprofile')).click();
        });

        afterEach(function () {
            user.logout();
        });

        /**
         * CASE 1:CHECK DEFAULT BUSINESS UNIT +VE TEST CASE
         */
        it('Check default business unit', function () {
            expect(browser.findElement(by.xpath('//*[@id="businessUnitTable"]/tbody/tr[1]/td[2]/span')).getText()).toBe('RegisteredOffice (default)');
            expect(browser.findElement(by.xpath('//*[@id="businessUnitTable"]/tbody/tr[1]/td[3]/h5')).getText()).toBe(bunittestdata[0].username);
            expect(browser.findElement(by.xpath('//*[@id="businessUnitTable"]/tbody/tr[1]/td[3]/h6')).getText()).toBe('Admin');
        });

        /**
         * CASE 2: UPDATE BUSINESS UNIT WITH NEW DATA +VE TEST CASE
         */
        it('Update business unit with new data',function(){
            browser.findElement(by.id('add-business-unit')).click();
            unitCreatePage.form.incharge.click();
            browser.findElement(by.id(bunittestdata[0].username)).click();
            unitCreatePage.form.businessUnitName.sendKeys(bunittestdata[1].businessUnitName);
            unitCreatePage.form.gstinNo.sendKeys(bunittestdata[1].gstinNumber);
            unitCreatePage.form.panNo.sendKeys(bunittestdata[1].panNumber);
            browser.findElement(by.id('createStep-2')).click();
            browser.findElement(by.id('phoneType-0')).click();
            browser.findElement(by.id('phone-Work')).click();
            browser.findElement(by.id('addBunitPhoneNumber-0')).click();

            unitCreatePage.form.phoneNumber.sendKeys(bunittestdata[1].phoneNumber);
            browser.findElement(by.xpath('//*[@id="editBunitEmailTab"]//md-tab-item[2]')).click();
            browser.findElement(by.xpath('//*[@id="email-0"]//*[@ng-model="email.emailType"]')).click();
            browser.findElement(by.id('editBunitAddEmails')).click();
            browser.findElement(by.id('emailType-0')).click();
            browser.findElement(by.id('email-Work')).click();
            unitCreatePage.form.email.sendKeys(bunittestdata[1].email);
            browser.driver.executeScript("arguments[0].scrollIntoView(true);", element(by.xpath('createStep-3')).getWebElement());

            browser.findElement(by.id('createStep-3')).click();
            browser.findElement(by.xpath('//*[id="addressLine"]//*[@ng-model="address.addressLine"]')).click();
            unitCreatePage.form.addressLine.sendKeys(bunittestdata[1].addressLine);
            unitCreatePage.form.city.sendKeys(bunittestdata[1].city);
            unitCreatePage.form.state.sendKeys(bunittestdata[1].state);
            unitCreatePage.form.country.sendKeys(bunittestdata[1].country);
            unitCreatePage.form.pinCode.sendKeys(bunittestdata[1].pincode);
            browser.driver.executeScript("arguments[0].scrollIntoView(true);", element(by.xpath('createStep-4')).getWebElement());
            browser.findElement(by.id('createStep-4')).click();
            browser.findElement(by.xpath('//*[id=bankAccountDetails"]//*[@ng-model="bankAccountDetails.bankAccountNumber"]')).click();
            unitCreatePage.form.bankAccountNumber.sendKeys(bunittestdata[1].bankAccountNumber);
            unitCreatePage.form.accountType.sendKeys(bunittestdata[1].accountType);
            unitCreatePage.form.bankName.sendKeys(bunittestdata[1].bankName);
            unitCreatePage.form.ifscCode.sendKeys(bunittestdata[1].ifscCode);
            unitCreatePage.form.submit.click();
            browser.findElement(by.xpath('//*[@id="businessUnitTable"]/tbody/tr[1]/td[2]/span')).click();
            browser.findElement(by.id('business-unit-edit-icon')).click();
            expect(unitEditPage.form.businessUnitName.getText()).toBe(bunittestdata[1].businessUnitName);
            expect(browser.findElement(by.id('inchargeName')).getText()).toBe(bunittestdata[0].username);
            expect(unitEditPage.form.gstinNumber.getText()).toBe(bunittestdata[1].gstinNumber);
            expect(unitEditPage.form.panNumber.getText()).toBe(bunittestdata[1].panNumber);
            expect(unitEditPage.form.phoneNumber.getText()).toBe(bunittestdata[1].phoneNumber);
            expect(unitEditPage.form.email.getText()).toBe(bunittestdata[1].email);
            browser.driver.executeScript("arguments[0].scrollIntoView(true);", unitEditPage.form.addressLine.getWebElement());

            expect(unitEditPage.form.addressLine.getText()).toBe(bunittestdata[1].addressLine);
            expect(unitEditPage.form.city.getText()).toBe(bunittestdata[1].city);
            expect(unitEditPage.form.state.getText()).toBe(bunittestdata[1].state);
            expect(unitEditPage.form.country.getText()).toBe(bunittestdata[1].country);
            expect(unitEditPage.form.pinCode.getText()).toBe(bunittestdata[1].pinCode);

            browser.findElement(by.xpath('//edit-business-unit/form//md-tab-item[3]')).click();
            expect(unitEditPage.form.bankAccountNumber.getText()).toBe(bunittestdata[1].bankAccountNumber);
            expect(unitEditPage.form.accountType.getText()).toBe(bunittestdata[1].accountType);
            expect(unitEditPage.form.bankName.getText()).toBe(bunittestdata[1].bankName);
            expect(unitEditPage.form.ifscCode.getText()).toBe(bunittestdata[1].ifscCode);
            browser.findElement(by.id('closeeditbusinessunit')).click();
            browser.findElement(by.xpath('//*[@id="businessUnitTable"]/tbody/tr[1]/td[2]/span')).click();
            browser.findElement(by.id('business-unit-edit-icon')).click();
            unitEditPage.form.businessUnitName.clear().sendKeys(bunittestdata[2].businessUnitName);
            unitEditPage.form.gstinNumber.clear().sendKeys(bunittestdata[2].gstinNumber);
            unitEditPage.form.panNumber.clear().sendKeys(bunittestdata[2].panNumber);
            unitEditPage.form.phoneNumber.clear().sendKeys(bunittestdata[2].phoneNumber);
            unitEditPage.form.email.clear().sendKeys(bunittestdata[2].email);
            unitEditPage.form.addressLine.clear().sendKeys(bunittestdata[2].addressLine);
            unitEditPage.form.city.clear().sendKeys(bunittestdata[2].city);
            unitEditPage.form.state.clear().sendKeys(bunittestdata[2].state);
            unitEditPage.form.country.clear().sendKeys(bunittestdata[2].country);
            unitEditPage.form.pinCode.clear().sendKeys(bunittestdata[2].pinCode);
            browser.findElement(by.xpath('//edit-business-unit/form//md-tab-item[3]')).click();
            unitEditPage.form.bankAccountNumber.clear().sendKeys(bunittestdata[2].bankAccountNumber);
            unitEditPage.form.accountType.clear().sendKeys(bunittestdata[2].accountType);
            unitEditPage.form.bankName.clear().sendKeys(bunittestdata[2].bankName);
            unitEditPage.form.ifscCode.clear().sendKeys(bunittestdata[2].ifscCode);
            unitEditPage.form.submit.click();
        });

            /**
             *CASE 3:UPDATE BUSINESS UNIT WITH GSTINNO AND PANNO +VE TEST CASE
              */
            it('Update business unit with gstinNo and panNo', function () {
                browser.findElement(by.xpath('//*[@id="businessUnitTable"]/tbody/tr[1]/td[2]/span')).click();
                browser.findElement(by.id('business-unit-edit-icon')).click();
                browser.findElement(by.id('editBunitGstinNo')).sendKeys('22AAAAA0000A1Z5');
                browser.findElement(by.id('editBunitPanNo')).sendKeys('AAAA1234A');
                unitEditPage.form.submit.click();
            });

            /**
             * Case 4:UPLOAD GSTINNO PROOF  WHILE EDIT BUSINESS UNIT +VE TEST CASE
             */
            it('should upload gstinNo proof while edit business unit',function () {
                browser.findElement(by.xpath('//*[@id="businessUnitTable"]/tbody/tr[1]/td[2]/span')).click();
                browser.findElement(by.id('business-unit-edit-icon')).click();
                unitEditPage.form.gstinNumber.sendKeys(bunittestdata[1].gstinNumber);
                var absolutePath = path.resolve(__dirname, '../images/gstinnumberproof.png');
                unitEditPage.form.gstinNumberProof.sendKeys(absolutePath);
                browser.sleep(2000);
                expect(browser.findElement(by.xpath('//div[@id="editBunitGstinproof"]//*[@class="fa fa-eye"]')).isDisplayed()).toBe(true);
                expect(browser.findElement(by.xpath('//div[@id="editBunitGstinproof"]//*[@class="fa fa-download"]')).isDisplayed()).toBe(true);
                unitEditPage.form.submit.click();
                browser.sleep(3000);
            });
             /**
             * CASE 5:UPLOAD PANNO PROOF  WHILE EDIT BUSINESS UNIT +VE TEST CASE
             */
            it('should upload panNo proof while edit business unit',function () {
                browser.findElement(by.xpath('//*[@id="businessUnitTable"]/tbody/tr[1]/td[2]/span')).click();
                browser.findElement(by.id('business-unit-edit-icon')).click();
                unitEditPage.form.panNumber.sendKeys(bunittestdata[1].panNumber);
                var absolutePath = path.resolve(__dirname, '../images/gstinnumberproof.png');
                unitEditPage.form.panNumberProof.sendKeys(absolutePath);
                browser.sleep(2000);
                expect(browser.findElement(by.xpath('//div[@id="editBunitPannoproof"]//*[@class="fa fa-eye"]')).isDisplayed()).toBe(true);
                expect(browser.findElement(by.xpath('//div[@id="editBunitPannoproof"]//*[@class="fa fa-download"]')).isDisplayed()).toBe(true);
                unitEditPage.form.submit.click();
                browser.sleep(3000);
            });
        /**
         * CASE 6:ADD MULTIPLE PHONE NUMBERS WHILE EDIT BUSINESS UNIT +VE TEST CASE
         */
        it('should login user, Checking Edit Business unit to add multiple phone numbers', function () {
            browser.findElement(by.xpath('//*[@id="businessUnitTable"]/tbody/tr[1]/td[2]/span')).click();
            browser.sleep(2000);
            browser.findElement(by.id('business-unit-edit-icon')).click();
            browser.findElement(by.id('editBunitAddPhones')).click();
            browser.findElement(by.id('editBunitPhoneNumber-0')).sendKeys(bunittestdata[3].phoneNumber[0]);
            browser.findElement(by.id('editBunitAddPhones')).click();
            browser.driver.executeScript("arguments[0].scrollIntoView(true);", element(by.id('editBunitPhoneNumber-1')).getWebElement());
            browser.findElement(by.id('editBunitPhoneNumber-1')).sendKeys(bunittestdata[3].phoneNumber[1]);
            browser.findElement(by.id('editBunitAddPhones')).click();
            browser.findElement(by.id('editBunitPhoneNumber-2')).sendKeys(bunittestdata[3].phoneNumber[2]);
            unitEditPage.form.submit.click();
            browser.sleep(3000);
        });

        /**
         * CASE 7:ADD MULTIPLE EMAILS WHILE EDIT BUSINESS UNIT +VE TEST CASE
         */
        it('should login user,checking Edit business unit to add multiple Emails',function(){
            browser.findElement(by.xpath('//*[@id="businessUnitTable"]/tbody/tr[1]/td[2]/span')).click();
            browser.sleep(2000);
            browser.findElement(by.id('business-unit-edit-icon')).click();
            browser.findElement(by.xpath('//*[@id="editBunitEmailTab"]//md-tab-item[2]')).click();
            browser.findElement(by.id('editBunitAddEmails')).click();
            browser.findElement(by.id('editBunitEmail-1')).sendKeys(bunittestdata[4].email[0]);
            browser.findElement(by.id('editBunitAddEmails')).click();
            browser.driver.executeScript("arguments[0].scrollIntoView(true);", element(by.id('editBunitEmail-1')).getWebElement());
            browser.findElement(by.id('editBunitEmail-2')).sendKeys(bunittestdata[4].email[1]);
            browser.findElement(by.id('editBunitAddEmails')).click();
            browser.findElement(by.id('editBunitEmail-3')).sendKeys(bunittestdata[4].email[2]);
            unitEditPage.form.submit.click();
            browser.sleep(3000);
        });
        /**
         * CASE 8:TEST CASE TO ADD MORE BILLING ADDRESSES WHILE EDIT BUSINESS UNIT
         */
        it('should login user,checking edit business unit to add more Billing addresses',function(){
            browser.findElement(by.xpath('//*[@id="businessUnitTable"]/tbody/tr[1]/td[2]/span')).click();
            browser.sleep(2000);
            browser.findElement(by.id('business-unit-edit-icon')).click();
            browser.findElement(by.id('editBunitAddAddresses-0')).click();
            browser.driver.executeScript("arguments[0].scrollIntoView(true);", element(by.id('editBunitAddressLine-0')).getWebElement());
            browser.findElement(by.id('editBunitAddressLine-0')).sendKeys(bunittestdata[5].billingaddresses[0].addressLine);
            browser.findElement(by.id('editBunitCity-0')).sendKeys(bunittestdata[5].billingaddresses[0].city);
            browser.findElement(by.id('editBunitState-0')).sendKeys(bunittestdata[5].billingaddresses[0].state);
            browser.findElement(by.id('editBunitCountry-0')).sendKeys(bunittestdata[5].billingaddresses[0].country);
            browser.findElement(by.id('editBunitPinCode-0')).sendKeys(bunittestdata[5].billingaddresses[0].pinCode);
            browser.findElement(by.id('editBunitAddAddresses-0')).click();
            browser.driver.executeScript("arguments[0].scrollIntoView(true);", element(by.id('editBunitAddressLine-1')).getWebElement());
            browser.findElement(by.id('editBunitAddressLine-1')).sendKeys(bunittestdata[5].billingaddresses[1].addressLine);
            browser.findElement(by.id('editBunitCity-1')).sendKeys(bunittestdata[5].billingaddresses[1].city);
            browser.findElement(by.id('editBunitState-1')).sendKeys(bunittestdata[5].billingaddresses[1].state);
            browser.findElement(by.id('editBunitCountry-1')).sendKeys(bunittestdata[5].billingaddresses[1].country);
            browser.findElement(by.id('editBunitPinCode-1')).sendKeys(bunittestdata[5].billingaddresses[1].pinCode);
            unitEditPage.form.submit.click();
            browser.sleep(3000);
        });

         /**
         * CASE 9:TEST CASE TO ADD MORE SHIPPING ADDRESSES WHILE EDIT BUSINESS UNIT
         */
        it('should login user,checking edit business unit to add more shipping addresses',function(){
            browser.findElement(by.xpath('//*[@id="businessUnitTable"]/tbody/tr[1]/td[2]/span')).click();
            browser.sleep(2000);
            browser.findElement(by.id('business-unit-edit-icon')).click();
            browser.findElement(by.xpath('//*[@id="editBunitAddressTab"]//md-tab-item[2]')).click();
            browser.findElement(by.id('editBunitAddAddresses-1')).click();
            browser.driver.executeScript("arguments[0].scrollIntoView(true);", element(by.id('editBunitAddressLine-0')).getWebElement());
            browser.findElement(by.id('editBunitAddressLine-0')).sendKeys(bunittestdata[5].shippingaddresses[0].addressLine);
            browser.findElement(by.id('editBunitCity-0')).sendKeys(bunittestdata[5].shippingaddresses[0].city);
            browser.findElement(by.id('editBunitState-0')).sendKeys(bunittestdata[5].shippingaddresses[0].state);
            browser.findElement(by.id('editBunitCountry-0')).sendKeys(bunittestdata[5].shippingaddresses[0].country);
            browser.findElement(by.id('editBunitPinCode-0')).sendKeys(bunittestdata[5].shippingaddresses[0].pinCode);
            browser.findElement(by.id('editBunitAddAddresses-1')).click();
            browser.driver.executeScript("arguments[0].scrollIntoView(true);", element(by.id('editBunitAddressLine-1')).getWebElement());
            browser.findElement(by.id('editBunitAddressLine-1')).sendKeys(bunittestdata[5].shippingaddresses[1].addressLine);
            browser.findElement(by.id('editBunitCity-1')).sendKeys(bunittestdata[5].shippingaddresses[1].city);
            browser.findElement(by.id('editBunitState-1')).sendKeys(bunittestdata[5].shippingaddresses[1].state);
            browser.findElement(by.id('editBunitCountry-1')).sendKeys(bunittestdata[5].shippingaddresses[1].country);
            browser.findElement(by.id('editBunitPinCode-1')).sendKeys(bunittestdata[5].shippingaddresses[1].pinCode);
            unitEditPage.form.submit.click();
            browser.sleep(3000);
        });
        /**
         * CASE 10:TEST CASE TO ADD MORE RECEIVING ADDRESSES WHILE EDIT BUSINESS UNIT
         */
        it('should login user,checking edit business unit to add more Receiving addresses',function(){
            browser.findElement(by.xpath('//*[@id="businessUnitTable"]/tbody/tr[1]/td[2]/span')).click();
            browser.sleep(2000);
            browser.findElement(by.id('business-unit-edit-icon')).click();
            browser.findElement(by.xpath('//*[@id="editBunitAddressTab"]//md-tab-item[3]')).click();
            browser.findElement(by.id('editBunitAddAddresses-2')).click();
            browser.driver.executeScript("arguments[0].scrollIntoView(true);", element(by.id('editBunitAddressLine-0')).getWebElement());
            browser.findElement(by.id('editBunitAddressLine-0')).sendKeys(bunittestdata[5].receivingaddresses[0].addressLine);
            browser.findElement(by.id('editBunitCity-0')).sendKeys(bunittestdata[5].receivingaddresses[0].city);
            browser.findElement(by.id('editBunitState-0')).sendKeys(bunittestdata[5].receivingaddresses[0].state);
            browser.findElement(by.id('editBunitCountry-0')).sendKeys(bunittestdata[5].receivingaddresses[0].country);
            browser.findElement(by.id('editBunitPinCode-0')).sendKeys(bunittestdata[5].receivingaddresses[0].pinCode);
            browser.findElement(by.id('editBunitAddAddresses-2')).click();
            browser.driver.executeScript("arguments[0].scrollIntoView(true);", element(by.id('editBunitAddressLine-1')).getWebElement());
            browser.findElement(by.id('editBunitAddressLine-1')).sendKeys(bunittestdata[5].receivingaddresses[1].addressLine);
            browser.findElement(by.id('editBunitCity-1')).sendKeys(bunittestdata[5].receivingaddresses[1].city);
            browser.findElement(by.id('editBunitState-1')).sendKeys(bunittestdata[5].receivingaddresses[1].state);
            browser.findElement(by.id('editBunitCountry-1')).sendKeys(bunittestdata[5].receivingaddresses[1].country);
            browser.findElement(by.id('editBunitPinCode-1')).sendKeys(bunittestdata[5].receivingaddresses[1].pinCode);
            unitEditPage.form.submit.click();
            browser.sleep(3000);
        });
        /**
         * CASE 11:TEST CASE TO ADD MORE INVOICE ADDRESSES WHILE EDIT BUSINESS UNIT
         */
        it('should login user,checking edit business unit to add more  Invoice addresses',function(){
            browser.findElement(by.xpath('//*[@id="businessUnitTable"]/tbody/tr[1]/td[2]/span')).click();
            browser.sleep(2000);
            browser.findElement(by.id('business-unit-edit-icon')).click();
            browser.findElement(by.xpath('//*[@id="editBunitAddressTab"]//md-tab-item[4]')).click();
            browser.findElement(by.id('editBunitAddAddresses-3')).click();
            browser.driver.executeScript("arguments[0].scrollIntoView(true);", element(by.id('editBunitAddressLine-0')).getWebElement());
            browser.findElement(by.id('editBunitAddressLine-0')).sendKeys(bunittestdata[5].invoiceaddresses[0].addressLine);
            browser.findElement(by.id('editBunitCity-0')).sendKeys(bunittestdata[5].invoiceaddresses[0].city);
            browser.findElement(by.id('editBunitState-0')).sendKeys(bunittestdata[5].invoiceaddresses[0].state);
            browser.findElement(by.id('editBunitCountry-0')).sendKeys(bunittestdata[5].invoiceaddresses[0].country);
            browser.findElement(by.id('editBunitPinCode-0')).sendKeys(bunittestdata[5].invoiceaddresses[0].pinCode);
            browser.findElement(by.id('editBunitAddAddresses-3')).click();
            browser.driver.executeScript("arguments[0].scrollIntoView(true);", element(by.id('editBunitAddressLine-1')).getWebElement());
            browser.findElement(by.id('editBunitAddressLine-1')).sendKeys(bunittestdata[5].invoiceaddresses[1].addressLine);
            browser.findElement(by.id('editBunitCity-1')).sendKeys(bunittestdata[5].invoiceaddresses[1].city);
            browser.findElement(by.id('editBunitState-1')).sendKeys(bunittestdata[5].invoiceaddresses[1].state);
            browser.findElement(by.id('editBunitCountry-1')).sendKeys(bunittestdata[5].invoiceaddresses[1].country);
            browser.findElement(by.id('editBunitPinCode-1')).sendKeys(bunittestdata[5].invoiceaddresses[1].pinCode);
            unitEditPage.form.submit.click();
            browser.sleep(3000);
        });
        /**
         * CASE 12:TEST CASE TO UPDATE BANK ACCOUNT DETAILS WHILE EDIT BUSINESS UNIT
         */
        it('should login user,checking edit business unit to update bank account details',function(){
            browser.findElement(by.xpath('//*[@id="businessUnitTable"]/tbody/tr[1]/td[2]/span')).click();
            browser.sleep(2000);
            browser.findElement(by.id('business-unit-edit-icon')).click();
            browser.findElement(by.xpath('//*[@id="editBunitTabs"]//md-tab-item[3]')).click();
            browser.findElement(by.id('editBunitAccountNo')).sendKeys(bunittestdata[1].accountNo);
            browser.findElement(by.id('editBunitAccountType')).click();
            browser.findElement(by.id('editBunitSaving')).click();
            browser.findElement(by.id('editBunitBankName')).sendKeys(bunittestdata[1].bankName);
            browser.findElement(by.id('editBunitIfscCode')).sendKeys(bunittestdata[1].ifscCode);
            unitEditPage.form.submit.click();
            browser.sleep(3000);
    });
        /**
         * CASE 13:TEST CASE TO ADD NEW BUSINESS USER  TO OTHER BUSINESS UNIT
         */
        it('should login user,checking edit business unit to add new business user',function(){
            browser.findElement(by.xpath('//*[@id="businessUnitTable"]/tbody/tr[1]/td[2]/span')).click();
            browser.sleep(2000);
            browser.findElement(by.id('business-unit-edit-icon')).click();
            browser.findElement(by.xpath('//*[@id="editBunitTabs"]//md-tab-item[2]')).click();

    })
    });
    });


