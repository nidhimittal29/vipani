
'use strict';
var form,formData=require('./form.data.common.po'),path = require('path');
exports.createInventory=function (data) {
    form = element(by.name('productForm'));
    if(data) {
        formData.formData(form, data);
    }
};
exports.createBrand=function (data) {
    form = element(by.name('productForm'));
    if(data) {
        formData.formData(form, data);
    }
};
exports.createStock=function (data) {
    if(data) {
        formData.formData(form, data);
    }
};
exports.getForm=function() {
    return form;
};
exports.initiateCreateProduct = function(){
    element(by.id("nav-products")).click();
    browser.sleep(1000);
    if(browser.isElementPresent(element(by.id('newproducthome'))) && element(by.id('newproducthome')).isDisplayed()){
        browser.findElement(by.id("newproducthome")).click();
    }else {
        browser.findElement(by.id("menu")).click();
        browser.sleep(1000);
        browser.findElement(by.id("menu-newproduct")).click();
    }
    browser.sleep(1000);
};

exports.initiateProducts = function(){
    element(by.id("nav-products")).click();
    browser.sleep(1000);

};

exports.fileImportProducts=function (filePath) {

    element(by.id("nav-products")).click();
    browser.sleep(1000);
    if(browser.isElementPresent(element(by.id('importproductfilehome'))) && element(by.id('importproductfilehome')).isDisplayed()){
        browser.findElement(by.id("importproductfilehome")).click();
    }else {
        browser.findElement(by.id("menu")).click();
        browser.sleep(1000);
        browser.findElement(by.id("importproductfile")).click();
    }
    browser.sleep(1000);
    var productPath = path.resolve(__dirname, filePath);
    browser.findElement(by.id("fileproducts")).sendKeys(productPath);
    element(by.css('[aria-label=\'Proceed to upload file\']')).click();
    browser.sleep(2000);
};

exports.deleteAllImported = function(){
    var list = element.all(by.xpath('//view-products/table/tbody/tr/*/md-checkbox'));
    list.each(function (item) {
        item.click();
    });
    browser.sleep(500);
    element(by.css('button[aria-label=\'Group Actions\']')).click();
    browser.sleep(1000);
    element(by.css('button[aria-label=\'Delete\']')).click();
};

exports.validateProductEdit = function(editProductForm,valuesArray){
    //expect(editProductForm.element(by.xpath('//h4')).getAttribute('value')).toBe('Product Details');
    console.log(valuesArray.length);
    if(valuesArray.length===8) {
        expect(editProductForm.element(by.xpath('div[1]/div[2]/div[1]/div[2]/div[1]/label')).getText()).toBe(valuesArray[0]);
        expect(editProductForm.element(by.xpath('div[1]/div[2]/div[1]/div[2]/div[2]/label')).getText()).toBe(valuesArray[1]);
        expect(editProductForm.element(by.xpath('div[1]/div[2]/div[1]/div[2]/div[3]/label')).getText()).toBe(valuesArray[2]);
        expect(editProductForm.element(by.xpath('div[1]/div[2]/div[1]/div[2]/div[4]/label')).getText()).toBe(valuesArray[3]);
        expect(editProductForm.element(by.xpath('div[1]/div[2]/div[1]/div[2]/div[5]/label')).getText()).toBe(valuesArray[4]);
        expect(editProductForm.element(by.xpath('div[1]/div[2]/div[1]/div[2]/div[6]/label')).getText()).toBe(valuesArray[5]);
        expect(editProductForm.element(by.xpath('div[1]/div[2]/div[1]/div[4]/div[1]/label')).getText()).toBe(valuesArray[6]);
        expect(editProductForm.element(by.xpath('div[1]/div[2]/div[1]/div[4]/div[2]/label')).getText()).toBe(valuesArray[7]);
    }
};

var validateStock = function(batchStockRow,valuesArray){
    if(valuesArray.length===7) {
        expect(batchStockRow.element(by.xpath('div/div[1]/div[1]/label'))).toBe(valuesArray[0]);
        expect(batchStockRow.element(by.xpath('div/div[2]/div[1]/label'))).toBe(valuesArray[1]);
        expect(batchStockRow.element(by.xpath('div/div[2]/div[2]/label'))).toBe(valuesArray[2]);
        expect(batchStockRow.element(by.xpath('div/div[2]/div[3]/label'))).toBe(valuesArray[3]);
        expect(batchStockRow.element(by.xpath('div/div[2]/div[4]/label'))).toBe(valuesArray[4]);
        expect(batchStockRow.element(by.xpath('div/div[2]/div[5]/label'))).toBe(valuesArray[5]);
        expect(batchStockRow.element(by.xpath('div/div[2]/div[6]/label'))).toBe(valuesArray[6]);
    }
};

exports.validateBatches = function(batchTab,batchValuesArray){
    for(var i=0;i<batchValuesArray.length;i++) {
        validateStock(batchTab.element(by.xpath('//*[@id="stock-"'+i+']/div/div/div[1]/div/div[1]/div[1]/label')),batchValuesArray[0]);
    }
};
var validateEachDiscount = function(moqMarginElement,valuesArray){
    //*[@id="salePrice-0"]/div[2]/md-input-container
    expect(moqMarginElement.element(by.xpath('div[2]/*/input')).getAttribute('value')).toBe(valuesArray[0]);
    expect(moqMarginElement.element(by.xpath('div[3]/*/input')).getAttribute('value')).toBe(valuesArray[1]);
};
exports.validateMOQDiscounts = function(discountsTab,stockValuesArray){
   var saleContent = discountsTab.element(by.xpath('//md-content[@id=\'sale-discounts\']'));
   for(var i=0;i<stockValuesArray.length;i++) {
       if(stockValuesArray[i].type==='MOQ') {
           validateEachDiscount(element(by.xpath('//md-content[@id=\'sale-discounts\']/div[1]//div[@id="salePrice-' + i + '"]')), stockValuesArray[i].values);
       }
       else {
           validateEachDiscount(element(by.xpath('//md-content[@id=\'sale-discounts\']/div[2]//div[@id="saleValue-' + i + '"]')), stockValuesArray[i].values);
       }
    }
};

exports.validateBlankMOQDiscounts = function(discountsTab,type){
    var saleContent = discountsTab.element(by.xpath('//md-content[@id=\'sale-discounts\']'));
    if(type==='MOQ') {
        expect(element(by.xpath('//md-content[@id=\'sale-discounts\']/div[1]//div[@id="salePrice-' + 0 + '"]')).isPresent()).toBe(false);
    }
    else{
        expect(element(by.xpath('//md-content[@id=\'sale-discounts\']/div[1]//div[@id="saleValue-' + 0 + '"]')).isPresent()).toBe(false);
    }

};
