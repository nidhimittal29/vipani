/**
 * This file uses the Page Object pattern to define the main page for tests
 * https://docs.google.com/presentation/d/1B6manhG0zEXkC-H-tPo2vwU06JhL8w9-XCF9oehXzAQ
 */



'use strict';
var form;
exports.formData=function(form,data) {
    form=form;
    for (var prop in data) {
        var formElem = form[prop];
        if (data.hasOwnProperty(prop) && formElem && typeof formElem.sendKeys === 'function') {
            formElem.sendKeys(data[prop]);
        }
    }
};
exports.getForm=function() {
    return form;
};
