/**
 * This file uses the Page Object pattern to define the main page for tests
 * https://docs.google.com/presentation/d/1B6manhG0zEXkC-H-tPo2vwU06JhL8w9-XCF9oehXzAQ
 */



'use strict';

exports.loadPage=function (url) {
    browser.manage().deleteAllCookies();
    browser.get(url);
};
exports.getMatchedElement=function (element,selectedlement) {
  return element.all(selectedlement).filter(function (eachElement) {
        return eachElement.isDisplayed();
    }).first();
};
