/**
 * This file uses the Page Object pattern to define the main page for tests
 * https://docs.google.com/presentation/d/1B6manhG0zEXkC-H-tPo2vwU06JhL8w9-XCF9oehXzAQ
 */



'use strict';
var form,formData=require('./form.data.common.po'),global=require('./global.common.po');

function eachSignin(data) {
    form = element(by.name('signup'));
    form.username = form.element(by.model('credentials.username'));
    form.password = form.element(by.model('credentials.password'));
    form.submit = form.element(by.css('.md-button'));
    formData.formData(form,data);
}
exports.signin =eachSignin;
function eachSignupPage(data) {
    form  = element(by.name('userForm'));
    form.username = form.element(by.model('username'));
    form.password = form.element(by.model('password'));
    form.otp = form.element(by.model('otp'));
    form.companyName = form.element(by.model('companyName'));
    form.radioSegments=form.element(by.model('selectedSegments'));
    form.checkboxSegments=form.element(by.model('segment.selected'));
    form.submit = form.element(by.css('.md-primary'));
    if(data) {
        formData.formData(form, data);
    }
}
exports.signupPage=eachSignupPage;
exports.forgotPassword=function (data) {
    form = this.form = element(by.name('password'));
    form.username1 = form.element(by.model('passwordcredentials.username'));
    form.existingOtp = form.element(by.model('passwordcredentials.forgotPasswordOtp'));
    form.newPassword = form.element(by.model('passwordcredentials.newPassword'));
    form.verifyPassword = form.element(by.model('passwordcredentials.verifyPassword'));
    form.submit = form.element(by.css('button'));
    if(data) {
        formData.formData(form, data);
    }
};
function logout() {
    var elm = element(by.id('profilemenu'));
    if(element(by.id('profilemenu')).isPresent() && element(by.id('profilemenu')).isDisplayed()) {
        element(by.id('profilemenu')).click();
        browser.sleep(2000);
        element(by.id('logout')).click();
    }
}
exports.logout= function () {
    logout();
};
exports.getForm=function() {
    return form;
};
exports.login=function(data){
    eachSignin(data);
    form.submit.click();
};
exports.registerProcess=function (data) {
    eachSignupPage(data);
    form.submit.click();
    browser.sleep(4000);
    var text1 = browser.findElement(by.binding("success")).getText().then(function (ss1) {

        return ss1.split(". ");
    });
    text1.then(function (slices) {

        var otp2 = slices[1];
        var otpValue = otp2.split(' ');
        form.otp.sendKeys(otpValue[0]);
    });
    form.submit.click();
    browser.sleep(2000);
    element(by.css('md-radio-button[aria-label^='+data.registrationCategory+']')).click();

    /* user.getForm().companyName.sendKeys('nVipania3');*/

    element(by.xpath('//button[@aria-label="Next"]')).click();
    browser.sleep(2000);
    data.selectedSegments.forEach(function(segment){
        element(by.css('md-checkbox[aria-label='+segment+']')).click();
    });
    browser.findElement(by.partialButtonText("Start")).click();
    logout();
};
