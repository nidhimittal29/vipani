
'use strict';
var   path = require('path');
var editContactForm,formData=require('./form.data.common.po');


exports.fileImportContacts=function (filePath) {

    browser.findElement(by.id("nav-contacts")).click();
    browser.sleep(1000);
    browser.findElement(by.id('importcontactshome')).isDisplayed().then(function(visible) {
        console.log("Visible:"+visible);
        if (visible) {
            browser.sleep(1000);
            browser.findElement(by.id("importcontactshome")).click();
            browser.sleep(1000);
            var contactsPath = path.resolve(__dirname, filePath);
            console.log("Contacts file path:"+contactsPath);
            browser.findElement(by.id("contactsbrowsefile")).sendKeys(contactsPath);
            browser.sleep(2000);
        }
        else {
/*
            var menus = element.all(by.id("menu"));
            menus.each(function(menu){
                menu.isDisplayed().then(function(visible){
                    console.log(visible);
                    menu.click();
                    browser.sleep(1000);
                    menu.findElement(by.id('importcontactsmenuhome')).click();
                });
            });
            var EC = protractor.ExpectedConditions;
            browser.sleep(1000);
            element.all(by.id('importcontactsmenuhome')).get(2).click();
*/

            var menuitem = browser.findElement(by.id("menu"));
            menuitem.click();
            browser.sleep(1000);
            var actionitems = element.all(by.id('importcontactsmenuhome'));
            actionitems.each(function(item){
                if(item.isDisplayed()){
                    item.click();
                    browser.sleep(1000);
                    var contactsPath = path.resolve(__dirname, filePath);
                    console.log("Contacts file path:"+contactsPath);
                    browser.findElement(by.id("contactsbrowsefile")).sendKeys(contactsPath);
                    browser.sleep(2000);
                }
            });
        }
    });
    /*}else {
        browser.findElement(by.id("menu")).click();
        browser.sleep(1000);
        browser.findElement(by.id("importcontactsmenuhome")).click();
    }*/

};

exports.getEditContactsForm = function(){
    return element(by.xpath('//form[@name="editContactForm"'));
}

exports.initiateContacts = function(){
    browser.sleep(500);
    element(by.id("nav-contacts")).click();
};
exports.initiateGroups = function(){
    browser.sleep(500);
    element(by.id("nav-contacts")).click();
    element(by.xpath('//*[@id="contact-tabs"]/md-tabs/*//md-tab-item[3]')).click();
};


exports.initiateCreateContact = function(type){
    browser.findElement(by.id("nav-contacts")).click();
    browser.sleep(500);
    if(type==='Customers') {
        var custXpath = element(by.xpath('//*[@id="contact_Customers"]/*//*[@id=\'create-customer\']'));
        if (browser.isElementPresent(custXpath) && custXpath.isDisplayed()) {
            custXpath.click();
        } else {
            browser.findElement(by.id("menu")).click();
            browser.sleep(1000);
            browser.findElement(by.linkText("New Contact")).click();
        }
    }
    else{
        var supXpath = element(by.xpath('//*[@id="contact_Suppliers"]/*//*[@id="create-supplier"]'));
        element(by.xpath('//*[@id="contact-tabs"]/md-tabs/*//md-tab-item[2]')).click();
        if (browser.isElementPresent(supXpath) && supXpath.isDisplayed()) {
            supXpath.click();
        } else {
            browser.findElement(by.id("menu")).click();
            browser.sleep(1000);
            browser.findElement(by.linkText("New Contact")).click();
        }
    }
    browser.sleep(1000);
};

exports.initiateCreateGroup = function(){
    browser.findElement(by.id("nav-contacts")).click();
    browser.sleep(500);
    var supXpath = element(by.xpath('//*[@id="contact_Groups"]/*//*[@aria-label="Create new group"]'));
    element(by.xpath('//*[@id="contact-tabs"]/md-tabs/*//md-tab-item[3]')).click();
    if (browser.isElementPresent(supXpath) && supXpath.isDisplayed()) {
        supXpath.click();
    } else {
        browser.findElement(by.id("menu")).click();
        browser.sleep(1000);
        browser.findElement(by.linkText("New Group")).click();
    }
    browser.sleep(1000);
};
exports.deleteGroups = function() {
    var list = element.all(by.xpath('//md-content[@id=\'contact_Groups\']/view-groups/table/tbody/tr/*/md-checkbox'));
    list.each(function (item) {
        item.click();
    });
    element(by.id('groupactionsmenu_Groups')).click();
    element(by.id('deleteaction_Groups')).click();
};

exports.deleteContacts = function(type) {
    var list = element.all(by.xpath('//md-content[@id=\'contact_'+type+'\']/view-contacts/table/tbody/tr/*/md-checkbox'));
    list.each(function (item) {
        item.click();
    });
    element(by.id('groupactionsmenu_'+type)).click();
    element(by.id('deleteaction_'+type)).click();
};

exports.sendAddressData = function(type,addressType,address,count){

    if(addressType!='Billing'){
        element(by.xpath('//md-content[@id=\'tab_'+addressType+'\']/div/span')).click();
    }
    browser.driver.executeScript("arguments[0].scrollIntoView(true);",
        element(by.xpath('//md-content[@id=\'tab_'+addressType+'\']//div[@id="address-'+count+'"]//*[@ng-model="address.addressLine"]')).getWebElement());

    element(by.xpath('//md-content[@id="tab_'+addressType+'"]//div[@id="address-'+count+'"]//*[@ng-model="address.addressLine"]'))
        .sendKeys(address.addressLine);
    element(by.xpath('//md-content[@id="tab_'+addressType+'"]//div[@id="address-'+count+'"]//*[@ng-model="address.city"]'))
        .sendKeys(address.city);
    element(by.xpath('//md-content[@id="tab_'+addressType+'"]//div[@id="address-'+count+'"]//*[@ng-model="address.state"]'))
        .sendKeys(address.state);
    element(by.xpath('//md-content[@id="tab_'+addressType+'"]//div[@id="address-'+count+'"]//*[@ng-model="address.country"]'))
        .sendKeys(address.country);
    element(by.xpath('//md-content[@id="tab_'+addressType+'"]//div[@id="address-'+count+'"]//*[@ng-model="address.pinCode"]'))
        .sendKeys(address.pinCode);

};
exports.validateAddress = function(addressElement,address){
    expect(addressElement.element(by.xpath('//*[@id="addressLine"]')).
    getAttribute('value')).toBe(address.addressLine);
    expect(addressElement.element(by.xpath('//*[@id="city"]')).
    getAttribute('value')).toBe(address.city);
    expect(addressElement.element(by.xpath('//*[@id="state"]')).
    getAttribute('value')).toBe(address.state);
    expect(addressElement.element(by.xpath('//*[@id="country"]')).
    getAttribute('value')).toBe(address.country);
    expect(addressElement.element(by.xpath('//*[@id="pinCode"]')).
    getAttribute('value')).toBe(address.pinCode);
};
exports.validatePhones = function(phonesElement,phonesArray){

}

exports.validateEmails = function(phonesElement,emailsArray){

}

