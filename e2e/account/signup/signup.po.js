/**
 * This file uses the Page Object pattern to define the main page for tests
 * https://docs.google.com/presentation/d/1B6manhG0zEXkC-H-tPo2vwU06JhL8w9-XCF9oehXzAQ
 */

'use strict';

var signupPage = function() {
  var form = this.form = element(by.name('userForm'));
    form.username = form.element(by.model('username'));
    form.password = form.element(by.model('password'));
    form.otp = form.element(by.model('otp'));
    form.companyName = form.element(by.model('companyName'));
    form.radioSegments=form.element(by.model('selectedSegments'));
    form.checkboxSegments=form.element(by.model('segment.selected'));
    form.submit = form.element(by.css('button[type="submit"]'));
  this.signup = function(data) {
    for (var prop in data) {
      var formElem = form[prop];
      if (data.hasOwnProperty(prop) && formElem && typeof formElem.sendKeys === 'function') {
        formElem.sendKeys(data[prop]);
      }
    }
  };
};

module.exports = new signupPage();
















































