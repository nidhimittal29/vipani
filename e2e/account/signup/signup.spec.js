
'use strict';

var config = browser.params;
//var UserModel = require(config.serverConfig.root + '/server/api/user/user.model').default;

describe('Signup View', function() {
  var page;
  var loginTestData;

  var loadPage = function() {
    browser.manage().deleteAllCookies();
      browser.get('http://localhost:3000/#!/register');

  };

    describe('with local auth', function() {
        beforeEach(function() {
            loadPage();
            page = require('./signup.po');
            loginTestData=require('./signup.testdata');
        });


        //1.TEST CASE FOR MISSING EMAIL/PHONE NUMBER ENTRY USERFORM[REGISTRATION] STEP-1 //
        it('should show error message for missing email/phone number entry step-1 and redirectong to "/register"', function () {
            page.form.submit.click();
            browser.sleep(2000);
            expect(page.form.username.getAttribute('required')).toBeDefined();
            expect(browser.getCurrentUrl()).toEqual('http://localhost:3000/#!/register');
        });

        //2.TEST CASE FOR MISSING PASSWORD ENTRY USERFORM[REGISTRATION] STEP-1 //
        it('should show error message for missing password entry step-2 and redirecting to "/register"', function() {
          page.signup(loginTestData[0]);
          page.form.submit.click();
          browser.sleep(2000);
          expect(page.form.password.getAttribute('required')).toBeDefined();
          expect(browser.getCurrentUrl()).toEqual('http://localhost:3000/#!/register');
      });

        //3.TEST CASE FOR INVALID MOBILE ENTRY  USERFORM[REGISTRATION] STEP-1 //
        it('should show error message for invalid mobile entry and redirecting to "/register"', function() {
          page.signup(loginTestData[1]);
          page.form.submit.click();
          browser.sleep(2000);
          browser.findElement(by.css('md-dialog [aria-label="Ok"]')).click();
          expect(browser.getCurrentUrl()).toEqual('http://localhost:3000/#!/register');
      });

        //4.TEST CASE FOR INVALID EMAIL ENTRY USERFORM[REGISTRATION] STEP-1 //
        it('should show error message for invalid email entry, and redirecting to "/register"', function() {
          page.signup(loginTestData[2]);
          page.form.submit.click();
          browser.sleep(2000);
          browser.findElement(by.css('md-dialog [aria-label="Ok"]')).click();
          expect(browser.getCurrentUrl()).toEqual('http://localhost:3000/#!/register');
      });

        //5.TEST CASE FOR NAVIGATE TO LOGIN PAGE USERFORM[REGISTRATION] STEP-1 //
        it('should display the login page , and redirecting to "/signin"',function () {
          browser.findElement(by.linkText("Existing User?")).click();
          expect(browser.getCurrentUrl()).toEqual('http://localhost:3000/#!/signin');
      });

        //6.VERIFYING WHETHER OTP IS GENERATED OR NOT USING EMAIL ID USERFORM[REGISTRATION] STEP-1 TO STEP-2 //
        it('should display the otp and verify the otp text' , function() {
            page.signup(loginTestData[3]);
            page.form.submit.click();
            browser.sleep(4000);
            var text1 = browser.findElement(by.binding("success")).getText().then(function (ss1) {

                return ss1.split(". ");
            });
            text1.then(function (slices) {

                var otp2 = slices[1];
                expect(browser.findElement(by.binding("success")).getText()).
                toBe('An OTP has been sent to Email :'+loginTestData[3].username+'. '+otp2);
            });
        });

        //7.TEST CASE FOR WRONG INPUT OF OTP USERFORM[REGISTRATION] STEP-2 //
        it('should display the error message for incorrect otp', function() {
            page.signup(loginTestData[3]);
            page.form.submit.click();
            browser.sleep(4000);
            expect(browser.getCurrentUrl()).toEqual('http://localhost:3000/#!/register');
            page.form.otp.sendKeys('123456');
            page.form.submit.click();
            expect(browser.findElement(by.binding("registererror")).getText()).
            toBe('Incorrect otp for the Email: ' + loginTestData[3].username);
        });

        //8.TEST CASE FOR NAVIGATE TO LOGIN PAGE USERFORM[REGISTRATION] STEP-2 //
        it('should find the button for login existing user, and redirecting to "/signin"',function () {
            browser.findElement(by.linkText("Existing User?")).click();
            expect(browser.getCurrentUrl()).toEqual('http://localhost:3000/#!/signin')
        });

        //9.TEST CASE FOR CLICKING RESEND OTP USERFORM[REGISTRATION] STEP-2 //
        it('should signup a new user, log them in, and redirecting to "/"', function() {
            page.signup(loginTestData[3]);
            page.form.submit.click();
            browser.sleep(4000);
            var text1 = browser.findElement(by.binding("success")).getText().then(function (ss1) {

                return ss1.split(". ");
            });
            text1.then(function (slices) {

                var otp2 = slices[1];
                expect(browser.findElement(by.binding("success")).getText()).
                toBe('An OTP has been sent to Email :'+loginTestData[3].username+'. '+otp2);
            });
            browser.findElement(by.linkText("Resend OTP")).click();
            expect(browser.getCurrentUrl()).toEqual('http://localhost:3000/#!/register');
            var text1 = browser.findElement(by.binding("success")).getText().then(function (ss1) {

                return ss1.split(". ");
            });
            text1.then(function (slices) {

                var otp2 = slices[1];
                expect(browser.findElement(by.binding("success")).getText()).
                toBe('An OTP has been sent to Email :'+loginTestData[3].username+'. '+otp2);
            });
        });

        //10.TEST CASE FOR SUCCESSFULL VERIFICATION OF NEW USER OTP USERFORM[REGISTRATION] STEP-2 to step-3 //
        it('should accept the given otp and proceed to next page', function() {
            page.signup(loginTestData[4]);
            page.form.submit.click();
            browser.sleep(4000);
            var text1 = browser.findElement(by.binding("success")).getText().then(function (ss1) {

                return ss1.split(". ");
            });
            text1.then(function (slices) {

                var otp2 = slices[1];
                page.form.otp.sendKeys(otp2);
            });
            page.form.submit.click();
            browser.sleep(4000);
            expect(browser.getCurrentUrl()).toEqual('http://localhost:3000/#!/register');
            expect(browser.findElement(by.cssContainingText('.nvc-signup-tab2 h2.nvc-primary-color', 'start Trading')).getText()).toBe('1. You\'d like to start Trading as');
        });

        //11.TEST CASE FOR ALREADY VERIFIED USER USERFORM[REGISTRATION] STEP-1 AND STEP-3 //
        it('should skip otp for already verified user', function() {
          page.signup(loginTestData[4]);
          page.form.submit.click();
          browser.sleep(2000);
          expect(browser.getCurrentUrl()).toEqual('http://localhost:3000/#!/register');
          expect(browser.findElement(by.cssContainingText('.nvc-signup-tab2 h2.nvc-primary-color', 'start Trading')).getText()).toBe('1. You\'d like to start Trading as');
      });

        //12.TEST CASE FOR SELECT BUSINESS CATEGORY USERFORM[REGISTRATION] STEP-3 //
        it('should proceed to 2nd page of registration page after selecting business category', function() {
            page.signup(loginTestData[5]);
            page.form.submit.click();
            browser.sleep(4000);
            var text1 = browser.findElement(by.binding("success")).getText().then(function (ss1) {

                return ss1.split(". ");
            });
            text1.then(function (slices) {

                var otp2 = slices[1];
                page.form.otp.sendKeys(otp2);
            });
            page.form.submit.click();
            browser.sleep(4000);
            expect(browser.getCurrentUrl()).toEqual('http://localhost:3000/#!/register');
            expect(browser.findElement(by.cssContainingText('.nvc-signup-tab2 h2.nvc-primary-color', 'start Trading')).getText()).toBe('1. You\'d like to start Trading as');
            browser.findElement(by.css('[aria-label~=Manufacturer]')).click();
            page.form.companyName.sendKeys('nVipani1');
            browser.findElement(by.css('[ng-show=rcategory]')).click();
            browser.sleep(3000);
            expect(browser.findElement(by.css(".nvc-signup-tab3 h2.nvc-primary-color")).getText()).toBe('2. Choose your Trading Segments');
        });

        //13.TEST CASE FOR SELECT BUSINESS SEGMENTS USERFORM[REGISTRATION] STEP-4 //
        it('should select the business sement after selecting business category', function() {
            page.signup(loginTestData[6]);
            page.form.submit.click();
            browser.sleep(4000);
            var text1 = browser.findElement(by.binding("success")).getText().then(function (ss1) {

                return ss1.split(". ");
            });
            text1.then(function (slices) {

                var otp2 = slices[1];
                page.form.otp.sendKeys(otp2);
            });
            page.form.submit.click();
            browser.sleep(4000);
            expect(browser.getCurrentUrl()).toEqual('http://localhost:3000/#!/register');
            expect(browser.findElement(by.cssContainingText('.nvc-signup-tab2 h2.nvc-primary-color', 'start Trading')).getText()).toBe('1. You\'d like to start Trading as');
            browser.findElement(by.css('[aria-label~=Manufacturer]')).click();
            page.form.companyName.sendKeys('nVipani2');
            browser.findElement(by.css('[ng-show=rcategory]')).click();
            browser.sleep(3000);
            expect(browser.findElement(by.css(".nvc-signup-tab3 h2.nvc-primary-color")).getText()).toBe('2. Choose your Trading Segments');
            browser.findElement(by.css("md-radio-button[aria-label=Rice]")).click();
            expect(browser.findElement(by.css('md-radio-button[aria-label=Rice] label')).getText()).toBe('Rice');
        });

        //14.TEST CASE FOR REGISTRATION WITH BUSINESS CATEGORY MANUFACTURER USERFORM[REGISTRATION] STEP-1 TO STEP-4 //
        it('should signup a new user with registration category manufacturer and segment rice and signout the user', function() {
          page.signup(loginTestData[7]);
          page.form.submit.click();
          browser.sleep(4000);
          var text1 = browser.findElement(by.binding("success")).getText().then(function (ss1) {

              return ss1.split(". ");
          });
          text1.then(function (slices) {

              var otp2 = slices[1];
              page.form.otp.sendKeys(otp2);
          });
          page.form.submit.click();
          browser.sleep(4000);
          expect(browser.getCurrentUrl()).toEqual('http://localhost:3000/#!/register');
          expect(browser.findElement(by.cssContainingText('.nvc-signup-tab2 h2.nvc-primary-color', 'start Trading')).getText()).toBe('1. You\'d like to start Trading as');
          browser.findElement(by.css('[aria-label~=Manufacturer]')).click();
          page.form.companyName.sendKeys('nVipania3');
          browser.findElement(by.css('[ng-show=rcategory]')).click();
          browser.sleep(3000);
          expect(browser.findElement(by.css(".nvc-signup-tab3 h2.nvc-primary-color")).getText()).toBe('2. Choose your Trading Segments');
          var rice = browser.driver.findElement(by.css("md-radio-button[aria-label=Rice]"));
          rice.click();
          expect(rice.getText('aria-label')).toBe("Rice");
          browser.findElement(by.partialButtonText("Start")).click();
          browser.sleep(5000);
          browser.findElement(by.css('[aria-label~=menu]')).click();
          browser.findElement(by.css('[aria-label=Sign-out]')).click();
      });

        //15.TEST CASE FOR ALREADY REGISTERED USER USERFORM[REGISTRATION] STEP-1 //
        it('should display already registered message for existing user', function() {
            page.signup(loginTestData[8]);
            page.form.submit.click();
            browser.sleep(4000);
            var text1 = browser.findElement(by.binding("success")).getText().then(function (ss1) {
                return ss1.split(". ");
            });
            text1.then(function (slices) {
                var otp2 = slices[1];
                page.form.otp.sendKeys(otp2);
            });
            page.form.submit.click();
            browser.sleep(4000);
            expect(browser.getCurrentUrl()).toEqual('http://localhost:3000/#!/register');
            expect(browser.findElement(by.cssContainingText('.nvc-signup-tab2 h2.nvc-primary-color', 'start Trading')).getText()).toBe('1. You\'d like to start Trading as');
            browser.findElement(by.css('[aria-label~=Manufacturer]')).click();
            page.form.companyName.sendKeys('nVipaniaa4');
            browser.findElement(by.css('[ng-show=rcategory]')).click();
            browser.sleep(3000);
            expect(browser.findElement(by.css(".nvc-signup-tab3 h2.nvc-primary-color")).getText()).toBe('2. Choose your Trading Segments');
            var rice = browser.driver.findElement(by.css("md-radio-button[aria-label=Rice]"));
            rice.click();
            expect(rice.getText('aria-label')).toBe("Rice");
            browser.findElement(by.partialButtonText("Start")).click();
            browser.sleep(5000);
            browser.findElement(by.css('[aria-label~=menu]')).click();
            browser.findElement(by.css('[aria-label=Sign-out]')).click();
            browser.findElement(by.css('a[href$="register"]')).click();
            page.signup(loginTestData[8]);
            page.form.submit.click();
            expect(browser.findElement(by.binding("registererror")).getText()).toBe('User is already Registered');
        });

        //16.TEST CASE FOR TOGGLING OF EYE ICON IN PASSWORD FIELD NEW REGISTRATION STEP-1 //
        it('should toggle the display type of password', function() {
            page.signup(loginTestData[9]);
            page.form.element(by.model('showpassword')).click();
            expect(page.form.password.getText().isDisplayed()).toBe(true);
            page.form.element(by.model('showpassword')).click();
            browser.sleep(2000);
            expect(browser.findElement(By.model("password")).getAttribute("type")).toBe("password");
        });

        //17.TEST CASE FOR REGISTRATION WITH BUSINESS CATEGORY DISTRIBUTOR USERFORM[REGISTRATION]STEP-1 TO STEP-4 //
        it('should signup a new userwith business category distributor', function() {
            page.signup(loginTestData[10]);
            page.form.submit.click();
            browser.sleep(4000);
            var text1 = browser.findElement(by.binding("success")).getText().then(function (ss1) {

                return ss1.split(". ");
            });
            text1.then(function (slices) {

                var otp2 = slices[1];
                page.form.otp.sendKeys(otp2);
            });
            page.form.submit.click();
            browser.sleep(4000);
            expect(browser.getCurrentUrl()).toEqual('http://localhost:3000/#!/register');
            expect(browser.findElement(by.cssContainingText('.nvc-signup-tab2 h2.nvc-primary-color', 'start Trading')).getText()).toBe('1. You\'d like to start Trading as');
            browser.findElement(by.css('[aria-label~=Distributor]')).click();
            page.form.companyName.sendKeys('nVipani5');
            browser.findElement(by.css('[ng-show=rcategory]')).click();
            browser.sleep(3000);
            expect(browser.findElement(by.css(".nvc-signup-tab3 h2.nvc-primary-color")).getText()).toBe('2. Choose your Trading Segments');
            browser.findElement(by.css('md-checkbox[aria-label=Rice]')).click();
            browser.findElement(by.css('md-checkbox[aria-label=Coffee]')).click();
            browser.findElement(by.partialButtonText("Start")).click();
            browser.sleep(5000);
            browser.findElement(by.css('[aria-label~=menu]')).click();
            browser.findElement(by.css('[aria-label=Sign-out]')).click();
            expect(browser.getCurrentUrl()).toEqual('http://localhost:3000/#!/');
        });

        //18.REGISTRATION WITH BUSINESS CATEGORY RETAILER USERFORM[REGISTRATION] STEP-1 TO STEP-4 //
        it('should signup a new user with retailer category', function() {
            page.signup(loginTestData[11]);
            page.form.submit.click();
            browser.sleep(4000);
            var text1 = browser.findElement(by.binding("success")).getText().then(function (ss1) {

                return ss1.split(". ");
            });
            text1.then(function (slices) {

                var otp2 = slices[1];
                page.form.otp.sendKeys(otp2);
            });
            page.form.submit.click();
            browser.sleep(4000);
            expect(browser.getCurrentUrl()).toEqual('http://localhost:3000/#!/register');
            expect(browser.findElement(by.cssContainingText('.nvc-signup-tab2 h2.nvc-primary-color', 'start Trading')).getText()).toBe('1. You\'d like to start Trading as');
            browser.findElement(by.css('[aria-label~=Retailer]')).click();
            page.form.companyName.sendKeys('nVipani6');
            browser.findElement(by.css('[ng-show=rcategory]')).click();
            browser.sleep(3000);
            expect(browser.findElement(by.css(".nvc-signup-tab3 h2.nvc-primary-color")).getText()).toBe('2. Choose your Trading Segments');
            browser.findElement(by.css('md-checkbox[aria-label=Cereals]')).click();
            browser.findElement(by.partialButtonText("Start")).click();
            browser.sleep(5000);
            browser.findElement(by.css('[aria-label~=menu]')).click();
            browser.findElement(by.css('[aria-label=Sign-out]')).click();
        });

        //19.TEST CASE FOR CHECKING SINGLE SELECTION OF SEGMENTS FOR MANUFACTURER USERFORM[REGISTRATION] STEP-4 //
        it('should check for radio elements for manufacturer', function () {
            page.signup(loginTestData[12]);
            page.form.submit.click();
            browser.sleep(4000);
            var text1 = browser.findElement(by.binding("success")).getText().then(function (ss1) {

                return ss1.split(". ");
            });
            text1.then(function (slices) {

                var otp2 = slices[1];
                page.form.otp.sendKeys(otp2);
            });
            page.form.submit.click();
            browser.sleep(2000);
            expect(browser.getCurrentUrl()).toEqual('http://localhost:3000/#!/register');
            expect(browser.findElement(by.cssContainingText('.nvc-signup-tab2 h2.nvc-primary-color', 'start Trading')).getText()).toBe('1. You\'d like to start Trading as');
            browser.findElement(by.css('[aria-label~=Manufacturer]')).click();
            page.form.companyName.sendKeys('nVipani7');
            browser.findElement(by.css('[ng-show=rcategory]')).click();
            browser.sleep(2000)
            expect(browser.findElement(by.css(".nvc-signup-tab3 h2.nvc-primary-color")).getText()).toBe('2. Choose your Trading Segments');
            //If the element used for segments displaying is radio buttons than it must allow user to select only one segment
            expect(page.form.radioSegments.isDisplayed()).toBe(true);
         });

        //20.TEST CASE FOR CHECKING THE LABELS OF THE BUSINESS SEGMENTS IN THE MANUFACTURER USERFORM[REGISTRATION] STEP-4 //
        it('should check the labels fo the business segments in the manufacturer registration form', function () {
            page.signup(loginTestData[13]);
            page.form.submit.click();
            browser.sleep(4000);
            var text1 = browser.findElement(by.binding("success")).getText().then(function (ss1) {

                return ss1.split(". ");
            });
            text1.then(function (slices) {

                var otp2 = slices[1];
                page.form.otp.sendKeys(otp2);
            });
            page.form.submit.click();
            browser.sleep(2000);
            expect(browser.getCurrentUrl()).toEqual('http://localhost:3000/#!/register');
            expect(browser.findElement(by.cssContainingText('.nvc-signup-tab2 h2.nvc-primary-color', 'start Trading')).getText()).toBe('1. You\'d like to start Trading as');
            browser.findElement(by.css('[aria-label~=Manufacturer]')).click();
            page.form.companyName.sendKeys('nVipani8');
            browser.findElement(by.css('[ng-show=rcategory]')).click();
            browser.sleep(2000)
            expect(browser.findElement(by.css(".nvc-signup-tab3 h2.nvc-primary-color")).getText()).toBe('2. Choose your Trading Segments');
            var rice = browser.driver.findElement(by.css("md-radio-button[aria-label=Rice]"));
            expect(rice.getText('aria-label')).toBe("Rice");
            var coffee = browser.driver.findElement(by.css("md-radio-button[aria-label=Coffee]"));
            expect(coffee.getText('aria-label')).toBe("Coffee");
        });

        //21.TEST CASE FOR CHECKING MULTIPLE SELECTION OF SEGMENTS FOR DISTRIBUTOR //
        it('should check multiple selection for distributor', function () {
            page.signup(loginTestData[14]);
            page.form.submit.click();
            browser.sleep(4000);
            var text1 = browser.findElement(by.binding("success")).getText().then(function (ss1) {

                return ss1.split(". ");
            });
            text1.then(function (slices) {

                var otp2 = slices[1];
                page.form.otp.sendKeys(otp2);
            });
            page.form.submit.click();
            browser.sleep(4000);
            expect(browser.getCurrentUrl()).toEqual('http://localhost:3000/#!/register');
            expect(browser.findElement(by.cssContainingText('.nvc-signup-tab2 h2.nvc-primary-color', 'start Trading')).getText()).toBe('1. You\'d like to start Trading as');
            browser.findElement(by.css('[aria-label~=Distributor]')).click();
            page.form.companyName.sendKeys('nVipani9');
            browser.findElement(by.css('[ng-show=rcategory]')).click();
            browser.sleep(3000)
            expect(browser.findElement(by.css(".nvc-signup-tab3 h2.nvc-primary-color")).getText()).toBe('2. Choose your Trading Segments');
            //If the element used for segments displaying is Check box than it must allow user to select multiple segment
            expect(page.form.checkboxSegments.isDisplayed()).toBe(true);
        });

        //22.TEST CASE FOR CHECKING THE LABELS OF THE BUSINESS SEGMENTS IN THE DISTRIBUTOR //
        it('should check the labels of business segments for the distrubutor category', function () {
            page.signup(loginTestData[15]);
            page.form.submit.click();
            browser.sleep(4000);
            var text1 = browser.findElement(by.binding("success")).getText().then(function (ss1) {

                return ss1.split(". ");
            });
            text1.then(function (slices) {

                var otp2 = slices[1];
                page.form.otp.sendKeys(otp2);
            });
            page.form.submit.click();
            browser.sleep(4000);
            expect(browser.getCurrentUrl()).toEqual('http://localhost:3000/#!/register');
            expect(browser.findElement(by.cssContainingText('.nvc-signup-tab2 h2.nvc-primary-color', 'start Trading')).getText()).toBe('1. You\'d like to start Trading as');
            browser.findElement(by.css('[aria-label~=Distributor]')).click();
            page.form.companyName.sendKeys('nVipani10');
            browser.findElement(by.css('[ng-show=rcategory]')).click();
            browser.sleep(3000)
            expect(browser.findElement(by.css(".nvc-signup-tab3 h2.nvc-primary-color")).getText()).toBe('2. Choose your Trading Segments');
            var rice = browser.driver.findElement(by.css('md-checkbox[aria-label=Rice]'));
            var coffee = browser.driver.findElement(by.css('md-checkbox[aria-label=Coffee]'));
            expect(rice.getText('aria-label')).toBe("Rice");
            expect(coffee.getText('aria-label')).toBe("Coffee");
        });

        //23.TEST CASE FOR CHECKING THE LABELS,IMAGES AND DISCRIPTION OF THE BUSINESS CATEGORIES USERFORM[REGISTRATION] STEP-3 //
        it('should check for images, labels and description for registration categories', function () {
            page.signup(loginTestData[16]);
            page.form.submit.click();
            browser.sleep(4000);
            var text1 = browser.findElement(by.binding("success")).getText().then(function (ss1) {

                return ss1.split(". ");
            });
            text1.then(function (slices) {

                var otp2 = slices[1];
                page.form.otp.sendKeys(otp2);
            });
            page.form.submit.click();
            browser.sleep(4000);
            expect(browser.getCurrentUrl()).toEqual('http://localhost:3000/#!/register');
            expect(browser.findElement(by.cssContainingText('.nvc-signup-tab2 h2.nvc-primary-color', 'start Trading')).getText()).toBe('1. You\'d like to start Trading as');
            //Check images for business categories
            expect(element(by.css("img[src='modules/core/img/nvc-img/Manufacturer.png']")).isPresent()).toBe(true);
            expect(element(by.css("img[src='modules/core/img/nvc-img/Distributor.png']")).isPresent()).toBe(true);
            expect(element(by.css("img[src='modules/core/img/nvc-img/Retailer.png']")).isPresent()).toBe(true);
            //Check Title for business categories
            expect(element.all(by.binding("item.name")).get(0).getText()).toEqual('Manufacturer');
            expect(element.all(by.binding("item.name")).get(1).getText()).toEqual('Distributor');
            expect(element.all(by.binding("item.name")).get(2).getText()).toEqual('Retailer');
            //Check comments for each business categories
            expect(element.all(by.binding("item.description")).get(0).getText()).toEqual('Manage all Business units & users Engaging mediators & Service Providers realtime insights about products research');
            expect(element.all(by.binding("item.description")).get(1).getText()).toEqual('Creative product showcasing Reach various customers & suppliers realtime analytics about market');
            expect(element.all(by.binding("item.description")).get(2).getText()).toEqual('Receive offers as required Automate the hectic manual processes Realtime Traceability about Product');
        });

        //24.TEST CASE FOR CHECK WITHOUT SELECT BUSINESS CATEGORY NOT POSSIBLE TO GO STEP-4 USERFORM[REGISTRATION] STEP-3 //
        it('should signup a new user, log them in, select business segment and redirecting to "/"', function () {
            page.signup(loginTestData[17]);
            page.form.submit.click();
            browser.sleep(4000);
            var text1 = browser.findElement(by.binding("success")).getText().then(function (ss1) {

                return ss1.split(". ");
            });
            text1.then(function (slices) {

                var otp2 = slices[1];
                page.form.otp.sendKeys(otp2);
            });
            page.form.submit.click();
            browser.sleep(4000);
            expect(browser.getCurrentUrl()).toEqual('http://localhost:3000/#!/register');
            expect(browser.findElement(by.cssContainingText('.nvc-signup-tab2 h2.nvc-primary-color', 'start Trading')).getText()).toBe('1. You\'d like to start Trading as');
            expect(element(by.css('[ng-show=rcategory]')).isDisplayed()).toBe(false);
        });

        //25.TEST CASE FOR CHECK NUMBER OF SELECTED SEGMENTS COUNT IS DISPLAYING CORRECTLY OR NOT USERFORM[REGISTRATION] STEP-4 //
        it('should signup a new user, log them in, select business segment and redirecting to "/"', function () {
            page.signup(loginTestData[18]);
            page.form.submit.click();
            browser.sleep(4000);
            var text1 = browser.findElement(by.binding("success")).getText().then(function (ss1) {

                return ss1.split(". ");
            });
            text1.then(function (slices) {

                var otp2 = slices[1];
                page.form.otp.sendKeys(otp2);
            });
            page.form.submit.click();
            browser.sleep(4000);
            expect(browser.getCurrentUrl()).toEqual('http://localhost:3000/#!/register');
            expect(browser.findElement(by.cssContainingText('.nvc-signup-tab2 h2.nvc-primary-color', 'start Trading')).getText()).toBe('1. You\'d like to start Trading as');
            browser.findElement(by.css('[aria-label~=Distributor]')).click();
            page.form.companyName.sendKeys('nVipani11');
            browser.findElement(by.css('[ng-show=rcategory]')).click();
            browser.sleep(3000);
            expect(browser.findElement(by.css(".nvc-signup-tab3 h2.nvc-primary-color")).getText()).toBe('2. Choose your Trading Segments');
            browser.findElement(by.css('md-checkbox[aria-label=Rice]')).click();
            browser.findElement(by.css('md-checkbox[aria-label=Coffee]')).click();
            expect(element(by.binding("selectedCount")).getText()).toEqual('2');

        });

        //26.TEST CASE FOR VALIDATING ALREADY REGISTERED COMPANY NAME USERFORM[REGISTRATION] STEP-4 //
        it('should check for the already registered company name and display error message', function() {
            page.signup(loginTestData[19]);
            page.form.submit.click();
            browser.sleep(4000);
            var text1 = browser.findElement(by.binding("success")).getText().then(function (ss1) {

                return ss1.split(". ");
            });
            text1.then(function (slices) {

                var otp2 = slices[1];
                page.form.otp.sendKeys(otp2);
            });
            page.form.submit.click();
            browser.sleep(4000);
            expect(browser.getCurrentUrl()).toEqual('http://localhost:3000/#!/register');
            expect(browser.findElement(by.cssContainingText('.nvc-signup-tab2 h2.nvc-primary-color', 'start Trading')).getText()).toBe('1. You\'d like to start Trading as');
            browser.findElement(by.css('[aria-label~=Distributor]')).click();
            page.form.companyName.sendKeys('nVipani13');
            browser.findElement(by.css('[ng-show=rcategory]')).click();
            browser.sleep(3000);
            expect(browser.findElement(by.css(".nvc-signup-tab3 h2.nvc-primary-color")).getText()).toBe('2. Choose your Trading Segments');
            var rice = browser.driver.findElement(by.css('md-checkbox[aria-label=Rice]'));
            rice.click();
            expect(rice.getText('aria-label')).toBe("Rice");
            browser.findElement(by.partialButtonText('Start')).click();
            browser.findElement(by.css('[aria-label~=menu]')).click();
            browser.findElement(by.css('[aria-label=Sign-out]')).click();
            browser.findElement(by.css('a[href$="register"]')).click();

            page.signup(loginTestData[40]);
            page.form.submit.click();
            browser.sleep(4000);
            var text1 = browser.findElement(by.binding("success")).getText().then(function (ss1) {

                return ss1.split(". ");
            });
            text1.then(function (slices) {

                var otp2 = slices[1];
                page.form.otp.sendKeys(otp2);
            });
            page.form.submit.click();
            browser.sleep(4000);
            expect(browser.getCurrentUrl()).toEqual('http://localhost:3000/#!/register');
            expect(browser.findElement(by.cssContainingText('.nvc-signup-tab2 h2.nvc-primary-color', 'start Trading')).getText()).toBe('1. You\'d like to start Trading as');
            browser.findElement(by.css('[aria-label~=Distributor]')).click();
            page.form.companyName.sendKeys('nVipani13');
            browser.findElement(by.css('[ng-show=rcategory]')).click();
            browser.sleep(3000);
            expect(browser.findElement(by.css(".nvc-signup-tab3 h2.nvc-primary-color")).getText()).toBe('2. Choose your Trading Segments');
            var rice = browser.driver.findElement(by.css('md-checkbox[aria-label=Rice]'));
            rice.click();
            expect(rice.getText('aria-label')).toBe("Rice");
            browser.findElement(by.partialButtonText('Start')).click();
            expect(element.all(by.css('.nvc-signup-tab3 .text-danger.bb-wid-100')).get(0).getText()).toBe('Someone has already registered with the company name - nVipani13. Please contact info@invipani.com');
        });

        //27.TEST CASE FOR REGISTER WITH ALREADY VERIFIED USERNAME   USERFORM[REGISTRATION] STEP-1 TO STEP-4 //
        it('should skip the otp page for already verified user', function() {
            page.signup(loginTestData[20]);
            page.form.submit.click();
            browser.sleep(4000);
            var text1 = browser.findElement(by.binding("success")).getText().then(function (ss1) {

                return ss1.split(". ");
            });
            text1.then(function (slices) {

                var otp2 = slices[1];
                page.form.otp.sendKeys(otp2);
            });
            page.form.submit.click();
            browser.sleep(4000);
            expect(browser.getCurrentUrl()).toEqual('http://localhost:3000/#!/register');
            expect(browser.findElement(by.cssContainingText('.nvc-signup-tab2 h2.nvc-primary-color', 'start Trading')).getText()).toBe('1. You\'d like to start Trading as');
            browser.findElement(by.css('a[href$="contact"]')).click();
            browser.findElement(by.css('a[href$="register"]')).click();
            page.signup(loginTestData[20]);
            page.form.submit.click();
            browser.sleep(4000);
            expect(browser.findElement(by.cssContainingText('.nvc-signup-tab2 h2.nvc-primary-color', 'start Trading')).getText()).toBe('1. You\'d like to start Trading as');
            browser.findElement(by.css('[aria-label~=Distributor]')).click();
            page.form.companyName.sendKeys('nVipani14');
            browser.findElement(by.css('[ng-show=rcategory]')).click();
            browser.sleep(3000);
            expect(browser.findElement(by.css(".nvc-signup-tab3 h2.nvc-primary-color")).getText()).toBe('2. Choose your Trading Segments');
            var rice = browser.driver.findElement(by.css('md-checkbox[aria-label=Rice]'));
            rice.click();
            expect(rice.getText('aria-label')).toBe("Rice");
            browser.findElement(by.partialButtonText('Start')).click();
            browser.sleep(5000);
            browser.findElement(by.css('[aria-label~=menu]')).click();
            browser.findElement(by.css('[aria-label=Sign-out]')).click();
        });

        //28.TEST CASE FOR VALIDATION FOR SELECTING BUSINESS SEGMENTS USERFORM[REGISTRATION] STEP-4 //
        it('should display error message for zero segment selection', function() {
            page.signup(loginTestData[21]);
            page.form.submit.click();
            browser.sleep(4000);
            var text1 = browser.findElement(by.binding("success")).getText().then(function (ss1) {

                return ss1.split(". ");
            });
            text1.then(function (slices) {

                var otp2 = slices[1];
                page.form.otp.sendKeys(otp2);
            });
            page.form.submit.click();
            browser.sleep(4000);
            expect(browser.getCurrentUrl()).toEqual('http://localhost:3000/#!/register');
            expect(browser.findElement(by.cssContainingText('.nvc-signup-tab2 h2.nvc-primary-color', 'start Trading')).getText()).toBe('1. You\'d like to start Trading as');
            browser.findElement(by.css('[aria-label~=Manufacturer]')).click();
            page.form.companyName.sendKeys('nVipani15');
            browser.findElement(by.css('[ng-show=rcategory]')).click();
            browser.sleep(3000);
            expect(browser.findElement(by.css(".nvc-signup-tab3 h2.nvc-primary-color")).getText()).toBe('2. Choose your Trading Segments');
            browser.findElement(by.partialButtonText('Start')).click();
            expect(element.all(by.binding("registererror")).get(1).getText()).toBe('No Segment selection for the Email: '+loginTestData[21].username);
        });

        //29.VERIFYING WHETHER OTP IS GENERATED OR NOT USING MOBILE NUMBER USERFORM[REGISTRATION] STEP-1 TO STEP-2 //
        it('should get the otp for entered mobile number', function() {
            page.signup(loginTestData[22]);
            page.form.submit.click();
            browser.sleep(4000);
            var text1 = browser.findElement(by.binding("success")).getText().then(function (ss1) {

                return ss1.split(". ");
            });
            text1.then(function (slices) {

                var otp2 = slices[1];
                expect(browser.findElement(by.binding("success")).getText()).
                toBe('An OTP has been sent to Phone :'+loginTestData[22].username+'. '+otp2);
            });
        });

        //30.TEST CASE FOR WRONG INPUT OF OTP USING MOBILE NUMBER USERFORM[REGISTRATION] STEP-2 //
        it('should display error message for incorrect otp using mobile number', function() {
            page.signup(loginTestData[23]);
            page.form.submit.click();
            browser.sleep(4000);
            expect(browser.getCurrentUrl()).toEqual('http://localhost:3000/#!/register');
            page.form.otp.sendKeys('123456');
            page.form.submit.click();
            expect(browser.findElement(by.binding("registererror")).getText())
                .toBe('Incorrect otp for the Mobile: ' + loginTestData[23].username);
        });

        //31.TEST CASE FOR CLICKING RESEND OTP USING MOBILE NUMBER USERFORM[REGISTRATION] STEP-2 //
        it('should resend the otp for given mobile number', function() {
            page.signup(loginTestData[24]);
            page.form.submit.click();
            browser.sleep(4000);
            var text1 = browser.findElement(by.binding("success")).getText().then(function (ss1) {

                return ss1.split(". ");
            });
            text1.then(function (slices) {

                var otp2 = slices[1];
                expect(browser.findElement(by.binding("success")).getText()).
                toBe('An OTP has been sent to Phone :'+loginTestData[24].username+'. '+otp2);
            });
            browser.findElement(by.linkText("Resend OTP")).click();
            expect(browser.getCurrentUrl()).toEqual('http://localhost:3000/#!/register');
            var text1 = browser.findElement(by.binding("success")).getText().then(function (ss1) {

                return ss1.split(". ");
            });
            text1.then(function (slices) {

                var otp2 = slices[1];
                expect(browser.findElement(by.binding("success")).getText()).
                toBe('An OTP has been sent to Phone :'+loginTestData[24].username+'. '+otp2);
            });
        });

         //32.TEST CASE FOR SUCCESSFULL VERIFICATION OF NEW USER OTP USING MOBILE NUMBER USERFORM[REGISTRATION] STEP-2 to step-3 //
        it('should proceed to next page after verifying mobile number and otp', function() {
            page.signup(loginTestData[24]);
            page.form.submit.click();
            browser.sleep(4000);
            var text1 = browser.findElement(by.binding("success")).getText().then(function (ss1) {

                return ss1.split(". ");
            });
            text1.then(function (slices) {

                var otp2 = slices[1];
                page.form.otp.sendKeys(otp2);
            });
            page.form.submit.click();
            browser.sleep(4000);
            expect(browser.getCurrentUrl()).toEqual('http://localhost:3000/#!/register');
            expect(browser.findElement(by.cssContainingText('.nvc-signup-tab2 h2.nvc-primary-color', 'start Trading')).getText()).toBe('1. You\'d like to start Trading as');
        });

        //33.TEST CASE FOR REGISTRATION WITH BUSINESS CATEGORY MANUFACTURER USING MOBILE NUMBER USERFORM[REGISTRATION] STEP-1 TO STEP-4 //
        it('should signup a new userusing mobile number with business category', function() {
            page.signup(loginTestData[26]);
            page.form.submit.click();
            browser.sleep(4000);
            var text1 = browser.findElement(by.binding("success")).getText().then(function (ss1) {

                return ss1.split(". ");
            });
            text1.then(function (slices) {

                var otp2 = slices[1];
                page.form.otp.sendKeys(otp2);
            });
            page.form.submit.click();
            browser.sleep(4000);
            expect(browser.getCurrentUrl()).toEqual('http://localhost:3000/#!/register');
            expect(browser.findElement(by.cssContainingText('.nvc-signup-tab2 h2.nvc-primary-color', 'start Trading')).getText()).toBe('1. You\'d like to start Trading as');
            browser.findElement(by.css('[aria-label~=Manufacturer]')).click();
            page.form.companyName.sendKeys('nVipani16');
            browser.findElement(by.css('[ng-show=rcategory]')).click();
            browser.sleep(3000);
            expect(browser.findElement(by.css(".nvc-signup-tab3 h2.nvc-primary-color")).getText()).toBe('2. Choose your Trading Segments');
            var rice = browser.driver.findElement(by.css("md-radio-button[aria-label=Rice]"));
            rice.click();
            expect(rice.getText('aria-label')).toBe("Rice");
            browser.findElement(by.partialButtonText('Start')).click();
            browser.sleep(5000);
            browser.findElement(by.css('[aria-label~=menu]')).click();
            browser.findElement(by.css('[aria-label=Sign-out]')).click();
        });

        //34.TEST CASE FOR REGISTRATION WITH BUSINESS CATEGORY DISTRIBUTOR USING MOBILE NUMBER USERFORM[REGISTRATION]STEP-1 TO STEP-4
        it('should signup a new user using mobile number with business categry distributor', function() {
            page.signup(loginTestData[27]);
            page.form.submit.click();
            browser.sleep(4000);
            var text1 = browser.findElement(by.binding("success")).getText().then(function (ss1) {

                return ss1.split(". ");
            });
            text1.then(function (slices) {

                var otp2 = slices[1];
                page.form.otp.sendKeys(otp2);
            });
            page.form.submit.click();
            browser.sleep(4000);
            expect(browser.getCurrentUrl()).toEqual('http://localhost:3000/#!/register');
            expect(browser.findElement(by.cssContainingText('.nvc-signup-tab2 h2.nvc-primary-color', 'start Trading')).getText()).toBe('1. You\'d like to start Trading as');
            browser.findElement(by.css('[aria-label~=Distributor]')).click();
            page.form.companyName.sendKeys('nVipani17');
            browser.findElement(by.css('[ng-show=rcategory]')).click();
            browser.sleep(3000);
            expect(browser.findElement(by.css(".nvc-signup-tab3 h2.nvc-primary-color")).getText()).toBe('2. Choose your Trading Segments');
            browser.findElement(by.css('md-checkbox[aria-label=Rice]')).click();
            browser.findElement(by.css('md-checkbox[aria-label=Coffee]')).click();
            browser.findElement(by.partialButtonText('Start')).click();
            browser.sleep(5000);
            browser.findElement(by.css('[aria-label~=menu]')).click();
            browser.findElement(by.css('[aria-label=Sign-out]')).click();
        });

        //35.REGISTRATION WITH BUSINESS CATEGORY RETAILER USING MOBILE NUMBER USERFORM[REGISTRATION] STEP-1 TO STEP-4
        it('should register new user with business category retailer using mobile number', function() {
            page.signup(loginTestData[28]);
            page.form.submit.click();
            browser.sleep(4000);
            var text1 = browser.findElement(by.binding("success")).getText().then(function (ss1) {

                return ss1.split(". ");
            });
            text1.then(function (slices) {

                var otp2 = slices[1];
                page.form.otp.sendKeys(otp2);
            });
            page.form.submit.click();
            browser.sleep(4000);
            expect(browser.getCurrentUrl()).toEqual('http://localhost:3000/#!/register');
            expect(browser.findElement(by.cssContainingText('.nvc-signup-tab2 h2.nvc-primary-color', 'start Trading')).getText()).toBe('1. You\'d like to start Trading as');
            browser.findElement(by.css('md-radio-button[aria-label~=Retailer]')).click();
            page.form.companyName.sendKeys('nVipani18');
            browser.findElement(by.css('[ng-show=rcategory]')).click();
            browser.sleep(3000);
            expect(browser.findElement(by.css(".nvc-signup-tab3 h2.nvc-primary-color")).getText()).toBe('2. Choose your Trading Segments');
            browser.findElement(by.css('md-checkbox[aria-label=Rice]')).click();
            browser.findElement(by.partialButtonText('Start')).click();
            browser.sleep(5000);
            browser.findElement(by.css('[aria-label~=menu]')).click();
            browser.findElement(by.css('[aria-label=Sign-out]')).click();
        });

        //36.TEST CASE TO CHECK IMAGES OF THE BUSINESS SEGMENTS IN THE MANUFACTURER USERFORM[REGISTRATION] STEP-4 //
        it('should check for the images in manufacturer segments', function () {
            page.signup(loginTestData[29]);
            page.form.submit.click();
            browser.sleep(4000);
            var text1 = browser.findElement(by.binding("success")).getText().then(function (ss1) {

                return ss1.split(". ");
            });
            text1.then(function (slices) {

                var otp2 = slices[1];
                page.form.otp.sendKeys(otp2);
            });
            page.form.submit.click();
            browser.sleep(2000);
            expect(browser.getCurrentUrl()).toEqual('http://localhost:3000/#!/register');
            expect(browser.findElement(by.cssContainingText('.nvc-signup-tab2 h2.nvc-primary-color', 'start Trading')).getText()).toBe('1. You\'d like to start Trading as');
            browser.findElement(by.css('[aria-label~=Manufacturer]')).click();
            page.form.companyName.sendKeys('nVipani19');
            browser.findElement(by.css('[ng-show=rcategory]')).click();
            browser.sleep(2000)
            expect(browser.findElement(by.css(".nvc-signup-tab3 h2.nvc-primary-color")).getText()).toBe('2. Choose your Trading Segments');
            expect(element(by.css("img[src='modules/categories/img/segments/rice.jpg']")).isPresent()).toBe(true);
            expect(element(by.css("img[src='modules/categories/img/segments/coffee.jpg']")).isPresent()).toBe(true);
        });

        //37.TEST CASE FOR CHECKING THE LABELS AND IMAGES OF THE BUSINESS SEGMENTS OF DISTRIBUTOR USERFORM[REGISTRATION] STEP-3 //
        it('should labels and images for business segments of distributor', function () {
            page.signup(loginTestData[30]);
            page.form.submit.click();
            browser.sleep(4000);
            var text1 = browser.findElement(by.binding("success")).getText().then(function (ss1) {

                return ss1.split(". ");
            });
            text1.then(function (slices) {

                var otp2 = slices[1];
                page.form.otp.sendKeys(otp2);
            });
            page.form.submit.click();
            browser.sleep(4000);
            expect(browser.getCurrentUrl()).toEqual('http://localhost:3000/#!/register');
            expect(browser.findElement(by.cssContainingText('.nvc-signup-tab2 h2.nvc-primary-color', 'start Trading')).getText()).toBe('1. You\'d like to start Trading as');
            browser.findElement(by.css('[aria-label~=Distributor]')).click();
            page.form.companyName.sendKeys('nVipani20');
            browser.findElement(by.css('[ng-show=rcategory]')).click();
            browser.sleep(2000);
            expect(browser.findElement(by.css(".nvc-signup-tab3 h2.nvc-primary-color")).getText()).toBe('2. Choose your Trading Segments');
            //Check images for business segments of distributor
            expect(element(by.css("img[src='modules/categories/img/segments/rice.jpg']")).isPresent()).toBe(true);
            expect(element(by.css("img[src='modules/categories/img/segments/coffee.jpg']")).isPresent()).toBe(true);
            //Check Title for business categories
            expect(element(by.css("md-checkbox[aria-label=Rice]")).getText('aria-label')).toEqual('Rice');
            expect(element(by.css("md-checkbox[aria-label=Coffee]")).getText('aria-label')).toEqual('Coffee');
        });

        //38.TEST CASE FOR CHECKING EXPLORE MORE WILL DISPLAY ALL THE BUSINESS SEGMENTS OF DISTRIBUTOR OR NOT USERFORM[REGISTRATION] STEP-3
        it('should check for all business segments in explore more', function () {
            page.signup(loginTestData[31]);
            page.form.submit.click();
            browser.sleep(4000);
            var text1 = browser.findElement(by.binding("success")).getText().then(function (ss1) {

                return ss1.split(". ");
            });
            text1.then(function (slices) {

                var otp2 = slices[1];
                page.form.otp.sendKeys(otp2);
            });
            page.form.submit.click();
            browser.sleep(4000);
            expect(browser.getCurrentUrl()).toEqual('http://localhost:3000/#!/register');
            expect(browser.findElement(by.cssContainingText('.nvc-signup-tab2 h2.nvc-primary-color', 'start Trading')).getText()).toBe('1. You\'d like to start Trading as');
            browser.findElement(by.css('[aria-label~=Distributor]')).click();
            page.form.companyName.sendKeys('nVipani20');
            browser.findElement(by.css('[ng-show=rcategory]')).click();
            browser.sleep(2000);
            expect(browser.findElement(by.css(".nvc-signup-tab3 h2.nvc-primary-color")).getText()).toBe('2. Choose your Trading Segments');
            browser.findElement(by.css('[aria-label=Switch1]')).click();
            expect(browser.findElement(by.css("md-checkbox[aria-label=Cereals]")).getText('aria-label')).toBe('Cereals');
            expect(element.all(by.css("md-checkbox[aria-label=Coffee]")).get(1).getText('aria-label')).toBe('Coffee');

        });

        //39.TEST CASE FOR CHECKING SHOW MORE IN EXPLORE MORE PAGE WILL DISPLAY ALL THE BUSINESS SEGMENTS OF CEREALS OF DISTRIBUTOR OR NOT USERFORM[REGISTRATION] STEP-3
        it('should check for the more in show details page for ceareals in distributor business category', function () {
            page.signup(loginTestData[32]);
            page.form.submit.click();
            browser.sleep(4000);
            var text1 = browser.findElement(by.binding("success")).getText().then(function (ss1) {

                return ss1.split(". ");
            });
            text1.then(function (slices) {

                var otp2 = slices[1];
                page.form.otp.sendKeys(otp2);
            });
            page.form.submit.click();
            browser.sleep(4000);
            expect(browser.getCurrentUrl()).toEqual('http://localhost:3000/#!/register');
            expect(browser.findElement(by.cssContainingText('.nvc-signup-tab2 h2.nvc-primary-color', 'start Trading')).getText()).toBe('1. You\'d like to start Trading as');
            browser.findElement(by.css('[aria-label~=Distributor]')).click();
            page.form.companyName.sendKeys('nVipani20');
            browser.findElement(by.css('[ng-show=rcategory]')).click();
            browser.sleep(2000);
            expect(browser.findElement(by.css(".nvc-signup-tab3 h2.nvc-primary-color")).getText()).toBe('2. Choose your Trading Segments');
            browser.findElement(by.css('[aria-label=Switch1]')).click();
            element.all(by.cssContainingText('label', 'Show Details')).get(0).click();
            expect(element(by.css("md-checkbox[aria-label~=Sona]")).
            getText('aria-label')).toBe('Sona Masoori Rice');
        });

        //40.TEST CASE FOR CHECKING SHOW MORE IN EXPLORE MORE PAGE WILL DISPLAY ALL THE BUSINESS SEGMENTS OF COFFEE OF DISTRIBUTOR OR NOT USERFORM[REGISTRATION] STEP-3
        it('should display the all business segments in coffee for distributor', function () {
            page.signup(loginTestData[33]);
            page.form.submit.click();
            browser.sleep(4000);
            var text1 = browser.findElement(by.binding("success")).getText().then(function (ss1) {

                return ss1.split(". ");
            });
            text1.then(function (slices) {

                var otp2 = slices[1];
                page.form.otp.sendKeys(otp2);
            });
            page.form.submit.click();
            browser.sleep(4000);
            expect(browser.getCurrentUrl()).toEqual('http://localhost:3000/#!/register');
            expect(browser.findElement(by.cssContainingText('.nvc-signup-tab2 h2.nvc-primary-color', 'start Trading')).getText()).toBe('1. You\'d like to start Trading as');
            browser.findElement(by.css('[aria-label~=Distributor]')).click();
            page.form.companyName.sendKeys('nVipani20');
            browser.findElement(by.css('[ng-show=rcategory]')).click();
            browser.sleep(2000);
            expect(browser.findElement(by.css(".nvc-signup-tab3 h2.nvc-primary-color")).getText()).toBe('2. Choose your Trading Segments');
            browser.findElement(by.css('[aria-label=Switch1]')).click();
            element.all(by.cssContainingText('label', 'Show Details')).get(1).click();
            expect(browser.findElement(by.css("md-checkbox[aria-label~=Berries]")).
            getText('aria-label')).toBe('Coffee Berries');
        });

        //41.TEST CASE FOR CHECKING LABEL AND IMAGES SHOW MORE IN EXPLORE MORE PAGE WILL DISPLAY ALL THE BUSINESS SEGMENTS OF CEREALS OF DISTRIBUTOR OR NOT USERFORM[REGISTRATION] STEP-3
        it('should check for images in explore more page for business segments of cereals', function () {
            page.signup(loginTestData[42]);
            page.form.submit.click();
            browser.sleep(4000);
            var text1 = browser.findElement(by.binding("success")).getText().then(function (ss1) {

                return ss1.split(". ");
            });
            text1.then(function (slices) {

                var otp2 = slices[1];
                page.form.otp.sendKeys(otp2);
            });
            page.form.submit.click();
            browser.sleep(4000);
            expect(browser.getCurrentUrl()).toEqual('http://localhost:3000/#!/register');
            expect(browser.findElement(by.cssContainingText('.nvc-signup-tab2 h2.nvc-primary-color', 'start Trading')).getText()).toBe('1. You\'d like to start Trading as');
            browser.findElement(by.css('[aria-label~=Distributor]')).click();
            page.form.companyName.sendKeys('nVipani20');
            browser.findElement(by.css('[ng-show=rcategory]')).click();
            browser.sleep(2000);
            expect(browser.findElement(by.css(".nvc-signup-tab3 h2.nvc-primary-color")).getText()).toBe('2. Choose your Trading Segments');
            browser.findElement(by.css('[aria-label=Switch1]')).click();
            element.all(by.cssContainingText('label', 'Show Details')).get(0).click();
            expect(browser.findElement(by.css("md-checkbox[aria-label~=Sona]")).getText('aria-label')).toBe('Sona Masoori Rice');
            expect(element(by.css("img[src='modules/categories/img/subcategory2/rice.png']")).isPresent()).toBe(true);

        });

        //42.TEST CASE FOR CHECKING LABEL AND IMAGES SHOW MORE IN EXPLORE MORE PAGE WILL DISPLAY ALL THE BUSINESS SEGMENTS OF COFFEE OF DISTRIBUTOR OR NOT USERFORM[REGISTRATION] STEP-3
        it('should checking labels and images in show more page for Distributor registration categroy', function () {
            page.signup(loginTestData[41]);
            page.form.submit.click();
            browser.sleep(4000);
            var text1 = browser.findElement(by.binding("success")).getText().then(function (ss1) {

                return ss1.split(". ");
            });
            text1.then(function (slices) {

                var otp2 = slices[1];
                page.form.otp.sendKeys(otp2);
            });
            page.form.submit.click();
            browser.sleep(4000);
            expect(browser.getCurrentUrl()).toEqual('http://localhost:3000/#!/register');
            expect(browser.findElement(by.cssContainingText('.nvc-signup-tab2 h2.nvc-primary-color', 'start Trading')).getText()).toBe('1. You\'d like to start Trading as');
            browser.findElement(by.css('[aria-label~=Distributor]')).click();
            page.form.companyName.sendKeys('nVipani20');
            browser.findElement(by.css('[ng-show=rcategory]')).click();
            browser.sleep(2000);
            expect(browser.findElement(by.css(".nvc-signup-tab3 h2.nvc-primary-color")).getText()).toBe('2. Choose your Trading Segments');
            browser.findElement(by.css('[aria-label=Switch1]')).click();
            browser.findElement(by.css('[aria-label=Coffee] + label')).click();
            expect(browser.findElement(by.css("[aria-label='Coffee Berries']")).getText('aria-label')).toBe('Coffee Berries');
            expect(element(by.css('[aria-label="Coffee Berries"] img[@src="modules/categories/img/subcategory2/coffeeberries.png"]')).isPresent()).toBe(true);

        });

        //43.TEST CASE FOR CHECKING THE LABELS AND IMAGES OF THE BUSINESS SEGMENTS OF RETAILER USERFORM[REGISTRATION] STEP-3
        it('should checking labels and images in show more page for Retailer registration categroy', function () {
            page.signup(loginTestData[34]);
            page.form.submit.click();
            browser.sleep(4000);
            var text1 = browser.findElement(by.binding("success")).getText().then(function (ss1) {

                return ss1.split(". ");
            });
            text1.then(function (slices) {

                var otp2 = slices[1];
                page.form.otp.sendKeys(otp2);
            });
            page.form.submit.click();
            browser.sleep(4000);
            expect(browser.getCurrentUrl()).toEqual('http://localhost:3000/#!/register');
            expect(browser.findElement(by.cssContainingText('.nvc-signup-tab2 h2.nvc-primary-color', 'start Trading')).getText()).toBe('1. You\'d like to start Trading as');
            browser.findElement(by.css('[aria-label=Retailer]')).click();
            page.form.companyName.sendKeys('nVipani20');
            browser.findElement(by.css('[ng-show=rcategory]')).click();
            browser.sleep(2000);
            expect(browser.findElement(by.css(".nvc-signup-tab3 h2.nvc-primary-color")).getText()).toBe('2. Choose your Trading Segments');
            //Check images for business segments of distributor
            expect(element(by.css("[aria-label=Cereals] img[@src='modules/categories/img/subcategory1/cereals.png']")).isPresent()).toBe(true);
            expect(element(by.css("[aria-label=Coffee] img[@src='modules/categories/img/subcategory1/coffee.png']")).isPresent()).toBe(true);
        });

        //44.TEST CASE FOR CHECKING SHOW MORE WILL DISPLAY ALL THE BUSINESS SEGMENTS OF CEREALS OF RETAILER OR NOT USERFORM[REGISTRATION] STEP-3
        it('should checking labels and images in show more page for retailer registration categroy and cereals', function () {
            page.signup(loginTestData[36]);
            page.form.submit.click();
            browser.sleep(4000);
            var text1 = browser.findElement(by.binding("success")).getText().then(function (ss1) {

                return ss1.split(". ");
            });
            text1.then(function (slices) {

                var otp2 = slices[1];
                page.form.otp.sendKeys(otp2);
            });
            page.form.submit.click();
            browser.sleep(4000);
            expect(browser.getCurrentUrl()).toEqual('http://localhost:3000/#!/register');
            expect(browser.findElement(by.cssContainingText('.nvc-signup-tab2 h2.nvc-primary-color', 'start Trading')).getText()).toBe('1. You\'d like to start Trading as');
            browser.findElement(by.css('[aria-label=Retailer]')).click();
            page.form.companyName.sendKeys('nVipani20');
            browser.findElement(by.css('[ng-show=rcategory]')).click();
            browser.sleep(2000);
            expect(browser.findElement(by.css(".nvc-signup-tab3 h2.nvc-primary-color")).getText()).toBe('2. Choose your Trading Segments');
            browser.findElement(by.cssContainingText('label', 'Show Details')).click();
            expect(browser.findElement(by.css('[aria-label="Sona Masoori Rice"]')).
            getText('aria-label')).toBe('Sona Masoori Rice');
        });

        //45.TEST CASE FOR CHECKING SHOW MORE WILL DISPLAY ALL THE BUSINESS SEGMENTS OF COFFEE OF RETAILER OR NOT USERFORM[REGISTRATION] STEP-3
        it('should show more page for retailer registration categroy', function () {
            page.signup(loginTestData[37]);
            page.form.submit.click();
            browser.sleep(4000);
            var text1 = browser.findElement(by.binding("success")).getText().then(function (ss1) {

                return ss1.split(". ");
            });
            text1.then(function (slices) {

                var otp2 = slices[1];
                page.form.otp.sendKeys(otp2);
            });
            page.form.submit.click();
            browser.sleep(4000);
            expect(browser.getCurrentUrl()).toEqual('http://localhost:3000/#!/register');
            expect(browser.findElement(by.cssContainingText('.nvc-signup-tab2 h2.nvc-primary-color', 'start Trading')).getText()).toBe('1. You\'d like to start Trading as');
            browser.findElement(by.css('[aria-label=Retailer]')).click();
            page.form.companyName.sendKeys('nVipani20');
            browser.findElement(by.css('[ng-show=rcategory]')).click();
            browser.sleep(2000);
            expect(browser.findElement(by.css(".nvc-signup-tab3 h2.nvc-primary-color")).getText()).toBe('2. Choose your Trading Segments');
            browser.findElement(by.css('[aria-label=Coffee] + lable')).click();
            expect(browser.findElement(by.css("[aria-label='Coffee Berries']")).
            getText('aria-label')).toBe('Coffee Berries');
        });

        //46.TEST CASE FOR CHECKING LABEL AND IMAGES SHOW MORE WILL DISPLAY ALL THE BUSINESS SEGMENTS OF CEREALS OF RETAILER OR NOT USERFORM[REGISTRATION] STEP-3
        it('should checking labels and images in show more page for retailer registration categroy', function () {
            page.signup(loginTestData[38]);
            page.form.submit.click();
            browser.sleep(4000);
            var text1 = browser.findElement(by.binding("success")).getText().then(function (ss1) {

                return ss1.split(". ");
            });
            text1.then(function (slices) {

                var otp2 = slices[1];
                page.form.otp.sendKeys(otp2);
            });
            page.form.submit.click();
            browser.sleep(4000);
            expect(browser.getCurrentUrl()).toEqual('http://localhost:3000/#!/register');
            expect(browser.findElement(by.cssContainingText('.nvc-signup-tab2 h2.nvc-primary-color', 'start Trading')).getText()).toBe('1. You\'d like to start Trading as');
            browser.findElement(by.css('[aria-label=Retailer]')).click();
            page.form.companyName.sendKeys('nVipani20');
            browser.findElement(by.css('[ng-show=rcategory]')).click();
            browser.sleep(2000);
            expect(browser.findElement(by.css(".nvc-signup-tab3 h2.nvc-primary-color")).getText()).toBe('2. Choose your Trading Segments');
            browser.findElement(by.cssContainingText('label', 'Show Details')).click();
            expect(browser.findElement(by.css("[aria-label='Sona Masoori Rice']")).getText('aria-label')).toBe('Sona Masoori Rice');
            expect(element(by.css("[aria-label='Sona Masoori Rice'] img[@src='modules/categories/img/subcategory2/rice.png']")).isPresent()).toBe(true);

        });
        /*
        //47.TEST CASE FOR CHECKING LABEL AND IMAGES SHOW MORE WILL DISPLAY ALL THE BUSINESS SEGMENTS OF COFFEE OF RETAILER OR NOT USERFORM[REGISTRATION] STEP-3
        it('should signup a new user, log them in, select business segment and redirecting to "/"', function () {
            page.signup(loginTestData[39]);
            page.form.submit.click();
            browser.sleep(4000);
            var text1 = browser.findElement(by.binding("success")).getText().then(function (ss1) {

                return ss1.split(". ");
            });
            text1.then(function (slices) {

                var otp2 = slices[1];
                page.form.otp.sendKeys(otp2);
            });
            page.form.submit.click();
            browser.sleep(4000);
            expect(browser.getCurrentUrl()).toEqual('http://localhost:3000/#!/register');
            expect(browser.findElement(by.cssContainingText('.nvc-signup-tab2 h2.nvc-primary-color', 'start Trading')).getText()).toBe('1. You\'d like to start Trading as');
            browser.findElement(by.css('[aria-label=Retailer]')).click();
            page.form.companyName.sendKeys('nVipani20');
            browser.findElement(by.css('[ng-show=rcategory]')).click();
            browser.sleep(2000);
            expect(browser.findElement(by.css(".nvc-signup-tab3 h2.nvc-primary-color")).getText()).toBe('2. Choose your Trading Segments');
            browser.findElement(by.xpath('//form/div[2]/div[2]/div/div[1]/select-segments/div/div[3]/nvc-tree/div/div/nvc-node[2]/div/label')).click();
            expect(browser.findElement(by.xpath("//form/div[2]/div[2]/div/div[1]/select-segments/div/div[3]/nvc-tree/div/" +
                "div[2]/nvc-tree/div/div/nvc-node/div/md-checkbox")).getText('aria-label')).toBe('Coffee Berries');
            expect(element(by.xpath("//form/div[2]/div[2]/div/div[1]/select-segments/div/div[3]/nvc-tree/div/" +
                "div[2]/nvc-tree/div/div/nvc-node/div/md-checkbox/div[2]/div/img[@src='modules/categories/img/subcategory2/coffeeberries.png']")).isPresent()).toBe(true);

        });*/

        //48.TEST CASE TO CHECK WITHOUT OTP VERIFICATION USING EMAIL ID
        it('should check without otp verification using email id', function () {
            page.signup(loginTestData[47]);
            page.form.submit.click();
            browser.sleep(4000);
            var text1 = browser.findElement(by.binding("success")).getText().then(function (ss1) {

                return ss1.split(". ");
            });
            text1.then(function (slices) {
                var otp2 = slices[1];
            });
            browser.findElement(by.linkText("Login")).click();
            browser.findElement(by.linkText("Sign-Up")).click();
            page.signup(loginTestData[47]);
            page.form.submit.click();
            expect(browser.findElement(by.xpath("//div[@data-ng-show='registererror']")).getText()).toBeFalsy('Not allowed to register with user name:'+loginTestData[47].username);

        });

        //49.TEST CASE TO CHECK WITHOUT OTP VERIFICATION USING MOBILE NUMBER
        it('should check without otp verification using phone', function () {
            page.signup(loginTestData[48]);
            page.form.submit.click();
            browser.sleep(4000);
            var text1 = browser.findElement(by.binding("success")).getText().then(function (ss1) {

                return ss1.split(". ");
            });
            text1.then(function (slices) {
                var otp2 = slices[1];
            });
            browser.findElement(by.linkText("Login")).click();
            browser.findElement(by.linkText("Sign-Up")).click();
            page.signup(loginTestData[48]);
            page.form.submit.click();
            expect(element(by.xpath("//div[@data-ng-show='registererror']")).getText()).toBeFalsy('Not allowed to register with user name:'+loginTestData[47].username);

        });

      //50.TEST CASE TO VALIDATE OTP FIELD WITH LESS DIGITS
        it('should display error for less otp digits', function() {
            page.signup(loginTestData[3]);
            page.form.submit.click();
            browser.sleep(4000);
            expect(browser.getCurrentUrl()).toEqual('http://localhost:3000/#!/register');
            page.form.otp.sendKeys('1234');
            page.form.submit.click();
            expect(element(by.css('md-dialog [aria-label="Ok"]')).isPresent()).toBe(true);
        });

        //51.TEST CASE TO VALIDATE OTP FIELD WITH START DIGIT IS 0
        it('should display error message for otp starting with 0', function() {
            page.signup(loginTestData[3]);
            page.form.submit.click();
            browser.sleep(4000);
            expect(browser.getCurrentUrl()).toEqual('http://localhost:3000/#!/register');
            page.form.otp.sendKeys('01234');
            page.form.submit.click();
            expect(element(by.css('md-dialog [aria-label="Ok"]')).isPresent()).toBe(true);
        });

        //52.TEST CASE TO VALIDATE OTP FIELD WITH TEXT
        it('should display error message for entering text in otp field', function() {
            page.signup(loginTestData[3]);
            page.form.submit.click();
            browser.sleep(4000);
            expect(browser.getCurrentUrl()).toEqual('http://localhost:3000/#!/register');
            page.form.otp.sendKeys('ASDF');
            page.form.submit.click();
            expect(element(by.css('md-dialog [aria-label="Ok"]')).isPresent()).toBe(true);
        });

    });
  });

