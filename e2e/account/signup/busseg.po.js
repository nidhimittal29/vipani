


'use strict';

var BusSignin = function() {
    var form = this.form = element(by.name('signup'));
    form.username = form.element(by.model('credentials.username'));
    form.password = form.element(by.model('credentials.password'));
    form.submit  = form.element(by.css('button'));
    this.bussignin = function(data) {
        for (var prop in data) {
            var formElem1 = form[prop];
            if (data.hasOwnProperty(prop) && formElem1 && typeof formElem1.sendKeys === 'function') {
                formElem1.sendKeys(data[prop]);
            }
        }
    };
};

module.exports = new BusSignin();

