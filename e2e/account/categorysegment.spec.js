

'use strict';

var config = browser.params;
//var UserModel = require(config.serverConfig.root + '/server/api/user/user.model').default;

describe('busseg View', function() {
    var page;
    var page1;

    var loadPage = function () {
        browser.manage().deleteAllCookies();
        browser.get('http://localhost:3000/#!/register');

    };
    var loadPage1 = function () {
        browser.manage().deleteAllCookies();
        browser.get('http://localhost:3000/#!/signin');
    };

    var testUser = {
        username: 'harrya0@gmail.com'
    };
    var testUser1 = {
        username: 'harryb0@gmail.com'
    };
    var testUser2 = {
        username: 'harryc@gmail.com'
    };
    var testUser3 = {
        username: 'harryd@gmail.com'
    };


    describe('with local auth', function() {
        beforeEach(function () {
            loadPage();

        });

        it('should signup a new user, log them in, select business segment and redirecting to "/"', function () {
            page = require('./signup/signup.po');
            page.signup(testUser);
            page.form.submit.click();
            browser.sleep(3000);
            var text = browser.findElement(by.xpath("//form[@name='userForm']/div/div[4]/div[1]/div")).getText().then(function (ss) {

                return ss.split(". ");
            });
            text.then(function (slices) {

                var otp1 = slices[1];
                page.form.otp.sendKeys(otp1);

            });
            page.form.submit.click();
            browser.sleep(3000);
            page.form.registrationCategory.click();
            browser.driver.findElement(by.xpath("//*[@id='RC_2']")).click();
            page.form.password.sendKeys(12345678);
            page.form.ConfirmPassword.sendKeys(12345678);
            page.form.submit.click();
            browser.sleep(3000);
            browser.driver.findElement(by.xpath("//nv-tree/div/nv-node[1]/div/div/span")).click();
            browser.driver.findElement(by.xpath("//nv-tree/div/nv-node[1]/div[2]/nv-tree/div/nv-node[1]/div/div/md-checkbox/div[1]")).click();
            browser.driver.findElement(by.xpath("//nv-tree/div/nv-node[1]/div/div/span")).click();
            browser.driver.findElement(by.xpath("//nv-tree/div/nv-node[2]/div/div/span")).click();
            browser.driver.findElement(by.xpath("//nv-tree/div/nv-node[2]/div[2]/nv-tree/div/nv-node[2]/div/div/md-checkbox/div[1]")).click();
            browser.driver.findElement(by.xpath("//nv-tree/div/nv-node[2]/div/div/span")).click();
            browser.driver.findElement(by.xpath("//nv-tree/div/nv-node[3]/div/div/span")).click();
            browser.driver.findElement(by.xpath("//nv-tree/div/nv-node[3]/div[2]/nv-tree/div/nv-node[3]/div/div/md-checkbox/div[1]")).click();
            browser.driver.findElement(by.xpath("//nv-tree/div/nv-node[3]/div/div/span")).click();
            browser.sleep(2000);
            browser.driver.findElement(by.xpath(".//button[@aria-label='Save']")).click();
            browser.sleep(2000);
            browser.driver.findElement(by.xpath("//*[@id='content']/div/div[1]/div/div[7]/button")).click();
            browser.sleep(1000);
            expect(browser.getCurrentUrl()).toEqual('http://localhost:3000/#!/');
            browser.get('http://localhost:3000/#!/signin');
            page1 = require('./signup/busseg.po');
            page1.bussignin(testUser);
            page1.form.password.sendKeys('12345678');
            page1.form.submit.click();
            browser.sleep(3000);
            browser.driver.findElement(by.xpath("//*[@class='nvc-category-select']")).then(function () {
                console.log('Dialog is popped');
            }, function (err) {
                console.info('Dialog is not present as expected');
            });
            browser.sleep(2000);
            browser.driver.findElement(by.xpath("//*[@id='content']/div/div[1]/div/div[7]/button")).click();
            browser.sleep(1000);
            expect(browser.getCurrentUrl()).toEqual('http://localhost:3000/#!/');
        });
        it('should signup a new user, log them in, select business segment and redirecting to "/"', function () {
            page = require('./signup/signup.po');
            page.signup(testUser1);
            page.form.submit.click();
            browser.sleep(3000);
            var text = browser.findElement(by.xpath("//form[@name='userForm']/div/div[4]/div[1]/div")).getText().then(function (ss) {

                return ss.split(". ");
            });
            text.then(function (slices) {

                var otp1 = slices[1];
                page.form.otp.sendKeys(otp1);

            });
            page.form.submit.click();
            browser.sleep(3000);
            page.form.registrationCategory.click();
            browser.driver.findElement(by.xpath("//*[@id='RC_2']")).click();
            page.form.password.sendKeys(12345678);
            page.form.ConfirmPassword.sendKeys(12345678);
            page.form.submit.click();
            browser.sleep(3000);
            browser.driver.findElement(by.xpath("//nv-tree/div/nv-node[1]/div/div/span")).click();
            browser.driver.findElement(by.xpath("//nv-tree/div/nv-node[1]/div[2]/nv-tree/div/nv-node[1]/div/div/md-checkbox/div[1]")).click();
            browser.driver.findElement(by.xpath("//nv-tree/div/nv-node[1]/div/div/span")).click();
            browser.driver.findElement(by.xpath("//nv-tree/div/nv-node[2]/div/div/span")).click();
            browser.driver.findElement(by.xpath("//nv-tree/div/nv-node[2]/div[2]/nv-tree/div/nv-node[2]/div/div/md-checkbox/div[1]")).click();
            browser.driver.findElement(by.xpath("//nv-tree/div/nv-node[2]/div/div/span")).click();
            browser.driver.findElement(by.xpath("//nv-tree/div/nv-node[3]/div/div/span")).click();
            browser.driver.findElement(by.xpath("//nv-tree/div/nv-node[3]/div[2]/nv-tree/div/nv-node[3]/div/div/md-checkbox/div[1]")).click();
            browser.driver.findElement(by.xpath("//nv-tree/div/nv-node[3]/div/div/span")).click();
            browser.sleep(2000);
            browser.driver.findElement(by.xpath(".//button[@aria-label='Cancel']")).click();
            browser.sleep(2000);
            browser.driver.findElement(by.xpath("//*[@id='content']/div/div[1]/div/div[7]/button")).click();
            browser.sleep(1000);
            expect(browser.getCurrentUrl()).toEqual('http://localhost:3000/#!/');
            browser.get('http://localhost:3000/#!/signin');
            page1 = require('./signup/busseg.po');
            page1.bussignin(testUser1);
            page1.form.password.sendKeys('12345678');
            page1.form.submit.click();
            browser.sleep(3000);
            browser.driver.findElement(by.xpath("//*[@class='nvc-category-select']")).then(function () {
                console.log('Dialog is popped');
            }, function (err) {
                console.info('Dialog is not present as expected');
            });
            browser.sleep(2000);
            browser.driver.findElement(by.xpath(".//button[@aria-label='Cancel']")).click();
            browser.driver.findElement(by.xpath("//*[@id='content']/div/div[1]/div/div[7]/button")).click();
            browser.sleep(2000);
            expect(browser.getCurrentUrl()).toEqual('http://localhost:3000/#!/');
        });
      
    });

});
