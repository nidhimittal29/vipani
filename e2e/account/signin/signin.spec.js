

'use strict';

var config = browser.params;

describe('Signin View', function() {
    var page,testData;
    var loadPage = function() {
        browser.manage().deleteAllCookies();
      browser.get('http://localhost:3000/#!/signin');

    };

    describe('with local auth', function() {

        beforeEach(function() {
            loadPage();
          page = require('./signin.po');
            testData=require('./../signup/signup.testdata');
        });

      //1.TEST CASE FOR MISSING BOTH THE CREDENTIALS
      it('should show error message for missing credentials ', function () {
          page.form.submit.click();
          expect(browser.findElement(by.css('input[placeholder="E-mail/Mobile"]')).getAttribute('required')).toBeDefined();
          expect(browser.getCurrentUrl()).toEqual('http://localhost:3000/#!/signin');
      });

      //2.TEST CASE FOR MISSING USERNAME
      it('should show error message for missing username', function() {
          page.signin(testData[43]);
          page.form.submit.click();
          expect(browser.findElement(by.css('input[placeholder="E-mail/Mobile"]')).getAttribute('required')).toBeDefined();
          expect(browser.getCurrentUrl()).toEqual('http://localhost:3000/#!/signin');
      });

      //3.TEST CASE FOR MISSING PASSWORD[MOBILE NUMBER]
      it('should show error message for missing password', function() {
          page.signin(testData[0]);
          page.form.submit.click();
          expect(browser.findElement(by.css('input[placeholder="Password"]')).getAttribute('required')).toBeDefined();
          expect(browser.getCurrentUrl()).toEqual('http://localhost:3000/#!/signin');
      });

      //4.TEST CASE FOR INVALID USERNAME[MOBILE NUMBER]
        it('should show error message for invalid username', function () {
            page = require('./signin.po');
            page.signin(testData[45]);
            expect(browser.findElement(by.xpath("//*/i[contains(@class,'pass-icon nv-red')]")).getAttribute('data')).toBe('Invalid Email/Mobile Format');
            expect(browser.getCurrentUrl()).toEqual('http://localhost:3000/#!/signin');
        });

        //5.TEST CASE FOR UNREGISTERED USER [MOBILE NUMBER]
      it('should validate authenticated user, and redirecting to "/"', function() {
          page = require('./signin.po');
          page.signin(testData[9]);
          page.form.submit.click();
          expect(browser.findElement(by.binding("error")).getText()).toBe('Unknown UserName "'+testData[9].username+'"');
      });

        //6.TEST CASE FOR UNREGISTERED USER [MOBILE NUMBER] CLICKING FORGOT PASSWORD
        it('should validate authenticated user, and redirecting to "/"', function() {
        page = require('./signin.po');
        page.form.username.sendKeys('5234567894');
        browser.findElement(by.linkText("Forgot Password")).click();
        page = require('./forpwd.po');
        page.form.submit.click();
        expect(browser.findElement(by.xpath("//form[@name='password']//*[@data-ng-show='error']")).getText()).toBe('No account with that username has been found');
    });


        //7.TEST CASE FOR INVALID PASSWORD
        it('should validate authenticated user, and redirecting to "/"', function() {
          page = require('./signin.po');
          page.form.username.sendKeys(testData[26].username);
          page.form.password.sendKeys("251545654");
          page.form.submit.click();
          expect(browser.findElement(by.xpath("//*[@data-ng-bind='error']")).getText()).toBe('Invalid password');

      });

        //8.TEST CASE FOR CHECKING TOGGLING OF EYE ICON IN PASSWORD FIELD
         it('should validate authenticated user, and redirecting to "/"', function() {
          page = require('./signin.po');
          page.signin(testData[4]);
          page.form.element(by.model('showpassword')).click();
          browser.sleep(2000);
          expect(browser.findElement(By.model("credentials.password")).getAttribute("type")).toBe("text");
      });

        //9.TEST CASE FOR CHECKING UNTOGGLING OF EYE ICON IN PASSWORD  FIELD
        it('should validate authenticated user, and redirecting to "/"', function() {
          page = require('./signin.po');
          page.signin(testData[4]);
          page.form.element(by.model('showpassword')).click();
          page.form.element(by.model('showpassword')).click();
          browser.sleep(2000);
          expect(browser.findElement(By.model("credentials.password")).getAttribute("type")).toBe("password");
      });


        //10.TEST CASE FOR SUCCESSFULL LOGIN
        it('should validate authenticated user, and redirecting to "/"', function() {
        page = require('./signin.po');
        page.signin(testData[7]);
        page.form.submit.click();
        browser.sleep(5000);
        browser.driver.findElement(by.xpath(".//button[@aria-label='Logout']")).click();
        browser.sleep(5000);
        expect(browser.getCurrentUrl()).toEqual('http://localhost:3000/#!/');
        });

        //11.TEST CASE FOR MISSING PASSWORD[EMAIL]
        it('should validate authenticated user, and redirecting to "/"', function() {
          page = require('./signin.po');
          page.signin(testData[0]);
            page.form.submit.click();
          expect(browser.findElement(by.css('input[placeholder="Password"]')).getAttribute('required')).toBeDefined();
          expect(browser.getCurrentUrl()).toEqual('http://localhost:3000/#!/signin');
        });

    //12.TEST CASE FOR UNREGISTERED USER [EMAIL]
        it('should validate authenticated user, and redirecting to "/"', function() {
          page = require('./signin.po');
          page.signin(testData[46]);
          page.form.submit.click();
          expect(browser.findElement(by.xpath("//*[@data-ng-bind='error']")).getText()).
          toBe('Unknown UserName "'+testData[46].username+'"');
      });

        //13.TEST CASE FOR INVALID USERNAME [EMAIL]
        it('should validate authenticated user, and redirecting to "/"', function() {
          page = require('./signin.po');
          page.signin(testData[44]);
          page.form.submit.click();
          expect(browser.findElement(by.xpath("//*/i[contains(@class,'pass-icon nv-red')]")).getAttribute('data')).toBe('Invalid Email/Mobile Format');
          expect(browser.getCurrentUrl()).toEqual('http://localhost:3000/#!/signin');
          });

        //14.TEST CASE FOR INVALID PASSWORD [EMAIL]
        it('should validate authenticated user, and redirecting to "/"', function() {
          page = require('./signin.po');
          page.form.username.sendKeys(testData[7].username);
          page.form.password.sendKeys("hfhgsjgf32");
          page.form.submit.click();
          expect(browser.findElement(by.xpath("//*[@data-ng-bind='error']")).getText()).toBe('Invalid password');
        });

        //15.TEST CASE FOR SUCCESSFULL LOGIN USING EMAIL
        it('should validate authenticated user, and redirecting to "/"', function() {
        page = require('./signin.po');
        page.signin(testData[7]);
        page.form.submit.click();
        browser.sleep(10000);
        browser.driver.findElement(by.xpath(".//button[@aria-label='Logout']")).click();
        browser.sleep(5000);
        expect(browser.getCurrentUrl()).toEqual('http://localhost:3000/#!/');
    });

        //16.TEST CASE FOR CLICKING NEW ACCOUNT REDIRECTING REGISTER PAGE
        it('should validate authenticated user, and redirecting to "/"', function() {
        page = require('./signin.po');
        browser.findElement(by.linkText("New Account")).click();
        expect(browser.getCurrentUrl()).toEqual('http://localhost:3000/#!/register');
    });

        //17.TEST CASE FOR FORGOT PASSWORD
        it('should validate authenticated user, and redirecting to "/"', function() {
          page = require('./signin.po');
          browser.findElement(by.linkText("Forgot Password")).click();
          page = require('./forpwd.po');
          expect(browser.getCurrentUrl()).toEqual('http://localhost:3000/#!/signin');
      });

        //18.TEST CASE FOR CHECKING THE AUTOMATIC FILL OF USERNAME
        it('should validate authenticated user, and redirecting to "/"', function() {
          page = require('./signin.po');
          page.signin(testData[7]);
          browser.findElement(by.linkText("Forgot Password")).click();
          page = require('./forpwd.po');
          var text = browser.findElement(By.model("passwordcredentials.username")).getAttribute("value");
          expect(text).toBe(testData[7].username);
      });

        //19.TEST CASE FOR WRONG INPUT OF USERNAME IN FORGOT PASSWORD
         it('should validate authenticated user, and redirecting to "/"', function() {
          browser.findElement(by.linkText("Forgot Password")).click();
          page = require('./forpwd.po');
          page.form.username1.sendKeys('568212');
          page.form.submit.click();
          browser.findElement(by.xpath("html/body/div[4]/md-dialog/md-dialog-actions/button")).click();
          expect(browser.getCurrentUrl()).toEqual('http://localhost:3000/#!/signin');
        });

        //20.TEST CASE FOR UNREGISTERED USERNAME IN FORGOT PASSWORD CASE
        it('should validate authenticated user, and redirecting to "/"', function() {
          page = require('./forpwd.po');
          browser.findElement(by.linkText("Forgot Password")).click();
          page = require('./forpwd.po');
          page.form.username1.sendKeys('5682147912');
          page.form.submit.click();
          expect(browser.findElement(by.xpath("//form[@name='password']//*[@data-ng-show='error']")).getText()).toBe('No account with that username has been found');

      });

        //21.TEST CASE FOR WRONG OTP INPUT IN FORGOT PASSWORD CASE
        it('should validate authenticated user, and redirecting to "/"', function() {
          browser.findElement(by.linkText("Forgot Password")).click();
          page = require('./forpwd.po');
          page.form.username1.sendKeys(testData[27].username);
          page.form.submit.click();
          page.form.existingOtp.sendKeys('569841');
          page.form.submit.click();
          expect(browser.findElement(by.xpath("//form[@name='password']//*[@data-ng-show='error']")).getText()).
          toBe('Verified OTP for the '  + testData[27].username +  ' is not correct');
      });

        //22.TEST CASE FOR MISSING OTP IN FORGOT PASSWORD
        it('should validate authenticated user, and redirecting to "/"', function() {
           browser.findElement(by.linkText("Forgot Password")).click();
           page = require('./forpwd.po');
           page.form.username1.sendKeys(testData[27].username);
           page.form.submit.click();
           var text = browser.findElement(by.xpath("//form[@name='password']//*[@data-ng-show='success']")).getText().then(function (ss) {

               return ss.split(". ");
           });
           text.then(function (slices) {
               var otp1 = slices[1];
               page.form.existingOtp.sendKeys(' ');
               page.form.submit.click();
               expect(browser.findElement(by.xpath("//form[@name='password']//*[@data-ng-show='success']")).isDisplayed()).
               toBe(true);
           });
       });

        //23.TEST CASE FOR SUCCESSFULL LOGIN INCASE OF FORGOT PASSWORD
        it('should validate authenticated user, and redirecting to "/"', function() {
          browser.findElement(by.linkText("Forgot Password")).click();
          page = require('./forpwd.po');
          page.form.username1.sendKeys(testData[27].username);
          page.form.submit.click();
          browser.sleep(3000);
          var text = browser.findElement(by.xpath("//form[@name='password']//*[@data-ng-show='success']")).getText().then(function (ss) {

              return ss.split(". ");
          });
         text.then(function (slices) {
             var text1 = slices[1].split(" ");
             var otp1 = text1[0];
             page.form.existingOtp.sendKeys(otp1);
         });

          page.form.submit.click();
          page.form.newPassword.sendKeys('123456789');
          page.form.verifyPassword.sendKeys('123456789');
          page.form.submit.click();
          browser.sleep(5000);
          browser.driver.findElement(by.xpath(".//button[@aria-label='Logout']")).click();
          browser.sleep(5000);
          expect(browser.getCurrentUrl()).toEqual('http://localhost:3000/#!/');

      });

        //24.TEST CASE FOR MISSING PASSWORD CREDENTIALS IN FORGOT PASSWORD
        it('should validate authenticated user, and redirecting to "/"', function() {
          browser.findElement(by.linkText("Forgot Password")).click();
          page = require('./forpwd.po');
          page.form.username1.sendKeys(testData[28].username);
          page.form.submit.click();
          var text = browser.findElement(by.xpath("//form[@name='password']//*[@data-ng-show='success']")).getText().then(function (ss) {

              return ss.split(". ");
          });
          text.then(function (slices) {
              var text1 = slices[1].split(" ");
              var otp1 = text1[0];
              page.form.existingOtp.sendKeys(otp1);
              page.form.submit.click();
              page.form.newPassword.sendKeys('12345678');
              page.form.verifyPassword.sendKeys(' ');
              page.form.submit.click();
              browser.findElement(by.xpath("html/body/div[4]/md-dialog/md-dialog-actions/button")).click();
              expect(browser.getCurrentUrl()).toEqual('http://localhost:3000/#!/signin');
          });
      });

        //25.TEST CASE FOR MISSING PASSWORD CREDENTIALS IN FORGOT PASSWORD
        it('should validate authenticated user, and redirecting to "/"', function() {
          browser.findElement(by.linkText("Forgot Password")).click();
          page = require('./forpwd.po');
          page.form.username1.sendKeys(testData[28].username);
          page.form.submit.click();
          var text = browser.findElement(by.xpath("//form[@name='password']//*[@data-ng-show='success']")).getText().then(function (ss) {

              return ss.split(". ");
          });
          text.then(function (slices) {
              var text1 = slices[1].split(" ");
              var otp1 = text1[0];
              page.form.existingOtp.sendKeys(otp1);
              page.form.submit.click();
              page.form.newPassword.sendKeys(' ');
              page.form.verifyPassword.sendKeys('12345678');
              page.form.submit.click();
              browser.findElement(by.xpath("html/body/div[4]/md-dialog/md-dialog-actions/button")).click();
              expect(browser.getCurrentUrl()).toEqual('http://localhost:3000/#!/signin');
          });
      });

        //26.TEST CASE FOR MISSING PASSWORD CREDENTIALS IN FORGOT PASSWORD
        it('should validate authenticated user, and redirecting to "/"', function() {
          browser.findElement(by.linkText("Forgot Password")).click();
          page = require('./forpwd.po');
          page.form.username1.sendKeys(testData[28].username);
          page.form.submit.click();
          var text = browser.findElement(by.xpath("//form[@name='password']//*[@data-ng-show='success']")).getText().then(function (ss) {

              return ss.split(". ");
          });
          text.then(function (slices) {
              var text1 = slices[1].split(" ");
              var otp1 = text1[0];
              page.form.existingOtp.sendKeys(otp1);
              page.form.submit.click();
              page.form.newPassword.sendKeys(' ');
              page.form.verifyPassword.sendKeys(' ');
              page.form.submit.click();
              browser.findElement(by.xpath("html/body/div[4]/md-dialog/md-dialog-actions/button")).click();
              expect(browser.getCurrentUrl()).toEqual('http://localhost:3000/#!/signin');
          });
      });

        //27.TEST CASE FOR MISS MATCH OF PASSWORD FIELDS
        it('should validate authenticated user, and redirecting to "/"', function() {
          browser.findElement(by.linkText("Forgot Password")).click();
          page = require('./forpwd.po');
          page.form.username1.sendKeys(testData[28].username);
          page.form.submit.click();
          var text = browser.findElement(by.xpath("//form[@name='password']//*[@data-ng-show='success']")).getText().then(function (ss) {

              return ss.split(". ");
          });
          text.then(function (slices) {
              var text1 = slices[1].split(" ");
              var otp1 = text1[0];
              page.form.existingOtp.sendKeys(otp1);
              page.form.submit.click();
              page.form.newPassword.sendKeys('12345678');
              page.form.verifyPassword.sendKeys('12345698');
              page.form.submit.click();
              expect(browser.findElement(by.xpath("//form[@name='password']//*[@data-ng-show='error']")).getText()).toBe('Passwords do not match');
          });
      });

        //28.TEST CASE FOR WRONG INPUT OF USERNAME IN FORGOT PASSWORD [EMAIL]
        it('should validate authenticated user, and redirecting to "/"', function() {
          browser.findElement(by.linkText("Forgot Password")).click();
          page = require('./forpwd.po');
          page.form.username1.sendKeys('NVIPANI');
          page.form.submit.click();
          browser.findElement(by.xpath("html/body/div[4]/md-dialog/md-dialog-actions/button")).click();
          expect(browser.getCurrentUrl()).toEqual('http://localhost:3000/#!/signin');
      });

        //29.TEST CASE FOR UNREGISTERED USERNAME IN FORGOT PASSWORD CASE [EMAIL]
        it('should validate authenticated user, and redirecting to "/"', function() {
          browser.findElement(by.linkText("Forgot Password")).click();
          page = require('./forpwd.po');
          page.form.username1.sendKeys(testData[0].username);
          page.form.submit.click();
          expect(browser.findElement(by.xpath("//form[@name='password']//*[@data-ng-show='error']")).getText()).toBe('No account with that username has been found');
          });

        //30.TEST CASE TO CHECK HINTS FOR INPUT FIELDS
        it('should validate authenticated user, and redirecting to "/"', function() {
          page=require('./signin.po');
          expect(browser.findElement(by.xpath("//form[@name='signup']//md-input-container[1]/input[@aria-label='E-mail/Mobile']")).isDisplayed()).
          toBe(true);
          expect(browser.findElement(by.xpath("//form[@name='signup']//md-input-container[2]/input[@aria-label='Password']")).isDisplayed()).
          toBe(true);
        });
    });
});


