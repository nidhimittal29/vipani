

'use strict';



var Forpwd = function() {
    var form = this.form = element(by.name('password'));
    form.username1 = form.element(by.model('passwordcredentials.username'));
    form.existingOtp = form.element(by.model('passwordcredentials.forgotPasswordOtp'));
    form.newPassword = form.element(by.model('passwordcredentials.newPassword'));
    form.verifyPassword = form.element(by.model('passwordcredentials.verifyPassword'));
    form.submit = form.element(by.css('button'));
    this.forpwd = function(data){
    for (var prop in data) { 
        var formElem = form[prop];
        if (data.hasOwnProperty(prop) && formElem && typeof formElem.sendKeys === 'function') {
            formElem.sendKeys(data[prop]);
        }
    }
};
};



module.exports = new Forpwd();
