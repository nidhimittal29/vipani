
'use strict';

var config = browser.params;
//var UserModel = require(config.serverConfig.root + '/server/api/user/user.model').default;

describe('busseg View', function() {
    var page;
    var page1;

    var loadPage = function () {
        browser.manage().deleteAllCookies();
        browser.get('http://localhost:3000/#!/register');

    };
    var loadPage1 = function(){
      browser.manage().deleteAllCookies();
      browser.get('http://localhost:3000/#!/signin'); 
    };
    var testUser = {
        username: 'lilly001a@gmail.com'
    };
    var testUser1 = {
        username: 'lilly001b@gmail.com'
    };
    var testUser2 = {
        username: 'lilly001c@gmail.com'
    };
    var testUser3 = {
        username: 'lilly001d@gmail.com'
    };
    var testUser4 = {
        username: 'lilly001e@gmail.com'
    };
    var testUser5 = {
        username: 'lilly001f@gmail.com'
    };
    var testUser6 = {
        username: 'lilly001g@gmail.com'
    };
    var testUser7 = {
        username: 'lilly001h@gmail.com'
    };
    var testUser8 = {
        username: 'lilly001i@gmail.com'
    };
    var testUser9 = {
        username: 'lilly001j@gmail.com'
    };
    var testUser10 = {
        username: 'lilly001k@gmail.com'
    };
    var testUser11 = {
        username: 'lilly001l@gmail.com'
    };
    var testUser12 = {
        username: 'lilly001m@gmail.com'
    };
    var testUser13 = {
        username: 'lilly001m@gmail.com',
        password: '12345678'
    };
    var testUser14 = {
        username: 'lilly001n@gmail.com'
    };
    var testUser15 = {
        username: 'lilly001n@gmail.com',
        password: '12345678'
    };
    var testUser16 = {
        username: 'lilly001o@gmail.com'
    };
    var testUser17 = {
        username: 'lilly001o@gmail.com',
        password: '12345678'
    };
    var testUser18 = {
        username: 'lilly001p@gmail.com'
    };
    var testUser19 = {
        username: 'lilly001q@gmail.com'
    };
    var testUser20 = {
        username: 'lilly001r@gmail.com'
    };
    var testUser21 = {
        username: 'lilly001s@gmail.com'
    };
    var testUser22 = {
        username: 'lilly001t@gmail.com'
    };
    var testUser23 = {
        username: 'lilly001u@gmail.com'
    };
    var testUser24 = {
        username: 'lilly001v@gmail.com'
    };
    var testUser25 = {
        username: 'lilly001w@gmail.com'
    };
    var testUser26 = {
        username: 'lilly001x@gmail.com'
    };
    var testUser27 = {
        username: 'lilly001y@gmail.com'
    };

    describe('with local auth', function() {
        beforeEach(function () {
            loadPage();

        });
        //1.TEST CASE FOR CHECKING WHETHER DIALOG HAS POPPED UP FOR NEWLY REGISTERED USER
        it('should signup a new user, log them in, select business segment and redirecting to "/"', function () {
            page = require('./signup/signup.po');
            page.signup(testUser);
            page.form.submit.click();
            browser.sleep(3000);
            var text = browser.findElement(by.xpath("//form[@name='userForm']/div/div[4]/div[1]/div")).getText().then(function (ss) {

                return ss.split(". ");
            });
            text.then(function (slices) {

                var otp1 = slices[1];
                page.form.otp.sendKeys(otp1);

            });
            page.form.submit.click();
            browser.sleep(3000);
            page.form.registrationCategory.click();
            browser.driver.findElement(by.xpath("//*[@id='RC_0']")).click();
            page.form.password.sendKeys(12345678);
            page.form.ConfirmPassword.sendKeys(12345678);
            page.form.submit.click();
            browser.sleep(3000);
            browser.driver.findElement(by.xpath("//*[@class='nvc-category-select']")).then(function () {
                console.log('Dialog is popped');
            }, function (err) {
                console.info('Dialog is not present as expected');
            });
            var rice = browser.driver.findElement(by.xpath("//*[@id='seg_0']"));
            expect(rice.getText('aria-label')).toBe("Rice");
            rice.click();
            browser.driver.findElement(by.xpath("//button[@aria-label='Save']")).click();
            browser.sleep(1500);
            browser.driver.findElement(by.xpath("//*[@id='content']/div/div[1]/div/div[7]/button")).click();
            browser.sleep(1000);
            expect(browser.getCurrentUrl()).toEqual('http://localhost:3000/#!/');
        });

        //2.TEST CASE FOR CHECKING WHETHER HEADER ON THE DIALOG IS CORRECTLY PRESENT
        it('should signup a new user, log them in, select business segment  and redirecting to "/"', function () {
            page = require('./signup/signup.po');
            page.signup(testUser1);
            page.form.submit.click();
            browser.sleep(3000);
            var text = browser.findElement(by.xpath("//form[@name='userForm']/div/div[4]/div[1]/div")).getText().then(function (ss) {

                return ss.split(". ");
            });
            text.then(function (slices) {

                var otp1 = slices[1];
                page.form.otp.sendKeys(otp1);

            });
            page.form.submit.click();
            browser.sleep(3000);
            page.form.registrationCategory.click();
            browser.driver.findElement(by.xpath("//*[@id='RC_0']")).click();
            page.form.password.sendKeys(12345678);
            page.form.ConfirmPassword.sendKeys(12345678);
            page.form.submit.click();
            browser.sleep(3000);
            expect(browser.driver.findElement(by.xpath("//*[@id='header']")).getText()).toBe("Business Segments");
            browser.sleep(1000);
            browser.driver.findElement(by.xpath("//button[@aria-label='Cancel']")).click();
            browser.sleep(1500);
            browser.driver.findElement(by.xpath("//*[@id='content']/div/div[1]/div/div[7]/button")).click();
            browser.sleep(1000);
            expect(browser.getCurrentUrl()).toEqual('http://localhost:3000/#!/');
        });

        //3.TEST CASE FOR CHECKING WHETHER SEARCH BAR IS ABLED IN THE DIALOG
        it('should signup a new user, log them in, select business segment and redirecting to "/"', function () {
            page = require('./signup/signup.po');
            page.signup(testUser2);
            page.form.submit.click();
            browser.sleep(3000);
            var text = browser.findElement(by.xpath("//form[@name='userForm']/div/div[4]/div[1]/div")).getText().then(function (ss) {

                return ss.split(". ");
            });
            text.then(function (slices) {

                var otp1 = slices[1];
                page.form.otp.sendKeys(otp1);

            });
            page.form.submit.click();
            browser.sleep(3000);
            page.form.registrationCategory.click();
            browser.driver.findElement(by.xpath("//*[@id='RC_0']")).click();
            page.form.password.sendKeys(12345678);
            page.form.ConfirmPassword.sendKeys(12345678);
            page.form.submit.click();
            browser.sleep(3000);
            expect(browser.driver.findElement(by.xpath("//*[@aria-label='Business Segments ...']/div/div[1]/div")).isDisplayed()).toBe(true);
            browser.sleep(1000);
            browser.driver.findElement(by.xpath("//button[@aria-label='Cancel']")).click();
            browser.sleep(1500);
            browser.driver.findElement(by.xpath("//*[@id='content']/div/div[1]/div/div[7]/button")).click();
            browser.sleep(1000);
            expect(browser.getCurrentUrl()).toEqual('http://localhost:3000/#!/');
        });

        //4.TEST CASE FOR SELECTING RICE AS BUSINESS SEGMENT FOR MANUFACTURER
        it('should signup a new user, log them in, select business segment and redirecting to "/"', function () {
            page = require('./signup/signup.po');
            page.signup(testUser3);
            page.form.submit.click();
            browser.sleep(3000);
            var text = browser.findElement(by.xpath("//form[@name='userForm']/div/div[4]/div[1]/div")).getText().then(function (ss) {

                return ss.split(". ");
            });
            text.then(function (slices) {

                var otp1 = slices[1];
                page.form.otp.sendKeys(otp1);

            });
            page.form.submit.click();
            browser.sleep(3000);
            page.form.registrationCategory.click();
            browser.driver.findElement(by.xpath("//*[@id='RC_0']")).click();
            page.form.password.sendKeys(12345678);
            page.form.ConfirmPassword.sendKeys(12345678);
            page.form.submit.click();
            browser.sleep(3000);
            browser.driver.findElement(by.xpath("//*[@id='seg_0']")).click();
            browser.sleep(1000);
            browser.driver.findElement(by.xpath(".//button[@aria-label='Cancel']")).click();
            browser.sleep(1500);
            browser.driver.findElement(by.xpath("//*[@id='content']/div/div[1]/div/div[7]/button")).click();
            browser.sleep(1000);
            expect(browser.getCurrentUrl()).toEqual('http://localhost:3000/#!/');
        });

        //5.TEST CASE FOR CHECKING WHETHER RICE SEGMENT WILL STAY SELECTED AFTER SELECTING COFFEE SEGMENT FOR MANUFACTURER
        it('should signup a new user, log them in, select business segment and redirecting to "/"', function () {
            page = require('./signup/signup.po');
            page.signup(testUser4);
            page.form.submit.click();
            browser.sleep(3000);
            var text = browser.findElement(by.xpath("//form[@name='userForm']/div/div[4]/div[1]/div")).getText().then(function (ss) {

                return ss.split(". ");
            });
            text.then(function (slices) {

                var otp1 = slices[1];
                page.form.otp.sendKeys(otp1);

            });
            page.form.submit.click();
            browser.sleep(3000);
            page.form.registrationCategory.click();
            browser.driver.findElement(by.xpath("//*[@id='RC_0']")).click();
            page.form.password.sendKeys(12345678);
            page.form.ConfirmPassword.sendKeys(12345678);
            page.form.submit.click();
            browser.sleep(3000);
            var rice = browser.driver.findElement(by.xpath("//*[@id='seg_0']"));
            rice.click();
            var coffee = browser.driver.findElement(by.xpath("//*[@id='seg_1']"));
            coffee.click();
            expect(rice.getAttribute('aria-checked')).toBe('false');
            expect(coffee.getAttribute('aria-checked')).toBe('true');
            rice.click();
            expect(coffee.getAttribute('aria-checked')).toBe('false');
            browser.driver.findElement(by.xpath("//button[@aria-label='Save']")).click();
            browser.sleep(1500);
            browser.driver.findElement(by.xpath("//*[@id='content']/div/div[1]/div/div[7]/button")).click();
            browser.sleep(1000);
            expect(browser.getCurrentUrl()).toEqual('http://localhost:3000/#!/');
        });

        //6.TEST CASE FOR CHECKING THE LABELS OF THE BUSINESS SEGMENTS IN THE MANUFACTURER
        it('should signup a new user, log them in, select business segment and redirecting to "/"', function () {
            page = require('./signup/signup.po');
            page.signup(testUser5);
            page.form.submit.click();
            browser.sleep(3000);
            var text = browser.findElement(by.xpath("//form[@name='userForm']/div/div[4]/div[1]/div")).getText().then(function (ss) {

                return ss.split(". ");
            });
            text.then(function (slices) {

                var otp1 = slices[1];
                page.form.otp.sendKeys(otp1);

            });
            page.form.submit.click();
            browser.sleep(3000);
            page.form.registrationCategory.click();
            browser.driver.findElement(by.xpath("//*[@id='RC_0']")).click();
            page.form.password.sendKeys(12345678);
            page.form.ConfirmPassword.sendKeys(12345678);
            page.form.submit.click();
            browser.sleep(3000);
            var rice = browser.driver.findElement(by.xpath("//*[@id='seg_0']"));
            expect(rice.getText('aria-label')).toBe("Rice");
            var coffee = browser.driver.findElement(by.xpath("//*[@id='seg_1']"));
            expect(coffee.getText('aria-label')).toBe("Coffee");
            browser.driver.findElement(by.xpath(".//button[@aria-label='Cancel']")).click();
            browser.sleep(1500);
            browser.driver.findElement(by.xpath("//*[@id='content']/div/div[1]/div/div[7]/button")).click();
            browser.sleep(1000);
            expect(browser.getCurrentUrl()).toEqual('http://localhost:3000/#!/');
        });

        //7.TEST CASE FOR CHECKING WHETHER EXPLORE MORE BUTTON IS APPEARING WHEN MANUFACTURER WAS CHOSEN
        it('should signup a new user, log them in, select business segment and redirecting to "/"', function () {
            page = require('./signup/signup.po');
            page.signup(testUser6);
            page.form.submit.click();
            browser.sleep(3000);
            var text = browser.findElement(by.xpath("//form[@name='userForm']/div/div[4]/div[1]/div")).getText().then(function (ss) {

                return ss.split(". ");
            });
            text.then(function (slices) {

                var otp1 = slices[1];
                page.form.otp.sendKeys(otp1);

            });
            page.form.submit.click();
            browser.sleep(2000);
            page.form.registrationCategory.click();
            browser.driver.findElement(by.xpath("//*[@id='RC_0']")).click();
            page.form.password.sendKeys(12345678);
            page.form.ConfirmPassword.sendKeys(12345678);
            page.form.submit.click();
            browser.sleep(2000);
            var rice = browser.driver.findElement(by.xpath("//*[@id='seg_0']"));
            expect(rice.getText('aria-label')).toBe("Rice");
            browser.sleep(5000);
            var explore = browser.driver.findElement(by.xpath("//*[@aria-label='Explore More']"));
            expect(explore.getAttribute('aria-hidden')).toBe('true');
            browser.driver.findElement(by.xpath(".//button[@aria-label='Cancel']")).click();
            browser.sleep(1500);
            browser.driver.findElement(by.xpath("//*[@id='content']/div/div[1]/div/div[7]/button")).click();
            browser.sleep(1000);
            expect(browser.getCurrentUrl()).toEqual('http://localhost:3000/#!/');
        });

        //8.TEST CASE FOR SELECTING RICE AS BUSINESS SEGMENT FOR DISTRIBUTOR
        it('should signup a new user, log them in, select business segment and redirecting to "/"', function () {
            page = require('./signup/signup.po');
            page.signup(testUser7);
            page.form.submit.click();
            browser.sleep(3000);
            var text = browser.findElement(by.xpath("//form[@name='userForm']/div/div[4]/div[1]/div")).getText().then(function (ss) {

                return ss.split(". ");
            });
            text.then(function (slices) {

                var otp1 = slices[1];
                page.form.otp.sendKeys(otp1);

            });
            page.form.submit.click();
            browser.sleep(3000);
            page.form.registrationCategory.click();
            browser.driver.findElement(by.xpath("//*[@id='RC_1']")).click();
            page.form.password.sendKeys(12345678);
            page.form.ConfirmPassword.sendKeys(12345678);
            page.form.submit.click();
            browser.sleep(3000);
            browser.driver.findElement(by.xpath("//*[@id='segm_0']/md-checkbox/div[1]")).click();
            browser.driver.findElement(by.xpath(".//button[@aria-label='Save']")).click();
            browser.sleep(1500);
            browser.driver.findElement(by.xpath("//*[@id='content']/div/div[1]/div/div[7]/button")).click();
            browser.sleep(1000);
            expect(browser.getCurrentUrl()).toEqual('http://localhost:3000/#!/');
        });

        //9.TEST CASE FOR CHECKING WHETHER RICE SEGMENT WILL STAY SELECTED AFTER SELECTING COFFEE SEGMENT FOR DISTRIBUTOR
        it('should signup a new user, log them in, select business segment and redirecting to "/"', function () {
            page = require('./signup/signup.po');
            page.signup(testUser8);
            page.form.submit.click();
            browser.sleep(3000);
            var text = browser.findElement(by.xpath("//form[@name='userForm']/div/div[4]/div[1]/div")).getText().then(function (ss) {

                return ss.split(". ");
            });
            text.then(function (slices) {

                var otp1 = slices[1];
                page.form.otp.sendKeys(otp1);

            });
            page.form.submit.click();
            browser.sleep(3000);
            page.form.registrationCategory.click();
            browser.driver.findElement(by.xpath("//*[@id='RC_1']")).click();
            page.form.password.sendKeys(12345678);
            page.form.ConfirmPassword.sendKeys(12345678);
            page.form.submit.click();
            browser.sleep(3000);
            var rice = browser.driver.findElement(by.xpath("//*[@id='segm_0']/md-checkbox"));
            rice.click();
            var coffee = browser.driver.findElement(by.xpath("//*[@id='segm_1']/md-checkbox"));
            coffee.click();
            expect(rice.getAttribute('aria-checked')).toBe('true');
            rice.click();
            expect(rice.getAttribute('aria-checked')).toBe('false');
            browser.driver.findElement(by.xpath(".//button[@aria-label='Cancel']")).click();
            browser.sleep(1500);
            browser.driver.findElement(by.xpath("//*[@id='content']/div/div[1]/div/div[7]/button")).click();
            browser.sleep(1000);
            expect(browser.getCurrentUrl()).toEqual('http://localhost:3000/#!/');
        });

        //10.TEST CASE FOR CHECKING THE LABELS OF THE BUSINESS SEGMENTS IN THE DISTRIBUTOR
        it('should signup a new user, log them in, select business segment and redirecting to "/"', function () {
            page = require('./signup/signup.po');
            page.signup(testUser9);
            page.form.submit.click();
            browser.sleep(3000);
            var text = browser.findElement(by.xpath("//form[@name='userForm']/div/div[4]/div[1]/div")).getText().then(function (ss) {

                return ss.split(". ");
            });
            text.then(function (slices) {

                var otp1 = slices[1];
                page.form.otp.sendKeys(otp1);

            });
            page.form.submit.click();
            browser.sleep(3000);
            page.form.registrationCategory.click();
            browser.driver.findElement(by.xpath("//*[@id='RC_1']")).click();
            page.form.password.sendKeys(12345678);
            page.form.ConfirmPassword.sendKeys(12345678);
            page.form.submit.click();
            browser.sleep(3000);
            var rice = browser.driver.findElement(by.xpath("//*[@id='segm_0']"));
            expect(rice.getText('aria-label')).toBe("Rice");
            var coffee = browser.driver.findElement(by.xpath("//*[@id='segm_1']"));
            expect(coffee.getText('aria-label')).toBe("Coffee");
            browser.driver.findElement(by.xpath(".//button[@aria-label='Cancel']")).click();
            browser.sleep(1500);
            browser.driver.findElement(by.xpath("//*[@id='content']/div/div[1]/div/div[7]/button")).click();
            browser.sleep(1000);
            expect(browser.getCurrentUrl()).toEqual('http://localhost:3000/#!/');
        });

        //11.TEST CASE FOR CHECKING WHETHER EXPLORE MORE BUTTON IS APPEARING WHEN DISTRIBUTOR WAS CHOSEN
        it('should signup a new user, log them in, select business segment and redirecting to "/"', function () {
            page = require('./signup/signup.po');
            page.signup(testUser10);
            page.form.submit.click();
            browser.sleep(3000);
            var text = browser.findElement(by.xpath("//form[@name='userForm']/div/div[4]/div[1]/div")).getText().then(function (ss) {

                return ss.split(". ");
            });
            text.then(function (slices) {

                var otp1 = slices[1];
                page.form.otp.sendKeys(otp1);

            });
            page.form.submit.click();
            browser.sleep(3000);
            page.form.registrationCategory.click();
            browser.driver.findElement(by.xpath("//*[@id='RC_1']")).click();
            page.form.password.sendKeys(12345678);
            page.form.ConfirmPassword.sendKeys(12345678);
            page.form.submit.click();
            browser.sleep(3000);
            var explore = browser.driver.findElement(by.xpath("//*[@aria-label='Explore More']"));
            expect(explore.getAttribute('aria-hidden')).toBe('false');
            browser.driver.findElement(by.xpath(".//button[@aria-label='Cancel']")).click();
            browser.sleep(1500);
            browser.driver.findElement(by.xpath("//*[@id='content']/div/div[1]/div/div[7]/button")).click();
            browser.sleep(1000);
            expect(browser.getCurrentUrl()).toEqual('http://localhost:3000/#!/');
        });

        //12.TEST CASE FOR CHECKING WHETHER DIALOG FOR BUSINESS SEGMENT IS DISAPPEARING AFTER CLICKING EITHER OF SAVE OR CANCEL BUTTONS
        it('should signup a new user, log them in, select business segment and redirecting to "/"', function () {
            page = require('./signup/signup.po');
            page.signup(testUser11);
            page.form.submit.click();
            browser.sleep(3000);
            var text = browser.findElement(by.xpath("//form[@name='userForm']/div/div[4]/div[1]/div")).getText().then(function (ss) {

                return ss.split(". ");
            });
            text.then(function (slices) {

                var otp1 = slices[1];
                page.form.otp.sendKeys(otp1);

            });
            page.form.submit.click();
            browser.sleep(2000);
            page.form.registrationCategory.click();
            browser.driver.findElement(by.xpath("//*[@id='RC_0']")).click();
            page.form.password.sendKeys(12345678);
            page.form.ConfirmPassword.sendKeys(12345678);
            page.form.submit.click();
            browser.sleep(1500);
            browser.driver.findElement(by.xpath(".//button[@aria-label='Cancel']")).click();
            browser.sleep(3000);
            browser.driver.findElement(by.xpath("//*[@class='nvc-category-select']")).then(function () {
                console.log('Dialog is popped');
            }, function (err) {
                console.info('Dialog is not present as expected');
            });
            browser.sleep(2000);
            browser.driver.findElement(by.xpath("//*[@id='content']/div/div[1]/div/div[7]/button")).click();
            browser.sleep(1000);
            expect(browser.getCurrentUrl()).toEqual('http://localhost:3000/#!/');
        });

        //13.TEST CASE FOR CHECKING WHETHER BUSINESS SEGMENT IS POPPING UP FOR THE USER WHO HAD ALREADY SELECTED THE SEGMENT AS PART OF REGISTRATION
        it('should signup a new user, log them in, select business segment and redirecting to "/"', function () {
            page = require('./signup/signup.po');
            page.signup(testUser12);
            page.form.submit.click();
            browser.sleep(3000);
            var text = browser.findElement(by.xpath("//form[@name='userForm']/div/div[4]/div[1]/div")).getText().then(function (ss) {

                return ss.split(". ");
            });
            text.then(function (slices) {

                var otp1 = slices[1];
                page.form.otp.sendKeys(otp1);

            });
            page.form.submit.click();
            browser.sleep(3000);
            page.form.registrationCategory.click();
            browser.driver.findElement(by.xpath("//*[@id='RC_0']")).click();
            page.form.password.sendKeys(12345678);
            page.form.ConfirmPassword.sendKeys(12345678);
            page.form.submit.click();
            browser.sleep(3000);
            browser.driver.findElement(by.xpath("//*[@id='seg_1']")).click();
            browser.driver.findElement(by.xpath(".//button[@aria-label='Save']")).click();
            browser.sleep(1000);
            browser.driver.findElement(by.xpath("//*[@id='content']/div/div[1]/div/div[7]/button")).click();
            browser.sleep(1000);
            expect(browser.getCurrentUrl()).toEqual('http://localhost:3000/#!/');
            browser.get('http://localhost:3000/#!/signin');
            page1 = require('./signup/busseg.po');
            page1.bussignin(testUser13);
            page1.form.submit.click();
            browser.sleep(3000);
            browser.driver.findElement(by.xpath("//*[@class='nvc-category-select']")).then(function () {
                console.log('Dialog is popped');
            }, function (err) {
                console.info('Dialog is not present as expected');
            });
            browser.sleep(2000);
            browser.driver.findElement(by.xpath("//*[@id='content']/div/div[1]/div/div[7]/button")).click();
            browser.sleep(1000);
            expect(browser.getCurrentUrl()).toEqual('http://localhost:3000/#!/');
        });

        //14.TEST CASE FOR CHECKING WHETHER BUSINESS SEGMENT IS POPPING UP FOR THE USER WHO DID NOT SELECTED THE BUSINESS SEGMENT AS PART OF REGISTRATION[CLICKED CANCEL]
        it('should signup a new user, log them in, select business segment and redirecting to "/"', function () {
            page = require('./signup/signup.po');
            page.signup(testUser14);
            page.form.submit.click();
            browser.sleep(3000);
            var text = browser.findElement(by.xpath("//form[@name='userForm']/div/div[4]/div[1]/div")).getText().then(function (ss) {

                return ss.split(". ");
            });
            text.then(function (slices) {

                var otp1 = slices[1];
                page.form.otp.sendKeys(otp1);

            });
            page.form.submit.click();
            browser.sleep(3000);
            page.form.registrationCategory.click();
            browser.driver.findElement(by.xpath("//*[@id='RC_0']")).click();
            page.form.password.sendKeys(12345678);
            page.form.ConfirmPassword.sendKeys(12345678);
            page.form.submit.click();
            browser.sleep(3000);
            browser.driver.findElement(by.xpath("//*[@id='seg_1']")).click();
            browser.driver.findElement(by.xpath(".//button[@aria-label='Cancel']")).click();
            browser.sleep(1000);
            browser.driver.findElement(by.xpath("//*[@id='content']/div/div[1]/div/div[7]/button")).click();
            browser.sleep(1000);
            expect(browser.getCurrentUrl()).toEqual('http://localhost:3000/#!/');
            browser.get('http://localhost:3000/#!/signin');
            page1 = require('./signup/busseg.po');
            page1.bussignin(testUser15);
            page1.form.submit.click();
            browser.sleep(3000);
            browser.driver.findElement(by.xpath("//*[@class='nvc-category-select']")).then(function () {
                console.log('Dialog is popped');
            }, function (err) {
                console.info('Dialog is not present as expected');
            });
            browser.driver.findElement(by.xpath(".//button[@aria-label='Cancel']")).click();
            browser.sleep(2000);
            browser.driver.findElement(by.xpath("//*[@id='content']/div/div[1]/div/div[7]/button")).click();
            browser.sleep(1000);
            expect(browser.getCurrentUrl()).toEqual('http://localhost:3000/#!/');
        });

        //15.TEST CASE FOR CHECKING WHETHER BUSINESS SEGMENT IS POPPING UP FOR THE USER WHO DID NOT SELECTED THE BUSINESS SEGMENT AS PART OF REGISTRATION[CLICKED SAVE]
        it('should signup a new user, log them in, select business segment and redirecting to "/"', function () {
            page = require('./signup/signup.po');
            page.signup(testUser16);
            page.form.submit.click();
            browser.sleep(3000);
            var text = browser.findElement(by.xpath("//form[@name='userForm']/div/div[4]/div[1]/div")).getText().then(function (ss) {

                return ss.split(". ");
            });
            text.then(function (slices) {

                var otp1 = slices[1];
                page.form.otp.sendKeys(otp1);

            });
            page.form.submit.click();
            browser.sleep(3000);
            page.form.registrationCategory.click();
            browser.driver.findElement(by.xpath("//*[@id='RC_0']")).click();
            page.form.password.sendKeys(12345678);
            page.form.ConfirmPassword.sendKeys(12345678);
            page.form.submit.click();
            browser.sleep(3000);
            browser.driver.findElement(by.xpath(".//button[@aria-label='Cancel']")).click();
            browser.sleep(1000);
            browser.driver.findElement(by.xpath("//*[@id='content']/div/div[1]/div/div[7]/button")).click();
            browser.sleep(1000);
            expect(browser.getCurrentUrl()).toEqual('http://localhost:3000/#!/');
            browser.get('http://localhost:3000/#!/signin');
            page1 = require('./signup/busseg.po');
            page1.bussignin(testUser17);
            page1.form.submit.click();
            browser.sleep(3000);
            browser.driver.findElement(by.xpath("//*[@class='nvc-category-select']")).then(function () {
                console.log('Dialog is popped');
            }, function (err) {
                console.info('Dialog is not present as expected');
            });
            browser.driver.findElement(by.xpath(".//button[@aria-label='Cancel']")).click();
            browser.sleep(2000);
            browser.driver.findElement(by.xpath("//*[@id='content']/div/div[1]/div/div[7]/button")).click();
            browser.sleep(1000);
            expect(browser.getCurrentUrl()).toEqual('http://localhost:3000/#!/');
        });

        //16.TEST CASE FOR SELECTING 'RETAILER' AND CHECKING HOW WE CAN SELECT THE BUSINESS SEGMENT
        it('should signup a new user, log them in, select business segment and redirecting to "/"', function () {
            page = require('./signup/signup.po');
            page.signup(testUser18);
            page.form.submit.click();
            browser.sleep(3000);
            var text = browser.findElement(by.xpath("//form[@name='userForm']/div/div[4]/div[1]/div")).getText().then(function (ss) {

                return ss.split(". ");
            });
            text.then(function (slices) {

                var otp1 = slices[1];
                page.form.otp.sendKeys(otp1);

            });
            page.form.submit.click();
            browser.sleep(3000);
            page.form.registrationCategory.click();
            browser.driver.findElement(by.xpath("//*[@id='RC_2']")).click();
            page.form.password.sendKeys(12345678);
            page.form.ConfirmPassword.sendKeys(12345678);
            page.form.submit.click();
            browser.sleep(3000);
            var explore = browser.driver.findElement(by.xpath("//*[@aria-label='Explore More']"));
            expect(explore.getAttribute('aria-hidden')).toBe('false');
            browser.driver.findElement(by.xpath(".//button[@aria-label='Cancel']")).click();
            browser.sleep(1000);
            browser.driver.findElement(by.xpath("//*[@id='content']/div/div[1]/div/div[7]/button")).click();
            browser.sleep(1000);
            expect(browser.getCurrentUrl()).toEqual('http://localhost:3000/#!/');
        });

        //17.TEST CASE FOR CHECKING WHETHER BUSINESS SEGMENTS ARE SHOWING WHEN EXPLORE MORE BUTTON IS CLICKED [DISTRIBUTOR]
        it('should signup a new user, log them in, select business segment and redirecting to "/"', function () {
            page = require('./signup/signup.po');
            page.signup(testUser19);
            page.form.submit.click();
            browser.sleep(3000);
            var text = browser.findElement(by.xpath("//form[@name='userForm']/div/div[4]/div[1]/div")).getText().then(function (ss) {

                return ss.split(". ");
            });
            text.then(function (slices) {

                var otp1 = slices[1];
                page.form.otp.sendKeys(otp1);

            });
            page.form.submit.click();
            browser.sleep(3000);
            page.form.registrationCategory.click();
            browser.driver.findElement(by.xpath("//*[@id='RC_1']")).click();
            page.form.password.sendKeys(12345678);
            page.form.ConfirmPassword.sendKeys(12345678);
            page.form.submit.click();
            browser.sleep(3000);
            browser.driver.findElement(by.xpath("//*[@aria-label='Explore More']")).click();
            expect(browser.driver.findElement(by.xpath("//nv-tree/div/nv-node[1]/div/div")).isDisplayed()).toBe(true);
            browser.sleep(1000);
            browser.driver.findElement(by.xpath(".//button[@aria-label='Cancel']")).click();
            browser.sleep(2000);
            browser.driver.findElement(by.xpath("//*[@id='content']/div/div[1]/div/div[7]/button")).click();
            browser.sleep(1000);
            expect(browser.getCurrentUrl()).toEqual('http://localhost:3000/#!/');
        });

        //18.TEST CASE FOR CHECKING WHETHER BUSINESS SEGMENTS ARE SHOWING WHEN EXPLORE MORE BUTTON IS CLICKED {TWO TIMES} [DISTRIBUTOR]
        it('should signup a new user, log them in, select business segment and redirecting to "/"', function () {
            page = require('./signup/signup.po');
            page.signup(testUser20);
            page.form.submit.click();
            browser.sleep(3000);
            var text = browser.findElement(by.xpath("//form[@name='userForm']/div/div[4]/div[1]/div")).getText().then(function (ss) {

                return ss.split(". ");
            });
            text.then(function (slices) {

                var otp1 = slices[1];
                page.form.otp.sendKeys(otp1);

            });
            page.form.submit.click();
            browser.sleep(3000);
            page.form.registrationCategory.click();
            browser.driver.findElement(by.xpath("//*[@id='RC_1']")).click();
            page.form.password.sendKeys(12345678);
            page.form.ConfirmPassword.sendKeys(12345678);
            page.form.submit.click();
            browser.sleep(3000);
            browser.driver.findElement(by.xpath("//*[@aria-label='Explore More']")).click();
            expect(browser.driver.findElement(by.xpath("//nv-tree/div/nv-node[1]/div/div")).isDisplayed()).toBe(true);
            browser.sleep(1000);
            browser.driver.findElement(by.xpath("//*[@aria-label='Explore More']")).click();
            browser.driver.findElement(by.xpath("//nv-tree/div/nv-node[1]/div/div")).then(function () {
                console.log('Category is present');
            }, function (err) {
                console.info('Category is not present as expected');
            });
            browser.sleep(1000);
            browser.driver.findElement(by.xpath(".//button[@aria-label='Cancel']")).click();
            browser.sleep(2000);
            browser.driver.findElement(by.xpath("//*[@id='content']/div/div[1]/div/div[7]/button")).click();
            browser.sleep(1000);
            expect(browser.getCurrentUrl()).toEqual('http://localhost:3000/#!/');
        });

        //19.TEST CASE FOR SELECTING 'CONSUMER AGENT' AND CHECKING HOW WE CAN SELECT THE BUSINESS SEGMENT
        it('should signup a new user, log them in, select business segment and redirecting to "/"', function () {
            page = require('./signup/signup.po');
            page.signup(testUser21);
            page.form.submit.click();
            browser.sleep(3000);
            var text = browser.findElement(by.xpath("//form[@name='userForm']/div/div[4]/div[1]/div")).getText().then(function (ss) {

                return ss.split(". ");
            });
            text.then(function (slices) {

                var otp1 = slices[1];
                page.form.otp.sendKeys(otp1);

            });
            page.form.submit.click();
            browser.sleep(3000);
            page.form.registrationCategory.click();
            browser.driver.findElement(by.xpath("//*[@id='RC_3']")).click();
            page.form.password.sendKeys(12345678);
            page.form.ConfirmPassword.sendKeys(12345678);
            page.form.submit.click();
            browser.sleep(3000);
            var explore = browser.driver.findElement(by.xpath("//*[@aria-label='Explore More']"));
            expect(explore.getAttribute('aria-hidden')).toBe('false');
            expect(browser.driver.findElement(by.xpath("//nv-tree/div/nv-node[1]/div/div")).getText()).toBe('Vegetable');
            browser.driver.findElement(by.xpath("//nv-tree/div/nv-node[1]/div/div/span")).click();
            expect(browser.driver.findElement(by.xpath("//nv-tree/div/nv-node[1]/div[2]/nv-tree/div/nv-node[2]/div/div")).isDisplayed()).toBe(true);
            browser.sleep(1000);
            expect(browser.driver.findElement(by.xpath("//nv-tree/div/nv-node[1]/div[2]/nv-tree/div/nv-node[2]/div/div/md-checkbox")).getAttribute('aria-checked')).toBe('false');
            browser.sleep(2000);
            browser.driver.findElement(by.xpath(".//button[@aria-label='Cancel']")).click();
            browser.sleep(2000);
            browser.driver.findElement(by.xpath("//*[@id='content']/div/div[1]/div/div[7]/button")).click();
            browser.sleep(1000);
            expect(browser.getCurrentUrl()).toEqual('http://localhost:3000/#!/');
        });

        //20.TEST CASE FOR SELECTING 'RETAILER' AND CHECKING HOW WE CAN SELECT THE BUSINESS SEGMENT
        it('should signup a new user, log them in, select business segment and redirecting to "/"', function () {
            page = require('./signup/signup.po');
            page.signup(testUser22);
            page.form.submit.click();
            browser.sleep(3000);
            var text = browser.findElement(by.xpath("//form[@name='userForm']/div/div[4]/div[1]/div")).getText().then(function (ss) {

                return ss.split(". ");
            });
            text.then(function (slices) {

                var otp1 = slices[1];
                page.form.otp.sendKeys(otp1);

            });
            page.form.submit.click();
            browser.sleep(3000);
            page.form.registrationCategory.click();
            browser.driver.findElement(by.xpath("//*[@id='RC_2']")).click();
            page.form.password.sendKeys(12345678);
            page.form.ConfirmPassword.sendKeys(12345678);
            page.form.submit.click();
            browser.sleep(3000);
            var explore = browser.driver.findElement(by.xpath("//*[@aria-label='Explore More']"));
            expect(explore.getAttribute('aria-hidden')).toBe('false');
            expect(browser.driver.findElement(by.xpath("//*[@aria-label='Vegetable']")).isDisplayed()).toBe(true);
            expect(browser.driver.findElement(by.xpath("//nv-tree/div/nv-node[2]/div/div/md-checkbox")).isDisplayed()).toBe(true);
            expect(browser.driver.findElement(by.xpath("//*[@aria-label='Pulses']")).isDisplayed()).toBe(true);
            expect(browser.driver.findElement(by.xpath("//*[@aria-label='Spices']")).isDisplayed()).toBe(true);
            expect(browser.driver.findElement(by.xpath("//*[@aria-label='Cotton']")).isDisplayed()).toBe(true);
            expect(browser.driver.findElement(by.xpath("//*[@aria-label='Yarn']")).isDisplayed()).toBe(true);
            expect(browser.driver.findElement(by.xpath("//*[@aria-label='Fabric']")).isDisplayed()).toBe(true);
            expect(browser.driver.findElement(by.xpath("//*[@aria-label='Apparel']")).isDisplayed()).toBe(true);
            expect(browser.driver.findElement(by.xpath("//*[@aria-label='Cereals']")).isDisplayed()).toBe(true);
            expect(browser.driver.findElement(by.xpath("//*[@aria-label='Health Care']")).isDisplayed()).toBe(true);
            expect(browser.driver.findElement(by.xpath("//*[@aria-label='Food Products']")).isDisplayed()).toBe(true);
            expect(browser.driver.findElement(by.xpath("//*[@aria-label='Home Care']")).isDisplayed()).toBe(true);
            expect(browser.driver.findElement(by.xpath("//*[@aria-label='Personal Care']")).isDisplayed()).toBe(true);
            expect(browser.driver.findElement(by.xpath("//*[@aria-label='Beverages']")).isDisplayed()).toBe(true);
            browser.driver.findElement(by.xpath(".//button[@aria-label='Cancel']")).click();
            browser.sleep(2000);
            browser.driver.findElement(by.xpath("//*[@id='content']/div/div[1]/div/div[7]/button")).click();
            browser.sleep(1000);
            expect(browser.getCurrentUrl()).toEqual('http://localhost:3000/#!/');
        });

        //21.TEST CASE FOR CHECKING ALL THE LABELS OF THE BUSINESS SEGMENTS
        it('should signup a new user, log them in, select business segment and redirecting to "/"', function () {
            page = require('./signup/signup.po');
            page.signup(testUser23);
            page.form.submit.click();
            browser.sleep(3000);
            var text = browser.findElement(by.xpath("//form[@name='userForm']/div/div[4]/div[1]/div")).getText().then(function (ss) {

                return ss.split(". ");
            });
            text.then(function (slices) {

                var otp1 = slices[1];
                page.form.otp.sendKeys(otp1);

            });
            page.form.submit.click();
            browser.sleep(3000);
            page.form.registrationCategory.click();
            browser.driver.findElement(by.xpath("//*[@id='RC_3']")).click();
            page.form.password.sendKeys(12345678);
            page.form.ConfirmPassword.sendKeys(12345678);
            page.form.submit.click();
            browser.sleep(3000);
            expect(browser.driver.findElement(by.xpath("//nv-tree/div/nv-node[1]/div/div")).getText()).toBe('Vegetable');
            expect(browser.driver.findElement(by.xpath("//nv-tree/div/nv-node[2]/div/div")).getText()).toBe('Coffee');
            expect(browser.driver.findElement(by.xpath("//nv-tree/div/nv-node[3]/div/div")).getText()).toBe('Pulses');
            expect(browser.driver.findElement(by.xpath("//nv-tree/div/nv-node[4]/div/div")).getText()).toBe('Spices');
            expect(browser.driver.findElement(by.xpath("//nv-tree/div/nv-node[5]/div/div")).getText()).toBe('Cotton');
            expect(browser.driver.findElement(by.xpath("//nv-tree/div/nv-node[6]/div/div")).getText()).toBe('Yarn');
            expect(browser.driver.findElement(by.xpath("//nv-tree/div/nv-node[7]/div/div")).getText()).toBe('Fabric');
            expect(browser.driver.findElement(by.xpath("//nv-tree/div/nv-node[8]/div/div")).getText()).toBe('Apparel');
            expect(browser.driver.findElement(by.xpath("//nv-tree/div/nv-node[9]/div/div")).getText()).toBe('Cereals');
            expect(browser.driver.findElement(by.xpath("//nv-tree/div/nv-node[10]/div/div")).getText()).toBe('Health Care');
            expect(browser.driver.findElement(by.xpath("//nv-tree/div/nv-node[11]/div/div")).getText()).toBe('Food Products');
            expect(browser.driver.findElement(by.xpath("//nv-tree/div/nv-node[12]/div/div")).getText()).toBe('Home Care');
            expect(browser.driver.findElement(by.xpath("//nv-tree/div/nv-node[13]/div/div")).getText()).toBe('Personal Care');
            expect(browser.driver.findElement(by.xpath("//nv-tree/div/nv-node[14]/div/div")).getText()).toBe('Beverages');
            browser.driver.findElement(by.xpath(".//button[@aria-label='Cancel']")).click();
            browser.sleep(2000);
            browser.driver.findElement(by.xpath("//*[@id='content']/div/div[1]/div/div[7]/button")).click();
            browser.sleep(1000);
            expect(browser.getCurrentUrl()).toEqual('http://localhost:3000/#!/');
        });

        //22.TEST CASE FOR CHECKING WHEN A PARENT SEGMENT IS SELECTED WHETHER ALL THE CHILD ARE GETTING SELECTED OT NOT
        it('should signup a new user, log them in, select business segment and redirecting to "/"', function () {
            page = require('./signup/signup.po');
            page.signup(testUser24);
            page.form.submit.click();
            browser.sleep(3000);
            var text = browser.findElement(by.xpath("//form[@name='userForm']/div/div[4]/div[1]/div")).getText().then(function (ss) {

                return ss.split(". ");
            });
            text.then(function (slices) {

                var otp1 = slices[1];
                page.form.otp.sendKeys(otp1);

            });
            page.form.submit.click();
            browser.sleep(3000);
            page.form.registrationCategory.click();
            browser.driver.findElement(by.xpath("//*[@id='RC_3']")).click();
            page.form.password.sendKeys(12345678);
            page.form.ConfirmPassword.sendKeys(12345678);
            page.form.submit.click();
            browser.sleep(3000);
            browser.driver.findElement(by.xpath("//nv-tree/div/nv-node[1]/div[1]/div/md-checkbox/div[1]")).click();
            browser.driver.findElement(by.xpath("//nv-tree/div/nv-node[1]/div/div/span")).click();
            expect(browser.driver.findElement(by.xpath("//*[@aria-label='Raw Corn']")).getAttribute('aria-checked')).toBe('true');
            expect(browser.driver.findElement(by.xpath("//*[@aria-label='Lemon']")).getAttribute('aria-checked')).toBe('true');
            expect(browser.driver.findElement(by.xpath("//*[@aria-label='Onion']")).getAttribute('aria-checked')).toBe('true');
            expect(browser.driver.findElement(by.xpath("//*[@aria-label='Garlic']")).getAttribute('aria-checked')).toBe('true');
            expect(browser.driver.findElement(by.xpath("//*[@aria-label='Potato']")).getAttribute('aria-checked')).toBe('true');
            browser.sleep(2000);
            browser.driver.findElement(by.xpath(".//button[@aria-label='Cancel']")).click();
            browser.sleep(2000);
            browser.driver.findElement(by.xpath("//*[@id='content']/div/div[1]/div/div[7]/button")).click();
            browser.sleep(1000);
            expect(browser.getCurrentUrl()).toEqual('http://localhost:3000/#!/');
        });

        //23.TEST CASE FOR CHECKING WHETHER PARENT IS GETTING SELECTED WHEN ALL THE CHILDREN ARE SELECTED
        it('should signup a new user, log them in, select business segment and redirecting to "/"', function () {
            page = require('./signup/signup.po');
            page.signup(testUser25);
            page.form.submit.click();
            browser.sleep(3000);
            var text = browser.findElement(by.xpath("//form[@name='userForm']/div/div[4]/div[1]/div")).getText().then(function (ss) {

                return ss.split(". ");
            });
            text.then(function (slices) {

                var otp1 = slices[1];
                page.form.otp.sendKeys(otp1);

            });
            page.form.submit.click();
            browser.sleep(3000);
            page.form.registrationCategory.click();
            browser.driver.findElement(by.xpath("//*[@id='RC_3']")).click();
            page.form.password.sendKeys(12345678);
            page.form.ConfirmPassword.sendKeys(12345678);
            page.form.submit.click();
            browser.sleep(3000);
            browser.driver.findElement(by.xpath("//nv-tree/div/nv-node[1]/div/div/span")).click();
            browser.driver.findElement(by.xpath("//nv-tree/div/nv-node[1]/div[2]/nv-tree/div/nv-node[1]/div/div/md-checkbox/div[1]")).click();
            browser.driver.findElement(by.xpath("//nv-tree/div/nv-node[1]/div[2]/nv-tree/div/nv-node[2]/div/div/md-checkbox/div[1]")).click();
            browser.driver.findElement(by.xpath("//nv-tree/div/nv-node[1]/div[2]/nv-tree/div/nv-node[3]/div/div/md-checkbox/div[1]")).click();
            browser.driver.findElement(by.xpath("//nv-tree/div/nv-node[1]/div[2]/nv-tree/div/nv-node[4]/div/div/md-checkbox/div[1]")).click();
            browser.driver.findElement(by.xpath("//nv-tree/div/nv-node[1]/div[2]/nv-tree/div/nv-node[5]/div/div/md-checkbox/div[1]")).click();
            expect(browser.driver.findElement(by.xpath("//nv-tree/div/nv-node[1]/div[1]/div/md-checkbox")).getAttribute('aria-checked')).toBe('true');
            browser.sleep(2000);
            browser.driver.findElement(by.xpath(".//button[@aria-label='Save']")).click();
            browser.sleep(2000);
            browser.driver.findElement(by.xpath("//*[@id='content']/div/div[1]/div/div[7]/button")).click();
            browser.sleep(1000);
            expect(browser.getCurrentUrl()).toEqual('http://localhost:3000/#!/');
        });

        //24.TEST CASE FOR SELECTING MULTIPLE BUSINESS SEGMENT ACCORDING TO THE WISH OF THE USER AND SAVING
        it('should signup a new user, log them in, select business segment and redirecting to "/"', function () {
            page = require('./signup/signup.po');
            page.signup(testUser26);
            page.form.submit.click();
            browser.sleep(3000);
            var text = browser.findElement(by.xpath("//form[@name='userForm']/div/div[4]/div[1]/div")).getText().then(function (ss) {

                return ss.split(". ");
            });
            text.then(function (slices) {

                var otp1 = slices[1];
                page.form.otp.sendKeys(otp1);

            });
            page.form.submit.click();
            browser.sleep(3000);
            page.form.registrationCategory.click();
            browser.driver.findElement(by.xpath("//*[@id='RC_3']")).click();
            page.form.password.sendKeys(12345678);
            page.form.ConfirmPassword.sendKeys(12345678);
            page.form.submit.click();
            browser.sleep(3000);
            browser.driver.findElement(by.xpath("//nv-tree/div/nv-node[5]/div/div/span")).click();
            browser.driver.findElement(by.xpath("//nv-tree/div/nv-node[5]/div[2]/nv-tree/div/nv-node[1]/div/div/md-checkbox/div[1]")).click();
            browser.driver.findElement(by.xpath("//nv-tree/div/nv-node[5]/div/div/span")).click();
            browser.driver.findElement(by.xpath("//nv-tree/div/nv-node[6]/div/div/span")).click();
            browser.driver.findElement(by.xpath("//nv-tree/div/nv-node[6]/div[2]/nv-tree/div/nv-node[2]/div/div/md-checkbox/div[1]")).click();
            browser.driver.findElement(by.xpath("//nv-tree/div/nv-node[6]/div/div/span")).click();
            browser.driver.findElement(by.xpath("//nv-tree/div/nv-node[10]/div/div/span")).click();
            browser.driver.findElement(by.xpath("//nv-tree/div/nv-node[10]/div[2]/nv-tree/div/nv-node[5]/div/div/md-checkbox/div[1]")).click();
            browser.driver.findElement(by.xpath("//nv-tree/div/nv-node[10]/div/div/span")).click();
            browser.sleep(2000);
            browser.driver.findElement(by.xpath(".//button[@aria-label='Save']")).click();
            browser.sleep(2000);
            browser.driver.findElement(by.xpath("//*[@id='content']/div/div[1]/div/div[7]/button")).click();
            browser.sleep(1000);
            expect(browser.getCurrentUrl()).toEqual('http://localhost:3000/#!/');
        });

        //25.TEST CASE FOR CHECKING WHETHER THE SELECTED BUSINESS SEGMENTS ARE GETTING SELECTED FOR REGISTRATION CATEGORY BEING RETAILER
        it('should signup a new user, log them in, select business segment and redirecting to "/"', function () {
            page = require('./signup/signup.po');
            page.signup(testUser27);
            page.form.submit.click();
            browser.sleep(3000);
            var text = browser.findElement(by.xpath("//form[@name='userForm']/div/div[4]/div[1]/div")).getText().then(function (ss) {

                return ss.split(". ");
            });
            text.then(function (slices) {

                var otp1 = slices[1];
                page.form.otp.sendKeys(otp1);

            });
            page.form.submit.click();
            browser.sleep(3000);
            page.form.registrationCategory.click();
            browser.driver.findElement(by.xpath("//*[@id='RC_3']")).click();
            page.form.password.sendKeys(12345678);
            page.form.ConfirmPassword.sendKeys(12345678);
            page.form.submit.click();
            browser.sleep(3000);
            browser.driver.findElement(by.xpath("//nv-tree/div/nv-node[5]/div/div/span")).click();
            browser.driver.findElement(by.xpath("//nv-tree/div/nv-node[5]/div[2]/nv-tree/div/nv-node[1]/div/div/md-checkbox/div[1]")).click();
            browser.driver.findElement(by.xpath("//nv-tree/div/nv-node[5]/div/div/span")).click();
            browser.driver.findElement(by.xpath("//nv-tree/div/nv-node[6]/div/div/span")).click();
            browser.driver.findElement(by.xpath("//nv-tree/div/nv-node[6]/div[2]/nv-tree/div/nv-node[2]/div/div/md-checkbox/div[1]")).click();
            browser.driver.findElement(by.xpath("//nv-tree/div/nv-node[6]/div/div/span")).click();
            browser.driver.findElement(by.xpath("//nv-tree/div/nv-node[10]/div/div/span")).click();
            browser.driver.findElement(by.xpath("//nv-tree/div/nv-node[10]/div[2]/nv-tree/div/nv-node[2]/div/div/md-checkbox/div[1]")).click();
            browser.driver.findElement(by.xpath("//nv-tree/div/nv-node[10]/div/div/span")).click();
            browser.sleep(2000);
            browser.driver.findElement(by.xpath(".//button[@aria-label='Save']")).click();
            browser.sleep(2000);
            browser.driver.findElement(by.xpath("//*[@id='content']/div/div[1]/div/div[7]/button")).click();
            browser.sleep(1000);
            expect(browser.getCurrentUrl()).toEqual('http://localhost:3000/#!/');
            browser.get('http://localhost:3000/#!/signin');
            page1 = require('./signup/busseg.po');
            page1.bussignin(testUser27);
            page1.form.password.sendKeys('12345678');
            page1.form.submit.click();
            browser.sleep(3000);
            browser.driver.findElement(by.xpath("//*[@class='nvc-category-select']")).then(function () {
                console.log('Dialog is popped');
            }, function (err) {
                console.info('Dialog is not present as expected');
            });
            browser.sleep(2000);
            browser.driver.findElement(by.xpath("//*[@id='content']/div/div[1]/div/div[7]/button")).click();
            browser.sleep(1000);
            expect(browser.getCurrentUrl()).toEqual('http://localhost:3000/#!/');
        });
        
    });


    describe('with local auth', function() {
        beforeEach(function () {
            loadPage1();

        });
        it('should signup a new user, log them in, select business segment and redirecting to "/"', function () {
            page1 = require('./signup/busseg.po');
            page1.bussignin(testUser18);
            page1.form.password.sendKeys('12345678');
            page1.form.submit.click();
            browser.sleep(3000);
            browser.driver.findElement(by.xpath("//nv-tree/div/nv-node[2]/div/div/span")).click();
            browser.sleep(1000);
            expect(browser.driver.findElement(by.xpath("//*[@aria-label='Coffee Berries']")).isDisplayed()).toBe(true);
            expect(browser.driver.findElement(by.xpath("//*[@aria-label='Green Coffee Beans']")).isDisplayed()).toBe(true);
            expect(browser.driver.findElement(by.xpath("//*[@aria-label='Roasted Coffee Beans']")).isDisplayed()).toBe(true);
            browser.sleep(2000);
            browser.driver.findElement(by.xpath(".//button[@aria-label='Cancel']")).click();
            browser.sleep(2000);
            browser.driver.findElement(by.xpath("//*[@id='content']/div/div[1]/div/div[7]/button")).click();
            browser.sleep(1000);
            expect(browser.getCurrentUrl()).toEqual('http://localhost:3000/#!/');
        });
        it('should signup a new user, log them in, select business segment and redirecting to "/"', function () {
            page1 = require('./signup/busseg.po');
            page1.bussignin(testUser18);
            page1.form.password.sendKeys('12345678');
            page1.form.submit.click();
            browser.sleep(3000);
            browser.driver.findElement(by.xpath("//nv-tree/div/nv-node[3]/div/div/span")).click();
            browser.sleep(1000);
            browser.sleep(1000);
            expect(browser.driver.findElement(by.xpath("//*[@aria-label='Toor Dal']")).isDisplayed()).toBe(true);
            expect(browser.driver.findElement(by.xpath("//*[@aria-label='Black Urad Dal Whole']")).isDisplayed()).toBe(true);
            expect(browser.driver.findElement(by.xpath("//*[@aria-label='Black Urad Dal Split']")).isDisplayed()).toBe(true);
            expect(browser.driver.findElement(by.xpath("//*[@aria-label='White Urad Dal Whole']")).isDisplayed()).toBe(true);
            expect(browser.driver.findElement(by.xpath("//*[@aria-label='White Urad Dal Split']")).isDisplayed()).toBe(true);
            expect(browser.driver.findElement(by.xpath("//*[@aria-label='Green Moong Dal Whole']")).isDisplayed()).toBe(true);
            expect(browser.driver.findElement(by.xpath("//*[@aria-label='Green Moong Dal Split']")).isDisplayed()).toBe(true);
            expect(browser.driver.findElement(by.xpath("//*[@aria-label='Yellow Moong Dal Split']")).isDisplayed()).toBe(true);
            expect(browser.driver.findElement(by.xpath("//*[@aria-label='Masoor Dal']")).isDisplayed()).toBe(true);
            expect(browser.driver.findElement(by.xpath("//*[@aria-label='Channa Dal']")).isDisplayed()).toBe(true);
            expect(browser.driver.findElement(by.xpath("//*[@aria-label='Kabuli Chana Dal White']")).isDisplayed()).toBe(true);
            expect(browser.driver.findElement(by.xpath("//*[@aria-label='Kabuli Chana Dal Brown']")).isDisplayed()).toBe(true);
            expect(browser.driver.findElement(by.xpath("//*[@aria-label='Horse Gram']")).isDisplayed()).toBe(true);
            expect(browser.driver.findElement(by.xpath("//*[@aria-label='Soya Bean']")).isDisplayed()).toBe(true);
            expect(browser.driver.findElement(by.xpath("//*[@aria-label='Cowpea']")).isDisplayed()).toBe(true);
            expect(browser.driver.findElement(by.xpath("//*[@aria-label='Sesame']")).isDisplayed()).toBe(true);
            expect(browser.driver.findElement(by.xpath("//*[@aria-label='Peas']")).isDisplayed()).toBe(true);
            expect(browser.driver.findElement(by.xpath("//*[@aria-label='Red Gram']")).isDisplayed()).toBe(true);
            expect(browser.driver.findElement(by.xpath("//*[@aria-label='Groundnut']")).isDisplayed()).toBe(true);
            expect(browser.driver.findElement(by.xpath("//*[@aria-label='Rajma Beans']")).isDisplayed()).toBe(true);
            browser.sleep(2000);
            browser.driver.findElement(by.xpath(".//button[@aria-label='Cancel']")).click();
            browser.sleep(2000);
            browser.driver.findElement(by.xpath("//*[@id='content']/div/div[1]/div/div[7]/button")).click();
            browser.sleep(1000);
            expect(browser.getCurrentUrl()).toEqual('http://localhost:3000/#!/');
        });
        it('should signup a new user, log them in, select business segment and redirecting to "/"', function () {
            page1 = require('./signup/busseg.po');
            page1.bussignin(testUser18);
            page1.form.password.sendKeys('12345678');
            page1.form.submit.click();
            browser.sleep(3000);
            browser.driver.findElement(by.xpath("//nv-tree/div/nv-node[4]/div/div/span")).click();
            browser.sleep(1000);
            expect(browser.driver.findElement(by.xpath("//*[@aria-label='Coriander']")).isDisplayed()).toBe(true);
            expect(browser.driver.findElement(by.xpath("//*[@aria-label='Jeera']")).isDisplayed()).toBe(true);
            expect(browser.driver.findElement(by.xpath("//*[@aria-label='Mustard']")).isDisplayed()).toBe(true);
            expect(browser.driver.findElement(by.xpath("//*[@aria-label='Methi']")).isDisplayed()).toBe(true);
            expect(browser.driver.findElement(by.xpath("//*[@aria-label='Dry Chilli']")).isDisplayed()).toBe(true);
            expect(browser.driver.findElement(by.xpath("//*[@aria-label='Tamarind']")).isDisplayed()).toBe(true);
            expect(browser.driver.findElement(by.xpath("//*[@aria-label='Clove']")).isDisplayed()).toBe(true);
            expect(browser.driver.findElement(by.xpath("//*[@aria-label='Black Pepper']")).isDisplayed()).toBe(true);
            expect(browser.driver.findElement(by.xpath("//*[@aria-label='Ginger']")).isDisplayed()).toBe(true);
            expect(browser.driver.findElement(by.xpath("//*[@aria-label='Carom Seed']")).isDisplayed()).toBe(true);
            expect(browser.driver.findElement(by.xpath("//*[@aria-label='Bay Leaf']")).isDisplayed()).toBe(true);
            expect(browser.driver.findElement(by.xpath("//*[@aria-label='Green Cardamom']")).isDisplayed()).toBe(true);
            expect(browser.driver.findElement(by.xpath("//*[@aria-label='Dry Coconut']")).isDisplayed()).toBe(true);
            expect(browser.driver.findElement(by.xpath("//*[@aria-label='Salt']")).isDisplayed()).toBe(true);
            expect(browser.driver.findElement(by.xpath("//*[@aria-label='Cassia']")).isDisplayed()).toBe(true);
            expect(browser.driver.findElement(by.xpath("//*[@aria-label='Turmeric']")).isDisplayed()).toBe(true);
            expect(browser.driver.findElement(by.xpath("//*[@aria-label='Jaggery Powder']")).isDisplayed()).toBe(true);
            expect(browser.driver.findElement(by.xpath("//*[@aria-label='Black Till']")).isDisplayed()).toBe(true);
            expect(browser.driver.findElement(by.xpath("//*[@aria-label='White Till']")).isDisplayed()).toBe(true);
            expect(browser.driver.findElement(by.xpath("//*[@aria-label='Poppy Seeds']")).isDisplayed()).toBe(true);
            expect(browser.driver.findElement(by.xpath("//*[@aria-label='Javitri']")).isDisplayed()).toBe(true);
            expect(browser.driver.findElement(by.xpath("//*[@aria-label='Sabja']")).isDisplayed()).toBe(true);
            expect(browser.driver.findElement(by.xpath("//*[@aria-label='Sara Pappu']")).isDisplayed()).toBe(true);
            expect(browser.driver.findElement(by.xpath("//*[@aria-label='Melon Seeds']")).isDisplayed()).toBe(true);
            expect(browser.driver.findElement(by.xpath("//*[@aria-label='Jakayi']")).isDisplayed()).toBe(true);
            expect(browser.driver.findElement(by.xpath("//*[@aria-label='Sunflower Seeds']")).isDisplayed()).toBe(true);
            expect(browser.driver.findElement(by.xpath("//*[@aria-label='Flax Seeds']")).isDisplayed()).toBe(true);
            browser.sleep(2000);
            browser.driver.findElement(by.xpath(".//button[@aria-label='Cancel']")).click();
            browser.sleep(2000);
            browser.driver.findElement(by.xpath("//*[@id='content']/div/div[1]/div/div[7]/button")).click();
            browser.sleep(1000);
            expect(browser.getCurrentUrl()).toEqual('http://localhost:3000/#!/');
        });
        it('should signup a new user, log them in, select business segment and redirecting to "/"', function () {
            page1 = require('./signup/busseg.po');
            page1.bussignin(testUser18);
            page1.form.password.sendKeys('12345678');
            page1.form.submit.click();
            browser.sleep(3000);
            browser.driver.findElement(by.xpath("//nv-tree/div/nv-node[5]/div/div/span")).click();
            browser.sleep(1000);
            expect(browser.driver.findElement(by.xpath("//*[@aria-label='Cotton Bales']")).isDisplayed()).toBe(true);
            expect(browser.driver.findElement(by.xpath("//*[@aria-label='Cotton Seeds']")).isDisplayed()).toBe(true);
            browser.sleep(2000);
            browser.driver.findElement(by.xpath(".//button[@aria-label='Cancel']")).click();
            browser.sleep(2000);
            browser.driver.findElement(by.xpath("//*[@id='content']/div/div[1]/div/div[7]/button")).click();
            browser.sleep(1000);
            expect(browser.getCurrentUrl()).toEqual('http://localhost:3000/#!/');
        });
        it('should signup a new user, log them in, select business segment and redirecting to "/"', function () {
            page1 = require('./signup/busseg.po');
            page1.bussignin(testUser18);
            page1.form.password.sendKeys('12345678');
            page1.form.submit.click();
            browser.sleep(3000);
            browser.driver.findElement(by.xpath("//nv-tree/div/nv-node[6]/div/div/span")).click();
            browser.sleep(1000);
            expect(browser.driver.findElement(by.xpath("//*[@aria-label='Cotton Yarn']")).isDisplayed()).toBe(true);
            expect(browser.driver.findElement(by.xpath("//*[@aria-label='Silk Yarn']")).isDisplayed()).toBe(true);
            expect(browser.driver.findElement(by.xpath("//*[@aria-label='Wool Yarn']")).isDisplayed()).toBe(true);
            browser.sleep(2000);
            browser.driver.findElement(by.xpath(".//button[@aria-label='Cancel']")).click();
            browser.sleep(2000);
            browser.driver.findElement(by.xpath("//*[@id='content']/div/div[1]/div/div[7]/button")).click();
            browser.sleep(1000);
            expect(browser.getCurrentUrl()).toEqual('http://localhost:3000/#!/');
        });
        it('should signup a new user, log them in, select business segment and redirecting to "/"', function () {
            page1 = require('./signup/busseg.po');
            page1.bussignin(testUser18);
            page1.form.password.sendKeys('12345678');
            page1.form.submit.click();
            browser.sleep(3000);
            browser.driver.findElement(by.xpath("//nv-tree/div/nv-node[7]/div/div/span")).click();
            browser.sleep(1000);
            expect(browser.driver.findElement(by.xpath("//*[@aria-label='Cotton Fabric']")).isDisplayed()).toBe(true);
            expect(browser.driver.findElement(by.xpath("//*[@aria-label='Silk Fabric']")).isDisplayed()).toBe(true);
            expect(browser.driver.findElement(by.xpath("//*[@aria-label='Wool Fabric']")).isDisplayed()).toBe(true);
            browser.sleep(2000);
            browser.driver.findElement(by.xpath(".//button[@aria-label='Cancel']")).click();
            browser.sleep(2000);
            browser.driver.findElement(by.xpath("//*[@id='content']/div/div[1]/div/div[7]/button")).click();
            browser.sleep(1000);
            expect(browser.getCurrentUrl()).toEqual('http://localhost:3000/#!/');
        });
        it('should signup a new user, log them in, select business segment and redirecting to "/"', function () {
            page1 = require('./signup/busseg.po');
            page1.bussignin(testUser18);
            page1.form.password.sendKeys('12345678');
            page1.form.submit.click();
            browser.sleep(3000);
            browser.driver.findElement(by.xpath("//nv-tree/div/nv-node[8]/div/div/span")).click();
            browser.sleep(1000);
            expect(browser.driver.findElement(by.xpath("//*[@aria-label='Cotton Apparel']")).isDisplayed()).toBe(true);
            expect(browser.driver.findElement(by.xpath("//*[@aria-label='Silk Apparel']")).isDisplayed()).toBe(true);
            expect(browser.driver.findElement(by.xpath("//*[@aria-label='Wool Apparel']")).isDisplayed()).toBe(true);
            browser.sleep(2000);
            browser.driver.findElement(by.xpath(".//button[@aria-label='Cancel']")).click();
            browser.sleep(2000);
            browser.driver.findElement(by.xpath("//*[@id='content']/div/div[1]/div/div[7]/button")).click();
            browser.sleep(1000);
            expect(browser.getCurrentUrl()).toEqual('http://localhost:3000/#!/');
        });

        it('should signup a new user, log them in, select business segment and redirecting to "/"', function () {
            page1 = require('./signup/busseg.po');
            page1.bussignin(testUser18);
            page1.form.password.sendKeys('12345678');
            page1.form.submit.click();
            browser.sleep(3000);
            browser.driver.findElement(by.xpath("//nv-tree/div/nv-node[9]/div/div/span")).click();
            browser.sleep(1000);
            expect(browser.driver.findElement(by.xpath("//nv-tree/div/nv-node[9]/div[2]/nv-tree/div/nv-node[1]/div/div/md-checkbox")).isDisplayed()).toBe(true);
            expect(browser.driver.findElement(by.xpath("//*[@aria-label='Wheat']")).isDisplayed()).toBe(true);
            expect(browser.driver.findElement(by.xpath("//*[@aria-label='Pearl Millet']")).isDisplayed()).toBe(true);
            expect(browser.driver.findElement(by.xpath("//*[@aria-label='Sorghum Bicolor']")).isDisplayed()).toBe(true);
            expect(browser.driver.findElement(by.xpath("//*[@aria-label='Corn']")).isDisplayed()).toBe(true);
            expect(browser.driver.findElement(by.xpath("//*[@aria-label='Finger Millet']")).isDisplayed()).toBe(true);
            expect(browser.driver.findElement(by.xpath("//*[@aria-label='Barnyard Millet']")).isDisplayed()).toBe(true);
            expect(browser.driver.findElement(by.xpath("//*[@aria-label='Foxtail Millet']")).isDisplayed()).toBe(true);
            expect(browser.driver.findElement(by.xpath("//*[@aria-label='Kodo Millet']")).isDisplayed()).toBe(true);
            expect(browser.driver.findElement(by.xpath("//*[@aria-label='Little Millet']")).isDisplayed()).toBe(true);
            expect(browser.driver.findElement(by.xpath("//*[@aria-label='Proso Millet']")).isDisplayed()).toBe(true);
            expect(browser.driver.findElement(by.xpath("//*[@aria-label='Husk']")).isDisplayed()).toBe(true);
            browser.sleep(2000);
            browser.driver.findElement(by.xpath(".//button[@aria-label='Cancel']")).click();
            browser.sleep(2000);
            browser.driver.findElement(by.xpath("//*[@id='content']/div/div[1]/div/div[7]/button")).click();
            browser.sleep(1000);
            expect(browser.getCurrentUrl()).toEqual('http://localhost:3000/#!/');
        });
        it('should signup a new user, log them in, select business segment and redirecting to "/"', function () {
            page1 = require('./signup/busseg.po');
            page1.bussignin(testUser18);
            page1.form.password.sendKeys('12345678');
            page1.form.submit.click();
            browser.sleep(3000);
            browser.driver.findElement(by.xpath("//nv-tree/div/nv-node[10]/div/div/span")).click();
            browser.sleep(1000);
            expect(browser.driver.findElement(by.xpath("//*[@aria-label='Digestives']")).isDisplayed()).toBe(true);
            expect(browser.driver.findElement(by.xpath("//*[@aria-label='Health Drinks']")).isDisplayed()).toBe(true);
            expect(browser.driver.findElement(by.xpath("//*[@aria-label='Honey']")).isDisplayed()).toBe(true);
            expect(browser.driver.findElement(by.xpath("//*[@aria-label='Health and Wellness']")).isDisplayed()).toBe(true);
            expect(browser.driver.findElement(by.xpath("//*[@aria-label='Immune Enhancers']")).isDisplayed()).toBe(true);
            browser.sleep(2000);
            browser.driver.findElement(by.xpath(".//button[@aria-label='Cancel']")).click();
            browser.sleep(2000);
            browser.driver.findElement(by.xpath("//*[@id='content']/div/div[1]/div/div[7]/button")).click();
            browser.sleep(1000);
            expect(browser.getCurrentUrl()).toEqual('http://localhost:3000/#!/');
        });
        it('should signup a new user, log them in, select business segment and redirecting to "/"', function () {
            page1 = require('./signup/busseg.po');
            page1.bussignin(testUser18);
            page1.form.password.sendKeys('12345678');
            page1.form.submit.click();
            browser.sleep(3000);
            browser.driver.findElement(by.xpath("//nv-tree/div/nv-node[11]/div/div/span")).click();
            browser.sleep(1000);
            expect(browser.driver.findElement(by.xpath("//*[@aria-label='Biscuits And Cookies']")).isDisplayed()).toBe(true);
            expect(browser.driver.findElement(by.xpath("//*[@aria-label='Dairy']")).isDisplayed()).toBe(true);
            expect(browser.driver.findElement(by.xpath("//*[@aria-label='Masalas and Spices']")).isDisplayed()).toBe(true);
            expect(browser.driver.findElement(by.xpath("//*[@aria-label='Wheat Atta']")).isDisplayed()).toBe(true);
            expect(browser.driver.findElement(by.xpath("//*[@aria-label='Flours']")).isDisplayed()).toBe(true);
            expect(browser.driver.findElement(by.xpath("//*[@aria-label='Oils']")).isDisplayed()).toBe(true);
            expect(browser.driver.findElement(by.xpath("//*[@aria-label='Sooji and Dalia']")).isDisplayed()).toBe(true);
            expect(browser.driver.findElement(by.xpath("//*[@aria-label='Corn Flakes']")).isDisplayed()).toBe(true);
            expect(browser.driver.findElement(by.xpath("//*[@aria-label='Noodles']")).isDisplayed()).toBe(true);
            expect(browser.driver.findElement(by.xpath("//*[@aria-label='Candies and Chocolates']")).isDisplayed()).toBe(true);
            expect(browser.driver.findElement(by.xpath("//*[@aria-label='Sugar']")).isDisplayed()).toBe(true);
            expect(browser.driver.findElement(by.xpath("//*[@aria-label='Maida']")).isDisplayed()).toBe(true);
            expect(browser.driver.findElement(by.xpath("//*[@aria-label='Jaggery']")).isDisplayed()).toBe(true);
            expect(browser.driver.findElement(by.xpath("//*[@aria-label='Ice Creams']")).isDisplayed()).toBe(true);
            expect(browser.driver.findElement(by.xpath("//*[@aria-label='Sweets']")).isDisplayed()).toBe(true);
            expect(browser.driver.findElement(by.xpath("//*[@aria-label='Oats']")).isDisplayed()).toBe(true);
            expect(browser.driver.findElement(by.xpath("//*[@aria-label='Namkeen']")).isDisplayed()).toBe(true);
            expect(browser.driver.findElement(by.xpath("//*[@aria-label='Papads']")).isDisplayed()).toBe(true);
            expect(browser.driver.findElement(by.xpath("//*[@aria-label='Pickles']")).isDisplayed()).toBe(true);
            expect(browser.driver.findElement(by.xpath("//*[@aria-label='Jams']")).isDisplayed()).toBe(true);
            expect(browser.driver.findElement(by.xpath("//*[@aria-label='Sauces']")).isDisplayed()).toBe(true);
            expect(browser.driver.findElement(by.xpath("//*[@aria-label='Cashew']")).isDisplayed()).toBe(true);
            expect(browser.driver.findElement(by.xpath("//*[@aria-label='Dry Grapes (Raisins)']")).isDisplayed()).toBe(true);
            expect(browser.driver.findElement(by.xpath("//*[@aria-label='Almonds (Badam)']")).isDisplayed()).toBe(true);
            expect(browser.driver.findElement(by.xpath("//*[@aria-label='Dates']")).isDisplayed()).toBe(true);
            expect(browser.driver.findElement(by.xpath("//*[@aria-label='Apricot']")).isDisplayed()).toBe(true);
            expect(browser.driver.findElement(by.xpath("//*[@aria-label='Anjeer (Fig)']")).isDisplayed()).toBe(true);
            expect(browser.driver.findElement(by.xpath("//*[@aria-label='Pistachios']")).isDisplayed()).toBe(true);
            expect(browser.driver.findElement(by.xpath("//*[@aria-label='Walnuts']")).isDisplayed()).toBe(true);
            expect(browser.driver.findElement(by.xpath("//*[@aria-label='Prunes']")).isDisplayed()).toBe(true);
            expect(browser.driver.findElement(by.xpath("//*[@aria-label='Other Dry Fruits']")).isDisplayed()).toBe(true);
            browser.sleep(2000);
            browser.driver.findElement(by.xpath(".//button[@aria-label='Cancel']")).click();
            browser.sleep(2000);
            browser.driver.findElement(by.xpath("//*[@id='content']/div/div[1]/div/div[7]/button")).click();
            browser.sleep(1000);
            expect(browser.getCurrentUrl()).toEqual('http://localhost:3000/#!/');
        });
        it('should signup a new user, log them in, select business segment and redirecting to "/"', function () {
            page1 = require('./signup/busseg.po');
            page1.bussignin(testUser18);
            page1.form.password.sendKeys('12345678');
            page1.form.submit.click();
            browser.sleep(3000);
            browser.driver.findElement(by.xpath("//nv-tree/div/nv-node[12]/div/div/span")).click();
            browser.sleep(1000);
            expect(browser.driver.findElement(by.xpath("//*[@aria-label='Laundry']")).isDisplayed()).toBe(true);
            expect(browser.driver.findElement(by.xpath("//*[@aria-label='Kitchen and Dining']")).isDisplayed()).toBe(true);
            expect(browser.driver.findElement(by.xpath("//*[@aria-label='Repellents']")).isDisplayed()).toBe(true);
            expect(browser.driver.findElement(by.xpath("//*[@aria-label='Toilets and Surface Cleaners']")).isDisplayed()).toBe(true);
            expect(browser.driver.findElement(by.xpath("//*[@aria-label='Air Care and Cleaning']")).isDisplayed()).toBe(true);
            expect(browser.driver.findElement(by.xpath("//*[@aria-label='Detergents']")).isDisplayed()).toBe(true);
            expect(browser.driver.findElement(by.xpath("//*[@aria-label='Pet Care']")).isDisplayed()).toBe(true);
            expect(browser.driver.findElement(by.xpath("//*[@aria-label='Paper and Disposable']")).isDisplayed()).toBe(true);
            expect(browser.driver.findElement(by.xpath("//*[@aria-label='Pooja Needs']")).isDisplayed()).toBe(true);
            browser.sleep(2000);
            browser.driver.findElement(by.xpath(".//button[@aria-label='Cancel']")).click();
            browser.sleep(2000);
            browser.driver.findElement(by.xpath("//*[@id='content']/div/div[1]/div/div[7]/button")).click();
            browser.sleep(1000);
            expect(browser.getCurrentUrl()).toEqual('http://localhost:3000/#!/');
        });
        it('should signup a new user, log them in, select business segment and redirecting to "/"', function () {
            page1 = require('./signup/busseg.po');
            page1.bussignin(testUser18);
            page1.form.password.sendKeys('12345678');
            page1.form.submit.click();
            browser.sleep(3000);
            browser.driver.findElement(by.xpath("//nv-tree/div/nv-node[13]/div/div/span")).click();
            browser.sleep(1000);
            expect(browser.driver.findElement(by.xpath("//*[@aria-label='Cosmetics and Toiletries']")).isDisplayed()).toBe(true);
            expect(browser.driver.findElement(by.xpath("//*[@aria-label='Skin Care']")).isDisplayed()).toBe(true);
            expect(browser.driver.findElement(by.xpath("//*[@aria-label='Oral Care']")).isDisplayed()).toBe(true);
            expect(browser.driver.findElement(by.xpath("//*[@aria-label='Hair Care']")).isDisplayed()).toBe(true);
            expect(browser.driver.findElement(by.xpath("//*[@aria-label='Perfumes and Deodrants']")).isDisplayed()).toBe(true);
            browser.sleep(2000);
            browser.driver.findElement(by.xpath(".//button[@aria-label='Cancel']")).click();
            browser.sleep(2000);
            browser.driver.findElement(by.xpath("//*[@id='content']/div/div[1]/div/div[7]/button")).click();
            browser.sleep(1000);
            expect(browser.getCurrentUrl()).toEqual('http://localhost:3000/#!/');
        });
        it('should signup a new user, log them in, select business segment and redirecting to "/"', function () {
            page1 = require('./signup/busseg.po');
            page1.bussignin(testUser18);
            page1.form.password.sendKeys('12345678');
            page1.form.submit.click();
            browser.sleep(3000);
            browser.driver.findElement(by.xpath("//nv-tree/div/nv-node[14]/div/div/span")).click();
            browser.sleep(1000);
            expect(browser.driver.findElement(by.xpath("//*[@aria-label='Tea']")).isDisplayed()).toBe(true);
            expect(browser.driver.findElement(by.xpath("//*[@aria-label='Juices']")).isDisplayed()).toBe(true);
            expect(browser.driver.findElement(by.xpath("//*[@aria-label='Softdrinks']")).isDisplayed()).toBe(true);
            expect(browser.driver.findElement(by.xpath("//*[@aria-label='Coffee Beans']")).isDisplayed()).toBe(true);
            expect(browser.driver.findElement(by.xpath("//*[@aria-label='Coffee Powder']")).isDisplayed()).toBe(true);
            browser.sleep(2000);
            browser.driver.findElement(by.xpath(".//button[@aria-label='Cancel']")).click();
            browser.sleep(2000);
            browser.driver.findElement(by.xpath("//*[@id='content']/div/div[1]/div/div[7]/button")).click();
            browser.sleep(1000);
            expect(browser.getCurrentUrl()).toEqual('http://localhost:3000/#!/');
        });
    });
});

