'use strict';

// Init the application configuration module for AngularJS application
var ApplicationConfiguration;
ApplicationConfiguration = (function () {
	// Init module configuration options
	var applicationModuleName = 'nVipani';
    var applicationModuleVendorDependencies = ['ngResource', 'ngCookies', 'ngAnimate', 'ngSanitize', 'ngAria', 'ngMaterial', 'ngMessages', 'ui.router', 'ui.bootstrap', 'ui.select2', 'xeditable', 'checklist-model', 'LocalStorageModule', 'angularFileUpload', 'ui-notification', 'ng.deviceDetector', 'angular-uuid','angularUtils.directives.dirPagination'];

	// Add a new vertical module
	var registerModule = function (moduleName, dependencies) {
		// Create angular module
		angular.module(moduleName, dependencies || []);

		// Add the module to the AngularJS configuration file
		angular.module(applicationModuleName).requires.push(moduleName);
	};

	return {
		applicationModuleName: applicationModuleName,
		applicationModuleVendorDependencies: applicationModuleVendorDependencies,
		registerModule: registerModule
	};
})();
