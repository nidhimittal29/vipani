'use strict';

// Init the application configuration module for AngularJS application
var ApplicationConfiguration;
ApplicationConfiguration = (function () {
	// Init module configuration options
	var applicationModuleName = 'nVipani';
    var applicationModuleVendorDependencies = ['ngResource', 'ngCookies', 'ngAnimate', 'ngSanitize', 'ngAria', 'ngMaterial', 'ngMessages', 'ui.router', 'ui.bootstrap', 'ui.select2', 'xeditable', 'checklist-model', 'LocalStorageModule', 'angularFileUpload', 'ui-notification', 'ng.deviceDetector', 'angular-uuid'];

	// Add a new vertical module
	var registerModule = function (moduleName, dependencies) {
		// Create angular module
		angular.module(moduleName, dependencies || []);

		// Add the module to the AngularJS configuration file
		angular.module(applicationModuleName).requires.push(moduleName);
	};

	return {
		applicationModuleName: applicationModuleName,
		applicationModuleVendorDependencies: applicationModuleVendorDependencies,
		registerModule: registerModule
	};
})();

'use strict';

//Start by defining the main module and adding the module dependencies
angular.module(ApplicationConfiguration.applicationModuleName, ApplicationConfiguration.applicationModuleVendorDependencies);

// Setting HTML5 Location Mode
angular.module(ApplicationConfiguration.applicationModuleName).config(['$locationProvider','$mdThemingProvider','$mdDateLocaleProvider',
	function($locationProvider,$mdThemingProvider,$mdDateLocaleProvider) {
        $locationProvider.hashPrefix('!');
		/*$locationProvider.html5Mode({ enabled: true, requireBase: false }).hashPrefix('!');*/
    /*    $locationProvider.html5Mode(true);*/
		$mdThemingProvider.theme('default')
			.primaryPalette('blue')
			.accentPalette('blue-grey')
			.warnPalette('deep-orange')
			.backgroundPalette('grey');
		$mdDateLocaleProvider.formatDate = function(date) {
			return date ? moment(date).format('DD-MM-YYYY') : '';
		};

		$mdDateLocaleProvider.parseDate = function(dateString) {
			var m =moment.utc(dateString);
			return m && m.isValid() ? m.toDate() : new Date(NaN);
		};

	}

]);

//Then define the init function for starting up the application
angular.element(document).ready(function() {
	//Fixing facebook bug with redirect
	if (window.location.hash === '#_=_') window.location.hash = '#!';

	//Then init the app
	angular.bootstrap(document, [ApplicationConfiguration.applicationModuleName]);
});

'use strict';

// Use applicaion configuration module to register a new module
ApplicationConfiguration.registerModule('categories');
'use strict';

// Use applicaion configuration module to register a new module
ApplicationConfiguration.registerModule('companies');
'use strict';

// Use applicaion configuration module to register a new module
ApplicationConfiguration.registerModule('contacts');
'use strict';

// Use Applicaion configuration module to register a new module
ApplicationConfiguration.registerModule('core');
'use strict';

// Use applicaion configuration module to register a new module
ApplicationConfiguration.registerModule('groups');
'use strict';

// Use applicaion configuration module to register a new module
ApplicationConfiguration.registerModule('inventories');
'use strict';

// Use applicaion configuration module to register a new module
ApplicationConfiguration.registerModule('leads');

'use strict';

// Use applicaion configuration module to register a new module
ApplicationConfiguration.registerModule('messages');
'use strict';

// Use applicaion configuration module to register a new module
ApplicationConfiguration.registerModule('notifications');
'use strict';

// Use applicaion configuration module to register a new module
ApplicationConfiguration.registerModule('offers');
'use strict';

// Use applicaion configuration module to register a new module
ApplicationConfiguration.registerModule('orders');
'use strict';

// Use applicaion configuration module to register a new module
ApplicationConfiguration.registerModule('products');
'use strict';

// Use applicaion configuration module to register a new module
ApplicationConfiguration.registerModule('reports');
'use strict';

// Use applicaion configuration module to register a new module
ApplicationConfiguration.registerModule('todos');
'use strict';

// Use Applicaion configuration module to register a new module
ApplicationConfiguration.registerModule('users');
'use strict';

// Configuring the Articles module
angular.module('categories',[]).run(['Menus',
    function (Menus) {
        // Set top bar menu items
        /*Menus.addMenuItem('topbar', 'Categories', 'categories', 'dropdown', '/categories(/create)?');
         Menus.addSubMenuItem('topbar', 'categories', 'List Categories', 'categories');
         Menus.addSubMenuItem('topbar', 'categories', 'New Category', 'categories/create');*/
    }
]);

'use strict';

//Setting up route
angular.module('categories').config(['$stateProvider',
    function ($stateProvider) {
        // Categories state routing
        $stateProvider.
        state('listCategories', {
            url: '/categories',
            templateUrl: 'modules/categories/views/list-categories.client.view.html'
        }).
        state('createCategory', {
            url: '/categories/create',
            templateUrl: 'modules/categories/views/create-category.client.view.html'
        }).
        state('viewCategory', {
            url: '/categories/:categoryId',
            templateUrl: 'modules/categories/views/view-category.client.view.html'
        }).
        state('editCategory', {
            url: '/categories/:categoryId/edit',
            templateUrl: 'modules/categories/views/edit-category.client.view.html'
        });
    }
]);

'use strict';

// Categories controller
angular.module('categories').controller('CategoriesController', ['$scope', '$stateParams', '$location', 'Authentication', 'Categories',
    function ($scope, $stateParams, $location, Authentication, Categories) {
        $scope.authentication = Authentication;

        // Create new Category
        $scope.create = function () {
            // Create new Category object
            var category = new Categories({
                name: this.name
            });

            // Redirect after save
            category.$save(function (response) {
                $location.path('products/create');

                // Clear form fields
                $scope.name = '';
            }, function (errorResponse) {
                $scope.error = errorResponse.data.message;
            });
        };

        $scope.panelChange = function () {
            $location.path('products/create');

            // Clear form fields
            $scope.name = '';
        };

        // Remove existing Category
        $scope.remove = function (category) {
            if (category) {
                category.$remove();

                for (var i in $scope.categories) {
                    if ($scope.categories [i] === category) {
                        $scope.categories.splice(i, 1);
                    }
                }
            } else {
                $scope.category.$remove(function () {
                    $location.path('categories');
                });
            }
        };

        // Update existing Category
        $scope.update = function () {
            var category = $scope.category;

            category.$update(function () {
                $location.path('categories/' + category._id);
            }, function (errorResponse) {
                $scope.error = errorResponse.data.message;
            });
        };

        // Find a list of Categories
        $scope.find = function () {
            $scope.categories = Categories.query();
        };

        // Find existing Category
        $scope.findOne = function () {
            $scope.category = Categories.get({
                categoryId: $stateParams.categoryId
            });
        };
    }
]);

'use strict';

//Categories service used to communicate Categories REST endpoints
angular.module('categories').factory('Categories', ['$resource',
    function ($resource) {
        return $resource('categories/:categoryId', {
            categoryId: '@_id'
        }, {
            update: {
                method: 'PUT'
            }
        });
    }
]);

'use strict';

// Configuring the Articles module
angular.module('companies',[]).run(['Menus',
    function (Menus) {
        // Set top bar menu items
       /* Menus.addMenuItem('topbar', 'Companies', 'companies', 'dropdown', '/companies(/create)?');
        Menus.addSubMenuItem('topbar', 'companies', 'List Companies', 'companies');
        Menus.addSubMenuItem('topbar', 'companies', 'New Company', 'companies/create');*/
    }
]);

'use strict';

//Setting up route
angular.module('companies').config(['$stateProvider',
    function ($stateProvider) {
        // Companies state routing
        $stateProvider.
            state('listCompanies', {
                url: '/companies',
                templateUrl: 'modules/companies/views/list-companies.client.view.html'
            }).
            state('createCompany', {
                url: '/companies/create',
                templateUrl: 'modules/companies/views/create-company.client.view.html'
            }).
            state('viewCompany', {
                url: '/company/:companyName',
                templateUrl: 'modules/companies/views/view-company.client.view.html'
            }).
            state('editCompany', {
                url: '/companies/:companyId/edit',
                templateUrl: 'modules/companies/views/edit-company.client.view.html'
            });
    }
]);

'use strict';

// Companies controller
angular.module('companies').controller('CompaniesController', ['$scope', '$stateParams', '$location', 'Authentication', 'Companies','$http',
    function ($scope, $stateParams, $location, Authentication, Companies,$http) {
        $scope.authentication = Authentication;
$scope.contactExists=false;
        // Create new Company
        $scope.create = function () {
            // Create new Company object
            var company = new Companies({
                name: this.name
            });

            // Redirect after save
            company.$save(function (response) {
                $location.path('companies/' + response._id);

                // Clear form fields
                $scope.name = '';
            }, function (errorResponse) {
                $scope.error = errorResponse.data.message;
            });
        };

        // Remove existing Company
        $scope.remove = function (company) {
            if (company) {
                company.$remove();

                for (var i in $scope.companies) {
                    if ($scope.companies [i] === company) {
                        $scope.companies.splice(i, 1);
                    }
                }
            } else {
                $scope.company.$remove(function () {
                    $location.path('companies');
                });
            }
        };

        $scope.getBusinessCategories=function (company) {
            var categories=[];
            if(this.company){
               if(this.company.category.seller){
                   categories.push('Seller');
               }
               if(this.company.category.buyer){
                   categories.push('Buyer');
               }
               if(this.company.category.mediator){
                   categories.push('Mediator');
               }
            }
            if(categories.length>0){
                return categories.toString();
            }
        };

        //Create Contacts for the company

        $scope.createCompanyContacts=function (company) {


            $http.put('createContacts/' + company._id).then(function (response) {
                /*if (response._id) {
                    $location.path('orders' + '/' + response._id);
                } else {
                    $location.path('orders');
                }*/
            }, function (response) {
                $scope.error = response.data.message;
            });

        };

        // Update existing Company
        // Company Contact is present in the current organization.
      /*  $scope.getCompanyContactPresent=function (company){

        };*/

        $scope.update = function () {
            var company = $scope.company;

            company.$update(function () {
                $location.path('companies/' + company._id);
            }, function (errorResponse) {
                $scope.error = errorResponse.data.message;
            });
        };
        $scope.createOrder = function (categoryOffer) {
            var offer = categoryOffer ? categoryOffer : $scope.selOffer;
            $http.put('createOrder/' + offer._id, offer).then(function (response) {
                if (response.data._id) {
                    $location.path('orders' + '/' + response.data._id);
                } else {
                    $location.path('orders');
                }
            }, function (response) {
                $scope.error = response.data.message;
            });
        };

        $scope.orderProductCount=function(selOffer){
            $scope.selectedproducts=[];
            if(selOffer){
                $scope.selectedproducts =selOffer.products.filter(function (product) {
                    return product.selected ;
                });
                return $scope.selectedproducts.length>0?false:true;

            }
            if($scope.selectedproducts.length>1){
                return false;
            }else{
                return true;
            }
        };
        // Find a list of Companies
        $scope.find = function () {
            $scope.companies = Companies.query();
        };

        // Find existing Company
        $scope.findOne = function () {
            $scope.company = Companies.get({
                companyId: $stateParams.companyId
            });
        };
        $scope.findOneCompany = function () {
           /* $scope.company = Companies.get({
                name: $stateParams.name
            });*/
            $http.get('company/' + $stateParams.companyName).then(function (response) {
                $scope.companyOffers = response.data.offers;
                $scope.company = response.data.company;
               if($scope.authentication && $scope.authentication.token && $scope.authentication.token.length!==0)
                   $http.get('contactscheck/' + $scope.company._id).then(function (response) {
                       if (response.data.contactExists) {
                           $scope.contactExists = response.data.contactExists;
                   }else{
                       $scope.contactExists=false;
                   }
               });

            });
        };
    }
]);

'use strict';

//Companies service used to communicate Companies REST endpoints
angular.module('companies').factory('Companies', ['$resource',
    function ($resource) {
        return $resource('companies/:companyId', {
            companyId: '@_id'
        }, {
            update: {
                method: 'PUT'
            }
        });
    }
]);

'use strict';

// Configuring the Articles module
angular.module('contacts',[]).run(['Menus',
	function(Menus) {
		// Set top bar menu items
		Menus.addMenuItem('topbar', 'Contacts', 'contacts', 'contacts');
		//Menus.addSubMenuItem('topbar', 'contacts', 'List Contacts', 'contacts');
		//Menus.addSubMenuItem('topbar', 'contacts', 'New Contact', 'contacts/create');
	}
]);

'use strict';

//Setting up route
angular.module('contacts').config(['$stateProvider',
	function($stateProvider) {
		// Contacts state routing
		$stateProvider.
		state('listContacts', {
			url: '/contacts',
			templateUrl: 'modules/contacts/views/contact/list-contacts.client.viewnew.html',
			ncyBreadcrumb: {
				label: 'listContacts'
			}
		}).
		state('createContact', {
			url: '/contacts/create',
			templateUrl: 'modules/contacts/views/contact/create-contact.client.view.html'
		}).
		state('viewContact', {
			url: '/contacts/:contactId',
			templateUrl: 'modules/contacts/views/contact/view-contact.client.view.html',
			ncyBreadcrumb: {
				label: 'ViewContact'
			}
		}).
		state('editContact', {
			url: '/contacts/:contactId/edit',
			templateUrl: 'modules/contacts/views/contact/edit-contacts.client.viewnew.html',
			ncyBreadcrumb: {
				label: 'editContact'
			}
		})/*.
		state('editGroup', {
			url: '/groups/:groupId/edit',
			templateUrl: 'modules/contacts/views/groups/edit-group.client.view.html'
		})*/;
	}
]);

'use strict';

//var logger  = require('../../../lib/log').getLogger('CONTACTS-CONTROLLER', 'DEBUG');

// Contacts controller
var app = angular.module('contacts');
//var jQuery = require('jquery');

app.controller('ContactsController', ['$scope', '$stateParams', '$location', '$window', 'Authentication', 'Contacts', 'Groups', 'FileUploader', '$timeout', '$document','verifyDelete','panelTrigger',
    function ($scope, $stateParams, $location, $window, Authentication, Contacts, Groups, FileUploader, $timeout, $document,verifyDelete ,panelTrigger ) {
        $scope.authentication = Authentication;
        $scope.contactImageURL = 'modules/contacts/img/default.png';

     $scope.toggleSideNavigation = function (id,val) {

            if(val === ''){
                $timeout(function () {
                    panelTrigger(id).open().then(function () {
                        console.log('Trigger is done');
                    });
                });
            }else{
                val=val;
            }
            if (val === 'toggle') {
                panelTrigger(id).toggle().then(function () {
                    console.log('Trigger is done');
                });
            }else if (val === 'open') {
                panelTrigger(id).open().then(function () {
                    console.log('Trigger is done');
                });
            }else if (val === 'close') {
                panelTrigger(id).close().then(function () {
                    console.log('Trigger is done');
                });
            }else if (val === 'isOpen') {
                panelTrigger(id).isOpen().then(function () {
                    console.log('Trigger is done');
                });
            }
        };


        $scope.selected = [];
        $scope.searchText = '';
        $scope.phones = [{
            phoneNumber: '',
            phoneType: 'Mobile',
            primary: true
        }];

        $scope.phoneTypes = ['Mobile', 'Home', 'Work', 'Other'];
        $scope.emailTypes = ['Work', 'Personal', 'Other'];

        $scope.addressTypes = ['Billing', 'Shipping', 'Receiving', 'Invoice'];
        $scope.emails = [{
            email: '',
            emailType: 'Work',
            primary: true
        }];
        $scope.addresses = [{
            addressLine: '',
            city: '',
            state: '',
            country: '',
            pinCode: '',
            addressType: 'Billing',
            primary: true
        }];
        $scope.error = null;
        $scope.selectedGroup = '';
        $scope.AllContactsString = 'All Contacts';
        $scope.allContacts = true;
        // Create new Contact
        $scope.create = function () {
            // Create new Contact object
            var contact = new Contacts({
                firstName: this.firstName,
                middleName: this.middleName,
                lastName: this.lastName,
                contactImageURL: this.contactImageURL,
                companyName: this.companyName,
                phones: this.phones,
                emails: this.emails,
                addresses: this.addresses
            });

            contact.phones=contact.phones.filter(function (phones) {
                return phones.phoneNumber.length>0;
            });

            contact.emails=contact.emails.filter(function (emails) {
                return emails.email.length>0;
            });

            contact.addresses=contact.addresses.filter(function (addresses) {
                return !(addresses.addressLine === '' &&
                    addresses.city === '' &&
                    addresses.state === '' &&
                    addresses.country === '' &&
                    addresses.pinCode === '');
            });
            if ($scope.selectedGroup.length !== 0) {
                contact.groups = [];
                contact.groups.push($scope.selectedGroup._id);
            }
            // Redirect after save
            contact.$save(function (response) {
                $location.path('contacts');

                // Clear form fields
                $scope.clearFields();

                //update current list
                $scope.contacts.push(contact);
                $scope.selcontact = contact;
                $scope.updatecontact = angular.copy(contact);
                $scope.panelChange($scope.selcontact, '');
                $scope.contact = contact;

            }, function (errorResponse) {
                $scope.error = errorResponse.data.message;
            });


        };

        $scope.reset = function (form) {
            $scope.error = null;
            if (form) {
                form.$setPristine();
                //form.$setUntouch  ed();
            }

        };
        $scope.clearFields = function () {
            $scope.firstName = '';
            $scope.lastName = '';
            $scope.companyName = '';
            $scope.middleName = '';
            $scope.phones = [{
                phoneNumber: '',
                phoneType: 'Mobile',
                primary: true
            }];
            $scope.emails = [{
                email: '',
                emailType: 'Work',
                primary: true
            }];
            $scope.addresses = [{
                addressLine: '',
                city: '',
                state: '',
                country: '',
                pinCode: '',
                addressType: 'Billing',
                primary: true
            }];
            $scope.error = null;
        };

        // Remove group
        $scope.uploader = new FileUploader({
            headers: {'token': $scope.token},
            url: 'contacts/contactPicture'
        });

        $scope.uploader.filters.push({
            name: 'imageFilter',
            fn: function (item, options) {

                var lblError = document.getElementById('lblError');
                var type = '|' + item.type.slice(item.type.lastIndexOf('/') + 1) + '|';
                var validError= '|jpg|png|jpeg|bmp|gif|'.indexOf(type) !== -1;
                lblError.innerHTML='';
                if(!validError) {
                    lblError.innerHTML = 'Please upload files having extensions:jpg, png, jpeg, bmp, gif <b> .Invalid type:'+item.type;
                }else if(validError && item.size >1024 * 1024 * 5){
                    $scope.contactImageURL='modules/contacts/img/default.png';
                    lblError.innerHTML = 'Please upload files size should be less than  5MB <b> .Current File size:'+item.size;
                }
                return validError;
            }
        });

        // Called after the user selected a new picture file
        $scope.uploader.onAfterAddingFile = function (fileItem) {
            if ($window.FileReader) {
                var fileReader = new FileReader();
                fileReader.readAsDataURL(fileItem._file);


                fileReader.onload = function (fileReaderEvent) {
                    $timeout(function () {
                        $scope.contactImageURL = fileReaderEvent.target.result;
                    }, 0);
                };
            }
        };

        // Called after the user has successfully uploaded a new picture
        $scope.uploader.onSuccessItem = function (fileItem, response, status, headers) {
            // Show success message
            $scope.imgsuccess = true;
            var imageURLStr = response.contactImageURL;
            $scope.contactImageURL = imageURLStr.replace(/"/g, '');
            // Clear upload buttons
            $scope.cancelUpload();
        };

        // Called after the user has failed to uploaded a new picture
        $scope.uploader.onErrorItem = function (fileItem, response, status, headers) {
            // Clear upload buttons
            $scope.cancelUpload();

            // Show error message
            $scope.imgerror = response.message;
        };
        $scope.removeGroup = function (group) {
            var i = 0;
            verifyDelete(group).then(function() {
            $scope.updateGroup.$remove(function (result) {
                for (i in $scope.groups) {
                    if ($scope.groups [i]._id === $scope.selectedGroup._id) {
                        $scope.groups.splice(i, 1);
                        $scope.showEdit = false;
                        $scope.showView = false;
                        $scope.viewAll = true;
                        break;
                    }
                }
                if ($scope.groups.length > 0) {
                    if (i === '0') {
                        $scope.selectedGroup = $scope.groups[i];
                        $scope.updateGroup = angular.copy($scope.groups[i]);

                    } else if (i <= $scope.groups.length) {

                        $scope.selectedGroup = $scope.groups[i - 1];
                        $scope.updateGroup = angular.copy($scope.groups[i - 1]);

                    } else {

                        $scope.selectedGroup = $scope.groups[i + 1];
                        $scope.updateGroup = angular.copy($scope.groups[i + 1]);


                    }
                    $scope.showEdit = false;

                    $scope.showAddToGroups = false;
                    $scope.showEditGroup = false;
                    $scope.showView = false;
                    $scope.viewAll = true;
                } else {
                    $scope.allContacts = true;
                    $scope.selectedGroup = '';
                    $scope.find();
                }
            });

            });

        };

        // Remove existing Contact
        $scope.remove = function (contact) {

            if ($scope.selcontact) {
                var i = 0;
                //$scope.selcontact.$remove();
                $scope.updatecontact.$remove();
                for (i in $scope.contacts) {
                    if ($scope.contacts [i] === contact) {
                        $scope.contacts.splice(i, 1);
                        /*	$scope.selcontact = $scope.contacts[i];
                         $scope.showEdit=false;
                         $scope.showView=true;*/
                        break;
                    }
                }
                if ($scope.contacts.length > 0) {
                    if (i === '0') {
                        $scope.selcontact = $scope.contacts[i];
                        $scope.updatecontact = angular.copy($scope.contacts[i]);

                    } else if (i <= $scope.contacts.length) {

                        $scope.selcontact = $scope.contacts[i - 1];
                        $scope.updatecontact = angular.copy($scope.contacts[i - 1]);

                    } else {

                        $scope.selcontact = $scope.contacts[i + 1];
                        $scope.updatecontact = angular.copy($scope.contacts[i + 1]);


                    }
                    $scope.showEdit = false;

                    $scope.showAddToGroups = false;
                    $scope.showEditGroup = false;
                    $scope.showView = false;
                    $scope.viewAll = true;
                }

            } else {
                $scope.selcontact.$remove(function () {
                    $location.path('contacts');
                });
            }

        };
        // Find a list of Groups
        $scope.findGroups = function () {
            $scope.showEditGroup = false;
            $scope.groups = Groups.query();
        };


        $scope.changeSelGroup = function (group, editgroup) {
            $scope.selectedGroup = group;
            $scope.updateGroup = angular.copy(group);
            //$scope.selectedGroup = group;
            $scope.showEditGroup = false;
            $scope.find();
            if (group.length !== 0) {
                $scope.allContacts = false;
            } else {
                $scope.allContacts = true;
            }
            if (editgroup === true) {
                $scope.showEditGroup = true;
            } else {
                $scope.showEditGroup = false;
            }
        };
        $scope.updateGroupval = function () {
            var group = $scope.updateGroup;

            group.$update(function () {
                for (var j in $scope.groups) {
                    if ($scope.groups [j]._id === group._id) {

                        $scope.groups[j] = group;
                        $scope.selectedGroup = group;
                        $scope.updateGroup = angular.copy(group);
                        break;
                    }

                }

                $scope.showEdit = false;

                $scope.showAddToGroups = false;
                $scope.showAddToGroups = false;
                $scope.showEditGroup = false;
                $scope.showView = false;
                $scope.viewAll = true;

            }, function (errorResponse) {
                $scope.error = errorResponse.data.message;
            });
        };
        $scope.updateGroupSel = function (group) {
            $scope.updateGroup = group;
            $scope.selectedGroup = group;
            $scope.showEditGroup = false;
            $scope.find();
            if (group.length !== 0) {
                $scope.allContacts = false;
            } else {
                $scope.allContacts = true;
            }
        };

        $scope.filterByDisplayName = function (contacts, typedValue) {
            if (typedValue !== undefined) {
                return contacts.filter(function (contact) {
                    var firstName = contact.firstName.indexOf(typedValue) !== -1;
                    var lastName = contact.lastName.indexOf(typedValue) !== -1;
                    var companyName = contact.lastName.indexOf(typedValue) !== -1;

                    return firstName || lastName || companyName;
                });
            } else {
                return true;
            }
        };
        //$scope.addToGroup = (selcontact)
        // Update existing Contact
        $scope.update = function (updatecontact, selcontact, groupval) {
         /*   $scope.updatecontact.contactImageURL = $scope.contactImageURL;*/

            updatecontact.phones=updatecontact.phones.filter(function (phones) {
                return phones.phoneNumber.length>0;
            });

            updatecontact.emails=updatecontact.emails.filter(function (emails) {
                return emails.email.length>0;
            });

            updatecontact.addresses=updatecontact.addresses.filter(function (addresses) {
                return !(addresses.addressLine === '' &&
                addresses.city === '' &&
                addresses.state === '' &&
                addresses.country === '' &&
                addresses.pinCode === '');
            });

            var contact = angular.copy(updatecontact);

            contact.$update(function () {
                var j = 0;
                var removedGroup = true;
                for (j in $scope.contacts) {
                    if ($scope.contacts [j]._id === selcontact._id) {
                        if ($scope.allContacts) {
                            removedGroup = false;
                            //$scope.contacts.splice(j, 1);
                            $scope.contacts[j] = contact;
                            $scope.selcontact = contact;
                            $scope.updateContact = contact;
                            $scope.updatecontact = angular.copy(contact);
                        } else if ($scope.selectedGroup._id && ($scope.updatecontact.groups.indexOf($scope.selectedGroup._id) >= 0)) {
                            removedGroup = false;
                            //$scope.contacts.splice(j, 1);
                            $scope.contacts[j] = contact;
                            $scope.selcontact = contact;
                            $scope.updateContact = contact;
                            $scope.updatecontact = angular.copy(contact);

                        } else if (removedGroup) {
                            $scope.contacts.splice(j, 1);
                            if ($scope.contacts.length > 0) {
                                //
                                if (j === '0') {
                                    $scope.selcontact = $scope.contacts[j];
                                    $scope.updatecontact = angular.copy($scope.contacts[j]);

                                } else if (j <= $scope.contacts.length) {

                                    $scope.selcontact = $scope.contacts[j - 1];
                                    $scope.updatecontact = angular.copy($scope.contacts[j - 1]);

                                } else {

                                    $scope.selcontact = $scope.contacts[j + 1];
                                    $scope.updatecontact = angular.copy($scope.contacts[j + 1]);


                                }
                                $scope.showEdit = false;
                                $scope.showAddToGroups = false;
                                $scope.showAddToGroups = false;
                                $scope.showView = false;
                                $scope.viewAll = true;
                            }

                        }
                        break;
                    }

                }

                $location.path('contacts');
                /*	if($scope.groupcontact){
                 $scope.viewAll=false;
                 $scope.showEdit=true;
                 }else{
                 $scope.showEdit=false;
                 $scope.viewAll=true;
                 }*/

                if (!groupval) {
                    $scope.panelChange(contact, '');
                }
            }, function (errorResponse) {

                $scope.error = errorResponse.data.message;
            });


        };


        $scope.updateStatus = function (updatecontact, selcontact) {
            var contact = angular.copy(updatecontact);
            contact.disabled = !contact.disabled;
            contact.$update(function () {
                var j = 0;
                var removedGroup = true;
                for (j in $scope.contacts) {
                    if ($scope.contacts [j]._id === selcontact._id) {
                        if ($scope.allContacts) {
                            removedGroup = false;
                            $scope.contacts.splice(j, 1);
                            $scope.contacts.push(contact);
                            $scope.selcontact = contact;
                            $scope.updateContact = contact;
                            $scope.updatecontact = angular.copy(contact);
                        } else if ($scope.selectedGroup._id && ($scope.updatecontact.groups.indexOf($scope.selectedGroup._id) >= 0)) {
                            removedGroup = false;
                            $scope.contacts.splice(j, 1);
                            $scope.contacts.push(contact);
                            $scope.selcontact = contact;
                            $scope.updateContact = contact;
                            $scope.updatecontact = angular.copy(contact);

                        } else if (removedGroup) {
                            $scope.contacts.splice(j, 1);
                            if ($scope.contacts.length > 0) {
                                //
                                if (j === '0') {
                                    $scope.selcontact = $scope.contacts[j];
                                    $scope.updatecontact = angular.copy($scope.contacts[j]);

                                } else if (j <= $scope.contacts.length) {

                                    $scope.selcontact = $scope.contacts[j - 1];
                                    $scope.updatecontact = angular.copy($scope.contacts[j - 1]);

                                } else {

                                    $scope.selcontact = $scope.contacts[j + 1];
                                    $scope.updatecontact = angular.copy($scope.contacts[j + 1]);


                                }
                                $scope.showEdit = false;

                                $scope.showAddToGroups = false;
                                $scope.showAddToGroups = false;
                                $scope.showView = true;
                            }

                        }
                        break;
                    }

                }
                $location.path('contacts');
                $scope.panelChange(contact, '');
            }, function (errorResponse) {
                $scope.error = errorResponse.data.message;
            });


        };

        // Find a list of Contacts
        $scope.find = function () {

            var results = Contacts.query();
            $scope.contacts = [];
            results.$promise.then(function (result) {
                angular.forEach(result, function (e) {
                    if ((e.nVipaniUser === null || !( (e.nVipaniRegContact !== null && e.nVipaniRegContact === true && (e.nVipaniUser === e.user || e.nVipaniUser === e.user._id) ))) && ($scope.selectedGroup === '' || e.groups.indexOf($scope.selectedGroup._id) >= 0)) {
                        $scope.contacts.push(e);
                    }
                });
                $scope.selcontact = $scope.contacts[0];
                $scope.updatecontact = angular.copy($scope.selcontact);
            });
        };

        $scope.showGroupContacts = function () {
            return ($scope.contacts !== undefined || $scope.contacts.length > 0);
        };

        $scope.searchFilter = function (item) {
            var re = new RegExp($scope.searchText, 'i');
            var phone=[];
            var email=[];
            if(item.phones.length>0) {
                phone = item.phones.filter(function (eachphone) {
                    return eachphone.phoneNumber.indexOf($scope.searchText) !== -1;
                });
            }
            if(item.emails.length>0){
            email=item.emails.filter(function (eachemail) {
                return eachemail.email.indexOf($scope.searchText) !== -1;
            });
            }
            return !$scope.searchText || re.test(item.firstName) || re.test(item.lastName) || re.test(item.middleName) || re.test(item.firstName +' '+item.middleName + ' '+item.lastName) ||re.test(item.companyName)|| (phone.length>0 && re.test(phone[0].phoneNumber)) ||(email.length>0 && re.test(email[0].email));
        };
        $scope.filterGroup = function (item) {
            var re = new RegExp($scope.searchText, 'i');
            return !$scope.searchText || re.test(item.name);
        };
        $scope.showOnlyGroupEdit = function () {
            return ($scope.group !== undefined && $scope.Edit.length > 0);
        };
        $scope.showSelection = function () {
            return $scope.selcontact !== undefined;
        };
        // Find existing Contact
        $scope.findOne = function () {
            $scope.contact = Contacts.get({
                contactId: $stateParams.contactId
            });

           /* $scope.contact.$promise.then(function (result) {
                $scope.contactImageURL = result.contactImageURL;
            });*/
        };
        $scope.findOneGroups = function (id) {
            $scope.group = Groups.get({
                groupId: id
            });
        };
        $scope.findContatById = function (id) {
            $scope.contact = Contacts.get({
                contactId: id
            });
        };
        $scope.groupcreate = function () {
            // Create new Group object
            $scope.error = null;
            var group = new Groups({
                name: this.name,
                description: this.description
            });
            // Redirect after save
            group.$save(function (response) {
                $location.path('contacts');

                // Clear form fields
               /* $scope.clearFields();*/

                //update current list
                $scope.allContacts = false;
                $scope.groups.push(response);
                $scope.selectedGroup = response;
                $scope.panelChange('', '');
                $scope.contacts = [];
                /*$location.path('contacts');*/
            }, function (errorResponse) {
                $scope.error = errorResponse.data.message;
            });

        };
        $scope.addPhone = function (phonesArray) {
            phonesArray.push({
                phoneNumber: '',
                phoneType: 'Mobile',
                primary: false
            });
        };

        $scope.addEmail = function (emailsArray) {
            emailsArray.push({
                email: '',
                emailType: 'Work',
                primary: false
            });
        };

        $scope.addAddress = function (addressesArray) {
            addressesArray.push({
                addressLine: '',
                city: '',
                state: '',
                country: '',
                pinCode: '',
                addressType: 'Billing',
                primary: false
            });
        };
        $scope.changeSelContact = function (contact) {
            $scope.selcontact = contact;
            /*$scope.like=!$scope.like;*/
            $scope.updatecontact = angular.copy(contact);
        };

        $scope.showEdit = false;
        $scope.showAdd = false;
        $scope.showView = false;
        $scope.showAddToGroups = false;
        $scope.showAddGroup = false;
        $scope.showEditGroup = false;

        $scope.panelChange = function (contact, changeStr) {
            $scope.showEdit = false;
            $scope.showAdd = false;
            $scope.showAddToGroups = false;
            $scope.showAddGroup = false;
            $scope.showEditGroup = false;
            $scope.showView = false;
            $scope.viewAll = false;
            $scope.error = null;
            $scope.contactImageURL = 'modules/contacts/img/default.png';
            if (contact !== null && angular.isObject(contact)) {
                if (typeof changeStr === 'string') {
                    if (changeStr === 'edit') {
                        $scope.showEdit = true;
                        /*	if(contact instanceof Contact){
                         $scope.
                         }*/
                        /*	$scope.showEditGroup = false;*/
                    } else if (changeStr === 'add') {
                        $scope.showAdd = true;
                        $scope.showEditGroup = false;
                    } else if (changeStr === 'addToGroups') {
                        $scope.showAddToGroups = true;
                        /*$scope.showEditGroup = false;*/
                    }/*else if (changeStr === 'editToGroups') {
                     if($scope.group){
                     $scope.showEditGroup = true;
                     }
                     }*/ else if (changeStr === 'group') {
                        $scope.showAddGroup = true;
                        /*$scope.showEditGroup = false;*/
                    } else if (changeStr === 'editgroup') {
                        /*	$scope.showAddGroup = false;*/
                        $scope.showEditGroup = true;
                        //$scope.selectedGroup=contact;
                        $scope.updategroup = angular.copy(contact);
                    } else {
                        $scope.viewAll = true;

                    }

                } else {
                    $scope.viewAll = true;

                }
            } else {
                if (changeStr === 'add') {
                    $scope.showAdd = true;
                } else if (changeStr === 'addToGroups') {
                    $scope.showAddToGroups = true;
                } else if (changeStr === 'group') {
                    $scope.showAddGroup = true;
                } else {

                    $scope.viewAll = true;

                }

            }
        };

        $scope.group = false;
        $scope.allContacts = true;

        $scope.contactfullName = function (selectContact) {
            return selectContact.firstName + ' ' + selectContact.middleName + ' ' + selectContact.lastName;

        };

        $scope.contactDetails = function (selectContact) {
            var details = '';
            if (selectContact.companyName.length > 0) {
                details = selectContact.companyName;
            }
            return details;

        };

        //copies selContact into copySelContact
        $scope.createCopy = function (selcontact) {
            $scope.copySelContact = angular.copy(selcontact);
            $scope.updateContact = angular.copy(selcontact);
        };
        /*	$scope.isFirstNameRequired = function (firstname) {
         if ($scope.firstName) {
         return false;
         } else {
         if (!$scope.error) {
         $scope.error = null;
         $scope.error = '* FirstName is must field for the contact creation';
         }
         return true;
         }
         };*/

        $scope.isOneContactRequired = function () {
            var valid = false;
            for (var i in $scope.emails) {
                if ($scope.emails[i].email && $scope.emails[i].email.length > 0) {
                    valid = true;
                    break;
                }
            }

            for (var j in $scope.phones) {
                if (valid === true || $scope.phones[j].phoneNumber && $scope.phones[j].phoneNumber.length > 0) {
                    valid = true;
                    break;
                }
            }
            $scope.fieldValid = !valid;
            if (!(valid)) {
                $scope.error = '*Email or phone is required for contact creation';
            }
            /*else if($scope.firstName==undefined ||  $scope.firstName .length<=0){
             valid=false;
             $scope.error=null;
             $scope.error="* FirstName is must field for the contact creation" ;
             }else{
             if(valid) {
             $scope.error = null;
             }
             }*/
            return !(valid);
        };

        $scope.isOneContactRequiredUpdate = function () {
            var valid = false;
            for (var i in $scope.updatecontact.emails) {
                if ($scope.updatecontact.emails[i].email.length > 0) {
                    valid = true;
                    break;
                }
            }

            for (var j in $scope.updatecontact.phones) {
                if (valid === true || ($scope.updatecontact.phones[j].phoneNumber && $scope.updatecontact.phones[j].phoneNumber.length>0)) {
                    valid = true;
                    break;
                }
            }
            $scope.fieldValid = !valid;
            if (!(valid)) {
                $scope.error = '*Email or phone is required for contact creation';
            }
            return !(valid);
        };

        //reverts the changes of selcontact to its version in copyselContact
        $scope.revert = function () {
            for (var i in $scope.contacts) {
                if ($scope.contacts [i] === $scope.selcontact) {
                    $scope.contacts [i] = $scope.copySelContact;
                    //$scope.contacts[i]=$scope.updateContact;
                    $scope.selcontact = $scope.contacts [i];
                    $scope.updatecontact = angular.copy($scope.contacts [i]);
                    break;
                }
            }
        };




        $scope.predicate = 'firstName';


       /* $scope.$on('$viewContentLoaded', function() {
            //call it here
            $scope.pageloaded = true;
            alert("call on image load");
        });*/

        $scope.$on('initialised', function() {
            //call it here
            $scope.pageloaded = true;

        });





        /*$scope.viewContactDetails = $window.innerWidth > 768;
         $scope.viewGroups = $window.innerWidth > 1200;

         $window.onresize = function(event) {
         $scope.viewContactDetails = $window.innerWidth > 768;
         $scope.viewGroups = $window.innerWidth >	 1200;
         };

         $scope.toggleView = function(){
         if($window.innerWidth < 768){
         $scope.viewContactDetails = !$scope.viewContactDetails;
         }
         };

         $scope.toggleGroupView = function(){
         if($window.innerWidth < 1200){
         $scope.viewGroups = !$scope.viewGroups;
         }
         };*/

    }
]);

/*app.controller('ContactsViewController'[$scope,Contacts,Group,function($scope,Contacts,Group){

}]);*/
app.directive('initialisation',['$rootScope',function($rootScope) {
    return {
        restrict: 'A',
        link: function($scope) {
            var to;
            var listener = $scope.$watch(function() {
                clearTimeout(to);
                to = setTimeout(function () {
                    console.log('initialised');
                    listener();
                    $rootScope.$broadcast('initialised');
                }, 30);
            });
        }
    };
}]);


app.directive('editContact', function () {
    return {
        restrict: 'E',
        templateUrl: 'modules/contacts/views/contact/edit-contact.client.view.html'
    };
});

app.directive('editContactNew', function () {
    return {
        restrict: 'E',
        templateUrl: 'modules/contacts/views/contact/edit-contacts.client.viewnew.html'
    };
});

app.directive('editContactGroup', function () {
    return {
        restrict: 'E',
        templateUrl: 'modules/contacts/views/groups/edit-contact-groups.client.view.html'
    };
});

app.directive('viewContact', function () {
    return {
        restrict: 'E',
        templateUrl: 'modules/contacts/views/contact/view-contact.client.view.html'
    };
});

app.directive('listContact', function () {
    return {
        restrict: 'E',
        templateUrl: 'modules/contacts/views/contact/list-contacts.client.view.html'
    };
});

app.directive('addContact', function () {
    return {
        restrict: 'E',
        templateUrl: 'modules/contacts/views/contact/create-contact.client.view.html'
    };
});

app.directive('addContactNew', function () {
    return {
        restrict: 'E',
        templateUrl: 'modules/contacts/views/contact/create-contact.client.viewnew.html'
    };
});

app.directive('addGroupContact', function () {
    return {
        restrict: 'E',
        templateUrl: 'modules/contacts/views/groups/create-group.client.view.html'
    };
});

app.directive('editGroup', function () {
    return {
        restrict: 'E',
        templateUrl: 'modules/contacts/views/groups/edit-group.client.view.html'
    };
});
app.directive('viewGroup', function () {
    return {
        restrict: 'E',
        templateUrl: 'modules/contacts/views/groups/view-group.client.view.html'
    };
});

app.directive('viewAlls', function () {
    return {
        restrict: 'E',
        templateUrl: 'modules/contacts/views/contact/view-contacts.client.view.html'
    };
});
app.directive('onlyDigits', function () {
    return {
        restrict: 'A',
        require: '?ngModel',
        link: function (scope, element, attrs, ngModel) {
            if (!ngModel) return;
            ngModel.$parsers.unshift(function (inputValue) {
                var digits = inputValue.split('').filter(function (s) {
                    return (!isNaN(s) && s !== ' ');
                }).join('');
                ngModel.$viewValue = digits;
                ngModel.$render();
                return digits;
            });
        }
    };
});


app.directive('nanuEmail', function () {
    return {
        restrict: 'A',
        require: 'ngModel',
        link: function (scope, elm, attrs, model) {
            //change this:
            var EMAIL_REGEXP = /^[_a-z0-9]+(\.[_a-z0-9]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,4})$/;
            var emailValidator = function (value) {
                if (!value || EMAIL_REGEXP.test(value)) {
                    model.$setValidity('email', true);
                    return value;
                } else {
                    model.$setValidity('email', false);
                    return undefined;
                }
                model.$parsers.push(emailValidator);
                model.$formatters.push(emailValidator);
            };
        }
    };
});

app.directive('scrollToItem', function () {
    return {
        restrict: 'A',
        scope: {
            scrollTo: '@'
        },
        link: function (scope, elm, attr) {

            elm.on('click', function () {
                angular.element('html,body').animate({scrollTop: angular.element(scope.scrollTo).offset().top}, 'slow');
            });
        }
    };
});


/*app.directive('limitTo', [function() {
    return {
        restrict: 'AE',
        link: function(scope, elem, attrs,ngModel) {
            var limit = parseInt(attrs.limitTo);
            angular.element(elem).on('keydown', function(event) {
                var s = 223;
                var a = 229;

                var charCode = (event.which) ? event.which : event.keyCode;
                alert(charCode);
                if(charCode === 8 || charCode === 46){

                    return true;
                }else if (this.value.length >limit) {

                    return false;
                }else{

                    return true;
                }
            });

           /!* function isNumber(evt) {
                evt = (evt) ? evt : window.event;
                var charCode = (evt.which) ? evt.which : evt.keyCode;
                if (charCode > 31 && (charCode < 48 || charCode > 57)) {
                    return false;
                }
                return true;
            }*!/
        }
    };
}]);*/
/*app.directive('regExpRequire', function() {

    var regexp;
    return {
        restrict: 'A',
        link: function(scope, elem, attrs) {
            regexp = eval(attrs.regExpRequire);

            var char;
            elem.on('keypress', function(event) {
                char = String.fromCharCode(event.which)
                if(!regexp.test(elem.val() + char))
                    event.preventDefault();
            })
        }
    };

});*/

app.directive('limitTo1',function () {
    return {
        restrict: 'AE',
        scope: {
            watched: '=',
            val: '='
        },
        template: '<span class="text-danger f-15" data-ng-show="addContactForm.firstName.$dirty && addContactForm.firstName.$invalid"><span data-ng-show="addContactForm.firstName.$error.required">First Name is required.</span> <span data-ng-show="addContactForm.firstName.$error.maxlength">First Name length should not exceed 128 characters.</span>',
        link: function (scope, elem,attrs) {
            var limit = parseInt(attrs.limitTo);
            scope.$watch('val',function(newVal,oldVal){
                if(!newVal) return;
                scope.countToType = limit-newVal.length;
                /*if(scope.countToType <0){

                }*/

            });
            elem.on('keypress', function(event) {
                if(scope.countToType <0){
                    event.preventDefault();
                }
            });
        }
    };
});
/*app. directive('confirmClick', ['$q', 'dialogModal', function($q, dialogModal) {
    return {
        link: function (scope, element, attrs) {
            // ngClick won't wait for our modal confirmation window to resolve,
            // so we will grab the other values in the ngClick attribute, which
            // will continue after the modal resolves.
            // modify the confirmClick() action so we don't perform it again
            // looks for either confirmClick() or confirmClick('are you sure?')
            var ngClick = attrs.ngClick.replace('confirmClick()', 'true')
                .replace('confirmClick(', 'confirmClick(true,');

            // setup a confirmation action on the scope
            scope.confirmClick = function(msg) {
                // if the msg was set to true, then return it (this is a workaround to make our dialog work)
                if (msg===true) {
                    return true;
                }
                // msg can be passed directly to confirmClick('are you sure?') in ng-click
                // or through the confirm-click attribute on the <a confirm-click="Are you sure?"></a>
                msg = msg || attrs.confirmClick || 'Are you sure?';
                // open a dialog modal, and then continue ngClick actions if it's confirmed
                dialogModal(msg).result.then(function() {
                    scope.$eval(ngClick);
                });
                // return false to stop the current ng-click flow and wait for our modal answer
                return false;
            };
        }
    };
}]);*/

app.directive('limitTo',function () {
    return {
        restrict: 'A',
        scope: {
            watched: '=',
            val: '='
        },
        link: function (scope, elem,attrs) {
            var limit = parseInt(attrs.limitTo);
            scope.$watch('val',function(newVal,oldVal){
                /*if(!newVal) return;*/
                scope.countToType = limit-newVal.length;
                /*if(scope.countToType <0){

                 }*/

            });
            elem.on('keypress', function(event) {
                if(scope.countToType <0){
                    event.preventDefault();
                }
            });
            elem.on('keyup', function(e){
                if(scope.countToType >0){
                    elem.val('Akila');
                }
            });
        }
    };
});
function isEmpty(value) {
    return angular.isUndefined(value) || value === '' || value === null || value !== value;
}

app.directive('ngMin', function () {
    return {
        restrict: 'A',
        require: 'ngModel',
        link: function (scope, elem, attr, ctrl) {
            scope.$watch(attr.ngMin, function () {
                ctrl.$setViewValue(ctrl.$viewValue);
            });
            var minValidator = function (value) {
                var min = attr.ngMin || 0;
                if (!isEmpty(value) && value < min) {
                    ctrl.$setValidity('ngMin', false);
                    return undefined;
                } else {
                    ctrl.$setValidity('ngMin', true);
                    return value;
                }
            };

            ctrl.$parsers.push(minValidator);
            ctrl.$formatters.push(minValidator);
        }
    };
});

app.directive('ngMax', function () {
    return {
        restrict: 'A',
        require: 'ngModel',
        link: function (scope, elem, attr, ctrl) {
            scope.$watch(attr.ngMax, function () {
                ctrl.$setViewValue(ctrl.$viewValue);
            });
            var maxValidator = function (value) {
                var max =attr.ngMax|| Infinity;
                if (!isEmpty(value) && value > max) {
                    ctrl.$setValidity('ngMax', false);
                    return undefined;
                } else {
                    ctrl.$setValidity('ngMax', true);
                    return value;
                }
            };

            ctrl.$parsers.push(maxValidator);
            ctrl.$formatters.push(maxValidator);
        }
    };
});



app.directive('validationMessage', function () {
    return{
        restrict: 'A',
        template: '<input tooltip tooltip-placement="bottom" >',
        replace: true,
        require: 'ngModel',
        link: function (scope, element, attrs, ctrl) {

            ctrl.$parsers.unshift(function(viewValue) {
                var valid = ctrl.$valid;
                if (valid) {
                    attrs.$set('tooltip', '');
                } else {
                    attrs.$set('tooltip', attrs.validationMessage);
                    scope.tt_isOpen = true; // doesn't work!?
                }
                return viewValue;
            });
        }
    };
})
    .directive('numberFormat', function() {
    return {
        require: 'ngModel',
        link: function(scope, element, attrs, ctrl) {
            ctrl.$parsers.unshift(function (viewValue) {
                var invalidNumber = /^[0-9]+$/.test(viewValue);
                if (invalidNumber || viewValue === ''){
                    ctrl.$setValidity('number', true);
                } else {
                    ctrl.$setValidity('number', false);
                }
            });
        }
    };
});

app.directive('floatNumber', function() {
    return {
        require: '?ngModel',
        link: function(scope, element, attrs, ngModelCtrl) {
            if(!ngModelCtrl) {
                return;
            }

            ngModelCtrl.$parsers.push(function(val) {
                if (angular.isUndefined(val)) {
                    val = '';
                }

                var clean = val.replace(/[^-0-9\.]/g, '');
                var decimalCheck = clean.split('.');
                if(!angular.isUndefined(decimalCheck[1])) {
                    decimalCheck[1] = decimalCheck[1].slice(0,2);
                    clean =decimalCheck[0] + '.' + decimalCheck[1];
                }

                    if (decimalCheck.length===2 && (decimalCheck[0].length <= 2 || (decimalCheck[0].length > 2 && decimalCheck[1]==='0' || decimalCheck[1].length==='00'))) {
                        clean=clean;
                    }else if(decimalCheck.length===1 && decimalCheck[0]>0 && decimalCheck[0]<=100) {
                        clean= clean;
                    }else{
                        clean='';
                    }
                    ngModelCtrl.$setViewValue(clean);
                    ngModelCtrl.$render();
                return clean;
            });

            element.bind('keypress', function(event) {
                if(event.keyCode === 32) {
                    event.preventDefault();
                }
            });
        }
    };
});

'use strict';

//Contacts service used to communicate Contacts REST endpoints
var app=angular.module('contacts');
app.factory('Contacts', ['$resource',
	function($resource) {
		return $resource('contacts/:contactId', { contactId: '@_id'
		}, {
			update: {
				method: 'PUT'
			}
		});
	}
]);

/*app.directive('uiBreadcrumbs', ['$interpolate', '$state', function($interpolate, $state) {
		return {
			restrict: 'E',
			templateUrl: function(elem, attrs) {
				return attrs.templateUrl || templateUrl;
			},
			scope: {
				displaynameProperty: '@',
				abstractProxyProperty: '@?'
			},
			link: function(scope) {
				scope.breadcrumbs = [];
				if ($state.$current.name !== '') {
					updateBreadcrumbsArray();
				}
				scope.$on('$stateChangeSuccess', function() {
					updateBreadcrumbsArray();
				});

				/!**
				 * Start with the current state and traverse up the path to build the
				 * array of breadcrumbs that can be used in an ng-repeat in the template.
				 *!/
			/!*	function updateBreadcrumbsArray() {
					var workingState;
					var displayName;
					var breadcrumbs = [];
					var currentState = $state.$current;

					while(currentState && currentState.name !== '') {
						workingState = getWorkingState(currentState);
						if (workingState) {
							displayName = getDisplayName(workingState);

							if (displayName !== false && !stateAlreadyInBreadcrumbs(workingState, breadcrumbs)) {
								breadcrumbs.push({
									displayName: displayName,
									route: workingState.name
								});
							}
						}
						currentState = currentState.parent;
					}
					breadcrumbs.reverse();
					scope.breadcrumbs = breadcrumbs;
				}*!/

				/!**
				 * Get the state to put in the breadcrumbs array, taking into account that if the current state is abstract,
				 * we need to either substitute it with the state named in the `scope.abstractProxyProperty` property, or
				 * set it to `false` which means this breadcrumb level will be skipped entirely.
				 * @param currentState
				 * @returns {*}
				 *!/
				function getWorkingState(currentState) {
					var proxyStateName;
					var workingState = currentState;
					if (currentState.abstract === true) {
						if (typeof scope.abstractProxyProperty !== 'undefined') {
							proxyStateName = getObjectValue(scope.abstractProxyProperty, currentState);
							if (proxyStateName) {
								workingState = $state.get(proxyStateName);
							} else {
								workingState = false;
							}
						} else {
							workingState = false;
						}
					}
					return workingState;
				}

				/!**
				 * Resolve the displayName of the specified state. Take the property specified by the `displayname-property`
				 * attribute and look up the corresponding property on the state's config object. The specified string can be interpolated against any resolved
				 * properties on the state config object, by using the usual {{ }} syntax.
				 * @param currentState
				 * @returns {*}
				 *!/
				function getDisplayName(currentState) {
					var interpolationContext;
					var propertyReference;
					var displayName;

					if (!scope.displaynameProperty) {
						// if the displayname-property attribute was not specified, default to the state's name
						return currentState.name;
					}
					propertyReference = getObjectValue(scope.displaynameProperty, currentState);

					if (propertyReference === false) {
						return false;
					} else if (typeof propertyReference === 'undefined') {
						return currentState.name;
					} else {
						// use the $interpolate service to handle any bindings in the propertyReference string.
						interpolationContext =  (typeof currentState.locals !== 'undefined') ? currentState.locals.globals : currentState;
						displayName = $interpolate(propertyReference)(interpolationContext);
						return displayName;
					}
				}

				/!**
				 * Given a string of the type 'object.property.property', traverse the given context (eg the current $state object) and return the
				 * value found at that path.
				 *
				 * @param objectPath
				 * @param context
				 * @returns {*}
				 *!/
				function getObjectValue(objectPath, context) {
					var i;
					var propertyArray = objectPath.split('.');
					var propertyReference = context;

					for (i = 0; i < propertyArray.length; i ++) {
						if (angular.isDefined(propertyReference[propertyArray[i]])) {
							propertyReference = propertyReference[propertyArray[i]];
						} else {
							// if the specified property was not found, default to the state's name
							return undefined;
						}
					}
					return propertyReference;
				}

				/!**
				 * Check whether the current `state` has already appeared in the current breadcrumbs array. This check is necessary
				 * when using abstract states that might specify a proxy that is already there in the breadcrumbs.
				 * @param state
				 * @param breadcrumbs
				 * @returns {boolean}
				 *!/
				function stateAlreadyInBreadcrumbs(state, breadcrumbs) {
					var i;
					var alreadyUsed = false;
					for(i = 0; i < breadcrumbs.length; i++) {
						if (breadcrumbs[i].route === state.name) {
							alreadyUsed = true;
						}
					}
					return alreadyUsed;
				}
			}
		};
	}]);*/

app.factory('verifyDelete', ['$mdDialog', 'Offers', 'Groups', 'Inventories', 'Products', 'Orders', function($mdDialog,Offers,Groups,Inventories,Products,Orders) {
	return function(user) {
		var content='Do you want to delete the  ';
		var label='Confirm';
		if(user instanceof Offers){
			content+=' Offer:' +user.name;
			label='Delete Offer';
			
		}else if(user instanceof Groups){
			content+=' Group:' +user.name;
			label='Delete Group';
		}else if(user instanceof Inventories){
			content+=' Product:' +(user.product ?user.product.name:'');
			label='Delete Product';
		}
		else if(user instanceof Products){
			content+=' Product:' +user.name;
			label='Delete Product';
		}else if(user instanceof Orders){
            content+=' Order Number:' +user.orderNumber;
            label='Delete Order';
        }
		var
			confirm = $mdDialog.confirm()
			.title('Confirm Your Choice')
			.content( content+ '?')
			.ariaLabel(label)
			.ok(label)
			.cancel('Cancel');
		return $mdDialog.show(confirm);
	};
}]);
app.directive('singleContact',function () {
 return {
	 restrict:'A',
	/* scope:{
		 contact:'='
	 },*/
	 templateUrl: 'modules/contacts/views/contact/contact-row.client.view.html',
 };
});

app.factory('panelTrigger',['$mdSidenav', function($mdSidenav) {
	return function(id) {
		// Component lookup should always be available since we are not using `ng-if`
		$mdSidenav(id)
			.toggle()
			.then(function () {
				/*$log.debug('toggle ' + navID + ' is done');*/
			});
	};
}]);

'use strict';

// Configuring the Articles module
angular.module('core',[]).run(['Menus',
	function(Menus) {
		Menus.addMenuItem('headerbar', 'HOME', '', 'home',false, true);
		Menus.addMenuItem('headerbar', 'ABOUT US', 'about', 'dropdown', '(/about)?(/careers)?',true);
		Menus.addSubMenuItem('headerbar','about', 'Who We are','about',false,true);
		Menus.addSubMenuItem('headerbar', 'about', 'Team', 'team', false, true);
		Menus.addSubMenuItem('headerbar', 'about','Careers', 'careers',false,true);
		Menus.addMenuItem('headerbar', 'CONTACT', 'contact', 'contact',false,true);
		/*Menus.addMenuItem('headerbar', 'Sign In', 'signin', 'signin',false,true);*/
	}
]);

'use strict';

// Setting up route
angular.module('core').config(['$stateProvider', '$urlRouterProvider',
	function($stateProvider, $urlRouterProvider) {
		// Redirect to home view when route not found
		$urlRouterProvider.otherwise('/');

		// Home state routing
		$stateProvider.
		state('home', {
			url: '/',
			templateUrl: 'modules/core/views/homenew.client.view.html'
		}).
		state('signin', {
			url: '/signin',
			templateUrl: 'modules/core/views/homenew.client.view.html'
		}).
		state('homeRegistration', {
			url: '/userreg/:registerToken',
			templateUrl: 'modules/core/views/homenew.client.view.html'
		}).
		state('contact', {
			url: '/contact',
			templateUrl: 'modules/core/views/contact.html'
		}).state('team', {
			url: '/team',
			templateUrl: 'modules/core/views/team.html'
		}).
		state('about', {
			url: '/about',
			templateUrl: 'modules/core/views/about.html'
		}).
		state('font', {
			url: '/font',
			templateUrl: 'modules/core/views/font.html'
		}).
		state('register', {
			url: '/register',
			templateUrl: 'modules/core/views/signin.client.viewnew2.html'
        }).state('enterotp', {
            url: '/enterotp/:statusToken',
            templateUrl: 'modules/core/views/signin.client.viewnew2.html'
        }).state('resendotp', {
				url: '/resendotp',
				templateUrl: 'modules/core/views/signin.client.viewnew2.html'
			}).
		state('careers', {
			url: '/careers',
			templateUrl: 'modules/core/views/careers.html'
		});
	}
]);

'use strict';

angular.module('core').controller('HeaderController', ['$scope', '$http', '$location', 'Authentication', 'Menus', 'localStorageService', 'Notifications','$mdSidenav','panelTrigger',
	function ($scope, $http, $location, Authentication, Menus, localStorageService, Notifications , $mdSidenav,panelTrigger) {
		$scope.authentication = Authentication;
		$scope.isCollapsed = false;
		$scope.location=$location;
		$scope.menu = Menus.getMenu('topbar');
		$scope.headermenu = Menus.getMenu('headerbar');
		$scope.notification = false;
		$scope.profile = true;
		$scope.toggleSideNavigation = function (notification,id,val) {
				$scope.notification = notification;
				$scope.profile = !notification;
		/*	alert($scope.headermenu.items[0].title);*/
			if(!($scope.authentication &&$scope.authentication.user)){
				id='center';
			}else{
				id=id;
			}
			if (val === 'toggle') {
			panelTrigger(id).toggle().then(function () {
				console.log('Trigger is done');
			});
			}else if (val === 'open') {
				panelTrigger(id).open().then(function () {
					console.log('Trigger is done');
				});
			}else if (val === 'close') {
				panelTrigger(id).close().then(function () {
					console.log('Trigger is done');
				});
			}else if (val === 'isOpen') {
				panelTrigger(id).isOpen().then(function () {
					console.log('Trigger is done');
				});
			}
		};



	/*	$scope.getnotifications = function()
		{
			$scope.isnotification = true;
			$scope.isprofile = false;
		};

		$scope.getprofile = function()
		{
			$scope.isprofile = true;
			$scope.isnotification = false;
		};

		$scope.isOpenRight = function(){
			return $mdSidenav('right').isOpen();
		};*/

		$scope.dashboardData = function () {
			if ($scope.authentication && $scope.authentication.user && $scope.authentication.token) {
				$http.get('/users/myhome')
                    .then(function (rs) {
                    	var response=rs.data;
						$scope.notifications = response.notifications;
						$scope.orders = response.orders;
						$scope.offers = response.offers;
						$scope.todos = response.todos;
					},function(response) {
                    localStorageService.remove('nvipanilogindata');
                    Authentication.token = null;
                    Authentication.user = null;
                    Authentication.deviceid = null;
                    $location.path('/');
                    $scope.authentication.token = null;
                    $scope.authentication.user = null;
                    $scope.authentication.deviceid = null;
					$scope.error = response.data.message;
				});
			}
		};
		$scope.isOpenRight = function(){
			return $mdSidenav('right').isOpen();
		};

		function buildToggler(navID) {
			return function() {
				// Component lookup should always be available since we are not using `ng-if`
				$mdSidenav(navID)
						.toggle()
						.then(function () {
						});
			};
		}




		$scope.toggleCollapsibleMenu = function () {
			$scope.isCollapsed = !$scope.isCollapsed;
		};
		// Collapsing the menu after navigation
		$scope.$on('$stateChangeSuccess', function () {
			$scope.isCollapsed = false;
		});

		$scope.changeUrl=function (url) {
			$location.path(url);
		};


		$scope.changeUrlNotification=function (notification) {
		$location.path((notification.type === 'Offer' || notification.type === 'OfferComment' || notification.type  ==='OfferStatus')?'offers/'+notification.source.offer:'orders/'+notification.source.order);
		};
		// Find a list of Groups
	$scope.findNotifications = function () {
			if ($scope.authentication && $scope.authentication.user && $scope.authentication.token) {
				$scope.notifications = Notifications.query();
			}
		};
	/*	$scope.$on('$locationChangeStart', function() {
			var token = $scope.authentication.token;
			if (token) {
				if (!jwtHelper.isTokenExpired(token)) {
					if (!$scope.authentication.isAuthenticated) {
						$scope.authentication.authenticate($scope.authentication.user, token);
					}
				} else {
					// Either show the login page or use the refresh token to get a new idToken
					Authentication.token = null;
					Authentication.user = null;
					$scope.authentication.token = null;
					localStorageService.remove('nvipanilogindata');
					$location.path('/');
				}
			}
		});*/
		$scope.signout = function () {
            $http.get('/auth/signout', {
                token: $scope.authentication.token,
                deviceid: $scope.authentication.deviceid
            }).then(function (response) {
				Authentication.token = null;
				Authentication.user = null;
                Authentication.deviceid = null;
				$scope.authentication.token = null;
				$scope.authentication.user = null;
                $scope.authentication.deviceid = null;

				localStorageService.remove('nvipanilogindata');

				// Redirect to signin page
				$location.path('/');
			},function(response) {
				localStorageService.remove('nvipanilogindata');
				Authentication.token = null;
				Authentication.user = null;
                Authentication.deviceid = null;
				$location.path('/');
				$scope.authentication.token = null;
				$scope.authentication.user = null;
                $scope.authentication.deviceid = null;
				$scope.signouterror = response.data.message;

			});

		};

		$scope.closeright = function () {
			// Component lookup should always be available since we are not using `ng-if`
			$mdSidenav('right').close()
					.then(function () {
					});
		};

	}
]);

'use strict';
var app=angular.module('core');

app.controller('HomeController', ['$scope', 'Authentication', '$http', '$location', '$stateParams', 'localStorageService',
    function ($scope, Authentication, $http, $location, $stateParams, localStorageService) {
		// This provides Authentication context.
		$scope.authentication = Authentication;
		$scope.location=$location;
		$scope.tag='New';
		$scope.appName='nVipani';
		if(!$scope.authentication.user && $stateParams && $stateParams.registerToken && $stateParams.registerToken.contains('userreg')){
			$location.path('/');
		}
		$scope.changeType= function (tagVal) {
			$scope.tag=tagVal;
		};
		$scope.dashboardData = function () {
          /*  var defer = $q.defer();*/
			if ($scope.authentication && $scope.authentication.user && $scope.authentication.token) {
				$http.get('/users/myhome')
					.then(function (rs) {
                       /* defer.resolve(response.data);*/
                       var response=rs.data.
						$scope.notifications = response.notifications;
						$scope.orders = response.orders;
						$scope.offers = response.offers;
						$scope.todos = response.todos;

					},function (response) {
                    localStorageService.remove('nvipanilogindata');
                    Authentication.token = null;
                    Authentication.user = null;
                    Authentication.deviceid = null;
                    $location.path('/');
                    $scope.authentication.token = null;
                    $scope.authentication.user = null;
                    $scope.authentication.deviceid = null;
					$scope.error = response.data.message;

				});
			}
		};

	}
]);
app.controller('BottomImageController', ['$scope', 'Authentication','$location',
	function($scope, Authentication,$location) {
		// This provides Authentication context.
		$scope.authentication = Authentication;
		$scope.location=$location;
	}

]);

app.controller('ContactPublicController', ['$scope', 'Leads','$location',
	function($scope, Leads,$location) {
		$scope.appName='nVipani';
		// This provides Authentication context.
		$scope.reset = function(form) {
			$scope.error = null;
			if (form) {
				form.$setPristine();
				//form.$setUntouched();
			}

		};

		$scope.create = function() {
			// Create new Lead object
			var lead = new Leads ({
				name: this.name,
				email:this.email,
				description: this.description
			});
			// Redirect after save
			lead.$save(function(response) {
				$scope.name=null;
				// Clear form fields
				/*$scope.reset('contactform');*/
				$scope.emailsent = 'Thanks for your interest on nVipani, Soon we will get back to you';
				$scope.email = null;
				$scope.description=null;
			}, function(errorResponse) {
				$scope.error = errorResponse.data.message;
			});
		};
	}

]);

app.directive('flexSlider', function () {

	return {
		link: function (scope, element, attrs) {

			element.flexslider({
				animation: 'slide'
			});
		}
	};


});

app.directive('carousel', function () {

	return {
		link: function (scope, element, attrs) {

			element.flexslider({
				animation: 'slide',
				controlNav: false,
				animationLoop: true,
				slideshow: true,
				itemWidth: 210,
				itemMargin: 5,
				asNavFor: '#slider'
			});
		}
	};


});
app.directive('register', function(){
	return {
		restrict:'E',
		templateUrl:'modules/core/views/signin.client.viewnew2.html'
	};
});

/*
$(document).ready(function() {

    /!* ======= Twitter Bootstrap hover dropdown ======= *!/
    /!* Ref: https://github.com/CWSpear/bootstrap-hover-dropdown *!/
    /!* apply dropdownHover to all elements with the data-hover="dropdown" attribute *!/

    $('[data-hover="dropdown"]').dropdownHover();

    /!* ======= Fixed header when scrolled ======= *!/
    $(window).on('scroll load', function() {

         if ($(window).scrollTop() > 0) {
             $('#header').addClass('scrolled');
         }
         else {
             $('#header').removeClass('scrolled');

         }
    });


    /!* ======= jQuery Placeholder ======= *!/
    /!* Ref: https://github.com/mathiasbynens/jquery-placeholder *!/

    $('input, textarea').placeholder();

    /!* ======= jQuery FitVids - Responsive Video ======= *!/
    /!* Ref: https://github.com/davatron5000/FitVids.js/blob/master/README.md *!/

    $(".video-container").fitVids();

    /!* ======= FAQ accordion ======= *!/
    function toggleIcon(e) {
    $(e.target)
        .prev('.panel-heading')
        .find('.panel-title a')
        .toggleClass('active')
        .find("i.fa")
        .toggleClass('fa-plus-square fa-minus-square');
    }
    $('.panel').on('hidden.bs.collapse', toggleIcon);
    $('.panel').on('shown.bs.collapse', toggleIcon);


    /!* ======= Header Background Slideshow - Flexslider ======= *!/
    /!* Ref: https://github.com/woothemes/FlexSlider/wiki/FlexSlider-Properties *!/

    $('.bg-slider').flexslider({
        animation: "fade",
        directionNav: false, //remove the default direction-nav - https://github.com/woothemes/FlexSlider/wiki/FlexSlider-Properties
        controlNav: false, //remove the default control-nav
        slideshowSpeed: 7000
    });

	/!* ======= Stop Video Playing When Close the Modal Window ====== *!/
    $("#modal-video .close").on("click", function() {
        $("#modal-video iframe").attr("src", $("#modal-video iframe").attr("src"));
    });


     /!* ======= Testimonial Bootstrap Carousel ======= *!/
     /!* Ref: http://getbootstrap.com/javascript/#carousel *!/
    $('#testimonials-carousel').carousel({
      interval: 8000
    });


    /!* ======= Style Switcher ======= *!/
    $('#config-trigger').on('click', function(e) {
        var $panel = $('#config-panel');
        var panelVisible = $('#config-panel').is(':visible');
        if (panelVisible) {
            $panel.hide();
        } else {
            $panel.show();
        }
        e.preventDefault();
    });

    $('#config-close').on('click', function(e) {
        e.preventDefault();
        $('#config-panel').hide();
    });


    $('#color-options a').on('click', function(e) {
        var $styleSheet = $(this).attr('data-style');
		$('#theme-style').attr('href', $styleSheet);

		var $listItem = $(this).closest('li');
		$listItem.addClass('active');
		$listItem.siblings().removeClass('active');

		e.preventDefault();

	});


});
*/

/*
var map;
jQuery(document).ready(function(){

    map = new GMaps({
        div: '#map',
        lat: 12.969409,
        lng: 77.641500,
    });
    map.addMarker({
      lat: 12.969409,
      lng: 77.641500,
        title: 'Address',
        infoWindow: {
            content: '<h5 class="title">nVipani Software Solutions Private Limited</h5><p><span class="region">#1134, 4th Floor, 100 Feet Road, HAL 2nd Stage, Bangalore</span><br><span class="postal-code">560038</span><br><span class="country-name">India</span></p>'
        }

    });

});
*/

'use strict';

//Menu service used for managing  menus
var app=angular.module('core');
app.service('Menus', [

	function() {
		// Define a set of default roles
		this.defaultRoles = ['*'];

		// Define the menus object
		this.menus = {};

		// A private function for rendering decision 
		var shouldRender = function(user) {
			if (user) {
				if (!!~this.roles.indexOf('*')) {
					return true;
				} else {
					for (var userRoleIndex in user.roles) {
						for (var roleIndex in this.roles) {
							if (this.roles[roleIndex] === user.roles[userRoleIndex]) {
								return true;
							}
						}
					}
				}
			} else {
				return this.isPublic;
			}

			return false;
		};

		// Validate menu existance
		this.validateMenuExistance = function(menuId) {
			if (menuId && menuId.length) {
				if (this.menus[menuId]) {
					return true;
				} else {
					throw new Error('Menu does not exists');
				}
			} else {
				throw new Error('MenuId was not provided');
			}

			return false;
		};

		// Get the menu object by menu id
		this.getMenu = function(menuId) {
			// Validate that the menu exists
			this.validateMenuExistance(menuId);

			// Return the menu object
			return this.menus[menuId];
		};

		// Add new menu object by menu id
		this.addMenu = function(menuId, isPublic, roles) {
			// Create the new menu
			this.menus[menuId] = {
				isPublic: isPublic || false,
				roles: roles || this.defaultRoles,
				items: [],
				shouldRender: shouldRender
			};

			// Return the menu object
			return this.menus[menuId];
		};

		// Remove existing menu object by menu id
		this.removeMenu = function(menuId) {
			// Validate that the menu exists
			this.validateMenuExistance(menuId);

			// Return the menu object
			delete this.menus[menuId];
		};

		// Add menu item object
		this.addMenuItem = function(menuId, menuItemTitle, menuItemURL, menuItemType, menuItemUIRoute, isPublic, roles, position) {
			// Validate that the menu exists
			this.validateMenuExistance(menuId);

			// Push new menu item
			this.menus[menuId].items.push({
				title: menuItemTitle,
				link: menuItemURL,
				menuItemType: menuItemType || 'item',
				menuItemClass: menuItemType,
				uiRoute: menuItemUIRoute || ('/' + menuItemURL),
				isPublic: ((isPublic === null || typeof isPublic === 'undefined') ? this.menus[menuId].isPublic : isPublic),
				roles: ((roles === null || typeof roles === 'undefined') ? this.menus[menuId].roles : roles),
				position: position || 0,
				items: [],
				shouldRender: shouldRender
			});

			// Return the menu object
			return this.menus[menuId];
		};

		// Add submenu item object
		this.addSubMenuItem = function(menuId, rootMenuItemURL, menuItemTitle, menuItemURL, menuItemUIRoute, isPublic, roles, position) {
			// Validate that the menu exists
			this.validateMenuExistance(menuId);

			// Search for menu item
			for (var itemIndex in this.menus[menuId].items) {
				if (this.menus[menuId].items[itemIndex].link === rootMenuItemURL) {
					// Push new submenu item
					this.menus[menuId].items[itemIndex].items.push({
						title: menuItemTitle,
						link: menuItemURL,
						uiRoute: menuItemUIRoute || ('/' + menuItemURL),
						isPublic: ((isPublic === null || typeof isPublic === 'undefined') ? this.menus[menuId].items[itemIndex].isPublic : isPublic),
						roles: ((roles === null || typeof roles === 'undefined') ? this.menus[menuId].items[itemIndex].roles : roles),
						position: position || 0,
						shouldRender: shouldRender
					});
				}
			}

			// Return the menu object
			return this.menus[menuId];
		};

		// Remove existing menu object by menu id
		this.removeMenuItem = function(menuId, menuItemURL) {
			// Validate that the menu exists
			this.validateMenuExistance(menuId);

			// Search for menu item to remove
			for (var itemIndex in this.menus[menuId].items) {
				if (this.menus[menuId].items[itemIndex].link === menuItemURL) {
					this.menus[menuId].items.splice(itemIndex, 1);
				}
			}

			// Return the menu object
			return this.menus[menuId];
		};

		// Remove existing menu object by menu id
		this.removeSubMenuItem = function(menuId, submenuItemURL) {
			// Validate that the menu exists
			this.validateMenuExistance(menuId);

			// Search for menu item to remove
			for (var itemIndex in this.menus[menuId].items) {
				for (var subitemIndex in this.menus[menuId].items[itemIndex].items) {
					if (this.menus[menuId].items[itemIndex].items[subitemIndex].link === submenuItemURL) {
						this.menus[menuId].items[itemIndex].items.splice(subitemIndex, 1);
					}
				}
			}

			// Return the menu object
			return this.menus[menuId];
		};

		//Adding the topbar menu
		this.addMenu('topbar');
		this.addMenu('headerbar');
	}
]);

app.factory('panelTrigger',['$mdSidenav','$q', function($mdSidenav,$q) {
	return function (handle) {
	var nav=$mdSidenav(handle);
		var errorMsg = 'SideNav ' + handle + ' is not available!';
		return  {
			
			isOpen: function () {
				return nav && nav.isOpen();
			},
			isLockedOpen: function () {
				return nav && nav.nav();
			},
			
			toggle: function () {
				return nav ? nav.toggle() : $q.reject(errorMsg);
			},
			open: function () {
				return nav ? nav.open() : $q.reject(errorMsg);
			},
			close: function () {
				return nav ? nav.close() : $q.reject(errorMsg);
			},
			/*then: function (callbackFn) {
				var promise = nav ? $q.when(nav) : waitForInstance();
				return promise.then(callbackFn || angular.noop);
			}*/
		};
	};
}]);

'use strict';

// Configuring the Articles module
/*
angular.module('groups').run(['Menus',
	function(Menus) {
		// Set top bar menu items
		Menus.addMenuItem('topbar', 'Groups', 'groups', 'dropdown', '/groups(/create)?');
		Menus.addSubMenuItem('topbar', 'groups', 'List Groups', 'groups');
		Menus.addSubMenuItem('topbar', 'groups', 'New Group', 'groups/create');
	}
]);*/

'use strict';

//Setting up route
angular.module('groups',[]).config(['$stateProvider',
	function($stateProvider) {
		// Groups state routing
		$stateProvider.
		state('listGroups', {
			url: '/groups',
			templateUrl: 'modules/groups/views/list-groups.client.view.html'
		}).
		state('createGroup', {
			url: '/groups/create',
			templateUrl: 'modules/groups/views/create-group.client.view.html'
		}).
		state('viewGroup', {
			url: '/groups/:groupId',
			templateUrl: 'modules/groups/views/view-group.client.view.html'
		});
		/*state('editGroup', {
			url: '/groups/:groupId/edit',
			templateUrl: 'modules/groups/views/edit-group.client.view.html'
		});*/
	}
]);

'use strict';

// Groups controller
var app = angular.module('groups');
app.controller('GroupsController', ['$scope', '$stateParams', '$location', 'Authentication', 'Groups',
	function($scope, $stateParams, $location, Authentication, Groups) {
		$scope.authentication = Authentication;

		// Create new Group
		$scope.create = function() {
			// Create new Group object
			var group = new Groups ({
				name: this.name,
				description:this.description
			});
			// Redirect after save
			group.$save(function(response) {
				$location.path('groups/' + response._id);

				// Clear form fields
				$scope.clearFields();
			}, function(errorResponse) {
				$scope.error = errorResponse.data.message;
			});
		};
		$scope.clearFields = function() {
			$scope.name = '';
			$scope.description = '';
		};

		$scope.reset = function(form) {
			if (form) {
				form.$setPristine();
				//form.$setUntouched();
			}
		};
		$scope.changeSelectedGroup = function(group){
			if($scope.allContacts===true) {
				$scope.allContacts = false;
			}
			$scope.selectedGroup = group;
		};
		$scope.mainContactView = function (changeStr){
			$scope.AllContactsString='All Contacts';
			if(typeof changeStr === 'string'){
				if(changeStr === 'groupEdit'){
					$scope.groupid=changeStr;
					$scope.allContacts = false;
					$scope.group=true;
					$scope.groupEditView=true;
				}else if(changeStr === 'groupDelete'){
					$scope.groupid=changeStr;
					$scope.allContacts = false;
					$scope.group=true;
					$scope.groupDelView=true;
				}else if(changeStr === 'group'){
					$scope.groupid=changeStr;
					$scope.allContacts = false;
					$scope.group=true;
					$scope.groupDelView=true;
				} else{
					$scope.allContacts = true;
					$scope.group=false;
				}

			}
		};

	/*	// Remove existing Group
		$scope.remove = function(group) {
			if ( group ) {
				group.$remove();

				for (var i in $scope.groups) {
					if ($scope.groups [i] === group) {
						$scope.groups.splice(i, 1);
					}
				}
			} else {
				$scope.group.$remove(function() {
					$location.path('groups');
				});
			}
		};*/

		// Update existing Group
		$scope.update = function() {
			var group = $scope.group;

			group.$update(function() {
				$location.path('groups/' + group._id);
			}, function(errorResponse) {
				$scope.error = errorResponse.data.message;
			});
		};

		// Find a list of Groups
		$scope.find = function() {
			$scope.groups = Groups.query();
		};

		// Find existing Group
		$scope.findOne = function() {
			$scope.group = Groups.get({
				groupId: $stateParams.groupId
			});
		};
	}
]);
/*app.directive('editGroup', function(){
	return {
		restrict:'E',
		templateUrl:'modules/groups/views/edit-group.client.view.html'
	};
});*/

app.directive('viewGroup', function(){
	return {
		restrict:'E',
		templateUrl:'modules/groups/views/view-group.client.view.html'
	};
});

app.directive('addGroup', function(){
	return {
		restrict:'E',
		templateUrl:'modules/groups/views/create-group.client.view.html'
	};
});

'use strict';

//Groups service used to communicate Groups REST endpoints
angular.module('groups').factory('Groups', ['$resource',
	function($resource) {
		return $resource('groups/:groupId', { groupId: '@_id'
		}, {
			update: {
				method: 'PUT'
			}
		});
	}
]);
'use strict';

//Setting up route
angular.module('inventories',[]).config(['$stateProvider',
    function ($stateProvider) {
        // Inventories state routing
        $stateProvider.state('listInventories', {
            url: '/inventories',
            templateUrl: 'modules/inventories/views/list-inventories.client.view.html'
        }).state('createInventory', {
            url: '/inventories/create',
            templateUrl: 'modules/inventories/views/create-inventory.client.view.html'
        }).state('viewInventory', {
            url: '/inventories/:inventoryId',
            templateUrl: 'modules/inventories/views/view-inventory.client.view.html'
        }).state('editInventory', {
            url: '/inventories/:inventoryId/edit',
            templateUrl: 'modules/inventories/views/edit-inventory.client.view.html'
        });
    }
]);

'use strict';

// Inventories controller
var app=angular.module('inventories');
app.controller('InventoriesController', ['$scope', '$stateParams', '$location', 'Authentication', 'Inventories',
    function ($scope, $stateParams, $location, Authentication, Inventories) {
        $scope.authentication = Authentication;

        // Create new Inventory
        $scope.create = function () {
            // Create new Inventory object
            var inventory = new Inventories({
                name: this.name
            });

            // Redirect after save
            inventory.$save(function (response) {
                $location.path('inventories/' + response._id);

                // Clear form fields
                $scope.name = '';
            }, function (errorResponse) {
                $scope.error = errorResponse.data.message;
            });
        };

        // Remove existing Inventory
        $scope.remove = function (inventory) {
            if (inventory) {
                inventory.$remove();

                for (var i in $scope.inventories) {
                    if ($scope.inventories [i] === inventory) {
                        $scope.inventories.splice(i, 1);
                    }
                }
            } else {
                $scope.inventory.$remove(function () {
                    $location.path('inventories');
                });
            }
        };

        // Update existing Inventory
        $scope.update = function () {
            var inventory = this.inventory;

            inventory.$update(function () {
                $location.path('products');
            }, function (errorResponse) {
                $scope.error = errorResponse.data.message;
            });
        };

        $scope.filterSelectedProducts=function () {
          return $scope.inventories.filter(function (inventory) {
              return inventory.selected;
          });
        };

        // Find a list of Inventories
        $scope.find = function () {
            $scope.inventories = Inventories.query();
        };

        // Find existing Inventory
        $scope.findOne = function () {
            $scope.inventory = Inventories.get({
                inventoryId: $stateParams.inventoryId
            });
        };
        $scope.panelChange = function (changeStr){
            $scope.viewAllProduct=false;
            $scope.showEditProduct=false;
            $scope.showAddProduct=false;

            if(changeStr==='viewAll'){
                $scope.viewAllProduct=true;
                $scope.find();
            }else if(changeStr==='Edit'){
                $stateParams.inventoryId=this.inventory._id;
                $scope.findOne();
                $scope.showEditProduct=true;
            }else if(changeStr==='Add'){
                $scope.showAddProduct=true;

            }
        };
    }
]);

app.directive('editProduct', function(){
    return {
        restrict:'E',
        templateUrl:'modules/products/views/edit-product.client.viewnew.html'
    };
});

app.directive('addProduct', function(){
    return {
        restrict:'E',
        templateUrl:'modules/products/views/create-product.client.viewnew.html'
    };
});

app.directive('addOfferProduct', function(){
    return {
        restrict:'E',
        templateUrl:'modules/products/views/create-product-offer.client.viewnew.html'
    };
});

app.directive('viewProduct', function(){
    return {
        restrict:'E',
        templateUrl:'modules/products/views/list-products.client.viewnew.html'
    };
});

'use strict';

//Inventories service used to communicate Inventories REST endpoints
var app=angular.module('inventories');
app.factory('Inventories', ['$resource',
    function ($resource) {
        return $resource('inventories/:inventoryId', {
            inventoryId: '@_id'
        }, {
            update: {
                method: 'PUT'
            }
        });
    }
]);

app.factory('InventoryCommon', ['$resource',
    function ($resource) {
        return $resource('inventories/:inventoryId', {
            inventoryId: '@_id'
        }, {
            update: {
                method: 'PUT'
            }
        });
    }
]);
app.directive('singleProduct',function () {
    return {
        restrict:'A',
        /* scope:{
         contact:'='
         },*/
        templateUrl: 'modules/products/views/product-row.client.view.html',
    };
});

'use strict';

// Configuring the Articles module
angular.module('leads').run(['Menus',
	function(Menus) {
		// Set top bar menu items
	/*	Menus.addMenuItem('topbar', 'Leads', 'leads', 'dropdown', '/leads(/create)?');
		Menus.addSubMenuItem('topbar', 'leads', 'List Leads', 'leads');
		Menus.addSubMenuItem('topbar', 'leads', 'New Lead', 'leads/create');*/
	}
]);

'use strict';

//Setting up route
angular.module('leads').config(['$stateProvider',
	function($stateProvider) {
		// Leads state routing
		$stateProvider.
		state('listLeads', {
			url: '/leads',
			templateUrl: 'modules/leads/views/list-leads.client.view.html'
		}).
		state('createLead', {
			url: '/leads/create',
			templateUrl: 'modules/leads/views/create-lead.client.view.html'
		}).
		state('viewLead', {
			url: '/leads/:leadId',
			templateUrl: 'modules/leads/views/view-lead.client.view.html'
		}).
		state('editLead', {
			url: '/leads/:leadId/edit',
			templateUrl: 'modules/leads/views/edit-lead.client.view.html'
		});
	}
]);

'use strict';

// Leads controller
angular.module('leads').controller('LeadsController', ['$scope', '$stateParams', '$location', 'Authentication', 'Leads',
	function($scope, $stateParams, $location, Authentication, Leads) {
		$scope.authentication = Authentication;

		// Create new Product
		$scope.create = function() {
			// Create new Lead object
			var lead = new Leads ({
				name: this.name,
				email:this.email,
				description: this.description
			});
			// Redirect after save
			lead.$save(function(response) {
				$location.path('leads/' + response._id);

				// Clear form fields
				$scope.clearFields();
				$scope.name = '';
			}, function(errorResponse) {
				$scope.error = errorResponse.data.message;
			});
		};

		//Clear Fields Function
		$scope.clearFields = function () {
			$scope.name = '';
			$scope.type = '';
			$scope.category = '';
			$scope.subCategory = '';
			$scope.unitSize = '';
			$scope.unitMeasure = '';
			$scope.description = '';
		};
		// Remove existing Product
		$scope.remove = function(lead) {
			if ( lead ) {
				lead.$remove();

				for (var i in $scope.leads) {
					if ($scope.leads [i] === lead) {
						$scope.leads.splice(i, 1);
					}
				}
			} else {
				$scope.lead.$remove(function() {
					$location.path('leads');
				});
			}
		};

		// Update existing Product
		$scope.update = function() {
			var lead = $scope.lead;
			lead.$update(function() {
				$location.path('leads/' + lead._id);
			}, function(errorResponse) {
				$scope.error = errorResponse.data.message;
			});
		};

		// Find a list of Products
		$scope.find = function() {
			$scope.leads = Leads.query();
		};

		// Find existing Product
		$scope.findOne = function() {
			$scope.lead = Leads.get({
				leadId: $stateParams.leadId
			});
		};
	}
]);

'use strict';

//Leads service used to communicate Leads REST endpoints
angular.module('leads').factory('Leads', ['$resource',
	function($resource) {
		return $resource('leads/:leadId', { leadId: '@_id'
		}, {
			update: {
				method: 'PUT'
			}
		});
	}
]);

'use strict';

// Configuring the Articles module
angular.module('messages',[]).run(['Menus',
    function (Menus) {
        // Set top bar menu items
       /* Menus.addMenuItem('topbar', 'Messages', 'messages', 'dropdown', '/messages(/create)?');
        Menus.addSubMenuItem('topbar', 'messages', 'List Messages', 'messages');
        Menus.addSubMenuItem('topbar', 'messages', 'New Message', 'messages/create');*/
    }
]);

'use strict';

//Setting up route
angular.module('messages').config(['$stateProvider',
    function ($stateProvider) {
        // Messages state routing
        $stateProvider.
            state('listMessages', {
                url: '/messages',
                templateUrl: 'modules/messages/views/list-messages.client.view.html'
            }).
            state('createMessage', {
                url: '/messages/create',
                templateUrl: 'modules/messages/views/create-message.client.view.html'
            }).
            state('viewMessage', {
                url: '/messages/:messageId',
                templateUrl: 'modules/messages/views/view-message.client.view.html'
            }).
            state('editMessage', {
                url: '/messages/:messageId/edit',
                templateUrl: 'modules/messages/views/edit-message.client.view.html'
            });
    }
]);

'use strict';

// Messages controller
angular.module('messages').controller('MessagesController', ['$scope', '$stateParams', '$location', 'Authentication', 'Messages',
    function ($scope, $stateParams, $location, Authentication, Messages) {
        $scope.authentication = Authentication;

        // Create new Message
        $scope.create = function () {
            // Create new Message object
            var message = new Messages({
                body: this.body
            });

            // Redirect after save
            message.$save(function (response) {
                $location.path('messages/' + response._id);

                // Clear form fields
                $scope.body = '';
            }, function (errorResponse) {
                $scope.error = errorResponse.data.message;
            });
        };

        // Remove existing Message
        $scope.remove = function (message) {
            if (message) {
                message.$remove();

                for (var i in $scope.messages) {
                    if ($scope.messages [i] === message) {
                        $scope.messages.splice(i, 1);
                    }
                }
            } else {
                $scope.message.$remove(function () {
                    $location.path('messages');
                });
            }
        };

        // Update existing Message
        $scope.update = function () {
            var message = $scope.message;

            message.$update(function () {
                $location.path('messages/' + message._id);
            }, function (errorResponse) {
                $scope.error = errorResponse.data.message;
            });
        };

        // Find a list of Messages
        $scope.find = function () {
            $scope.messages = Messages.query();
        };

        // Find existing Message
        $scope.findOne = function () {
            $scope.message = Messages.get({
                messageId: $stateParams.messageId
            });
        };
    }
]);

'use strict';

//Messages service used to communicate Messages REST endpoints
angular.module('messages').factory('Messages', ['$resource',
    function ($resource) {
        return $resource('messages/:messageId', {
            messageId: '@_id'
        }, {
            update: {
                method: 'PUT'
            }
        });
    }
]).factory('OrderMessages', ['$resource',
    function ($resource) {
        return $resource('orderMessages/:orderId', {
            orderId: '@_id'
        }, {
            update: {
                method: 'PUT'
            }
        });
    }
]).factory('OfferMessages', ['$resource',
    function ($resource) {
        return $resource('offerMessages/:offerId', {
            offerId: '@_id'
        }, {
            update: {
                method: 'PUT'
            }
        });
    }
]);

'use strict';

// Configuring the Articles module
angular.module('notifications',[]).run(['Menus',
    function (Menus) {
        // Set top bar menu items
     /*   Menus.addMenuItem('topbar', 'Notifications', 'notifications', 'dropdown', '/notifications(/create)?');
        Menus.addSubMenuItem('topbar', 'notifications', 'List Notifications', 'notifications');
        Menus.addSubMenuItem('topbar', 'notifications', 'New Notification', 'notifications/create');*/
    }
]);

'use strict';

//Setting up route
angular.module('notifications').config(['$stateProvider',
    function ($stateProvider) {
        // Notifications state routing
        $stateProvider.
            state('listNotifications', {
                url: '/notifications',
                templateUrl: 'modules/notifications/views/list-notifications.client.view.html'
            }).
            state('createNotification', {
                url: '/notifications/create',
                templateUrl: 'modules/notifications/views/create-notification.client.view.html'
            }).
            state('viewNotification', {
                url: '/notifications/:notificationId',
                templateUrl: 'modules/notifications/views/view-notification.client.view.html'
            }).
            state('editNotification', {
                url: '/notifications/:notificationId/edit',
                templateUrl: 'modules/notifications/views/edit-notification.client.view.html'
            });
    }
]);

'use strict';

// Notifications controller
angular.module('notifications').controller('NotificationsController', ['$scope', '$stateParams', '$location', 'Authentication', 'Notifications',
    function ($scope, $stateParams, $location, Authentication, Notifications) {
        $scope.authentication = Authentication;

        // Create new Notification
        $scope.create = function () {
            // Create new Notification object
            var notification = new Notifications({
                name: this.name
            });

            // Redirect after save
            notification.$save(function (response) {
                $location.path('notifications/' + response._id);

                // Clear form fields
                $scope.name = '';
            }, function (errorResponse) {
                $scope.error = errorResponse.data.message;
            });
        };

        // Remove existing Notification
        $scope.remove = function (notification) {
            if (notification) {
                notification.$remove();

                for (var i in $scope.notifications) {
                    if ($scope.notifications [i] === notification) {
                        $scope.notifications.splice(i, 1);
                    }
                }
            } else {
                $scope.notification.$remove(function () {
                    $location.path('notifications');
                });
            }
        };

        // Update existing Notification
        $scope.update = function () {
            var notification = $scope.notification;

            notification.$update(function () {
                $location.path('notifications/' + notification._id);
            }, function (errorResponse) {
                $scope.error = errorResponse.data.message;
            });
        };

        // Find a list of Notifications
        $scope.find = function () {
            $scope.notifications = Notifications.query();
        };

        // Find existing Notification
        $scope.findOne = function () {
            $scope.notification = Notifications.get({
                notificationId: $stateParams.notificationId
            });
        };
    }
]);

'use strict';

//Notifications service used to communicate Notifications REST endpoints
angular.module('notifications').factory('Notifications', ['$resource',
    function ($resource) {
        return $resource('notifications/:notificationId', {
            notificationId: '@_id'
        }, {
            update: {
                method: 'PUT'
            }
        });
    }
]);

'use strict';

// Configuring the Articles module
angular.module('offers', []).run(['Menus',
	function(Menus) {
		// Set top bar menu items
		Menus.addMenuItem('topbar', 'Offers', 'offers', 'offers');
		/*	Menus.addMenuItem('topbar', 'Offers', 'offers', 'dropdown', '/offers(/create)?');
	Menus.addSubMenuItem('topbar', 'offers', 'List Offers', 'offers');
		Menus.addSubMenuItem('topbar', 'offers', 'New Offer', 'offers/create');*/
	}
]);
//Xeditable method - sets theme
angular.module('offers').run(['editableOptions', function(editableOptions) {
	editableOptions.theme = 'bs3';
}]);

'use strict';

//Setting up route
angular.module('offers').config(['$stateProvider',
	function($stateProvider) {
		// Offers state routing
		$stateProvider.
		state('listOffers', {
			url: '/offers',
			templateUrl: 'modules/offers/views/list-offers.client.viewnew.html'
		}).
		state('createOffer', {
			url: '/offers/create',
			templateUrl: 'modules/offers/views/create-offer.client.viewnew.html'
		}).
		state('viewOffer', {
			url: '/offers/:offerId',
			templateUrl: 'modules/offers/views/list-offers.client.viewnew.html'
		}).
		state('viewOffers', {
			url: '/offersnotification/:offerId',
			templateUrl: 'modules/offers/views/list-category.client.view.html'
		}).
		state('editOffer', {
			url: '/offers/category/:categoryId',
			templateUrl: 'modules/offers/views/single-offer.client.view.html'
		});
	}
]);

'use strict';

var app = angular.module('offers');

/*
 Controller for the list Offers page
 */
app.controller('OffersController', ['$scope', '$stateParams', '$location', 'Authentication', 'Offers', 'Contacts', 'Products','inventoryProducts', 'Orders', '$timeout', '$compile', '$element', 'Groups', 'Messages', 'OfferMessages', '$filter', '$q', 'DatePicker', 'verifyDelete', 'panelTrigger', '$mdToast', '$mdDialog', '$mdMedia','publicOfferCategories','offersApi','contactsGroup',
	function ($scope, $stateParams, $location, Authentication, Offers, Contacts, Products,inventoryProducts, Orders, $timeout, $compile, $element, Groups, Messages, OfferMessages, $filter, $q, DatePicker, verifyDelete, panelTrigger, $mdToast, $mdDialog, $mdMedia,publicOfferCategories,offersApi,contactsGroup) {
		// Remove existing Offer
		var last = {
			bottom: false,
			top: true,
			left: false,
			right: true
		};
		$scope.offers = [];
		$scope.categories = [];
		$scope.searchOfferText = '';
		$scope.authentication = Authentication;
		$scope.noResultsTag = null;
		$scope.selectedproducts = [];
		$scope.offerMessages = [];
		$scope.showModal = false;
		$scope.selectedtab = 'MyOffers';
		$scope.offerTypes = [{value: 'Sell', text: 'Seller'}, {value: 'Buy', text: 'Buyer'}];
		$scope.offerType = 'Sell';
		$scope.selectedIndex = 0;
		$scope.offerCategories = ['Buyer', 'Seller', 'Mediator'];
		$scope.offerStatusTypes = {
			'Drafted': [{value: 'Placed', text: 'Placed'}, {value: 'Cancelled', text: 'Cancelled'}],
			'Placed': [{value: 'Cancelled', text: 'Cancelled'}],
			'Completed': [],
			'Cancelled':[]
		};
		$scope.tabs = [
			{title: 'MY OFFERS', type: 'MyOffers', content: 'Drafted'},
			{title: 'RECEIVED OFFERS', type: 'ReceivedOffers', content: 'Drafted'},
			{title: 'PUBLIC OFFERS', type: 'PublicOffers', content: 'Placed'}
		];
		$scope.myDate = new Date();
		$scope.minDate = new Date(
			$scope.myDate.getFullYear(),
			$scope.myDate.getMonth() - 2,
			$scope.myDate.getDate());
		$scope.maxDate = new Date(
			$scope.myDate.getFullYear(),
			$scope.myDate.getMonth() + 2,
			$scope.myDate.getDate());
		$scope.filterOffer = function (item) {
			var re = new RegExp($scope.searchOfferText, 'i');
			return !$scope.searchOfferText || re.test(item.name) || re.test(item.offerStatus);
		};
		$scope.changeSelOffer = function (offer) {

			$scope.selOffer = offer;
			/*	$scope.updateOffer = angular.copy(offer);*/
			/*$scope.findOne(false);*/
			/*$scope.panelChange($scope.selOffer,'view',false);*/

			$scope.findOne(false);
			/*$scope.findOne();*/
		};
		$scope.cloneOffer = function () {
			var offer = this.offer ? this.offer:this.selOffer;
            offersApi.cloneOffer(offer._id).then(function (response) {
                $scope.offers.push(new Offers(response.data));
                $scope.selOffer = new Offers(response.data);
                $scope.offer = new Offers(response.data);
				$scope.panelChange($scope.selOffer, 'view', false);
				/*$scope.changeSelOffer(response);*/
				/*$scope.resetCall(true);*/
				/*$location.path('offers');*/
            }, function (response) {
                $scope.error = response.data.message;
			});
		};


		//$timeout(function () { $mdSidenav('left').open(); }, false);
		$scope.toggleSideNavigation = function (id, val) {

			if (val === '') {
				$timeout(function () {
					panelTrigger(id).open().then(function () {
						console.log('Trigger is done');
					});
				});
			} else {
				val = val;
			}
			if (val === 'toggle') {
				panelTrigger(id).toggle().then(function () {
					console.log('Trigger is done');
				});
			} else if (val === 'open') {
				panelTrigger(id).open().then(function () {
					console.log('Trigger is done');
				});
			} else if (val === 'close') {
				panelTrigger(id).close().then(function () {
					console.log('Trigger is done');
				});
			} else if (val === 'isOpen') {
				panelTrigger(id).isOpen().then(function () {
					console.log('Trigger is done');
				});
			}
		};

		$scope.orderProductCount=function(companyoffer){
			if(companyoffer) {
				$scope.selOffer=companyoffer;
			}
			$scope.selectedproducts=[];
			if($scope.selOffer){
				$scope.selectedproducts = $scope.selOffer.products.filter(function (product) {
					return product.selected ;
				});
				return $scope.selectedproducts.length>0?false:true;

			}
			if($scope.selectedproducts.length>1){
				return false;
			}else{
				return true;
		   }
		};
		$scope.createOrder = function (categoryOffer) {
			var offer = categoryOffer ? categoryOffer : $scope.selOffer;
            offersApi.createOrder(offer).then(function (response) {
                if (response.data._id) {
                    $location.path('orders' + '/' + response.data._id);
				} else {
					$location.path('orders');
				}
            }, function (response) {
                $scope.error = response.data.message;
			});
		};

		$scope.resetCall = function (update) {
			if (update) {
				$scope.allCategoriesProducts = angular.copy($scope.metaProducts);
			} else {
				$scope.allCategoriesProducts = angular.copy($scope.metaProducts);
			}
			if ($scope.selOffer) {
				$scope.offerUpdateFields();
			}
			/*$scope.tableform.reset();*/
			$scope.selectedproducts = [];
			$scope.my = {role: 'Seller'};
			//DatePicker
			$scope.dt = {};
			$scope.dt = new Date();
			$scope.to = {};
			$scope.to.contact = [];
			$scope.contacts = [];
			$scope.products = [];
			$scope.publicOffer = true;
			$scope.my = {role: 'Seller'};
			$scope.role = 'Seller';
			$scope.offerName = '';


		};
		$scope.changeSelectedProduct = function (update, offer) {

			/*	$scope.selectedproducts = $scope.allCategoriesProducts.filter(function (product) {
			 return product.selected;
			 });*/

			//9663578800
			var product = $scope.allCategoriesProducts[update];
			if (product) {
				/*
				 MOQ is REQUIRED - MOQ
				 Qty is REQUIRED - numberOfUnitsAvailable
				 Price is REQUIRED - unitIndicativePrice
				 Margin is REQUIRED - margin
				 Required By Date is REQUIRED - availableDate
				 Sample Required is REQUIRED - sampleAvailable*/
				product.inventory = product;
				product.MOQ = '';
				product.margin = '';
				if(this.offerType==='Sell' || ($scope.selOffer && $scope.selOffer.offerType==='Sell')){
					product.numberOfUnitsAvailable = product.numberOfUnits - product.offerUnits;
				}
				if(this.offerType==='Buy' || ($scope.selOffer && $scope.selOffer.offerType==='Buy')){
					product.numberOfUnitsAvailable = null;
				}
				product.category = product.product.category ? product.product.category._id : null;
				product.subCategory1 = product.product.subCategory1 ? product.product.subCategory1._id : null;
				product.subCategory2 = product.product.subCategory2 ? product.product.subCategory2._id : null;
				product.subCategory3 = product.product.subCategory3 ? product.product.subCategory3._id : null;
				product.unitIndicativePrice = product.unitPrice;
				product.availableDate = DatePicker.build({}).dt;
				product.sampleAvailable = '';
				product.producer = $scope.authentication.user._id;
			}
			if (product && product.selected && product.numberOfUnitsAvailable > 0) {
				if (offer) {
					$scope.selOffer.products.push(product);
				}
				$scope.selectedproducts = $scope.allCategoriesProducts.filter(function (product) {
					return product.selected;
				});


			} else {
				if (offer) {
					$scope.selOffer.products = $scope.selOffer.products.filter(function (singleP) {
						return singleP.inventory._id !== product.inventory._id;
					});
				} else {
					$scope.selectedproducts = $scope.allCategoriesProducts.filter(function (product) {
						return product.selected;
					});
				}

			}

		};


		$scope.removeProduct=function (index) {
            var removeProduct=$scope.selOffer.products[index];
            var pindex=0;
            angular.forEach($scope.allCategoriesProducts, function (e) {
                if (e._id === removeProduct.inventory._id) {
                    $scope.allCategoriesProducts[pindex].selected=false;
                    $scope.selOffer.products.splice(index, 1);
                }
                pindex++;
            });

        };
		$scope.offerUpdateFields = function () {
			$scope.selOffer.statusArray = $scope.offerStatusTypes[$scope.selOffer.offerStatus];
			if ($scope.selOffer.offerStatus === 'Placed' || $scope.selOffer.offerStatus === 'Drafted') {
				$scope.selOffer.notConfirmed = true;
			} else {
				$scope.selOffer.notConfirmed = false;
			}
			if ($scope.selOffer.user && $scope.selOffer.user._id && $scope.selOffer.user._id === $scope.authentication.user._id) {
				$scope.selOffer.createduser = true;
			} else {
				$scope.selOffer.createduser = false;
			}
			/*
			 $scope.offer.changeStatusDisplay=$scope.offer.onchangestatus===false;*/
			$scope.selOffer.changeStatusDisplay = $scope.selOffer.onchangestatus === false && $scope.selOffer.statusArray !== undefined;
			//if($scope.order.currentStatus==='Placed' || $scope.order.currentStatus==='Drafted' ){

			//}

		};
		$scope.findOne = function (selOfferId) {
			var offerId = null;
			/*$scope.selOffer=null;*/
			if (selOfferId && $stateParams.offerId) {
				offerId = $stateParams.offerId;
			}/*else if(selOrderId) {
			 orderId=null;
			 }*/ else if ($scope.selOffer) {
				offerId = $scope.selOffer._id;
			}
			/*if(!offerId) {*/
			$scope.selOffer = Offers.get({
				offerId: offerId
			});
			$scope.showModal = false;
			$scope.selOffer.$promise.then(function (result) {
				$scope.selOffer.onchangestatus = false;
				$scope.selOffer = result;
				/*$scope.offerUpdateFields();*/
				$scope.statusComments = '';
				/*$scope.selOffer = $scope.selOffer;*/

				$scope.selectedValues = [];

				$scope.resetCall(true);
				$scope.findOfferMessages($scope.selOffer._id);
				$scope.offerUpdateFields();
				$scope.panelChange($scope.selOffer, 'view', selOfferId);

				/*$scope.offerUpdateFields();*/
			});

			/*}else{
			 $location.path('offers');
			 }*/
		};
		$scope.create = function () {
			// Create new Offer object*/
			/*$scope.selectedproducts = $scope.allCategoriesProducts.filter(function (product) {
			 return product.selected;
			 });*/
			var form = this.tableform;
			var products = angular.copy($scope.selectedproducts);
			for (var i = 0; i < $scope.selectedproducts.length; i++) {
				$scope.selectedproducts[i].margin = (($scope.selectedproducts[i].inventory.unitPrice - $scope.selectedproducts[i].unitIndicativePrice) / $scope.selectedproducts[i].inventory.unitPrice) * 100;
				$scope.selectedproducts[i].inventory = $scope.selectedproducts[i]._id;
				/*$scope.selectedproducts[i].sampleAvailable = $scope.selectedproducts[i].sampleAvailable.dt;*/
			}

			$scope.tocontacts = {contacts: [], groups: []};
			$scope.togroups = [];
			$scope.toallcontacts = [];
			for (var j = 0; j < $scope.to.contact.length; j++) {
				var singlecontact = angular.fromJson($scope.to.contact[j]);
				if (singlecontact.firstName) {
					$scope.tocontacts.contacts.push({'contact': singlecontact._id});
					$scope.toallcontacts.push({'contact': singlecontact._id, 'nVipaniUser': singlecontact.nVipaniUser});
					$scope.togroups.push(singlecontact);
				}
				if (singlecontact.name) {
					$scope.tocontacts.groups.push({'group': singlecontact._id});
					for (var k = 0; k < $scope.contactsVsGroup.length; k++) {
						var groupContacts = angular.fromJson($scope.contactsVsGroup[k]);
						if (groupContacts.groups && groupContacts.groups.indexOf(singlecontact._id) >= 0)
							$scope.toallcontacts.push({'contact': groupContacts._id});
						$scope.togroups.push(singlecontact);
					}

				}
			}

			var offer = new Offers();
			offer.name = this.offerName;
			offer.validTill = this.dt;
			offer.offerStatus = 'Placed';
			/*
			 offer.notificationsContacts=$scope.toallcontacts.length>0 ? $scope.toallcontacts : null;*/
			offer.role = this.my.role;
			offer.offerType = this.offerType;
			offer.products = this.selectedproducts;
			offer.isPublic = this.publicOffer;
			offer.producer = $scope.authentication.user._id;
			offer.toContacts = {contacts: $scope.tocontacts.contacts, groups: $scope.tocontacts.groups};
			// Redirect after save
			offer.$save(function (response) {
				/*$location.path('offers');*/
				/*offer.products=products;*/
				/*	response.$promise.then(function (result) {*/
				$scope.offers.push(offer);
				$scope.selOffer = new Offers(offer);
				/*$scope.offer = new Offers(response);*/
				/*			$scope.selOffer=new Offers(response);
				 $scope.offer=new Offers(response);*/
				/*$scope.resetCall(false);*/
				/*form.$setPristine();*/
				/*$scope.selOffer=offer;*/
				// Clear form fields
				/*$scope.selOffer=response;
				 $scope.selOffer=angular.copy($scope.offer);*/
				$scope.changeSelOffer(offer);
				$scope.resetCall(false);

				/*$location.path('offers');
				 $scope.clearFields();*/
				/*});*/
			}, function (errorResponse) {
				$scope.error = errorResponse.data.message;
			});
		};
		$scope.createMessage = function (offerId) {
			// Create new Message object
			var message = new Messages({
				body: this.body,
				offer: offerId
			});

			// Redirect after save
			message.$save(function (response) {
				response.user = $scope.authentication.user;
				$scope.offerMessages.unshift(response);
				$location.path('offers');
			}, function (errorResponse) {
				$scope.error = errorResponse.data.message;
			});
		};

		$scope.remove = function (offer) {
			if (offer) {
				verifyDelete(offer).then(function () {
                    offer.$remove(function () {

						for (var i in $scope.offers) {
							if ($scope.offers [i]._id === offer._id) {
								$scope.offers.splice(i, 1);
							}
						}
						$mdToast.show(
							$mdToast.simple()
								.textContent('Successfully deleted Offer')
								.position('top right')
								.theme('success-toast')
								.hideDelay(6000)
						);
					}, function (errorResponse) {
						$mdToast.show(
							$mdToast.simple()
								.textContent(errorResponse.data.message)
								.position('top right')
								.theme('error-toast')
								.hideDelay(6000)
						);


						$scope.error = errorResponse.data.message;
					});
				});
			}
		};
		$scope.selproducts = '';
		$scope.findOfferMessages = function (offer) {
			$scope.offerMessages = OfferMessages.query({
				offerId: $stateParams.offerId ? $stateParams.offerId : offer
			});
			$scope.offerMessages.$promise.then(function (result) {
			});

		};
		$scope.selectedTab = 'MyOffers';
		$scope.updateOfferStatus = function () {
			var offer = $scope.selOffer;
			/*offer.offerStatus = this.currentStatus;
			 offer.statusComments = this.statusComments;*/
			offer=new Offers({_id:offer._id,offerStatus:this.currentStatus,statusComments:this.statusComments});
			offer.$update(function () {
				offer.onchangestatus = false;
				offer.onchangespaymentstatus = false;
				$scope.currentStatus = '';
				$scope.statusComments = '';
				/*$scope.showModal = !this.showModal;*/
				var j = 0;
				for (j in $scope.offers) {
					if ($scope.offers [j]._id === offer._id) {
						$scope.offers[j] = offer;
					}
				}
				$scope.offerUpdateFields();
				$scope.panelChange(offer, 'view', false);
				$scope.hide();
				$scope.clearFields();
			}, function (errorResponse) {
				$scope.error = errorResponse.data.message;
			});

		};
		$scope.update = function () {
			var offer = $scope.selOffer;
			/*	if($scope.selOffer.offerStatus==='Expired' && $scope.selOffer.user._id===$scope.authentication.user._id){

			 //No Requirements for the Pending offers.
			 /!*$scope.selOffer.offerStatus='Pending';*!/
			 }*/
			$scope.tocontacts = {contacts: [], groups: []};
			if ($scope.selOffer.toAllContacts) {

				$scope.togroups = [];
				$scope.toallcontacts = [];

				for (var j = 0; j < $scope.selOffer.toAllContacts.length; j++) {
					var singlecontact = angular.fromJson(offer.toAllContacts[j]);
					if (singlecontact.firstName) {
						$scope.tocontacts.contacts.push({'contact': singlecontact._id});
						$scope.toallcontacts.push({
							'contact': singlecontact._id,
							'nVipaniUser': singlecontact.nVipaniUser
						});
						$scope.togroups.push(singlecontact);
					}
					if (singlecontact.name) {
						$scope.tocontacts.groups.push({'group': singlecontact._id});
						for (var k = 0; k < $scope.contactsVsGroup.length; k++) {
							var singlecontacts = angular.fromJson($scope.contactsVsGroup[k]);
							if (singlecontacts.groups && singlecontacts.groups.indexOf(singlecontact._id) >= 0)
								$scope.toallcontacts.push({'contact': singlecontacts._id});
							$scope.togroups.push(singlecontact);
						}

					}
				}


			}
			offer.toContacts = {contacts: $scope.tocontacts.contacts, groups: $scope.tocontacts.groups};
			/*
			 offer.notificationsContacts=($scope.toallcontacts && $scope.toallcontacts.length>0 ? $scope.toallcontacts : null);*/
			for (var i = 0; i < offer.products.length; i++) {
				if (offer.offerStatus === 'Placed') {
					offer.products[i].margin = ((offer.products[i].inventory.unitPrice - offer.products[i].unitIndicativePrice) / offer.products[i].inventory.unitPrice) * 100;
				}
				offer.products[i].inventory = offer.products[i].inventory._id;

				/*$scope.products[i].sampleAvailable = $scope.products[i].sampleAvailable.dt;*/
			}

			offer.$update(function () {
				var j = 0;
				for (j in $scope.offers) {
					if ($scope.offers [j]._id === offer._id) {
						$scope.offers[j] = offer;
					}
				}
				$scope.offerUpdateFields();
				$scope.panelChange(offer, 'view', false);

				/*if($scope.offer.offerStatus==='Drafted'){
				 $location.path('offers/' + offer._id + '/edit');
				 }else{
				 $location.path('offers/' + offer._id);
				 }*/
			}, function (errorResponse) {
				$scope.error = errorResponse.data.message;
			});
		};
		// Find a list of Offers
		$scope.find = function (tabSelection) {
			$scope.selectedTab = 'MyOffers';
			$scope.selectedIndex = 0;
			$scope.selOffer = null;
			$scope.offers = Offers.query();
			//$scope.products = Products.query();
			$scope.offers.$promise.then(function (result) {
				$scope.selOffer = $scope.offers[0];
				/*if ($scope.selOffer) {
				 $scope.panelChange($scope.selOffer, 'view', tabSelection);
				 } else {*/

				/*}*/
				$scope.panelChange(null, 'viewAll', tabSelection);
			});

		};
		$scope.offerUpdateStatusEditButton=function (offer) {
			var currentRole='Sale';
			if($scope.authentication.user && offer.user===$scope.authentication.user._id){
				currentRole='Sale';
			}else{
				currentRole='Purchase';
			}
			return ($scope.authentication.user && offer.offerStatus && offer.user===$scope.authentication.user._id && this.offer.offerStatus!=='Drafted' && $scope.offerStatusTypes[offer.offerStatus].length>0);

		};
		$scope.changeStatusOffer = function (offer) {

			$scope.selOffer = offer;
			$scope.offerUpdateFields();
			$scope.showOfferStatusDialog(this.event);
		};
		$scope.panelChange = function (offer, changeStr, tabSelection) {
			$scope.showEdit = false;
			$scope.showAddOffer = false;
			$scope.showAddToGroups = false;
			$scope.showAddGroup = false;
			$scope.showEditGroup = false;
			$scope.showView = false;
			$scope.showViewAll = false;
			$scope.error=null;
			$scope.contactsVsGroup = [];
			$scope.metaProducts = [];
			$scope.allCategoriesProducts = [];
			if (offer !== null && angular.isObject(offer)) {
				if (typeof changeStr === 'string') {
					if (changeStr === 'edit') {
						$scope.showEdit = true;

						/*$scope.findOne(tabSelection);*/
					} else if (changeStr === 'add') {
						$scope.allData($scope.selOffer, function (result) {
							$scope.showAddOffer = true;
						});
					} else if (changeStr === 'view') {
						$scope.selOffer = angular.copy(offer);
						if (offer.offerStatus === 'Drafted') {

							$scope.allData($scope.selOffer, function (result) {
								/*$scope.findOne(tabSelection);*/
								$scope.showView = true;
							});
							$scope.offerUpdateFields();

						} else {
							/*$scope.findOne(tabSelection);*/
							$scope.showView = true;
							$scope.offerUpdateFields();
						}


					}
					/*else {
					 $scope.showView = true;

					 }*/

				} else {
					$scope.showView = true;

				}
			}
			else {
				if (changeStr === 'add') {
					/*$scope.toggleSideNavigation('left','close');*/

					$scope.showAddOffer = true;
					$scope.selOffer=offer;
					$scope.resetCall(false);
					$scope.selectedIndex=0;
					$scope.allData($scope.showAddOffer, function (result) {
						/*$scope.findOne(tabSelection);*/
					});
				} else if (changeStr === 'viewAll') {
					$scope.showViewAll = true;
				}
				/*else if (changeStr === 'addToGroups') {
				 $scope.showAddToGroups = true;
				 } else if (changeStr === 'group') {
				 $scope.showAddGroup = true;
				 }else {

				 $scope.showView = true;

				 }*/
			}


		};

		$scope.allData = function (result, callback) {
			$scope.contactsVsGroup = [];
			$scope.allCategoriesProducts = [];
			/*$scope.metaProducts = Inventories.query();*/
            inventoryProducts.inventoryProducts().then(function (results) {
				$scope.allCategoriesProducts = angular.copy(results.data);
				$scope.findContactVSGroups();

				if($scope.selOffer && $scope.selOffer.offerStatus==='Drafted') {
					 $scope.pIndex = 0;
					angular.forEach($scope.selOffer.products, function (e) {
						$scope.allCategoriesProducts.filter(function (product) {
							if (e.inventory._id === product._id) {
                                if (((product.numberOfUnits - product.offerUnits > 0 && $scope.selOffer && $scope.selOffer.offerType === 'Sell') || ($scope.selOffer.offerType === 'Buy')) && !product.disabled) {
                                    product.selected = true;

                                    if ($scope.selOffer && $scope.selOffer.offerType === 'Sell') {
                                        $scope.selOffer.products[$scope.pIndex].numberOfUnitsAvailable = product.numberOfUnits - product.offerUnits;
                                    }

                                    if ($scope.selOffer && $scope.selOffer.offerType === 'Buy') {
                                        $scope.selOffer.products[$scope.pIndex].numberOfUnitsAvailable = 0;
                                    }
                                } else {
                                    $scope.selOffer.products.splice($scope.pIndex, 1);
                                }
                            }
						});
                        $scope.pIndex++;
					});
				}

				callback();
			},function (response) {
                $scope.error = response.data.message;
            });
		};

		/*$scope.findMainCategories = function () {
			$scope.selOffer = null;
            $http.get('queryMainCategories').then(function (response) {
                $scope.categories = response.data;
			});
		};

		$scope.findSubcategory2Offers = function () {
            $http.get('subCategory2Offers/' + $stateParams.categoryId).then(function (response) {
                $scope.subCategoryOffers = response.data;
				$scope.selectedSubCategoryOffer = $scope.subCategoryOffers[0];
			});
		};

		$scope.findCategory = function () {
			$scope.category = Categories.get({
				categoryId: $stateParams.categoryId
			});
		};*/


		$scope.findReceivedOffers = function (tabSelection) {
			$scope.selectedTab = 'ReceivedOffers';
			$scope.selectedIndex = 1;
			$scope.offers = [];
			$scope.selOffer = null;
			/*$scope.selOffer=null;*/
            offersApi.receivedOffers().then(function (response) {
                $scope.offers = response.data;
				if ($scope.offers.length > 0) {
					$scope.selOffer = $scope.offers[0];
					/*$scope.findOne(tabSelection);*/

				}
				$scope.panelChange(null, 'viewAll', tabSelection);
			},function (response) {
                $scope.error = response.data.message;
            });
		};

		$scope.findSpecific = function () {
			/*	$scope.orders=[];*/
            offersApi.offersQuery().then(function (response) {
                $scope.offers = response.data;
				if ($scope.offers.length > 0) {
					$scope.selOffer = $scope.offers[0];
					/*$scope.findOne(true);*/
					$scope.panelChange($scope.selOffer, 'view', true);
				}
				/*$scope.registerUser = response;
				 $scope.userCategories = ['Trader', 'Agent', 'Consumer', 'Producer'];
				 $scope.selectedItem=$scope.registerUser.userCategories;
				 $scope.phones = [];

				 //return response;
				 if($scope.registerUser.status!=='Register Request'){
				 $location.path('/');
				 }*/
			},function (response) {
                $scope.error = response.data.message;
            });
		};


		$scope.findInit = function () {
			$scope.my = {role: 'Seller'};
			//DatePicker
			$scope.dt = new Date();
			$scope.to = {};
			$scope.to.contact = [];
			$scope.contacts = [];
			$scope.products = [];
			$scope.publicOffer = true;
			$scope.my = {role: 'Seller'};
			$scope.role = 'Seller';

			$scope.contactsVsGroup = [];
			if ($stateParams.offerId !== undefined) {
				$scope.selOffer = Offers.get({
					offerId: $stateParams.offerId
				});

				$scope.selOffer.$promise.then(function (result) {
					if ($scope.selOffer.user._id === $scope.authentication.user._id) {
						$scope.selectedTab = 'MyOffers';
						$scope.selectedIndex = 0;
					} else {
						$scope.selectedTab = 'ReceivedOffers';
						$scope.selectedIndex = 1;
					}
					$scope.findOne(true);
				});
			} else {
				$scope.find(false);

			}


		};


		$scope.changeSelectionTab = function (type) {
			if (type === 'MyOffers') {
				$scope.selOffer = null;
				$scope.find(false);
			} else if (type === 'ReceivedOffers') {
				$scope.selOffer = null;
				$scope.findReceivedOffers(false);
			} else if (type === 'PublicOffers') {
				$scope.selOffer = null;
                publicOfferCategories.findMainCategories().then(function (response) {
                    $scope.categories = response.data;
                },function (response) {
                    $scope.error = response.data.message;
                });
			}
		};
		$scope.allCategories=function () {

            publicOfferCategories.findCategory().then(function (response) {
                $scope.category=response.data;
            },function (response) {
                $scope.error = response.data.message;
            });
			publicOfferCategories.findSubcategory2Offers().then(function (response) {
                $scope.subCategoryOffers = response.data;
                $scope.selectedSubCategoryOffer = $scope.subCategoryOffers[0];
            },function (response) {
                $scope.error = response.data.message;
            });
        };
		$scope.select2Options = {
			formatNoMatches: function (term) {
				/*console.log('Term: ' + term);*/
				var message = 'No Contacts, Groups';
				if (!$scope.$$phase) {
					$scope.$apply(function () {
						$scope.noResultsTag = term;
					});
				}
				return message;
			},
			initSelection: function (element, callback) {
				callback($element.data('$ngModelController').$modelValue);
			}
		};

		$scope.addTag = function () {
			$scope.tags.push({
				id: $scope.tags.length,
				name: $scope.noResultsTag
			});
		};
		$scope.$watch('noResultsTag', function (newVal, oldVal) {
			if (newVal && newVal !== oldVal) {
				$timeout(function () {
					var noResultsLink = $element.find('.select2-no-results');
					/*console.log(noResultsLink.contents());*/
					$compile(noResultsLink.contents())($scope);
				});
			}
		}, true);
		$scope.findContactVSGroups = function () {
			/*	this.timeout(5000);*/
			var results = Contacts.query();
			var resultsG = Groups.query();

			$scope.selfcontact = [];
			results.$promise.then(function (result) {
				angular.forEach(result, function (e) {
					if (e.nVipaniUser !== null && (e.nVipaniRegContact !== null && e.nVipaniRegContact === true && (e.nVipaniUser === e.user || e.nVipaniUser === e.user._id))) {
						$scope.selfcontact.push(e);
					} else {
						if (!e.disabled)
							$scope.contactsVsGroup.push(e);
					}
				});
			});
			resultsG.$promise.then(function (result) {
				angular.forEach(result, function (e) {
					$scope.contactsVsGroup.push(e);

				});
			});

		};

		$scope.findContactVSGroupsEdit = function (response) {


			$timeout(function () {
				var results = Contacts.query();
				var resultsG = Groups.query();
				$scope.offerExistingContacts = [];
				response.contactsVsGroup = [];
				response.selectedContacts = [];
				$scope.selfcontact = [];
				$scope.selContactValues = [];
				$scope.selGroupValues = [];
				response.selectedContactsText = [];
				response.toAllContacts = [];
				var resultContacts = response.toContacts;


				/*angular.forEach(resultContacts.groups, function (e) {
				 var deferred = $q.defer();
				 $scope.selGroupValues.push(e.group._id);
				 setTimeout(function () {
				 deferred.resolve();
				 //console.log('long-running operation inside forEach loop done');
				 }, 100);
				 });
				 angular.forEach(resultContacts.contacts, function (e) {
				 var deferred = $q.defer();
				 $scope.selContactValues.push(e.contact._id);
				 setTimeout(function () {
				 deferred.resolve();
				 //console.log('long-running operation inside forEach loop done');
				 }, 100);
				 });*/
				resultsG.$promise.then(function (gresult) {
					angular.forEach(gresult, function (eg) {
						var tmpValuesg = response.toContacts.groups.filter(function (eachGroup) {
							return eachGroup.group._id === eg._id;
						});
						if (tmpValuesg && tmpValuesg.length > 0) {
							eg.selected = true;

						}
						eg.text = eg.name;
						eg.id = response.selectedContacts.length;
						response.contactsVsGroup.push(eg);
						response.selectedContacts.push({tag: eg.name, eachObject: eg, selected: eg.selected});
						if (eg.selected) {
							response.toAllContacts.push(JSON.stringify(eg));
						}
					});
				});


				results.$promise.then(function (result) {

					angular.forEach(result, function (e) {
						if (e.nVipaniUser !== null && (e.nVipaniRegContact !== null && e.nVipaniRegContact === true && (e.nVipaniUser === e.user || e.nVipaniUser === e.user._id))) {
							$scope.selfcontact.push(e);
						} else {

							//		response.selectedContacts.push(JSON.stringify(e));
							//response.contactsVsGroup.push(e);
							//response.selectedContacts.push(JSON.stringify(e));
							if (!e.disabled) {
								var tmpValues = response.toContacts.contacts.filter(function (eachContact) {
									return eachContact.contact._id === e._id;
								});
								if (tmpValues && tmpValues.length > 0) {
									e.selected = true;

									//}/!*else{*!/
								}
								var text = (e.firstName ? e.firstName : '') + (e.middleName ? ' ' + e.middleName : '') + (e.lastName ? ' ' + e.lastName : '');
								response.contactsVsGroup.push(e);
								response.selectedContacts.push({tag: text, eachObject: e, selected: e.selected});
								if (e.selected) {
									response.toAllContacts.push(JSON.stringify(e));
								}
							}
						}
					});

				});

			});

		};
		$scope.findContacts = function () {
			$scope.contacts = Contacts.query();
		};
		$scope.findGroups = function () {
			$scope.groups = Groups.query();
		};

		$scope.displayEachName = function (item) {
			return (item.eachObject.firstName ? item.eachObject.firstName : item.eachObject.name) + (item.eachObject.middleName ? ' ' + item.eachObject.middleName : '') + (item.eachObject.lastName ? ' ' + item.eachObject.lastName : '');
		};

//Clear Fields Function
		$scope.clearFields = function () {
			$scope.product = '';
			$scope.unitSize = '';
			$scope.unitMeasure = '';
			$scope.numberOfUnits = '';
			$scope.totalQuantity = '';
			$scope.isPublic = true;
			$scope.currentStatus = '';
			$scope.statusComments = '';

		};
		$scope.offerRedirection = function () {
			$location.path('offers');
		};
		$scope.findProducts = function () {
            inventoryProducts.inventoryProducts().then(function (results) {
			/*$scope.metaProducts.$promise.then(function (result) {*/
				$scope.allCategoriesProducts = angular.copy(results.data);
			});
		};

		$scope.selectProduct = function (metaProduct, index) {
			$scope.products[index].product = metaProduct;
			$scope.products[index].unitSize = metaProduct.unitSize;
			$scope.products[index].unitPrice = metaProduct.unitPrice;
			$scope.products[index].unitMeasure = metaProduct.unitMeasure;
			$scope.products[index].unitIndicativePrice = metaProduct.unitPrice;
		};

		$scope.filterByName = function (contacts, typedValue) {
			return contacts.filter(function (contact) {
				var matches_first_name = contact.firstName.indexOf(typedValue) !== -1;
				var matches_last_name = contact.lastName.indexOf(typedValue) !== -1;

				return matches_first_name || matches_last_name;
			});
		};

		$scope.filterByProductName = function (products, typedValue) {
			return products.filter(function (product) {
				var matches_name = angular.lowercase(product.name).indexOf(angular.lowercase(typedValue)) !== -1;
				/*var matches_last_name =  angular.lowercase(product.lastName).indexOf(angular.lowercase(typedValue)) !== -1;*/

				return matches_name;
			});
		};


		$scope.toggleModal = function () {
			$scope.showModal = !$scope.showModal;
		};

		/*$scope.selected = 0;*/
		$scope.select = function (index) {
			$scope.selected = index;
		};


		$scope.showOfferStatusDialog = function (ev) {

			var useFullScreen = ($mdMedia('sm') || $mdMedia('xs'));
			$mdDialog.show({
				template: '<md-dialog aria-label=" Dialog">' +
				'<md-toolbar><div class="md-toolbar-tools"><h2>Offer Status</h2><span flex></span>' +
				'<md-button class="md-icon-button" ng-click="cancel()">' +
				'<md-icon class="fa fa-minus" aria-label="Close dialog"></md-icon></md-button></div></md-toolbar>' +
				'<md-dialog-content class="md-dialog-content">' +
				/*'<md-input-container class="wid-100">' +*/
				'<md-select aria-label="Offer Status" ng-model="currentStatus">' +
				'<md-option ng-repeat="s in selOffer.statusArray">{{s.text}}</md-option>' +
				'</md-select>' +
				/*'</md-input-container>' +*/
				'<md-input-container class="md-block">' +
				'<label>Comments</label>' +
				'<textarea required ng-model="statusComments"></textarea>' +
				'</md-input-container>' +
				'</md-dialog-content>' +
				'<md-dialog-actions>' +
				'<md-button class="md-raised md-primary" ng-click="updateOfferStatus();">Submit</md-button>' +
				'<md-button class="md-raised" ng-click="cancel();">Cancel</md-button></md-dialog-actions>' +
				'</md-dialog>',
				targetEvent: ev,
				scope: $scope,
				clickOutsideToClose: true,
				fullscreen: useFullScreen,
				preserveScope: true
			});

		};


		$scope.hide = function () {
			$mdDialog.hide();
		};
		$scope.cancel = function () {
			$mdDialog.cancel();
		};

		$scope.tag = 'Agriculture';
		$scope.changeType = function (tagVal) {
			$scope.tag = tagVal;
		};


		//$scope.subCategoryOffers[0].expanded = true;

		$scope.collapseAll = function (index) {
			$scope.selectedSubCategoryOffer = $scope.subCategoryOffers[index];
		};

	}

]);
/*
app.controller('OffersEditController'['$scope',function ($scope) {

}]);
app.controller('OffersViewController'['$scope',function ($scope) {
}]);
*/
app.directive('addOffer', function(){
	return {
		restrict:'E',
		templateUrl:'modules/offers/views/create-offer.client.viewnew.html'
	};
});

app.directive('viewOffer', function(){
	return {
		restrict:'E',
		templateUrl:'modules/offers/views/view-offer.client.viewnew.html'
	};
});
app.directive('addOfferStatusHistory', function(){
	return {
		restrict:'E',
		templateUrl:'modules/offers/views/create-offersstatus.client.view.html'
	};
});


app.directive('offercarousel', function () {

	return {
		link: function (scope, element, attrs) {

			element.flexslider({
				animation: 'slide',
				controlNav: true,
				animationLoop: true,
				slideshow: true,
				itemWidth: 210,
				itemMargin: 5,
				asNavFor: '#slider',
			});
		}
	};


});

'use strict';

//Offers service used to communicate Offers REST endpoints
var app=angular.module('offers');
app.factory('Offers', ['$resource',
    function($resource) {
        return $resource('offers/:offerId', { offerId: '@_id'
        }, {
            update: {
                method: 'PUT'
            }
        });
    }
]);
app.factory('DatePicker',[ '$stateParams', '$location', 'Authentication',
    'Offers', 'Contacts', 'Products',
    function($scope, $stateParams, $location, Authentication, Offers, Contacts, Products){
        //Constructer
        var DatePicker = function(){
            //Sets default date as today
            this.dt = new Date();

            //Sets the minimum selectable date as today
            this.minDate =  new Date();

            //Boolean value for opening datePicker popup
            this.opened = false;

            //Sets format shown in the textbox input
            this.format = 'dd-MMMM-yyyy';
        };

        //Method for popup open
        DatePicker.prototype.open = function($event) {
            $event.preventDefault();
            $event.stopPropagation();
            console.log('opened');
            this.opened = true;
        };

        /**
         * Static method, assigned to class
         * Instance ('this') is not available in static context
         */
        DatePicker.build = function (data) {
            return new DatePicker(
                data.dt,
                data.minDate,
                data.opened,
                data.format
            );
        };

        /**
         * Return the constructor function
         */
        return DatePicker;
    }]);

/*
 Contacts Service used for AutoComplete
 */
app.factory('ContactsForOffers',['$stateParams', '$location', 'Authentication',
    'Offers', 'Contacts', 'Products', function($scope, $stateParams, $location, Authentication, Offers, Contacts, Products){
        //AutoComplete methods for Contacts
        $scope.getContacts = function(){
            $scope.contacts = Contacts.query();
        };
        $scope.getContacts();

        $scope.contactsOffer = [];

        $scope.contactSelected = undefined;

        $scope.addContact = function(event){
            console.log(event);
            if(event.keyCode === 13 || event === undefined){
                $scope.contactsOffer.push($scope.contactSelected);
                var index = $scope.contacts.indexOf($scope.contactSelected);
                $scope.contacts.splice(index, 1);
                $scope.contactSelected = '';
            }
        };

        $scope.removeContact = function(contactSel){
            var index = $scope.contactsOffer.indexOf(contactSel);
            if(index >= 0){
                $scope.contactsOffer.splice(index, 1);
                $scope.contacts.push(contactSel);
            }

        };
    }]);

app.factory('contactsGroup',['$http',function($http){
    return {
        contacts:function () {
            return $http.get('contacts');
        },
        groups:function () {
            return $http.get('groups');
        }

    };
}]);
app.factory('inventoryProducts',['$http',function($http){
    return {
        inventoryProducts:function () {
            return $http.get('inventories');
        }

    };
}]);

app.factory('offersApi',['$http',function($http){
    return {
        cloneOffer:function ($offerId) {
            return $http.get('cloneOffer/'+$offerId);
        },
        receivedOffers:function () {
            return $http.get('receivedOffers');
        },
        offersQuery:function () {
            return $http.get('offersQuery');
        },
        createOrder:function (offer) {
            return $http.put('createOrder/' + offer._id, offer);
        }
    };
}]);




'use strict';

// Configuring the Articles module
angular.module('orders',[]).run(['Menus',
	function(Menus) {
		// Set top bar menu items
	/*	Menus.addMenuItem('topbar', 'Orders', 'orders', 'dropdown', '/orders(/create)?');
		Menus.addSubMenuItem('topbar', 'orders', 'List Orders', 'orders');
		Menus.addSubMenuItem('topbar', 'orders', 'New Order', 'orders/create');*/
		Menus.addMenuItem('topbar', 'Orders', 'orders', 'orders');
	}
]);

'use strict';

//Setting up route
angular.module('orders').config(['$stateProvider',
	function($stateProvider) {
		// Orders state routing
		$stateProvider.
		state('listOrders', {
			url: '/orders',
			templateUrl: 'modules/orders/views/list-orders.client.viewnew.html'
		}).
		state('createOrder', {
			url: '/orders/create',
			templateUrl: 'modules/orders/views/create-order.client.view.html'
		}).
		state('viewOrder', {
			url: '/orders/:orderId',
			templateUrl: 'modules/orders/views/list-orders.client.viewnew.html'
		}).
		state('editOrder', {
			url: '/orders/:orderId/edit',
			templateUrl: 'modules/orders/views/list-orders.client.viewnew.html'
		}).
		state('changeOrder', {
			url: '/orders/:orderId/change',
			templateUrl: 'modules/orders/views/change-order.client.view.html'
		});
	}
]);

'use strict';

// Orders controller
var app = angular.module('orders');
app.controller('OrdersController', ['$scope', '$sce', '$stateParams', '$filter', '$location', '$http', 'Authentication', 'Orders', 'Contacts', 'Products', 'Messages', 'OrderMessages', 'DatePicker', '$element', 'panelTrigger', '$timeout', '$mdDialog', '$mdMedia','verifyDelete','$mdToast',
    function ($scope, $sce, $stateParams, $filter, $location, $http, Authentication, Orders, Contacts, Products, Messages, OrderMessages, DatePicker, $element, panelTrigger, $timeout, $mdDialog, $mdMedia,verifyDelete,$mdToast) {
        $scope.authentication = Authentication;
        $scope.addressTypes=[];
        $scope.billing=false;
        $scope.receving=false;
        $scope.shipping=false;
        $scope.invoice=false;
        $scope.showBilling=false;
        $scope.showShipping=false;
        $scope.singleValueShipping=false;
        $scope.singleValueBilling=false;
        $scope.addressTypesVsDetails=[];
        $scope.selectedPageId=0;
        $scope.selectedPage={};
        $scope.view=true;
        $scope.payment=false;
        $scope.searchText = '';
        $scope.addresspage=false;
        $scope.orderPaymentOptions=[];
        $scope.selectedIndex=0;
        $scope.selectedTab='MyOrders';
        $scope.paymentOptionselection=[];
        $scope.selectedTag=false;
        $scope.tabs = [
            { title:'MY ORDERS', type:'MyOrders'},
            { title:'RECEIVED ORDERS',type:'ReceivedOrders'}
        ];
        $scope.paymentOptions = {
            installments: 'Installments',
            lineOfCredit: 'Line of Credit',
            payNow: 'Pay Now',
            payOnDelivery: 'Pay On Delivery'
        };
        $scope.pages=[];
        $scope.dt = {};
        $scope.dt = DatePicker.build({});
        $scope.products = [];
        $scope.product = [];
        $scope.metaProducts = [];
        $scope.paymentHistory=[];
        $scope.statusHistory=[];
        $scope.newproducts=[];
        $scope.currentOrderPaymentStatus=[];
        $scope.currentOrderStatus=[];
        $scope.workspaces =[];
        $scope.orderTTypes={
            Sale: 'Sell',
            Purchase: 'Buy',

        };
        $scope.statusTypes=[{value:'Drafted',text:'Drafted'},{value:'Placed',text:'Placed'},{value: 'Cancelled',text:'Cancelled'},{value: 'Confirmed',text:'Confirmed'}, {value: 'Rejected',text:'Rejected'}, {value:'InProgress',text:'InProgress'}, {value:'Shipped',text:'Shipped'},{value:'Delivered',text:'Delivered'},{value:'Completed',text:'Completed'}];


            $scope.orderStatusTypes=
            {'Sell':{
                'Sale':{
                    'Drafted':[{value:'Placed',text:'Placed'},{value:'Confirmed',text:'Confirmed'},{value: 'Cancelled',text:'Cancelled'}],
                    'Placed':[{value: 'Confirmed',text:'Confirmed'},{value: 'Cancelled',text:'Cancelled'},{value: 'Rejected',text:'Rejected'}],
                    'Confirmed':[{value:'InProgress',text:'InProgress'},{value:'Shipped',text:'Shipped'}],
                    'Shipped':[{value:'Delivered',text:'Delivered'}],
                    'Delivered':[{value:'Completed',text:'Completed'}],
                    'Disputed':[],
                    'InProgress':[{value:'Shipped',text:'Shipped'},{value:'Delivered',text:'Delivered'}],
                    'Rejected':[],
                    'Completed':[],
                    'Resolved':[{value:'Completed',text:'Completed'}],
                    'Returned':[],
                    'Cancelled':[]
                },
                'Purchase':{
                    'Drafted':[{value:'Placed',text:'Placed'},{value: 'Cancelled',text:'Cancelled'}],
                    'Placed':[{value: 'Cancelled',text:'Cancelled'}],
                    'Confirmed':[],
                    'Shipped':[{value:'Delivered',text:'Delivered'}],
                    'Delivered':[{value:'Disputed',text:'Disputed'}],
                    'InProgress':[],
                    'Completed':[],
                    'Rejected':[],
                    'Disputed':[{value:'Resolved',text:'Resolved'},{value:'Returned',text:'Returned'}],
                    'Resolved':[],
                    'Returned':[],
                    'Cancelled':[]
                }},'Buy':{
                'Sale':{	'Drafted':[{value:'Placed',text:'Placed'},{value: 'Cancelled',text:'Cancelled'}],
                    'Placed':[{value: 'Cancelled',text:'Cancelled'}],
                    'Confirmed':[],
                    'Shipped':[{value:'Delivered',text:'Delivered'}],
                    'Delivered':[{value:'Disputed',text:'Disputed'}],
                    'InProgress':[],
                    'Completed':[],
                    'Rejected':[],
                    'Disputed':[{value:'Resolved',text:'Resolved'},{value:'Returned',text:'Returned'}],
                    'Resolved':[],
                    'Returned':[],
                    'Cancelled':[]
                },'Purchase':{
                    'Drafted':[{value:'Placed',text:'Placed'},{value:'Confirmed',text:'Confirmed'},{value: 'Cancelled',text:'Cancelled'}],
                    'Placed':[{value: 'Confirmed',text:'Confirmed'},{value: 'Cancelled',text:'Cancelled'},{value: 'Rejected',text:'Rejected'}],
                    'Confirmed':[{value:'InProgress',text:'InProgress'},{value:'Shipped',text:'Shipped'}],
                    'Shipped':[{value:'Delivered',text:'Delivered'}],
                    'Delivered':[{value:'Completed',text:'Completed'}],
                    'Disputed':[],
                    'InProgress':[{value:'Shipped',text:'Shipped'},{value:'Delivered',text:'Delivered'}],
                    'Rejected':[],
                    'Completed':[],
                    'Resolved':[{value:'Completed',text:'Completed'}],
                    'Returned':[],
                    'Cancelled':[]
                }
            }};
        $scope.changeSelectionTab=function (type) {

            if(type==='MyOrders'){

                $scope.find(false);
            }else if(type==='ReceivedOrders'){
                $scope.findSpecific(false);
            }
        };
        $scope.orderUpdateStatusEditButton=function (order) {
            $scope.currentRole='Sale';
            if(!this.order.offer || this.order.offer.user===this.authentication.user._id){
                this.order.createduser=true;
                $scope.currentRole='Sale';
                if(!this.order.offer && ((angular.isObject(this.order.user)&& this.order.user._id!==$scope.authentication.user._id) || (!angular.isObject(this.order.user) && this.order.user!==this.authentication.user._id))) {
                    $scope.currentRole = 'Purchase';

                }

            }else{
                $scope.order.createduser=false;
                $scope.currentRole='Purchase';
            }
            if(this.order.offer)
                this.order.statusArray=(($scope.orderStatusTypes[this.order.offer.offerType])[$scope.currentRole])[this.order.currentStatus];
                else
                this.order.statusArray= (($scope.orderStatusTypes.Sell[$scope.currentRole])[this.order.currentStatus]);

            return (this.order.currentStatus!=='Drafted' &&  this.order.statusArray.length>0);

        };
        $scope.showOrderDialog = function (ev) {
            var useFullScreen = ($mdMedia('sm') || $mdMedia('xs'));
            $mdDialog.show({
                template: ('<md-dialog aria-label=" Dialog">' +
                    '<md-toolbar><div class="md-toolbar-tools"><h2>Order Status</h2><span flex></span>' +
                    '<md-button class="md-icon-button" ng-click="cancel()">' +
                    '<md-icon class="fa fa-minus" aria-label="Close dialog"></md-icon></md-button></div></md-toolbar>' +
                    '<md-dialog-content class="md-dialog-content">' +
                    '<md-input-container><md-select  ng-model="currentStatus" placeholder="Select a state">' +
                    '<md-option ng-value="s.value" ng-repeat="s in selOrder.statusArray">{{s.text}}</md-option></md-select> </md-input-container>' +
                    '<md-datepicker  ng-if="currentStatus && currentStatus===\'Confirmed\'" ng-model="selOrder.deliveryDate" md-placeholder="Delivery date">'+
                    '</md-datepicker>'+
                    '<md-input-container class="md-block">' +
                    '<label>Comments</label>' +
                    '<textarea required ng-model="statusComments"></textarea>' +
                    '</md-input-container>' +
                    '</md-dialog-content>' +
                    '<md-dialog-actions>' +
                    '<md-button class="md-raised md-primary" ng-disabled="currentStatus===\'Confirmed\' && !selOrder.deliveryDate" data-ng-click="updateOrderStatus(true)">Submit</md-button>' +
                    '<md-button class="md-raised md-accent" ng-click="cancel();">Cancel</md-button></md-dialog-actions>' +
                    '</md-dialog>'),
                targetEvent: ev,
                scope: $scope,
                clickOutsideToClose: true,
                fullscreen: useFullScreen,
                preserveScope: true
            });

        };

        $scope.hide = function () {
            $mdDialog.hide();
        };
        $scope.cancel = function () {
            $mdDialog.cancel();
        };



        $scope.submitOrder=function(){
            $scope.selOrder.currentStatus=$scope.pages[$scope.selectedPageId].currentStatus;
            $scope.update();
        };
        $scope.next=function () {

            $scope.selectedPageId = $scope.selectedPageId + 1;
            var page = $scope.pages[$scope.selectedPageId];
            /*	if(isCompleted($scope.pages[$scope.selectedPageId])) {
             if ($scope.pages[$scope.selectedPageId].addresspage) {
             $scope.updateAddress();
             } else if ($scope.pages[$scope.selectedPageId].view) {
             $scope.update();
             }else if ($scope.pages[$scope.selectedPageId].payment) {
             $scope.update();
             }
             }*/
            if(page.payment){
                $scope.view=false;
                $scope.payment=page.payment;
                $scope.addresspage=false;
                $scope.updateAddress();
            }
            if(page.addresspage){
                $scope.view=false;
                $scope.payment=false;
                $scope.addresspage=page.addresspage;

                $scope.update();
            }
            if(page.view){
                $scope.view=page.view;
                $scope.payment=false;
                $scope.addresspage=false;
                /*$scope.update();*/
            }
            if(page.confirm){
                $scope.selOrder.currentStatus='Placed';
                $scope.update();
            }

        };
        $scope.prev=function (oldPages) {
            var oldPage=$scope.pages[$scope.selectedPageId];
            $scope.selectedPageId = this.page.number;
            var page = $scope.pages[this.page.number];

            if(oldPage.payment){
                $scope.update();
                if(page.payment){
                    $scope.view=false;
                    $scope.payment=page.payment;
                    $scope.addresspage=false;

                }
                if(page.addresspage){
                    $scope.view=false;
                    $scope.payment=false;
                    $scope.addresspage=page.addresspage;
                }
                if(page.view){
                    $scope.view=page.view;
                    $scope.payment=false;
                    $scope.addresspage=false;
                }
            }
            if(oldPage.addresspage){
                $scope.updateAddress();
                if(page.payment){
                    $scope.view=false;
                    $scope.payment=page.payment;
                    $scope.addresspage=false;

                }
                if(page.addresspage){
                    $scope.view=false;
                    $scope.payment=false;
                    $scope.addresspage=page.addresspage;
                }
                if(page.view){
                    $scope.view=page.view;
                    $scope.payment=false;
                    $scope.addresspage=false;
                }
            }
            if(oldPage.view){
                $scope.update();
                if(page.payment){
                    $scope.view=false;
                    $scope.payment=page.payment;
                    $scope.addresspage=false;

                }
                if(page.addresspage){
                    $scope.view=false;
                    $scope.payment=false;
                    $scope.addresspage=page.addresspage;
                }
                if(page.view){
                    $scope.view=page.view;
                    $scope.payment=false;
                    $scope.addresspage=false;
                }
            }



        };
        $scope.isActive = function (page)
        {
            $scope.selectedPage = $scope.pages[$scope.selectedPageId];

            return ($scope.selectedPage && $scope.selectedPage.title === page.title);
        };
        $scope.isCompleted = function (page)
        {
            var index = $scope.pages.indexOf(page);
            return (index < $scope.selectedPageId);
        };
        $scope.nextShouldBeDisabled = function ()
        {
            if(isNaN(this.selOrder && this.selOrder.totalAmount)){
                return true;
            }else if($scope.selectedPageId >= $scope.pages.length - 1){
                return true;
            }else if($scope.view){
                return $scope.orderValidation(true);
            }else if ($scope.payment){
                return !$scope.selOrder.selectedPayMentOptions.selection;
            }else if($scope.addresspage){
                return !$scope.selOrder.billingAddress || $scope.selOrder.billingAddress.length===0 || !$scope.selOrder.shippingAddress || $scope.selOrder.shippingAddress.length===0 || $scope.selOrder.billingAddress.pinCode.length===0 || $scope.selOrder.shippingAddress.pinCode.length===0;

            }

        };

        $scope.prevShouldBeDisabled = function ()
        {
            return ($scope.selectedPageId <= 0);
        };
        $scope.selectTabs=function () {
            $scope.selectedPageId=0;
            if($scope.addresspage){
                $scope.selectedPageId=1;
            }else if($scope.payment){
                $scope.selectedPageId=2;
            }


            if($scope.selOrder.currentStatus==='Drafted'){
                if(! $scope.selOrder.offer || $scope.selOrder.offer.offerType==='Sell'){
                    $scope.pages =
                        [
                            { id: 'step1', number:0,title: $scope.selOrder.orderType+' Order',active:true,view:true,isDifferentUser:true ,currentStatus:'Drafted'},
                            { id: 'step2', number:1, title: 'Addresses',active:false,addresspage:true,isDifferentUser:true ,currentStatus:'Drafted'},
                            { id: 'step3', number:2,title: 'Payment Details',active:false,payment:true,isDifferentUser:true ,currentStatus:'Placed'}
                        ];
                }else if($scope.selOrder.offer.offerType==='Buy'){
                    $scope.pages =
                        [
                            { id: 'step1',  number:0, title: $scope.selOrder.orderType+' Order',active:true,view:true,isDifferentUser:true ,currentStatus:'Drafted'},
                            { id: 'step2', number:1, title: 'Addresses',active:false,addresspage:true,isDifferentUser:true ,currentStatus:'Drafted'},
                            { id: 'step3', number:2, title: 'Payment Details',active:false,payment:true,isDifferentUser:true ,currentStatus:'Placed'}
                        ];
                }


            }else{
                $scope.alldetails =[
                    { id: 'step1', title: 'Placed Order',active:false,isDifferentUser:false,pageIndex:0,parentStatus:'Placed',complete:false,currentStatus:'Placed',date:''},
                    { id: 'step2', title: 'Cancel Order',active:true,isDifferentUser:false,pageIndex:1,parentStatus:'Placed',complete:false,currentStatus:'Cancelled',date:''},
                    { id: 'step3', title: 'Confirmed Order',active:false,isDifferentUser:false,pageIndex:2,parentStatus:'Placed',complete:false,currentStatus:'Confirmed',date:''},
                    { id: 'step4', title: 'Reject Order',active:true,isDifferentUser:false,pageIndex:3,parentStatus:'Placed',complete:false,currentStatus:'Rejected',date:''},
                    { id: 'step5', title: 'InProgress Order',active:false,isDifferentUser:false,pageIndex:4,parentStatus:'Confirmed',complete:false,currentStatus:'InProgress',date:''},
                    { id: 'step6', title: 'Shipped Order',active:false,isDifferentUser:false,pageIndex:5,parentStatus:'InProgress',complete:false,currentStatus:'Shipped',date:''},
                    { id: 'step7', title: 'Shipped Order',active:false,isDifferentUser:false,pageIndex:6,parentStatus:'Confirmed',complete:false,currentStatus:'Shipped',date:''},
                    { id: 'step8', title: 'Delivered Order',active:false,isDifferentUser:false,pageIndex:7,parentStatus:'Shipped',complete:false,currentStatus:'Delivered',date:''},
                    { id: 'step9', title: 'Disputed Order',active:true,isDifferentUser:false,pageIndex:8,parentStatus:'Delivered',complete:false,currentStatus:'Disputed',date:''},
                    {id: 'step10', title: 'Completed Order', active: false, isDifferentUser: false, pageIndex: 9, parentStatus: 'Delivered', complete: false, currentStatus: 'Completed', date: ''},
                    {id: 'step11', title: 'Pending Order', active: false, isDifferentUser: false, pageIndex: 10, parentStatus: 'Disputed', complete: false, currentStatus: 'Pending', date: ''},
                    {id: 'step12', title: 'Resolved Order', active: false, isDifferentUser: false, pageIndex: 11, parentStatus: 'Disputed', complete: false, currentStatus: 'Resolved', date: ''},
                    {id: 'step13', title: 'Completed Order', active: false, isDifferentUser: false, pageIndex: 12, parentStatus: 'Resolved', complete: false, currentStatus: 'Completed', date: ''},
                    {id: 'step14', title: 'Returned Order', active: true, isDifferentUser: false, pageIndex: 13, parentStatus: 'Disputed', complete: false, currentStatus: 'Returned', date: ''},
                    {id: 'step15', title: 'Completed Order', active: false, isDifferentUser: false, pageIndex: 14, parentStatus: 'Returned', complete: false, currentStatus: 'Completed', date: ''},

                ];
                $scope.trackdetails=[];


                var completed=false;
                var statushistoryLastStatus='';
                for(var j=0;j<$scope.alldetails.length;j++) {
                    var singleobject = $scope.alldetails[j];
                    if ($scope.selOrder.statusHistory.length > 0) {
                        var item = $scope.selOrder.statusHistory.filter(function (item) {
                            return item.status === singleobject.currentStatus;
                        });
                        if (item.length > 0 && !completed && $scope.trackdetails.filter(function (eachitem) {
                                return eachitem.currentStatus === singleobject.currentStatus;
                            }).length===0) {
                            singleobject.complete = true;
                            singleobject.date = item[0].date;
                            $scope.trackdetails.push(singleobject);
                            if ($scope.selOrder.statusHistory[$scope.selOrder.statusHistory.length-1].status === singleobject.currentStatus) {
                                completed = true;
                                statushistoryLastStatus=singleobject.currentStatus;
                                if (singleobject.active) {
                                    break;
                                }else{
                                    continue;
                                }

                            }

                        }if($scope.trackdetails.length===0){
                            continue;
                        }

                        if (completed) {
                            if (!singleobject.active && statushistoryLastStatus===singleobject.parentStatus){
                                statushistoryLastStatus=singleobject.currentStatus;
                                $scope.trackdetails.push(singleobject);
                            }
                        }
                    }
                }
                if($scope.trackdetails.length===0){
                    $scope.trackdetails=$scope.alldetails.filter(function (item) {
                        return !item.active;
                    });
                }

                /*else{
                 $scope.trackdetails=$scope.alldetails.filter(function (item) {
                 return !item.active;
                 });
                 }
                 }*/
                for(var i=0;i<$scope.trackdetails.length;i++){
                    $scope.trackdetails[i].complete=true;
                    if($scope.trackdetails[i].currentStatus===$scope.selOrder.currentStatus){
                        break;
                    }

                }
            }

        };

        $scope.addAddress=function () {
            $scope.address=this.address;
            $scope.updateAddress();
        };

        /*$scope.next=function () {

         };*/
        //{value:'InProgress',text:'InProgress']}, {value:'Shipped',text:'Shipped'},{value:'Delivered',text:'Delivered'},{value:'Completed',text:'Completed'}];

        $scope.paymentType=[{value:'Cash',text:'Cash'}, {value:'Cheque',text:'Cheque'}, {value:'Card',text:'Card'}, {value:'NEFT',text:'NEFT'},{value: 'IMPS',text:'IMPS'},{value: 'RTGS',text:'RTGS'}, {value: 'Other',text:'Other'}];
        //	$scope.paymentStatusList=[{value:'YetToPay',text:'YetToPay'}, {value:'AdvancePaid',text:'AdvancePaid'}, {value:'PartialPayment',text:'PartialPayment'}, {value:'Paid',text:'Paid'}];



        $scope.paymentStatusList={
            'YetToPay':[{value:'AdvancePaid',text:'AdvancePaid'}, {value:'PartialPayment',text:'PartialPayment'}, {value:'Paid',text:'Paid'}],
            'AdvancePaid':[{value:'PartialPayment',text:'PartialPayment'}, {value:'Paid',text:'Paid'}],
            'PartialPayment':[{value:'PartialPayment',text:'PartialPayment'},{value:'Paid',text:'Paid'}],
            //'Paid':[{value:'Paid',text:'Paid'}]
        };
        $scope.roles = ['Buyer', 'Seller','Mediator'];
        //$scope.role=['Buyer'];
        $scope.my = { role: 'Buyer' };
        /*if($scope.order){
         $scope.orderUpdateFields();
         }*/
        // Create new Order
        $scope.create = function() {
            var tempProducts = angular.copy($scope.products);
            // Create new Order object
            for (var i = 0; i < $scope.products.length; i++) {
                $scope.products[i].product = $scope.products[i].product._id;
            }

            var tempBuyer = angular.copy($scope.buyer);
            var tempSeller = angular.copy($scope.seller);
            var tempMediator = angular.copy($scope.mediator);

            if($scope.my.role==='Buyer') {
                $scope.buyer = {};
                if ($scope.selfcontact.length>0) {
                    $scope.buyer.contact = $scope.selfcontact[0]._id;
                    $scope.buyer.nVipaniUser = $scope.selfcontact[0].nVipaniUser;
                }
            }else if (angular.isObject($scope.buyer) && $scope.buyer && $scope.buyer.contact!==undefined ) {
                if($scope.buyer.contact.nVipaniUser){
                    $scope.buyer.nVipaniUser = $scope.buyer.contact.nVipaniUser;
                }

                $scope.buyer.contact = $scope.buyer.contact._id;
            }

            if($scope.my.role==='Seller') {
                $scope.seller={};
                if ($scope.selfcontact.length>0) {
                    $scope.seller.contact = $scope.selfcontact[0]._id;
                    $scope.seller.nVipaniUser = $scope.selfcontact[0].nVipaniUser;

                }
            }else if (angular.isObject($scope.seller) && $scope.seller && $scope.seller.contact!==undefined && $scope.seller.contact._id) {
                if($scope.seller.contact.nVipaniUser){
                    $scope.seller.nVipaniUser = $scope.seller.contact.nVipaniUser;
                }
                $scope.seller.contact = $scope.seller.contact._id;
            }

            if($scope.my.role==='Mediator') {
                $scope.mediator={};
                if ($scope.selfcontact.length>0) {
                    $scope.mediator.contact = $scope.selfcontact[0]._id;
                    $scope.mediator.nVipaniUser = $scope.selfcontact[0].nVipaniUser;
                }
            }else if (angular.isObject($scope.mediator) && $scope.mediator && $scope.mediator.contact!==undefined && $scope.mediator.contact._id && $scope.mediator.contact) {

                if($scope.mediator.contact.nVipaniUser){
                    $scope.mediator.nVipaniUser = $scope.mediator.contact.nVipaniUser;
                }
                $scope.mediator.contact = $scope.mediator.contact._id;
            }
            if(this.products) {
                $scope.totalPrice=0;
                angular.forEach(this.products, function (row) {
                    /*angular.foreach($scope.order.products, function (i, row) {
                     */			$scope.totalPrice += +(row.unitPrice *row.numberOfUnits);
                });
            }
            var order = new Orders ({
                buyer: this.buyer,
                seller: this.seller,
                mediator: this.mediator,
                products: this.products,
                currentStatus: this.currentStatus,
                paymentStatus: this.paymentStatus,
                offer: this.offer,
                deliveryDate:this.deliveryDate,
                role:this.my.role,
                totalAmount:$scope.totalPrice
            });

            $scope.showGroup = function(user) {
                if(user.paymentMode && $scope.paymentType.length) {
                    var selected = $filter('filter')($scope.paymentType, {id: user.paymentMode});
                    return selected.length ? selected[0].text : 'Not set';
                } else {
                    return user.paymentMode || 'Not set';
                }
            };

            $scope.showStatus = function(user) {
                var selected = [];
                if(user.paymentStatus) {
                    selected = $filter('filter')($scope.paymentStatusList, {value: user.status});
                }
                return selected.length ? selected[0].text : 'Not set';
            };
            $scope.showStatusHistory = function(user) {
                var selected = [];
                if(user.status) {
                    selected = $filter('filter')($scope.statusTypes, {value: user.currentStatus});
                }
                return selected.length ? selected[0].text : 'Not set';
            };

            // Redirect after save
            order.$save(function(response) {
                if(response.currentStatus==='Drafted'){
                    $location.path('orders/' + response._id+'/edit');
                }/*else if(response.currentStatus==='Placed') {
                 $location.path('orders/' + response._id + '/change');
                 }*/else{
                    $location.path('orders/' + response._id);
                }

                // Clear form fields
                $scope.clearFields();
            }, function(errorResponse) {
                $scope.products = angular.copy(tempProducts);

                $scope.buyer = angular.copy(tempBuyer);
                $scope.seller = angular.copy(tempSeller);
                $scope.mediator = angular.copy(tempMediator);

                $scope.error = errorResponse.data.message;
            });
        };
        $scope.invoice=function(){
            $http.get('invoiceOrder/' + $scope.selOrder._id, {responseType: 'arraybuffer'}).then(function (response) {
                /*$scope.order.invoicereport(function(response) {*/
                /*$scope.content=response;*/
                /*$scope.content=response;*/
                /*$scope.content=response;*/
                /*alert(response.invoiceurl);*/
                /*	var file = new Blob([response.invoiceurl], { type: 'application/pdf'});
                 var fileURL = URL.createObjectURL(file);
                 window.open(fileURL);*/

                /*var element = angular.element('<a/>');
                 element.attr({
                 href: 'data:attachment/pdf;charset=utf-8,' + encodeURI(response.invoiceurl),
                 target: '_self',
                 download:'test.pdf'
                 })[0].click();*/
                /*var file = new Blob([JSON.stringify(response.invoiceurl)], { type: 'application/pdf' });
                 var fileURL = URL.createObjectURL(file);*/
                /*
                 var file = new Blob([response], { type: 'application/pdf' });
                 var fileURL = URL.createObjectURL(file);*/
                //$scope.pdfContent = $sce.trustAsResourceUrl(fileURL);
                /*	var data = new Blob([response.invoiceurl], { type: 'application/pdf;' });
                 var fileURL = URL.createObjectURL(data);
                 $scope.content = $sce.trustAsResourceUrl(fileURL);*/
                /*FileSaver.saveAs(data, 'text.txt');*/

                var file = new Blob([response.data], {type: 'application/pdf'});
                var fileURL = URL.createObjectURL(file);
                window.open(fileURL);
            },function(errorResponse){
                $scope.error = errorResponse.data.message;
            });
        };
        $scope.showOrderStatusHistorys = function() {
            var selected = [];
            /*if($scope.currentOrder.currentStatus) {
             selected = $scope.orderStatusTypes[$scope.currentOrder.currentStatus];
             }*/
            return selected.length ? selected : 'Not set';
        };

        $scope.orderUpdateFields =function(){
            $scope.selectedTag=false;
            for (var j in $scope.orders) {
                if ($scope.orders [j]._id === $scope.selOrder._id) {
                    $scope.orders[j] = $scope.selOrder;
                }
            }
            $scope.paymentMode=[{value: 'Cash',text:'Cash',isDisplay:$scope.selOrder.paymentModes.cash},{value: 'Cheque',text:'Cheque',isDisplay:$scope.selOrder.paymentModes.cheque},{value: 'Online',text:'Online',isDisplay:$scope.selOrder.paymentModes.online},
                {value: 'NEFT',text:'NEFT',isDisplay:$scope.selOrder.paymentModes.neft},{value: 'IMPS',text:'IMPS',isDisplay:$scope.selOrder.paymentModes.imps},{value: 'RTGS',text:'RTGS',isDisplay:$scope.selOrder.paymentModes.rtgs},
            ];

            if($scope.selOrder.currentStatus==='Placed' || $scope.selOrder.currentStatus==='Drafted'){
                $scope.selOrder.notConfirmed=true;
            }else{
                $scope.selOrder.notConfirmed=false;
            }
            if(!$scope.selOrder.onchangestatus){
                $scope.selOrder.onchangestatus=false;
            }

            /*if($scope.selOrder.offer.offerType === 'Seller'){
             currentRole=$scope.selOrder.orderType;
             if($scope.selOrder.orderType==='Sale'){
             $scope.selOrder.createduser=true;
             }else{
             $scope.selOrder.createduser=false;
             }
             }else{
             $scope.selOrder.createduser=false;
             currentRole='Purchase';
             if($scope.selOrder.orderType === 'Sale'){

             } else{
             //Purchase
             }
             }*/
             $scope.currentRole='Sale';
            if(!$scope.selOrder.offer || $scope.selOrder.offer.user===$scope.authentication.user._id){
                $scope.selOrder.createduser=true;
                $scope.currentRole='Sale';
                if(!$scope.selOrder.offer && ((angular.isObject($scope.selOrder.user)&& $scope.selOrder.user._id!==$scope.authentication.user._id) || (!angular.isObject($scope.selOrder.user) && $scope.selOrder.user!==$scope.authentication.user._id))) {
                    $scope.currentRole = 'Purchase';

                }

            }else{
                $scope.selOrder.createduser=false;
                $scope.currentRole='Purchase';
            }

            if($scope.selOrder.offer) {
                $scope.selOrder.statusArray = (($scope.orderStatusTypes[$scope.selOrder.offer.offerType])[$scope.currentRole])[$scope.selOrder.currentStatus];
            }else{
                $scope.selOrder.statusArray = ($scope.orderStatusTypes.Sell[$scope.currentRole])[$scope.selOrder.currentStatus];
            }
            if($scope.selOrder.role==='Seller'){
                if($scope.selOrder.seller) {
                    if ($scope.selOrder.seller.contact.firstName) {
                        $scope.selOrder.sellerDisplayName = $scope.selOrder.seller.contact.firstName + ' ' + $scope.selOrder.seller.contact.lastName;
                    } else {
                        $scope.selOrder.sellerDisplayName = $scope.selfcontact[0].firstName + ' ' + $scope.selfcontact[0].lastName;
                    }
                }
            }

            if($scope.selOrder.role==='Buyer') {
                if($scope.selOrder.buyer) {
                    if ($scope.selOrder.buyer.contact.firstName) {
                        $scope.selOrder.buyerDisplayName = $scope.selOrder.buyer.contact.firstName + ' ' + $scope.selOrder.buyer.contact.lastName;
                    } else {
                        $scope.selOrder.buyerDisplayName = $scope.selfcontact[0].firstName + ' ' + $scope.selfcontact[0].lastName;
                    }
                }
            }
            if($scope.selOrder.role==='Mediator'){
                if($scope.selOrder.mediator){
                    if($scope.selOrder.mediator.contact.firstName) {
                        $scope.selOrder.mediatorDisplayName = $scope.selOrder.mediator.contact.firstName + ' ' + $scope.selOrder.mediator.contact.lastName;
                    }else{
                        $scope.selOrder.mediatorDisplayName = $scope.selfcontact[0].firstName + ' ' + $scope.selfcontact[0].lastName;
                    }
                }
            }
            $scope.selOrder.totalAmount=$scope.totalPriceFormatter();
            $scope.selOrder.changeStatusDisplay=$scope.selOrder.onchangestatus===false && $scope.selOrder.statusArray!==undefined && $scope.selOrder.totalAmount>0;

            $scope.selOrder.onchangestatus=false;
            $scope.selOrder.onchangespaymentstatus=false;
            $scope.selOrder.paymentOptionType={
                'payNow':$scope.selOrder.paymentOptionType.payNow,
                'payOnDelivery':$scope.selOrder.paymentOptionType.payOnDelivery,
                'installments':$scope.selOrder.paymentOptionType.installments,
                'lineOfCredit':$scope.selOrder.paymentOptionType.lineOfCredit
            };
            if($scope.selOrder.paymentOptionType.payNow.selection){
                $scope.selOrder.selectedPayMentOptions=$scope.selOrder.paymentOptionType.payNow;
                $scope.selOrder.selectedPayMentOptions.key='payNow';
            }else if($scope.selOrder.paymentOptionType.payOnDelivery.selection){
                $scope.selOrder.selectedPayMentOptions=$scope.selOrder.paymentOptionType.payOnDelivery;
                $scope.selOrder.selectedPayMentOptions.key='payOnDelivery';
            }else if($scope.selOrder.paymentOptionType.lineOfCredit.selection){
                $scope.selOrder.selectedPayMentOptions=$scope.selOrder.paymentOptionType.lineOfCredit;
                $scope.selOrder.selectedPayMentOptions.key='lineOfCredit';
            }else if($scope.selOrder.paymentOptionType.installments.selection) {
                $scope.selOrder.selectedPayMentOptions=$scope.selOrder.paymentOptionType.installments;
                $scope.selOrder.selectedPayMentOptions.key='installments';
                var eachStatus=false;
                for(var i=0;i<$scope.selOrder.selectedPayMentOptions.options.length;i++){
                    $scope.selOrder.selectedPayMentOptions.options[i].amount=($scope.selOrder.totalAmount * $scope.selOrder.selectedPayMentOptions.options[i].percentage) /100;
                    if(!eachStatus && ($scope.selOrder.selectedPayMentOptions.options[i].status==='YetToPay' || $scope.selOrder.selectedPayMentOptions.options[i].status==='Overdue') ){
                        $scope.selOrder.selectedPayMentOptions.options[i].statusval=true;
                        eachStatus=true;
                    }else{
                        $scope.selOrder.selectedPayMentOptions.options[i].statusval=false;
                    }

                }
            }

            $scope.amount=$scope.selOrder.totalAmount-$scope.selOrder.paidAmount;
            $scope.selOrder.changePaymentDisplay=$scope.selOrder.onchangespaymentstatus===false && $scope.selOrder.paymentArray!==undefined && $scope.selOrder.totalAmount>0 && $scope.selOrder.totalAmount-$scope.selOrder.paidAmount>0 && !($scope.selOrder.currentStatus==='Drafted' || $scope.selOrder.currentStatus==='Placed' || $scope.selOrder.currentStatus==='Cancelled' || $scope.selOrder.currentStatus==='Rejected');
            $scope.showSellerChange = ($scope.selOrder && (!$scope.selOrder.offer || $scope.selOrder.createduser) && $scope.selOrder.currentStatus === 'Drafted' && $scope.selOrder.offer && $scope.selOrder.offer.offerType === 'Sell' && $scope.selOrder.orderType === 'Sale') || ($scope.selOrder && $scope.selOrder.currentStatus === 'Drafted' && (!$scope.selOrder.offer || ! $scope.selOrder.createduser) && $scope.selOrder.seller && $scope.selOrder.seller.contact);
            $scope.showBuyerChange = ( $scope.selOrder && $scope.selOrder.createduser && $scope.selOrder.currentStatus === 'Drafted' && $scope.selOrder.offer && $scope.selOrder.offer.offerType === 'Buy' && $scope.selOrder.orderType === 'Purchase') || ($scope.selOrder && $scope.selOrder.currentStatus === 'Drafted' &&  ! $scope.selOrder.createduser && $scope.selOrder.seller && $scope.selOrder.seller.contact);

        };

        $scope.changeFields=function() {
            if ($scope.order.role === 'Buyer') {
                $scope.selOrder.buyerDisplayName=$scope.selfcontact[0].firstName+' '+$scope.selfcontact[0].lastName;

                $scope.selOrder.buyer.contact=$scope.selfcontact[0];
                $scope.selOrder.mediator=null;
                $scope.selOrder.seller=null;
                $scope.selOrder.sellerDisplayName=null;
                $scope.selOrder.mediatorDisplayName=null;
                $scope.selOrder.buyer.nVipaniUser=$scope.selfcontact[0].nVipaniUser;
            }else if ($scope.selOrder.role === 'Seller'){
                $scope.selOrder.sellerDisplayName=$scope.selfcontact[0].firstName+' '+$scope.selfcontact[0].lastName;

                $scope.selOrder.seller.contact=$scope.selfcontact[0];
                $scope.selOrder.mediator=null;
                $scope.selOrder.buyer=null;
                $scope.selOrder.buyerDisplayName=null;
                $scope.selOrder.mediatorDisplayName=null;
                $scope.selOrder.seller.nVipaniUser=$scope.selfcontact[0].nVipaniUser;
            }else if($scope.selOrder.role ==='Mediator') {
                $scope.selOrder.mediatorDisplayName=$scope.selfcontact[0].firstName+' '+$scope.selfcontact[0].lastName;

                $scope.selOrder.mediator.contact=$scope.selfcontact[0];
                $scope.selOrder.sellerDisplayName=null;
                $scope.selOrder.buyerDisplayName=null;
                $scope.selOrder.seller=null;
                $scope.selOrder.buyer=null;
                $scope.selOrder.mediator.nVipaniUser=$scope.selfcontact[0].nVipaniUser;
            }
        };


        $scope.showOrderPaymentStatusHistorys = function() {
            var selected = [];
            if($scope.currentOrder.paymentStatus) {
                selected = $scope.paymentStatusList[$scope.currentOrder.paymentStatus];
            }
            return selected.length ? selected : 'Not set';
        };
        // Create new Message
        $scope.createMessage = function (orderId) {
            // Create new Message object
            var message = new Messages({
                body: this.body,
                order: orderId
            });

            // Redirect after save
            message.$save(function (response) {
                response.user = $scope.authentication.user;
                $scope.orderMessages.unshift(response);
                /*$location.path('orders/' + orderId);*/

                // Clear form fields
                $scope.body = '';
            }, function (errorResponse) {
                $scope.error = errorResponse.data.message;
            });
        };

        //Clear Fields Function
        $scope.clearFields = function () {
            $scope.buyer = '';
            $scope.seller = '';
            $scope.mediator = '';
            $scope.products = [];
            $scope.status = '';
            $scope.paymentStatus = '';
            $scope.offer = '';
            $scope.statusComments='';
            $scope.currentStatus='';

        };

        /*		$scope.totalSum = function (products) {
         $scope.printTotal=Object.keys(products).map(function(k){
         return +products[k].unitPrice *products[k].numberOfUnits
         }).reduce(function(a,b){ return a + b },0);
         };*/
        $scope.totalPriceFormatter=function() {
            if($scope.selOrder){
                $scope.selOrder.totalAmount=0;
                angular.forEach($scope.selOrder.products, function (row) {
                    /*angular.foreach($scope.order.products, function (i, row) {
                     */
                    if(!row.inventory.disabled) {
                        $scope.selOrder.totalAmount += +(row.unitPrice * row.numberOfUnits);
                    }
                });
                return  $scope.selOrder.totalAmount;
            }else{
                return 0;
            }
        };
        $scope.orderRedirection=function(){
            $location.path('orders');
        };
        $scope.orderValidation=function(update){
            if($scope.selOrder){

                if($scope.selOrder.offer) {
                    $scope.currentrole = $scope.selOrder.offer.offerType;
                }else{
                    $scope.currentrole = 'Sell';
                }
                if(update){
                    $scope.seller=$scope.selOrder.seller;
                    $scope.buyer=$scope.selOrder.buyer;
                    $scope.mediator=$scope.selOrder.mediator;
                    $scope.products=$scope.selOrder.products;
                    $scope.deliveryDate=$scope.selOrder.deliveryDate;
                }

                if($scope.products && $scope.products.length===0 || $scope.products.filter(function (product) {
                        return product.inventory.disabled;
                    }).length>0){
                    return true;
                }else if($scope.currentrole==='Buy' ) {
                    if(($scope.seller && $scope.seller.contact && $scope.seller.contact!=='' && $scope.seller.contact._id) ||  ($scope.mediator && $scope.mediator.contact && $scope.mediator.contact!==''&&  $scope.mediator.contact._id)){
                        return false;
                    }else{
                        return true;
                    }

                }else if(!$scope.currentrole || $scope.currentrole==='Sell')
                {
                    if(($scope.buyer && $scope.buyer.contact && $scope.buyer.contact!=='' &&  $scope.buyer.contact._id) || ($scope.mediator && $scope.mediator.contact && $scope.mediator.contact!=='' && $scope.mediator.contact._id)){
                        return false;
                    }else{
                        return true;
                    }
                }else {
                    return false;
                }
            }else {
                return false;
            }
        };


        $scope.addOrderPayment=function(onchangespaymentstatus){
            $scope.onchangespaymentstatus=onchangespaymentstatus;
            return onchangespaymentstatus;
        };
        $scope.addOrderStatus=function(onchangestatus){
            $scope.onchangestatus=onchangestatus;
        };
        // Remove existing Order
        $scope.remove = function(order) {

            if (order) {
                verifyDelete(order).then(function () {
                    order.$remove(function () {

                        for (var i in $scope.orders) {
                            if ($scope.orders [i]._id === order._id) {
                                $scope.orders.splice(i, 1);
                            }
                        }
                        $mdToast.show(
                            $mdToast.simple()
                                .textContent('Successfully deleted Order')
                                .position('center center')
                                .theme('success-toast')
                                .hideDelay(6000)
                        );
                    }, function (errorResponse) {
                        $mdToast.show(
                            $mdToast.simple()
                                .textContent(errorResponse.data.message)
                                .position('top right')
                                .theme('error-toast')
                                .hideDelay(6000)
                        );


                        $scope.error = errorResponse.data.message;
                    });
                });
            }
        };


        $scope.updateOrderStatus= function ($statusUpdate) {
            var order=$scope.selOrder;
            $scope.selectedTag=false;
            order.totalAmount=$scope.totalPriceFormatter();

            if($statusUpdate){
                order =new Orders({_id: order._id});
                order.currentStatus= this.currentStatus;
                order.statusComments =this.statusComments;
                if(order.currentStatus==='Confirmed'){
                    order.deliveryDate=$scope.selOrder.deliveryDate;
                }

            }else{

                order.paymentMode=this.paymentMode;
                order.singleamount=this.amount;
                if(order.paidAmount <order.totalAmount && order.selectedPayMentOptions.amount){
                    order.paidAmount=order.paidAmount+order.selectedPayMentOptions.amount;
                }
                order.paymentComments=this.paymentComments;


                if(order.selectedPayMentOptions.key==='payNow'){
                    order.paymentOptionType.payNow=order.selectedPayMentOptions;
                    order.paymentOptionType.payOnDelivery.selection=false;
                    order.paymentOptionType.lineOfCredit.selection=false;
                    order.paymentOptionType.installments.selection=false;
                }else if(order.selectedPayMentOptions.key==='payOnDelivery'){
                    order.paymentOptionType.payOnDelivery=order.selectedPayMentOptions;
                    order.paymentOptionType.payOnDelivery.status='Paid';
                    order.paymentOptionType.payNow.selection=false;
                    order.paymentOptionType.lineOfCredit.selection=false;
                    order.paymentOptionType.installments.selection=false;
                }else if(order.selectedPayMentOptions.key==='lineOfCredit'){
                    order.paymentOptionType.lineOfCredit=order.selectedPayMentOptions;
                    order.paymentOptionType.lineOfCredit.status='Paid';
                    order.paymentOptionType.payOnDelivery.selection=false;
                    order.paymentOptionType.payNow.selection=false;
                    order.paymentOptionType.installments.selection=false;
                }else if(order.selectedPayMentOptions.key==='installments') {
                    order.paymentOptionType.installments.options[$scope.selectedPaymentIndex]=$scope.selectedOptions;
                    order.paymentOptionType.installments.options[$scope.selectedPaymentIndex].status='Paid';
                    if(order.paidAmount <order.totalAmount && $scope.selectedOptions.amount)
                        order.paidAmount=order.paidAmount+$scope.selectedOptions.amount;
                    order.paymentComments=this.paymentComments;
                    order.paymentOptionType.payOnDelivery.selection=false;
                    order.paymentOptionType.payNow.selection=false;
                    order.paymentOptionType.lineOfCredit.selection=false;
                }
                order =new Orders({
                    _id:order._id,
                    paymentOptionType:order.paymentOptionType,
                    paymentComments:this.paymentComments,
                    paidAmount:order.paidAmount});
            }
            order.$update(function() {
                order.onchangestatus=false;
                order.onchangespaymentstatus=false;
                $scope.selOrder=order;
                $scope.clearFields();


                if(order.paymentOptionType.payNow.selection){
                    order.selectedPayMentOptions=order.paymentOptionType.payNow;
                    order.selectedPayMentOptions.key='payNow';
                    /*order.paymentOptionselection=order.paymentOptionType.payNow;
                     order.paymentOptionselection.key='payNow';*/
                }else if(order.paymentOptionType.payOnDelivery.selection){
                    order.selectedPayMentOptions=order.paymentOptionType.payOnDelivery;
                    order.selectedPayMentOptions.key='payOnDelivery';
                    /*order.paymentOptionselection=order.paymentOptionType.payOnDelivery;
                     order.paymentOptionselection.key='payOnDelivery';*/
                }else if(order.paymentOptionType.lineOfCredit.selection){
                    order.selectedPayMentOptions=order.paymentOptionType.lineOfCredit;
                    order.selectedPayMentOptions.key='lineOfCredit';
                    /*order.paymentOptionselection=order.paymentOptionType.lineOfCredit;
                     order.paymentOptionselection.key='lineOfCredit';*/
                }else if(order.paymentOptionType.installments.selection) {
                    order.selectedPayMentOptions=order.paymentOptionType.installments;
                    order.selectedPayMentOptions.key='installments';
                }
                $scope.selectTabs();
                $scope.panelChange(order,'view');
                $scope.hide();
            }, function(errorResponse) {
                $scope.error = errorResponse.data.message;
            });
        };
        $scope.paymentModeSelected=function () {
            return  $scope.paymentMode && $scope.paymentMode.filter(function (paymentMode) {
                return paymentMode.isDisplay;
            });
        };
        $scope.updateAddress = function() {
            if($scope.selOrder.seller && $scope.selOrder.seller.contact && $scope.selOrder.seller.contact._id===null){
                if(this.buyer.contact){
                    $scope.selOrder.seller.contact._id=this.seller.contact;
                }
            }
            if($scope.selOrder.buyer && $scope.selOrder.buyer.contact && $scope.selOrder.buyer.contact._id===null){
                if(this.buyer.contact){
                    $scope.selOrder.buyer.contact._id=this.buyer.contact;
                }
            }
            if($scope.selOrder.mediator && $scope.selOrder.mediator.contact && $scope.selOrder.mediator.contact._id===null){
                if(this.buyer.contact){
                    $scope.selOrder.mediator.contact._id=this.mediator.contact;
                }
            }
            var order = $scope.selOrder;
            var ShippingAddress=null;
            var billingAddress=null;
            /* if(order.role==='Buyer') {
             $scope.buyer = {};
             if ($scope.selfcontact.length>0) {
             order.buyer.contact = $scope.selfcontact[0]._id;
             order.buyer.nVipaniUser = $scope.selfcontact[0].nVipaniUser;
             }
             }else if (angular.isObject(order.buyer) && order.buyer && order.buyer.contact!==undefined && order.buyer.contact!==''&& order.buyer.contact._id ) {
             if(order.buyer.contact.nVipaniUser){
             order.buyer.nVipaniUser = order.buyer.contact.nVipaniUser;
             }
             order.buyer.contact = order.buyer.contact._id;
             }

             if(order.role==='Seller') {
             order.seller={};
             if ($scope.selfcontact.length>0) {
             order.seller.contact = $scope.selfcontact[0]._id;
             order.seller.nVipaniUser = $scope.selfcontact[0].nVipaniUser;
             }
             }else if (angular.isObject(order.seller) && order.seller && order.seller.contact!==undefined && order.seller.contact!=='' && order.seller.contact._id) {
             if(order.seller.contact.nVipaniUser){
             order.seller.nVipaniUser = order.seller.contact.nVipaniUser;
             }
             order.seller.contact = order.seller.contact._id;
             }

             if(order.role==='Mediator') {
             order.mediator={};
             if ($scope.selfcontact.length>0) {
             order.mediator.contact = $scope.selfcontact[0]._id;
             order.mediator.nVipaniUser = $scope.selfcontact[0].nVipaniUser;
             }
             }else if (angular.isObject(order.mediator) && order.mediator && order.mediator.contact &&  order.mediator.contact !==undefined  && order.mediator.contact!=='' && order.mediator.contact._id && order.mediator.contact) {

             if(order.mediator.contact.nVipaniUser){
             order.mediator.nVipaniUser = order.mediator.contact.nVipaniUser;
             }
             order.mediator.contact = order.mediator.contact._id;
             }*/
            if($scope.address && ($scope.address.shipping || $scope.address.billing) && (((order.offer && order.offer.offerType==='Sell' && order.buyer) || (order.offer && order.offer.offerType==='Buy' && order.seller)))) {
                order.isAddress=true;
                if($scope.address.shipping){
                    ShippingAddress=angular.copy($scope.address);
                    ShippingAddress.addressType='Shipping';
                    ShippingAddress.isNew=true;
                }else{
                    ShippingAddress=order.billingAddress;
                }
                if($scope.address.billing){
                    billingAddress=angular.copy($scope.address);
                    billingAddress.addressType='Billing';
                    billingAddress.isNew=true;
                }else{
                    billingAddress=order.billingAddress;
                }
                order= new Orders({_id:order._id});
                order.isAddress=true;
                order.shippingAddress = ShippingAddress;
                order.billingAddress = billingAddress;
                order.$update(function(orderResponse) {
                    /*$scope.order=orderResponse;*/

                    /*orderResponse.$promise.then(function (order) {*/
                    order.isAddress=false;
                    $scope.address.shipping=false;
                    $scope.address.billing=false;
                    /* $scope.selectTabs();*/
                    $scope.panelChange( order,'view');
                    /*});*/
                }, function(errorResponse) {
                    $scope.error = errorResponse.data.message;
                });
                /*}, function (errorcontactResponse) {
                 $scope.error = errorcontactResponse.data.message;
                 });
                 });
                 }*/
            }else{
                if($scope.address && ($scope.address.shipping || $scope.address.billing) && !(order.offer)){
                    if(order.orderType==='Sale'){
                        $scope.shippingContact = Contacts.get({
                            contactId: order.buyer.contact._id.toString()
                        });
                        if($scope.shippingContact){
                            $scope.shippingContact.$promise.then(function (result2) {

                                if($scope.address.shipping){
                                    ShippingAddress=angular.copy($scope.address);
                                    ShippingAddress.addressType='Shipping';
                                    ShippingAddress.isNew=true;
                                }else{
                                    ShippingAddress=order.billingAddress;
                                }
                                if($scope.address.billing){
                                    billingAddress=angular.copy($scope.address);
                                    billingAddress.addressType='Billing';
                                    billingAddress.isNew=true;
                                }else{
                                    billingAddress=order.billingAddress;
                                }
                                if ($scope.address.shipping && result2.addresses) {
                                    result2.addresses.push(ShippingAddress);

                                    /*}else{
                                     order.shippingAddress = ShippingAddress;
                                     }*/
                                }
                                if ($scope.address.billing && result2.addresses){
                                    //var oldAddress=$scope.findSingleAddress(billingAddress,result2.addresses);

                                    result2.addresses.push(billingAddress);
                                    /*result2.$update(function (response) {
                                     order.billingAddress=billingAddress;
                                     }, function (errorcontact1Response) {
                                     $scope.error = errorcontact1Response.data.message;
                                     });*/
                                }
                                result2.$update(function (response) {
                                    order= new Orders({_id:order._id});
                                    order.isAddress=true;
                                    order.shippingAddress = ShippingAddress;
                                    order.billingAddress = billingAddress;
                                    order.$update(function(orderResponse) {
                                        order.isAddress=false;
                                        $scope.address.shipping=false;
                                        $scope.address.billing=false;

                                        $scope.panelChange(order,'view');
                                        /*	orderResponse.$promise.then(function (orderresponse) {
                                         $scope.selOrder=orderResponse;
                                         /!* $scope.address.shipping=false;
                                         $scope.address.billing=false;*!/
                                         /!* $scope.selOrder = order;*!/
                                         // $scope.panelChange(order,'view');
                                         });*/
                                    }, function(errorResponse) {
                                        $scope.error = errorResponse.data.message;
                                    });
                                }, function (errorcontactResponse) {
                                    $scope.error = errorcontactResponse.data.message;
                                });
                            });
                        }
                    }
                }else {
                    order = new Orders({
                        _id: order._id,
                        shippingAddress: order.shippingAddress,
                        billingAddress: order.billingAddress
                    });

                    order.$update(function () {
                        $scope.orderUpdateFields();
                        $scope.address.shipping = false;
                        $scope.address.billing = false;
                        /* $scope.selOrder = order;*/
                        $scope.panelChange(order, 'view');
                    }, function (errorResponse) {
                        $scope.error = errorResponse.data.message;
                    });
                }
            }



        };
        $scope.update = function() {
            var order = $scope.selOrder;
            order.totalAmount=$scope.totalPriceFormatter();
            if(order.currentStatus==='Drafted') {
                if (order.buyer.contact && order.buyer.contact._id) {
                    var buyerContact = $scope.contacts.filter(function (contact) {
                        return contact._id === order.buyer.contact._id;
                    });
                    if (buyerContact.length > 0 && buyerContact[0].nVipaniUser) {
                        order.buyer.nVipaniUser = buyerContact[0].nVipaniUser;
                    }
                }
                if (order.seller && order.seller.contact._id) {
                    var sellerContact = $scope.contacts.filter(function (contact) {
                        return contact._id === order.seller.contact._id;
                    });
                    if (sellerContact.length > 0 && sellerContact[0].nVipaniUser) {
                        order.seller.nVipaniUser = sellerContact[0].nVipaniUser;
                    }
                }
                if (order.mediator && order.mediator.contact._id) {
                    var mediatorContact = $scope.contacts.filter(function (contact) {
                        return contact._id === order.mediator.contact._id;
                    });
                    if (mediatorContact.length > 0 && mediatorContact[0].nVipaniUser) {
                        order.mediator.nVipaniUser = mediatorContact[0].nVipaniUser;
                    }
                }
            }

                if($scope.selectedPageId > $scope.pages.length-1 && $scope.payment){
                    order.currentStatus=$scope.pages[$scope.selectedPageId-1].currentStatus;
                }
                /*alert($scope.selectedPageId);*/
                if($scope.selectedPageId===1){
                    order.currentStatus=$scope.pages[$scope.selectedPageId].currentStatus;
                }
                /*	if(order.currentStatus==='Drafted'){*/
                if($scope.selectedPayMentOptions){
                    order.paymentOptionType=$scope.selectedPayMentOptions;
                    /*if(order.selectedPayMentOptions.key===''){
                     }*/
                }

                if(order.selectedPayMentOptions && order.selectedPayMentOptions.key && order.currentStatus!=='Drafted'){
                    order.paymentMode=this.paymentMode;

                    if(order.selectedPayMentOptions.key==='payNow'){
                        order.paymentOptionType.payNow=order.selectedPayMentOptions;
                        order.paymentOptionType.payNow.status='Paid';
                        if(order.paidAmount <order.totalAmount && order.selectedPayMentOptions.amount)
                            order.paidAmount=order.paidAmount+order.selectedPayMentOptions.amount;
                        order.paymentComments=this.paymentComments;
                        order.paymentOptionType.payOnDelivery.selection=false;
                        order.paymentOptionType.lineOfCredit.selection=false;
                        order.paymentOptionType.installments.selection=false;
                    }else if(order.selectedPayMentOptions.key==='payOnDelivery'){
                        order.paymentOptionType.payOnDelivery=order.selectedPayMentOptions;
                        /*if(!$scope.payment){
                         order.paymentOptionType.lineOfCredit.status='Paid';
                         }*/
                        order.paymentOptionType.payNow.selection=false;
                        order.paymentOptionType.lineOfCredit.selection=false;
                        order.paymentOptionType.installments.selection=false;
                    }else if(order.selectedPayMentOptions.key==='lineOfCredit'){
                        order.paymentOptionType.lineOfCredit=order.selectedPayMentOptions;
                        /*if(!$scope.payment) {
                         order.paymentOptionType.lineOfCredit.status = 'Paid';
                         }*/
                        order.paymentOptionType.payOnDelivery.selection=false;
                        order.paymentOptionType.payNow.selection=false;
                        order.paymentOptionType.installments.selection=false;
                    }else if(order.selectedPayMentOptions.key==='installments') {
                        order.paymentOptionType.installments.options[$scope.selectedPaymentIndex]=$scope.selectedOptions;
                        order.paymentOptionType.installments.options[$scope.selectedPaymentIndex].status='Paid';
                        if(order.paidAmount <order.totalAmount)
                            order.paidAmount=order.paidAmount+$scope.selectedOptions.amount;
                        order.paymentOptionType.payOnDelivery.selection=false;
                        order.paymentOptionType.payNow.selection=false;
                        order.paymentOptionType.lineOfCredit.selection=false;
                    }
                }
                if(order.role==='Buyer') {
                    $scope.buyer = {};
                    if ($scope.selfcontact.length>0) {
                        order.buyer.contact = $scope.selfcontact[0]._id;
                        order.buyer.nVipaniUser = $scope.selfcontact[0].nVipaniUser;
                    }
                }else if (angular.isObject(order.buyer) && order.buyer && order.buyer.contact!==undefined && order.buyer.contact!==''&& order.buyer.contact._id ) {
                    if(order.buyer.contact.nVipaniUser){
                        order.buyer.nVipaniUser = order.buyer.contact.nVipaniUser;
                    }
                    order.buyer.contact = order.buyer.contact._id;
                }

                if(order.role==='Seller') {
                    order.seller={};
                    if ($scope.selfcontact.length>0) {
                        order.seller.contact = $scope.selfcontact[0]._id;
                        order.seller.nVipaniUser = $scope.selfcontact[0].nVipaniUser;
                    }
                } else if (angular.isObject(order.seller) && order.seller && order.seller.contact && order.seller.contact !== undefined && order.seller.contact !== '' && order.seller.contact._id) {
                    if(order.seller.contact.nVipaniUser){
                        order.seller.nVipaniUser = order.seller.contact.nVipaniUser;
                    }
                    order.seller.contact = order.seller.contact._id;
                }

                if(order.role==='Mediator') {
                    order.mediator={};
                    if ($scope.selfcontact.length>0) {
                        order.mediator.contact = $scope.selfcontact[0]._id;
                        order.mediator.nVipaniUser = $scope.selfcontact[0].nVipaniUser;
                    }
                }else if (angular.isObject(order.mediator) && order.mediator && order.mediator.contact &&  order.mediator.contact !==undefined  && order.mediator.contact!=='' && order.mediator.contact._id && order.mediator.contact) {

                    if(order.mediator.contact.nVipaniUser){
                        order.mediator.nVipaniUser = order.mediator.contact.nVipaniUser;
                    }
                    order.mediator.contact = order.mediator.contact._id;
                }
                /*}*/


                /*		if($scope.address.billing){
                 /!*$scope.address.addressType='Billing';*!/
                 $scope.billingContact=Contacts.get({
                 contactId: order.seller.contact
                 });

                 $scope.billingContact.$promise.then(function (result1) {
                 if (result1.addresses && $scope.findSingleAddress($scope.address,result1.addresses).length===0) {
                 var billingAddress=angular.copy($scope.address);
                 billingAddress.addressType='Billing';
                 result1.addresses.push($scope.billingAddress);
                 result1.$update(function (response) {
                 order.billingAddress=billingAddress;
                 }, function (errorContact1Response) {
                 $scope.error = errorContact1Response.data.message;
                 });
                 }

                 });


                 }*/
                /*	if($scope.address && ($scope.address.shipping || $scope.address.shipping)){
                 $scope.shippingContact= Contacts.get({
                 contactId: order.buyer.contact
                 });
                 var ShippingAddress=null;
                 var billingAddress=null;
                 $scope.shippingContact.$promise.then(function (result2) {

                 if($scope.address.shipping){
                 ShippingAddress=angular.copy($scope.address);
                 ShippingAddress.addressType='Shipping';
                 }
                 if($scope.address.billing){
                 billingAddress=angular.copy($scope.address);
                 billingAddress.addressType='Billing';
                 }
                 if ($scope.address.shipping && result2.addresses) {
                 result2.addresses.push(ShippingAddress);
                 /!*}else{
                 order.shippingAddress = ShippingAddress;
                 }*!/
                 }
                 if ($scope.address.billing && result2.addresses){
                 //var oldAddress=$scope.findSingleAddress(billingAddress,result2.addresses);

                 result2.addresses.push(billingAddress);
                 /!*result2.$update(function (response) {
                 order.billingAddress=billingAddress;
                 }, function (errorcontact1Response) {
                 $scope.error = errorcontact1Response.data.message;
                 });*!/
                 }
                 result2.$update(function (response) {
                 order.shippingAddress = ShippingAddress;
                 order.billingAddress = billingAddress;
                 }, function (errorcontactResponse) {
                 $scope.error = errorcontactResponse.data.message;
                 });
                 });

                 }*/



                order.$update(function(orderResponse) {
                    /*$scope.order=orderResponse;*/

                    /*orderResponse.$promise.then(function (order) {*/
                    $scope.selOrder=order;

                    if(order.shippingAddress.addressLine.length>0){
                        $scope.showShipping=false;
                        $scope.shipping=false;
                    }else{
                        $scope.showShipping=true;
                        $scope.shipping=true;

                    }
                    if(order.billingAddress.addressLine.length>0){
                        $scope.showBilling=false;
                        $scope.billing=false;
                    }else{
                        $scope.showBilling=true;
                        $scope.billing=true;

                    }
                    /*	$scope.orderUpdateFields();


                     });*/
                    /*$scope.findOne();*/
                    /*if($scope.order.currentStatus==='Drafted'){
                     $location.path('orders/' + $scope.order._id+'/edit');
                     }/!*else if($scope.order.currentStatus==='Placed') {
                     $location.path('orders/' + $scope.order._id + '/change');
                     }*!/else{*/

                    $scope.selectTabs();

                    $scope.panelChange($scope.selOrder,'view');
                    /*$location.path('orders/' + $scope.order._id);*/
                    /*}*/
                }, function(errorResponse) {
                    $scope.error = errorResponse.data.message;
                    /*});*/
                });
            };

            $scope.updatePaymentHistory=function() {
                var order = $scope.order;
                var history=order.paymentHistory;
                for(var i=0;i<$scope.paymentHistory.length;i++) {
                    delete $scope.paymentHistory[i].date;
                    history.push($scope.paymentHistory[i]);
                    order.paymentStatus=$scope.paymentHistory[i].status;
                    order.paymentMode=$scope.paymentHistory[i].paymentMode;
                }
                $scope.paymentHistory=[];
                //order.paymentHistory=history;
                order.$update(function(response) {
                    $scope.paymentHistory=[];
                    $location.path('orders' + response._id);
                }, function(errorResponse) {
                    $scope.error = errorResponse.data.message;

                });
            };

            $scope.updateStatusHistory=function() {
                var order = $scope.order;
                var history=order.statusHistory;
                for(var i=0;i<$scope.statusHistory.length;i++) {
                    delete $scope.statusHistory[i].date;
                    order.currentStatus=$scope.statusHistory[i].status;
                    history.push($scope.statusHistory[i]);
                }
                order.statusHistory=history;
                $scope.statusHistory=[];
                order.$update(function	(response) {
                    $scope.order=response;
                    $scope.statusHistory=[];
                    $location.path('orders');
                }, function(errorResponse) {
                    $scope.error = errorResponse.data.message;

                });
            };

            $scope.findInit=function () {
                if($stateParams.orderId!==undefined){
                    $scope.preOrder = Orders.get({
                        orderId: $stateParams.orderId
                    });

                    $scope.preOrder.$promise.then(function (result) {
                        if($scope.preOrder.user._id===$scope.authentication.user._id || $scope.preOrder.buyer.nVipaniUser===$scope.authentication.user._id){
                            $scope.selectedTab = 'MyOrders';
                            $scope.selectedIndex=0;
                        }else{
                            $scope.selectedTab = 'ReceivedOrders';
                            $scope.selectedIndex=1;
                        }
                        $scope.findOne(true);
                    });
                }else{
                    $scope.viewAllOrders=true;
                    $scope.find(false);
                }


            };

            $scope.panelChange = function (order, changeStr) {
                $scope.showEdit = false;
                $scope.showAddOffer = false;
                $scope.showAddToGroups = false;
                $scope.showAddGroup = false;
                $scope.showEditGroup = false;
                $scope.showView = false;
                $scope.viewAllOrders=false;

                if (order !== null && angular.isObject(order)) {
                    if (typeof changeStr === 'string' ) {
                        if (changeStr === 'edit' ) {
                            $scope.selOrder=angular.copy(order);
                            /*$scope.selOffer=angular.copy(offer);*/
                            $scope.showEdit = true;
                            /*$scope.findOne();*/
                        } else if (changeStr === 'add') {
                            $scope.showAddOrder = true;


                        }else if (changeStr === 'view') {
                            $scope.selOrder=angular.copy(order);
                            $scope.orderUpdateFields();
                            /*$scope.findOne();*/
                            if($scope.selOrder.currentStatus==='Drafted'){
                                $scope.showEdit = true;
                                $scope.findContacts();
                            }else{
                                $scope.showView = true;
                            }

                            /*	$scope.findOne();*/
                            /*$scope.resetCall();*/


                        }/*else {
                         $scope.showView = true;

                         }*/

                    } else {
                        $scope.selOrder=angular.copy(order);
                        if($scope.selOrder){
                            $scope.showView = true;
                        }

                    }
                } else {
                    if (changeStr === 'add') {
                        $scope.showAddOrder = true;
                    } else if(changeStr==='viewAll'){
                        $scope.viewAllOrders=true;
                    }/*else if (changeStr === 'addToGroups') {
                     $scope.showAddToGroups = true;
                     } else if (changeStr === 'group') {
                     $scope.showAddGroup = true;
                     }else {

                     $scope.showView = true;

                     }*/

                }
            };
            $scope.changeSelOrder = function (order) {

                $scope.selOrder = angular.copy(order);
                /*$scope.updateOrder = angular.copy(order);*/
                $scope.findOne(false);

                /*$scope.panelChange($scope.selOrder,'view');*/
            };
            $scope.changeStatusOrder = function (order) {

                $scope.selOrder = angular.copy(order);
                $scope.orderUpdateFields();
                $scope.showOrderDialog(this.event);
                /*$scope.updateOrder = angular.copy(order);
                 $scope.findOne(false);*/
                /*$scope.panelChange($scope.selOrder,'view');*/
            };
            // Find a list of Orders
            $scope.find = function(tabSelection) {
                $scope.selectedTab = 'MyOrders';
                $scope.selectedIndex=0;
                $scope.selOrder=null;
                $scope.orders = Orders.query();
                $scope.orders.$promise.then(function (result) {
                    if($scope.orders.length>0) {
                        $scope.selOrder = $scope.orders[0];
                        if(!$scope.selOrder.buyer){
                            $scope.selOrder.buyer=null;
                        }
                        /*$scope.findContacts();*/

                        /*$scope.findOne(tabSelection);*/
                        /*if(tabSelection){
                         $scope.findOne(tabSelection);
                         }else {*/
                        $scope.panelChange(null, 'viewAll');
                        /*}*/
                    }
                });
            };

            // Find a list of Orders
            $scope.findOrderMessages = function (orderId) {
                $scope.orderMessages = OrderMessages.query({
                    orderId: $stateParams.orderId?$stateParams.orderId :orderId
                });
            };
            $scope.findSpecific = function(tabSelection) {
                $scope.selectedTab = 'ReceivedOrders';
                $scope.selectedIndex=1;
                $scope.orders=[];
                $scope.selOrder=null;
                /*	$scope.orders=[];*/
                $http.get('ordersQuery').then(function (response) {
                    $scope.orders = response.data;
                    if($scope.orders.length>0) {
                        if(tabSelection){
                            $scope.findOne(tabSelection);
                        }else {
                            $scope.panelChange(null, 'viewAll');
                        }
                        /*$scope.selOrder = $scope.orders[0];
                         /!*$scope.findContacts();*!/
                         $scope.findOne(tabSelection);*/

                    }

                    /*$scope.registerUser = response;
                     $scope.userCategories = ['Trader', 'Agent', 'Consumer', 'Producer'];
                     $scope.selectedItem=$scope.registerUser.userCategories;
                     $scope.phones = [];

                     //return response;
                     if($scope.registerUser.status!=='Register Request'){
                     $location.path('/');
                     }*/
                });
            };

            $scope.findInvoice= function () {
                $http.get('ordersQuery1').then(function (response) {

                    /*jsreport.render('<h1>Hello world</h1>').then(function (out) {
                     out.stream.pipe(response);*/
                });
            };

            $scope.expandAddress=function(typeValue){
                if(typeValue==='Billing'){
                    $scope.showBilling=!$scope.showBilling;
                    if($scope.showBilling===true){
                        $scope.billing = false;
                    }else{
                        $scope.billing = true;
                    }
                }
                if(typeValue==='Shipping'){
                    $scope.showShipping=!$scope.showShipping;
                    if($scope.showShipping===true){
                        $scope.shipping = false;
                    }else{
                        $scope.shipping = true;
                    }
                }
            };
            $scope.paymentClick=function(index){
                $scope.selectedOptions=$scope.selOrder.selectedPayMentOptions.options[index];
                $scope.selectedTag=true;
                $scope.selectedPaymentIndex=index;/*
                 if($scope.selOrder.selectedPayMentOptions.selection&& $scope.selOrder.selectedPayMentOptions.key==='installments')
                 $scope.selOrder.selectedPayMentOptions=$scope.selectedPayMentOptions.installments.options[selectedIndex];*/

            };
            $scope.paymentChanges=function(key,singleKey){
                if(singleKey){
                    $scope.selectedTag=singleKey;
                }
            };
            $scope.paymentChange=function(key){
                $scope.selectedTag=false;
                $scope.selectedPayMentOptions=angular.copy($scope.selOrder.paymentOptionType);

                if(key==='installments') {
                    /*$scope.selectedTag=true;*/
                    $scope.selOrder.selectedPayMentOptions=$scope.selectedPayMentOptions.installments;
                    /*$scope.selOrder.selectedPayMentOptions=$scope.selectedPayMentOptions.installments.options[$scope.selectedIndex];*/
                    var eachStatus=false;
                    for(var i=0;i<$scope.selectedPayMentOptions.installments.options.length;i++){
                        $scope.selOrder.selectedPayMentOptions.options[i].amount=($scope.selOrder.totalAmount * $scope.selectedPayMentOptions.installments.options[i].percentage) /100;
                        if(!eachStatus && ($scope.selectedPayMentOptions.installments.options[i].status==='YetToPay' || $scope.selectedPayMentOptions.installments.options[i].status==='OverDue') ){
                            $scope.selectedPayMentOptions.installments.options[i].statusval=true;
                            eachStatus=true;
                        }else{
                            $scope.selectedPayMentOptions.installments.options[i].statusval=false;
                        }

                    }


                    $scope.selOrder.selectedPayMentOptions.key='installments';
                    $scope.selOrder.selectedPayMentOptions.selection=true;/*
                     $scope.selectedPayMentOptions.payNow.selection=false;
                     $scope.selectedPayMentOptions.payOnDelivery.selection=false;
                     $scope.selectedPayMentOptions.lineOfCredit.selection=false;*/

                }else if(key==='payNow'){
                    $scope.selOrder.selectedPayMentOptions=$scope.selectedPayMentOptions.payNow;
                    $scope.selOrder.selectedPayMentOptions.key='payNow';
                    $scope.selOrder.selectedPayMentOptions.selection=true;
                    $scope.selOrder.selectedPayMentOptions.amount=$scope.selOrder.totalAmount;
                    /*$scope.selectedPayMentOptions.installments.selection=false;
                     $scope.selectedPayMentOptions.payOnDelivery.selection=false;
                     $scope.selectedPayMentOptions.lineOfCredit.selection=false;*/

                }else if(key==='payOnDelivery'){
                    $scope.selOrder.selectedPayMentOptions=$scope.selectedPayMentOptions.payOnDelivery;
                    $scope.selOrder.selectedPayMentOptions.selection=true;
                    $scope.selOrder.selectedPayMentOptions.amount=$scope.selOrder.totalAmount;
                    /*$scope.selectedPayMentOptions.payNow.selection=false;
                     $scope.selectedPayMentOptions.installments.selection=false;
                     $scope.selectedPayMentOptions.lineOfCredit.selection=false;*/

                }else if(key==='lineOfCredit') {
                    $scope.selOrder.selectedPayMentOptions=$scope.selectedPayMentOptions.lineOfCredit;
                    $scope.selOrder.selectedPayMentOptions.amount=$scope.selOrder.totalAmount;
                    $scope.selOrder.selectedPayMentOptions.selection = true;
                    /*$scope.selectedPayMentOptions.payNow.selection=false;
                     $scope.selectedPayMentOptions.installments.selection=false;
                     $scope.selectedPayMentOptions.payOnDelivery.selection=false;*/
                }
            };
            // Find existing Order
            $scope.findOne = function(selOrderId) {
                var orderId=null;
                $scope.selectedPageId=0;
                $scope.addresspage=false;
                $scope.payment=false;
                $scope.view=true;
                // $scope.selOrder=null;
                if(selOrderId && $stateParams.orderId){
                    orderId=$stateParams.orderId;
                }/*else if(selOrderId) {
                 orderId=null;
                 }*/else if($scope.selOrder) {
                    orderId=$scope.selOrder._id;
                }
                if(orderId){
                    $scope.selOrder = Orders.get({
                        orderId: orderId
                    });

                    $scope.selOrder.$promise.then(function (result) {
                        $scope.selOrder=result;
                        $scope.selOrder.onchangestatus=false;
                        $scope.selOrder.onchangespaymentstatus=false;
                        if($scope.selOrder){
                            $scope.singleBilling=result.shippingAddress.sameAsBilling;

                            $scope.findOrderMessages($scope.selOrder._id);

                            if($scope.selOrder.shippingAddress.addressLine.length>0){
                                $scope.showShipping=false;
                                $scope.shipping=false;
                                /*	if($scope.order.seller.addresses.length>0){
                                 $scope.shipping=false;
                                 }else{
                                 $scope.shipping=true;
                                 }*/
                            }else{
                                $scope.showShipping=true;
                                $scope.shipping=true;

                            }
                            if($scope.selOrder.billingAddress.addressLine.length>0 ||$scope.selOrder.shippingAddress.sameAsBilling){
                                $scope.showBilling=false;
                                $scope.billing = false;

                            }else{
                                $scope.showBilling=true;

                                $scope.billing=true;
                            }


                            $scope.selectTabs();
                            $scope.panelChange($scope.selOrder,'view');
                        }
                    });
                }else{
                    $location.path('orders');
                }
            };
            $scope.findAddress=function(seller,buyer,mediator){
                var array=[{'types':[]},{'Address':[]}];
                var eachType={};

                if(seller){
                    if($scope.order.seller){
                        $scope.addressTypes=[];
                        $scope.addressTypesVsDetails=[];
                        angular.forEach($scope.order.buyer.contact.addresses, function (e) {
                            $scope.addressTypes.push(e.addressType);
                        });
                        if($scope.order.seller.contact.addresses) {
                            //array.Address.push($scope.order.seller.contact.addresses);
                            $scope.addressTypesVsDetails=$scope.order.buyer.contact.addresses;
                            return $scope.addressTypesVsDetails;
                        }
                    }
                }
                return array;
            };
            $scope.filterAddressLines = function (typedValue) {
                var addresses=null;

                if($scope.selOrder && $scope.selOrder.offer && $scope.selOrder.offer.offerType==='Sell' && $scope.selOrder.buyer && $scope.selOrder.buyer.contact){
                    addresses=$scope.selOrder.buyer.contact.addresses;
                }
                if($scope.selOrder && $scope.selOrder.offer && $scope.selOrder.offer.offerType==='Buy' && $scope.selOrder.seller && $scope.selOrder.seller.contact){
                    addresses=$scope.selOrder.seller.contact.addresses;
                }
                if($scope.selOrder && !$scope.selOrder.offer && $scope.selOrder.buyer && $scope.selOrder.buyer.contact){
                    addresses=$scope.selOrder.buyer.contact.addresses;
                }
                var array = [];
                if(addresses){
                    addresses.filter(function (address) {
                        /*var matches_firstvsLastName = angular.lowercase(address.addressType).indexOf(angular.lowercase(typedValue)) !== -1;*/
                        if(address){
                            if(address.addressType===typedValue) {
                                /*if(angular.equals(typedValue, $scope.order.shippingAddress)){
                                 address.checked = true;
                                 array.push(address);
                                 }*/

                                /*	if (typedValue === 'Billing' &&  address.addressTypes === $scope.order.billingAddress.addressLine && address.pinCode === $scope.order.billingAddress.pinCode && address.city === $scope.order.billingAddress.city && address.country === $scope.order.billingAddress.country && address.state === $scope.order.billingAddress.state) {
                                 /!*	 address.checked = true;*!/

                                 array.push(address);
                                 $scope.selectedBillingAddress=address;
                                 }else if (typedValue === 'Shipping' &&  address.addressTypes === $scope.order.shippingAddress.addressLine && address.pinCode === $scope.order.shippingAddress.pinCode && address.city === $scope.order.shippingAddress.city && address.country === $scope.order.shippingAddress.country && address.state === $scope.order.shippingAddress.state) {
                                 $scope.selectedShippingAddress=address;
                                 array.push(address);
                                 }else{
                                 address.checked = false;
                                 array.push(address);
                                 }*/
                                array.push(address);
                            }
                        }
                    });
                }
                return array;
            };

            $scope.selectedAddress = function (typedValue,address) {
                if(typedValue==='Billing'){
                    $scope.order.billingAddress=address;
                }
                if(typedValue==='Shipping'){
                    address.sameAsBilling=$scope.order.billingAddress.sameAsBilling;
                    $scope.order.shippingAddress=address;
                    $scope.singleBilling=address.sameAsBilling;
                }
            };

            /*	$scope.findSingleAddress=function(type,addresses){
             var array=[];

             angular.forEach(addresses, function (e) {
             if(type=== e.addressType){
             array.type.push(e.addressType);
             }
             });
             if(array.length>0){
             return array[0];
             }else{
             return array;
             }


             };*/
            $scope.findSingleAddress=function(type,addresses){
                return addresses.filter(function (address) {
                    if (address && type.addressLine === address.addressLine && type.pinCode === address.pinCode && type.city === address.city && type.country === address.country && type.state === address.state) {
                        return true;
                    }
                });

            };
            $scope.findContacts = function () {
                var results = Contacts.query();
                $scope.contacts = [];
                $scope.selfcontact = [];
                results.$promise.then(function (result) {
                    angular.forEach(result, function (e) {
                        if (e.nVipaniUser!==null &&   (e.nVipaniRegContact!==null && e.nVipaniRegContact===true && (e.nVipaniUser=== e.user || e.nVipaniUser=== e.user._id))) {
                            $scope.selfcontact.push(e);
                        }else{
                            if(!e.disabled)
                                $scope.contacts.push(e);
                        }
                    });
                });

            };

            $scope.findProducts = function () {
                $scope.metaProducts = Products.query();
            };

            $scope.addPaymentHistory = function () {
                $scope.paymentHistory.push({
                    status: '',
                    amount: '',
                    date: '',
                    paymentMode: '',
                    user:$scope.authentication.user._id,
                    displayName:$scope.authentication.user.displayName
                });
            };
            $scope.addStatusHistory = function () {
                $scope.statusHistory.push({
                    status: '',
                    date: '',
                    user:$scope.authentication.user._id,
                    displayName:$scope.authentication.user.displayName
                });
            };
            $scope.selectProduct = function (metaProduct, index) {
                $scope.products[index].product = metaProduct;
                $scope.products[index].unitSize = metaProduct.unitSize;
                $scope.products[index].unitMeasure = metaProduct.unitMeasure;
                $scope.products[index].unitPrice = metaProduct.unitPrice;
            };

            $scope.changeProductUnitMeasure = function (index,each) {


                $scope.products[index].unitMeasure=each.$data;
                /*$scope.apply();*/
            };
            $scope.changeProductUnitSize = function (index,each) {

                //totalPrice = numberOfUnits * unitPrice
                $scope.products[index].unitSize=each.$data;
                $scope.products[index].totalQuantity = $scope.products[index].unitSize * $scope.products[index].numberOfUnits;
                $scope.products[index].totalPrice = $scope.products[index].unitPrice * $scope.products[index].numberOfUnits;
                /*$scope.product.totalQuantity=$scope.products[index].totalQuantity;*/

            };
            $scope.changeProductSampleNumber= function (index,each) {

                $scope.products[index].sampleNumber=each.$data;

            };
            $scope.changeProductUnitPrice = function (index,each) {
                $scope.products[index].unitPrice=each.$data;
                $scope.products[index].totalPrice = $scope.products[index].unitPrice * $scope.products[index].numberOfUnits;

            };

            $scope.changeProductNumberOfUnits = function (index,each) {
                $scope.products[index].numberOfUnits=each.$data;
                $scope.products[index].totalQuantity = $scope.products[index].unitSize * $scope.products[index].numberOfUnits;
                $scope.products[index].totalPrice = $scope.products[index].unitPrice * $scope.products[index].numberOfUnits;
            };


            $scope.calculateTotalQuantity = function(index) {
                var quantity = $scope.products[index].totalQuantity;
                return (isNaN(quantity)) ? 0 : quantity;
            };

            $scope.calculateTotalPrice = function(index) {
                var amount = $scope.products[index].totalPrice;
                return (isNaN(amount)) ? 0 : amount;
            };

            $scope.displayCalculateTotalQuantity = function(product) {
                var quantity = product.totalQuantity;
                return (isNaN(quantity)) ? 0 : quantity;
            };

            $scope.calculateTotalPrice = function(index) {
                var amount = $scope.products[index].totalPrice;
                return (isNaN(amount)) ? 0 : amount;
            };
            /* $scope.select2Options = {
             formatNoMatches: function(term) {
             var message = 'No Contacts';
             if(!$scope.$$phase) {
             $scope.$apply(function() {
             $scope.noResultsTag = term;
             });
             }
             return message;
             },
             initSelection : function (element, callback) {
             callback($element.data('$ngModelController').$modelValue);
             }
             };*/
            $scope.clearPaymentData = function(){
                /*$scope.amount=order.totalPrice-order.paidAmount;*/
                $scope.paymentStatus='';
                $scope.paymentComments='';
                $scope.paymentMode='';
                //$scope.amount=$scope.order.totalPrice-$scope.order.paidAmount;
            };

            $scope.clearStatusData = function(){
                //$scope.onchangespaymentstatus=true;
                $scope.statusComments='';
                $scope.currentStatus='';
            };

            $scope.filterByName = function (contacts, typedValue) {
                return contacts.filter(function (contact) {
                    var matches_firstvsLastName = angular.lowercase(contact.firstName +' '+contact.lastName).indexOf(angular.lowercase(typedValue)) !== -1;

                    return !contact.disabled && matches_firstvsLastName;
                });
            };

            $scope.filterByProductName=function (products, typedValue) {
                return products.filter(function (product) {
                    var matches_name = angular.lowercase(product.name).indexOf(angular.lowercase(typedValue)) !== -1;
                    return matches_name ;
                });
            };
            $scope.addProduct = function () {
                $scope.products.push({
                    product: '',
                    sampleNumber: '',
                    unitSize: '',
                    unitMeasure: '',
                    numberOfUnits: '',
                    unitPrice: '',
                    totalQuantity: '',
                    totalPrice: ''
                });
            };
            $scope.updateProducts = function (order) {
                $scope.order.products.push({
                    product: '',
                    sampleNumber: '',
                    unitSize: '',
                    unitMeasure: '',
                    numberOfUnits: '',
                    unitPrice: '',
                    totalQuantity: '',
                    totalPrice: ''
                });
                $scope.products=$scope.order.products;
            };

            $scope.updateProduct = function (order) {
                $scope.products=$scope.order.products;
            };

            $scope.showModal = false;
            $scope.toggleModal = function(){
                $scope.showModal = !$scope.showModal;
            };

            $scope.showModal2 = false;
            $scope.toggleModal2 = function(){
                $scope.showModal2 = !$scope.showModal2;
            };

            $scope.filterOrder = function (item) {
                var re = new RegExp($scope.searchText, 'i');
                return !$scope.searchText || re.test(item.currentStatus) || re.test(item.orderNumber);
            };


            $scope.toggleSideNavigation = function (id,val) {

                if(val === ''){
                    $timeout(function () {
                        panelTrigger(id).open().then(function () {
                            console.log('Trigger is done');
                        });
                    });
                }else{
                    val=val;
                }
                if (val === 'toggle') {
                    panelTrigger(id).toggle().then(function () {
                        console.log('Trigger is done');
                    });
                }else if (val === 'open') {
                    panelTrigger(id).open().then(function () {
                        console.log('Trigger is done');
                    });
                }else if (val === 'close') {
                    panelTrigger(id).close().then(function () {
                        console.log('Trigger is done');
                    });
                }else if (val === 'isOpen') {
                    panelTrigger(id).isOpen().then(function () {
                        console.log('Trigger is done');
                    });
                }
            };

        }
        ]);
app.controller('OrdersViewController', []);
app.controller('OrdersEditController', []);

app.directive('mdDatepicker', ['$mdDateLocale', function($mdDateLocale) {
    return {
        require: '?ngModel',
        restrict: 'E',
        link: function ($scope, $element, $attrs, ngModelController) {
            var inputType = angular.lowercase($attrs.type);

            if (!ngModelController || inputType === 'radio' ||
                inputType === 'checkbox') {
                return;
            }

            ngModelController.$formatters.unshift(function (value) {
                if (!ngModelController.$invalid && !angular.isUndefined(value) && typeof ngModelController.$modelValue === 'string') {
                    return $mdDateLocale.parseDate(ngModelController.$modelValue);
                } else {
                    return value;
                }
            });
        }
    };
}]);

app.directive('addStatusHistory', function(){
    return {
        restrict:'E',
        templateUrl:'modules/orders/views/create-ordersstatus.client.view.html'
    };
});

app.directive('addPaymentHistory', function(){
    return {
        restrict:'E',
        templateUrl:'modules/orders/views/create-orderpaymenthistory.client.view.html'
    };
});
app.directive('updateOrder', function(){
    return {
        restrict:'E',
        templateUrl:'modules/orders/views/edit-order.client.view.html'
    };
});

app.directive('viewOrder', function(){
    return {
        restrict:'E',
        templateUrl:'modules/orders/views/view-order.client.viewnew.html'
    };
});
app.directive('checkOut', function(){
    return {
        restrict:'E',
        templateUrl:'modules/orders/views/checkout-order.client.view.html'
    };
});
app.directive('addressOrder', function(){
    return {
        restrict:'E',
        templateUrl:'modules/orders/views/address-order.client.view.html'
    };
});
app.directive('paymentOrder', function(){
    return {
        restrict:'E',
        templateUrl:'modules/orders/views/payment-order.client.view.html'
    };
});
app.directive('orderDetails', function(){
    return {
        restrict:'E',
        templateUrl:'modules/orders/views/view-details-order.client.view.html'
    };
});
app.directive('paymentOrderView', function(){
    return {
        restrict:'E',
        templateUrl:'modules/orders/views/payment-view-order.client.view.html'
    };
});

app.directive('isNumber', function () {
    return {
        require: 'ngModel',
        restrict: 'A',
        link: function (scope, element, attr, ctrl) {
            function inputValue(val) {
                if (val) {
                    var digits = val.replace(/[^0-9.]/g, '');

                    if (digits !== val) {
                        ctrl.$setViewValue(digits);
                        ctrl.$render();
                    }
                    return parseFloat(digits);
                }
                return undefined;
            }
            ctrl.$parsers.push(inputValue);
        }
    };
});

app.directive('radioTrackBy', function(){
    return {
        restrict: 'A',
        scope: {
            ngModel: '=',
            ngValue: '='
        },
        link: function (ng) {
            if (ng.ngValue.addressLine ===ng.ngModel.addressLine && ng.ngValue.pinCode === ng.ngModel.pinCode && ng.ngValue.city === ng.ngModel.city && ng.ngValue.country === ng.ngModel.country && ng.ngValue.state === ng.ngModel.state) {
                ng.ngModel = ng.ngValue;
            }


        }
    };
});

app.directive('paymentOptions', function(){
    return {
        restrict: 'A',
        scope: {
            ngModel: '=',
            ngValue: '='

        },
        link: function (scope,ng) {
            /*if (ng.ngValue.selection ===ng.ngModel.selection &&  ng.ngValue.enabled === ng.ngModel.enabled) {
             ng.ngModel = ng.ngValue;
             }*/
            if(ng.ngModel.selection===true){
                ng.ngModel=ng.ngValue;
            }


        }
    };
});

app.directive('emailValidation', function () {
    return {
        require: 'ngModel',
        restrict: 'A',
        link: function (scope, element, attr, ctrl) {
            function inputValue(val) {
                if (val) {
                    var digits = val.replace(/[^0-9.]/g, '');

                    if (digits !== val) {
                        ctrl.$setViewValue(digits);
                        ctrl.$render();

                    }
                    //ctrl.$__error("Valid");
                    return parseFloat(digits);
                }
                //ctrl.$__error("Valid");
                return undefined;
            }
            ctrl.$parsers.push(inputValue);
        }
    };
});
app.directive('paymentmodeOptions',function () {
    return {
        restrict: 'A',
        link: function (scope, element, attr, ctrl) {

        }
    };
});
/*$scope.changeOption=function(test){
 if(test){
 console.log(test);
 }else{

 }
 return test;
 };*/
app.directive('slideable', function () {
    return {
        restrict: 'C',
        compile: function (element, attr) {
            // wrap tag
            var contents = element.html();
            element.html('<div class="slideable_content" style="margin:0 !important; padding:0 !important" >' + contents + '</div>');

            return function postLink(scope, element, attrs) {
                // default properties
                attrs.duration = (!attrs.duration) ? '1s' : attrs.duration;
                attrs.easing = (!attrs.easing) ? 'ease-in-out' : attrs.easing;
                element.css({
                    'overflow': 'hidden',
                    'height': '0px',
                    'transitionProperty': 'height',
                    'transitionDuration': attrs.duration,
                    'transitionTimingFunction': attrs.easing
                });
            };
        }
    };
})
    .directive('slideToggle', function () {
        return {
            restrict: 'A',
            link: function (scope, element, attrs) {
                var target = document.querySelector(attrs.slideToggle);
                attrs.expanded = false;
                element.bind('click', function () {
                    var content = target.querySelector('.slideable_content');
                    if (!attrs.expanded) {
                        content.style.border = '1px solid rgba(0,0,0,0)';
                        var y = content.clientHeight;
                        content.style.border = 0;
                        target.style.height = y + 'px';
                    } else {
                        target.style.height = '0px';
                    }
                    attrs.expanded = !attrs.expanded;
                });
            }
        };
    });


'use strict';

//Orders service used to communicate Orders REST endpoints
angular.module('orders').factory('Orders', ['$resource',
	function($resource) {
		return $resource('orders/:orderId', { orderId: '@_id'
		}, {
			update: {
				method: 'PUT'
			}
		});
	}
]);
'use strict';

// Configuring the Articles module
angular.module('products',[]).run(['Menus',
	function(Menus) {
		// Set top bar menu items
		Menus.addMenuItem('topbar', 'Products', 'products', 'products');
		/*Menus.addSubMenuItem('topbar', 'products', 'List Products', 'products');
		Menus.addSubMenuItem('topbar', 'products', 'New Product', 'products/create');*/
	}
]);

'use strict';

//Setting up route
angular.module('products').config(['$stateProvider',
	function($stateProvider) {
		// Products state routing
		$stateProvider.
		state('listProducts', {
			url: '/products',
			templateUrl: 'modules/products/views/list-products.client.viewnew.html'
		}).
		state('createProduct', {
			url: '/products/create',
			templateUrl: 'modules/products/views/create-product.client.view.html'
		}).
		state('viewProduct', {
			url: '/products/:productId',
			templateUrl: 'modules/products/views/view-product.client.view.html'
		}).
        state('editProduct', {
            url: '/products/:productId/edit',
            templateUrl: 'modules/products/views/edit-product.client.view.html'
        });
	}
]);

'use strict';

// Products controller
angular.module('products').controller('ProductsController', ['$scope', '$stateParams', '$location', 'Authentication', '$http', '$timeout', '$window', 'FileUploader', 'Products', 'Categories',
	function ($scope, $stateParams, $location, Authentication, $http, $timeout, $window, FileUploader, Products, Categories) {
		$scope.authentication = Authentication;
		$scope.token = Authentication.token;
        $scope.unitMeasureTypes = ['Kg', 'gram', 'litre', 'ml', 'count', 'tonne', 'Bag', 'Box', 'Carton', 'Bale', 'Candy', 'Bundle', 'Case', 'metre', 'dozen', 'inch', 'foot', 'Pack', 'Tray', 'Other'];
		$scope.categories = [];
		$scope.mainCategories = [];
		$scope.subCategories1 = [];
		$scope.subCategories2 = [];
		$scope.subCategories3 = [];
		$scope.imageURL1 = 'modules/products/img/profile/default.png';
		$scope.keyWords = [];
		// Create new Product
		$scope.create = function() {
			// Create new Product object
			var product = new Products ({
				name: this.name,
				category: this.category,
				subCategory1: this.subCategory1,
				subCategory2: this.subCategory2,
				unitSize: this.unitSize,
				unitMeasure: this.unitMeasure,
				unitPrice: this.unitPrice,
				numberOfUnits:this.numberOfUnits,
				description: this.description,
				fssaiLicenseNumber: this.fssaiLicenseNumber,
				fssaiLicenseNumberProof: this.fssaiLicenseNumberProof,
				keywords:this.keyWords,
				productImageURL1:this.imageURL1
			});
			// Redirect after save
			product.$save(function(response) {
				$location.path('settings/editmybusiness');

				// Clear form fields
				$scope.clearFields();
			}, function(errorResponse) {
				$scope.error = errorResponse.data.message;
			});
		};

		//Clear Fields Function
		$scope.clearFields = function () {
			$scope.name = '';
			$scope.category = '';
			$scope.subCategory1 = '';
			$scope.subCategory2 = '';
			$scope.unitSize = '';
			$scope.unitMeasure = '';
			$scope.unitPrice= '';
			$scope.numberOfUnits= '';
			$scope.description = '';
			$scope.fssaiLicenseNumber='';
			$scope.fssaiLicenseNumberProof='';
			$scope.keywords = [];
			$scope.imageURL1 = '';
		};

		$scope.panelChange = function () {
			$location.path('settings/editmybusiness');

			// Clear form fields
			$scope.clearFields();
		};


		// Remove existing Product
		$scope.remove = function(product) {
			if ( product ) {
				product.$remove();

				for (var i in $scope.products) {
					if ($scope.products [i] === product) {
						$scope.products.splice(i, 1);
					}
				}
			} else {
				$scope.product.$remove(function() {
					$location.path('products');
				});
			}
		};

		// Update existing Product
		$scope.update = function() {
			$scope.product.productImageURL1 = $scope.imageURL1;
			var product = $scope.product;
			product.$update(function() {
				$location.path('settings/editmybusiness');
			}, function(errorResponse) {
				$scope.error = errorResponse.data.message;
			});
		};

		$scope.changeCategory = function() {
			if($scope.category) {
				angular.forEach($scope.mainCategories, function (e) {
					if ($scope.category === e._id)
						$scope.subCategories1 = e.children;
				});
			}
		};


		$scope.changeSubCategory1 = function () {
			if ($scope.subCategory1) {
				angular.forEach($scope.subCategories1, function (e) {
					if ($scope.subCategory1 === e._id){
						var subCat1 = Categories.get({
							categoryId: e._id
						});
						subCat1.$promise.then(function (result) {
							$scope.subCategories2 = result.children;
						});
					}

				});
			}
		};


		$scope.reset = function (form) {
			// save the profile picture
			$scope.uploadProfilePicture();
			$scope.error = null;
			if (form) {
				form.$setPristine();
				//form.$setUntouched();
			}

		};

		// Find a list of Products
		$scope.findMainCategories = function () {
            $http.get('queryMainCategories').then(function (response) {
                $scope.mainCategories = response.data;
			});
		};
		// Find a list of Products
		$scope.find = function() {
			$scope.products = Products.query();
		};

		// Find existing Product
		$scope.findOne = function() {
			$scope.product = Products.get({ 
				productId: $stateParams.productId
			});
			$scope.product.$promise.then(function (result) {
				$scope.imageURL1 = result.productImageURL1;
				if (result.category) {
					//var children = $scope.category.children;
					angular.forEach($scope.mainCategories, function (e) {
						if (result.category === e._id) {
							$scope.subCategories1 = e.children;
						}
					});
				}
			});
		};

		// Create file uploader instance
		$scope.uploader = new FileUploader({
			headers: {'token': $scope.token},
			url: 'products/productPicture'
		});

		// Set file uploader image filter
		$scope.uploader.filters.push({
			name: 'imageFilter',
			fn: function (item, options) {
				var type = '|' + item.type.slice(item.type.lastIndexOf('/') + 1) + '|';
				return '|jpg|png|jpeg|bmp|gif|'.indexOf(type) !== -1;
			}
		});

		// Called after the user selected a new picture file
		$scope.uploader.onAfterAddingFile = function (fileItem) {
			if ($window.FileReader) {
				var fileReader = new FileReader();
				fileReader.readAsDataURL(fileItem._file);

				fileReader.onload = function (fileReaderEvent) {
					$timeout(function () {
						$scope.imageURL1 = fileReaderEvent.target.result;
					}, 0);
				};
			}
		};

		// Called after the user has successfully uploaded a new picture
		$scope.uploader.onSuccessItem = function (fileItem, response, status, headers) {
			// Show success message
			$scope.imgsuccess = true;
			var imageURLStr =response.imageURL;
			$scope.imageURL1 = imageURLStr.replace (/"/g,'');
			// Clear upload buttons
			$scope.cancelUpload();
		};

		// Called after the user has failed to uploaded a new picture
		$scope.uploader.onErrorItem = function (fileItem, response, status, headers) {
			// Clear upload buttons
			$scope.cancelUpload();

			// Show error message
			$scope.imgerror = response.message;
		};

		// Change user profile picture
		$scope.uploadProfilePicture = function () {
			// Clear messages
			$scope.imgsuccess = $scope.imgerror = null;

			// Start upload
			$scope.uploader.uploadAll();
		};

		// Cancel the upload process
		$scope.cancelUpload = function () {
			$scope.uploader.clearQueue();
		};

		// Keywords
		$scope.keyWord = null;
		$scope.addKey = function(){
			var index = $scope.keyWords.indexOf($scope.keyWord);
			if(index === -1) // if keyWord doesn't already exist, push it
				$scope.keyWords.push($scope.keyWord);
			$scope.keyWord = null;
		};

		$scope.removeKey = function(key){
			var index = $scope.keyWords.indexOf(key);
			if(index !== -1) // if keyWord exists then delete it
				$scope.keyWords.splice(index, 1);
		};
	}
]);

'use strict';

//Products service used to communicate Products REST endpoints
angular.module('products').factory('Products', ['$resource',
	function($resource) {
		return $resource('products/:productId', { productId: '@_id'
		}, {
			update: {
				method: 'PUT'
			}
		});
	}
]);
'use strict';

//Setting up route
angular.module('reports').config(['$stateProvider',
    function ($stateProvider) {
        // Reports state routing
        $stateProvider.state('listReports', {
            url: '/reports',
            templateUrl: 'modules/reports/views/list-reports.client.view.html'
        }).state('createReport', {
            url: '/reports/create',
            templateUrl: 'modules/reports/views/create-report.client.view.html'
        }).state('viewReport', {
            url: '/reports/:reportId',
            templateUrl: 'modules/reports/views/view-report.client.view.html'
        }).state('editReport', {
            url: '/reports/:reportId/edit',
            templateUrl: 'modules/reports/views/edit-report.client.view.html'
        });
    }
]);

'use strict';

// Reports controller
angular.module('reports').controller('ReportsController', ['$scope', '$stateParams', '$location', 'Authentication', 'Reports',
    function ($scope, $stateParams, $location, Authentication, Reports) {
        $scope.authentication = Authentication;

        // Create new Report
        $scope.create = function () {
            // Create new Report object
            var report = new Reports({
                name: this.name
            });

            // Redirect after save
            report.$save(function (response) {
                $location.path('reports/' + response._id);

                // Clear form fields
                $scope.name = '';
            }, function (errorResponse) {
                $scope.error = errorResponse.data.message;
            });
        };

        // Remove existing Report
        $scope.remove = function (report) {
            if (report) {
                report.$remove();

                for (var i in $scope.reports) {
                    if ($scope.reports [i] === report) {
                        $scope.reports.splice(i, 1);
                    }
                }
            } else {
                $scope.report.$remove(function () {
                    $location.path('reports');
                });
            }
        };

        // Update existing Report
        $scope.update = function () {
            var report = $scope.report;

            report.$update(function () {
                $location.path('reports/' + report._id);
            }, function (errorResponse) {
                $scope.error = errorResponse.data.message;
            });
        };

        // Find a list of Reports
        $scope.find = function () {
            $scope.reports = Reports.query();
        };

        // Find existing Report
        $scope.findOne = function () {
            $scope.report = Reports.get({
                reportId: $stateParams.reportId
            });
        };
    }
]);

'use strict';

//Reports service used to communicate Reports REST endpoints
angular.module('reports').factory('Reports', ['$resource',
    function ($resource) {
        return $resource('reports/:reportId', {
            reportId: '@_id'
        }, {
            update: {
                method: 'PUT'
            }
        });
    }
]);

'use strict';

//Setting up route
angular.module('todos').config(['$stateProvider',
    function ($stateProvider) {
        // Todos state routing
        $stateProvider.state('listTodos', {
            url: '/todos',
            templateUrl: 'modules/todos/views/list-todos.client.view.html'
        }).state('createTodo', {
            url: '/todos/create',
            templateUrl: 'modules/todos/views/create-todo.client.view.html'
        }).state('viewTodo', {
            url: '/todos/:todoId',
            templateUrl: 'modules/todos/views/view-todo.client.view.html'
        }).state('editTodo', {
            url: '/todos/:todoId/edit',
            templateUrl: 'modules/todos/views/edit-todo.client.view.html'
        });
    }
]);

'use strict';

// Todos controller
angular.module('todos').controller('TodosController', ['$scope', '$stateParams', '$location', 'Authentication', 'Todos',
    function ($scope, $stateParams, $location, Authentication, Todos) {
        $scope.authentication = Authentication;

        // Create new Todo
        $scope.create = function () {
            // Create new Todo object
            var todo = new Todos({
                name: this.name
            });

            // Redirect after save
            todo.$save(function (response) {
                $location.path('todos/' + response._id);

                // Clear form fields
                $scope.name = '';
            }, function (errorResponse) {
                $scope.error = errorResponse.data.message;
            });
        };

        // Remove existing Todo
        $scope.remove = function (todo) {
            if (todo) {
                todo.$remove();

                for (var i in $scope.todos) {
                    if ($scope.todos [i] === todo) {
                        $scope.todos.splice(i, 1);
                    }
                }
            } else {
                $scope.todo.$remove(function () {
                    $location.path('todos');
                });
            }
        };

        // Update existing Todo
        $scope.update = function () {
            var todo = $scope.todo;

            todo.$update(function () {
                $location.path('todos/' + todo._id);
            }, function (errorResponse) {
                $scope.error = errorResponse.data.message;
            });
        };

        // Find a list of Todos
        $scope.find = function () {
            $scope.todos = Todos.query();
        };

        // Find existing Todo
        $scope.findOne = function () {
            $scope.todo = Todos.get({
                todoId: $stateParams.todoId
            });
        };
    }
]);

'use strict';

//Todos service used to communicate Todos REST endpoints
angular.module('todos').factory('Todos', ['$resource',
    function ($resource) {
        return $resource('todos/:todoId', {
            todoId: '@_id'
        }, {
            update: {
                method: 'PUT'
            }
        });
    }
]);

'use strict';

// Config HTTP Error Handling
angular.module('users',[]).config(['$httpProvider',
	function($httpProvider) {
		// Set the httpProvider "not authorized" interceptor
		$httpProvider.interceptors.push(['$q','$location', 'Authentication','$window','localStorageService',
			function($q, $location, Authentication,$window,localStorageService) {
				return {
					responseError: function(rejection) {
						switch (rejection.status) {
							case 401:
								// Deauthenticate the global user
								Authentication.token = null;

								// Redirect to signin page
								$location.path('signin');
								break;
							case 403:
								// Add unauthorized behaviour
								break;
						}

						return $q.reject(rejection);
					},
					'request': function(config) {
						/*if ($window.localStorage.token) {
							config.headers.token = $window.localStorage.token;
							/!*$http.post('/users/me', {token:$window.localStorage.token}).success(function(response) {
								$window.user=response;
							});*!/

						}else {
							config.headers.token = Authentication.token;
						}*/

						var authData = localStorageService.get('nvipanilogindata');
						if (authData) {
							config.headers.token = authData.token;
							config.headers.deviceid = authData.deviceid;
							Authentication.token=authData.token;
							Authentication.deviceid = authData.deviceid;
							Authentication.user=authData.user;
						}else{
							config.headers.token = Authentication.token;
							config.headers.deviceid = Authentication.deviceid;

						}
						return config;
					}
				};
			}
		]);
	}
]);

'use strict';

// Setting up route
angular.module('users').config(['$stateProvider',
	function($stateProvider) {
		// Users state routing
		$stateProvider.
		state('profile', {
			url: '/settings/profile',
			templateUrl: 'modules/users/views/settings/edit-profile.client.view.html'
		}).
		state('editmybusiness', {
			url: '/settings/editmybusiness',
		templateUrl: 'modules/users/views/settings/edit-mybusiness.client.viewnew.html'
		}).
		state('password', {
			url: '/settings/password',
			templateUrl: 'modules/users/views/settings/change-password.client.viewnew.html'
		}).
		state('accounts', {
			url: '/settings/accounts',
			templateUrl: 'modules/users/views/settings/social-accounts.client.view.html'
		}).
		state('signup', {
			url: '/signup/:registerToken',
			templateUrl: 'modules/users/views/authentication/signup.client.viewnew2.html',
//
		}).
		state('register-success', {
			url: '/register/valid',
			templateUrl: 'modules/users/views/authentication/success.user.html'
		}).
		/*state('register-failure', {
			url: '/register/invalid',
			templateUrl: 'modules/users/views/authentication/failure.user.html'
		}).*/
		/*state('register', {
			url: '/register',
			templateUrl: 'modules/users/views/authentication/signin.client.viewnew2.html'
		}).*/
		state('forgot', {
			url: '/password/forgot',
			templateUrl: 'modules/users/views/password/forgot-password.client.viewnew.html'
		}).
		state('reset-invalid', {
			url: '/password/reset/invalid',
			templateUrl: 'modules/users/views/password/reset-password-invalid.client.view.html'
		}).
		state('reset-success', {
			url: '/password/reset/success',
			templateUrl: 'modules/users/views/password/reset-password-success.client.view.html'
		}).
		state('reset', {
			url: '/password/reset/:token',
			templateUrl: 'modules/users/views/password/reset-password.client.viewnew.html'
		});
	}
]);

'use strict';

angular.module('users').controller('AuthenticationController', ['$scope', '$stateParams', 'localStorageService', '$http', '$location', 'Authentication', 'deviceDetector', 'uuid',
    function ($scope, $stateParams, localStorageService, $http, $location, Authentication, deviceDetector, uuid) {
		$scope.authentication = Authentication;
		$scope.passwordcredentials={username:null,forgotPasswordOtp:null,otpVerified:false,otpGenerated:false,changePassword:false,newPassword:null,verifyPassword:null};
		var query_parameters = $location.hash;
		var currenturl=$location.protocol() + '://'+ $location.host() +':'+  $location.port();


		$scope.register=false;

		$scope.categories = [];

		$scope.path=$location.path();
		if($scope.authentication && $scope.authentication.token!== null && $scope.authentication.user && $scope.authentication.user.length!==0) {
			$location.path('/');
		}

		// If user is signed in then redirect back home
		/*if ($scope.authentication.token) $location.path('/');*/
		$scope.init = function () {
			$scope.businessCategories = [{value: 'Seller', text: 'Seller',selected : false}, {value: 'Buyer', text: 'Buyer',selected : false}, {value: 'Mediator', text: 'Mediator'}];
			$scope.category=[];
			$scope.category.seller = 'false';
			$scope.category.buyer = 'false';
			$scope.category.mediator = 'false';
			$scope.acceptTerms=true;
			$scope.businessTypesSelection = [{value: 'Trader', text: 'Trader'}, {value: 'Farmer', text: 'Farmer'}, {value: 'Wholesaler', text: 'Wholesaler'}, {
				value: 'Distributor',text: 'Distributor'
			},{value: 'Retailer', text: 'Retailer'}, {value: 'Manufacturer', text: 'Manufacturer'},
				{value: 'Importer', text: 'Importer'},
				{value: 'Exporter', text: 'Exporter'},
			];
			$scope.businessType = 'Trader';
			$scope.findMainCategories();
			$scope.isOptionsRequired();$scope.panelChange();
		};
		$scope.reset=function () {
			$scope.passwordcredentials={username:null,forgotPasswordOtp:null,otpVerified:false,otpGenerated:false,changePassword:false,newPassword:null,verifyPassword:null};
		};

		$scope.askForPasswordReset = function() {
			$scope.success = $scope.error = null;

            $http.post('/auth/forgot', $scope.passwordcredentials).then(function (response) {
				// Show user success message and clear form
				$scope.passwordcredentials = null;
                $scope.success = response.data.message;
				$scope.verifyPasswordOTP=true;
				$scope.sendPasswordOTP=true;

            }, function (response) {
				// Show user error message and clear form
				$scope.passwordcredentials = null;

                $scope.error = response.data.message;
                $scope.userstatus = response.data.userstatus;
			});
		};

		$scope.showPasswordOTP=function ($username) {
			$scope.changenumber=false;
            var numbers = new RegExp(/^[0-9]+$/);
            if(numbers.test($scope.passwordcredentials.username)) {
                $scope.changenumber=true;
            }else{
                $scope.changenumber=false;
			}
        };

		$scope.forgotPassword=function () {
			$scope.success = $scope.error = null;

            $http.post('/user/otpuserpassword', $scope.passwordcredentials).then(function (response) {

                $scope.success = response.data.message;
                $scope.showFpwd=true;
                if (response.data.verifiedOtp) {
					$scope.passwordcredentials.otpVerified=true;
					$scope.passwordcredentials.otpGenerated=false;

					$scope.passwordcredentials.changePassword=false;
				}
                if (response.data.otpGenerated) {
					$scope.passwordcredentials.otpVerified=false;
					$scope.passwordcredentials.changePassword=false;
					$scope.passwordcredentials.otpGenerated=true;

				}
                if (response.data.changePassword) {
					$scope.passwordcredentials.changePassword=true;
					$scope.passwordcredentials.otpVerified=false;
					$scope.passwordcredentials.otpGenerated=false;
					$scope.passwordcredentials={username:null,forgotPasswordOtp:null,otpVerified:false,otpGenerated:false,changePassword:false,newPassword:null,verifyPassword:null};
					$scope.showSignin = true;
					$scope.showFpwd = false;
				}
				// Attach user profile
                Authentication.token = response.data.token;

				// And redirect to the index page
				/*$location.path('/password/reset/success');*/
            }, function (response) {
                $scope.error = response.data.message;
			});
		};
		// Change user password
		$scope.resetUserPassword = function() {
			$scope.success = $scope.error = null;

            $http.post('/auth/reset/' + $stateParams.token, $scope.passwordDetails).then(function (response) {
				// If successful show success message and clear form
				$scope.passwordDetails = null;

				// Attach user profile
                Authentication.token = response.data.token;

				// And redirect to the index page
				$location.path('/password/reset/success');
            }, function (response) {
                $scope.error = response.data.message;
			});
		};
		$scope.isOptionsRequired=function () {
			$scope.invalidselection=!(($scope.category.seller && $scope.category.seller ===true)|| ($scope.category.buyer && $scope.category.buyer ===true) || ($scope.category.mediator && $scope.category.mediator===true));
		};
		$scope.changeSelection=function(){
			$scope.businessTypesSelection=[{value: 'Trader', text: 'Trader'},{value: 'Importer', text: 'Importer'},
				{value: 'Exporter', text: 'Exporter'}];
			var types=this.category;
			$scope.invalidselection=!((types.seller && types.seller===true) || (types.buyer && types.buyer===true) ||  (types.mediator && types.mediator===true));
			if(types.seller && types.seller===true) {
				if(!types.buyer || types.buyer==='false') {

					$scope.businessTypesSelection.unshift({value: 'Farmer', text: 'Farmer'}, {
						value: 'Wholesaler',
						text: 'Wholesaler'
					}, {
						value: 'Distributor', text: 'Distributor'
					}, {value: 'Manufacturer', text: 'Manufacturer'});
				}else{
					$scope.businessTypesSelection.unshift({value: 'Farmer', text: 'Farmer'}, {
						value: 'Wholesaler',
						text: 'Wholesaler'
					}, {
						value: 'Distributor', text: 'Distributor'
					}, {value: 'Retailer', text: 'Retailer'},{value: 'Manufacturer', text: 'Manufacturer'});
				}
			}else if(types.buyer && types.buyer===true){
				/*if(!types.mediator || types.mediator==='false') {*/
				$scope.businessTypesSelection.unshift({
					value: 'Wholesaler',
					text: 'Wholesaler'
				}, {
					value: 'Distributor', text: 'Distributor'
				}, {value: 'Retailer', text: 'Retailer'});
				/*}else {
				 $scope.businessTypesSelection.unshift({value: 'Retailer', text: 'Retailer'});
				 }*/
			}else if(types.mediator && types.mediator===true){
				$scope.businessTypesSelection=$scope.businessTypesSelection;
			}else{
				$scope.businessTypesSelection = [{value: 'Trader', text: 'Trader'}, {value: 'Farmer', text: 'Farmer'}, {value: 'Wholesaler', text: 'Wholesaler'}, {
					value: 'Distributor',text: 'Distributor'
				},{value: 'Retailer', text: 'Retailer'}, {value: 'Manufacturer', text: 'Manufacturer'},
					{value: 'Importer', text: 'Importer'},
					{value: 'Exporter', text: 'Exporter'},
				];
			}

		};
		$scope.findMainCategories = function () {

            /*var deferred = $q.defer();*/
			$http.get('queryMainCategories').then(function (response) {
				$scope.categories = response.data;
               /* defer.resolve($scope.categories);*/
			}, function (response) {
				$scope.error=response.data.message;
                /*defer.reject(response);*/
                // called asynchronously if an error occurs
                // or server returns response with an error status.
            });
		};
		//$scope.init();
		$scope.addEmail = function (emailsArray) {
			emailsArray.push({
				email: '',
				emailType: 'Work',
				primary: false
			});
		};
		$scope.addAddress = function (addressesArray) {
			addressesArray.push({
				addressLine: '',
				city: '',
				state: '',
				country: '',
				pinCode:'',
				addressType: 'Billing',
				primary: false
			});
		};
		$scope.addPhone = function (phonesArray) {
			/*	var emptyArray={};
			 if (!phonesArray) {
			 phonesArray=emptyArray;
			 }*/
			phonesArray.push({
				phoneNumber: '',
				phoneType: 'Mobile',
				primary: false
			});


		};
		$scope.userCategories = 'Seller';
		$scope.signup = function() {
            $http.post('/auth/signup', $scope.registerUser).then(function (response) {
				// If successful we assign the response to the global user model
                $scope.authentication.token = response.data.token;
				$http.post('/users/me', {token: $scope.authentication.token})
                    .then(function (response) {

						localStorageService.set('nvipanilogindata', {
							token: $scope.authentication.token,
                            user: response.data
						});
                        $scope.authentication.user = response.data;

						$location.path('/');
                    }, function (response) {
					localStorageService.remove('nvipanilogindata');
                        $scope.error = response.data.message;

				});
            }, function (response) {
                $scope.error = response.data.message;
			});
		};
		$scope.dashboardData = function () {
			if ($scope.authentication && $scope.authentication.user && $scope.authentication.token) {
				$http.get('/users/myhome')
					.then(function (rs) {
						var response=rs.data;
						$scope.notifications = response.notifications;
						$scope.orders = response.orders;
						$scope.offers = response.offers;
						$scope.todos = response.todos;
					},function (response) {
                    localStorageService.remove('nvipanilogindata');
                    Authentication.token = null;
                    Authentication.user = null;
                    Authentication.deviceid = null;
                    $location.path('/');
                    $scope.authentication.token = null;
                    $scope.authentication.user = null;
                    $scope.authentication.deviceid = null;
					$scope.error = response.data.message;
				});
			}
		};

		$scope.reset = function(form) {
			$scope.error = null;
			if (form) {
				form.$setPristine();
				//form.$setUntouched();
			}

		};
		$scope.askPreRegistration = function() {
			$scope.success = $scope.registererror = null;
			$scope.userregister=[];
			if (this.useMobileAsUserName) {
				this.username = this.mobile;
			} else {
				this.username = this.email;
			}
			$scope.registeruser = {
				username: this.username,
				password:this.password,
				ConfirmPassword:this.ConfirmPassword,
				useMobileAsUserName: this.useMobileAsUserName,
				firstName:this.firstName,
				lastName:this.lastName,
				companyName:this.companyName,
				businessType:this.businessType,
				categorySeller:this.category.seller && this.category.seller===true?true:false,
				categoryBuyer:this.category.buyer&& this.category.buyer===true?true:false,
				categoryMediator:this.category.mediator && this.category.mediator===true?true:false,
				email: this.email,
				mobile:this.mobile,
				acceptTerms:this.acceptTerms
			};

			// Redirect after save

			/*	if ($scope.username) {
			 $scope.userregister.username = $scope.username;
			 }
			 if ($scope.password) {
			 $scope.userregister.password = $scope.password;
			 }*/

            $http.post('/user/presignup', $scope.registeruser).then(function (response) {
				// Show user success message and clear form
				/*$scope.register.registerusername = null;*/
                $scope.success = response.data.message;
                $scope.statusToken = response.data.statusToken;

            }, function (response) {
				// Show user error message and clear form
				$scope.registererror = response.data.message;
			});
		};

		$scope.signin = function() {
            // Get Instance ID token. Initially this makes a network call, once retrieved
            // subsequent calls to getToken will return from cache.
            /*			messaging.getToken()
             .then(function(currentToken) {
             if (currentToken) {
             sendTokenToServer(currentToken);
             updateUIForPushEnabled(currentToken);
             } else {
             // Show permission request.
             console.log('No Instance ID token available. Request permission to generate one.');
             // Show permission UI.
             updateUIForPushPermissionRequired();
             setTokenSentToServer(false);
             }
             })
             .catch(function(err) {
             console.log('An error occurred while retrieving token. ', err);
             showToken('Error retrieving Instance ID token. ', err);
             setTokenSentToServer(false);
             });
             */

            $scope.credentials.devicedescription = deviceDetector.raw.userAgent;
            $scope.credentials.devicename = deviceDetector.os_version;

            $scope.credentials.deviceid = uuid.v4();

			$http.post('/auth/signin', $scope.credentials).then(function(res) {
				//If successful we assign the response to the global user model
				//$window.localStorage.token= response.token;
				var response=res.data;
				$scope.authentication.token = response.token;
                $scope.authentication.deviceid = $scope.credentials.deviceid;

				$http.post('/users/me', {token: $scope.authentication.token})
					.then(function (us) {
						var userresponse=us.data;
						$http.get('/users/myhome')
							.then(function (nr) {
                                var notificationresponse=nr.data;
								$scope.notifications = notificationresponse.notifications;
								$scope.orders = notificationresponse.orders;
								$scope.offers = notificationresponse.offers;
								$scope.todos = notificationresponse.todos;
								localStorageService.set('nvipanilogindata', {
									token: $scope.authentication.token,
                                    deviceid: $scope.authentication.deviceid,
									user: userresponse
								});
								$scope.authentication.user = userresponse;
							}, function (errorMessage) {
                            localStorageService.remove('nvipanilogindata');
                            Authentication.token = null;
                            Authentication.user = null;
                            Authentication.deviceid = null;
                            $location.path('/');
                            $scope.authentication.token = null;
                            $scope.authentication.user = null;
                            $scope.authentication.deviceid = null;
							$scope.error = errorMessage.data.message;
						});
					},function(userresponse) {
                    localStorageService.remove('nvipanilogindata');
                    Authentication.token = null;
                    Authentication.user = null;
                    Authentication.deviceid = null;
                    $location.path('/');
                    $scope.authentication.token = null;
                    $scope.authentication.user = null;
                    $scope.authentication.deviceid = null;
					$scope.error = userresponse.data.message;

				});

				/*	// And redirect to the index page
				 */
			},function(signresponse) {
				$scope.error = signresponse.data.message;
				$scope.userstatus = signresponse.data.userstatus;
			});
		};


		$scope.panelChange = function (changeStr) {
			$scope.showSignin = false;
			$scope.showFpwd = false;
			$scope.error = null;
			if (changeStr === 'fpwd') {
				$scope.showFpwd = true;
			} else {
				$scope.showSignin = true;

			}


		};

	}
]);

'use strict';
var app=angular.module('users');
app.controller('BusinessSettingsController', ['$scope', '$compile','$q','$stateParams', '$http', '$location', 'Users', '$timeout', '$element','$window', 'Authentication', 'FileUploader', 'localStorageService', 'Companies','Notification','verifyDelete','Reports','$mdDialog', '$mdMedia',
    function ($scope,$compile, $q,$stateParams, $http, $location,Users, $timeout,$element, $window, Authentication, FileUploader, localStorageService, Companies,Notification,verifyDelete ,Reports,$mdDialog, $mdMedia) {
        $scope.token = Authentication.token;
        $scope.user = Authentication.user;
        $scope.tab='currentprofile';
        // If user is not signed in then redirect back home
        if (!$scope.token) {$location.path('/');}

        $scope.phoneTypes = ['Mobile', 'Home', 'Work', 'Other'];
        $scope.emailTypes = ['Work', 'Personal', 'Other'];
       /* $scope.unitMeasureTypes = ['Kg', 'gram', 'litre', 'ml', 'count', 'tonne', 'Bag', 'Box', 'Carton', 'Bale', 'Candy', 'Bundle', 'Case', 'metre', 'dozen', 'inch', 'foot', 'Pack', 'Tray', 'Other'];
        $scope.categories = [];
        $scope.mainCategories = [];
        $scope.subCategories1 = [];
        $scope.subCategories2 = [];
        $scope.subCategories3 = [];
        $scope.allCategories=[];
        $scope.showEditProduct = false;
        $scope.viewAllProduct=false;
        $scope.showAddProduct = false;
        $scope.imageURL1 = 'modules/products/img/profile/default.png';
        $scope.keyWords = [];*/
        $scope.addressTypes = ['Billing', 'Shipping', 'Receiving','Invoice'];
        $scope.accountTypes = ['Saving', 'Current'];


        $scope.categoryProducts = [];
        $scope.paymentOptions = {
            installments: 'Installments',
            lineOfCredit: 'Line of Credit',
            payNow: 'Pay Now',
            payOnDelivery: 'Pay On Delivery'
        };
        /*    var selectOptions = {
         formatNoMatches: function(term) {
         return "<a ng-click=\"addCountry('abc');\">Add new Product</a>";
         }
         };
         */
        $scope.select2Options = {
            formatNoMatches: function(term) {
                var message = '<a ng-click="addProduct()">Add New Product:"' + term + '"</a>';
                if(!$scope.$$phase) {
                    $scope.$apply(function() {
                        $scope.productName = term;

                    });
                }
                return message;
            },
            initSelection : function (element, callback) {
                callback(element.data('$ngModelController').$modelValue);
            }
        };
        $scope.addProduct=function () {
            //alert($scope.productName);
            $scope.categoryProducts.push({name: $scope.productName});

        };
        $scope.$watch('productName', function(newVal, oldVal) {
            if(newVal && newVal !== oldVal) {
                $timeout(function() {
                    var noResultsLink = angular.element( document.querySelector('.select2-no-results'));
                    console.log(noResultsLink.contents());
                    $compile(noResultsLink.contents())($scope);
                });
            }
        }, true);
        $scope.selValTabs =function(tabvalue){
            $scope.tab=tabvalue;
            $scope.success=false;
            $scope.error=false;
            $scope.showEditOnlyProduct = false;
            $scope.showEditProduct = false;
            if($scope.tab==='businsesprofile' || $scope.tab==='paymentoptions' || $scope.tab==='paymentmode'){
                $scope.findOne();
                if($scope.company)
                $scope.imageURL= $scope.company.profileImageURL;
            }else if($scope.tab==='reports') {
                $scope.showReport=true;
                $scope.from = new Date();
                $scope.to = new Date();
                $scope.tType = 'Sell';
                $scope.transactionType=[{value: 'Sell', text: 'Transaction Date'}, {value: 'Buy', text: 'Transaction Period'}];
                $scope.reportFormats=['PDF', 'XLS'];
                $scope.reportPeriods = ['Daily', 'Monthly', 'Quarterly', 'HalfYearly', 'Yearly', 'Duration'];
                $scope.reportTypes=['Stock','SalesAndPurchase', 'Sales', 'Purchases'];
                //$scope.reportType=['Stock'];
                $scope.reportType='Stock';
                $scope.reportPeriod='Daily';
                $scope.reportFormat='PDF';
            } else {
                $scope.imageURL= $scope.user.profileImageURL;
            }
        };
        $scope.addPhone = function (phonesArray) {
            if (phonesArray) {
                phonesArray.push({
                    phoneNumber: '',
                    phoneType: 'Mobile',
                    primary: false,
                    isPublic: false
                });
            }
        };

        $scope.addEmail = function (emailsArray) {
            if (emailsArray) {
                emailsArray.push({
                    email: '',
                    emailType: 'Work',
                    primary: false,
                    isPublic: false
                });
            }
        };

        $scope.addAddress = function (addressesArray) {
            if (addressesArray) {
                addressesArray.push({
                    addressLine: '',
                    city: '',
                    state: '',
                    country: '',
                    pinCode: '',
                    addressType: 'Billing',
                    primary: false,
                    public: false
                });
            }
        };
        // Update a user business profile
        $scope.updateUserBusinessProfile = function (isValid) {
            if (isValid) {
                $scope.success = $scope.error = null;

                $scope.company.phones=$scope.company.phones.filter(function (phones) {
                    return phones.phoneNumber.length>0;
                });

                $scope.company.emails=$scope.company.emails.filter(function (emails) {
                    return emails.email.length>0;
                });

                $scope.company.addresses=$scope.company.addresses.filter(function (addresses) {
                    return !(addresses.addressLine === '' &&
                    addresses.city === '' &&
                    addresses.state === '' &&
                    addresses.country === '' &&
                    addresses.pinCode === '');
                });

                if ($scope.user.company) {
                    var company = new Companies($scope.company);

                    // Redirect after save
                    company.$update(function (response) {
                        $scope.company = response;
                        $scope.success = true;
                    }, function (errorResponse) {
                        $scope.error = errorResponse.data.message;
                    });
                } else {
                    var newCompany = new Companies($scope.company);

                    newCompany.profileUrl = newCompany.name.replace(/\s+/g, '');

                    // Redirect after save
                    newCompany.$save(function (response) {
                        $scope.company = response;
                        var user = new Users($scope.user);
                        user.company = $scope.company;
                        user.$update(function (response) {
                            $scope.success = true;
                            Authentication.user = response.user;
                            Authentication.token = response.token;
                            var authData = localStorageService.get('nvipanilogindata');
                            if (authData) {
                                Authentication.deviceid = authData.deviceid;
                            }

                            localStorageService.remove('nvipanilogindata');
                            localStorageService.set('nvipanilogindata', {
                                token: Authentication.token,
                                user: Authentication.user,
                                deviceid: Authentication.deviceid
                            });
                        }, function (response) {
                            $scope.error = response.data.message;
                        });
                    }, function (errorResponse) {
                        $scope.error = errorResponse.data.message;

                    });
                }

            } else {
                $scope.submitted = true;
            }
        };

        // Find existing Company
        $scope.findOne = function () {
            if ($scope.user.company) {
                $scope.company = Companies.get({
                    companyId: $scope.user.company
                });
                $scope.company.$promise.then(function (result) {
                    $scope.imageURL = result.profileImageURL;
                    /* console.log('Company Products'+$scope.company.products.length);*/
                    /*  $scope.products=$scope.company.inventories;*/
                    if($scope.products){
                        console.log('User Products'+$scope.products.length);
                    }

                    /*$scope.products = Products.query();*/
                });
            }
        };

        // Create file uploader instance
        $scope.uploader = new FileUploader({
            headers: {'token': $scope.token},
            url: $scope.tab==='products'?'products/productPicture':'companies/profilePicture'
        });
        $scope.uploaderCases = new FileUploader({
            headers: {'token': $scope.token},
            url: $scope.tab==='products'?'products/product':'companies/profilePicture'
        });
        /*$scope.productUploader = new FileUploader({
         headers: {'token': $scope.token},
         url: 'products/productPicture'
         });*/

        // Set file uploader image filter
        $scope.uploader.filters.push({
            name: 'imageFilter',
            fn: function (item, options) {
                var lblError = document.getElementById('lblError');
                if($scope.tab==='businsesprofile'){
                    lblError = document.getElementById('lblError');
                }else if($scope.tab==='currentprofile'){
                    lblError = document.getElementById('lblError1');
                }else if($scope.tab==='products' && $scope.showEditProduct){
                    lblError = document.getElementById('lblErrorEditProduct');
                }else if($scope.tab==='products' && $scope.showAddProduct){
                    lblError = document.getElementById('lblErrorAddProduct');

                }
                var type = '|' + item.type.slice(item.type.lastIndexOf('/') + 1) + '|';
                var validError= '|jpg|png|jpeg|bmp|gif|'.indexOf(type) !== -1;
                lblError.innerHTML='';
                if(!validError) {
                    lblError.innerHTML = 'Please upload files having extensions:jpg, png, jpeg, bmp, gif <b> .Invalid type:'+item.type;
                }else if(validError && item.size/1024/1024 >4){
                    lblError.innerHTML = 'Please upload files size should be less than  4MB <b> .Current File size:'+item.size/1024/1024;
                }
                return validError;
            }
        });

        /* var uploadOptions = {
         url: '/whatever/uploadfile',
         filters: []
         };

         // File must be jpeg or png
         uploadOptions.filters.push({ name: 'imagefilter', fn: function(item) {
         var type = '|' + item.type.slice(item.type.lastIndexOf('/') + 1) + '|';
         return '|jpg|png|jpeg|bmp|gif|'.indexOf(type) !== -1;
         }});

         // File must not be larger then some size
         uploadOptions.filters.push({ name: 'sizeFilter', fn: function(item) {
         return item.size < 10000;
         }});

         $scope.uploadOptions = uploadOptions;
         */

        // Called after the user selected a new picture file
        $scope.uploader.onAfterAddingFile = function (fileItem) {
            if ($window.FileReader) {
                var fileReader = new FileReader();
                fileReader.readAsDataURL(fileItem._file);

                fileReader.onload = function (fileReaderEvent) {
                    $timeout(function () {
                        $scope.imageURL = fileReaderEvent.target.result;
                        $scope.imageURL1 = fileReaderEvent.target.result;

                    }, 0);
                };
            }
        };
        // Called after the user has successfully uploaded a new picture
        $scope.uploadProfilePicture = function () {
            // Clear messages
            $scope.imgsuccess = $scope.imgerror = null;

            // Start upload
            $scope.uploader.uploadAll();
        };
        // Cancel the upload process
        $scope.cancelUpload = function () {
            $scope.uploader.clearQueue();
            $scope.imageURL = $scope.company.profileImageURL;
        };
        $scope.clearFields = function () {
            $scope.name = '';
            $scope.category = '';
            $scope.subCategory1 = '';
            $scope.subCategory2 = '';
            $scope.unitSize = '';
            $scope.unitMeasure = '';
            $scope.unitPrice= '';
            $scope.numberOfUnits= '';
            $scope.description = '';
            $scope.imageURL1='modules/products/img/profile/default.png';
            $scope.categories = [];
            $scope.mainCategories = [];
            $scope.subCategories1 = [];
            $scope.subCategories2 = [];
            $scope.subCategories3 = [];
            $scope.allCategories=[];
            $scope.selectedItem=null;
            $scope.showEditProduct = false;
            $scope.viewAllProduct=false;
            $scope.showAddProduct = false;
            $scope.showReport=false;
            $scope.keyWords = [];
            $scope.addressTypes = ['Billing', 'Shipping', 'Receiving','Invoice'];
            $scope.accountTypes = ['Saving', 'Current'];
            $scope.category=[];
            $scope.error = null;
            $scope.success=null;
            $scope.selectedSubCategory=null;
            $scope.selectedItem=null;
        };
        $scope.addOptions=function(){
            if($scope.company){
                $scope.company.settings.paymentOptions.installments.options.push({
                    days: 30,
                    percentage: 100,
                    description: 'Upfront Partial Payment'
                });
            }
        };


        $scope.reset = function (form) {
            // save the profile picture
            /*  $scope.uploadProfilePicture();*/
            $scope.findOne();
            $scope.error = null;
            $scope.success=null;
            if (form) {
                form.$pristine = true;
                form.$valid = true;

            }

        };
        // Keywords
        $scope.keyWord = null;
        $scope.addKey = function(){
            $scope.product.keywords = this.product.keywords.split(',');

        };

        $scope.removeKey = function(key){
            var index = $scope.keyWords.indexOf(key);
            if(index !== -1) // if keyWord exists then delete it
                $scope.keyWords.splice(index, 1);
        };

        $scope.panelChange = function (changeStr,product){
            $scope.clearFields();
         if(changeStr === 'report') {
                $scope.showReport=true;
                $scope.from = new Date();
                $scope.to = new Date();
                $scope.tType = 'Sell';
                $scope.transactionType=[{value: 'Sell', text: 'Transaction Date'}, {value: 'Buy', text: 'Transaction Period'}];
                $scope.reportFormats=['PDF', 'XLS'];
                $scope.reportPeriods = ['Daily', 'Monthly', 'Quarterly', 'HalfYearly', 'Yearly', 'Duration'];
                $scope.reportPeriod='Dialy';
                $scope.reportFormat='PDF';
            }

        };
        $scope.showReportDialog = function (ev,isOrder) {

            var useFullScreen = ($mdMedia('sm') || $mdMedia('xs'));
            $scope.showReport=true;
            $scope.from = new Date();
            $scope.to = new Date();
            $scope.tType = 'Sell';
            $scope.transactionType=[{value: 'Sell', text: 'Transaction Date'}, {value: 'Buy', text: 'Transaction Period'}];
            $scope.reportFormats=['PDF', 'XLS'];
            $scope.reportPeriods = ['Daily', 'Monthly', 'Quarterly', 'HalfYearly', 'Yearly', 'Duration'];
            $scope.reportTypes=['Stock', 'Sales', 'Purchases','SalesAndPurchase'];
            //$scope.reportType=['Stock'];
            $scope.reportType='Stock';
            $scope.reportPeriod='Daily';
            $scope.reportFormat='PDF';
            $mdDialog.show({
                template: ('<md-dialog aria-label=" Dialog">' +
                    '<md-toolbar><div class="md-toolbar-tools"><h2>Reports</h2><span flex></span>' +
                    '<md-button class="md-icon-button" ng-click="cancel()">' +
                    '<md-icon class="fa fa-minus" aria-label="Close dialog"></md-icon></md-button></div></md-toolbar>' +
                    '<md-dialog-content class="md-dialog-content">' +
                    /*'<md-input-container class="wid-100">' +*/
                    /*'<label>Select</label>' +*/
                    '<md-select aria-label="Report Type" ng-model="reportType">' +
                    '<md-option ng-repeat="eachReportType in reportTypes">{{eachReportType}}</md-option>' +
                    '</md-select>' +
                    '<md-select aria-label="Report Period" ng-model="reportPeriod">' +
                    '<md-option ng-repeat="s in reportPeriods">{{s}}</md-option>' +
                    '</md-select>' +

                    /*	'</md-input-container>' +*/

                   /* '<md-datepicker   ng-model="" md-placeholder="Delivery date">'+
                    '</md-datepicker>'+*/
                '<md-select aria-label="Report Format" ng-model="reportFormat">' +
                '<md-option ng-repeat="rt in reportFormats">{{rt}}</md-option>' +
                '</md-select>' +
                   /* '<md-input-container class="md-block">' +
                    '<label>Comments</label>' +
                    '<textarea required ng-model="statusComments"></textarea>' +
                    '</md-input-container>' +*/
                    '</md-dialog-content>' +
                    '<md-dialog-actions>' +
                    '<md-button class="md-raised md-primary" data-ng-click="generateStockReport()">Submit</md-button>' +
                    '<md-button class="md-raised md-accent" ng-click="cancel();">Cancel</md-button></md-dialog-actions>' +
                    '</md-dialog>'),
                targetEvent: ev,
                scope: $scope,
                clickOutsideToClose: true,
                fullscreen: useFullScreen,
                preserveScope: true
            });

        };
        $scope.generateStockReport=function(){
          var report= new Reports();
            /*report.reportType='Stock';*/
            report.reportPeriod=this.reportPeriod;
            report.reportFormat=this.reportFormat;
            report.reportType=this.reportType;
          /*  report.reportType='Stock';*/
            report.$save(function(response) {
                $scope.hide();
                /*if(response){
                    $scope.cancel();

                }*/

            });
        };

        $scope.hide = function () {
            $mdDialog.hide();
        };
        $scope.cancel = function () {
            $mdDialog.cancel();
        };
        $scope.IsVisible = true;
        $scope.ShowHide = function () {
            //If DIV is visible it will be hidden and vice versa.

            $scope.IsVisible = $scope.company.settings.paymentOptions.installments.enabled;
        };

        $scope.IsLoc = true;
        $scope.ShowLoc = function () {
            //If DIV is visible it will be hidden and vice versa.
            $scope.IsLoc = $scope.company.settings.paymentOptions.lineOfCredit.enabled;
        };


    }
]);/*
 app.controller('CompanyProductController',[]);
 app.controller('CompanyEditProductController',[]);*/

'use strict';

angular.module('users').controller('OTPController', ['$scope', '$stateParams', '$http', '$location', 'Authentication',
    function ($scope, $stateParams, $http, $location, Authentication) {
        $scope.authentication = Authentication;
    $scope.resendOtp=true;
    $scope.resendOtpUser=false;
        //If user is signed in then redirect back home
        if ($scope.authentication.token) $location.path('/');

        // Submit forgotten password account id
        $scope.submitEnterOTP = function () {
            $scope.success = $scope.error = null;


            $http.post('/user/otp/' + $stateParams.statusToken, $scope.credentials).then(function (response) {
                // Show user success message and clear form
                $scope.credentials = null;
                $scope.success = response.data.message;

            }, function (response) {
                // Show user error message and clear form
                $scope.credentials = null;
                $scope.error = response.data.message;
            });
        };

        $scope.panelChange=function(resendotpuser){
            if($location.path().startsWith('/resendotp')){
                $scope.resendOtpUser=true;
                $scope.resendOtp=false;

            }
            if($location.path().startsWith('/enterotp')){
                $scope.resendOtp=true;
                $scope.resendOtpUser=false;
            }

           /* if(resendotpuser==='resenduser'){
                $scope.resendOtp=false;
                $scope.resendOtpuser=true;
            }else{
                $scope.resendOtp=true;
                $scope.resendOtpuser=false;
            }*/
        };
        // Submit forgotten password account id
        $scope.resendOTP = function () {
            $scope.success = $scope.error = null;


            $http.post('/user/resendotp/' + $stateParams.statusToken, {}).then(function (response) {
                // Show user success message and clear form
                $scope.credentials = null;
                $scope.success = response.data.message;
                $scope.panelChange('resendotpuser');

            }, function (response) {
                // Show user error message and clear form
                $scope.credentials = null;
                $scope.error = response.data.message;
            });
        };

        $scope.sendOtp = function () {

        if( $scope.success) {
            $http.post('/user/otpuser/', $scope.credentials).then(function (response) {
                // Show user success message and clear form
                $scope.credentials = null;
                $scope.success = response.data.message;

               /* $scope.panelChange();*/
            }, function (response) {
                // Show user error message and clear form
                $scope.credentials = null;
                $scope.error = response.data.message;
            });

        } else {
            $http.post('/user/resendotpuser/', $scope.credentials).then(function (response) {
                // Show user success message and clear form
                $scope.credentials = null;
                $scope.success = response.data.message;
                $scope.panelChange();
                $scope.email = response.data.email;
            }, function (response) {
                // Show user error message and clear form
                $scope.credentials = null;
                $scope.error = response.data.message;
            });
        }
        };
    }
]);

'use strict';

angular.module('users').controller('PasswordController', ['$scope', '$stateParams', '$http', '$location', 'Authentication',
	function($scope, $stateParams, $http, $location, Authentication) {
		$scope.authentication = Authentication;

		//If user is signed in then redirect back home
		if ($scope.authentication.token) $location.path('/');

		// Submit forgotten password account id
		$scope.askForPasswordReset = function() {
			$scope.success = $scope.error = null;

            $http.post('/auth/forgot', $scope.credentials).then(function (response) {
				// Show user success message and clear form
				$scope.credentials = null;
                $scope.success = response.data.message;

            }, function (response) {
				// Show user error message and clear form
				$scope.credentials = null;
                $scope.error = response.data.message;
                $scope.userstatus = response.data.userstatus;
			});
		};

		// Change user password
		$scope.resetUserPassword = function() {
			$scope.success = $scope.error = null;

            $http.post('/auth/reset/' + $stateParams.token, $scope.passwordDetails).then(function (response) {
				// If successful show success message and clear form
				$scope.passwordDetails = null;

				// Attach user profile
                Authentication.token = response.data.token;

				// And redirect to the index page
				$location.path('/password/reset/success');
            }, function (response) {
                $scope.error = response.data.message;
			});
		};
	}
]);

'use strict';
var app=angular.module('users');
app.controller('SettingsController', ['$scope', '$http', '$location', 'Users', 'Authentication', 'localStorageService', '$timeout', '$window', 'FileUploader',
	function ($scope, $http, $location, Users, Authentication, localStorageService, $timeout, $window, FileUploader) {
		$scope.token = Authentication.token;
		$scope.user = Authentication.user;
		
		
$scope.copyUser=angular.copy(Authentication.user);
		$scope.imageURL = $scope.user.profileImageURL;

		// Create file uploader instance
		$scope.uploader = new FileUploader({
			headers: {'token': $scope.token},
			url: 'users/profilePicture'
		});

		// Set file uploader image filter
		/*$scope.uploader.filters.push({
			name: 'imageFilter',
			fn: function (item, options) {
				var type = '|' + item.type.slice(item.type.lastIndexOf('/') + 1) + '|';
				return '|jpg|png|jpeg|bmp|gif|'.indexOf(type) !== -1;
			}
		});*/
		$scope.uploader.filters.push({
			name: 'imageFilter',
			fn: function (item, options) {
				var lblError = document.getElementById('lblError');
				if($scope.tab==='businsesprofile'){
					lblError = document.getElementById('lblError');
				}else if($scope.tab==='currentprofile'){
					lblError = document.getElementById('lblError1');
				}else if($scope.tab==='products' && $scope.showEditProduct){
					lblError = document.getElementById('lblErrorEditProduct');
				}else if($scope.tab==='products' && $scope.showAddProduct){
					lblError = document.getElementById('lblErrorAddProduct');

				}
				var type = '|' + item.type.slice(item.type.lastIndexOf('/') + 1) + '|';
				var validError= '|jpg|png|jpeg|bmp|gif|'.indexOf(type) !== -1;
				lblError.innerHTML='';
				if(!validError) {
					lblError.innerHTML = 'Please upload files having extensions:jpg, png, jpeg, bmp, gif <b> .Invalid type:'+item.type;
				}else if(validError && item.size/1024/1024 >4){
					lblError.innerHTML = 'Please upload files size should be less than  4MB <b> .Current File size:'+item.size/1024/1024;
				}
				return validError;
			}
		});
		// Called after the user selected a new picture file
		$scope.uploader.onAfterAddingFile = function (fileItem) {
			if ($window.FileReader) {
				var fileReader = new FileReader();
				fileReader.readAsDataURL(fileItem._file);

				fileReader.onload = function (fileReaderEvent) {
					$timeout(function () {
						$scope.imageURL = fileReaderEvent.target.result;
						$scope.user.profileImageURL = $scope.imageURL;
					}, 0);
				};
			}
		};
		$scope.uploader.xhrTransport=function (item,callback) {
			var xhr = item._xhr = new XMLHttpRequest();
			var sendable;
			if (!item.disableMultipart) {
				sendable = new FormData();
				angular.forEach(item.formData, function(obj) {
					angular.forEach(obj, function(value ,key)  {
						sendable.append(key, value);
					});
				});

				sendable.append(item.alias, item._file, item.file.name);
			} else {
				sendable = item._file;
			}
			xhr.onload = function() {
				var headers = $scope.uploader._parseHeaders(xhr.getAllResponseHeaders());
				var response = $scope.uploader._transformResponse(xhr.response, headers);
				var gist = $scope.uploader._isSuccessCode(xhr.status) ? 'Success' : 'Error';
				var method = '_on' + gist + 'Item';
				$scope.uploader[method](item, response, xhr.status, headers);
				$scope.uploader._onCompleteItem(item, response, xhr.status, headers);
			};

			xhr.onerror = function(){
				var headers = $scope.uploader._parseHeaders(xhr.getAllResponseHeaders());
				var response = $scope.uploader._transformResponse(xhr.response, headers);
				$scope.uploader._onErrorItem(item, response, xhr.status, headers);
				$scope.uploader._onCompleteItem(item, response, xhr.status, headers);
			};

			xhr.onabort =function (){
				var headers = $scope.uploader._parseHeaders(xhr.getAllResponseHeaders());
				var response = $scope.uploader._transformResponse(xhr.response, headers);
				$scope.uploader._onCancelItem(item, response, xhr.status, headers);
				$scope.uploader._onCompleteItem(item, response, xhr.status, headers);
			};

			xhr.open(item.method, item.url, true);
			angular.forEach(item.headers, function(value ,name) {
				xhr.setRequestHeader(name, value);});
			xhr.send(sendable);
			callback(xhr);
			/*xhr.send(sendable,function(response){*/

			//callback(response);
			/*});*/


			/*  var request = {
			 method: item.method,
			 url:  item.url,
			 data: item._file,
			 headers:{'Content-Type': undefined}
			 };
			 angular.forEach(item.headers, function(value ,name) {
			 request.headers.name=value;
			 });
			 $http(request)
			 .success(function (d) {
			 console.log(d.fileUrl)
			 })
			 .error(function () {
			 });*/
		};

		// Called after the user has successfully uploaded a new picture
		$scope.uploader.onSuccessItem = function (fileItem, response, status, headers) {
			// Show success message
			$scope.imgsuccess = true;

			// Populate user object
			$scope.user = Authentication.user = response.user;
			$scope.token = Authentication.token = response.token;
			$scope.imageURL = $scope.user.profileImageURL;
            var authData = localStorageService.get('nvipanilogindata');
            if (authData) {
                $scope.deviceid = Authentication.deviceid = authData.deviceid;
            }
			localStorageService.remove('nvipanilogindata');
			localStorageService.set('nvipanilogindata', {
				token: $scope.token,
                user: $scope.user,
                deviceid: $scope.deviceid
			});
			// Clear upload buttons
			$scope.cancelUpload();
		};

		// Called after the user has failed to uploaded a new picture
		$scope.uploader.onErrorItem = function (fileItem, response, status, headers) {
			// Clear upload buttons
			$scope.cancelUpload();

			// Show error message
			$scope.error = response.message;
		};

		// Change user profile picture
		$scope.uploadProfilePicture = function () {
			// Clear messages
			$scope.imgsuccess = $scope.imgerror = null;

			// Start upload
			$scope.uploader.uploadAll();
		};

		// Cancel the upload process
		$scope.cancelUpload = function () {
			$scope.uploader.clearQueue();
			$scope.imageURL = $scope.user.profileImageURL;
		};

		// If user is not signed in then redirect back home
		if (!$scope.token) $location.path('/');

		// Check if there are additional accounts 
		$scope.hasConnectedAdditionalSocialAccounts = function(provider) {
			for (var i in $scope.user.additionalProvidersData) {
				return true;
			}

			return false;
		};

		// Check if provider is already in use with current user
		$scope.isConnectedSocialAccount = function(provider) {
			return $scope.user.provider === provider || ($scope.user.additionalProvidersData && $scope.user.additionalProvidersData[provider]);
		};

		// Remove a user social account
		$scope.removeUserSocialAccount = function(provider) {
			$scope.success = $scope.error = null;

			$http.delete('/users/accounts', {
				params: {
					provider: provider
				}
            }).then(function (response) {
				// If successful show success message and clear form
				$scope.success = true;
                $scope.user = Authentication.user = response.data;
            }, function (response) {
                $scope.error = response.data.message;
			});
		};

		// Update a user profile
		$scope.updateUserProfile = function(isValid) {
			if (isValid) {
				$scope.success = $scope.error = null;
				var user = new Users($scope.user);

				user.$update(function(response) {
					$scope.success = true;
					Authentication.user = response.user;
					Authentication.token = response.token;
                    var authData = localStorageService.get('nvipanilogindata');
                    if (authData) {
                        Authentication.deviceid = authData.deviceid;
                    }
					localStorageService.remove('nvipanilogindata');
					localStorageService.set('nvipanilogindata', {
						token: Authentication.token,
                        user: Authentication.user,
                        deviceid: Authentication.deviceid
					});
				}, function(response) {
					$scope.error = response.data.message;
				});
			} else {
				$scope.submitted = true;
			}
		};

		$scope.reset = function (form) {
			$scope.error = null;
			$scope.user=angular.copy($scope.copyUser);
			$scope.imageURL = $scope.user.profileImageURL;
			if (form) {
				form.$setPristine();
				//form.$setUntouched();
			}

		};

		// Change user password
		$scope.changeUserPassword = function() {
			$scope.success = $scope.error = null;

            $http.post('/users/password', $scope.passwordDetails).then(function (response) {
				// If successful show success message and clear form
				$scope.success = true;
				$scope.passwordDetails = null;
            }, function (response) {
                $scope.error = response.data.message;
			});
		};
	}
]);
app.directive('uploader', ['$q', 'httpPostFactory', function($q,httpPostFactory) {
	var slice = Array.prototype.slice;

	return {
		restrict: 'A',
		require: '?ngModel',
		uploadpathkey:'@',
		link: function(scope, element, attrs, ngModel) {
			if (!ngModel) return;

			ngModel.$render = function() {};

			element.bind('change', function(e) {
				var element = e.target;

			/*	$q.all(slice.call(element.files, 0).map(onAfterAddingFile))
					.then(function(values) {
						if (element.multiple) ngModel.$setViewValue(values);
						else ngModel.$setViewValue(values.length ? values[0] : null);
					});*/


				var formData = new FormData();
				formData.append('file', element.files[0]);
				httpPostFactory('fileupload', attrs.uploadpathkey, formData, function (callback) {
					// recieve image name to use in a ng-src
					console.log(callback);
					ngModel.$setViewValue(callback.imageURI ? callback.imageURI : null);
				});



			}); //change

		} //link
	}; //return
}]);

app.factory('httpPostFactory', ['$http', function ($http) {
	return function (file,key, data, callback) {
		$http({
			url: file,
			method: 'POST',
			data: data,
			headers: {'Content-Type': undefined,'uploadpath':key}
        }).then(function (response) {
            callback(response.data);
        }, function (response) {
            console.log(response.data);
		});
	};
}]);

'use strict';

// Authentication service for user variables
angular.module('users').factory('Authentication', [
	function() {
		var _this = this;

		_this._data = {
			user: window.user
		};

		return _this._data;
	}
]);
'use strict';

// Users service used for communicating with the users REST endpoint
angular.module('users').factory('Users', ['$resource',
	function($resource) {
		return $resource('users', {}, {
			update: {
				method: 'PUT'
			}
		});
	}
]);

/*
$(document).ready(function(){function e(e){$(e.target).prev(".panel-heading").find(".panel-title a").toggleClass("active").find("i.fa").toggleClass("fa-plus-square fa-minus-square")}$('[data-hover="dropdown"]').dropdownHover(),$(window).on("scroll load",function(){$(window).scrollTop()>0?$("#header").addClass("scrolled"):$("#header").removeClass("scrolled")}),$("input, textarea").placeholder(),$(".video-container").fitVids(),$(".panel").on("hidden.bs.collapse",e),$(".panel").on("shown.bs.collapse",e),$(".bg-slider").flexslider({animation:"fade",directionNav:!1,controlNav:!1,slideshowSpeed:8e3}),$("#modal-video .close").on("click",function(){$("#modal-video iframe").attr("src",$("#modal-video iframe").attr("src"))}),$("#testimonials-carousel").carousel({interval:8e3}),$("#config-trigger").on("click",function(e){var o=$("#config-panel"),a=$("#config-panel").is(":visible");a?o.hide():o.show(),e.preventDefault()}),$("#config-close").on("click",function(e){e.preventDefault(),$("#config-panel").hide()}),$("#color-options a").on("click",function(e){var o=$(this).attr("data-style");$("#theme-style").attr("href",o);var a=$(this).closest("li");a.addClass("active"),a.siblings().removeClass("active"),e.preventDefault()})});*/
