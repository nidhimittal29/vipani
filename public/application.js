'use strict';

//Start by defining the main module and adding the module dependencies
angular.module(ApplicationConfiguration.applicationModuleName, ApplicationConfiguration.applicationModuleVendorDependencies);

// Setting HTML5 Location Mode
angular.module(ApplicationConfiguration.applicationModuleName).config(['$locationProvider','$mdThemingProvider','$mdDateLocaleProvider',
	function($locationProvider,$mdThemingProvider,$mdDateLocaleProvider) {
        $locationProvider.hashPrefix('!');
		/*$locationProvider.html5Mode({ enabled: true, requireBase: false }).hashPrefix('!');*/
    /*    $locationProvider.html5Mode(true);*/
        $mdThemingProvider.definePalette('nvc-palette', {
            '50': 'e5f3e5',
            '100': 'bfe2be',
            '200': '94cf93',
            '300': '69bb67',
            '400': '49ad47',
            '500': '299e26',
            '600': '249622',
            '700': '1f8c1c',
            '800': '198217',
            '900': '0f700d',
            'A100': 'a4ffa3',
            'A200': '72ff70',
            'A400': '3fff3d',
            'A700': '26ff24',
            'contrastDefaultColor': 'dark',
            'contrastDarkColors': ['50','100','200','300','400','A100','A200','A400','A700'],
            'contrastLightColors': ['500','600','700','800','900']
        });
		$mdThemingProvider.theme('default')
			.primaryPalette('nvc-palette',{
				'default': '500',
				'hue-1': '100',
				'hue-2': '700',
				'hue-3': 'A100'
			})
			.accentPalette('blue-grey')
			.warnPalette('deep-orange')
			.backgroundPalette('grey');
        $mdThemingProvider.theme('error-toast');
        $mdThemingProvider.theme('success-toast');
		$mdDateLocaleProvider.formatDate = function(date) {
			return date ? moment(date).format('DD-MM-YYYY') :  moment(new Date()).format('DD-MM-YYYY');
		};

		$mdDateLocaleProvider.parseDate = function(dateString) {
			var m =moment.utc(dateString);
			return m && m.isValid() ? m.toDate() : new Date(NaN);
		};

	}

]);

//Then define the init function for starting up the application
angular.element(document).ready(function() {
	//Fixing facebook bug with redirect
	if (window.location.hash === '#_=_') window.location.hash = '#!';

	//Then init the app
	angular.bootstrap(document, [ApplicationConfiguration.applicationModuleName]);
});
