'use strict';
var app=angular.module('users');
app.controller('SettingsController', ['$scope', '$http', '$location', 'Users', 'Authentication', 'localStorageService', '$timeout', '$window', 'FileUploader',
	function ($scope, $http, $location, Users, Authentication, localStorageService, $timeout, $window, FileUploader) {
		$scope.token = Authentication.token;
		$scope.user = Authentication.user;
		$scope.context = [
			{title:'Change Password',link:''}
		];
		
$scope.copyUser=angular.copy(Authentication.user);
		$scope.imageURL = $scope.user.profileImageURL;

		// Create file uploader instance
		$scope.uploader = new FileUploader({
			headers: {'token': $scope.token},
			url: 'users/profilePicture'
		});

		// Set file uploader image filter
		/*$scope.uploader.filters.push({
			name: 'imageFilter',
			fn: function (item, options) {
				var type = '|' + item.type.slice(item.type.lastIndexOf('/') + 1) + '|';
				return '|jpg|png|jpeg|bmp|gif|'.indexOf(type) !== -1;
			}
		});*/
		$scope.uploader.filters.push({
			name: 'imageFilter',
			fn: function (item, options) {
				var lblError = document.getElementById('lblError');
				if($scope.tab==='businsesprofile'){
					lblError = document.getElementById('lblError');
				}else if($scope.tab==='currentprofile'){
					lblError = document.getElementById('lblError1');
				}else if($scope.tab==='products' && $scope.showEditProduct){
					lblError = document.getElementById('lblErrorEditProduct');
				}else if($scope.tab==='products' && $scope.showAddProduct){
					lblError = document.getElementById('lblErrorAddProduct');

				}
				var type = '|' + item.type.slice(item.type.lastIndexOf('/') + 1) + '|';
				var validError= '|jpg|png|jpeg|bmp|gif|'.indexOf(type) !== -1;
				lblError.innerHTML='';
				if(!validError) {
					lblError.innerHTML = 'Please upload files having extensions:jpg, png, jpeg, bmp, gif <b> .Invalid type:'+item.type;
				}else if(validError && item.size/1024/1024 >4){
					lblError.innerHTML = 'Please upload files size should be less than  4MB <b> .Current File size:'+item.size/1024/1024;
				}
				return validError;
			}
		});
		// Called after the user selected a new picture file
		$scope.uploader.onAfterAddingFile = function (fileItem) {
			if ($window.FileReader) {
				var fileReader = new FileReader();
				fileReader.readAsDataURL(fileItem._file);

				fileReader.onload = function (fileReaderEvent) {
					$timeout(function () {
						$scope.imageURL = fileReaderEvent.target.result;
						$scope.user.profileImageURL = $scope.imageURL;
					}, 0);
				};
			}
		};
		$scope.uploader.xhrTransport=function (item,callback) {
			var xhr = item._xhr = new XMLHttpRequest();
			var sendable;
			if (!item.disableMultipart) {
				sendable = new FormData();
				angular.forEach(item.formData, function(obj) {
					angular.forEach(obj, function(value ,key)  {
						sendable.append(key, value);
					});
				});

				sendable.append(item.alias, item._file, item.file.name);
			} else {
				sendable = item._file;
			}
			xhr.onload = function() {
				var headers = $scope.uploader._parseHeaders(xhr.getAllResponseHeaders());
				var response = $scope.uploader._transformResponse(xhr.response, headers);
				var gist = $scope.uploader._isSuccessCode(xhr.status) ? 'Success' : 'Error';
				var method = '_on' + gist + 'Item';
				$scope.uploader[method](item, response, xhr.status, headers);
				$scope.uploader._onCompleteItem(item, response, xhr.status, headers);
			};

			xhr.onerror = function(){
				var headers = $scope.uploader._parseHeaders(xhr.getAllResponseHeaders());
				var response = $scope.uploader._transformResponse(xhr.response, headers);
				$scope.uploader._onErrorItem(item, response, xhr.status, headers);
				$scope.uploader._onCompleteItem(item, response, xhr.status, headers);
			};

			xhr.onabort =function (){
				var headers = $scope.uploader._parseHeaders(xhr.getAllResponseHeaders());
				var response = $scope.uploader._transformResponse(xhr.response, headers);
				$scope.uploader._onCancelItem(item, response, xhr.status, headers);
				$scope.uploader._onCompleteItem(item, response, xhr.status, headers);
			};

			xhr.open(item.method, item.url, true);
			angular.forEach(item.headers, function(value ,name) {
				xhr.setRequestHeader(name, value);});
			xhr.send(sendable);
			callback(xhr);
			/*xhr.send(sendable,function(response){*/

			//callback(response);
			/*});*/


			/*  var request = {
			 method: item.method,
			 url:  item.url,
			 data: item._file,
			 headers:{'Content-Type': undefined}
			 };
			 angular.forEach(item.headers, function(value ,name) {
			 request.headers.name=value;
			 });
			 $http(request)
			 .success(function (d) {
			 console.log(d.fileUrl)
			 })
			 .error(function () {
			 });*/
		};

		// Called after the user has successfully uploaded a new picture
		$scope.uploader.onSuccessItem = function (fileItem, response, status, headers) {
			// Show success message
			$scope.imgsuccess = true;

			// Populate user object
			$scope.user = Authentication.user = response.user;
			$scope.token = Authentication.token = response.token;
			$scope.imageURL = $scope.user.profileImageURL;
            var authData = localStorageService.get('nvipanilogindata');
            if (authData) {
                $scope.deviceid = Authentication.deviceid = authData.deviceid;
            }
			localStorageService.remove('nvipanilogindata');
			localStorageService.set('nvipanilogindata', {
				token: $scope.token,
                user: $scope.user,
                deviceid: $scope.deviceid
			});
			// Clear upload buttons
			$scope.cancelUpload();
		};

		// Called after the user has failed to uploaded a new picture
		$scope.uploader.onErrorItem = function (fileItem, response, status, headers) {
			// Clear upload buttons
			$scope.cancelUpload();

			// Show error message
			$scope.error = response.message;
		};

		// Change user profile picture
		$scope.uploadProfilePicture = function () {
			// Clear messages
			$scope.imgsuccess = $scope.imgerror = null;

			// Start upload
			$scope.uploader.uploadAll();
		};

		// Cancel the upload process
		$scope.cancelUpload = function () {
			$scope.uploader.clearQueue();
			$scope.imageURL = $scope.user.profileImageURL;
		};

		// If user is not signed in then redirect back home
		if (!$scope.token) $location.path('/');

		// Check if there are additional accounts 
		$scope.hasConnectedAdditionalSocialAccounts = function(provider) {
			for (var i in $scope.user.additionalProvidersData) {
				return true;
			}

			return false;
		};

		// Check if provider is already in use with current user
		$scope.isConnectedSocialAccount = function(provider) {
			return $scope.user.provider === provider || ($scope.user.additionalProvidersData && $scope.user.additionalProvidersData[provider]);
		};

		// Remove a user social account
		$scope.removeUserSocialAccount = function(provider) {
			$scope.success = $scope.error = null;

			$http.delete('/users/accounts', {
				params: {
					provider: provider
				}
            }).then(function (response) {
				// If successful show success message and clear form
				$scope.success = true;
                $scope.user = Authentication.user = response.data;
            }, function (response) {
                $scope.error = response.data.message;
			});
		};

		// Update a user profile
		$scope.updateUserProfile = function(isValid) {
			if (isValid) {
				$scope.success = $scope.error = null;
				var user = new Users($scope.user);

				user.$update(function(response) {
					$scope.success = true;
					Authentication.user = response.user;
					Authentication.token = response.token;
                    var authData = localStorageService.get('nvipanilogindata');
                    if (authData) {
                        Authentication.deviceid = authData.deviceid;
                    }
					localStorageService.remove('nvipanilogindata');
					localStorageService.set('nvipanilogindata', {
						token: Authentication.token,
                        user: Authentication.user,
                        deviceid: Authentication.deviceid,
                        companyBusinessUnits:Authentication.companyBusinessUnits
					});
				}, function(response) {
					$scope.error = response.data.message;
				});
			} else {
				$scope.submitted = true;
			}
		};

		$scope.reset = function (form) {
			$scope.error = null;
			$scope.user=angular.copy($scope.copyUser);
			$scope.imageURL = $scope.user.profileImageURL;
			if (form) {
				form.$setPristine();
				//form.$setUntouched();
			}

		};

		// Change user password
		$scope.changeUserPassword = function() {
			$scope.success = $scope.error = null;

            $http.post('/users/password', $scope.passwordDetails).then(function (response) {
				// If successful show success message and clear form
				$scope.success = true;
				$scope.passwordDetails = null;
            }, function (response) {
                $scope.error = response.data.message;
			});
		};
	}
]);
