'use strict';
var app=angular.module('users');
app.controller('BusinessSettingsController', ['$scope', '$compile','$q','$stateParams', '$http', '$location', 'Users', '$timeout', '$element','$window', 'Authentication', 'FileUploader', 'localStorageService', 'Companies','Notification','Reports','$mdDialog', '$mdMedia','businessCategories','CompanyService','$mdToast','PaymentTerms',
    function ($scope,$compile, $q,$stateParams, $http, $location,Users, $timeout,$element, $window, Authentication, FileUploader, localStorageService, Companies,Notification ,Reports,$mdDialog, $mdMedia,businessCategories,CompanyService,$mdToast,PaymentTerms) {
        $scope.token = Authentication.token;
        $scope.user = Authentication.user;
        $scope.expiryDate = moment($scope.user.created).toDate().setDate(moment($scope.user.created).toDate().getDate() + 45);
        $scope.userRole=Authentication.companyBusinessUnits.userGroup.name;
        $scope.tab='currentprofile';
        // If user is not signed in then redirect back home
        if (!$scope.token) {$location.path('/');}

        $scope.phoneTypes = ['Mobile', 'Office', 'Work', 'Other'];
        $scope.emailTypes = ['Work', 'Personal', 'Other'];
        $scope.paymentTypes= ['NetTerm', 'NetTermDiscount', 'PayInAdvance', 'Installments'];
        /* $scope.categories = [];
         $scope.mainCategories = [];
         $scope.subCategories1 = [];
         $scope.subCategories2 = [];
         $scope.subCategories3 = [];
         $scope.allCategories=[];
         $scope.showEditProduct = false;
         $scope.viewAllProduct=false;
         $scope.showAddProduct = false;
         $scope.imageURL1 = 'modules/products/img/profile/default.png';
         $scope.keyWords = [];*/
        $scope.addressTypes = ['Billing', 'Shipping', 'Receiving','Invoice'];
        $scope.accountTypes = ['Saving', 'Current'];
        $scope.context = [
            {title:'Company Profile',link:''}
        ];
        $scope.phones = [{
            phoneNumber: '',
            phoneType: 'Mobile',
            primary: true
        }];
        $scope.emails = [{
            email: '',
            emailType: 'Work',
            primary: true
        }];
        $scope.addresses = [{
            addressLine: '',
            city: '',
            state: '',
            country: '',
            pinCode: '',
            addressType: 'Billing',
            primary: true
        }];
        $scope.businessUnit = {};
        $scope.categoryProducts = [];
        $scope.paymentOptions = {
            installments: 'Installments',
            lineOfCredit: 'Line of Credit',
            payNow: 'Pay Now',
            payOnDelivery: 'Pay On Delivery'
        };
        $scope.businessUnitsForBatchOperations=[];
        /*    var selectOptions = {
         formatNoMatches: function(term) {
         return "<a ng-click=\"addCountry('abc');\">Add new Product</a>";
         }
         };
         */
        $scope.searchBusinessUserText='';
        $scope.CompanyService=CompanyService;
        $scope.select2Options = {
            formatNoMatches: function(term) {
                var message = '<a ng-click="addProduct()">Add New Product:' + term + '</a>';
                if(!$scope.$$phase) {
                    $scope.$apply(function() {
                        $scope.productName = term;

                    });
                }
                return message;
            },
            initSelection : function (element, callback) {
                callback(element.data('$ngModelController').$modelValue);
            }
        };
        $scope.addProduct=function () {
            //alert($scope.productName);
            $scope.categoryProducts.push({name: $scope.productName});

        };
        $scope.$watch('productName', function(newVal, oldVal) {
            if(newVal && newVal !== oldVal) {
                $timeout(function() {
                    var noResultsLink = angular.element( document.querySelector('.select2-no-results'));
                    //console.log(noResultsLink.contents());
                    $compile(noResultsLink.contents())($scope);
                });
            }
        }, true);
        $scope.getBusinessUnitName = function(bunitId){

            var bunits = $scope.company.businessUnits.filter(function(businessUnit){
                return bunitId?businessUnit.businessUnit._id===bunitId: businessUnit.businessUnit._id===$scope.busUnitId;
            });
            if(bunits && bunits.length===1){
                return bunits[0].businessUnit.name;
            }
            else{
                return '';
            }


        };
        $scope.getBusinessUnitLocation = function(bunitId){

            var bunits = $scope.company.businessUnits.filter(function(businessUnit){
                return bunitId?businessUnit.businessUnit._id===bunitId: businessUnit.businessUnit._id===$scope.busUnitId;
            });
            if(bunits && bunits.length===1 && bunits[0].businessUnit.addresses.length>0){
                return bunits[0].businessUnit.addresses[0].state+' '+bunits[0].businessUnit.addresses[0].pinCode;
            }
            else{
                return '';
            }


        };
        $scope.getBusinessUnitIncharge = function(bunitId,status){
            var bunits = $scope.company.businessUnits.filter(function(businessUnit){
                return bunitId?businessUnit.businessUnit._id===bunitId: businessUnit.businessUnit._id===$scope.busUnitId;
            });
            if(bunits && bunits.length===1 && bunits[0].businessUnit && bunits[0].businessUnit.employees){
                var inchargeemp= bunits[0].businessUnit.employees.filter(function(employee){
                    return employee.incharge===true;
                });
                if(inchargeemp && inchargeemp.length===1){
                    if(status){
                        return inchargeemp[0];
                    }else {
                        return inchargeemp[0].user.username;
                    }
                }
            }
        };
        $scope.getBusinessUnitInchargeStatus=function (bunitId) {
            var inchargeEmp=$scope.getBusinessUnitIncharge(bunitId,true);
            if(inchargeEmp) {
                var inchargeEmpStatus = $scope.userGroups.filter(function (eachGroup) {
                    return eachGroup._id === inchargeEmp.userGroup;
                });
                if (inchargeEmpStatus && inchargeEmpStatus.length === 1) {
                    return inchargeEmpStatus[0].name;
                } else {
                    return 'Admin';
                }
            }
        };
        $scope.selValTabs =function(tabvalue){
            $scope.tab=tabvalue;
            $scope.success=false;
            $scope.error=false;
            $scope.showEditOnlyProduct = false;
            $scope.showEditProduct = false;
            if($scope.tab==='businsesprofile' || $scope.tab==='paymentoptions' || $scope.tab==='paymentmode'){
                $scope.findOne();
                if($scope.company)
                    $scope.imageURL= $scope.company.profileImageURL;
            }else if($scope.tab==='reports') {
                $scope.showReport=true;
                $scope.from = new Date();
                $scope.to = new Date();
                $scope.tType = 'Sell';
                $scope.transactionType=[{value: 'Sell', text: 'Transaction Date'}, {value: 'Buy', text: 'Transaction Period'}];
                $scope.reportFormats=['PDF', 'XLS'];
                $scope.reportPeriods = ['Daily', 'Monthly', 'Quarterly', 'HalfYearly', 'Yearly', 'Duration'];
                $scope.reportTypes=['Stock','SalesAndPurchase', 'Sales', 'Purchases'];
                //$scope.reportType=['Stock'];
                $scope.reportType='Stock';
                $scope.reportPeriod='Daily';
                $scope.reportFormat='PDF';
            } else {
                $scope.imageURL= $scope.user.profileImageURL;
            }
        };
        $scope.findUserGroups=function (isUnit) {
            if(!isUnit) {
                $scope.toggleEdit = !$scope.toggleEdit;
            }
            CompanyService.getUserGroup().then(function (response) {
                $scope.userGroups = response.data;
                return $scope.userGroups;
            },function (err) {
                $scope.error=err.data.message;
                return $scope.userGroups;
            });
        };
        $scope.addPhone = function (phonesArray) {
            if (phonesArray) {
                phonesArray.push({
                    phoneNumber: '',
                    phoneType: 'Mobile',
                    primary: false,
                    isPublic: false
                });
            }
        };

        $scope.addEmail = function (emailsArray) {
            if (emailsArray) {
                emailsArray.push({
                    email: '',
                    emailType: 'Work',
                    primary: false,
                    isPublic: false
                });
            }
        };

        $scope.addAddress = function (addressesArray,addressType) {
            if (addressesArray) {
                if(!addressType) {
                    addressesArray.push({
                        addressLine: '',
                        city: '',
                        state: '',
                        country: '',
                        pinCode: '',
                        addressType: 'Billing',
                        primary: false,
                        public: false
                    });
                }
                if(addressType){
                    addressesArray.push({
                        addressLine: '',
                        city: '',
                        state: '',
                        country: '',
                        pinCode: '',
                        addressType: addressType,
                        primary: false,
                        public: false
                    });
                }
            }
        };
        $scope.displayErrorToastMessage=function (message) {
            $mdToast.show(
                $mdToast.simple()
                    .textContent(message)
                    .theme('error-toast')
                    .position('bottom right')
                    .action('close')
                    .hideDelay(6000)
            );
        };
        // Update a user business profile
        $scope.updateCompanyBusinessProfile = function (isValid) {
            if (isValid) {
                $scope.success = $scope.error = null;

                $scope.company.phones=$scope.company.phones.filter(function (phones) {
                    return phones.phoneNumber.length>0;
                });

                $scope.company.emails=$scope.company.emails.filter(function (emails) {
                    return emails.email.length>0;
                });

                $scope.company.addresses=$scope.company.addresses.filter(function (addresses) {
                    return !(addresses.addressLine === '' &&
                        addresses.city === '' &&
                        addresses.state === '' &&
                        addresses.country === '' &&
                        addresses.pinCode === '');
                });
                if($scope.company.registrationCategory._id)
                    $scope.company.registrationCategory=$scope.company.registrationCategory._id;

                if ($scope.user.company) {
                    var company = new Companies($scope.company);

                    // Redirect after save
                    company.$update(function (response) {
                        $scope.company = response;
                        $scope.success = true;
                        $scope.editCompany=false;
                    }, function (errorResponse) {
                        $scope.error = errorResponse.data.message;
                    });
                } else {
                    var newCompany = new Companies($scope.company);

                    newCompany.profileUrl = newCompany.name.replace(/\s+/g, '');

                    // Redirect after save
                    newCompany.$save(function (response) {
                        $scope.company = response;
                        var user = new Users($scope.user);
                        user.company = $scope.company;
                        user.$update(function (response) {
                            $scope.success = true;
                            Authentication.user = response.user;
                            Authentication.token = response.token;
                            $scope.editCompany=false;
                            var authData = localStorageService.get('nvipanilogindata');
                            if (authData) {
                                Authentication.deviceid = authData.deviceid;
                            }

                            localStorageService.remove('nvipanilogindata');
                            localStorageService.set('nvipanilogindata', {
                                token: Authentication.token,
                                user: Authentication.user,
                                deviceid: Authentication.deviceid
                            });
                        }, function (response) {
                            $scope.displayErrorToastMessage(response.data.message);
                        });
                    }, function (errorResponse) {
                        $scope.displayErrorToastMessage(errorResponse.data.message);
                    });
                }

            } else {
                $scope.submitted = true;
            }
        };
        /*=====================Adding business unit (temporary link)=======================*/
        $scope.addBussiness = function (string,bunitId) {
            $scope.addBusinessUser = false;
            $scope.addBusinessUnit = false;
            if(string==='user'){
                $scope.addBusinessUnit = false;
                //to add business user to a bunit
                if(bunitId) {
                    $scope.bunitToAddTo = bunitId;
                }
                $scope.addBusinessUser = true;
            }
            else if(string==='unit'){
                if(!$scope.employees) {
                    $scope.findBusinessUsersByCompany();
                }
                $scope.addBusinessUser = false;
                $scope.addBusinessUnit = true;
                $scope.businessUnit.phones = $scope.phones;
                $scope.businessUnit.emails = $scope.emails;
                $scope.businessUnit.addresses = $scope.addresses;
            }
            $scope.businessUnitImageUrl = 'modules/contacts/img/default.png';
            $mdDialog.show({
                template: '<div class="md-dialog-container trowserView" tabindex="-1"><md-dialog class="trowser">' +
                '<add-business-unit ng-show="addBusinessUnit"></add-business-unit>\n' +
                '<add-business-user ng-show="addBusinessUser" ></add-business-user>' +
                '</md-dialog></div>',
                scope: $scope,
                clickOutsideToClose: true,
                preserveScope: true
            });
        };
        $scope.resetBusinessUnitForm=function (unitForm) {
            if(!unitForm) {

                if($scope.businessUnit.phones && $scope.businessUnit.phones.length >0){
                    angular.forEach($scope.businessUnit.phones,function (eachPhone) {
                        eachPhone.phoneNumber=null;
                        eachPhone.phoneType=null;
                    });
                }
                if($scope.businessUnit.emails && $scope.businessUnit.emails.length >0){
                    angular.forEach($scope.businessUnit.emails,function (eachEmail) {
                        eachEmail.email=null;
                        eachEmail.emailType=null;
                    });
                }
                if($scope.businessUnit.addresses && $scope.businessUnit.addresses.length >0){
                    angular.forEach($scope.businessUnit.addresses,function (eachAddress) {
                        eachAddress.addressLine=null;
                        eachAddress.city=null;
                        eachAddress.state=null;
                        eachAddress.country=null;
                        eachAddress.pinCode=null;
                    });
                }
            }
            $scope.businessUnit.businessUnitIncharge = null;

        };
        $scope.resetBusinessUserForm=function (userForm) {
            if(!userForm) {
                if($scope.businessUser.username)
                    $scope.businessUser.username = null;
            }
            if($scope.businessUser.userGroup)
                $scope.businessUser.userGroup = null;
            if($scope.businessUser.digitalSignature)
                $scope.businessUser.digitalSignature = null;
            if($scope.success)
                $scope.success=null;
            if($scope.failure)
                $scope.failure=null;
        };
        $scope.addUser=function () {
            this.businessUser.company=$scope.company._id;
            if($scope.bunitToAddTo){
                this.businessUser.bunits=[$scope.bunitToAddTo];
            }
            if(!this.businessUser.userGroup){
                $scope.displayErrorToastMessage('Please select userGroup');
            }else if(!this.businessUser.username){
                $scope.displayErrorToastMessage('Please enter user name');
            }else {
                CompanyService.addUser(this.businessUser).then(function (response) {
                    $scope.company.employees = response.data.businessUser;
                    $scope.leng = $scope.company.employees.length;
                    if (response.data.message) {
                        $scope.success = response.data.message;
                        $timeout(function () {
                            /*if(myDialog.$$state.status) {
                                $scope.cancel();
                                $scope.resetBusinessUserForm();
                            }*/
                            $scope.cancel();
                            $scope.resetBusinessUserForm();
                        }, 10000);
                    } else {
                        $scope.cancel();
                        $scope.resetBusinessUserForm();
                    }
                }, function (err) {
                    $scope.displayErrorToastMessage(err.data.message);
                });
            }
        };

        $scope.resendLink = function () {
            $scope.businessUser.company=$scope.company._id;
            var unitNames=[];
            if($scope.businessUser.businessUnits && this.businessUser.businessUnits.length <= 1) {
                var businessUserUnit = Authentication.companyBusinessUnits.businessUnits.filter(function (eachUnit) {
                    return eachUnit.businessUnit._id.toString() === $scope.businessUser.businessUnits[0].businessUnit.toString();
                });
                if(businessUserUnit && businessUserUnit.length>0){
                    unitNames.push(businessUserUnit[0].businessUnit.name);
                    $scope.businessUser.businessUnitNames=unitNames;
                }
            }else{
                angular.forEach($scope.businessUser.businessUnits,function (eachUserUnit, callback) {
                    var businessUserUnit = Authentication.companyBusinessUnits.businessUnits.filter(function (eachUnit) {
                        return eachUnit.businessUnit._id.toString() === eachUserUnit.businessUnit.toString();
                    });
                    if(businessUserUnit && businessUserUnit.length>0){
                        unitNames.push(businessUserUnit[0].businessUnit.name);
                    }
                    callback();
                },function (err) {
                    if(!err){
                        $scope.businessUser.businessUnitNames=unitNames;
                    }
                });
            }
            this.businessUser.isResendLink=true;
            CompanyService.addUser(this.businessUser).then(function(response){
                $mdToast.show(
                    $mdToast.simple()
                        .textContent(response.data.message)
                        .theme('success-toast')
                        .position('bottom right')
                        .action('close')
                        .hideDelay(6000)
                );
            },function (err) {
                $scope.displayErrorToastMessage(err.data.message);
            });
        };

        $scope.getCount=function(){
            if($scope.company && $scope.company.employees) {
                return CompanyService.getSelectedEmployeeCount($scope.company.employees);
            }else {
                return 0;
            }
        };
        $scope.closeAddUnitUser=function () {
            if($scope.addUnitUser)
                $scope.addUnitUser=false;
        };
        $scope.openUnitBusinessUser=function (unitId, isNew) {
            $scope.addUnitUser=true;
            $scope.isNew=isNew;
            $scope.selectedUserItem = null;
            $scope.selectedUser=[];
            $scope.selectedUsers=[];
            $scope.searchEmployeeText = null;
            var companyUsers=[];
            angular.forEach($scope.company.employees,function (eachEmployee) {
                var index=$scope.businessUnit.employees.filter(function (eachCompanyEmployee) {
                    return (eachCompanyEmployee.user._id ? eachCompanyEmployee.user._id.toString():eachCompanyEmployee.user.toString()) === (eachEmployee.user._id?eachEmployee.user._id.toString():eachEmployee.user.toString());
                });
                if(index.length === 0) {
                    companyUsers.push(eachEmployee);
                }
            });
            CompanyService.loadChipUsers($scope,$scope.selectedUser,companyUsers);
        };
        $scope.addMultipleUserToUnit=function (unitId) {
            var employees=[];
            angular.forEach($scope.selectedUsers,function (eachUser) {
               employees.push({user:eachUser.user._id,userGroup:eachUser.userGroup._id});
            });
            var data={unitId:unitId,employees:employees};
            CompanyService.addMultipleUserToUnit(data).then(function (res) {
                if(res.data) {
                    $scope.businessUnit.employees = res.data.employees;
                    $scope.closeAddUnitUser();
                }else {
                    $scope.displayErrorToastMessage('No responce');
                }
            },function (err) {
                $scope.displayErrorToastMessage(err.data.message);
            });
        };
        $scope.addUnitBusinessUser=function (unitId) {
            if($scope.isNew){
                $scope.businessUser.bunits=[unitId];
                $scope.businessUser.company=$scope.company._id;
                CompanyService.addUser(this.businessUser).then(function(response){
                    $scope.businessUnit.employees.push(response.data.businessUser[response.data.businessUser.length-1]);
                    $scope.businessUser.bunits='';
                    $scope.resetBusinessUserForm();
                    $scope.closeAddUnitUser();
                },function(err){
                    $scope.displayErrorToastMessage(err.data.message);
                    $scope.closeAddUnitUser();
                });
            }
        };
        $scope.getSelectedUnitEmployees=function () {
            var selectedUnitEmployees=[];
            angular.forEach($scope.businessUnit.employees,function (eachEmployee) {
                if(eachEmployee.selected){
                    selectedUnitEmployees.push(eachEmployee.user._id?eachEmployee.user._id:eachEmployee.user);
                }
            });
            return selectedUnitEmployees;
        };
        $scope.unitEmployeeBatchOperations=function (unitId) {
            CompanyService.batchBusinessUnitEmployees($scope.getSelectedUnitEmployees(),$scope.businessUnit._id).then(function (responce) {
                $scope.result=responce.data;
                if($scope.result.success) {
                    if ($scope.result.success.length > 0) {
                        $scope.businessUnit.employees=$scope.businessUnit.employees.filter(function (eachEmployee) {
                            return !eachEmployee.selected;
                        });
                        if ($scope.result.failure && $scope.result.failure.length !== 0) {
                            $scope.displayErrorToastMessage($scope.result.failure[0].message);
                        }
                    }
                }else{
                    $scope.displayErrorToastMessage($scope.result.message);
                }
            },function (err) {
                $scope.result=err.data;
                if($scope.result.success && $scope.result.success.length>0){
                    $scope.businessUnit.employees=$scope.businessUnit.employees.filter(function (eachEmployee) {
                        return !eachEmployee.selected;
                    });
                    if($scope.result.failure && $scope.result.failure.length!==0){
                        $scope.displayErrorToastMessage($scope.result.failure[0].message);
                    }
                }else if ($scope.result.failure && $scope.result.failure.length>=1) {
                    $scope.displayErrorToastMessage($scope.result.failure[0].message);
                }else{
                    $scope.displayErrorToastMessage($scope.result.message);
                }
            });
        };
        $scope.businessUserBatchOperations=function (index) {
            CompanyService.batchCompanyEmployees(index,$scope).then(function (responce) {
                $scope.result=responce.data;
                if($scope.result.success) {
                    if ($scope.result.success.length > 0) {
                        if(index === 0){
                            $scope.company.employees=$scope.company.employees.filter(function (eachEmployee) {
                                return !eachEmployee.selected;
                            });
                        }else {
                            angular.forEach($scope.result.success, function (eachSuccess) {
                                var changeEmployee = $scope.company.employees.filter(function (eachEmployee) {
                                    return eachEmployee._id ? (eachEmployee._id.toString() === eachSuccess._id.toString()) : eachEmployee.user._id.toString() === eachSuccess.companyEmployee.user._id.toString();
                                });
                                $scope.company.employees[$scope.company.employees.indexOf(changeEmployee[0])] = eachSuccess.companyEmployee;

                            });
                        }
                        if ($scope.result.failure && $scope.result.failure.length !== 0) {
                            $scope.displayErrorToastMessage($scope.result.failure[0].message);
                        }
                    }
                }else if($scope.result.failure){
                    $scope.displayErrorToastMessage($scope.result.failure[0].message);
                }else {
                    $scope.displayErrorToastMessage($scope.result.message);

                }
            },function (err) {
                $scope.result=err.data;
                if($scope.result.success && $scope.result.success.length>0){
                    if(index === 0){
                        $scope.company.employees=$scope.company.employees.filter(function (eachEmployee) {
                            return !eachEmployee.selected;
                        });
                    }else {
                        angular.forEach($scope.result.success, function (eachSuccess) {
                            var changeEmployee = $scope.company.employees.filter(function (eachEmployee) {
                                return eachEmployee._id ? (eachEmployee._id.toString() === eachSuccess._id.toString()) : eachEmployee.user._id.toString() === eachSuccess.companyEmployee.user._id.toString();
                            });
                            $scope.company.employees[$scope.company.employees.indexOf(changeEmployee[0])] = eachSuccess.companyEmployee;

                        });
                    }
                    if($scope.result.failure && $scope.result.failure.length!==0){
                        $scope.displayErrorToastMessage($scope.result.failure[0].message);
                    }
                }else if ($scope.result.failure && $scope.result.failure.length>=1) {
                    $scope.displayErrorToastMessage($scope.result.failure[0].message);
                }else{
                    $scope.displayErrorToastMessage($scope.result.message);
                }
            });
        };

        $scope.addBUnit=function () {
            var bu = this.businessUnit;
            if(bu.businessUnitIncharge) {
                bu.employees = [];
                var incharge = $scope.inchargeUsers.employees.filter(function(employee){
                   return employee.user._id === bu.businessUnitIncharge;
                });
                if(incharge && incharge.length===1) {
                    this.businessUnit.employees.push({
                        user: incharge[0].user._id,
                        incharge: true,
                        userGroup: incharge[0].userGroup._id
                    });
                }

            }
            this.businessUnit.phones = this.businessUnit.phones.filter(function (phones) {
                return phones.phoneNumber.length > 0;
            });

            this.businessUnit.emails = this.businessUnit.emails.filter(function (emails) {
                return emails.email.length > 0;
            });

            this.businessUnit.addresses = this.businessUnit.addresses.filter(function (addresses) {
                return !(addresses.addressLine === '' &&
                    addresses.city === '' &&
                    addresses.state === '' &&
                    addresses.country === '' &&
                    addresses.pinCode === '');
            });
            this.businessUnit.company=$scope.company._id;
            /* this.businessUnit.phones = this.phones;
             this.businessUnit.emails = this.emails;*/
            CompanyService.createBusinessUnit(this.businessUnit).then(function (response) {
                if($scope.company) {
                    $scope.company.businessUnits.push({businessUnit:response.data});
                    Authentication.companyBusinessUnits.businessUnits.push({businessUnit:{name:response.data.name,_id:response.data._id},defaultBusinessUnit:false});
                    localStorageService.set('nvipanilogindata', {
                        token: $scope.token,
                        deviceid: Authentication.deviceid,
                        user: $scope.user,
                        companyBusinessUnits:Authentication.companyBusinessUnits
                    });
                    $scope.cancel();
                    $scope.resetBusinessUnitForm();
                }
            },function(err){
                $scope.displayErrorToastMessage(err.data.message);
            });

        };

        $scope.updateBusinessUser=function (value) {
            CompanyService.updateUser($scope.businessUser,value).then(function (response) {
                /*if(response.data.employees){
                    $scope.company.employees=response.data.employees;
                }else {
                    $scope.businessUser = response.data;
                }*/
                $scope.company.employees=response.data.employees;
                $scope.cancel();
            },function (err) {
                $scope.displayErrorToastMessage(err.data.message);
            });

        };
        $scope.getEachEmployeeStatus=function (eachEmployee) {
            if(eachEmployee) {
                if (eachEmployee.status === 'Active') {
                    eachEmployee.toggle = true;
                } else {
                    eachEmployee.toggle = false;
                }
            }
        };
        $scope.updateEmployeeStatus=function (eachEmployee,id) {
            if(eachEmployee.toggle){
                eachEmployee.status='Active';
            }else {
                eachEmployee.status='Inactive';
            }
            eachEmployee.company=eachEmployee.user.company;
            CompanyService.updateUser(eachEmployee,eachEmployee.status==='Active'?2:3).then(function (response) {
                $scope.company.employees = response.data.employees;
            },function (err) {
                var elem = angular.element(document.querySelector('#businessUser-'+id));
                $scope.displayErrorToastMessage(err.data.message);
            });
        };
        $scope.getEachBusinessUnitStatus=function (eachUnit) {
            if(eachUnit){
                if(eachUnit.disabled){
                    eachUnit.toggle=false;
                }else{
                    eachUnit.toggle=true;
                }
            }
        };
        $scope.updateBusinessUnit=function (businessUnit,value) {
            this.businessUnit=businessUnit;
            this.businessUnit.phones = this.businessUnit.phones.filter(function (phones) {
                return phones.phoneNumber.length > 0;
            });

            this.businessUnit.emails = this.businessUnit.emails.filter(function (emails) {
                return emails.email.length > 0;
            });

            this.businessUnit.addresses = this.businessUnit.addresses.filter(function (addresses) {
                return !(addresses.addressLine === '' &&
                    addresses.city === '' &&
                    addresses.state === '' &&
                    addresses.country === '' &&
                    addresses.pinCode === '');
            });
            this.businessUnit.company=this.company._id;
            if(value === 4)
                var deletedBusinessUnit=this.businessUnit;

            CompanyService.updateBusinessUnit(businessUnit,value).then(function(response){
                if(response.data){
                    if(value === 1 || value === 2 || value === 3) {
                        var businessUnit = response.data;
                        var businessUnitIndex = $scope.company.businessUnits.filter(function (eachUnit) {
                            return eachUnit.businessUnit._id.toString() === businessUnit._id.toString();
                        });
                        if(!businessUnit.delete) {
                            if (businessUnitIndex.length === 1) {

                                $scope.company.businessUnits[$scope.company.businessUnits.indexOf(businessUnitIndex[0])].businessUnit = businessUnit;
                                if (businessUnit.disable)
                                    $scope.company.businessUnits[$scope.company.businessUnits.indexOf(businessUnitIndex[0])].status = 'Inactive';
                                if (!businessUnit.disable)
                                    $scope.company.businessUnits[$scope.company.businessUnits.indexOf(businessUnitIndex[0])].status = 'Active';
                                $scope.getEachBusinessUnitStatus(businessUnit);
                            } else {
                                $scope.displayErrorToastMessage('UI display problem.');
                            }
                        }else{
                            $scope.company.businessUnits.splice($scope.company.businessUnits[$scope.company.businessUnits.indexOf(businessUnitIndex[0])],1);
                        }
                        var busUnit=Authentication.companyBusinessUnits.businessUnits.filter(function (eachUnit) {
                            return eachUnit.businessUnit._id.toString() === businessUnit._id;
                        });
                        if(businessUnit.disabled){
                            Authentication.companyBusinessUnits.businessUnits.splice(Authentication.companyBusinessUnits.businessUnits.indexOf(busUnit[0]),1);
                        }else if(!businessUnit.disabled){
                            if(busUnit.length === 0){
                                Authentication.companyBusinessUnits.businessUnits.push({businessUnit:{name:businessUnit.name,_id:businessUnit._id},defaultBusinessUnit:businessUnitIndex[0].defaultBusinessUnit});
                            }else {
                                Authentication.companyBusinessUnits.businessUnits[Authentication.companyBusinessUnits.businessUnits.indexOf(busUnit[0])].businessUnit = {
                                    _id: businessUnit._id,
                                    name: businessUnit.name
                                };
                            }
                        }
                        localStorageService.set('nvipanilogindata', {
                            token: $scope.token,
                            deviceid: Authentication.deviceid,
                            user: $scope.user,
                            companyBusinessUnits:Authentication.companyBusinessUnits
                        });
                    }else if(value === 4){
                        if(response.data.businessUnits){
                            //$scope.company=response.data;
                            $scope.company.businessUnits=response.data.businessUnits;
                            var busUnitIndex=Authentication.companyBusinessUnits.businessUnits.filter(function (eachUnit) {
                                return eachUnit.businessUnit._id.toString() === deletedBusinessUnit._id;
                            });
                            Authentication.companyBusinessUnits.businessUnits.splice(Authentication.companyBusinessUnits.businessUnits.indexOf(busUnitIndex[0]),1);
                            //Authentication.companyBusinessUnits.businessUnits.push({businessUnit:{name:response.data.name,_id:response.data._id},defaultBusinessUnit:false});
                            localStorageService.set('nvipanilogindata', {
                                token: $scope.token,
                                deviceid: Authentication.deviceid,
                                user: $scope.user,
                                companyBusinessUnits:Authentication.companyBusinessUnits
                            });
                        }
                    }else{
                        $scope.displayErrorToastMessage('Operation is not possible');
                    }
                    $scope.cancel();
                    $scope.resetBusinessUserForm();
                    $scope.closeAddUnitUser();
                }
            },function(err){
                $scope.displayErrorToastMessage(err);
            });

        };
        $scope.findBunitUsers = function(){
            CompanyService.getBusinessUnitUsers($scope.businessUnit._id).then(function(response){
                $scope.buUsers=response.data;
            },function (err) {
                $scope.displayErrorToastMessage(err.data.message);
            });
        };
        $scope.findBusinessUsersByCompany=function (pageno) {
            var paginationfield={};
            if(!isNaN(pageno)){
                paginationfield={page:pageno,limit:$scope.itemsPerPage};
            }
            if(this.searchBusinessUserText && this.searchBusinessUserText.length>0){
                paginationfield.searchBusinessUserText=this.searchBusinessUserText;
            }
            CompanyService.getBusinessUsersByCompany($scope.user.company,paginationfield).then(function (response) {
                $scope.company.employees = response.data.employees;
                $scope.total_count=response.data.total_count;
            },function (err) {
                $scope.displayErrorToastMessage(err.data.message);
            });
        };

        /*====================== End of adding business unit ===============================*/
        $scope.changeSelection=function(){
            $scope.businessTypesSelection=businessCategories.changeSelection($scope.company.category);
        };

        $scope.findInchargeUsers = function(){
            CompanyService.getBUInchargeUsers($scope.company._id).then(function(response){
                $scope.inchargeUsers = response.data;
            },function (err) {
                $scope.displayErrorToastMessage(err.data.message);
            });
        };
        // Find existing Company
        $scope.findOne = function (company) {
            if(!company) {
                if ($scope.user.company) {
                    $scope.companiesData = Companies.get({
                        companyId: $scope.user.company
                    });
                    $scope.companiesData.$promise.then(function (result) {
                        if (result.registrationCategories) {
                            $scope.company = result.company;
                            $scope.company.registrationCategories = result.company.registrationCategories;
                            $scope.imageURL = result.profileImageURL;
                        } else {
                            $scope.company = result;
                        }
                    });
                }
            }else {
                CompanyService.getBusinessUnits($scope.user.company).then(function (response) {
                    if($scope.company) {
                        $scope.company.businessUnits = response.data.businessUnits;
                    }else{
                        $scope.company=response.data;
                        $scope.company.businessUnits = response.data.businessUnits;
                    }
                },function (err) {
                    $scope.displayErrorToastMessage(err.data.message);
                });
            }
        };

        // Create file uploader instance
        $scope.uploader = new FileUploader({
            headers: {'token': $scope.token},
            url: $scope.tab==='products'?'products/productPicture':'companies/profilePicture'
        });
        $scope.uploaderCases = new FileUploader({
            headers: {'token': $scope.token},
            url: $scope.tab==='products'?'products/product':'companies/profilePicture'
        });
        /*$scope.productUploader = new FileUploader({
         headers: {'token': $scope.token},
         url: 'products/productPicture'
         });*/

        // Set file uploader image filter
        $scope.uploader.filters.push({
            name: 'imageFilter',
            fn: function (item, options) {
                var lblError = document.getElementById('lblError');
                if($scope.tab==='businsesprofile'){
                    lblError = document.getElementById('lblError');
                }else if($scope.tab==='currentprofile'){
                    lblError = document.getElementById('lblError1');
                }else if($scope.tab==='products' && $scope.showEditProduct){
                    lblError = document.getElementById('lblErrorEditProduct');
                }else if($scope.tab==='products' && $scope.showAddProduct){
                    lblError = document.getElementById('lblErrorAddProduct');

                }
                var type = '|' + item.type.slice(item.type.lastIndexOf('/') + 1) + '|';
                var validError= '|jpg|png|jpeg|bmp|gif|'.indexOf(type) !== -1;
                lblError.innerHTML='';
                if(!validError) {
                    lblError.innerHTML = 'Please upload files having extensions:jpg, png, jpeg, bmp, gif <b> .Invalid type:'+item.type;
                }else if(validError && item.size/1024/1024 >4){
                    lblError.innerHTML = 'Please upload files size should be less than  4MB <b> .Current File size:'+item.size/1024/1024;
                }
                return validError;
            }
        });

        /* var uploadOptions = {
         url: '/whatever/uploadfile',
         filters: []
         };

         // File must be jpeg or png
         uploadOptions.filters.push({ name: 'imagefilter', fn: function(item) {
         var type = '|' + item.type.slice(item.type.lastIndexOf('/') + 1) + '|';
         return '|jpg|png|jpeg|bmp|gif|'.indexOf(type) !== -1;
         }});

         // File must not be larger then some size
         uploadOptions.filters.push({ name: 'sizeFilter', fn: function(item) {
         return item.size < 10000;
         }});

         $scope.uploadOptions = uploadOptions;
         */

        // Called after the user selected a new picture file
        $scope.uploader.onAfterAddingFile = function (fileItem) {
            if ($window.FileReader) {
                var fileReader = new FileReader();
                fileReader.readAsDataURL(fileItem._file);

                fileReader.onload = function (fileReaderEvent) {
                    $timeout(function () {
                        $scope.imageURL = fileReaderEvent.target.result;
                        $scope.imageURL1 = fileReaderEvent.target.result;

                    }, 0);
                };
            }
        };
        // Called after the user has successfully uploaded a new picture
        $scope.uploadProfilePicture = function () {
            // Clear messages
            $scope.imgsuccess = $scope.imgerror = null;

            // Start upload
            $scope.uploader.uploadAll();
        };
        // Cancel the upload process
        $scope.cancelUpload = function () {
            $scope.uploader.clearQueue();
            $scope.imageURL = $scope.company.profileImageURL;
        };

        $scope.clearFields = function () {
            $scope.name = '';
            $scope.category = '';
            $scope.subCategory1 = '';
            $scope.subCategory2 = '';
            $scope.unitSize = '';
            $scope.unitMeasure = '';
            $scope.unitPrice= '';
            $scope.numberOfUnits= '';
            $scope.description = '';
            $scope.imageURL1='modules/products/img/profile/default.png';
            $scope.categories = [];
            $scope.mainCategories = [];
            $scope.subCategories1 = [];
            $scope.subCategories2 = [];
            $scope.subCategories3 = [];
            $scope.allCategories=[];
            $scope.selectedItem=null;
            $scope.showEditProduct = false;
            $scope.viewAllProduct=false;
            $scope.showAddProduct = false;
            $scope.showReport=false;
            $scope.keyWords = [];
            $scope.addressTypes = ['Billing', 'Shipping', 'Receiving','Invoice'];
            $scope.accountTypes = ['Saving', 'Current'];
            $scope.category=[];
            $scope.error = null;
            $scope.success=null;
            $scope.selectedSubCategory=null;
            $scope.selectedItem=null;
        };
        $scope.addOptions=function(){
            if($scope.company){
                $scope.company.settings.paymentOptions.installments.options.push({
                    days: 30,
                    percentage: 100,
                    description: 'Upfront Partial Payment'
                });
            }
        };


        $scope.reset = function (form) {
            // save the profile picture
            /*  $scope.uploadProfilePicture();*/
            $scope.findOne();
            $scope.error = null;
            $scope.success=null;
            if (form) {
                form.$pristine = true;
                form.$valid = true;

            }

        };
        // Keywords
        $scope.keyWord = null;
        $scope.addKey = function(){
            $scope.product.keywords = this.product.keywords.split(',');

        };

        $scope.removeKey = function(key){
            var index = $scope.keyWords.indexOf(key);
            if(index !== -1) // if keyWord exists then delete it
                $scope.keyWords.splice(index, 1);
        };
        function addUser(data) {
            CompanyService.addBusinessUnit(data);

        }

        $scope.getSelectedUnits = function(){
            if($scope.company && $scope.company.businessUnits) {
                return CompanyService.getSelectedBusinessUnitCount($scope,true);
            }else {
                return 0;
            }
        };


        $scope.businessUnitBatchOperations = function(index){
            if(index === 0){
                var deletedBusinessUnits=CompanyService.getSelectedBusinessUnitCount($scope,false);
            }
            CompanyService.batchBusinessUnit(index,$scope).then(function (response) {
                $scope.company=response.data;
                if(index === 0){
                    angular.forEach(deletedBusinessUnits,function (eachDelUnit) {
                        var busUnit=Authentication.companyBusinessUnits.businessUnits.filter(function (eachUnit) {
                            return eachUnit.businessUnit._id.toString() === eachDelUnit._id;
                        });
                        var authdata= localStorageService.get('nvipanilogindata');
                        authdata.companyBusinessUnits.businessUnits.splice(Authentication.companyBusinessUnits.businessUnits.indexOf(busUnit[0]),1);
                        if(authdata.companyBusinessUnits.defaultBusinessUnitId === eachDelUnit._id) {
                           var defaultUnit = $scope.company.businessUnits.filter(function (eachUnit) {
                                return eachUnit.defaultBusinessUnit === true;
                            });
                            authdata.companyBusinessUnits.defaultBusinessUnitId=defaultUnit[0].businessUnit._id;
                        }
                        localStorageService.set('nvipanilogindata',authdata);
                        /*Authentication.companyBusinessUnits.businessUnits.splice(Authentication.companyBusinessUnits.businessUnits.indexOf(busUnit[0]),1);
                        localStorageService.set('nvipanilogindata', {
                            token: $scope.token,
                            deviceid: Authentication.deviceid,
                            user: $scope.user,
                            companyBusinessUnits:Authentication.companyBusinessUnits
                        });*/
                    });
                }
            },function (err) {
                $scope.displayErrorToastMessage(err.data.message);
            });
        };
        $scope.getBusinessUnitUsers=function (id) {
            CompanyService.getBusinessUnitUsers(id).then(function (response) {
                var activeEmployee=[];
                angular.forEachSeries(response.data.employees,function (eachEmployee, callback) {
                    var matchedEmp=$scope.company.employees.filter(function (eachCemployee) {
                        return ((eachCemployee.user._id?eachCemployee.user._id.toString():eachCemployee.user.toString()) === eachEmployee.user._id.toString()) && (eachCemployee.status !== 'Inactive');
                    });
                    if(matchedEmp && matchedEmp.length>0){
                        activeEmployee.push(eachEmployee);
                        callback();
                    }else{
                        callback();
                    }
                },function (err) {
                    if(err){
                        $scope.displayErrorToastMessage(err);
                    }else{
                        $scope.businessUnit.employees=activeEmployee;
                    }
                });
            });
        };
        $scope.businessUnitStatusUpdate=function (index, id) {
            var updateUnit=$scope.company.businessUnits.filter(function(businessUnit) {
                return businessUnit.businessUnit._id === id;
            });
            $scope.company.businessUnits[$scope.company.businessUnits.indexOf(updateUnit[0])].businessUnit.seleted=true;
            $scope.businessUnitBatchOperations(index);
        };
        $scope.panelChange = function (changeStr,id){
            $scope.clearFields();
            if(changeStr === 'businessUnit' ||  changeStr === 'businessUser') {
                if (changeStr === 'businessUnit') {
                    $scope.viewBusinessUser = false;
                    $scope.viewBusinessUnit = true;
                    $scope.bunitToAddTo=id.businessUnit._id;
                    $scope.isDefaultBusinessUnit=id.defaultBusinessUnit;
                    $scope.findUserGroups();
                    CompanyService.getBusinessUnit(id.businessUnit._id).then(function (response) {
                        $scope.businessUnit=response.data;
                    });
                    $scope.itemsPerPage=0;

                }else if (changeStr === 'businessUser') {
                    $scope.viewBusinessUnit = false;
                    $scope.viewBusinessUser = true;
                    $scope.businessUser=id;
                    $scope.businessUser.company=$scope.company._id;

                }
                $mdDialog.show({
                    template: '<div class="md-dialog-container trowserView" ><md-dialog class="trowser" aria-label="fav-dialog">\n' +
                    '<edit-business-unit ng-show="viewBusinessUnit"></edit-business-unit>\n' +
                    '<edit-business-user ng-show="viewBusinessUser"></edit-business-user>\n' +
                    '</md-dialog></div>',
                    scope: $scope,
                    clickOutsideToClose: true,
                    preserveScope: true
                });

            } else if(changeStr === 'report') {
                $scope.showReport=true;
                $scope.from = new Date();
                $scope.to = new Date();
                $scope.tType = 'Sell';
                $scope.transactionType=[{value: 'Sell', text: 'Transaction Date'}, {value: 'Buy', text: 'Transaction Period'}];
                $scope.reportFormats=['PDF', 'XLS'];
                $scope.reportPeriods = ['Daily', 'Monthly', 'Quarterly', 'HalfYearly', 'Yearly', 'Duration'];
                $scope.reportPeriod='Dialy';
                $scope.reportFormat='PDF';
            }else if(changeStr === 'businessUserViewAll'){
                $scope.paginationId='businessUsers';
                $scope.pageno=1;
                $scope.total_count = 0;
                $scope.itemsPerPage = 0;
                $scope.findBusinessUsersByCompany($scope.pageno);
            }

        };
        $scope.showReportDialog = function (ev,isOrder) {

            var useFullScreen = ($mdMedia('sm') || $mdMedia('xs'));
            $scope.showReport=true;
            $scope.from = new Date();
            $scope.to = new Date();
            $scope.tType = 'Sell';
            $scope.transactionType=[{value: 'Sell', text: 'Transaction Date'}, {value: 'Buy', text: 'Transaction Period'}];
            $scope.reportFormats=['PDF', 'XLS'];
            $scope.reportPeriods = ['Daily', 'Monthly', 'Quarterly', 'HalfYearly', 'Yearly', 'Duration'];
            $scope.reportTypes=['Stock', 'Sales', 'Purchases','SalesAndPurchase'];
            //$scope.reportType=['Stock'];
            $scope.reportType='Stock';
            $scope.reportPeriod='Daily';
            $scope.reportFormat='PDF';
            $mdDialog.show({
                template: ('<md-dialog aria-label=" Dialog">' +
                    '<md-toolbar><div class="md-toolbar-tools"><h2>Reports</h2><span flex></span>' +
                    '<md-button class="md-icon-button" ng-click="cancel()">' +
                    '<md-icon class="fa fa-minus" aria-label="Close dialog"></md-icon></md-button></div></md-toolbar>' +
                    '<md-dialog-content class="md-dialog-content">' +
                    /*'<md-input-container class="wid-100">' +*/
                    /*'<label>Select</label>' +*/
                    '<md-select aria-label="Report Type" ng-model="reportType">' +
                    '<md-option ng-repeat="eachReportType in reportTypes">{{eachReportType}}</md-option>' +
                    '</md-select>' +
                    '<md-select aria-label="Report Period" ng-model="reportPeriod">' +
                    '<md-option ng-repeat="s in reportPeriods">{{s}}</md-option>' +
                    '</md-select>' +

                    /*	'</md-input-container>' +*/

                    /* '<md-datepicker   ng-model="" md-placeholder="Delivery date">'+
                     '</md-datepicker>'+*/
                    '<md-select aria-label="Report Format" ng-model="reportFormat">' +
                    '<md-option ng-repeat="rt in reportFormats">{{rt}}</md-option>' +
                    '</md-select>' +
                    /* '<md-input-container class="md-block">' +
                     '<label>Comments</label>' +
                     '<textarea required ng-model="statusComments"></textarea>' +
                     '</md-input-container>' +*/
                    '</md-dialog-content>' +
                    '<md-dialog-actions>' +
                    '<md-button class="md-raised md-primary" data-ng-click="generateStockReport()">Submit</md-button>' +
                    '<md-button class="md-raised md-accent" ng-click="cancel();">Cancel</md-button></md-dialog-actions>' +
                    '</md-dialog>'),
                targetEvent: ev,
                scope: $scope,
                clickOutsideToClose: true,
                fullscreen: useFullScreen,
                preserveScope: true
            });

        };
        $scope.editBsedOnUserRole=function (businessUnit) {
          if($scope.userRole==='Employee'){
              angular.element(document.querySelector('#accountNo')).attr('readonly', 'readonly');
              angular.element(document.querySelector('#accountType')).attr('readonly', 'readonly');
              angular.element(document.querySelector('#bankName')).attr('readonly', 'readonly');
              angular.element(document.querySelector('#ifscCode')).attr('readonly', 'readonly');
              angular.element(document.querySelector('#gstinNo')).attr('readonly', 'readonly');
              angular.element(document.querySelector('#panNo')).attr('readonly', 'readonly');
              for(var i=0;i<businessUnit.phones.length;i++){
                  angular.element(document.querySelector('#phoneType-'+i)).attr('readonly', 'readonly');
                  angular.element(document.querySelector('#phoneNumber-'+i)).attr('readonly', 'readonly');
              }
              for(var j=0;j<businessUnit.emails.length;j++){
                  angular.element(document.querySelector('#emailType-'+j)).attr('readonly', 'readonly');
                  angular.element(document.querySelector('#email-'+j)).attr('readonly', 'readonly');
              }
              for(var k=0;k<businessUnit.addresses.length;k++){
                  angular.element(document.querySelector('#addressLine-'+k)).attr('readonly', 'readonly');
                  angular.element(document.querySelector('#city-'+k)).attr('readonly', 'readonly');
                  angular.element(document.querySelector('#state-'+k)).attr('readonly', 'readonly');
                  angular.element(document.querySelector('#country-'+k)).attr('readonly', 'readonly');
                  angular.element(document.querySelector('#pinCode-'+k)).attr('readonly', 'readonly');

              }

              //$element('#accountNo').attr('readonly', 'readonly');
          }else {
              angular.element(document.querySelector('#accountNo')).removeAttr('readonly');
              angular.element(document.querySelector('#accountType')).removeAttr('readonly');
              angular.element(document.querySelector('#bankName')).removeAttr('readonly');
              angular.element(document.querySelector('#ifscCode')).removeAttr('readonly');
              angular.element(document.querySelector('#gstinNo')).removeAttr('readonly');
              angular.element(document.querySelector('#panNo')).removeAttr('readonly');
              for(var a=0;a<businessUnit.phones.length;a++){
                  angular.element(document.querySelector('#phoneType-'+a)).removeAttr('readonly');
                  angular.element(document.querySelector('#phoneNumber-'+a)).removeAttr('readonly');
              }
              for(var b=0;b<businessUnit.emails.length;b++){
                  angular.element(document.querySelector('#emailType-'+b)).removeAttr('readonly');
                  angular.element(document.querySelector('#email-'+b)).removeAttr('readonly');
              }
              for(var c=0;c<businessUnit.addresses.length;c++){
                  angular.element(document.querySelector('#addressLine-'+c)).removeAttr('readonly');
                  angular.element(document.querySelector('#city-'+c)).removeAttr('readonly');
                  angular.element(document.querySelector('#state-'+c)).removeAttr('readonly');
                  angular.element(document.querySelector('#country-'+c)).removeAttr('readonly');
                  angular.element(document.querySelector('#pinCode-'+c)).removeAttr('readonly');

              }
          }
        };
        $scope.generateStockReport=function(){
            var report= new Reports();
            /*report.reportType='Stock';*/
            report.reportPeriod=this.reportPeriod;
            report.reportFormat=this.reportFormat;
            report.reportType=this.reportType;
            report.
            /*  report.reportType='Stock';*/
            report.$save(function(response) {
                $scope.hide();
                /*if(response){
                    $scope.cancel();

                }*/

            });
        };

        $scope.hide = function () {
            $mdDialog.hide();
        };
        $scope.cancel = function () {
            this.businessUnit.addresses=[];
            this.businessUnit.emails=[];
            this.businessUnit.phones=[];
            this.businessUnit={};
            $scope.bunitToAddTo='';

            $mdDialog.cancel();
        };
        $scope.IsVisible = true;
        $scope.ShowHide = function () {
            //If DIV is visible it will be hidden and vice versa.

            $scope.IsVisible = $scope.company.settings.paymentOptions.installments.enabled;
        };

        $scope.IsLoc = true;
        $scope.ShowLoc = function () {
            //If DIV is visible it will be hidden and vice versa.
            $scope.IsLoc = $scope.company.settings.paymentOptions.lineOfCredit.enabled;
        };

        $scope.getCompanyPaymentTerms=function () {
            PaymentTerms.findCompanyPaymentTerms($scope.user.company).then(function (results) {
                $scope.paymentterms = results.data;
            }, function (errPayment) {
                $scope.errPayment = errPayment.message;
            });
        };
        $scope.getCompanyPaymentModes=function () {
            PaymentTerms.findCompanyPaymentModes($scope.user.company).then(function (results) {
                $scope.paymentmodes = results.data;
            }, function (errPayment) {
                $scope.errPayment = errPayment.message;
            });
        };
        $scope.setCompanyEdit=function () {
            $scope.myTabIndex=3;
            $scope.editCompany=true;
            $scope.findOne(false);
        };
        $scope.addInstallments = function (installmentArray) {
            if (installmentArray) {
                installmentArray.push({
                    description: '',
                    days: 0,
                    percentage: 40
                });
            }
        };
        $scope.isChecked = function (item, list) {
            return list.indexOf(item) > -1;
        };
        $scope.selectPaymentTerms = function (item, list) {
            if(!$scope.company)
                $scope.company={};
            if(!$scope.company.settings)
                $scope.company.settings={};
            if(!$scope.company.settings.paymentterms)
                $scope.company.settings.paymentterms=[];
            var idx = list.indexOf(item);
            if (idx > -1) {
                list.splice(idx, 1);
            }
            else {
                list.push(item);
            }
        };
        $scope.newPaymentTerm = function () {
            $scope.isNew=true;
            $scope.paymentTerm={};
            $scope.paymentTerm.installments=[{
                description: '',
                days: 0,
                percentage: 40
            }];
        };
        $scope.cancelNewPaymentTerm = function () {
            $scope.isNew=false;
            $scope.paymentTerm={};
        };
        $scope.createNewPaymentTerm = function () {
            if($scope.paymentTerm){
                CompanyService.createPaymentTerm($scope.paymentTerm).then(function (responce) {
                     var paymentTerm=responce.data;
                    $scope.paymentterms.push({name:paymentTerm.name,_id:paymentTerm._id});
                    $scope.isNew=false;
                    $scope.paymentTerm=null;
                },function (err) {
                    $scope.displayErrorToastMessage(err.data.message);
                });
            }
        };

    }
]);/*
 app.controller('CompanyProductController',[]);
 app.controller('CompanyEditProductController',[]);*/
/*============= Temperory business unit page directive ====================*/
app.directive('addBusinessUnit', function () {
    return {
        restrict: 'E',
        templateUrl: 'modules/companies/views/add-business-unit.client.viewnew.html'
    };
});
app.directive('addBusinessUser', function () {
    return {
        restrict: 'E',
        templateUrl: 'modules/companies/views/add-business-user.client.viewnew.html'
    };
});
app.directive('editBusinessUser', function(){
    return{
        restrict: 'E',
        templateUrl: 'modules/companies/views/edit-business-user.client.viewnew.html'
    };
});
app.directive('editBusinessUnit', function(){
    return{
        restrict: 'E',
        templateUrl: 'modules/companies/views/edit-business-unit.client.viewnew.html'
    };
});
app.directive('addAddresses', function(){
    return{
        restrict: 'E',
        scope:{
            addresses: '=addresses',
            addresstypes: '=addresstypes',
            addAddress:'&'
        },
        templateUrl: 'modules/users/views/settings/common.addresses.client.view.html'
    };
});
/*============= End of temporary business unit page directives ===============*/
