'use strict';

angular.module('users').controller('OTPController', ['$scope', '$stateParams', '$http', '$location', 'Authentication',
    function ($scope, $stateParams, $http, $location, Authentication) {
        $scope.authentication = Authentication;
    $scope.resendOtp=true;
    $scope.resendOtpUser=false;
        //If user is signed in then redirect back home
        if ($scope.authentication.token) $location.path('/');

        // Submit forgotten password account id
        $scope.submitEnterOTP = function () {
            $scope.success = $scope.error = null;


            $http.post('/user/otp/' + $stateParams.statusToken, $scope.credentials).then(function (response) {
                // Show user success message and clear form
                $scope.credentials = null;
                $scope.success = response.data.message;

            }, function (response) {
                // Show user error message and clear form
                $scope.credentials = null;
                $scope.success=null;
                $scope.error = response.data.message;
            });
        };

        $scope.panelChange=function(resendotpuser){
            if($location.path().startsWith('/resendotp')){
                $scope.resendOtpUser=true;
                $scope.resendOtp=false;

            }
            if($location.path().startsWith('/enterotp')){
                $scope.resendOtp=true;
                $scope.resendOtpUser=false;
            }

           /* if(resendotpuser==='resenduser'){
                $scope.resendOtp=false;
                $scope.resendOtpuser=true;
            }else{
                $scope.resendOtp=true;
                $scope.resendOtpuser=false;
            }*/
        };
        // Submit forgotten password account id
        $scope.resendOTP = function () {
            $scope.success = $scope.error = null;


            $http.post('/user/resendotp/' + $stateParams.statusToken, {}).then(function (response) {
                // Show user success message and clear form
                $scope.credentials = null;
                $scope.error = null;
                $scope.success = response.data.message;
                $scope.panelChange('resendotpuser');

            }, function (response) {
                // Show user error message and clear form
                $scope.credentials = null;
                $scope.success=null;
                $scope.error = response.data.message;
            });
        };

        $scope.sendOtp = function () {
        if( $scope.success) {
            $http.post('/user/otpuser/', $scope.credentials).then(function (response) {
                // Show user success message and clear form
                $scope.credentials = null;
                $scope.success = response.data.message;
                $scope.error=null;
                /* $scope.panelChange();*/
            }, function (response) {
                // Show user error message and clear form
                $scope.credentials = null;
                $scope.success=null;
                $scope.error = response.data.message;
            });

        } else {
            $scope.error=null;
            $scope.success=null;
            $http.post('/user/resendotpuser/', $scope.credentials).then(function (response) {
                // Show user success message and clear form
                $scope.credentials = null;
                $scope.success = response.data.message;
                $scope.panelChange();
                $scope.email = response.data.email;
            }, function (response) {
                // Show user error message and clear form
                $scope.credentials = null;
                $scope.success=null;
                $scope.error = response.data.message;
            });
        }
        };
    }
]);
