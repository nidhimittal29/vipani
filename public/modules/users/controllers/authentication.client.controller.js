'use strict';
var app= angular.module('users');
app.controller('AuthenticationController', ['$scope', '$stateParams', 'localStorageService', '$http', '$location', 'Authentication','deviceDetector', 'uuid','businessCategories','$mdDialog', '$mdMedia','alert','LoginService',
    function ($scope, $stateParams, localStorageService, $http, $location, Authentication, deviceDetector, uuid,businessCategories,$mdDialog,$mdMedia,alert,LoginService) {
        $scope.authentication = Authentication;
        $scope.passwordcredentials={username:null,forgotPasswordOtp:null,otpVerified:false,otpGenerated:false,changePassword:false,newPassword:null,verifyPassword:null};
        var query_parameters = $location.hash;
        var currenturl=$location.protocol() + '://'+ $location.host() +':'+  $location.port();

        $scope.register=false;
        $scope.selectedSegments = '';
        $scope.$watch('segments', function(items){
            if(items) {
                $scope.selectedCount = items.filter(function (item) {
                    return item.selected;
                }).length;
            }
        }, true
        );
        $scope.$watch('mainSubCategories', function(items){
                if(items) {
                    var count=0;
                    angular.forEach(items, function (item) {
                        if(item.children.length>0){
                            count += item.children.filter(function (child) {
                                return child.isChecked;
                            }).length;
                        }
                        else{
                            if(item.isChecked){
                                count++;
                            }
                        }
                    });
                    $scope.selectedCount = count;
                }
            }, true
        );
        $scope.getSegmentCount=function () {
            $scope.selectedSegments=this.segment;
            $scope.selectedCount=this.segment &&  this.segment._id?1:0;
        };
        $scope.categories = [];
        $scope.path = $location.path();
        $scope.isMob = $mdMedia('xs');
        $scope.selectedPanel=0;
        if($scope.authentication && $scope.authentication.token!== null && $scope.authentication.user && $scope.authentication.user.length!==0) {
            $location.path('/');
        }

        $scope.getSelectedPanel=function () {
            return $scope.selectedPanel;
        };
        $scope.setSelectedPanelId=function (selectedPanel) {
            $scope.selectedPanel=selectedPanel;
        };

        // If user is signed in then redirect back home
        /*if ($scope.authentication.token) $location.path('/');*/
        $scope.init = function () {
            $scope.businessCategories = [{value: 'Seller', text: 'Seller',selected : false}, {value: 'Buyer', text: 'Buyer',selected : false}, {value: 'Mediator', text: 'Mediator'}];
            $scope.category=[];
            $scope.category.seller = 'false';
            $scope.category.buyer = 'false';
            $scope.category.mediator = 'false';
            $scope.acceptTerms=true;
            $scope.credentials={};
            /*$scope.findMainCategories();*/
            $scope.isOptionsRequired();$scope.panelChange('');
        };
        $scope.reset=function () {
            $scope.passwordcredentials={username:null,forgotPasswordOtp:null,otpVerified:false,otpGenerated:false,changePassword:false,newPassword:null,verifyPassword:null};
        };

        $scope.askForPasswordReset = function() {
            $scope.success = $scope.error = null;

            $http.post('/auth/forgot', $scope.passwordcredentials).then(function (response) {
                // Show user success message and clear form
                $scope.passwordcredentials = null;
                $scope.success = response.data.message;
                $scope.verifyPasswordOTP=true;
                $scope.sendPasswordOTP=true;

            }, function (response) {
                // Show user error message and clear form
                $scope.passwordcredentials = null;

                $scope.error = response.data.message;
                $scope.userstatus = response.data.userstatus;
            });
        };

        $scope.showPasswordOTP=function ($username) {
            $scope.changenumber=false;
            var numbers = new RegExp(/^[0-9]+$/);
            if(numbers.test($scope.passwordcredentials.username)) {
                $scope.changenumber=true;
            }else{
                $scope.changenumber=false;
            }
        };

        $scope.forgotPassword=function (isValid) {
            if(!isValid){
                alert({title:'Error',content:'please fill out all fields'});
            }else {
                $scope.success = $scope.error = null;

                $http.post('/user/otpuserpassword', $scope.passwordcredentials).then(function (response) {

                    $scope.success = response.data.message;
                    $scope.showFpwd = true;
                    if (response.data.verifiedOtp) {
                        $scope.passwordcredentials.otpVerified = true;
                        $scope.passwordcredentials.otpGenerated = false;
                        $scope.passwordcredentials.changePassword = false;
                    }
                    if (response.data.otpGenerated) {
                        $scope.passwordcredentials.otpVerified = false;
                        $scope.passwordcredentials.changePassword = false;
                        $scope.passwordcredentials.otpGenerated = true;
                    }
                    if (response.data.changedPassword) {
                        $scope.passwordcredentials.changePassword = true;
                        $scope.passwordcredentials.otpVerified = false;
                        $scope.passwordcredentials.otpGenerated = false;
                        $scope.credentials = {username: $scope.passwordcredentials.username, password:  $scope.passwordcredentials.newPassword};

                        $scope.passwordcredentials = {
                            username: null,
                            forgotPasswordOtp: null,
                            otpVerified: false,
                            otpGenerated: false,
                            changePassword: false,
                            newPassword: null,
                            verifyPassword: null
                        };
                        $scope.signin(true);
                        $scope.showSignin = true;
                        $scope.showFpwd = false;
                    }
                    // Attach user profile
                    Authentication.token = response.data.token;

                    // And redirect to the index page
                    /*$location.path('/password/reset/success');*/
                }, function (response) {
                    $scope.error = response.data.message;
                });
            }
        };
        // Change user password
        $scope.resetUserPassword = function() {
            $scope.success = $scope.error = null;

            $http.post('/auth/reset/' + $stateParams.token, $scope.passwordDetails).then(function (response) {
                // If successful show success message and clear form
                $scope.passwordDetails = null;

                // Attach user profile
                Authentication.token = response.data.token;

                // And redirect to the index page
                $location.path('/password/reset/success');
            }, function (response) {
                $scope.error = response.data.message;
            });
        };
        $scope.isOptionsRequired=function () {
            $scope.invalidselection=!(($scope.category.seller && $scope.category.seller ===true)|| ($scope.category.buyer && $scope.category.buyer ===true) || ($scope.category.mediator && $scope.category.mediator===true));
        };
        $scope.changeSelection=function(){
            $scope.businessTypesSelection=businessCategories.changeSelection(this.category);
        };
        $scope.otherSegment=function () {
            $scope.otherSegments=$scope.segments.filter(function (t) { return t.isSpecific===true; })[0];
            $scope.otherSegments.selected=!$scope.otherSegments.selected;
        };
        $scope.addEmail = function (emailsArray) {
            emailsArray.push({
                email: '',
                emailType: 'Work',
                primary: false
            });
        };
        $scope.addAddress = function (addressesArray) {
            addressesArray.push({
                addressLine: '',
                city: '',
                state: '',
                country: '',
                pinCode:'',
                addressType: 'Billing',
                primary: false
            });
        };
        $scope.addPhone = function (phonesArray) {
            /*	var emptyArray={};
             if (!phonesArray) {
             phonesArray=emptyArray;
             }*/
            phonesArray.push({
                phoneNumber: '',
                phoneType: 'Mobile',
                primary: false
            });


        };
        $scope.userCategories = 'Seller';
        $scope.signup = function() {
            $http.post('/auth/signup', $scope.registerUser).then(function (response) {
                // If successful we assign the response to the global user model
                $scope.authentication.token = response.data.token;
                $http.post('/users/me', {token: $scope.authentication.token})
                    .then(function (response) {

                        localStorageService.set('nvipanilogindata', {
                            token: $scope.authentication.token,
                            user: response.data
                        });
                        $scope.authentication.user = response.data;

                        $location.path('/');
                    }, function (response) {
                        localStorageService.remove('nvipanilogindata');
                        $scope.error = response.data.message;

                    });
            }, function (response) {
                $scope.error = response.data.message;
            });
        };
        /*	$scope.dashboardData = function () {
                if ($scope.authentication && $scope.authentication.user && $scope.authentication.token) {
                    $http.get('/users/myhome')
                        .then(function (rs) {
                            var response=rs.data;
                            $scope.notifications = response.notifications;
                            $scope.orders = response.orders;
                            $scope.offers = response.offers;
                            $scope.todos = response.todos;

                        },function (response) {
                        localStorageService.remove('nvipanilogindata');
                        Authentication.token = null;
                        Authentication.user = null;
                        Authentication.deviceid = null;
                        $location.path('/');
                        $scope.authentication.token = null;
                        $scope.authentication.user = null;
                        $scope.authentication.deviceid = null;
                        $scope.error = response.data.message;
                    });
                }
            };
    */
        $scope.clearNotifications=function (type,notificationId) {
            if ($scope.authentication && $scope.authentication.user && $scope.authentication.token) {
                var parms={};
                if(type){
                    parms={type:type};
                }
                if(notificationId){
                    parms.notificationId=notificationId;
                }
                $http.get('notificationsclear',{
                    params:parms
                })
                    .then(function (rs) {
                        $scope.notifications = rs.data;
                    });
            }
        };

        $scope.reset = function(form) {
            $scope.error = null;
            if (form) {
                form.$setPristine();
                //form.$setUntouched();
            }
        };
        $scope.getSegmentCategories=function (categories) {
            var selectedCategoriresArray=[];
            categories.filter(function (childCategory){
                selectedCategoriresArray.push({category:childCategory.category._id.toString(),enabled:childCategory.enabled});
            });
            return selectedCategoriresArray;
        };
        $scope.selectedCategory=function (categories,selcategoires) {
            categories.filter(function (cat) {
                if(cat.isChecked===true)
                    selcategoires.push({category:cat._id});
                else if(cat.children && cat.children.length>0) {
                    $scope.selectedCategory(cat.children,selcategoires);
                }
            });
            return selcategoires;
        };
        function getSelectedSegments(registrationCategory,segments,selectedSegments) {
            var name=registrationCategory.name;

            switch(name){
                case 'Manufacturer': selectedSegments = [{
                    segment: selectedSegments._id,
                    categories: $scope.getSegmentCategories(selectedSegments.categories)
                }];break;
                case 'Distributor': selectedSegments =[];
                    segments.filter(function (segment) {
                        if( segment.selected === true )
                            if(segment.isSpecific) {
                                $scope.selectedCat=[];
                                selectedSegments.push({segment: segment._id, categories: $scope.selectedCategory($scope.mainSubCategories,$scope.selectedCat)});

                            }else {
                                selectedSegments.push({segment: segment._id, categories:$scope.getSegmentCategories(segment.categories)});
                            }
                    });break;
                default:selectedSegments =[];
                    $scope.selectedCat=[];
                    var selectedCategories=$scope.selectedCategory($scope.mainSubCategories,$scope.selectedCat);
                    if(selectedCategories.length>0) {
                        segments.filter(function (segment) {
                            /* if( segment.selected === true )*/
                            if (segment.isSpecific) {
                                $scope.selectedCat = [];
                                selectedSegments.push({
                                    segment: segment._id,
                                    categories: selectedCategories
                                });

                            }
                        });
                    } break;
            }
            return selectedSegments;

        }
        $scope.askRegistration = function(panel,isValid) {
            if(!isValid){
                alert({title:'Error',content:'please fill out all fields'});
            }else {
                $scope.success = $scope.registererror = null;
                $scope.userregister = [];
                $scope.password = this.password;
                $scope.registeruser = {
                    username: this.username ? this.username : $scope.username
                };

                $scope.array = [];
                $scope.setSelectedPanelId(panel);
                if (panel === 0) {
                    $scope.registeruser.issendotp = true;
                    $scope.registeruser.password = this.password;
                } else if (panel === 1) {
                    $scope.registeruser.isverifyotp = true;
                    $scope.registeruser.otp = this.otp.toString();
                } else if(panel===2){
                    $scope.setSelectedPanelId(3);
                    $scope.registererror='';
                    $scope.selectedCount = 0;
                } else if (panel === 3) {
                    $scope.registeruser.ispassword = true;
                    if(this.companyName){
                        $scope.registeruser.companyName=this.companyName;
                    }
                    if(this.registrationCategory){
                        $scope.registeruser.registrationCategory=this.registrationCategory._id;
                    }
                    $scope.registeruser.selectedSegments=getSelectedSegments(this.registrationCategory,this.segments,this.selectedSegments);

                }
                if(panel !==2) {
                    $http.post('/user/sendpresignupotp', $scope.registeruser).then(function (response) {
                        // Show user success message and clear form
                        var user = response.data.user;
                        if (user) {
                            $scope.username = user.username;
                            $scope.isEmail = (user.username === user.email);

                            if (user && user.status === 'Verified') {
                                $scope.businessTypesSelection = response.data.registrationCategories;

                                $scope.mainSubCategories = response.data.categories;

                                $scope.segments = response.data.segments;
                                $scope.setSelectedPanelId(2);
                            } else if (user && user.status === 'Register Request') {
                                $scope.setSelectedPanelId(1);
                            } else if (user && user.status === 'Registered') {
                                $scope.credentials = {username: user.username, password: $scope.password};
                                $scope.signin(true);
                            }
                        }

                        $scope.success = response.data.message;
                        $scope.statusToken = response.data.statusToken;


                    }, function (response) {
                        // Show user error message and clear form
                        if (response.data) {
                            var user = response.data.user;
                            if (user && user.status === 'Verified') {
                                $scope.isEmail = (user.username === user.email);
                                $scope.businessTypesSelection = response.data.registrationCategories;
                                $scope.segments = response.data.segments;
                                $scope.mainSubCategories = response.data.categories;
                                $scope.setSelectedPanelId(2);
                            }
                            $scope.registererror = response.data.message;
                        } else {
                            $scope.registererror = 'No internet connection';
                        }

                    });
                }
            }

        };
        $scope.changeRegisterCategory=function () {
            $scope.rcategory=this.rcategory;
            var registerCategories= $scope.businessTypesSelection.filter(function (category) {
                return category._id=== $scope.rcategory;
            });
            if(registerCategories.length>0) {
                $scope.registrationCategory = registerCategories[0];
                return $scope.registrationCategory._id;
            }else{
                return -1;
            }

        };
        $scope.setText=function (panel) {
            if(panel===0){
                return 'Generate OTP';
            }else if(panel===1){
                return 'Verify Otp';
            }else if(panel===2){
                return 'Signup';
            }/*else if(panel===3){
                return 'Segments';
            }*/
        };
        $scope.askPreRegistration = function() {
            $scope.success = $scope.registererror = null;
            $scope.userregister=[];
            if (this.useMobileAsUserName) {
                this.username = this.mobile;
            } else {
                this.username = this.email;
            }
            $scope.registeruser = {
                username: this.username,
                password:this.password,
                ConfirmPassword:this.ConfirmPassword,
                useMobileAsUserName: this.useMobileAsUserName,
                firstName:this.firstName,
                lastName:this.lastName,
                companyName:this.companyName,
                businessType:this.businessType,
                categorySeller:this.category.seller && this.category.seller===true?true:false,
                categoryBuyer:this.category.buyer&& this.category.buyer===true?true:false,
                categoryMediator:this.category.mediator && this.category.mediator===true?true:false,
                email: this.email,
                mobile:this.mobile,
                acceptTerms:this.acceptTerms
            };

            // Redirect after save

            /*	if ($scope.username) {
             $scope.userregister.username = $scope.username;
             }
             if ($scope.password) {
             $scope.userregister.password = $scope.password;
             }*/

            $http.post('/user/presignup', $scope.registeruser).then(function (response) {
                // Show user success message and clear form
                /*$scope.register.registerusername = null;*/
                $scope.success = response.data.message;
                $scope.statusToken = response.data.statusToken;

            }, function (response) {
                // Show user error message and clear form
                $scope.registererror = response.data.message;
            });
        };
        $scope.create=function(){
            if(this.registrationCategory===-1)
                $http.post('/user/presignup', $scope.registeruser).then(function (response) {

                });
        };
        $scope.signin = function(isValid) {

            if(!isValid){
                alert({title:'Error',content:'please fill out all fields'});
            }
            else {
                LoginService.login($scope);
            }

        };
        $scope.panelChange = function (changeStr) {
            $scope.context = [
                {title:'Home',link:''}
            ];
            $scope.showSignin = false;
            $scope.showFpwd = false;
            $scope.error = null;
            if (changeStr === 'fpwd') {
                $scope.showFpwd = true;
                if($scope.credentials && $scope.credentials.username){
                    $scope.passwordcredentials.username=$scope.credentials.username;
                    $scope.showPasswordOTP($scope.passwordcredentials.username);
                }
            } else {
                $scope.showSignin = true;
            }
        };
    }
]);
app.directive('nvType', function ($compile) {
    return {
        restrict: 'A',
        scope: {},
        link: function (scope, element, attrs) {
            scope.showpassword = false;
            var htmlText = '<span ng-model="showpassword" class="fa fa-eye pass-icon" password-check="' + attrs.nvType + '"></span>';
            element.attr('id', attrs.nvType);
            element.attr('type', scope.showpassword ? 'password' : 'password');
            var linkFn = $compile(htmlText);
            element.parent().append(linkFn(scope));
        }
    };
});
app.directive('passwordCheck', [
    function() {
        return {
            restrict: 'A',
            link: function(scope, element,attrs) {
                element.on('click', function () {
                    scope.showpassword=!scope.showpassword;

                    var id='#'+attrs.passwordCheck;
                    if(scope.showpassword){
                        element.attr('class','fa pass-icon fa-eye-slash');
                    }else{
                        element.attr('class','fa pass-icon fa-eye');
                    }
                    if (scope.showpassword) {
                        angular.element(document.querySelector(id)).attr('type', 'text');
                    } else {
                        angular.element(document.querySelector(id)).attr('type', 'password');
                    }
                });
            }
        };
    }
]);
app.directive('registerForm', function () {
    return {
        restrict: 'E',
        templateUrl: 'modules/users/views/authentication/registration-form.client.view.html'
    };
});
app.directive('nvcRegistrationFormPanel2', function ($mdMedia) {
    return{
        restrict: 'E',
        templateUrl: function (element, attributes) {
            if($mdMedia('xs')){
                return 'modules/users/views/authentication/registration-form-panel-2-xs.client.view.html';
            }else{
                return 'modules/users/views/authentication/registration-form-panel-2-gtxs.client.view.html';
            }
        }
    }
});
