'use strict';

// Authentication service for user variables
var app=angular.module('users');
app.factory('Authentication', [
	function() {
		var _this = this;

		_this._data = {
			user: window.user
		};

		return _this._data;
	}
]);

app.factory('businessCategories', function() {
    var categories = {
        init: function () {
            return [{value: 'Seller', text: 'Seller',selected : false}, {value: 'Buyer', text: 'Buyer',selected : false}, {value: 'Mediator', text: 'Mediator'}];
        },
        changeSelection:function (types) {/*
        	var businessTypesSelection=(types && (types.seller===true || types.buyer===true  || types.mediator===true)) ?[{value: 'Trader', text: 'Trader'},{value: 'Importer', text: 'Importer'},
                {value: 'Exporter', text: 'Exporter'}]:[{value:'',text:'Select Business Type'}];*/

            var businessTypesSelection = [{value:'',text:'Select Business Type'},{value: 'Wholesaler/Retailer', text: 'Wholesaler/Retailer'}, {value: 'Stockist/Distributor', text: 'Stockist/Distributor'}, {value: 'Manufacturer', text: 'Manufacturer'}, {
                value: 'Importer',text: 'Importer'
            },{value: 'Exporter', text: 'Exporter'}
            ];
            if(types.seller && types.seller===true) {
                if(!types.buyer || types.buyer==='false') {

                    businessTypesSelection.unshift({value: 'Farmer', text: 'Farmer'}, {
                        value: 'Wholesaler',
                        text: 'Wholesaler'
                    }, {
                        value: 'Distributor', text: 'Distributor'
                    }, {value: 'Manufacturer', text: 'Manufacturer'});
                }else{
                    businessTypesSelection.unshift({value: 'Farmer', text: 'Farmer'}, {
                        value: 'Wholesaler',
                        text: 'Wholesaler'
                    }, {
                        value: 'Distributor', text: 'Distributor'
                    }, {value: 'Retailer', text: 'Retailer'},{value: 'Manufacturer', text: 'Manufacturer'});
                }
            }else if(types.buyer && types.buyer===true){
				/*if(!types.mediator || types.mediator==='false') {*/
                businessTypesSelection.unshift({
                    value: 'Wholesaler',
                    text: 'Wholesaler'
                }, {
                    value: 'Distributor', text: 'Distributor'
                }, {value: 'Retailer', text: 'Retailer'});
				/*}else {
				 $scope.businessTypesSelection.unshift({value: 'Retailer', text: 'Retailer'});
				 }*/
            }else if(types.mediator && types.mediator===true){
                businessTypesSelection=businessTypesSelection;
            }/*else{
                businessTypesSelection = [{value: 'Trader', text: 'Trader'}, {value: 'Farmer', text: 'Farmer'}, {value: 'Wholesaler', text: 'Wholesaler'}, {
                    value: 'Distributor',text: 'Distributor'
                },{value: 'Retailer', text: 'Retailer'}, {value: 'Manufacturer', text: 'Manufacturer'},
                    {value: 'Importer', text: 'Importer'},
                    {value: 'Exporter', text: 'Exporter'},
                ];
            }*/
            return businessTypesSelection;

        }
    };
    return categories;
});
