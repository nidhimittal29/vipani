'use strict';

// Users service used for communicating with the users REST endpoint
var app=angular.module('users');
app.factory('Users', ['$resource',
    function($resource) {
        return $resource('users', {}, {
            update: {
                method: 'PUT'
            }
        });
    }
]);
app.directive('nvRegulationExp', function ($compile) {
    return {
        restrict: 'A',
        require: 'ngModel',

        link: function (scope, element, attrs, ctrl) {
            ctrl.$parsers.unshift(function (viewValue) {
                var exp ;
                var error;
                if(attrs.nvRegulationExp) {
                    if (attrs.nvRegulationExp === 'gstin') {
                        exp = /^[0-9]{2,2}[a-zA-Z]{5,5}[0-9]{4,4}[a-zA-Z]{1,1}[1-9a-zA-Z]{1,1}[a-zA-Z]{1,1}[0-9a-zA-Z]{1,1}$/;
                        error = 'Invalid GSTIN Number format. Ex: 29AAOFK0414G1ZE';
                    }else if (attrs.nvRegulationExp === 'mail') {
                        exp = /^(?:\d{10,11}|([_a-zA-Z0-9]+(\.[_a-zA-Z0-9]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,4})))$/;
                        error = 'Invalid Email/Mobile Format';
                    }else if (attrs.nvRegulationExp === 'mail1') {
                        exp =/^[_a-zA-Z0-9]+(\.[_a-zA-Z0-9]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,4})$/;
                        error = 'Invalid Input..';
                    }else if (attrs.nvRegulationExp === 'phonenumber') {
                        exp = /^[0-9]{10,11}$/;
                        error = 'Invalid Input..';
                    }else if (attrs.nvRegulationExp === 'otp') {
                        exp = /^[0-9]{6}$/;
                        error = 'Invalid Input..';
                    }else if (attrs.nvRegulationExp === 'num') {
                        exp = /^[0-9]{1,128}$/;
                        error = 'Invalid Input..';
                    } else if (attrs.nvRegulationExp === 'float') {
                        exp =  /^(?=.*\d)\d*(\.\d+)?$/;
                        if(parseFloat(viewValue) && parseFloat(viewValue).toString()===viewValue)
                            viewValue=parseFloat(viewValue).toFixed('2');
                        error = 'Invalid input Ex:25 or 0.5 etc.,';
                    }else if (attrs.nvRegulationExp === 'key') {
                        exp = /^[a-zA-Z0-9. _-]{1,128}$/;
                        error = 'Enter Proper key..';
                    }else if (attrs.nvRegulationExp === 'value') {
                        exp = /^[a-zA-Z0-9. _-]{1,128}$/;
                        error = 'Enter Proper Value..';
                    }else if (attrs.nvRegulationExp === 'margin') {
                        exp = /^0*(?=.*[0-9])\d{0,2}(?:\.\d+|100)?$/;
                        if(parseFloat(viewValue) && parseFloat(viewValue).toString()===viewValue)
                            viewValue=parseFloat(viewValue).toFixed('2');
                        error = 'Enter Proper input..';
                    }else if(attrs.nvRegulationExp === 'pan'){
                        exp = /^[A-Za-z]{5}[0-9]{4}[a-zA-Z]{1}$/;
                        error = 'Invalid PAN Number format. Ex:AAAPL1234C';
                    }else if(attrs.nvRegulationExp === 'ifsc'){
                        exp = /^[A-Za-z]{4}0[0-9]{6}$/;
                        error = 'Invalid IFSC Code';
                    }

                }
                var elements=element.parent().find('.fa-info-circle');
                if (exp && exp.test(viewValue) || (!attrs.required && !viewValue)) { // it is valid
                    if(elements.length !==0){
                        elements.remove();
                    }

                    ctrl.$setValidity('gstinNumber', true);
                    ctrl.$viewValue =viewValue;
                    // ctrl.$render();
                    return viewValue;
                } else { // it is invalid, return undefined (no model update)
                    ctrl.$setValidity('gstinNumber', false);
                    if(elements.length ===0) {
                        var findelement=element.parent().find('.md-char-counter');
                        var htmlText;
                        if(findelement.length >0)
                            htmlText = '<i class="fa fa-info-circle pass-icons nv-red" tabindex="1" data="'+error+'"></i>';
                        else htmlText = '<i class="fa fa-info-circle pass-icon nv-red" tabindex="1" data="'+error+'"></i>';
                        var linkFn = $compile(htmlText);

                        if(findelement.length >0) {
                            element.parent().find('.md-errors-spacer').append(linkFn(scope));
                        }else{
                            element.parent().append(linkFn(scope));
                        }

                    }
                    return undefined;
                }
            });

        }
    };
});

app.directive('uploader', function($q,httpPostFactory,csvFileParse,categoryService,fileImport,contactsCSVFileParse,contactsQuery,inventoryProduct) {
    var slice = Array.prototype.slice;
    return {
        restrict: 'A',
        require: '?ngModel',
        uploadpathkey:'@',
        link: function(scope, element, attrs, ngModel) {
            if (!ngModel) return;

            ngModel.$render = function() {};
            function importContacts(done) {
                var results={errors:[],contacts:{}};


                if (!scope.contactResult.errorType) {
                    results.contacts.headers = scope.contactResult.header;
                    results.contacts.dataValues = scope.contactResult.data;
                    contactsQuery.preImportValidateContacts({contacts: results.contacts}, function (response) {
                        results.errors = response.data.errors;
                        results.contacts = response.data.contacts;
                        done(results);

                    });
                } else {
                    //if header is in error
                    if (scope.contactResult.error.length === 1) {
                        results.errors.push({errorType:'Header' ,message:scope.contactResult.error[0]});
                        done(results);

                    }
                    else {
                        done(results.errors.push({errorType:'Header',message:'There were errors. ' + scope.contactResult.error.length + ' were found'}));
                    }
                }
            }
            function getSubCategoryJSON(categoryXLMapping) {
                var jsonToQuery=[];
                for (var cName in categoryXLMapping) {
                    var mvalue = categoryXLMapping[cName];
                    for (var scName in mvalue) {
                        jsonToQuery.push({
                            mainCategoryName: cName,
                            subCategoryName: scName
                        });
                    }
                }
                return jsonToQuery;
            }
            function importProducts(done){
                //{header: [], data: [], error: [], status: true};
                //get the category mapped csv data
                //if header itself is not in error then
                var result = {};
                if(!scope.result){
                    result.errors.push('Data format is not as expected');
                    done(result);
                }else if (scope.result && !scope.result.errorType) {
                    var categoryXLMapping = scope.result.map;
                    result.header = scope.result.header;
                    result.errors = [];
                    result.matchedCategories = [];
                    result.errorsummary='';

                    var csvCategories = [];
                    //from the map get only the subcategories
                    for (var categoryName in categoryXLMapping) {
                        var mappedValue = categoryXLMapping[categoryName];
                        for (var subCategoryName in mappedValue) {
                            csvCategories.push({
                                category: categoryName,
                                subCategory: subCategoryName
                            });
                        }
                    }
                    var categoryNotExisting = [];
                    //create the json object to post for findSubCategoriesByName()

                    //var map = scope.result;
                    var jsonToQuery = getSubCategoryJSON(categoryXLMapping);

                    //make a call to findSubcategories
                    categoryService.findSubCategoriesByName(jsonToQuery).then(function (response) {
                        var mainCategories = response.data;
                        var indicesAlreadyAdded = [];
                        if (mainCategories) {
                            for (var j = 0; j < csvCategories.length; j++) {
                                var matchedCategory = {};
                                var csvCategory = csvCategories[j];
                                var matched = false;
                                for (var i = 0; i < mainCategories.length; i++) {

                                    var mainCategory = mainCategories[i].parent.name;
                                    var masterCategory = mainCategories[i].name;
                                    if (csvCategory.category === mainCategory && csvCategory.subCategory === masterCategory) {
                                        //if not already added then add to matchedCategories
                                        if (indicesAlreadyAdded.indexOf(masterCategory) === -1 && mainCategories[i]._id) {

                                            result.matchedCategories.push({
                                                subCategoryId: mainCategories[i]._id,
                                                mainCategoryId: mainCategories[i].parent._id,
                                                mainCategoryName: mainCategories[i].parent.name,
                                                name: mainCategories[i].name,
                                                value: categoryXLMapping[mainCategory][masterCategory]

                                            });

                                            indicesAlreadyAdded.push(i);
                                            matched = true;
                                        }
                                    }
                                    var objExists = result.errors.filter(function (obj) {
                                        return obj.lineData === mainCategories[i].error;
                                    });
                                    if (mainCategories[i].error && !objExists) {
                                        result.errors.push({lineData: mainCategories[i].error});
                                    }
                                }
                            }
                        }
                        done(result);
                    }, function (err) {
                        result.errors.push(err.data);
                        done(result);
                    });
                    scope.result.fileName = scope.filename;
                    scope.result.importType = scope.importType;

                }
                else {
                    //if header is in error
                    scope.errors = scope.result.error;
                    if (scope.errors.length === 1) {
                        scope.errorsummary = scope.errors[0].rowError;
                        delete scope.errors;
                    }
                    else {
                        scope.errorsummary = 'There were errors. ' + scope.result.error.length + ' were found';
                    }
                    done(result);

                    //header message already has summary.
                }
            }
            function mappedData(scope,content,attr){
                if(!content ||(content && content.length<1) || !attr){
                    scope.result={errors :[]};
                    scope.result.errors.push({lineData:'Data format is not as expected'});
                }else {
                    if (attr.uploadpathkey === 'product') {
                        csvFileParse.csvToJSON(content, scope.importType, function (data) {
                            scope.result = data;

                            importProducts(function(results){

                                scope.results=results;
                                if(scope.result.error.length>0){
                                    scope.results.errors = scope.results.errors.concat(scope.result.error);
                                }
                                if (results.errors.length > 0) {
                                    scope.errorsummary = results.errors.length + ' errors were found';
                                }



                            });
                        });

                    }
                    else if (attr.uploadpathkey === 'contacts') {
                        scope.contactImports = {};
                        scope.contactHeaderError = {};
                        contactsCSVFileParse.csvToJSON(content, scope.importType, function (data) {
                            scope.contactResult = data;
                            if(scope.contactResult.errorType===1){
                                scope.errorsummary = scope.contactResult.error[0].rowError;
                                scope.contactHeaderError = scope.contactResult.error[0];
                            }
                            else {
                                importContacts(function (results) {
                                    scope.contactImports = {
                                        errors: results.errors,
                                        contacts: results.contacts
                                    };
                                });
                            }
                        });


                    }
                }
            }
            function isWorksheetVisible(sheetName,workbook){
                for(var i=0;i<workbook.Workbook.Sheets.length;i++){
                    if(workbook.Workbook.Sheets[i].name===sheetName && (workbook.Workbook.Sheets[i].state==='visible' || workbook.Workbook.Sheets[i].Hidden===0)){
                        return true;
                    }
                }
                return false;
            }

            function getContentRows(sheet){
                var contentRows = [];
                var range = XLSX.utils.decode_range(sheet['!ref']);
                var C, R = range.s.r+1; /* start in the first row */
                /* walk every column in the range */
                for(R = range.s.r+1;R<=range.e.r;++R) {
                    var row='';
                    for (C = range.s.c; C <= range.e.c; ++C) {
                        var cell = sheet[XLSX.utils.encode_cell({c: C, r: R})];
                        /* find the cell in the first row */

                        var hdr = ''; // <-- replace with your desired default
                        if (cell && cell.t) hdr = XLSX.utils.format_cell(cell).trim();
                        row+='"'+hdr+'",';

                    }
                    contentRows.push(row);
                }
                return contentRows;
            }
            function getHeaderRow(sheet) {
                var headers = '';
                var range = XLSX.utils.decode_range(sheet['!ref']);
                var C, R = range.s.r; /* start in the first row */
                /* walk every column in the range */
                for(C = range.s.c; C <= range.e.c; ++C) {
                    var cell = sheet[XLSX.utils.encode_cell({c:C, r:R})];/* find the cell in the first row */

                    var hdr = ''; // <-- replace with your desired default
                    if(cell && cell.t) hdr = XLSX.utils.format_cell(cell).trim();

                    headers+=''+hdr+',';
                }
                return headers;
            }

            function uploadCSVImages(targetDir){
                var categoriesData = {};
                categoriesData.csvValues=[];
                //flatten out header and values.
                scope.results.matchedCategories.forEach(function(item){
                    categoriesData.header=item.header;
                    item.value.forEach(function(item1)
                    {
                        categoriesData.csvValues.push(item1);
                    });
                });
                var header = categoriesData.header;
                var lineData = categoriesData.csvValues;
                var inventoryData = scope.inventory;
                var inventoriesToUpload = [];
                //get the inventoryid of the object
                inventoryData.forEach(function(inventoryObj,index){
                    var invToUpload = {};
                    invToUpload.inventoryId = inventoryObj.inventory._id;
                    invToUpload.srcLine= inventoryObj.srcLine;
                    //get the matching linedata
                    //get all images to upload
                    //upload individual images to server and update progress

                    lineData.forEach(function (csvLine) {
                        var brandImages = [];
                        var itemMasterImages = [];
                        var userItemMasterImages = [];
                        if(csvLine.srcLine===inventoryObj.srcLine) {
                            var imageFields = header.filter(function (field) {
                                return field.indexOf('Image') >= 0;
                            });
                            var brandImageIndex=0,itemMasterIndex=0,userItemMasterIndex=0;
                            imageFields.forEach(function (imageField,index) {
                                if (imageField.startsWith('ProductBrandImage')) {
                                    switch (brandImageIndex){
                                        case 0: invToUpload.productBrandImageURL1 = targetDir+'/'+csvLine.lineData[header.indexOf(imageField)];break;
                                        case 1: invToUpload.productBrandImageURL2 = targetDir+'/'+csvLine.lineData[header.indexOf(imageField)];break;
                                        case 2: invToUpload.productBrandImageURL3 = targetDir+'/'+csvLine.lineData[header.indexOf(imageField)];break;
                                        case 3: invToUpload.productBrandImageURL4 = targetDir+'/'+csvLine.lineData[header.indexOf(imageField)];break;
                                        default:break;
                                    }
                                    brandImageIndex++;
                                }
                                else if (imageField.startsWith('ItemMasterImage')) {
                                    switch (itemMasterIndex){
                                        case 0: invToUpload.itemMasterImageURL1 = targetDir+'/'+csvLine.lineData[header.indexOf(imageField)];break;
                                        case 1: invToUpload.itemMasterImageURL2 = targetDir+'/'+csvLine.lineData[header.indexOf(imageField)];break;
                                        case 2: invToUpload.itemMasterImageURL3 = targetDir+'/'+csvLine.lineData[header.indexOf(imageField)];break;
                                        case 3: invToUpload.itemMasterImageURL4 = targetDir+'/'+csvLine.lineData[header.indexOf(imageField)];break;
                                        default:break;
                                    }
                                    itemMasterIndex++;

                                }
                                else if (imageField.startsWith('UserItemMasterImage')) {
                                    switch (userItemMasterIndex){
                                        case 0: invToUpload.inventoryImageURL1 = targetDir+'/'+csvLine.lineData[header.indexOf(imageField)];break;
                                        case 1: invToUpload.inventoryImageURL2 = targetDir+'/'+csvLine.lineData[header.indexOf(imageField)];break;
                                        case 2: invToUpload.inventoryImageURL3 = targetDir+'/'+csvLine.lineData[header.indexOf(imageField)];break;
                                        case 3: invToUpload.inventoryImageURL4 = targetDir+'/'+csvLine.lineData[header.indexOf(imageField)];break;
                                        default:break;
                                    }
                                    userItemMasterIndex++;
                                }
                            });
                            inventoriesToUpload.push(invToUpload);
                            //inventoryObj has inventory and srcLine

                        }
                    });


                });

                inventoryProduct.massUpdateImages({inventoriesToUpload:inventoriesToUpload}).then(function(res){
                    if(res.data.errors && res.data.errors.length>0){
                        scope.imageUploadErrors = res.data.errors;
                    }
                    else{
                        scope.imageUploadMessage=res.data.message;
                    }
                });
            }

            element.bind('change', function(e) {
                var element = e.target;

                if(attrs.fileimport) {
                    if (e.target.files[0].type.startsWith('application/vnd' )) {
                        if (element.files[0].name.endsWith('xlsx')) {
                            var reader = new FileReader();
                            reader.onload = function (onLoadEvent) {
                                scope.$apply(function () {
                                    var data = onLoadEvent.target.result;
                                    var workbook = XLSX.read(data,{type:'binary'});
                                    workbook.SheetNames.forEach(function(sheetName) {
                                        var worksheet = workbook.Sheets[sheetName];

                                        if(isWorksheetVisible(sheetName,workbook)) {
                                            var headers = getHeaderRow(worksheet);
                                            // console.log(headers)
                                            var contentRows = getContentRows(worksheet);
                                            var lines = [];
                                            lines.push(headers);
                                            lines = lines.concat(contentRows);
                                            scope.content = {
                                                lines: lines,
                                                separator: ',',
                                                header: true

                                            };
                                        }
                                    });
                                    //convert the csv file to json.
                                    mappedData(scope, scope.content,attrs);

                                });

                            };
                            reader.readAsBinaryString(e.target.files[0]);
                        }
                    }
                    if (e.target.files[0].type === 'text/csv' || element.files[0].name.endsWith('csv')) {
                        if (scope.results && scope.results.matchedCategories) {
                            scope.results.matchedCategories = null;
                        }
                        if (scope.matchedProducts) {
                            scope.matchedProducts = null;
                        }
                        scope.filename = e.target.files[0].name;
                        scope.fileObject = element.files[0];
                        var csvReader = new FileReader();
                        csvReader.onload = function (onLoadEvent) {
                            scope.$apply(function () {
                                var csv = onLoadEvent.target.result.replace(/\r\n|\r/g, '\n');
                                var content = {
                                    lines: csv.split(new RegExp('\n(?![^"]*"(?:(?:[^"]*"){2})*[^"]*$)')),
                                    header: true,
                                    separator: ','
                                };
                                scope.categoryMatching = {};

                                //convert the csv file to json.
                                mappedData(scope,content,attrs);
                                /*else {
                                    //unsupported
                                }
*/

                            });

                        };
                        csvReader.readAsText(e.target.files[0]);
                    }
                }
                else{
                    var formData = new FormData();
                    formData.append('file', element.files[0]);
                    httpPostFactory(attrs.fileimport ? 'fileimport' : 'fileupload', attrs.uploadpathkey, attrs.fileimport, formData, function (callback) {
                        // recieve image name to use in a ng-src
                        console.log(callback);
                        if(element.files[0].name.indexOf('.zip')>=0){
                            if(callback.status) {
                                var targetDir = callback.targetDir;
                                uploadCSVImages(targetDir);
                            }
                        }

                        ngModel.$setViewValue(callback.imageURI ? callback.imageURI : null);
                    });
                }


            }); //change


        } //link
    }; //return
});

app.factory('httpPostFactory', function ($http) {
    return function (file,key,fileimport, data, callback) {
        var headers = {'Content-Type': undefined};
        if (key) {
            headers.uploadpath = key;
        }
        if (fileimport) {
            headers.fileimport = fileimport;
        }
        if (headers.fileimport) {
            var req = {
                method: 'POST',
                url: file,
                headers: {'uploadpath': key},
                data: { importdata: fileimport}
            };
            $http(req).then(function (response) {
                callback(response.data);
            }, function (response) {
                console.log(response.data);
            });
        } else
            $http({
                url: file,
                method: 'POST',
                data: data,
                headers: {'Content-Type': undefined, 'uploadpath': key}
            }).then(function (response) {
                callback(response.data);
            }, function (response) {
                console.log(response.data);
            });

    };
});


