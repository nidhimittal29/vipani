'use strict';

// Config HTTP Error Handling
angular.module('users',[]).config(['$httpProvider',
	function($httpProvider) {
		// Set the httpProvider "not authorized" interceptor
		$httpProvider.interceptors.push(['$q','$location', 'Authentication','$window','localStorageService',
			function($q, $location, Authentication,$window,localStorageService) {
				return {
					responseError: function(rejection) {
						switch (rejection.status) {
							case 401:
								// Deauthenticate the global user
								Authentication.token = null;

								// Redirect to signin page
								$location.path('signin');
								break;
							case 403:
								// Add unauthorized behaviour
								break;
						}

						return $q.reject(rejection);
					},
					'request': function(config) {
						/*if ($window.localStorage.token) {
							config.headers.token = $window.localStorage.token;
							/!*$http.post('/users/me', {token:$window.localStorage.token}).success(function(response) {
								$window.user=response;
							});*!/

						}else {
							config.headers.token = Authentication.token;
						}*/

						var authData = localStorageService.get('nvipanilogindata');
						if (authData) {
							config.headers.token = authData.token;
							config.headers.deviceid = authData.deviceid;
							Authentication.token=authData.token;
							Authentication.deviceid = authData.deviceid;
							Authentication.user=authData.user;
							if(authData.companyBusinessUnits) {
                                authData.companyBusinessUnits.businessUnits.businessUnits = (authData.companyBusinessUnits.businessUnits.businessUnits && authData.companyBusinessUnits.businessUnits.businessUnits.length>0) ? authData.companyBusinessUnits.businessUnits.businessUnits : '';
                                authData.companyBusinessUnits.defaultBusinessUnitId = authData.companyBusinessUnits.defaultBusinessUnitId ? authData.companyBusinessUnits.defaultBusinessUnitId : '';
                            }
                            Authentication.companyBusinessUnits=authData.companyBusinessUnits?authData.companyBusinessUnits:'';

						}else{
							config.headers.token = Authentication.token;
							config.headers.deviceid = Authentication.deviceid;

						}
						return config;
					}
				};
			}
		]);
	}
]);
