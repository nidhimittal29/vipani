'use strict';

// Setting up route
angular.module('users').config(['$stateProvider',
	function($stateProvider) {
		// Users state routing
		$stateProvider.
		state('myprofile', {
			url: '/settings/myprofile',
			templateUrl: 'modules/users/views/settings/edit-mybusiness.client.viewnew.html'
		}).
        state('mycompanyprofile', {
            url: '/settings/mycompanyprofile',
            templateUrl: 'modules/users/views/settings/edit-profile.client.view.html'
        }).
        state('signin', {
            url: '/signin',
            templateUrl: 'modules/users/views/authentication/signupc.client.viewnew2.html'
        }).
		state('editmybusiness', {
			url: '/settings/editmybusiness',
		templateUrl: 'modules/users/views/settings/settings.client.viewnew.html'
		}).
		state('password', {
			url: '/settings/password',
			templateUrl: 'modules/users/views/settings/change-password.client.viewnew.html'
		}).
		state('accounts', {
			url: '/settings/accounts',
			templateUrl: 'modules/users/views/settings/social-accounts.client.view.html'
		}).
		state('signup', {
			url: '/signup/:registerToken',
			templateUrl: 'modules/users/views/authentication/signup.client.viewnew2.html',
//
		}).
		state('register-success', {
			url: '/register/valid',
			templateUrl: 'modules/users/views/authentication/success.user.html'
		}).
		/*state('register-failure', {
			url: '/register/invalid',
			templateUrl: 'modules/users/views/authentication/failure.user.html'
		}).*/
		state('forgot', {
			url: '/password/forgot',
			templateUrl: 'modules/users/views/password/forgot-password.client.viewnew.html'
		}).
		state('reset-invalid', {
			url: '/password/reset/invalid',
			templateUrl: 'modules/users/views/password/reset-password-invalid.client.view.html'
		}).
		state('reset-success', {
			url: '/password/reset/success',
			templateUrl: 'modules/users/views/password/reset-password-success.client.view.html'
		}).
		state('reset', {
			url: '/password/reset/:token',
			templateUrl: 'modules/users/views/password/reset-password.client.viewnew.html'
		});
	}
]);
