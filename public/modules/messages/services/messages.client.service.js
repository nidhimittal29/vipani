'use strict';

//Messages service used to communicate Messages REST endpoints
angular.module('messages').factory('Messages', ['$resource',
    function ($resource) {
        return $resource('messages/:messageId', {
            messageId: '@_id'
        }, {
            update: {
                method: 'PUT'
            }
        });
    }
]).factory('OrderMessages', ['$resource',
    function ($resource) {
        return $resource('orderMessages/:orderId', {
            orderId: '@_id'
        }, {
            update: {
                method: 'PUT'
            }
        });
    }
]).factory('OfferMessages', ['$resource',
    function ($resource) {
        return $resource('offerMessages/:offerId', {
            offerId: '@_id'
        }, {
            update: {
                method: 'PUT'
            }
        });
    }
]);
