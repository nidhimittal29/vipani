'use strict';

// Configuring the Articles module
angular.module('reports',[]).run(['Menus',
    function(Menus) {
        // Set top bar menu items
        Menus.addMenuItem('topbar', 'Reports', 'reports', 'reports',false,false, null, 5);
		/*Menus.addSubMenuItem('topbar', 'products', 'List Products', 'products');
		 Menus.addSubMenuItem('topbar', 'products', 'New Product', 'products/create');*/
    }
]);
