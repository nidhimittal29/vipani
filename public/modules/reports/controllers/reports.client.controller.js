'use strict';

// Reports controller
angular.module('reports').controller('ReportsController', ['$scope','$http', '$stateParams', '$location', 'Authentication', 'Reports','$mdMedia','$mdDialog',
    function ($scope, $http,$stateParams, $location, Authentication, Reports,$mdMedia,$mdDialog) {
        $scope.authentication = Authentication;
        $scope.report= new Reports();
        // Create new Report
        $scope.create = function () {
            // Create new Report object
            var report = new Reports({
                name: this.name
            });

            // Redirect after save
            report.$save(function (response) {
                $location.path('reports/' + response._id);

                // Clear form fields
                $scope.name = '';
            }, function (errorResponse) {
                $scope.error = errorResponse.data.message;
            });
        };

        // Remove existing Report
        $scope.remove = function (report) {
            if (report) {
                report.$remove();

                for (var i in $scope.reports) {
                    if ($scope.reports [i] === report) {
                        $scope.reports.splice(i, 1);
                    }
                }
            } else {
                $scope.report.$remove(function () {
                    $location.path('reports');
                });
            }
        };

        // Update existing Report
        $scope.update = function () {
            var report = $scope.report;

            report.$update(function () {
                $location.path('reports/' + report._id);
            }, function (errorResponse) {
                $scope.error = errorResponse.data.message;
            });
        };


        $scope.getFiles=function () {
            $scope.showFiles=true;
            $http.get('files').then(function (res) {
               $scope.files=res.data;
            });
        };
        $scope.getFile=function (file) {
             $scope.fileType=file.type;
            $http.get('files'+'?path='+file.name,{ responseType: 'arraybuffer' }).then(function (res) {
                /*var reportMimeType = 'application/pdf';
                if (response.reports[0].format === 'XLS') {
                    reportMimeType = 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet';
                }*/
                var file = new Blob([res.data], {type: $scope.fileType});
                var fileURL = URL.createObjectURL(file);
                window.open(fileURL);
            });
        };
        $scope.getViewFile=function (option) {

            $http.get('fileexport'+'?type='+option.toLowerCase(),{ responseType: 'arraybuffer' }).then(function (res) {
                /*var reportMimeType = 'application/pdf';
                if (response.reports[0].format === 'XLS') {
                    reportMimeType = 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet';
                }*/
                var file = new Blob([res.data], {type: 'application/vnd.ms-excel'});
                var fileURL = URL.createObjectURL(file);
                window.open(fileURL);
            });
        };
        $scope.generateCompanyReport=function(){
            var report = new Reports();
            /*report.reportType='Stock';*/
            report.reportPeriod=this.reportPeriod;
            report.reportFormat=this.reportFormat;
            report.reportType=this.reportType;
            /*  report.reportType='Stock';*/
            report.$save(function(response) {
                $scope.hide();
                /*if(response){
                 $scope.cancel();

                 }*/

            });
        };

        $scope.showReportDialog = function (ev) {

            var useFullScreen = ($mdMedia('sm') || $mdMedia('xs'));
           /* $scope.showReport=true;
            $scope.from = new Date();
            $scope.to = new Date();
            $scope.tType = 'Sell';
            $scope.transactionType=[{value: 'Sell', text: 'Transaction Date'}, {value: 'Buy', text: 'Transaction Period'}];
            $scope.reportFormats=['PDF', 'XLS'];
            $scope.reportPeriods = ['Daily', 'Monthly', 'Quarterly', 'HalfYearly', 'Yearly', 'Duration'];
       */    /* $scope.reportTypes=['Stock', 'Sales', 'Purchases','SalesAndPurchase'];*/
            //$scope.reportType=['Stock'];
            $scope.reportType='Stock';
            $scope.reportPeriod='Daily';
            $scope.reportFormat='PDF';
            $mdDialog.show({
                template: ('<md-dialog aria-label=" Dialog">' +
                '<md-toolbar><div class="md-toolbar-tools"><h2>Reports</h2><span flex></span>' +
                '<md-button class="md-icon-button" ng-click="cancel()">' +
                '<md-icon class="fa fa-minus" aria-label="Close dialog"></md-icon></md-button></div></md-toolbar>' +
                '<md-dialog-content class="md-dialog-content">' +
                '<md-datepicker  ng-model="from" md-placeholder="Start date">'+
                '</md-datepicker>'+
                '<md-datepicker  ng-model="to" md-placeholder="End date">'+
                '</md-datepicker>'+
                '<md-select aria-label="Report Format" ng-model="reportFormat">' +
                '<md-option ng-repeat="rt in reportFormats">{{rt}}</md-option>' +
                '</md-select>' +
                '</md-dialog-content>' +
                '<md-dialog-actions>' +
                '<md-button class="md-raised md-primary" data-ng-click="generateSalesReport()">Submit</md-button>' +
                '<md-button class="md-raised md-accent" ng-click="cancel()">Cancel</md-button></md-dialog-actions>' +
                '</md-dialog>'),
                targetEvent: ev,
                scope: $scope,
                clickOutsideToClose: true,
                fullscreen: useFullScreen,
                preserveScope: true
            });

        };
        // Find a list of Reports
        $scope.find = function () {
            $scope.showReport=true;
            $scope.from = new Date();
            $scope.to = new Date();
            $scope.tType = 'Sell';
            $scope.transactionType=[{value: 'Sell', text: 'Transaction Date'}, {value: 'Buy', text: 'Transaction Period'}];
            $scope.reportFormats=['PDF', 'XLS'];
            $scope.reportPeriods = ['Daily', 'Monthly', 'Quarterly', 'HalfYearly', 'Yearly', 'Duration'];
            $scope.reportTypes=['Stock', 'Sales', 'Purchases', 'SalesAndPurchase', 'FastAndSlowMoving', 'Profit', 'ItemWiseProfit', 'Expenses', 'PaymentPending', 'SalesTax'];
            $scope.fileUploads=['Contacts', 'Inventory'];
            $scope.fileUpload='Contacts';
            //$scope.reportType=['Stock'];
            $scope.reportType='Stock';
            $scope.reportPeriod='Daily';
            $scope.reportFormat='PDF';
            $scope.reports = Reports.query();
        };

        $scope.cancel = function () {
            $mdDialog.cancel();
        };
        $scope.generateSalesReport=function(){
            var report= new Reports();
            /*report.reportType='Stock';*/
            report.reportPeriod=this.reportPeriod;
            report.reportFormat=this.reportFormat;
            report.reportType=this.reportType;
            report.startDate=this.from;
            report.endDate=this.to;
            /*  report.reportType='Stock';*/
            report.$save(function(response) {
                $http.get('reportPath/' + response._id, {responseType: 'arraybuffer'}).then(function (res) {
                    var reportMimeType= 'application/pdf';
                    if( response.reports[0].format==='XLS'){
                        reportMimeType='application/vnd.openxmlformats-officedocument.spreadsheetml.sheet';
                    }
                var file = new Blob([res.data], {type: reportMimeType});
                var fileURL = URL.createObjectURL(file);
                window.open(fileURL);
            }, function(errorResponse) {
                $scope.error = errorResponse.data.message;
            });
            });
        };
        // Find existing Report
        $scope.findOne = function () {
            $scope.report = Reports.get({
                reportId: $stateParams.reportId
            });
        };
    }
]);
