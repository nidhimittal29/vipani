'use strict';

//Offers service used to communicate Offers REST endpoints
var app=angular.module('offers');
app.factory('Offers', ['$resource',
    function($resource) {
        return $resource('offers/:offerId', { offerId: '@_id'
        }, {
            update: {
                method: 'PUT'
            }
        });
    }
]);
app.factory('DatePicker',[ '$stateParams', '$location', 'Authentication',
    'Offers', 'Contacts', 'Products',
    function($scope, $stateParams, $location, Authentication, Offers, Contacts, Products){
        //Constructer
        var DatePicker = function(){
            //Sets default date as today
            this.dt = new Date();

            //Sets the minimum selectable date as today
            this.minDate =  new Date();

            //Boolean value for opening datePicker popup
            this.opened = false;

            //Sets format shown in the textbox input
            this.format = 'dd-MMMM-yyyy';
        };

        //Method for popup open
        DatePicker.prototype.open = function($event) {
            $event.preventDefault();
            $event.stopPropagation();
            console.log('opened');
            this.opened = true;
        };

        /**
         * Static method, assigned to class
         * Instance ('this') is not available in static context
         */
        DatePicker.build = function (data) {
            return new DatePicker(
                data.dt,
                data.minDate,
                data.opened,
                data.format
            );
        };

        /**
         * Return the constructor function
         */
        return DatePicker;
    }]);

/*
 Contacts Service used for AutoComplete
 */
app.factory('ContactsForOffers',['$stateParams', '$location', 'Authentication',
    'Offers', 'Contacts', 'Products', function($scope, $stateParams, $location, Authentication, Offers, Contacts, Products){
        //AutoComplete methods for Contacts
        $scope.getContacts = function(){
            $scope.contacts = Contacts.query();
        };
        $scope.getContacts();

        $scope.contactsOffer = [];

        $scope.contactSelected = undefined;

        $scope.addContact = function(event){
            console.log(event);
            if(event.keyCode === 13 || event === undefined){
                $scope.contactsOffer.push($scope.contactSelected);
                var index = $scope.contacts.indexOf($scope.contactSelected);
                $scope.contacts.splice(index, 1);
                $scope.contactSelected = '';
            }
        };

        $scope.removeContact = function(contactSel){
            var index = $scope.contactsOffer.indexOf(contactSel);
            if(index >= 0){
                $scope.contactsOffer.splice(index, 1);
                $scope.contacts.push(contactSel);
            }

        };
    }]);

app.factory('contactsGroup',['$http',function($http){
    return {
        contacts:function () {
            return $http.get('contacts');
        },
        groups:function () {
            return $http.get('groups');
        }

    };
}]);
app.factory('inventoryProducts',['$http',function($http){
    return {
        inventoryProducts:function () {
            return $http.get('inventories');
        }

    };
}]);
app.factory('offerData',function () {
    function getProductClassification($scope) {
        return (($scope.subCategory1) || ($scope.subCategory2) || ($scope.productBrand) || ($scope.unitOfMeasure));
    }
    function queryCriteria($scope) {
        var criteria;
        if(getProductClassification($scope)){
            criteria={};
            if($scope.subCategory1){
                if($scope.subCategory1!==1) {
                    if ($scope.subCategory2) {
                        if ($scope.subCategory2 !== -1) {
                            criteria.subCategory2 = $scope.subCategory2;
                            if ($scope.productBrand && $scope.productBrand !== -1) {
                                criteria.productBrand = $scope.productBrand;
                            }
                            if ($scope.unitOfMeasure && $scope.unitOfMeasure !== -1) {
                                criteria.unitOfMeasure = $scope.unitOfMeasure;
                            }
                        } else {
                            criteria.subCategory1 = $scope.subCategory1;
                        }
                    }else {
                        criteria.subCategory1 = $scope.subCategory1;
                    }
                }
            }else if($scope.subCategory2){
                if($scope.subCategory2!==-1){
                    criteria.subCategory2=$scope.subCategory2;
                    if($scope.productBrand && $scope.productBrand!==-1){
                        criteria.productBrand=$scope.productBrand;
                    }
                    if($scope.unitOfMeasure && $scope.unitOfMeasure!==-1){
                        criteria.unitOfMeasure=$scope.unitOfMeasure;
                    }
                }

            }else if($scope.productBrand){
                if($scope.productBrand!==-1) {
                    criteria.productBrand = $scope.productBrand;

                    if ($scope.unitOfMeasure && $scope.unitOfMeasure !== -1) {
                        criteria.unitOfMeasure = $scope.unitOfMeasure;
                    }
                }
            }else if($scope.unitOfMeasure){
                if($scope.unitOfMeasure!==-1){
                    criteria.unitOfMeasure=$scope.unitOfMeasure;
                }

            }

        }
        return criteria;
    }
    function updateProducts($scope){
        if(getProductClassification($scope)){
            $scope.offer.inventoryItemsClassification.classificationType = 'Criteria';
            $scope.offer.inventoryItemsClassification.classificationCriteria={};
            var criteria=queryCriteria($scope);
            if(criteria){
                $scope.offer.inventoryItemsClassification.classificationCriteria.criteriaType='All';
                if (criteria.subCategory1 || criteria.subCategory2) {
                    if(criteria.subCategory2){
                      delete criteria.subCategory1;
                        criteria.category=criteria.subCategory2;
                        delete criteria.subCategory2;

                    }else{
                        criteria.category=criteria.subCategory1;
                        delete criteria.subCategory1;
                    }
                    $scope.offer.inventoryItemsClassification.classificationCriteria.criteriaType = 'Custom';

                }
                if (criteria.productBrand) {
                    $scope.offer.inventoryItemsClassification.classificationCriteria.criteriaType = 'Custom';

                }
                if (criteria.unitOfMeasure) {
                    $scope.offer.inventoryItemsClassification.classificationCriteria.criteriaType = 'Custom';

                }
                if($scope.offer.inventoryItemsClassification.classificationCriteria.criteriaType === 'Custom'){
                    $scope.offer.inventoryItemsClassification.classificationCriteria.criteria=[];
                    $scope.offer.inventoryItemsClassification.classificationCriteria.criteria.push(criteria);
                }

            }
        }
        return $scope.offer;

    }
    return {
        setProducts:function ($scope) {
            updateProducts($scope);
        },
        setContacts:function (offer,contacts) {

        },
        getCriteriaQuery:function ($scope) {
            return queryCriteria($scope);
        },
        getOfferProductsClassification:function ($scope) {
            return getProductClassification($scope);
        }
    };

});

app.factory('offersApi',['$http','Offers','$mdToast',function($http,Offers,$mdToast){
    function filterSelectedOffers($scope) {
        var selectedOffers={'offers':[],'businessUnit':$scope.authentication.companyBusinessUnits.defaultBusinessUnitId};
        angular.forEach($scope.offers,function (offer) {
            if(offer.selected){
                selectedOffers.offers.push({_id:offer._id});
            }
        });
        return selectedOffers;
    }

    function getUpdatedOffers($scope,updatedOffers) {
        for (var i=0;i<$scope.offers.length ;i++){
            for (var j=0;j<updatedOffers.length ;j++){
                if($scope.offers[i]._id===updatedOffers[j]._id) {
                    $scope.offers[i] = updatedOffers[j];
                }
            }
        }
    }
    function getOfferToContacts($scope){
        $scope.tocontacts = {contacts: [], groups: []};
        $scope.togroups = [];
        $scope.toallcontacts = [];
        for (var j = 0; j < $scope.selectedContacts.length; j++) {
            var singlecontact = $scope.selectedContacts[j];
            if (singlecontact.displayName) {
                $scope.tocontacts.contacts.push({'contact': singlecontact._id});
                $scope.toallcontacts.push({'contact': singlecontact._id, 'nVipaniUser': singlecontact.nVipaniUser});
                $scope.togroups.push(singlecontact);
            }
            else if (singlecontact.name) {
                $scope.tocontacts.groups.push({'group': singlecontact._id});
                for (var k = 0; k < $scope.contactsVsGroup.length; k++) {
                    var groupContacts = angular.fromJson($scope.contactsVsGroup[k]);
                    if (groupContacts.groups && groupContacts.groups.indexOf(singlecontact._id) >= 0)
                        $scope.toallcontacts.push({'contact': groupContacts._id});
                    $scope.togroups.push(singlecontact);
                }

            }
        }

        return  {contacts: $scope.tocontacts.contacts, groups: $scope.tocontacts.groups};

    }
    function getSelectedProducts(products) {
        return products.filter(function (product) {
            return product.selected;
        });
    }
    return {
        offerCreateData:function ($scope) {
            $scope.selOffer = new Offers($scope.offer);
            $scope.selOffer.businessUnit=$scope.busUnitId;
            return $scope.selOffer.$save();
        },
        getContactUserOffer:function () {
            return $http.get('offerusercontacts');
        },
        getOrdersByOfferId:function (offerId) {
            return $http.get('offerOrders/'+offerId);
        },
        getContactCompanyOffer:function (companyId) {
            return $http.get('offerusercontacts/'+companyId);
        },
        getPublicOffersBySegments:function () {
            return $http.get('publicOffersBySegments');
        },
        getSegmentOffers:function (segmentId) {
            return $http.get('publicOffersBySegments/'+segmentId);
        },
        getOffersByBusinessUnit:function(index,pagination,selectedBusinessUnit){
            pagination.businessUnitId = selectedBusinessUnit;
            if(index===0) {
                return $http.get('offersByBusinessUnit', {params: pagination});
            }else{
                return $http.get('receivedOffers', {params: pagination});
            }
        },
        getOffers:function (isMyOffers,pagination) {
            if(isMyOffers) {
                return $http.get('offers', {params: pagination});
            }else{
                return $http.get('receivedOffers', {params: pagination});
            }
        },
        getInventoryCriteria:function($scope){
            var selCategories=$scope.selectedCategories;
            $scope.offer.inventoryItemsClassification.classificationCriteria.criteria=[];
            angular.forEach($scope.selectedCategories,function (category) {
                $scope.offer.inventoryItemsClassification.classificationCriteria.criteria.push({'category':category._id});
            });

            return $scope;
        },
        cloneOffer:function ($offerId) {
            return $http.get('cloneOffer/'+$offerId);
        },
        receivedOffers:function () {
            return $http.get('receivedOffers');
        },
        offersQuery:function () {
            return $http.get('offersQuery');
        },
        createOrder:function (offer,bunit) {
            return $http.put('createOrder/' + offer._id, {offer:offer,products:getSelectedProducts(offer.products),businessUnit:bunit});
        },
        batchOffers:function (index,scope) {
            var selectedOffer= scope.showView?{offers:[{_id:scope.selOffer._id}],toContacts:getOfferToContacts(scope)}:filterSelectedOffers(scope);
            if(selectedOffer.offers.length>0) {
                if (index === 0) {
                    return $http.post('/offersremove', selectedOffer).then(function (results) {
                        scope.offers = results.data.offer;
                        scope.total_count = results.data.total_count;
                        scope.getOffersSummary(0,scope.busUnitId);
                    }, function (err) {
                        scope.error = err.data.message;
                    });
                } else if (index === 1) {
                    $http.post('/offersinactive', selectedOffer).then(function (results) {
                        getUpdatedOffers(scope, results.data);
                    }, function (err) {
                        scope.err = err.data.message;
                    });
                } else if (index === 2) {
                    $http.post('/offersactive', selectedOffer).then(function (results) {
                        getUpdatedOffers(scope, results.data);
                    }, function (err) {
                        scope.err = err.data.message;
                    });
                } else if (index === 3) {
                    $http.post('/offersnotify', selectedOffer).then(function (results) {
                        getUpdatedOffers(scope, results.data);
                    }, function (err) {
                        scope.err = err.data.message;
                    });
                }
            }else{
                $mdToast.show(
                    $mdToast.simple()
                        .textContent('Please select at least one Offer')
                        .position('bottom right')
                        .theme('error-toast')
                        .hideDelay(3000)
                );
            }
        },
        offersSummary:function (index, businessUnitId) {
            if(index === 0){
                return $http.get('offerssourcesummary?businessUnitId='+businessUnitId);
            }else {
                return $http.get('offerstargetsummary?businessUnitId='+businessUnitId);
            }
        },
        offersFilterSummary:function (index,businessUnitId, paginationKeys,data) {
            if(index === 0){
                return $http.post('offerssourcesummary?businessUnitId='+businessUnitId,data,{params:paginationKeys});
            }else {
                return $http.post('offerstargetsummary?businessUnitId='+businessUnitId,data,{params:paginationKeys});
            }
        }

    };
}]);


app.directive('addOffer', function(){
    return {
        restrict:'E',
        templateUrl:'modules/offers/views/create-offer-checkout.client.viewnew.html'
    };
});

app.directive('addOfferHeader', function(){
    return {
        restrict:'E',
        templateUrl:'modules/offers/views/create-offer-header.client.viewnew.html'
    };
});

app.directive('addOfferDefinition', function(){
    return {
        restrict:'E',
        templateUrl:'modules/offers/views/create-offer-definition.client.viewnew.html'
    };
});
app.directive('addOfferContacts', function(){
    return {
        restrict:'E',
        templateUrl:'modules/offers/views/create-offer-contacts.client.viewnew.html'
    };
});
app.directive('addOfferProducts', function(){
    return {
        restrict:'E',
        templateUrl:'modules/offers/views/create-offer-products.client.viewnew.html'
    };
});
app.directive('viewOffer', function(){
    return {
        restrict:'E',
        templateUrl:'modules/offers/views/view-offer.client.viewnew.html'
    };
});
app.directive('offerPaymentTerms',function () {
    return {
        restrict:'E',
        templateUrl:'modules/offers/views/create-offer-checkout-payment-terms.client.viewnew.html'
    };
});
app.directive('addOfferStatusHistory', function(){
    return {
        restrict:'E',
        templateUrl:'modules/offers/views/create-offersstatus.client.view.html'
    };
});
app.directive('viewOfferList', function(){
    return{
        restrict: 'E',
        templateUrl: 'modules/offers/views/list-offers-table.client.view.html'
    };
});
app.directive('viewOfferContactList', function(){
    return{
        restrict: 'E',
        templateUrl: 'modules/offers/views/list-contact-offers-table.client.view.html'
    };
});
app.directive('viewOfferSegmentList', function(){
    return{
        restrict: 'E',
        templateUrl: 'modules/offers/views/list-segment-offers-table.client.view.html'
    };
});
app.directive('viewOfferCards', function () {
    return{
        restrict:'E',
        templateUrl: 'modules/offers/views/list-offers-cards.client.view.html'
    };
});
app.directive('offercarousel', function () {

    return {
        link: function (scope, element, attrs) {

            element.flexslider({
                animation: 'slide',
                controlNav: true,
                animationLoop: true,
                slideshow: true,
                itemWidth: 210,
                itemMargin: 5,
                asNavFor: '#slider'
            });
        }
    };


});


