'use strict';

//Setting up route
angular.module('offers').config(['$stateProvider',
	function($stateProvider) {
		// Offers state routing
		$stateProvider.
		state('listOffers', {
			url: '/offers',
			templateUrl: 'modules/offers/views/list-offers.client.viewnew.html'
		}).
		state('offercreate', {
			url: '/offercreate',
			templateUrl: 'modules/offers/views/list-offers.client.viewnew.html'
		}).
        state('createOffer', {
            url: '/createoffer',
            templateUrl: 'modules/offers/views/list-offers.client.viewnew.html'
        }).
		state('viewOffer', {
			url: '/offers/:offerId',
			templateUrl: 'modules/offers/views/list-offers.client.viewnew.html'
		}).
        state('viewOfferComment', {
            url: '/offers/:offerId/:messageId',
            templateUrl: 'modules/offers/views/list-offers.client.viewnew.html'
        }).
		state('viewOffers', {
			url: '/offersnotification/:offerId',
			templateUrl: 'modules/offers/views/list-category.client.view.html'
		}).
		state('editOffer', {
			url: '/offers/category/:categoryId',
			templateUrl: 'modules/offers/views/single-offer.client.view.html'
		});
	}
]);
