'use strict';

// Configuring the Articles module
angular.module('offers', []).run(['Menus',
	function(Menus) {
		// Set top bar menu items
		Menus.addMenuItem('topbar', 'Offers', 'offers', 'offers',false,false, null, 3);
		/*	Menus.addMenuItem('topbar', 'Offers', 'offers', 'dropdown', '/offers(/create)?');
	Menus.addSubMenuItem('topbar', 'offers', 'List Offers', 'offers');
		Menus.addSubMenuItem('topbar', 'offers', 'New Offer', 'offers/create');*/
	}
]);
//Xeditable method - sets theme
angular.module('offers').run(function(editableOptions) {
	editableOptions.theme = 'bs3';
});
