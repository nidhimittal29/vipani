'use strict';

var app = angular.module('offers');

/*
 Controller for the list Offers page
 */
app.controller('OffersController', ['$scope', '$stateParams', '$location', 'Authentication', 'Offers','OrderProducts','productDetail', 'inventoryProduct','inventoryProducts', 'Orders', '$timeout', '$compile', '$element', 'contactsQuery', 'Messages', 'OfferMessages', '$filter', '$q', 'DatePicker', 'verifyDelete', 'panelTrigger', '$mdToast', '$mdDialog', '$mdMedia','categoryService', 'productBrandService', 'offersApi','offerData','PaymentTerms','localStorageService','Contacts','CompanyService','criteriaChips','inventoryProductConversion',
    function ($scope, $stateParams, $location, Authentication, Offers,OrderProducts,productDetail, inventoryProduct,inventoryProducts, Orders, $timeout, $compile, $element, contactsQuery, Messages, OfferMessages, $filter, $q, DatePicker, verifyDelete, panelTrigger, $mdToast, $mdDialog, $mdMedia,categoryService,productBrandService,offersApi,offerData,PaymentTerms,localStorageService,Contact,CompanyService,criteriaChips,inventoryProductConversion) {
        // Remove existing Offer
        $scope.company={};
        $scope.authentication = Authentication;
        $scope.user=Authentication.user;
        $scope.busUnitId=$scope.authentication.companyBusinessUnits.defaultBusinessUnitId;
        $scope.getSelectedItem=function (item) {
            $scope.selectedItem=item;
        };
        $scope.getChangeItem=function () {
            $scope.selectedItem=null;
        };
        $scope.location=$location;
        $scope.Oldoffer=Offers;

        $scope.filterOffer = function (item) {
            var specifiedKey=Object.keys($scope.searchText);
            if($scope.searchText[$scope.queryBy] && $scope.searchText[$scope.queryBy].length>0) {
                var re = new RegExp($scope.escapeRegExp($scope.searchText[$scope.queryBy]), 'i');

                return !$scope.searchText[$scope.queryBy] || re.test(item[$scope.queryBy]);
            }
            else return item;
        };
        $scope.getMyOfferIndex=function (offerTabType) {
            return $scope.tabs[$scope.selectedIndex].type===offerTabType;
        };
        $scope.getCompanyBusinessUnits=function () {
            CompanyService.getCompanyBusinessUnits().then(function (response) {
                $scope.companyBusinessunits=response.data;
            },function (err) {
                $scope.error=err.data.message;
            });
        };
        $scope.getOffersSummary = function (index,businessUnitId) {
            offersApi.offersSummary(index,businessUnitId).then(function (responce) {
                $scope.offerfilters=responce.data;
            },function (err) {
                $scope.error=err.data;
            });
        };
        $scope.offersFilterSummary=function(filterKey,filterCount,pageno) {
            var paginationfield = {};
            $scope.isFilterSummary = true;
            if (!isNaN(pageno)) {
                paginationfield = {page: pageno, limit: $scope.itemsPerPage};
            }
            if (this.searchText && this.searchText.length > 0) {
                paginationfield.searchText = this.searchText;
            }
            if (filterKey) {
                $scope.filterKey = filterKey;
                $scope.data = {summaryFilters: {}};
                $scope.data.summaryFilters[$scope.filterKey] = true;
            }
            offersApi.offersFilterSummary($scope.selectedIndex,$scope.busUnitId, paginationfield, $scope.data).then(function (response) {
                $scope.offers=response.data[$scope.filterKey];
                if (filterCount)
                    $scope.total_count = filterCount;
            }, function (res) {
                $scope.error = res.data.message;
            });
        };
        $scope.getOffersFilterSummary=function(filterKey,filterCount,toggle){
            if(toggle){
                $scope.offersFilterSummary(filterKey,filterCount,1);
            }else{
                $scope.filterKey=null;
                // var isMyOffers=$scope.tabs[$scope.selectedIndex].type==='MyOffers';
                $scope.find($scope.selectedIndex,1);
            }
        };
        $scope.getOffersByBusinessUnit = function(selectedBusinessUnit,tabSelection,pagenumber,isTab){
            $scope.isFilterSummary=false;
            var paginationfield={};
            if(this.searchText && this.searchText.length>0){
                paginationfield.searchText=this.searchText;
            }
            if(!isNaN(pagenumber)){
                paginationfield={page:pagenumber,limit:$scope.itemsPerPage};
            }
            offersApi.getOffersByBusinessUnit($scope.selectedIndex,paginationfield,selectedBusinessUnit).then(function(offers){
                $scope.offers=offers.data.offer;
                $scope.total_count=offers.data.total_count;
                if(offers.data.summaryData){
                    $scope.offerfilters=offers.data.summaryData;
                }
                if(!isTab) {
                    if ($scope.location.$$url === '/offercreate' || $scope.location.$$url === '/createoffer') {
                        $scope.panelChange(null, ($scope.location.$$url === '/offercreate' ?'addnew':'add'), 0);
                    } else if ($stateParams.offerId !== undefined) {
                        $scope.panelChange($scope.selOffer, 'view', $scope.selectedIndex);
                    } else {
                        $scope.panelChange(null, 'viewAll', $scope.selectedIndex);
                    }
                }else{
                    $scope.location.path('offers');
                    $scope.panelChange(null, 'viewAll', $scope.selectedIndex);
                }
            },function (err) {
                $scope.error=err.data.message;
                if($scope.error==='Session Expired.'){
                    localStorageService.remove('nvipanilogindata');
                }
            });
        };
        /*==== function to get dynamic height =====*/
        $scope.getHeight = function(string){
            var height;
            if(string==='createOffer'){
                var cards = angular.element(document.querySelectorAll('#createOffer>md-card'));
                height = window.innerHeight - cards[0].offsetTop - cards[0].clientHeight - cards[2].clientHeight - cards[3].clientHeight;
            }
            return {'max-height': height.toString()+'px'};
        };
        $scope.changeSelOffer = function (offer) {
            $scope.selOffer = offer;
            $scope.findOne(offer._id, function (err) {
            });

        };
        $scope.cloneOffer = function () {
            var offer = this.offer ? this.offer:this.selOffer;
            offersApi.cloneOffer(offer._id).then(function (response) {
                $scope.offers.push(new Offers(response.data));
                $scope.selOffer = new Offers(response.data);
                $scope.offer = new Offers(response.data);
                $scope.getOffersSummary($scope.selectedIndex,$scope.busUnitId);
                $scope.panelChange($scope.selOffer, 'view', false);
                /*$scope.changeSelOffer(response);*/
                /*$scope.resetCall(true);*/
                /*$location.path('offers');*/
            }, function (response) {
                $scope.error = response.data.message;
            });
        };
        $scope.changeUrl=function(){
            $mdDialog.cancel();
            $scope.location.path('/inventories/selection');
        };
        $scope.getOfferProducts = function(){
            /*var products  = angular.copy($scope.Oldoffer.products);
            if(products){
                $scope.offerProducts = [];
                for(var i=0;i<products.length;i++){
                    if(products[i].selected){
                        $scope.offerProducts.push(products[i]);
                    }
                }
            }*/
            if(!$scope.Oldoffer  ||!$scope.Oldoffer.products ||$scope.Oldoffer.products.length===0){
                return 0;
            }else {
                return $scope.Oldoffer.products.filter(function (product) {
                    return product.selected;

                }).length;
            }
        };
        $scope.getOrdersByOffer=function (id) {
            offersApi.getOrdersByOfferId(id).then(function (orders) {
                $scope.offerOrders=orders.data;
            },function (errResponse) {
                $scope.error=errResponse.data.message;
            }) ;
        };
        function getSelectedLevel(){
            return $scope.selectedLevel;
        }
        function nextPrepareData() {
            if(getSelectedLevel()===0) {
                $scope.offer.name = $scope.offerName;
                $scope.offer.validTill = $scope.validTill?$scope.validTill:null;
                $scope.offer.validFrom = $scope.validFrom;
                $scope.offer.offerStatus = 'Opened';
                $scope.offer.offerType = $scope.offerType;

            }else if(getSelectedLevel()===1) {
                var isOfferCriteria=offerData.getOfferProductsClassification($scope);
                if (isOfferCriteria && !$scope.offerProducts) {
                    $scope.offer.inventoryItemsClassification={};
                    offerData.setProducts($scope);
                }else{
                    $scope.products = angular.copy($scope.selOffer.products);
                    $scope.offer.products = [];
                    if ($scope.products && $scope.products.length > 0) {
                        for (var i = 0; i < $scope.products.length; i++) {
                            if ($scope.products[i].selected || $scope.offerProducts) {
                                var margin=$scope.products[i].MOQ ?$scope.products[i].MOQ.margin:0;
                                $scope.offer.products.push({
                                    inventory: $scope.products[i]._id,
                                    margin: margin,
                                    MOQ: margin?$scope.products[i].MOQ.MOQ:1,
                                    availableDate: $scope.products[i].availableDate,
                                    sampleAvailable: $scope.products[i].sampleAvailable
                                });
                            }
                        }
                    }
                }
                $scope.criteriaChips.loadChipContacts($scope,false,false,false,$scope.selectedContact,contactsQuery);
            }else if(getSelectedLevel()===2) {
                $scope.offer.isPublic = $scope.publicOffer;
                $scope.offer.producer = $scope.authentication.user._id;
                $scope.tocontacts = {contacts: [], groups: []};
                $scope.togroups = [];
                $scope.toallcontacts = [];
                if($scope.isIntraCompany){
                    var bArray=[];
                    angular.forEach($scope.businessContacts,function (eachBusinessUnit) {
                        bArray.push({businessUnit:eachBusinessUnit._id});
                    });
                    $scope.offer.toContacts = {contacts:[], groups: [],businessUnits:bArray};
                }else {
                    for (var j = 0; j < $scope.selectedContacts.length; j++) {
                        var singlecontact = $scope.selectedContacts[j];
                        if (singlecontact.displayName) {
                            $scope.tocontacts.contacts.push({'contact': singlecontact._id});
                            $scope.toallcontacts.push({
                                'contact': singlecontact._id,
                                'nVipaniUser': singlecontact.nVipaniUser
                            });
                            $scope.togroups.push(singlecontact);
                        }
                        else if (singlecontact.name) {
                            $scope.tocontacts.groups.push({'group': singlecontact._id});
                            for (var k = 0; k < $scope.contactsVsGroup.length; k++) {
                                var groupContacts = angular.fromJson($scope.contactsVsGroup[k]);
                                if (groupContacts.groups && groupContacts.groups.indexOf(singlecontact._id) >= 0)
                                    $scope.toallcontacts.push({'contact': groupContacts._id});
                                $scope.togroups.push(singlecontact);
                            }

                        }
                    }
                    $scope.offer.toContacts = {contacts: $scope.tocontacts.contacts, groups: $scope.tocontacts.groups};
                }


            }else if(getSelectedLevel()===3) {
                $scope.offer.paymentTerms = [];
                $scope.offer.paymentModes = [];
                if ($scope.paymentTerms && $scope.paymentTerms.length > 0) {
                    $scope.offer.paymentTerms = $scope.paymentTerms;
                }
                if ($scope.paymentModes && $scope.paymentModes.length > 0) {
                    $scope.offer.paymentModes = $scope.paymentModes;
                }

            }
            $scope.setSelectedLevel($scope.selectedLevel + 1);
        }
        $scope.setSelectedLevel=function(level){
            $scope.selectedLevel = level;
        };

        function dataSet(level,err) {
            if (err && err.length>0) {
                var parentEl2 = angular.element(document.querySelector('#offerLevel-' + level));
                $mdToast.show(
                    $mdToast.simple()
                        .textContent(err)
                        .position('top right')
                        .theme('error-toast')
                        .hideDelay(6000)
                        .parent(parentEl2)
                );
            } else {
                if(level===3){
                    nextPrepareData();
                    $scope.create();
                }else{
                    nextPrepareData();
                }


            }
        }


        $scope.getOfferInventories=function () {
            var query=offerData.getCriteriaQuery($scope);
            if(query) {
                inventoryProductConversion.getConvertInventory(query, $scope.busUnitId).then(function (response) {
                    if (response.data.inventories.length > 0) {
                        $scope.offerProducts=true;
                        $scope.selOffer.products=response.data.inventories;
                        $scope.setMoq();
                    }else{
                        var error = 'No Matching Products';
                        $scope.selOffer.products=[];
                        var parentEl2 = angular.element(document.querySelector('#offerLevel-' + $scope.selectedLevel));
                        $mdToast.show(
                            $mdToast.simple()
                                .textContent(error)
                                .position('top right')
                                .theme('error-toast')
                                .hideDelay(6000)
                                .parent(parentEl2)
                        );
                    }

                }, function (res) {
                    var parentEl2 = angular.element(document.querySelector('#offerLevel-' + $scope.selectedLevel));
                    $mdToast.show(
                        $mdToast.simple()
                            .textContent(res.data.message)
                            .position('top right')
                            .theme('error-toast')
                            .hideDelay(6000)
                            .parent(parentEl2)
                    );
                    $scope.error = res.data.message;
                });
            }


        };
        $scope.changeOfferValidation = function(){
            if($scope.offerHasValidity){
                $scope.validFrom = new Date();
                $scope.validTill = new Date();
            }else{
                $scope.validFrom = new Date;
                $scope.validTill = undefined;
            }
        };
        $scope.nextStepOffer = function (level) {
            var err;
            if (level === 0) {
                /*if($scope.offer){
                    $scope.offer={};
                }*/
                if (!this.offerName) {
                    err = 'Offer Name is empty';
                } else if($scope.offerHasValidity){
                    if (!($scope.validFrom) || ($scope.validTill && $scope.validFrom > $scope.validTill)) {
                        err = 'Offer Validity is not correct';
                    }
                } else if (!this.offerType) {
                    err = 'Please specify offer Type';
                }

            } else if (level === 1) {
                var isOfferCriteria=offerData.getOfferProductsClassification($scope);
                if(isOfferCriteria || (!isOfferCriteria && ($scope.selOffer &&  $scope.selOffer.products && $scope.selOffer.products.length>0))){
                    err='' ;
                }else{
                    err = 'At least one product is required to Open  offer';
                }
            } else if (level === 2) {
                if(!this.publicOffer && !this.isIntraCompany &&  $scope.selectedContacts.length===0 ){
                    err = 'At least one Contact is required  to Open Private offer ';
                }else if(!this.publicOffer && this.isIntraCompany && this.businessContacts.length===0){
                    err = 'At least one Business unit is required  to Open offer ';
                }

            }
            dataSet(level,err);
        };
        $scope.getSelectedPageId=function () {
            return $scope.selectedPageId;
        };
        $scope.setSelectedPageId=function (selectedPageId) {
            $scope.selectedPageId=selectedPageId;
        };
        //$timeout(function () { $mdSidenav('left').open(); }, false);
        $scope.toggleSideNavigation = function (id, val) {

            if (val === '') {
                $timeout(function () {
                    panelTrigger(id).open().then(function () {
                        console.log('Trigger is done');
                    });
                });
            } else {
                val = val;
            }
            if (val === 'toggle') {
                panelTrigger(id).toggle().then(function () {
                    /*console.log('Trigger is done');*/
                });
            } else if (val === 'open') {
                panelTrigger(id).open().then(function () {
                    /*console.log('Trigger is done');*/
                });
            } else if (val === 'close') {
                panelTrigger(id).close().then(function () {
                    /*console.log('Trigger is done');*/
                });
            } else if (val === 'isOpen') {
                panelTrigger(id).isOpen().then(function () {
                    /*console.log('Trigger is done');*/
                });
            }
        };

        $scope.orderProductCount=function(companyoffer){
            if(companyoffer) {
                $scope.selOffer=companyoffer;
            }
            $scope.selectedproducts=[];
            if($scope.selOffer && $scope.selOffer.products){
                $scope.selectedproducts = $scope.selOffer.products.filter(function (product) {
                    return product.selected ;
                });
                return $scope.selectedproducts.length>0?false:true;

            }
            return $scope.selectedproducts.length <= 1;
        };
        $scope.createOrder = function (categoryOffer) {
            var offer = categoryOffer ? categoryOffer : $scope.selOffer;
            offersApi.createOrder(offer,$scope.busUnitId).then(function (response) {
                if (response.data._id) {
                    $scope.panelChange(null,'cancelViewAll',null);
                    $location.path('orders' + '/' + response.data._id);
                } else {
                    $location.path('orders');
                }
            }, function (response) {
                $scope.error = response.data.message;
                var parentEl2 = angular.element(document.querySelector('#offerLevel-' + $scope.selectedLevel));
                $mdToast.show(
                    $mdToast.simple()
                        .textContent($scope.error)
                        .position('top right')
                        .theme('error-toast')
                        .hideDelay(6000)
                        .parent(parentEl2)
                );

            });
        };

        $scope.getDisabledItem=function (item) {
            return  item.inventory && item.inventory.disabled || (item.inventory && inventoryProduct.getMaxQuantity(item.inventory,$scope.selOffer.offerType==='Buy') < item.MOQ)  || item.numberOfUnits === 0 ;
        };
        $scope.resetCall = function (update) {
            if (update) {
                $scope.allCategoriesProducts = angular.copy($scope.metaProducts);
            } else {
                $scope.allCategoriesProducts = angular.copy($scope.metaProducts);
            }
            if ($scope.selOffer) {
                $scope.offerUpdateFields();
            }
            /*$scope.tableform.reset();*/
            $scope.selectedproducts = [];
            $scope.my = {role: 'Seller'};
            //DatePicker
            $scope.dt = {};
            $scope.dt = new Date();
            $scope.to = {};
            $scope.to.contact = [];
            $scope.contacts = [];
            $scope.products = [];
            $scope.publicOffer = true;
            $scope.my = {role: 'Seller'};
            $scope.role = 'Seller';
            $scope.offerName = '';
            $scope.paymentTerms=[];
            $scope.selectedContacts=[];
            $scope.offerType=null;
            $scope.publicOffer=null;
        };
        $scope.changeSelectedProduct = function (update, offer) {

            /*	$scope.selectedproducts = $scope.allCategoriesProducts.filter(function (product) {
             return product.selected;
             });*/

            //9663578800
            var product = $scope.allCategoriesProducts[update];
            if (product) {
                /*
                 MOQ is REQUIRED - MOQ
                 Qty is REQUIRED - numberOfUnitsAvailable
                 Price is REQUIRED - unitIndicativePrice
                 Margin is REQUIRED - margin
                 Required By Date is REQUIRED - availableDate
                 Sample Required is REQUIRED - sampleAvailable*/
                product.inventory = product;
                product.MOQ = '';
                product.margin = '';
                if(this.offerType==='Sell' || ($scope.selOffer && $scope.selOffer.offerType==='Sell')){
                    product.numberOfUnitsAvailable = product.inventory.numberOfUnits;
                    product.unitIndicativePrice=product.saleUnitPrice;
                }
                if(this.offerType==='Buy' || ($scope.selOffer && $scope.selOffer.offerType==='Buy')){
                    /*product.numberOfUnitsAvailable = null;*/
                    product.unitIndicativePrice=product.buyUnitPrice;
                }
                product.category = product.product.category ? product.product.category._id : null;
                product.subCategory1 = product.product.subCategory1 ? product.product.subCategory1._id : null;
                product.subCategory2 = product.product.subCategory2 ? product.product.subCategory2._id : null;
                product.subCategory3 = product.product.subCategory3 ? product.product.subCategory3._id : null;
                product.availableDate = DatePicker.build({}).dt;
                product.sampleAvailable = '';
                product.producer = $scope.authentication.user._id;
            }
            if (product && product.selected && product.numberOfUnitsAvailable > 0) {
                if (offer) {
                    $scope.selOffer.products.push(product);
                }
                $scope.selectedproducts = $scope.allCategoriesProducts.filter(function (product) {
                    return product.selected;
                });


            } else {
                if (offer) {
                    $scope.selOffer.products = $scope.selOffer.products.filter(function (singleP) {
                        return singleP.inventory._id !== product.inventory._id;
                    });
                } else {
                    $scope.selectedproducts = $scope.allCategoriesProducts.filter(function (product) {
                        return product.selected;
                    });
                }

            }

        };
        /*  $scope.queryOptions=[{name:'Name',value:'name'},{name:'Offer Number',value:'offerNumber'}];
            */
        $scope.removeProduct=function (index) {
            var removeProduct=$scope.selOffer.products[index];
            var pindex=0;
            angular.forEach($scope.allCategoriesProducts, function (e) {
                if (e._id === removeProduct.inventory._id) {
                    $scope.allCategoriesProducts[pindex].selected=false;
                    $scope.selOffer.products.splice(index, 1);
                }
                pindex++;
            });

        };

        $scope.batchOffers=function (index) {
            offersApi.batchOffers(index,$scope);
        };
        $scope.offerUpdateFields = function () {
            $scope.selOffer.statusArray = $scope.offerStatusTypes[$scope.selOffer.offerStatus];
            if ($scope.selOffer.offerStatus === 'Opened'  || $scope.selOffer.offerStatus === 'Placed' || $scope.selOffer.offerStatus === 'Drafted') {
                $scope.selOffer.notConfirmed = true;
            } else {
                $scope.selOffer.notConfirmed = false;
            }
            if ($scope.selOffer.owner.company === $scope.authentication.user.company) {
                $scope.selOffer.createduser = true;
            } else {
                $scope.selOffer.createduser = false;
            }
            /*
             $scope.offer.changeStatusDisplay=$scope.offer.onchangestatus===false;*/
            $scope.selOffer.changeStatusDisplay = $scope.selOffer.onchangestatus === false && $scope.selOffer.statusArray !== undefined;
            //if($scope.order.currentStatus==='Opened' || $scope.order.currentStatus==='Drafted' ){

            //}

        };
        $scope.findOne = function (selOfferId,done) {
            var offerId = null;
            /*$scope.selOffer=null;*/
            if (selOfferId && $stateParams.offerId) {
                offerId = $stateParams.offerId;
            }/*else if(selOrderId) {
			 orderId=null;
			 }*/ else if ($scope.selOffer) {
                offerId = $scope.selOffer._id;
            }
            /*if(!offerId) {*/
            $scope.selOffer = Offers.get({
                offerId: offerId
            });
            $scope.showModal = false;
            $scope.selOffer.$promise.then(function (result) {
                $scope.selOffer.onchangestatus = false;
                $scope.selOffer = result;
                /*$scope.offerUpdateFields();*
                $scope.statusComments = '';
                /*$scope.selOffer = $scope.selOffer;*/

                $scope.selectedValues = [];
                if ($scope.selOffer.owner.company === $scope.authentication.user.company) {
                    $scope.paginationId = 'MyOffers';
                    $scope.selectedIndex = 0;
                } else {
                    $scope.paginationId = 'ReceivedOffers';
                    $scope.selectedIndex = 1;
                }

                $scope.resetCall(true);
                $scope.findOfferMessages($scope.selOffer._id);
                $scope.offerUpdateFields();
                if(!$stateParams.offerId || $stateParams.offerId!==$scope.selOffer._id) {
                    $scope.panelChange($scope.selOffer, 'view', $scope.selectedIndex);
                }
                done();

                /*$scope.offerUpdateFields();*/
            });

            /*}else{
             $location.path('offers');
             }*/
        };
        $scope.create = function () {
            if($scope.selOffer._id){
                
                var offer = new Offers($scope.offer);
                offer._id = $scope.selOffer._id;
                offer.businessUnit=$scope.busUnitId;
                offer.created = new Date();
                offer.$update(function () {
                    var j = 0;
                    for (j in $scope.offers) {
                        if ($scope.offers [j]._id === offer._id) {
                            $scope.offers[j] = offer;
                        }
                    }
                    $scope.offerUpdateFields();
                    $scope.getOffersSummary(0,$scope.busUnitId);
                    $scope.changeSelOffer(offer);
                },function (err) {
                    $scope.error = err.response;
                })
            }else {
                offersApi.offerCreateData($scope).then(function (response) {
                    $scope.offers.push($scope.selOffer);
                    $scope.changeSelOffer($scope.selOffer);
                    $scope.oldOffer = null;
                    $scope.selOffer = null;
                    $scope.offer = {};
                    $scope.resetCall(false);
                    $scope.selectedLevel = 0;
                    $location.path('offers');
                    $scope.getOffersSummary(0, $scope.busUnitId);
                    $scope.panelChange('', 'cancelViewAll', 0);
                }, function (errorResponse) {/**/
                    $scope.selectedLevel = $scope.selectedLevel - 1;
                    var parentEl2 = angular.element(document.querySelector('#offerLevel-' + $scope.selectedLevel));
                    $mdToast.show(
                        $mdToast.simple()
                            .textContent(errorResponse.data.message)
                            .position('top right')
                            .theme('error-toast')
                            .hideDelay(6000)
                            .parent(parentEl2)
                    );
                });
            }
        };
        $scope.createMessage = function (offerId) {
            // Create new Message object
            var message = new Messages({
                body: this.body,
                offer: offerId
            });

            // Redirect after save
            message.$save(function (response) {
                response.user = $scope.authentication.user;
                $scope.offerMessages.unshift(response);
                $scope.showComment = false;

            }, function (errorResponse) {
                $scope.error = errorResponse.data.message;
            });
        };
        $scope.selproducts = '';
        $scope.findOfferMessages = function (offer) {
            $scope.offerMessages = OfferMessages.query({
                offerId: $stateParams.offerId ? $stateParams.offerId : offer
            });
            $scope.offerMessages.$promise.then(function (result) {
            });

        };
        $scope.selectedTab = 'MyOffers';
        $scope.updateOfferStatus = function () {
            var offer = this.offer ?this.offer:$scope.selOffer;
            /*offer.offerStatus = this.currentStatus;
             offer.statusComments = this.statusComments;*/

            offer=new Offers({_id:offer._id, offerStatus:offer.offerStatus});
            if(this.currentStatus)
                offer.offerStatus=this.currentStatus;
            if(this.statusComments)offer.statusComments=this.statusComments;

            offer.$update(function () {
                offer.onchangestatus = false;
                offer.onchangespaymentstatus = false;
                $scope.currentStatus = '';
                $scope.statusComments = '';
                /*$scope.showModal = !this.showModal;*/
                var j = 0;
                for (j in $scope.offers) {
                    if ($scope.offers [j]._id === offer._id) {
                        $scope.offers[j] = offer;
                    }
                }
                $scope.offerUpdateFields();
                $scope.getOffersSummary(0,$scope.busUnitId);
                /*
                $scope.panelChange(offer, 'view', false);
                $scope.hide();
                $scope.clearFields();*/
            }, function (errorResponse) {
                $scope.error = errorResponse.data.message;
            });

        };

        $scope.update = function () {
            var offer = $scope.selOffer;
            /*	if($scope.selOffer.offerStatus==='Expired' && $scope.selOffer.user._id===$scope.authentication.user._id){

             //No Requirements for the Pending offers.
             /!*$scope.selOffer.offerStatus='Pending';*!/
             }*/
            $scope.tocontacts = {contacts: [], groups: []};
            if($scope.selectedContacts) {
                $scope.togroups = [];
                $scope.toallcontacts = [];

                for (var j = 0; j < $scope.selectedContacts.length; j++) {
                    var singlecontact =  $scope.selectedContacts[j];
                    if (singlecontact.firstName) {
                        $scope.tocontacts.contacts.push({'contact': singlecontact._id});
                        $scope.toallcontacts.push({
                            'contact': singlecontact._id,
                            'nVipaniUser': singlecontact.nVipaniUser
                        });
                        $scope.togroups.push(singlecontact);
                    }
                    if (singlecontact.name) {
                        $scope.tocontacts.groups.push({'group': singlecontact._id});
                        for (var k = 0; k < $scope.contactsVsGroup.length; k++) {
                            var singlecontacts =$scope.contactsVsGroup[k];
                            if (singlecontacts.groups && singlecontacts.groups.indexOf(singlecontact._id) >= 0)
                                $scope.toallcontacts.push({'contact': singlecontacts._id});
                            $scope.togroups.push(singlecontact);
                        }

                    }
                }


            }
            offer.toContacts = {contacts: $scope.tocontacts.contacts, groups: $scope.tocontacts.groups};
            /*offer.notificationsContacts=($scope.toallcontacts && $scope.toallcontacts.length>0 ? $scope.toallcontacts : null);*/
            for (var i = 0; i < offer.products.length; i++) {
                $scope.changeProductPrice(offer.products[i],i);
                if(offer.products[i].inventory)
                    offer.products[i].inventory = offer.products[i].inventory._id;

                /*$scope.products[i].sampleAvailable = $scope.products[i].sampleAvailable.dt;*/
            }

            offer.$update(function () {
                var j = 0;
                for (j in $scope.offers) {
                    if ($scope.offers [j]._id === offer._id) {
                        $scope.offers[j] = offer;
                    }
                }
                $scope.offerUpdateFields();
                $scope.getOffersSummary($scope.selectedIndex,$scope.busUnitId);
                $scope.panelChange(offer, 'view', false);
                /*if($scope.offer.offerStatus==='Drafted'){
                 $location.path('offers/' + offer._id + '/edit');
                 }else{
                 $location.path('offers/' + offer._id);
                 }*/
            }, function (errorResponse) {
                $scope.error = errorResponse.data.message;
            });
        };
        /*------ Find the categorues and products for criteria -----------*/
        $scope.findSubCategories1 = function () {
            categoryService.findSubCategories1().then(function (response) {
                $scope.subCategories1 = response.data;
            }, function (res) {
                $scope.error = res.data.message;
            });
        };
        $scope.findCategory = function (id) {
            if(id) {
                categoryService.findCategoryById(id).then(function (response) {
                    if (response.data.type === 'MainCategory') {
                        $scope.subCategories1 = response.data.children;
                        $scope.selectedMainCategory=response.data;
                        $scope.selectedSubCategory1Category=null;
                        $scope.selectedSubCategory=null;
                        $scope.subCategories2 = [];
                        $scope.hsncodes=[];
                    } else if (response.data.type === 'SubCategory1') {
                        $scope.subCategories2 = response.data.children;
                        $scope.selectedSubCategory1Category=response.data;
                        $scope.selectedSubCategory=null;
                        $scope.hsncodes=[];
                    } else if (response.data.type === 'SubCategory2') {
                        $scope.selectedSubCategory = response.data;
                        $scope.hsncodes = response.data.hsncodes;
                        $scope.inventoryUoms=response.data.unitOfMeasures;
                        $scope.taxgroups = response.data.taxGroups;
                        $scope.productBrands=  response.data.productBrands;
                    }
                }, function (res) {
                    $scope.error = res.data.message;
                });
            }
        };
        $scope.findBrandChange=function (id) {
            if(id){
                productBrandService.findBrandById(id).then(function (response) {
                    $scope.brandUoms=response.data.unitOfMeasures;
                    $scope.selectedProductBrand=response.data;

                });
            }
        };
        $scope.alertClose = function(ev){
            var confirm = $mdDialog.confirm()
                .title('Are you sure you want to discard this offer')
                .textContent('All the entered data will be lost')
                .ariaLabel('Confirmation')
                .targetEvent(ev)
                .ok('Discard Anyway')
                .cancel('Cancel');

            $mdDialog.show(confirm).then(function() {
                //$scope.panelChange('viewAll');
                $location.path('offers');
                $scope.panelChange(null,'viewAll','MyOffers');
            }, function() {
                $scope.cancel();
            });
        };
        // Find a list of Offers
        $scope.find = function (tabSelection,pagenumber,isTab) {
            $scope.isFilterSummary=false;
            var paginationfield={};
            if(!isNaN(pagenumber)){
                paginationfield={page:pagenumber,limit:$scope.itemsPerPage,businessUnitId:$scope.authentication.companyBusinessUnits.defaultBusinessUnitId};
            }
            if(this.searchText && this.searchText.length>0){
                paginationfield.searchText=this.searchText;
            }

            offersApi.getOffers($scope.tabs[$scope.selectedIndex].type==='MyOffers'?true:false,paginationfield).then(function (offers) {
                $scope.offers=offers.data.offer;
                $scope.total_count=offers.data.total_count;
                if(offers.data.summaryData){
                    if($scope.filterKey){
                        $scope.offers=offers.data.summaryData[$scope.filterKey];
                    }else {
                        $scope.offerfilters = offers.data.summaryData;
                    }
                }
                /* if($scope.offers.length>0)*/
                /* $scope.selOffer = $scope.offers[0];*/
                if(!isTab) {
                    if ($scope.location.$$url === '/offercreate' || $scope.location.$$url === 'createoffer') {
                        $scope.panelChange(null, ($scope.location.$$url === 'offercreate'?'add':'addnew'), 0);
                    } else if ($stateParams.offerId !== undefined) {
                        $scope.panelChange($scope.selOffer, 'view', tabSelection);
                    } else {
                        $scope.panelChange(null, 'viewAll', tabSelection);
                    }
                }else{
                    $scope.location.path('offers');
                    $scope.panelChange(null, 'viewAll', tabSelection);
                }
            },function (err) {
                $scope.error=err.data.message;
                if($scope.error==='Session Expired.'){
                    localStorageService.remove('nvipanilogindata');
                }
            });
        };
        $scope.offerUpdateStatusEditButton=function (offer) {
            var currentRole='Sale';
            if($scope.authentication.user && offer.user===$scope.authentication.user._id){
                currentRole='Sale';
            }else{
                currentRole='Purchase';
            }
            return ($scope.authentication.user && offer.offerStatus && offer.user._id===$scope.authentication.user._id && this.offer.offerStatus!=='Drafted' && $scope.offerStatusTypes[offer.offerStatus].length>0);

        };
        $scope.changeStatusOffer = function (offer) {

            $scope.selOffer = offer;
            $scope.offerUpdateFields();
            /*$scope.showOfferStatusDialog(this.event);*/
        };


        $scope.changeProductPrice=function (singleproduct,index) {
            var price=$scope.getPrice(singleproduct);
            $scope.selOffer.products[index].unitIndicativePrice=price;
            $scope.selOffer.products[index].margin=(singleproduct.inventory && singleproduct.inventory.MRP) ?(singleproduct.inventory.MRP-price)*100/singleproduct.inventory.MRP:0;
        };
        $scope.getPrice=function(singleproduct){
            if(singleproduct.inventory && (singleproduct.inventory.moqAndMargin || singleproduct.inventory.moqAndBuyMargin)) {
                return inventoryProduct.getQuantityPrice((($scope.selOffer.offerType === 'Sell') ? singleproduct.inventory.moqAndMargin : singleproduct.inventory.moqAndBuyMargin), singleproduct.MOQ, singleproduct.inventory.MRP);
            }else {
                return 0;
            }

        };
        $scope.selectedtab = 'MyOffers';
        $scope.criteriaChips=criteriaChips;
        $scope.paginationId='MyOffers';
        $scope.offers = [];
        $scope.categories = [];
        $scope.searchText = {};
        $scope.offerTypeText = 'Buy';
        $scope.searchFields={};
        $scope.queryBy = '$';
        $scope.authentication = Authentication;
        $scope.noResultsTag = null;
        $scope.selectedproducts = [];
        $scope.offerMessages = [];
        $scope.showModal = false;
        $scope.offerTypes = [{value: 'Sell', text: 'Seller'}, {value: 'Buy', text: 'Buyer'}];
        $scope.offerType = 'Sell';
        $scope.selectedIndex = 0;
        $scope.offerCategories = ['Buyer', 'Seller', 'Mediator'];
        $scope.offerStatusTypes = {
            'Drafted': [{value: 'Opened', text: 'Opened'},{value: 'Cancelled', text: 'Cancelled'}],
            'Placed': [{value: 'Cancelled', text: 'Cancelled'}],
            'Opened': [{value: 'Cancelled', text: 'Cancelled'}],
            'Completed': [],
            'Cancelled':[],
            'Expired':[]
        };
        $scope.tabs = [
            {title: 'My Offers', type: 'MyOffers', content: 'Drafted'},
            {title: 'Received Offers', type: 'ReceivedOffers', content: 'Drafted'} /* ,
          {title: 'Public Offers', type: 'PublicOffers', content: 'Opened'}*/
        ];
        $scope.myDate = new Date();
        $scope.minDate = new Date(
            $scope.myDate.getFullYear(),
            $scope.myDate.getMonth() - 2,
            $scope.myDate.getDate());
        $scope.maxDate = new Date(
            $scope.myDate.getFullYear(),
            $scope.myDate.getMonth() + 2,
            $scope.myDate.getDate());
        $scope.escapeRegExp=function(text) {
            return text.replace(/[-[\]{}()*+?.,\\^$|#\s]/g, '\\$&');
        };
        $scope.panelChange = function (offer, changeStr, tabSelection, type) {
            $scope.context=[
                {title:'Offers',link:'/#!/offers'}
            ];
            /*$scope.offerfilters = [
                { title:'New Offers', content: 0 , tag: 'New' },
                { title: 'Sell Offers', content: 0, tag: 'sell' },
                { title:'Buy Offers', content: 0, tag: 'buy'},
                { title:'Expired Offers', content: 0, tag: 'expired'},
                { title:'Draft offers', content: 0, tag: 'draft'}
            ];*/
            $scope.showEditOffer = false;
            $scope.showAddOffer = false;
            $scope.showView = false;
            $scope.showViewAll = true;
            $scope.error=null;
            $scope.contactsVsGroup = [];
            $scope.cancel();
            if (offer !== null && angular.isObject(offer) && changeStr === 'edit') {
                $scope.showEditOffer = true;
                /*$scope.findOne(tabSelection);*/
            } else if (offer !== null && angular.isObject(offer) && changeStr === 'view' && offer.offerStatus!=='Drafted') {
                /*$scope.selOffer = angular.copy(offer);*/
                /* if (offer.offerStatus === 'Drafted') {*/
                $scope.showView = true;
                $scope.selectedContactItem = null;
                $scope.searchOfferContactText = null;
                $scope.selectedCategoryItem = null;
                $scope.searchOfferCategoryText = null;
                $scope.selectedContactItem = null;
                $scope.searchOfferContactText = null;
                $scope.selectedCategories = [];
                $scope.selectedCategoryItem = null;
                $scope.searchOfferCategoryText = null;
                $scope.selectedContact=[];
                $scope.myTabIndex = 0;
                if($stateParams.messageId){
                    $scope.myTabIndex = 2;
                }
                // This needs to be added based on tab level selection of offers view.
                /*$scope.criteriaChips.loadChipContacts($scope,$scope.selectedContact,contactsQuery);
                $scope.criteriaChips.loadCategories($scope);*/
                $scope.criteriaChips.loadChipContacts($scope,false,false,false,$scope.selectedContact,contactsQuery);
                $scope.offerUpdateFields();

                /* } else {
                     /!*$scope.findOne(tabSelection);*!/
                     $scope.showView = true;
                     $scope.offerUpdateFields();
                 }*/


            }else if (changeStr === 'add' || changeStr === 'addnew' || (offer !== null && angular.isObject(offer) && changeStr === 'view' && offer.offerStatus==='Drafted') ) {

                /* $scope.offer=Offers;*/
                if(changeStr === 'add') {
                    $scope.Oldoffer.products = [];
                }else{
                    $scope.Oldoffer = Offers;
                }
                $scope.context.push({title: 'Create Offer', link: ''});
                $scope.selectedPageId = 0;
                $scope.selectedIndex = 0;
                $scope.selectedLevel = 0;
                $scope.showAddOffer = true;
                $scope.selOffer = null;
                $scope.paginationId = 'MyOffers';
                $scope.readonly = false;
                $scope.showView = false;
                $scope.showViewAll = false;
                $scope.selectedContactItem = null;
                $scope.searchOfferContactText = null;
                $scope.selectedCategories = [];
                $scope.selectedCategoryItem = null;
                $scope.searchOfferCategoryText = null;
                $scope.selectedContact = [];
                $scope.selectedContacts = [];
                $scope.offer = {};
                $scope.validFrom = new Date();
                $scope.paymentTerms = [];
                $scope.selOffer = {};
                $scope.paymentModes = [];
                if(offer){
                    $scope.offerName = offer.name;
                    $scope.offerType = offer.offerType;
                    $scope.selOffer = angular.copy(offer);
                    $scope.isPublic = offer.isPublic;
                    $scope.isIntraCompany = offer.isIntraCompany;
                    offer.toContacts.contacts.forEach(function(contact){
                        $scope.selectedContacts.push(contact.contact);
                    });
                    offer.toContacts.groups.forEach(function (group) {
                        $scope.selectedContacts.push(group.group);
                    });
                    $scope.selOffer.products = [];
                    offer.products.forEach(function (product) {
                        product.inventory.MOQ = {MOQ:product.MOQ,margin:product.margin};
                        product.inventory.selected = true;
                        $scope.selOffer.products.push(product.inventory);
                    });
                    offer.applicablePaymentTerms.forEach(function(term){
                        $scope.paymentTerms.push(term.paymentTerm._id);
                    });
                    offer.applicablePaymentModes.forEach(function(mode){
                        $scope.paymentModes.push(mode.paymentMode._id);
                    })
                }else{
                    $scope.offerName = '';
                    $scope.offerType = type;
                    $scope.selOffer = angular.copy($scope.Oldoffer);
                    $scope.setMoq();
                    $scope.isPublic = true;
                    $scope.isIntraCompany = false;
                }
                //$scope.getOfferProducts();
                /*
                 $scope.criteriaChips.loadCategories($scope);*/
                $scope.inventoryItemsClassification = {
                    classificationTypes: [{text: 'Custom', value: 'Custom'}, {
                        text: 'Criteria',
                        value: 'Criteria'
                    }],
                    classificationType: 'Custom',
                    classificationCriteria: {
                        criteriaType: 'Custom',
                        criteriaTypes: [{
                            text: 'All',
                            value: 'All'
                        }, {text: 'Custom', value: 'Custom'}],
                        criteria: [{
                            category: '',
                            productBrand: '',
                            gradeDefinition: [{
                                attributeKey: '',
                                attributeValue: ''
                            }],
                            qualityDefinition: [{
                                attributeKey: '',
                                attributeValue: ''
                            }],
                            unitOfMeasure: ''
                        }]
                    }
                };

            }else if (changeStr === 'cancelViewAll') {
                $mdDialog.cancel();
            }else if (changeStr === 'viewAll') {

                $scope.pageno=1;
                /* $scope.total_count = 0;*/
                $scope.itemsPerPage = 10;
                $scope.showViewAll = true;
                $scope.showAddoffer = false;
            }
            if($scope.showView){
                $mdDialog.show({
                    template: '<div class="md-dialog-container trowserView"><md-dialog class="trowser">' +
                    '<view-offer ng-show="showView" class="nvc-position-top-left"></view-offer>' +
                    '<tracker-view class="bb-wid-50 nvc-lfloat nvc-display-block nv-position-relative nvc-white-background nvc-view-separator" ng-if="trackview"></tracker-view>' +
                    '</md-dialog><div class="md-dialog-focus-trap" tabindex="0"></div></div>',
                    /* targetEvent: ev,*/
                    scope: $scope,
                    clickOutsideToClose: true,
                    preserveScope: true
                });
            }


        };

        $scope.getCompanyPaymentTerms=function () {
            PaymentTerms.findCompanyPaymentTerms($scope.authentication.user.company).then(function (results) {
                $scope.paymentterms = results.data;

            }, function (errPayment) {
                $scope.errPayment = errPayment.message;
            });
        };
        $scope.getCompanyPaymentModes=function () {
            PaymentTerms.findCompanyPaymentModes($scope.authentication.user.company).then(function (results) {
                $scope.paymentmodes = results.data;

            }, function (errPayment) {
                $scope.errPayment = errPayment.message;
            });
        };
        $scope.allData = function (result, callback) {
            /* if(!$scope.allCategoriesProducts) {*/
            inventoryProduct.getInventories().then(function (productResults) {
                /* $scope.contactsVsGroup = [];*/
                $scope.allCategoriesProducts = angular.copy(productResults.data.inventory);
                /*  contactsQuery.findContactsVsGroups($scope);*/
                /*  contactsQuery.findContacts($scope);
                  $scope.contactsVsGroup=$scope.contacts;*/

                callback();
            }, function (response) {
                $scope.error = response.data.message;

            });
        };
        /* ============== For tracking view ================*/
        $scope.expandView = function(){
            var elem = angular.element(document.querySelector('md-dialog.trowser'));
            elem[0].style.minWidth = '100%';
            elem[0].style.display = 'block';
            elem[0].children[0].style.width = '50%';
            elem[0].children[0].style.float = 'left';
        };
        $scope.minimiseView = function(){
            $scope.trackview = false;
            $scope.stockview = false;
            var elem = angular.element(document.querySelector('md-dialog.trowser'));
            if(elem) {
                elem[0].style.minWidth = '50%';
                elem[0].children[0].removeAttribute('style');
            }
        };
        $scope.track = function(inventory,stock,id){
            var trackPathfields={};
            inventoryProduct.getTrackPaths({inventoryId:inventory._id,stockId:stock._id}).then(function (response) {
                $scope.trackPaths=response.data;

                if($scope.trackPaths.destinations.length===0){
                    var elem = angular.element(document.querySelector('#offerProduct-'+id));
                    $mdToast.show(
                        $mdToast.simple()
                            .textContent('No Track Paths')
                            .position('top right')
                            .theme('error-toast')
                            .hideDelay(600)
                            .parent(elem)
                    );
                }else {
                    $scope.expandView();
                    $scope.trackview = true;
                }
            },function (errorResponse) {
                $scope.error=errorResponse.data.message;
                var elem = angular.element(document.querySelector('#stock-'+id));
                $mdToast.show(
                    $mdToast.simple()
                        .textContent($scope.error)
                        .position('top right')
                        .theme('error-toast')
                        .hideDelay(600)
                        .parent(elem)
                );
            });
        };
        $scope.getStockSource=function (stock) {
            return ((stock.stockIn.inventories && stock.stockIn.inventories.length >0) ||(stock.stockIn.orders &&  stock.stockIn.orders.length>0));
        };
        /* ============= for tracking view ==================*/

        $scope.findReceivedOffers = function (tabSelection) {
            $scope.selectedTab = 'ReceivedOffers';
            $scope.selectedIndex = 1;
            $scope.offers = [];
            $scope.selOffer = null;
            /*$scope.selOffer=null;*/
            offersApi.receivedOffers().then(function (response) {
                $scope.offers = response.data;
                if ($scope.offers.length > 0) {
                    $scope.selOffer = $scope.offers[0];
                    /*$scope.findOne(tabSelection);*/

                }
                $scope.panelChange(null, 'viewAll', tabSelection);
            },function (response) {
                $scope.error = response.data.message;
            });
        };

        $scope.findSpecific = function () {
            /*	$scope.orders=[];*/
            offersApi.offersQuery().then(function (response) {
                $scope.offers = response.data;
                if ($scope.offers.length > 0) {
                    $scope.selOffer = $scope.offers[0];
                    /*$scope.findOne(true);*/
                    $scope.panelChange($scope.selOffer, 'view', true);
                }
                /*$scope.registerUser = response;
                 $scope.userCategories = ['Trader', 'Agent', 'Consumer', 'Producer'];
                 $scope.selectedItem=$scope.registerUser.userCategories;
                 $scope.phones = [];

                 //return response;
                 if($scope.registerUser.status!=='Register Request'){
                 $location.path('/');
                 }*/
            },function (response) {
                $scope.error = response.data.message;
            });
        };

        $scope.setMoq = function () {
            if($scope.selOffer.products) {
                angular.forEach($scope.selOffer.products, function (product) {
                    if (product.moqAndMargin.length > 0 && product.moqAndMargin[0].MOQ && product.moqAndMargin[0].margin) {
                        product.MOQ = product.moqAndMargin[0];
                    } else {
                        product.MOQ = {MOQ: 1, margin: 0};
                    }
                });
            }
        };
        $scope.findInit = function () {
            $scope.my = {role: 'Seller'};
            //DatePicker
            $scope.dt = new Date();
            $scope.to = {};
            $scope.to.contact = [];
            $scope.contacts = [];
            $scope.products = [];
            $scope.publicOffer = true;
            $scope.my = {role: 'Seller'};
            $scope.role = 'Seller';
            //$scope.getOffersSummary(0,$scope.busUnitId);
            if($stateParams.offerId !== undefined){
                $scope.findOne(true,function (err) {
                    if(!err) {
                        $scope.offers = [];
                        $scope.pageno = 1;
                        $scope.total_count = 0;
                        $scope.itemsPerPage = 10;
                        $scope.find($scope.selectedIndex, $scope.pageno,false);
                    }
                });
            }else {
                $scope.selectedIndex = 0;
                $scope.paginationId = 'MyOffers';

                $scope.offers = [];
                $scope.pageno = 1;
                $scope.total_count = 0;
                $scope.itemsPerPage = 10;
                $scope.find($scope.selectedIndex, $scope.pageno,false);
            }

            /* }*/
        };

        $scope.changeSelectionTabByIndex = function(businessUnitId,index){
            var type;
            $scope.busUnitId=businessUnitId;
            switch(index){
                case 0: type='MyOffers';break;
                case 1:type = 'ReceivedOffers';break;
                case 2:type='PublicOffers';break;
            }
            $scope.changeSelectionTab(businessUnitId,type);
        };

        $scope.changeSelectionTab = function (businessUnitId,type) {
            $scope.businessUnit = businessUnitId;
            var authdata= localStorageService.get('nvipanilogindata');
            authdata.companyBusinessUnits.defaultBusinessUnitId=businessUnitId;
            localStorageService.set('nvipanilogindata',authdata);
            $scope.filterKey=null;
            if (type === 'MyOffers') {
                $scope.selOffer = null;
                $scope.selectedIndex = 0;
                $scope.paginationId='MyOffers';
                $scope.getOffersByBusinessUnit(businessUnitId,$scope.selectedIndex,$scope.pageno,true);
            } else if (type === 'ReceivedOffers') {
                $scope.selectedIndex = 1;
                $scope.paginationId='ReceivedOffers';
                $scope.getOffersByBusinessUnit(businessUnitId,$scope.selectedIndex,$scope.pageno,true);
            } else if (type === 'PublicOffers') {
                $scope.selOffer = null;
                $scope.selectedIndex = 2;
                $scope.paginationId='PublicOffers';
                categoryService.findMainCategories().then(function (response) {
                    $scope.categories = response.data;
                },function (response) {
                    $scope.error = response.data.message;
                });
            }
        };
        $scope.allCategories=function () {

            categoryService.findCategory().then(function (response) {
                $scope.category=response.data;
            },function (response) {
                if(response.data) {
                    $scope.error = response.data.message;
                }
            });
            categoryService.findSubcategory2Offers().then(function (response) {
                $scope.subCategoryOffers = response.data.offer;
                $scope.selectedSubCategoryOffer = $scope.subCategoryOffers[0];
            },function (response) {
                $scope.error = response.data.message;
            });
        };

        $scope.displayEachName = function (item) {
            return (item.eachObject.firstName ? item.eachObject.firstName : item.eachObject.name) + (item.eachObject.middleName ? ' ' + item.eachObject.middleName : '') + (item.eachObject.lastName ? ' ' + item.eachObject.lastName : '');
        };

//Clear Fields Function
        $scope.clearFields = function () {
            $scope.product = '';
            $scope.unitSize = '';
            $scope.unitMeasure = '';
            $scope.numberOfUnits = '';
            $scope.totalQuantity = '';
            $scope.isPublic = true;
            $scope.currentStatus = '';
            $scope.statusComments = '';

        };
        $scope.offerRedirection = function () {
            $location.path('offers');
        };
        $scope.findProducts = function () {
            inventoryProducts.inventoryProducts().then(function (results) {
                /*$scope.metaProducts.$promise.then(function (result) {*/
                $scope.allCategoriesProducts = angular.copy(results.data);
            });
        };

        $scope.selectProduct = function (metaProduct, index) {
            $scope.products[index].product = metaProduct;
            $scope.products[index].unitSize = metaProduct.unitSize;
            $scope.products[index].unitPrice = metaProduct.unitPrice;
            $scope.products[index].unitMeasure = metaProduct.unitMeasure;
            $scope.products[index].unitIndicativePrice= metaProduct.unitPrice;
            /*if($scope.offerType==='Sell') {
                $scope.products[index].unitIndicativePrice = metaProduct.saleUnitPrice;
            }else if($scope.offerType==='Buy'){
                $scope.products[index].unitIndicativePrice = metaProduct.buyUnitPrice;
            }*/
        };
        $scope.getImageLabel=function (inventory) {
            if(inventory.disabled===true){
                return 'Disabled';
            }else  if(inventory.numberOfUnits===0 && inventory.maxUnits!==0){
                return 'No Stock';
            }else if(inventory.numberOfUnits===0 && inventory.maxUnits===0){
                return 'No Use';
            }else{
                return null;
            }
        };


        $scope.toggleModal = function () {
            $scope.showModal = !$scope.showModal;
        };

        /*$scope.selected = 0;*/
        $scope.select = function (index) {
            $scope.selected = index;
        };


        $scope.showOfferStatusDialog = function (ev) {

            var useFullScreen = ($mdMedia('sm') || $mdMedia('xs'));
            $mdDialog.show({
                template: '<md-dialog aria-label=" Dialog">' +
                '<md-toolbar><div class="md-toolbar-tools"><h2>Offer Status</h2><span flex></span>' +
                '<md-button class="md-icon-button" ng-click="cancel()">' +
                '<md-icon class="fa fa-minus" aria-label="Close dialog"></md-icon></md-button></div></md-toolbar>' +
                '<md-dialog-content class="md-dialog-content">' +
                /*'<md-input-container class="wid-100">' +*/
                '<md-select aria-label="Offer Status" ng-model="currentStatus">' +
                '<md-option ng-repeat="s in selOffer.statusArray">{{s.text}}</md-option>' +
                '</md-select>' +
                /*'</md-input-container>' +*/
                '<md-input-container class="md-block">' +
                '<label>Comments</label>' +
                '<textarea required ng-model="statusComments"></textarea>' +
                '</md-input-container>' +
                '</md-dialog-content>' +
                '<md-dialog-actions>' +
                '<md-button class="md-raised md-primary" ng-click="updateOfferStatus();">Submit</md-button>' +
                '<md-button class="md-raised" ng-click="cancel();">Cancel</md-button></md-dialog-actions>' +
                '</md-dialog>',
                targetEvent: ev,
                scope: $scope,
                clickOutsideToClose: true,
                fullscreen: useFullScreen,
                preserveScope: true
            });

        };



        $scope.hide = function () {
            $mdDialog.hide();
        };
        $scope.cancel = function () {
            $mdDialog.cancel();
        };

        $scope.tag = 'Agriculture';
        $scope.changeType = function (tagVal) {
            $scope.tag = tagVal;
        };

        $scope.closeOfferDetails = function () {
            $scope.showView=false;
            $scope.cancel();
            $scope.location.path('/offers');
        };


        //$scope.subCategoryOffers[0].expanded = true;

        $scope.collapseAll = function (index) {
            $scope.selectedSubCategoryOffer = $scope.subCategoryOffers[index];
        };

    }

]);

app.controller('PublicOfferController', [ '$scope','$stateParams','$location','Authentication', 'Offers','offersApi','localStorageService','categoryService',function ($scope,$stateParams,$location, Authentication, Offers,offersApi,localStorageService,categoryService){
    $scope.authentication = Authentication;
    $scope.user=Authentication.user;
    $scope.location=$location;
    $scope.tabs = [
        {title: 'Received Orders', type: 'ReceivedOrders', content: 'Drafted'},
        {title: 'Received Offers', type: 'ReceivedOffers', content: 'Drafted'},
        {title: 'Offers from my Segments', type: 'segments', content: 'Opened'},
        {title: 'Offers from my Contacts ', type: 'contacts', content: 'Drafted'}/*,
        {title: 'My Offers', type: 'MyOffers', content: 'Opened'}*/
    ];
    function getSelectedIndex(type) {
        var filterValue= $scope.tabs.filter(function (eachtab) {
            return eachtab.type===type;
        });
        if(filterValue.length>0){
            return $scope.tabs.indexOf(filterValue[0]);
        }else {
            return 0;
        }
    }

    $scope.getMyOfferIndex=function (offerTabType) {
        return $scope.tabs[$scope.selectedIndex].type===offerTabType;
    };
    function findReceivedOffers(tabSelection) {
        $scope.selectedTab = 'ReceivedOffers';
        $scope.selectedIndex = 1;
        $scope.offers = [];
        $scope.selOffer = null;
        /*$scope.selOffer=null;*/
        offersApi.receivedOffers().then(function (response) {
            $scope.offers = response.data;
            if ($scope.offers.length > 0) {
                $scope.selOffer = $scope.offers[0];
                /*$scope.findOne(tabSelection);*/

            }
            $scope.panelChange(null, 'viewAll', tabSelection);
        },function (response) {
            $scope.error = response.data.message;
        });
    }
    function getOffersByBusinessUnit(selectedBusinessUnit,tabSelection,pagenumber,isTab){
        var paginationfield={};
        if($scope.searchText && $scope.searchText.length>0){
            paginationfield.searchText=$scope.searchText;
        }
        if(!isNaN(pagenumber)){
            paginationfield={page:pagenumber,limit:$scope.itemsPerPage};
        }

        offersApi.getOffersByBusinessUnit(tabSelection,paginationfield,selectedBusinessUnit).then(function(offers){
            $scope.offers=offers.data.offer;
            $scope.total_count=offers.data.total_count;
            /*  $scope.company.businessUnit = selectedBusinessUnit;*/
            /* if($scope.offers.length>0)*/
            /* $scope.selOffer = $scope.offers[0];*/
            if(!isTab) {
                if ($scope.location.$$url === '/offercreate' || $scope.location.$$url === '/createoffer') {
                    $scope.panelChange(null, ($scope.location.$$url === '/offercreate' ?'addnew':'add'), 0);
                } else if ($stateParams.offerId !== undefined) {
                    $scope.panelChange($scope.selOffer, 'view', $scope.selectedIndex);
                } else {
                    $scope.panelChange(null, 'viewAll', $scope.selectedIndex);
                }
            }else{
                $scope.panelChange(null, 'viewAll', $scope.selectedIndex);
            }
        },function (err) {
            if(err) {
                $scope.error = err.data.message;
                if ($scope.error === 'Session Expired.') {
                    localStorageService.remove('nvipanilogindata');
                }
            }
        });
    }

    $scope.findSpecific = function () {

        offersApi.offersQuery().then(function (response) {
            $scope.offers = response.data;
            if ($scope.offers.length > 0) {
                $scope.selOffer = $scope.offers[0];
                /*$scope.findOne(true);*/
                $scope.panelChange($scope.selOffer, 'view', true);
            }
        },function (response) {
            $scope.error = response.data.message;
        });
    };
    $scope.find = function (tabSelection,pagenumber,isTab) {
        var paginationfield={};
        if(!isNaN(pagenumber)){
            paginationfield={page:pagenumber,limit:$scope.itemsPerPage};
        }
        if(this.searchText && this.searchText.length>0){
            paginationfield.searchText=this.searchText;
        }

        offersApi.getOffers(tabSelection===0?true:false,paginationfield).then(function (offers) {
            $scope.offers=offers.data.offer;
            $scope.total_count=offers.data.total_count;
            /* if($scope.offers.length>0)*/
            /* $scope.selOffer = $scope.offers[0];*/
            if(!isTab) {
                if ($scope.location.$$url === '/offercreate' || $scope.location.$$url === 'createoffer') {
                    $scope.panelChange(null, ($scope.location.$$url === 'offercreate'?'add':'addnew'), 0);
                } else if ($stateParams.offerId !== undefined) {
                    $scope.panelChange($scope.selOffer, 'view', tabSelection);
                } else {
                    $scope.panelChange(null, 'viewAll', tabSelection);
                }
            }else{
                $scope.location.path('offers');
                $scope.panelChange(null, 'viewAll', tabSelection);
            }
        },function (err) {
            $scope.error=err.data.message;
            if($scope.error==='Session Expired.'){
                localStorageService.remove('nvipanilogindata');
            }
        });
    };

    $scope.findInit = function () {
        if($stateParams.offerId !== undefined){
            $scope.findOne(true,function (err) {
                if(!err) {
                    $scope.offers = [];
                    $scope.pageno = 1;
                    $scope.total_count = 0;
                    $scope.itemsPerPage = 10;
                    $scope.find($scope.selectedIndex, $scope.pageno,false);
                }
            });
        }else if($stateParams.orderId !== undefined){
            $scope.findOne(true,function (err) {
                if(!err) {
                    $scope.offers = [];
                    $scope.pageno = 1;
                    $scope.total_count = 0;
                    $scope.itemsPerPage = 10;
                    $scope.find($scope.selectedIndex, $scope.pageno,false);
                }
            });
        }else {
            $scope.selectedIndex = 0;
            $scope.paginationId = $scope.tabs[ $scope.selectedIndex].type;

            $scope.offers = [];
            $scope.pageno = 1;
            $scope.total_count = 0;
            $scope.itemsPerPage = 10;
            $scope.changeSelectionTab($scope.paginationId);
        }

    };

    $scope.changeSelectionTabByIndex = function(index){
        var type;

        switch(index){
            case 0: type='ReceivedOffers';break;
            case 1:type = 'segments';break;
            case 2:type='contacts';break;
            /* case 3:type='MyOffers';break;*/
        }
        $scope.changeSelectionTab(type);
    };

    $scope.changeOfferContactSelection=function (contact) {
        $scope.selectedContact=contact;
        offersApi.getContactCompanyOffer(contact._id).then(function (response) {
            if(response.data.length>0) {
                $scope.offers = response.data[0].offers;
            }else{
                $scope.offers=[];
            }
        });

    };
    $scope.changeOfferSegmentSelection=function (segment) {
        $scope.selectedSegment=segment;
        offersApi.getSegmentOffers(segment._id).then(function (response) {
            if(response.data) {
                $scope.offers = response.data.offers;
            }else{
                $scope.offers=[];
            }
        },function (err) {
            $scope.error=err.data.message;
        });

    };


    $scope.changeSelectionTab = function (type) {
        $scope.offers=[];
        $scope.selectedIndex=getSelectedIndex(type);
        if (type === 'MyOffers') {
            $scope.selOffer = null;
            /* $scope.selectedIndex = 0;*/
            $scope.paginationId='MyOffers';
            getOffersByBusinessUnit($scope.authentication.companyBusinessUnits.defaultBusinessUnitId,$scope.selectedIndex,$scope.pageno,true);
        } else if (type === 'ReceivedOffers') {
            /* $scope.selectedIndex = 1;*/
            $scope.paginationId='ReceivedOffers';
            getOffersByBusinessUnit($scope.authentication.companyBusinessUnits.defaultBusinessUnitId,$scope.selectedIndex,$scope.pageno,true);
        } else if (type === 'PublicOffers') {
            $scope.selOffer = null;
            /* $scope.selectedIndex = 2;*/
            $scope.paginationId='PublicOffers';
            categoryService.findMainCategories().then(function (response) {
                $scope.categories = response.data;
            },function (response) {
                $scope.error = response.data.message;
            });
        }else if(type === 'segments'){
            offersApi.getPublicOffersBySegments().then(function (response) {
                $scope.segmentOffers=response.data;
                $scope.selectedSegment= $scope.segmentOffers[0];
                $scope.offers=  $scope.selectedSegment.offers;
            },function (err) {
                $scope.error=err.data.message;
            });

        }else if(type === 'contacts'){
            offersApi.getContactUserOffer().then(function (response) {
                $scope.contactoffers=response.data;
                if(response.data.length>0) {
                    $scope.selectedContact = $scope.contactoffers[0];
                    $scope.offers = $scope.selectedContact.offers;
                }
            },function (err) {

                $scope.error=err.data.message;
            });

        }
    };



}]);
