'use strict';

// Leads controller
angular.module('leads').controller('LeadsController', ['$scope', '$stateParams', '$location', 'Authentication', 'Leads',
	function($scope, $stateParams, $location, Authentication, Leads) {
		$scope.authentication = Authentication;

		// Create new Product
		$scope.create = function() {
			// Create new Lead object
			var lead = new Leads ({
				name: this.name,
				email:this.email,
				description: this.description
			});
			// Redirect after save
			lead.$save(function(response) {
				$location.path('leads/' + response._id);

				// Clear form fields
				$scope.clearFields();
				$scope.name = '';
			}, function(errorResponse) {
				$scope.error = errorResponse.data.message;
			});
		};

		//Clear Fields Function
		$scope.clearFields = function () {
			$scope.name = '';
			$scope.type = '';
			$scope.category = '';
			$scope.subCategory = '';
			$scope.unitSize = '';
			$scope.unitMeasure = '';
			$scope.description = '';
		};
		// Remove existing Product
		$scope.remove = function(lead) {
			if ( lead ) {
				lead.$remove();

				for (var i in $scope.leads) {
					if ($scope.leads [i] === lead) {
						$scope.leads.splice(i, 1);
					}
				}
			} else {
				$scope.lead.$remove(function() {
					$location.path('leads');
				});
			}
		};

		// Update existing Product
		$scope.update = function() {
			var lead = $scope.lead;
			lead.$update(function() {
				$location.path('leads/' + lead._id);
			}, function(errorResponse) {
				$scope.error = errorResponse.data.message;
			});
		};

		// Find a list of Products
		$scope.find = function() {
			$scope.leads = Leads.query();
		};

		// Find existing Product
		$scope.findOne = function() {
			$scope.lead = Leads.get({
				leadId: $stateParams.leadId
			});
		};
	}
]);
