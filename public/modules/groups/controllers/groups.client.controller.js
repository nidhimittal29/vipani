'use strict';

// Groups controller
var app = angular.module('groups');
app.controller('GroupsController', ['$scope', '$stateParams', '$location', 'Authentication', 'Groups',
	function($scope, $stateParams, $location, Authentication, Groups) {
		$scope.authentication = Authentication;

		// Create new Group
		$scope.create = function() {
			// Create new Group object
			var group = new Groups ({
				name: this.name,
				description:this.description
			});
			// Redirect after save
			group.$save(function(response) {
				$location.path('groups/' + response._id);

				// Clear form fields
				$scope.clearFields();
			}, function(errorResponse) {
				$scope.error = errorResponse.data.message;
			});
		};
		$scope.clearFields = function() {
			$scope.name = '';
			$scope.description = '';
		};

		$scope.reset = function(form) {
			if (form) {
				form.$setPristine();
				//form.$setUntouched();
			}
		};
		$scope.changeSelectedGroup = function(group){
			if($scope.allContacts===true) {
				$scope.allContacts = false;
			}
			$scope.selectedGroup = group;
		};
		$scope.mainContactView = function (changeStr){
			$scope.AllContactsString='All Contacts';
			if(typeof changeStr === 'string'){
				if(changeStr === 'groupEdit'){
					$scope.groupid=changeStr;
					$scope.allContacts = false;
					$scope.group=true;
					$scope.groupEditView=true;
				}else if(changeStr === 'groupDelete'){
					$scope.groupid=changeStr;
					$scope.allContacts = false;
					$scope.group=true;
					$scope.groupDelView=true;
				}else if(changeStr === 'group'){
					$scope.groupid=changeStr;
					$scope.allContacts = false;
					$scope.group=true;
					$scope.groupDelView=true;
				} else{
					$scope.allContacts = true;
					$scope.group=false;
				}

			}
		};

	/*	// Remove existing Group
		$scope.remove = function(group) {
			if ( group ) {
				group.$remove();

				for (var i in $scope.groups) {
					if ($scope.groups [i] === group) {
						$scope.groups.splice(i, 1);
					}
				}
			} else {
				$scope.group.$remove(function() {
					$location.path('groups');
				});
			}
		};*/

		// Update existing Group
		$scope.update = function() {
			var group = $scope.group;

			group.$update(function() {
				$location.path('groups/' + group._id);
			}, function(errorResponse) {
				$scope.error = errorResponse.data.message;
			});
		};

		// Find a list of Groups
		$scope.find = function() {
			$scope.groups = Groups.query();
		};

		// Find existing Group
		$scope.findOne = function() {
			$scope.group = Groups.get({
				groupId: $stateParams.groupId
			});
		};
	}
]);
/*app.directive('editGroup', function(){
	return {
		restrict:'E',
		templateUrl:'modules/groups/views/edit-group.client.view.html'
	};
});*/

app.directive('viewGroup', function(){
	return {
		restrict:'E',
		templateUrl:'modules/groups/views/view-group.client.view.html'
	};
});

app.directive('addGroup', function(){
	return {
		restrict:'E',
		templateUrl:'modules/groups/views/create-group.client.view.html'
	};
});
