'use strict';
var app = angular.module('core');
app.controller('HeaderController', ['$scope','$location','$http', 'Authentication', 'Menus', 'localStorageService', 'NotificationService','$mdSidenav','panelTrigger','LoginService','$mdDialog',
    function ($scope, $location,$http, Authentication, Menus, localStorageService, NotificationService , $mdSidenav,panelTrigger,LoginService,$mdDialog) {
        $scope.authentication = Authentication;
        $scope.isCollapsed = false;
        $scope.location=$location;
        $scope.menu = Menus.getMenu('topbar');
        $scope.headermenu = Menus.getMenu('headerbar');
        $scope.landmenu = Menus.getMenu('landbar');
        $scope.notification = false;
        $scope.profile = true;
        $scope.businessUnits=[];
        $scope.toggleSideNavigation = function (notification,id,val) {
            $scope.notification = notification;
            $scope.profile = !notification;
            /*	alert($scope.headermenu.items[0].title);*/
            if(!($scope.authentication &&$scope.authentication.user)){
                id='center';
            }
            if (val === 'toggle') {
                panelTrigger(id).toggle().then(function () {
                    /*console.log('Trigger is done');*/
                });
            }else if (val === 'open') {
                panelTrigger(id).open().then(function () {
                    /*console.log('Trigger is done');*/
                });
            }else if (val === 'close') {
                panelTrigger(id).close().then(function () {
                    /*console.log('Trigger is done');*/
                });
            }else if (val === 'isOpen') {
                panelTrigger(id).isOpen().then(function () {
                    /*console.log('Trigger is done');*/
                });
            }
        };
        $scope.toggleLeft = function(navId){
            panelTrigger(navId).toggle().then(function(){

            });
        };
        $mdDialog.cancel();

        $scope.dashboardData = function () {
            /* if ($scope.authentication && $scope.authentication.user && $scope.authentication.token) {
                 LoginService.homeData($scope);
             }*/
            if ($scope.authentication && $scope.authentication.user && $scope.authentication.token) {
                $http.get('/users/myhome')
                    .then(function (rs) {
                        /* defer.resolve(response.data);*/
                        var response=rs.data;
                        $scope.notifications = response.notifications;
                        $scope.orders = response.orders;
                        $scope.offers = response.offers;
                        $scope.todos = response.todos;
                        $scope.businessUnits = $scope.authentication.companyBusinessUnits.businessUnits;
                        if($scope.businessUnits && $scope.businessUnits.length>0) {
                            $scope.businessUnitName = $scope.businessUnits[0].businessUnit.name;
                            $scope.busUnitId = $scope.businessUnits[0].businessUnit._id;
                        }else{
                            $scope.businessUnitName='';
                            $scope.busUnitId='';
                        }

                    },function (response) {
                        localStorageService.remove('nvipanilogindata');
                        Authentication.token = null;
                        Authentication.user = null;
                        Authentication.deviceid = null;
                        $location.path('/');
                        $scope.authentication.token = null;
                        $scope.authentication.user = null;
                        $scope.authentication.deviceid = null;
                        $scope.error = response.data.message;

                    });
            }
        };
        $scope.clearNotifications=function (notification) {
            if ($scope.authentication && $scope.authentication.user && $scope.authentication.token) {
                var parms={};
                if(notification.type){
                    parms={type:notification.type};
                }
                if(notification._id){
                    parms.notificationId=notification._id;
                }
                NotificationService.notificationClear($scope,parms).then(function (rs) {
                    $scope.notifications = rs.data;
                    $scope.changeUrlNotification(notification);
                },function (err) {
                    $scope.error=err.data.message;
                });
            }
        };
        $scope.isOpenRight = function(){
            return $mdSidenav('right').isOpen();
        };

        function buildToggler(navID) {
            return function() {
                // Component lookup should always be available since we are not using `ng-if`
                $mdSidenav(navID)
                    .toggle()
                    .then(function () {
                    });
            };
        }

        $scope.toggleCollapsibleMenu = function () {
            $scope.isCollapsed = !$scope.isCollapsed;
        };
        // Collapsing the menu after navigation
        $scope.$on('$stateChangeSuccess', function () {
            $scope.isCollapsed = false;
        });

        $scope.changeUrl=function (url) {
            $location.path(url);
        };


        $scope.changeUrlNotification=function (notification) {
            if(notification.type==='Offer' || notification.type==='OfferStatus'){
                $location.path('offers/'+notification.source.offer);
            }else if(notification.type==='Order' || notification.type==='OrderStatus'){
                $location.path('orders/'+notification.source.order);
            }else if(notification.type==='OfferComment'){
                $location.path('offers/'+notification.source.offer+'/'+notification.source.message);
            }else if(notification.type === 'OrderComment'){
                $location.path('orders/'+notification.source.order+'/'+notification.source.message);
            }
        };
        // Find a list of Groups
        $scope.findNotifications = function () {
            if ($scope.authentication && $scope.authentication.user && $scope.authentication.token) {
                NotificationService.notifications($scope);
            }
        };
        $scope.signout = function () {
            LoginService.logout($scope);

        };

        $scope.closeright = function (notification) {
            // Component lookup should always be available since we are not using `ng-if`
            if(notification)
                $scope.clearNotifications(notification);
            $mdSidenav('right').close()
                .then(function () {
                });
        };
        panelTrigger('header-sidenav').close().then(function (){

        });
    }
]);
app.directive('headBar', function(){
    return {
        restrict:'E',
        scope:{
            nvipanipath: '=nvipanipath',
            isbusinessunitslist: '=isbusinessunitslist',
            changeBusinessUnit: '&'
        },
        templateUrl:'modules/core/views/head-bar.nvc.client.html'
    };
});
