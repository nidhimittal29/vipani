'use strict';
var app=angular.module('core');

app.controller('HomeController', ['$scope', 'Authentication', '$http', '$location', '$stateParams', 'localStorageService',
    function ($scope, Authentication, $http, $location, $stateParams, localStorageService) {
		// This provides Authentication context.
		$scope.authentication = Authentication;
		$scope.location=$location;
		$scope.tag='New';
		$scope.appName='nVipani';
		if(!$scope.authentication.user && $stateParams && $stateParams.registerToken && $stateParams.registerToken.contains('userreg')){
			$location.path('/');
		}
		$scope.changeType= function (tagVal) {
			$scope.tag=tagVal;
		};
		$scope.dashboardData = function () {
          /*  var defer = $q.defer();*/
			if ($scope.authentication && $scope.authentication.user && $scope.authentication.token) {
				$http.get('/users/myhome')
					.then(function (rs) {
                       /* defer.resolve(response.data);*/
                       var response=rs.data;
						$scope.notifications = response.notifications;
						$scope.orders = response.orders;
						$scope.offers = response.offers;
						$scope.todos = response.todos;

					},function (response) {
                    localStorageService.remove('nvipanilogindata');
                    Authentication.token = null;
                    Authentication.user = null;
                    Authentication.deviceid = null;
                    $location.path('/');
                    $scope.authentication.token = null;
                    $scope.authentication.user = null;
                    $scope.authentication.deviceid = null;
					$scope.error = response.data.message;

				});
			}
		};
	}
]);
app.controller('BottomImageController', ['$scope', 'Authentication','$location',
	function($scope, Authentication,$location) {
		// This provides Authentication context.
		$scope.authentication = Authentication;
		$scope.location=$location;
	}

]);

app.controller('ContactPublicController', ['$scope', 'Leads','$location',
	function($scope, Leads,$location) {
		$scope.appName='nVipani';
		// This provides Authentication context.
		$scope.reset = function(form) {
			$scope.error = null;
			if (form) {
				form.$setPristine();
				//form.$setUntouched();
			}

		};

		$scope.create = function() {
			// Create new Lead object
			var lead = new Leads ({
				name: this.name,
				email:this.email,
				description: this.description
			});
			// Redirect after save
			lead.$save(function(response) {
				$scope.name=null;
				// Clear form fields
				/*$scope.reset('contactform');*/
				$scope.emailsent = 'Thanks for your interest on nVipani, Soon we will get back to you';
				$scope.email = null;
				$scope.description=null;
			}, function(errorResponse) {
				$scope.error = errorResponse.data.message;
			});
		};
	}

]);

app.directive('flexSlider', function ($timeout) {
    return {
        link: function (scope, element, attrs) {
            $timeout(function(){
                element.flexslider({
                    animation: 'slide'
                });
            });
        }
    };
});

app.directive('carousel', function () {

	return {
		link: function (scope, element, attrs) {

			element.flexslider({
				animation: 'slide',
				controlNav: false,
				animationLoop: true,
				slideshow: true,
				itemWidth: 210,
				itemMargin: 5,
               /* autoplay: true,*/
				asNavFor: '#slider'
			});
		}
	};


});
/*

app.directive('flexSliders', function () {

    return {
        link: function (scope, element, attrs) {

            element.flexslider({
                animation: 'slide',
                controlNav: false,
                animationLoop: true,
                slideshow: true,
                itemWidth: 4000,
                itemMargin: 1,
                autoplay: true,
                asNavFor: '#slider'
            });
        }
    };


});*/
app.directive('register', function(){
	return {
		restrict:'E',
		templateUrl:'modules/core/views/landing-auth.nvc.client.html'
	};
});
/*app.directive('bunitSelect', function(){
    return {
        restrict:'E',
        templateUrl:'modules/companies/views/bunitselect.nvc.client.html',
        scope:{
            selectionPage: '=type'
        },
        controller:['$scope','$location','CompanyService','BussinessProducts', 'Authentication','inventoryProduct','contactsQuery',function ($scope,$location,CompanyService,BusinessProducts,Authentication,inventoryProduct,contactsQuery) {
            $scope.user=Authentication.user;
            $scope.company={};
            $scope.location=$location;
            $scope.findBunits = function(){
                CompanyService.getBusinessUnits($scope.user.company).then(function (response) {
                    $scope.company.businessUnits = response.data.businessUnits;
                });
            };
            var getInventoriessByBusinessUnit = function(selectedBusinessUnit){
                $scope.businessUnit = selectedBusinessUnit;
                inventoryProduct.getInventoriesByBUnit(selectedBusinessUnit).then(function(response){
                    //returned response
                    /!* var retVal = {};
                     retVal.inventories=businessUnit.inventories;
                     retVal.total_count=businessUnit.inventories.length;
                     retVal.businessUnit = businessUnit._id;*!/

                    if(response.data) {
                        /!*inventoryProduct.setInventories($scope,{inventories:response.data.inventories,total_count:response.data.total_count});*!/
                        if($scope.location.$$url==='/inventories/selection'){
                            $scope.panelChange('CreateOffer');
                        }
                    }
                });
            }
            $scope.panelChange=function(changeStr){
                if(changeStr==='CreateOffer'){
                    $scope.selOffer=Offers;
                    /!*$scope.selOffer.offerType='Sell';
                    $scope.selOffer.isPublic=true;
                    $scope.selOffer.validTill=new Date();*!/
                    if($scope.selOffer.products && $scope.selOffer.products.length>0) {
                        $scope.inventories=$scope.selOffer.products;
                        $scope.selOffer.products = angular.copy($scope.filterSelectedProductsOffer($scope.selOffer.offerType === 'Sell', true));
                    }else{
                        $scope.selOffer.products = angular.copy($scope.filterSelectedProductsOffer($scope.selOffer.offerType === 'Sell', true));
                    }
                    /!*var useFullScreen = ($mdMedia('sm') || $mdMedia('xs'));*!/
                    $scope.offerTypes = [{value: 'Sell', text: 'Seller'}, {value: 'Buy', text: 'Buyer'}];
                    contactsQuery.findContactsVsGroups($scope);
                    $scope.showOfferProducts=true;
                    $scope.viewAllProduct=false;

                    /!*  $mdDialog.show({
                          template:'<md-dialog><add-offer-product></add-offer-product></md-dialog>',
                          targetEvent: ev,
                          scope: $scope,
                          clickOutsideToClose: false,
                          fullscreen: useFullScreen,
                          preserveScope: true
                      });*!/

                }
            }
            $scope.checkChange = function(item) {
                switch($scope.selectionPage){
                    case 'orders':break;
                    case 'offers':break;
                    case 'inventories':getInventoriessByBusinessUnit(item.businessUnit.businessUnit);break;
                    case 'contacts':break;
                }

            };
        }]

    };
});*/
app.directive('action', function(){
    return {
        restrict:'E',
        templateUrl:'modules/core/views/action-bar.nvc.client.html',
        scope:{
            nvactions: '=nvactions',
            filterSummary:'&',
			page: '=page'
        },
        controller: function ($scope) {
            $scope.setActive = function (key,value) {
                /*if($scope.selectedKey===undefined || ($scope.selectedKey && $scope.selectedKey !== key)) {*/
               if(value>0) {
                    $scope.filterSummary({filterKey: key, filterCount: value, toggle: $scope.selectedKey !== key});
                   if($scope.selectedKey && $scope.selectedKey === key) {
                        delete $scope.selectedKey;
                    }else {
                       $scope.selectedKey = key;
                   }
                }
            };
        }
    };
});
