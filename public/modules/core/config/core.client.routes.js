'use strict';

// Setting up route
angular.module('core').config(['$stateProvider', '$urlRouterProvider',
	function($stateProvider, $urlRouterProvider) {
		// Redirect to home view when route not found
		$urlRouterProvider.otherwise('/');

		// Home state routing
		$stateProvider.
		state('home', {
			url: '/',
			templateUrl: 'modules/users/views/authentication/signupc.client.viewnew2.html'
		}).
		/*state('signin', {
			url: '/signin',
			templateUrl: 'modules/core/views/homenew.client.view.html'
		}).*/
		state('homeRegistration', {
			url: '/userreg/:registerToken',
			templateUrl: 'modules/core/views/homenew.client.view.html'
		}).
        state('privacypolicy', {
            url: '/privacypolicy',
            templateUrl: 'modules/core/views/privacypolicy.html'
        }).
        state('terms', {
            url: '/terms',
            templateUrl: 'modules/core/views/termsandconditions.html'
        }).
		state('contact', {
			url: '/contact',
			templateUrl: 'modules/core/views/contact.html'
		}).state('team', {
			url: '/team',
			templateUrl: 'modules/core/views/team.html'
		}).
		state('about', {
			url: '/about',
			templateUrl: 'modules/core/views/about.html'
		}).
		state('font', {
			url: '/font',
			templateUrl: 'modules/core/views/font.html'
		}).
        state('index', {
            url: '/index',
            templateUrl: 'modules/core/views/index.html'
        }).
		state('register', {
			url: '/register',
			templateUrl: 'modules/users/views/authentication/signupc.client.viewnew2.html'
        }).state('enterotp', {
            url: '/enterotp/:statusToken',
            templateUrl: 'modules/core/views/signin.client.viewnew2.html'
        }).state('resendotp', {
				url: '/resendotp',
				templateUrl: 'modules/core/views/signin.client.viewnew2.html'
			}).
		state('careers', {
			url: '/careers',
			templateUrl: 'modules/core/views/careers.html'
		}).
        state('businesssegments',{
            url:'/businesssegments',
            templateUrl: 'modules/categories/views/select-category.client.viewnew.html'
        });
	}
]);
