'use strict';

// Configuring the Articles module
angular.module('core',[]).run(['Menus',
	function(Menus) {
		Menus.addMenuItem('headerbar', 'Home', '', 'home',false, true);
		/*Menus.addMenuItem('headerbar', 'ABOUT US', 'about', 'dropdown', '(/about)?(/careers)?',true);
		Menus.addSubMenuItem('headerbar','about', 'Who We are','about',false,true);
		Menus.addSubMenuItem('headerbar', 'about', 'Team', 'team', false, true);
		Menus.addSubMenuItem('headerbar', 'about','Careers', 'careers',false,true);*/
        Menus.addMenuItem('headerbar', 'About', '#about', 'about',false, true);
        Menus.addMenuItem('headerbar', 'Team', '#team', 'team',false, true);
        Menus.addMenuItem('headerbar', 'Testimonials', '#testimonials', 'Testimonials',false, true);
		Menus.addMenuItem('headerbar', 'Contact', '#contact', 'contact',false,true);

        Menus.addMenuItem('headerbar', 'Sign-Up', 'register', 'register',false,true);
        Menus.addMenuItem('headerbar', 'Login', 'signin', 'signin',false,true);
        Menus.addMenuItem('landbar', 'Sign-Up', 'register', 'register',false,true);
        Menus.addMenuItem('landbar', 'Login', 'signin', 'signin',false,true);
	}
]);
