'use strict';

//Menu service used for managing  menus
var app=angular.module('core');
app.directive('nvCheckBox', ['$log', '$filter', '$timeout', function($log, $filter, $timeout) {

    return {

        restrict: 'EA',//E-element & A - attribute

        template:

        '<md-checkbox class="md-primary" '+

        'id="all" name="all" ng-model="all" ng-show="orginallist.length>1" aria-label="selectAll" ng-disabled="disabled" md-indeterminate="indeterminate" ng-change="selectAll()"/> ' +

        '{{label}}</md-checkbox> ',

        scope: {  //isolate scope created

            selectedlist: '=',

            orginallist: '=',

            disabled: '=',

            value: '@',

            label: '@',

            all: '@',

            sortBy: '@'

        },

        link: function($scope, element, attrs) {

            $scope.checkbox = {};
            $scope.selectedlist = [];

            $scope.all = false; //set 'All' checkbox to false

            // we need to check the disabled flag for select all.
            function selectedList(list){
                if(list) {
                    return list.filter(function (eachList) {
                        return eachList.selected;
                    }).length;
                }else{
                    return 0;
                }
            }

            //function called on click of check box
            $scope.toggle = function(index) {

                var item = $scope.list[index];
                if(!$scope.selectedlist){
                    $scope.selectedlist = [];
                }
                var i;
                if($scope.selectedlist.length)

                   i = $scope.selectedlist.length > 0 ? $scope.selectedlist.indexOf(item) : -1;

                item.selected = !item.selected;

                if (!item.selected) {

                    $scope.selectedlist.splice(i, 1);//remove item if unselected

                    $scope.all = false;//make 'All' to uncheck too

                } else if (item.selected) {

                    $scope.selectedlist.push(item);//add item if selected

                }
                if(selectedList($scope.selectedlist)===$scope.orginallist.length){
                   /* $scope.checkbox.indeterminate=false;*/
                    $scope.all = true;
                    angular.element(document.querySelector('#all')).setValue(true);

                }else{
                    $scope.all = false;
                    if(selectedList($scope.selectedlist)>0){
                        $scope.indeterminate = true;
                    }else {
                        $scope.indeterminate = false;
                    }

                }

            };

            //function called when 'All' checkbox is selected
            $scope.selectAll = function() {

                var totalList = $scope.orginallist;


                //if selected add all items
                //if unselected remove all items from selected list
                angular.forEach(totalList, function(item) {

                    item.selected = $scope.all;

                    if (item.selected) {

                        $scope.selectedlist.push(item);

                    } else {

                        $scope.selectedlist = [];

                    }

                });
            };

            //always watch my source list if it has been modified and update back..
            $scope.$watch('orginallist', function(value) {

                //sort accordingly..
                value = $filter('orderBy')(value, $scope.sortBy);

                $scope.selectedlist = [];

                /*if (angular.isArray(value)) {

                    angular.forEach(value, function(item) {

                        $scope.list.push(item);

                    });

                }*/
                if(value && $scope.orginallist && selectedList(value)===$scope.orginallist.length){
                    $scope.indeterminate=false;
                    $scope.all=true;
                    $scope.value=true;/*

                    angular.element(document.querySelector('#all')).setValue(true);*/

                }else{
                    $scope.all = false;
                    if(value && $scope.orginallist && angular.isArray($scope.orginallist) && selectedList(value)>0){
                        $scope.indeterminate = true;
                    }else{
                        $scope.indeterminate=false;
                    }
                }




            }, true);
            //clear 'All' checkbox value if all items are de selected
            $scope.$watch('selectedlist', function(value) {
                if(value && $scope.orginallist && angular.isArray($scope.orginallist) && angular.isArray(value) && selectedList(value)===$scope.orginallist.length){
                    $scope.indeterminate=false;
                    $scope.all = true;

                }else{
                    $scope.all = false;
                    if(value && $scope.orginallist && angular.isArray(value) && $scope.selectedlist.length>0){
                        $scope.indeterminate = true;
                    }else{
                        $scope.indeterminate=false;
                    }
                }

            }, true);

        }

    };

}]);
