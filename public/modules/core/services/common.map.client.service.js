'use strict';

//Menu service used for managing  menus
var app=angular.module('core');
app.directive('geocoder', function() {
    return {
        restrict: 'E',
        replace: true,
        templateUrl: '../../../modules/inventories/views/geo-address.client.view.html',
        scope: {city:'=', placeholder:'='},
        link: function($scope, element, attrs){

            var geocoder = new google.maps.Geocoder();
            $scope.active = 0;
            $scope.results = [];
            $scope.status = 'OK';
            $scope.is_visible = false;
            $scope.set_active = function (index) {$scope.active = index;};

            $scope.get_results = function (text) {
                if (text.length >= 3) {
                    geocoder.geocode({'address': text},
                        function (results, status) {
                            $scope.results = results;
                            $scope.status = status;

                            $scope.$apply(function() {
                                $scope.results = results;

                            });
                        });
                }
                else{
                    $scope.results.length = 0;
                }
            };
            $scope.reverse_location = function() {
                function success(pos) {
                    var latlng = new google.maps.LatLng(pos.coords.latitude, pos.coords.longitude);
                    geocoder.geocode({'location': latlng},
                        function (results, status) {
                            var r = get_chunk(results, 'locality');
                            $scope.city = serialize_loc(r);
                            $scope.address = r.formatted_address;
                            $scope.$apply();
                        });
                }
                if (navigator.geolocation) {
                    navigator.geolocation.getCurrentPosition(success);
                }
                else {
                    console.log("Geolocation is not supported by your browser.");
                }

            };
            $scope.input_blur = function(){
                if($scope.city){ $scope.results.length = 0 }
                $scope.is_visible = false;
            };
            $scope.select_item = function (index) {
                if ($scope.results.length > 0) {
                    $scope.city = serialize_loc($scope.results[index]);
                    $scope.address = $scope.results[index].formatted_address;
                    $scope.results.length = 0;
                }
            };
            function remove_obj_prop(obj){
                for(var i=1; i < arguments.length; i++){
                    if(arguments[i] in obj){ delete obj[arguments[i]]}
                }
            }
            function get_chunk(arr, seek){
                function filter_by(value) {
                    return value.types[0] == seek || value.types[1] == seek;
                }
                var fil = arr.filter(filter_by)[0];
                if(fil.types[0] !='country'){ delete fil.short_name }
                var chunk = {};
                for(var key in fil){
                    if(key !='types'){ chunk[key] = fil[key] }
                }
                return chunk;
            }
            function serialize_loc(loc) {
                var addresses = loc.address_components;
                var city = get_chunk(addresses, 'locality');
                city.region = get_chunk(addresses, 'administrative_area_level_1');
                city.region.country = get_chunk(addresses, 'country');
                city.longitude = loc.geometry.location.lng();
                city.latitude = loc.geometry.location.lat();
                return city;
            }
        }
    };
});
/*app.directive('myMap', function() {
    // directive link function
    var link = function(scope, element, attrs) {
        var map, infoWindow;
        var markers = [];

        // map config
        var mapOptions = {
            center: new google.maps.LatLng(50, 2),
            zoom: 4,
            mapTypeId: google.maps.MapTypeId.ROADMAP,
            scrollwheel: false
        };

        // init the map
        function initMap() {
            if (map === void 0) {
                map = new google.maps.Map(element[0], mapOptions);
            }
        }

        // place a marker
        function setMarker(map, position, title, content) {
            var marker;
            var markerOptions = {
                position: position,
                map: map,
                title: title,
                icon: 'https://maps.google.com/mapfiles/ms/icons/green-dot.png'
            };

            marker = new google.maps.Marker(markerOptions);
            markers.push(marker); // add marker to array

            google.maps.event.addListener(marker, 'click', function () {
                // close window if not undefined
                if (infoWindow !== void 0) {
                    infoWindow.close();
                }
                // create new window
                var infoWindowOptions = {
                    content: content
                };
                infoWindow = new google.maps.InfoWindow(infoWindowOptions);
                infoWindow.open(map, marker);
            });
        }

        // show the map and place some markers
        initMap();

        setMarker(map, new google.maps.LatLng(51.508515, -0.125487), 'London', 'Just some content');
        setMarker(map, new google.maps.LatLng(52.370216, 4.895168), 'Amsterdam', 'More content');
        setMarker(map, new google.maps.LatLng(48.856614, 2.352222), 'Paris', 'Text here');
    };

    return {
        restrict: 'A',
        template: '<div id="gmaps"></div>',
        replace: true,
        link: link
    };
});*/
