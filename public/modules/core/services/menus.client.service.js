'use strict';

//Menu service used for managing  menus
var app=angular.module('core');
app.service('Menus', [

    function() {
        // Define a set of default roles
        this.defaultRoles = ['*'];

        // Define the menus object
        this.menus = {};

        // A private function for rendering decision
        var shouldRender = function(user) {
            if (user) {
                if (!!~this.roles.indexOf('*')) {
                    return true;
                } else {
                    for (var userRoleIndex in user.roles) {
                        for (var roleIndex in this.roles) {
                            if (this.roles[roleIndex] === user.roles[userRoleIndex]) {
                                return true;
                            }
                        }
                    }
                }
            } else {
                return this.isPublic;
            }

            return false;
        };

        // Validate menu existance
        this.validateMenuExistance = function(menuId) {
            if (menuId && menuId.length) {
                if (this.menus[menuId]) {
                    return true;
                } else {
                    throw new Error('Menu does not exists');
                }
            } else {
                throw new Error('MenuId was not provided');
            }

            return false;
        };

        // Get the menu object by menu id
        this.getMenu = function(menuId) {
            // Validate that the menu exists
            this.validateMenuExistance(menuId);

            // Return the menu object
            return this.menus[menuId];
        };

        // Add new menu object by menu id
        this.addMenu = function(menuId, isPublic, roles) {
            // Create the new menu
            this.menus[menuId] = {
                isPublic: isPublic || false,
                roles: roles || this.defaultRoles,
                items: [],
                shouldRender: shouldRender
            };

            // Return the menu object
            return this.menus[menuId];
        };

        // Remove existing menu object by menu id
        this.removeMenu = function(menuId) {
            // Validate that the menu exists
            this.validateMenuExistance(menuId);

            // Return the menu object
            delete this.menus[menuId];
        };

        // Add menu item object
        this.addMenuItem = function(menuId, menuItemTitle, menuItemURL, menuItemType, menuItemUIRoute, isPublic, roles, position) {
            // Validate that the menu exists
            this.validateMenuExistance(menuId);

            // Push new menu item
            this.menus[menuId].items.push({
                title: menuItemTitle,
                link: menuItemURL,
                menuItemType: menuItemType || 'item',
                menuItemClass: menuItemType,
                uiRoute: menuItemUIRoute || ('/' + menuItemURL),
                isPublic: ((isPublic === null || typeof isPublic === 'undefined') ? this.menus[menuId].isPublic : isPublic),
                roles: ((roles === null || typeof roles === 'undefined') ? this.menus[menuId].roles : roles),
                position: position || 0,
                items: [],
                shouldRender: shouldRender
            });

            // Return the menu object
            return this.menus[menuId];
        };

        // Add submenu item object
        this.addSubMenuItem = function(menuId, rootMenuItemURL, menuItemTitle, menuItemURL, menuItemUIRoute, isPublic, roles, position) {
            // Validate that the menu exists
            this.validateMenuExistance(menuId);

            // Search for menu item
            for (var itemIndex in this.menus[menuId].items) {
                if (this.menus[menuId].items[itemIndex].link === rootMenuItemURL) {
                    // Push new submenu item
                    this.menus[menuId].items[itemIndex].items.push({
                        title: menuItemTitle,
                        link: menuItemURL,
                        uiRoute: menuItemUIRoute || ('/' + menuItemURL),
                        isPublic: ((isPublic === null || typeof isPublic === 'undefined') ? this.menus[menuId].items[itemIndex].isPublic : isPublic),
                        roles: ((roles === null || typeof roles === 'undefined') ? this.menus[menuId].items[itemIndex].roles : roles),
                        position: position || 0,
                        shouldRender: shouldRender
                    });
                }
            }

            // Return the menu object
            return this.menus[menuId];
        };

        // Remove existing menu object by menu id
        this.removeMenuItem = function(menuId, menuItemURL) {
            // Validate that the menu exists
            this.validateMenuExistance(menuId);

            // Search for menu item to remove
            for (var itemIndex in this.menus[menuId].items) {
                if (this.menus[menuId].items[itemIndex].link === menuItemURL) {
                    this.menus[menuId].items.splice(itemIndex, 1);
                }
            }

            // Return the menu object
            return this.menus[menuId];
        };

        // Remove existing menu object by menu id
        this.removeSubMenuItem = function(menuId, submenuItemURL) {
            // Validate that the menu exists
            this.validateMenuExistance(menuId);

            // Search for menu item to remove
            for (var itemIndex in this.menus[menuId].items) {
                for (var subitemIndex in this.menus[menuId].items[itemIndex].items) {
                    if (this.menus[menuId].items[itemIndex].items[subitemIndex].link === submenuItemURL) {
                        this.menus[menuId].items[itemIndex].items.splice(subitemIndex, 1);
                    }
                }
            }

            // Return the menu object
            return this.menus[menuId];
        };

        //Adding the topbar menu
        this.addMenu('topbar');
        this.addMenu('headerbar');
        this.addMenu('landbar');
    }
]);

app.factory('panelTrigger',['$mdSidenav','$q', function($mdSidenav,$q) {
    return function (handle) {
        var nav=$mdSidenav(handle);
        var errorMsg = 'SideNav ' + handle + ' is not available!';
        return  {

            isOpen: function () {
                return nav && nav.isOpen();
            },
            isLockedOpen: function () {
                return nav && nav.nav();
            },

            toggle: function () {
                return nav ? nav.toggle() : $q.reject(errorMsg);
            },
            open: function () {
                return nav ? nav.open() : $q.reject(errorMsg);
            },
            close: function () {
                return nav ? nav.close() : $q.reject(errorMsg);
            },
            /*then: function (callbackFn) {
                var promise = nav ? $q.when(nav) : waitForInstance();
                return promise.then(callbackFn || angular.noop);
            }*/
        };
    };
}]);
app.directive('scroll', function ($window, $location) {
    return function ($scope, element, attr) {
        angular.element($window).bind('scroll', function () {
            if(!$scope.authentication.user) {
                var winHeight = $window.innerHeight;
                $scope.offtop = angular.element(document.querySelector('.nvc-panel2'));
                if($scope.offtop.length) {
                    if (this.pageYOffset >= $scope.offtop[0].offsetTop - winHeight / 2) {
                        $scope.offtop.addClass('animate-slideIn');
                    }
                }
                $scope.offtop = angular.element(document.querySelector('.nvc-panel3'));
                if($scope.offtop.length) {
                    if (this.pageYOffset >= $scope.offtop[0].offsetTop - winHeight / 2) {
                        $scope.offtop.addClass('animate-slideIn');
                    }
                }
                $scope.offtop = angular.element(document.querySelector('.nvc-panel4'));
                if($scope.offtop.length) {
                    if (this.pageYOffset >= $scope.offtop[0].offsetTop - winHeight / 2) {
                        $scope.offtop.addClass('animate-slideIn');
                    }
                }
            }
        });
    };
});

app.directive('loading', ['$http', function ($http) {
    return {
        restrict: 'A',
        link: function (scope, elm, attrs) {
            scope.isPending = function () {
                return $http.pendingRequests.length > 0;
            };
            scope.$watch(scope.isPending, function (v) {
                if (v) {
                    elm.show();
                } else {
                    elm.hide();
                }
            });
        }
    };
}]);
app.factory('NotificationService',['LoginService','$http','localStorageService','Notifications',function (LoginService,$http,localStorageService,Notifications) {
    return {
        notifications:function (scope) {
            $http.get('notifications').then(function (results) {
                scope.notifications=results.data;
            },function (err) {
                scope.error=err.data.message;
                if(err.data.message==='Session Expired.'){
                    LoginService.logout(scope);
                }

            });

        },
        notificationClear:function (scope,parms) {
            return $http.get('notificationsclear',{
                params:parms
            });/*
                .then(function (rs) {
                    scope.notifications = rs.data;
                },function (err) {
                    scope.error=err.data.message;
                });*/
        }
    };
}]);
app.factory('LoginService', ['$http','$location','Authentication', 'localStorageService','deviceDetector','uuid', function($http,$location,Authentication,localStorageService,deviceDetector,uuid) {
    /* console.log('Loaded Login Service');*/
    return {
        /*
        determines if user is logged out with local storage Service.
         */

        logout: function(scope) {
            $http.get('/auth/signout',{token: scope.authentication.token,
                deviceid: scope.authentication.deviceid}).then(function (response) {
                Authentication.token = null;
                Authentication.user = null;
                Authentication.deviceid = null;
                Authentication.companyBusinessUnits=null;
                scope.authentication.token = null;
                scope.authentication.user = null;
                scope.authentication.deviceid = null;
                localStorageService.remove('nvipanilogindata');

                // Redirect to signin page
                $location.path('/');
            },function(response) {
                localStorageService.remove('nvipanilogindata');
                Authentication.token = null;
                Authentication.user = null;
                Authentication.deviceid = null;
                Authentication.companyBusinessUnits=null;
                $location.path('/');
                scope.authentication.token = null;

                scope.authentication.user = null;
                scope.authentication.deviceid = null;
                scope.signouterror = response.data.message;

            });
            return scope;
        },
        homeData:function (scope) {
            $http.get('/users/myhome')
                .then(function (rs) {
                    var response=rs.data;
                    scope.notifications = response.notifications;
                    scope.orders = response.orders;
                    scope.offers = response.offers;
                    scope.todos = response.todos;
                },function(response) {
                    localStorageService.remove('nvipanilogindata');
                    Authentication.token = null;
                    Authentication.user = null;
                    Authentication.deviceid = null;
                    $location.path('/');
                    scope.authentication.token = null;
                    scope.authentication.user = null;
                    scope.authentication.deviceid = null;
                    scope.error = response.data.message;
                });
            return scope;
        },
        login:function (scope) {
            scope.credentials.devicedescription = deviceDetector.raw.userAgent;
            scope.credentials.devicename = deviceDetector.os_version;

            scope.credentials.deviceid = uuid.v4();

            $http.post('/auth/signin', scope.credentials).then(function (res) {
                //If successful we assign the response to the global user model
                //$window.localStorage.token= response.token;
                var response = res.data;
                scope.authentication.token = response.token;
                scope.authentication.companyBusinessUnits = response.businessUnits;
                if(response.businessUnits && response.businessUnits.businessUnits.length>0) {
                    var defaultBusinessUnit=response.businessUnits.businessUnits.filter(function (eachUnit) {
                        return eachUnit.defaultBusinessUnit === true;
                    });
                    scope.authentication.companyBusinessUnits.defaultBusinessUnitId=defaultBusinessUnit[0].businessUnit._id;
                }else {
                    scope.authentication.companyBusinessUnits.defaultBusinessUnitId='';
                }
                if (response.companySegments)
                    scope.authentication.segments = response.companySegments;
                if (response.categories)
                    scope.authentication.categories = response.categories;
                if (response.registrationCategory)
                    scope.registrationCategory = response.registrationCategory;
                if (response.segments)
                    scope.segments = response.segments;

                scope.authentication.deviceid = scope.credentials.deviceid;

                $http.post('/users/me', {token: scope.authentication.token})
                    .then(function (us) {
                        var userResponse = us.data;
                        localStorageService.set('nvipanilogindata', {
                            token: scope.authentication.token,
                            deviceid: scope.authentication.deviceid,
                            user: userResponse,
                            companyBusinessUnits:scope.authentication.companyBusinessUnits
                        });
                        scope.authentication.user = userResponse;
                        /*this.homeData(scope);*/
                    }, function (userResponse) {
                        localStorageService.remove('nvipanilogindata');
                        Authentication.token = null;
                        Authentication.user = null;
                        Authentication.deviceid = null;
                        $location.path('/');
                        scope.authentication.token = null;
                        scope.authentication.user = null;
                        scope.authentication.deviceid = null;
                        scope.error = userResponse.data.message;
                    });
                /*	// And redirect to the index page
                 */
            }, function (signResponse) {
                scope.error = signResponse.data.message;
                scope.userstatus = signResponse.data.userstatus;
            });
        }


    };
}]);
