'use strict';

// Categories controller
var app=angular.module('categories');
app.controller('CategoriesController', ['$scope', '$stateParams', '$location', 'Authentication', 'Categories','categoryService','Companies','$mdDialog','alert','$mdToast',
    function ($scope, $stateParams, $location, Authentication, Categories,categoryService,Companies,$mdDialog,alert,$mdToast) {
        $scope.authentication = Authentication;

        // Create new Category
        $scope.create = function () {
            // Create new Category object
            var category = new Categories({
                name: this.name
            });

            // Redirect after save
            category.$save(function (response) {
                $location.path('products/create');

                // Clear form fields
                $scope.name = '';
            }, function (errorResponse) {
                $scope.error = errorResponse.data.message;
            });
        };

        $scope.panelChange = function () {
            $location.path('products/create');

            // Clear form fields
            $scope.name = '';
        };

        // Remove existing Category
        $scope.remove = function (category) {
            if (category) {
                category.$remove();

                for (var i in $scope.categories) {
                    if ($scope.categories [i] === category) {
                        $scope.categories.splice(i, 1);
                    }
                }
            } else {
                $scope.category.$remove(function () {
                    $location.path('categories');
                });
            }
        };
        $scope.selectedCategory=function (categories,selcategoires) {
            categories.filter(function (cat) {
                if(cat.isChecked===true)
                    selcategoires.push({category:cat._id});
                else if(cat.children && cat.children.length>0) {
                    $scope.selectedCategory(cat.children,selcategoires);
                }
            });
            return selcategoires;
        };

        $scope.findCategory = function (id,parentIndex,index) {
            if(id) {
                categoryService.findCategoryById(id).then(function (response) {
                    if (response.data.type === 'MainCategory') {
                        $scope.subCategories1 = response.data.children;
                        $scope.selectedMainCategory=response.data;
                        $scope.selectedSubCategory1Category=null;
                        $scope.selectedSubCategory=null;
                        $scope.subCategories2 = [];
                        $scope.hsncodes=[];
                    } else if (response.data.type === 'SubCategory1') {
                        $scope.selectedSubCategories1 = response.data;
                        $scope.subCategories2 = response.data.children;
                        $scope.mainCategories[parentIndex][index]=response.data;
                        $scope.selectedSubCategories1=response.data.children;
                        var catEl = angular.element(document.querySelector('#' + $scope.mainCategories[parentIndex][index].code));
                        var icoEl = angular.element(document.querySelector('#icon-' + $scope.mainCategories[parentIndex][index].code));
                        if (catEl.attr('expanded') === 'true') {
                            catEl.css('max-height', '0');
                            catEl.attr('expanded', 'false');
                            icoEl.removeClass('fa-minus').addClass('fa-plus');
                        }
                        else {
                            catEl.css('max-height', '400px');
                            catEl.attr('expanded', 'true');
                            icoEl.removeClass('fa-plus').addClass('fa-minus');
                        }
                        $scope.selectedSubCategory=null;
                        $scope.hsncodes=[];
                    } else if (response.data.type === 'SubCategory2') {
                        $scope.selectedSubCategory = response.data;
                        $scope.hsncodes = response.data.hsncodes;
                        $scope.clearProduct();
                    }
                }, function (res) {
                    $scope.error = res.data.message;
                });
            }
        };

        $scope.getSegmentCategories=function (categories) {
            var selectedCategoriresArray=[];
            categories.filter(function (childCategory){
              selectedCategoriresArray.push({category:childCategory.category._id,enabled:childCategory.enabled});
          });
          return selectedCategoriresArray;
        };
        $scope.updateSelectedCategories=function (allMainCategories) {
            if ($scope.registrationCategory && $scope.registrationCategory.name === 'Manufacturer') {
                if ($scope.selectedSegments)
                    $scope.selectedSegments = [{
                        segment: $scope.selectedSegments._id,
                        categories: $scope.getSegmentCategories($scope.selectedSegments.categories)
                    }];
            }else  if ($scope.registrationCategory && $scope.registrationCategory.name === 'Distributor') {
                $scope.selectedSegments =[];
                 $scope.segments.filter(function (segment) {
                    if( segment.selected === true )
                        if(segment.isSpecific) {
                        $scope.selectedCat=[];
                                $scope.selectedSegments.push({segment: segment._id, categories: $scope.selectedCategory($scope.mainSubCategories,$scope.selectedCat)});

                        }else {
                            $scope.selectedSegments.push({segment: segment._id, categories:$scope.getSegmentCategories(segment.categories)});
                        }
                });
            }else{
                $scope.selectedSegments =[];
                $scope.selectedCat=[];
                var selectedCategories=$scope.selectedCategory($scope.mainSubCategories,$scope.selectedCat);
                if(selectedCategories.length>0) {
                    $scope.segments.filter(function (segment) {
                        /* if( segment.selected === true )*/
                        if (segment.isSpecific) {
                            $scope.selectedCat = [];
                            $scope.selectedSegments.push({
                                segment: segment._id,
                                categories: selectedCategories
                            });

                        }
                    });
                }
            }
            if($scope.selectedSegments && $scope.selectedSegments.length>0) {
                var company = new Companies({
                    _id: $scope.authentication.user.company,
                    segments: $scope.selectedSegments
                });

                // Redirect after save
                company.$update(function (response) {
                    $scope.company = response;
                    $scope.success = true;
                    $scope.cancel();
                }, function (errorResponse) {
                    $scope.error = errorResponse.data.message;
                });
            }else {
                    $mdToast.show( $mdToast.simple()
                        .textContent('Please Select One!')
                        .position('top right')
                        .theme('error-toast')
                        .hideDelay(3000)

                    );
                    //$scope.error = "Please select one ";
            }



        };
        $scope.cancel = function () {
            /*$scope.currentStatus=$scope.order.;*/
            $mdDialog.cancel();
        };
        // Update existing Category
        $scope.update = function () {
            var category = $scope.category;

            category.$update(function () {
                $location.path('categories/' + category._id);
            }, function (errorResponse) {
                $scope.error = errorResponse.data.message;
            });
        };
        $scope.findMainCategories= function () {
            $scope.mainCategories=[];
            categoryService.findMainCategoriesWithSearch().then(function (response) {
                $scope.mainCategories = response.data;
            }, function (res) {
                $scope.error = res.data.message;
            });
            return $scope.mainCategories;
        };
        $scope.findSubCategories1= function () {
            $scope.mainSubCategories=[];
            categoryService.findCategoriesWithSearch().then(function (response) {
                $scope.mainSubCategories = response.data;
            }, function (res) {
                $scope.error = res.data.message;
            });
            return $scope.mainSubCategories;
        };
        // Find a list of Categories
        $scope.find = function () {
            $scope.categories = Categories.query();
        };

        // Find existing Category
        $scope.findOne = function () {
            $scope.category = Categories.get({
                categoryId: $stateParams.categoryId
            });
        };
    }
]);
/*app.directive('selectCategory', function(){
    return {
        restrict:'E',
        templateUrl:'modules/categories/views/select-category.client.viewnew.html'
    };
});*/

app.directive('nvcTree', function () {
    return {
        restrict: 'E',
        template: '<div class="nvc-reg-cat-select"><div class="nvc-horizontal-scroll" ><nvc-node ng-repeat="c in tree" parent="parent" cnode="c" class="nvc-scroll-item"></nvc-node></div></div>',
        scope: {
            tree: '=tree',
            parent:'=parent'
        }
    };
});
app.directive('nvcNode', function ($compile) {
    return {
        restrict: 'E',
        scope:{
            node: '=cnode',
            parent:'=parent'
        },
        templateUrl: 'modules/categories/views/select-category.client.viewnew2.html',
        link: function (scope, element) {
            if (scope.node && scope.node.children && scope.node.children.length > 0) {
                scope.node.childrenVisibility = false;
                var childNode = $compile('<div class="nvc-horizontal-scroll" ng-if="node.childrenVisibility"><nvc-tree parent="node" tree="node.children" class="bb-wid-100"></nvc-tree></div>');
                element=element.closest('.nvc-horizontal-scroll');
                element.after(childNode(scope));
            } else {
                scope.node.childrenVisibility = true;
            }
        },
        controller: ['$scope','categoryService', function ($scope,categoryService) {
            $scope.toggleVisibility = function (node) {
                if(node.children && node.childrenVisibility){
                    node.childrenVisibility = false;
                }else if(node.children && !node.childrenVisibility){
                    if(this.parent.length>0){
                        this.parent.forEach(function (item) {
                            item.childrenVisibility = false;
                        });
                    }
                    node.childrenVisibility = true;
                }
            };

            function parentCheckChange(item) {
                for (var i in item.children) {
                    item.children[i].isChecked = item.isChecked;
                    if (item.children[i].children) {
                        parentCheckChange(item.children[i]);
                    }
                }
            }
            function childCheckChange(parent) {
                var allChecks = 0;
                for (var i in parent.children) {
                    if (parent.children[i].isChecked) {
                        allChecks++;
                    }
                }
                if (parent.children && (allChecks === parent.children.length)) {
                    parent.isChecked = true;
                    parent.selectedChild = null;
                }
                else if(parent.children){
                    parent.isChecked = false;
                    parent.selectedChild = allChecks!==0?allChecks:null;
                }
                if (parent.parent) {
                    childCheckChange(parent.parent);
                }
            }


            $scope.checkChange = function(item) {
                if(item.isChecked){
                    if(this.parent.length>0){
                        this.parent.forEach(function (item) {
                            item.childrenVisibility = false;
                        });
                    }
                    item.childrenVisibility = true;
                }else if(!item.isChecked){
                    item.childrenVisibility = false;
                }
                item.selectedChild = null;
                if (item.children) {
                    parentCheckChange(item);
                }
                if(this.parent)
                    childCheckChange(this.parent);
            };

        }]
    };
});
app.directive('nvTree', function () {
    return {
        restrict: 'E',
        template: '<div class="nvc-category-select" layout="row" layout-wrap layout-align="space-around"><nv-node ng-repeat="c in tree" parent="parent" cnode="c"></nv-node></div>',
        scope: {
            tree: '=tree',
            parent:'=parent'
        }
    };
});

app.directive('nvNode', function ($compile) {
    return {
        restrict: 'E',
        scope:{
            node: '=cnode',
            parent:'=parent'
        },
        templateUrl: 'modules/categories/views/select-category.client.viewnew.html',
        link: function (scope, element) {
            if (scope.node && scope.node.children && scope.node.children.length > 0) {
                scope.node.childrenVisibility = false;
                var childNode = $compile('<div class="nvc-subcategory1" ng-if="node.childrenVisibility"><nv-tree parent="node" tree="node.children"></nv-tree></div>');
                element.append(childNode(scope));
            } else {
                scope.node.childrenVisibility = true;
            }
        },
        controller: ['$scope','categoryService', function ($scope,categoryService) {
            $scope.toggleVisibility = function (node) {
                if(node.children && node.childrenVisibility){
                    node.childrenVisibility = false;
                }else if(node.children && !node.childrenVisibility){
                    if(this.parent.length>0){
                        this.parent.forEach(function (item) {
                            item.childrenVisibility = false;
                        });
                    }
                    node.childrenVisibility = true;
                }
            };

            function parentCheckChange(item) {
                for (var i in item.children) {
                    item.children[i].isChecked = item.isChecked;
                    if (item.children[i].children) {
                        parentCheckChange(item.children[i]);
                    }
                }
            }
            function childCheckChange(parent) {
                var allChecks = 0;
                for (var i in parent.children) {
                    if (parent.children[i].isChecked) {
                        allChecks++;
                    }
                }
                if (parent.children && (allChecks === parent.children.length)) {
                    parent.isChecked = true;
                    parent.selectedChild = null;
                }
                else if(parent.children){
                    parent.isChecked = false;
                    parent.selectedChild = allChecks!==0?allChecks:null;
                }
                if (parent.parent) {
                    childCheckChange(parent.parent);
                }
            }
            $scope.checkChange = function(item) {
                if(item.isChecked){
                    if(this.parent.length>0){
                        this.parent.forEach(function (item) {
                            item.childrenVisibility = false;
                        });
                    }
                    item.childrenVisibility = true;
                }else if(!item.isChecked){
                    item.childrenVisibility = false;
                }
                item.selectedChild = null;
                if (item.children) {
                    parentCheckChange(item);
                }
                if(this.parent)
                    childCheckChange(this.parent);
            };

        }]
    };
});



app.directive('nvTracker',function () {
   return {
       restrict: 'E',
       template: '<div class="nvc-reg-cat-select"><div class="nvc-horizontal-scroll" ><nv-tracker-child ng-repeat="c in tree" parent="parent" cnode="c" class="nvc-scroll-item"></nv-tracker-child></div></div>',
       scope: {
           tree: '=tree',
           parent:'=parent'
       }
   };
});

app.directive('nvTrackerChild',function () {
    return {
        restrict: 'E',
        template: '<div class="nvc-reg-cat-select"><div class="nvc-horizontal-scroll" ><nvc-node ng-repeat="c in tree" parent="parent" cnode="c" class="nvc-scroll-item"></nvc-node></div></div>',
        scope: {
            tree: '=tree',
            parent:'=parent'
        }
    };
});
