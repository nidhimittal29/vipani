'use strict';

//Categories service used to communicate Categories REST endpoints
var app=angular.module('categories');
app.factory('Categories', ['$resource',
    function ($resource) {
        return $resource('categories/:categoryId', {
            categoryId: '@_id'
        }, {
            update: {
                method: 'PUT'
            }
        });
    }
]);

app.directive('selectSegments', function($mdMedia){
    return {
        restrict:'E',
        templateUrl: function(elem, attr){
            if($mdMedia('xs')){
                return 'modules/categories/views/select-segments.client.viewnew.html';
            }else{
                return 'modules/categories/views/select-segments.client.viewnew2.html';
            }
        }

    };
});
app.factory('categoryService',['$stateParams', '$http','Categories',function($stateParams,$http,Categories){
    return {
        /**This call is intended to be used to get the
         * products that matched with item master.
         * This is used as part of csv or xls import of
         * products.
         * Input json:{subCategoryId,mainCategoryId,{srcLine:linenumber,lineData:[array of column for a line]}}
         * @param data
         * @returns product with line in the file that matched the product
         */
        findProductsByName:function(data){
            return $http.post('fetchProductsName',data);
        },

        /**This call to be used to get only subcategories
         * Pass in only the subcategory name and category name.
         *
         * @param [{mainCategoryName:"",subCategoryName:""}]
         * @returns {HttpPromise} [
         */
        findSubCategoriesByName:function(data){
            return $http.post('fetchSubCategoryByName',data);
        },
        findMainCategories:function () {
            return $http.get('queryMainCategories');
        },
        findSubCategories2:function () {
            return $http.get('querySubCategories2');
        },
        findSubCategories1:function () {
            return $http.get('querySubCategories1');
        },
        findMainCategoriesWithSearch:function (search) {
            return $http.get('queryMainCategoriesSearch');
        },
        findCategoriesWithSearch:function (search) {
            return $http.get('queryCategoriesSearch');
        },
        findByCategoryProducts:function (searchkeys) {
            return $http.get('/subcategory2inventory', {params: searchkeys});
        },
        findByHsncodes:function (searchkeys) {
            return $http.get('/hsncodeSearch', {params: searchkeys});
        },
        findByTaxGroups:function (searchkeys) {
            return $http.get('/taxSearch', {params: searchkeys});
        },
        findByUoms:function (searchkeys) {
            return $http.get('/uomSearch', {params: searchkeys});
        },
        findOneCompany:function (companyName) {
            return $http.get('companyProducts/'+companyName);
        },
        findSubcategory2Offers : function () {
            return $http.get('subCategory2Offers/' + $stateParams.categoryId);
        },
        findCategory : function () {
            return $http.get('categories/' + $stateParams.categoryId);
        },
        createProductMaster : function (data) {
            var category= new Categories(data);
            return $http.post('categories',category);
        },
        findCategoryById : function (id) {
            return $http.get('categories/' + id);
        },
        /*,
        selectedCategory: function (categories,selcategoires,categoryCallBack) {
            async.forEach(categories,function (cat) {
            if(cat.selected===true)
                selcategoires.push({category:cat._id});
            else if(cat.children.length>0) {
                categoryService.selectedCategory(cat.children,selcategoires,function (respChildNode) {
                    categoryCallBack();
                });
            }
        }, function (categoryError) {
            if(!categoryError) done(null, selcategoires);
            else done(categoryError);
        });
        }*/
    };
}]);
app.factory('productBrandService',['$stateParams', '$http',function($stateParams,$http){
    return {
        findProductBrands: function (subcategory,searchText) {
            return $http.get('queryProductBrands',{category:subcategory,searchText:searchText});
        },
        createProductBrandMaster : function (data) {
            return $http.post('productBrand',data);
        },
        updateProductBrandMaster : function (data) {
            return $http.put('productBrand',data);
        },
        findBrandById : function (id) {
            return $http.get('productBrand/' + id);
        }
    };
}]);
app.factory('productUomService',['$stateParams', '$http',function($stateParams,$http){
    return {
        findMainCategories: function () {
            return $http.get('queryMainCategories');
        }
    };
}]);
app.factory('productTaxgroupService',['$stateParams', '$http',function($stateParams,$http){
    return {
        findMainCategories: function () {
            return $http.get('queryMainCategories');
        }
    };
}]);
app.factory('productHsncodeService',['$stateParams', '$http',function($stateParams,$http){
    return {
        findMainCategories: function () {
            return $http.get('queryMainCategories');
        }
    };
}]);
