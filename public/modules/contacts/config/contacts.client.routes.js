'use strict';

//Setting up route
angular.module('contacts').config(['$stateProvider',
	function($stateProvider) {
		// Contacts state routing
		$stateProvider.
		state('listContacts', {
			url: '/contacts',
			templateUrl: 'modules/contacts/views/contact/list-contacts-nvc.client.viewnew.html',
			ncyBreadcrumb: {
				label: 'listContacts'
			}
		}).
		state('createContact', {
			url: '/contacts/create',
			templateUrl: 'modules/contacts/views/contact/create-contact.client.view.html'
		}).
		state('viewContact', {
			url: '/contacts/:contactId',
			templateUrl: 'modules/contacts/views/contact/view-contact.client.view.html',
			ncyBreadcrumb: {
				label: 'ViewContact'
			}
		}).
		state('editContact', {
			url: '/contacts/:contactId/edit',
			templateUrl: 'modules/contacts/views/contact/edit-contacts.client.viewnew.html',
			ncyBreadcrumb: {
				label: 'editContact'
			}
		})/*.
		state('editGroup', {
			url: '/groups/:groupId/edit',
			templateUrl: 'modules/contacts/views/groups/edit-group.client.view.html'
		})*/;
	}
]);
