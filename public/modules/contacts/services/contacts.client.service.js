'use strict';

//Contacts service used to communicate Contacts REST endpoints
var app=angular.module('contacts');
app.factory('Contacts', ['$resource',
    function($resource) {
        return $resource('contacts/:contactId', { contactId: '@_id'
        }, {
            update: {
                method: 'PUT'
            }
        });
    }
]);

app.factory('PaymentTerms',['$stateParams', '$http','Contacts','Groups',function($stateParams,$http,Contacts,Groups){
    return {
        findPaymentTerms: function () {
            return $http.get('paymentterms');
        },
        findCompanyPaymentTerms: function (companyId) {
            return $http.get('companypaymentterms?paymentCompanyId='+companyId);
        },
        findCompanyPaymentModes: function (companyId) {
            return $http.get('companypaymentmodes?paymentCompanyId='+companyId);
        }
    };
}]);
app.factory('contactsQuery',['$stateParams', '$http','Contacts','Groups','contactsGroupActionQuery',function($stateParams,$http,Contacts,Groups,contactsGroupActionQuery){

    return {
        importMultipleContacts:function(contacts,done) {
            $http.post('/contactsImport', {contacts:contacts}).then(function (results) {
                if(results.status===200) {
                    done(null,results.data);
                }

            },function(err){
                if(err){
                 done(err,null);
                }
            });
        },
        preImportValidateContacts:function(contacts,done){
            $http.post('/contactsPreimport',contacts).then(function(response){
                done(response);
            });
        },
        findContacts: function (scope,isCustomer) {
            scope.contacts = [];
            scope.selfcontact = [];
            Contacts.query().$promise.then(function (result) {
                angular.forEach(result, function (e) {
                    if ( e.nVipaniUser!==null &&   (e.nVipaniRegContact!==null && e.nVipaniRegContact===true && (e.nVipaniUser=== e.user || e.nVipaniUser=== e.user._id))) {
                        scope.selfcontact.push(e);
                    }else{
                        if(!e.disabled)
                            scope.contacts.push(e);
                    }
                });
            });
            return scope;

        },
        getContacts:function (isCustomer,pagination) {
            if(isCustomer) {
                return $http.get('/contacts/customers',{params: pagination});
            }else{
                return $http.get('/contacts/suppliers',{params: pagination});
            }
        },
        getOffersByGroup:function (groupId) {
            return $http.get('/groupOffers/'+groupId);
        },

        getOffersByContact:function (contactId) {
            return $http.get('/contactOffers/'+contactId);
        },
        getOrdersByContact:function (contactId) {
            return $http.get('/contactOrders/'+contactId);
        },
        findContactsVsGroups: function (scope,isOnlyContacts,isCustomer,isSpecific,done){
            scope.contactsVsGroup=[];
            var url = 'contacts';
            if(isCustomer && isSpecific){
                url='contacts/customers';
            }
            else if(isSpecific && !isCustomer){
                url='contacts/suppliers';
            }
            $http.get(url).then(function (resultContact) {
                if(!isOnlyContacts) {
                    $http.get('groups').then(function (result) {
                        scope.contactsVsGroup= resultContact.data.concat(result.data.group);
                        done(scope.contactsVsGroup);
                    });
                }
                else{
                    scope.contactsVsGroup = resultContact.data.contact;
                    done(scope.contactsVsGroup);
                }
            });
        },
        findGroupById:function (id) {
            return $http.get('groups/'+id);
        },
        getGroups: function (scope,pagination) {
            if(pagination) {
                return $http.get('groups', {params: pagination});
            }else {
                return $http.get('groups');
            }
        },
        batchContact:function (index,selectedContacts) {
            if(index===0) {
                return $http.post('/contactsremove', selectedContacts);
            }else if(index===1) {
                return $http.post('/contactsinactive', selectedContacts);
            }else if(index===2) {
                return $http.post('/contactsactive', selectedContacts);
            }

        },
        batchGroup:function (index,selectedGroups) {
            if(index===0) {
                return $http.post('/groupsremove', selectedGroups);
            }else if(index===1) {
                return $http.post('/groupsinactive',selectedGroups);
            }else if(index===2) {
                return $http.post('/groupsactive', selectedGroups);
            }

        },
        getUpdatedContacts:function ($scope,updatedContacts) {
            for (var i=0;i<$scope.contacts.length ;i++){
                for (var j=0;j<updatedContacts.length ;j++){
                    if($scope.contacts[i]._id===updatedContacts[j]._id) {
                        $scope.contacts[i] = updatedContacts[j];
                    }
                }
            }
        },
        getSelectedContacts:function (type,scope) {
            if(type===1){
                type = 'Supplier';
            }
            if(type===0){
                type = 'Customer';
            }
            var selectedContacts={'contacts':[],type:type};
            angular.forEach(scope.contacts,function (contact) {
                if(contact.selected){
                    selectedContacts.contacts.push({_id:contact._id});
                }
            });
            return selectedContacts;
        },
        getUpdatedGroups:function ($scope,updatedGroups){
            for (var i=0;i<$scope.groups.length ;i++){
                for (var j=0;j<updatedGroups.length ;j++){
                    if($scope.groups[i]._id===updatedGroups[j]._id) {
                        $scope.groups[i] = updatedGroups[j];
                    }

                }
            }
        },
        getSelectedGroups:function (scope) {
            var selectedGroups={'groups':[]};
            angular.forEach(scope.groups,function (group) {
                if(group.selected){
                    selectedGroups.groups.push({_id:group._id});
                }
            });
            return selectedGroups;
        },
        contactSummaries:function (index) {
            if(index === 0){
                return $http.get('customercontactssummary');
            }else if(index === 1){
                return $http.get('suppliercontactssummary');
            }
        },
        contactFilterSummaries:function (index,paginationKeys,data) {
            if(index === 0){
                return $http.post('customercontactssummary',data,{params:paginationKeys});
            }else if(index === 1){
                return $http.post('suppliercontactssummary',data,{params:paginationKeys});
            }
        }
    };
}
]);
app.factory('contactsGroupActionQuery',['$http','$mdToast',function ($http,$mdToast) {
    return {
        removeContact:function(contact,scope){
            $http.post('/contactsremove', contact).then(function (results) {
                scope.contacts = results.data.contact;
                scope.total_count = results.data.total_count;
            }, function (err) {
                scope.error = err.data.message;
            });
        }
    };
}]);
app.factory('verifyDelete', function($mdDialog,Offers,Groups,Inventories,Products,Orders) {
    return function(user) {
        var content='Do you want to delete the  ';
        var label='Confirm';
        if(user instanceof Offers){
            content+=' Offer:' +user.name;
            label='Delete Offer';

        }else if(user instanceof Groups){
            content+=' Group:' +user.name;
            label='Delete Group';
        }else if(user instanceof Inventories){
            content+=' Product:' +(user.product ?user.product.name:'');
            label='Delete Product';
        }
        else if(user instanceof Products){
            content+=' Product:' +user.name;
            label='Delete Product';
        }else if(user instanceof Orders){
            content+=' Order Number:' +user.orderNumber;
            label='Delete Order';
        }else{
            if(user.shipping){
                content = ' Are you sure you want to use Shipping  Address as Billing';
                label = 'Billing Address';
            }else if(user.billing){
                content= 'Are you sure you want to use Billing  Address as Shipping';
                label = 'Shipping Address';
            }
        }
        var
            confirm = $mdDialog.confirm()
                .title('Confirm Your Choice')
                .content( content+ '?')
                .ariaLabel(label)
                .ok(label)
                .cancel('Cancel');

        return $mdDialog.show(confirm);
    };
});
app.factory('alert',function ($mdDialog) {
    return function(user) {
        var alert = $mdDialog.alert()
            .clickOutsideToClose(true)
            .title(user.title)
            .textContent(user.content)
            .ok('Ok');
        return $mdDialog.show(alert);
    };
});

app.directive('singleContact',function () {
    return {
        restrict:'A',
        /* scope:{
             contact:'='
         },*/
        templateUrl: 'modules/contacts/views/contact/contact-row.client.view.html',
    };
});

app.directive('nvAddress', function ($compile) {
    return {
        restrict: 'E',
        scope:{
            address: '=cnode',
            isBilling:'=billing',
            isShipping:'=shipping'
        },
        templateUrl: 'modules/orders/views/address-dialogs.html'
    };
});


app.directive('nvcTable', function () {
    return {
        restrict: 'E',
        template: '<nv-node ng-repeat="c in tree" cnode="c"></nv-node></div>',
        scope: {
            eachTable: '=tree',
            isGroup:'=isGroup'
        }
    };
});

app.directive('nvTableRow', function ($compile) {
    return {
        restrict: 'E',
        scope:{
            node: '=cnode'
        },
        templateUrl: 'modules/categories/views/select-category.client.viewnew.html',
        link: function (scope, element) {
            if (scope.node && scope.node.children && scope.node.children.length > 0) {
                scope.node.childrenVisibility = true;
                var childNode = $compile('<div class="nvc-subcategory1" ng-if="!node.childrenVisibility"><nv-tree parent="node" tree="node.children"></nv-tree></div>');
                element.append(childNode(scope));
            } else {
                scope.node.childrenVisibility = false;
            }
        },
        controller: ['$scope','categoryService', function ($scope,categoryService) {
            $scope.toggleVisibility = function (node) {
                if (node.children) {
                    node.childrenVisibility = !node.childrenVisibility;
                }
            };

            function parentCheckChange(item) {
                for (var i in item.children) {
                    item.children[i].isChecked = item.isChecked;
                    if (item.children[i].children) {
                        parentCheckChange(item.children[i]);
                    }
                }
            }
            function childCheckChange(parent) {
                var allChecks = true;
                for (var i in parent.children) {
                    if (!parent.children[i].isChecked) {
                        allChecks = false;
                        break;
                    }
                }
                if (allChecks) {
                    parent.isChecked = true;
                }
                else {
                    parent.isChecked = false;
                }
                if (parent.parent) {
                    childCheckChange(parent.parent);
                }
            }


            $scope.checkChange = function(item) {
                $scope.toggleVisibility(item);
                if (item.children) {
                    parentCheckChange(item);
                }
                if(this.parent)
                    childCheckChange(this.parent);
            };

        }]
    };
});

app.factory('panelTrigger',['$mdSidenav', function($mdSidenav) {
    return function(id) {
        // Component lookup should always be available since we are not using `ng-if`
        $mdSidenav(id)
            .toggle()
            .then(function () {
                /*$log.debug('toggle ' + navID + ' is done');*/
            });
    };
}]);
app.factory('contactsCSVFileParse',function () {
    return {
        csvToJSON : function(content,importType,done) {
            var lines=content.lines;
            var result = {header: [], data: [], error: [], status: true};
            var start = 0;
            var columnCount = lines[0].split(content.separator).length;

            var headerPhoneConst = ['Type','DisplayName','PrimaryPhone'];
            var headerEmailConst = ['Type','DisplayName','PrimaryEmail'];

            var headers = [];
            var errors='';
            if (content.header) {
                headers=lines[0].split(content.separator);
                start = 1;
                for(var t=0;t<headers.length;t++){
                    headers[t]=this.cleanCsvValue(headers[t]);
                }
                result.header = headers;

                var headerMissing = '';
                for(var i=0;i<headerPhoneConst.length;i++){
                    if(headers.indexOf(headerPhoneConst[i])===-1){
                        headerMissing +=headerPhoneConst[i]+',';
                    }
                }
                if(headerMissing!=='') {
                    headerMissing = '';
                    for(var j=0;j<headerEmailConst.length;j++){
                        if(headers.indexOf(headerEmailConst[j])===-1){
                            headerMissing +=headerEmailConst[j]+',';
                        }
                    }
                    if(headerMissing!=='') {
                        errors = 'All mandatory columns required for importing the CSV file is not present.  ' +
                            'Following fields are missing or are not in the correct position:' + headerMissing +
                            'The mandatory columns are :Type,DisplayName,PrimaryPhone/PrimaryEmail';
                        var headerObj = {};
                        headerObj.srcLine = 0;
                        headerObj.rowError = errors;
                        result.error.push(headerObj);
                        result.errorType = 1;
                        result.status = false;
                        done(result);
                    }
                }

            }

            for (var k=start; k<lines.length; k++) {
                if(lines[k]!=='') {
                    var rowData = [];
                    errors = '';
                    var currentline = lines[k].split(new RegExp(content.separator + '(?![^"]*"(?:(?:[^"]*"){2})*[^"]*$)'));
                    if (!this.isRowBlank(currentline)) {
                        if (currentline.length === columnCount) {
                            for (var l = 0; l < headers.length; l++) {
                                var obj = {};
                                obj.name = this.cleanCsvValue(headers[l]);
                                obj.value = this.cleanCsvValue(currentline[l]);

                                if (headerPhoneConst.indexOf(headers[l]) >= 0 && obj.value === ''){
                                    if(headerEmailConst.indexOf(headers[l]) >= 0 && obj.value === '') {
                                        if (errors === '') {
                                            errors = 'Column ' + headers[l] + ': The mandatory column ' + headers[l] + ' value is empty';
                                        } else {
                                            errors = errors + '<br/>Column ' + headers[l] + ':The mandatory column ' + headers[l] + ' value is empty';
                                        }
                                    }
                                }
                                if (errors === '') {
                                    rowData.push(obj.value);
                                }
                            }

                            if (errors === '') {
                                result.data.push({srcLine: k, lineData: rowData});
                            } else {
                                rowData.push(errors);
                                result.error.push({srcLine: k, lineData: rowData});
                                result.status = false;
                            }

                        } else if (currentline.length <= 1) {
                            //just log no need to put push to errors.
                            console.log('Ignored line' + lines[k]);
                        }
                        else {
                            errors = 'This row column data does not match with header column data';
                            for (var m = 0; m < currentline.length; m++) {
                                var objEach = {};
                                objEach.name = '' + m;
                                objEach.value = this.cleanCsvValue(currentline[m]);
                                rowData.push(objEach.value);
                            }

                            rowData.push(errors);
                            result.error.push({srcLine: k, lineData: rowData});
                            result.status = false;
                        }
                    }
                }
            }

            done(result);
        },
        cleanCsvValue : function(value) {
            return value.trim()
                .replace(/^\s*|\s*$/g,'') // remove leading & trailing space
                .replace(/^"|"$/g,'') // remove " on the beginning and end
                .replace(/\r?\n|\r/g,'');
        },
        csvHeader: function(strData, strDelimiter) {},
        isRowBlank:function(rows){
            var fieldNotBlank=0;
            for(var i=0;i<rows.length;i++){
                if(this.cleanCsvValue(rows[i])!==''){
                    fieldNotBlank++;
                }
            }
            return fieldNotBlank===0;
        }

    };
});
