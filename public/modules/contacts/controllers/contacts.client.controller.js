'use strict';

//var logger  = require('../../../lib/log').getLogger('CONTACTS-CONTROLLER', 'DEBUG');

// Contacts controller
var app = angular.module('contacts');
//var jQuery = require('jquery');

app.controller('ContactsController', ['$scope', '$stateParams', '$location', '$window','$http', 'Authentication', 'Contacts', 'Groups', 'FileUploader', '$timeout', '$document','verifyDelete','panelTrigger','$mdToast','contactsQuery','$mdDialog','PaymentTerms','contactsGroupActionQuery','CompanyService','criteriaChips',
    function ($scope, $stateParams, $location, $window, $http, Authentication, Contacts, Groups, FileUploader, $timeout, $document,verifyDelete ,panelTrigger,$mdToast,contactsQuery,$mdDialog,PaymentTerms,contactsGroupActionQuery,CompanyService,criteriaChips) {
        $scope.authentication = Authentication;
        $scope.company={};
        $scope.busUnitId='';
        $scope.user = $scope.authentication.user;
        $scope.contactImageURL = 'modules/contacts/img/default.png';
        $scope.groupImageURL = 'modules/groups/img/default.png';

        $scope.toggleSideNavigation = function (id,val) {

            if(val === ''){
                $timeout(function () {
                    panelTrigger(id).open().then(function () {
                        console.log('Trigger is done');
                    });
                });
            }else{
                val=val;
            }
            if (val === 'toggle') {
                panelTrigger(id).toggle().then(function () {
                    /*console.log('Trigger is done');*/
                });
            }else if (val === 'open') {
                panelTrigger(id).open().then(function () {
                    /* console.log('Trigger is done');*/
                });
            }else if (val === 'close') {
                panelTrigger(id).close().then(function () {
                    /*console.log('Trigger is done');*/
                });
            }else if (val === 'isOpen') {
                panelTrigger(id).isOpen().then(function () {
                    /* console.log('Trigger is done');*/
                });
            }
        };
        /* =========== Code for cropping an image before upload, commented the code because of some integrating issue - 02-04-18 ===========*/
        /*$scope.myImage='';
        $scope.myCroppedImage='';
        $scope.handleFileSelect=function(evt) {
            if(evt) {
                var file = evt.currentTarget.files[0];
                var reader = new FileReader();
                reader.onload = function (evt) {
                    $scope.$apply(function ($scope) {
                        $scope.myImage = evt.target.result;
                    });
                };
                if(file) {
                    reader.readAsDataURL(file);
                }
            }
        };
        $scope.setImageUrl = function(){
            var el = angular.element(document.querySelector('#ele'));
            var blob = $scope.dataToBlob($scope.myCroppedImage);
            el[0].change();
        };
        $scope.dataToBlob = function(dataURI){
            var byteString;
            if (dataURI.split(',')[0].indexOf('base64') >= 0)
                byteString = atob(dataURI.split(',')[1]);
            else
                byteString = unescape(dataURI.split(',')[1]);

            // separate out the mime component
            var mimeString = dataURI.split(',')[0].split(':')[1].split(';')[0];

            // write the bytes of the string to a typed array
            var ia = new Uint8Array(byteString.length);
            for (var i = 0; i < byteString.length; i++) {
                ia[i] = byteString.charCodeAt(i);
            }

            return new Blob([ia], {type:mimeString});
        };*/
        /* ================ End of code for crop and upload image ==============*/
        /*----------- Close Md dialog ---------------*/
        $scope.cancel = function(){
            $mdDialog.cancel();
        };
        $scope.selected = [];
        $scope.searchText = '';
        $scope.phones = [{
            phoneNumber: '',
            phoneType: 'Mobile',
            primary: true
        }];
        $scope.tabs = [
            { title:'Customers', type:'Customers'},
            { title:'Suppliers',type:'Suppliers'},
            { title:'Groups',type:'Groups'}
        ];
        $scope.phoneTypes = ['Mobile', 'Home', 'Work', 'Other'];
        $scope.emailTypes = ['Work', 'Personal', 'Other'];

        $scope.addressTypes = ['Billing', 'Shipping', 'Receiving', 'Invoice'];
        $scope.emails = [{
            email: '',
            emailType: 'Work',
            primary: true
        }];
        $scope.addresses = [{
            addressLine: '',
            city: '',
            state: '',
            country: '',
            pinCode: '',
            addressType: 'Billing',
            primary: true
        },
            {
                addressLine: '',
                city: '',
                state: '',
                country: '',
                pinCode: '',
                addressType: 'Shipping',
                primary: true
            },
            {
                addressLine: '',
                city: '',
                state: '',
                country: '',
                pinCode: '',
                addressType: 'Receiving',
                primary: true
            },
            {
                addressLine: '',
                city: '',
                state: '',
                country: '',
                pinCode: '',
                addressType: 'Invoice',
                primary: true
            }];
        $scope.error = null;
        $scope.selectedGroup = '';
        $scope.AllContactsString = 'All Contacts';
        $scope.allContacts = true;
        $scope.paginationId='Customers';

        $scope.getContactsSummaries=function(index){
            contactsQuery.contactSummaries(index).then(function (responce) {
                $scope.contactfilters=responce.data;
            },function (err) {
                $scope.error=err.data;
            });
        };

        function setContacts(contacts) {
            $scope.contacts = [];
            angular.forEach(contacts, function (e) {
                if (e.nVipaniUser !== null && (e.nVipaniRegContact !== null && e.nVipaniRegContact === true && (e.nVipaniUser === e.user || e.nVipaniUser === e.user._id))) {
                    $scope.selfcontact.push(e);
                } else {
                    $scope.contacts.push(e);
                }
            });
        }
        $scope.contactFilterSummary=function(filterKey,filterCount,pageno) {
            var paginationfield = {};
            $scope.isFilterSummary = true;
            if (!isNaN(pageno)) {
                paginationfield = {page: pageno, limit: $scope.itemsPerPage};
            }
            if (this.searchText && this.searchText.length > 0) {
                paginationfield.searchText = this.searchText;
            }
            if (filterKey) {
                $scope.filterKey = filterKey;
                $scope.data = {summaryFilters: {}};
                $scope.data.summaryFilters[$scope.filterKey] = true;
            }
            contactsQuery.contactFilterSummaries($scope.selectedIndex, paginationfield, $scope.data).then(function (response) {
                setContacts(response.data[$scope.filterKey]);
                if (filterCount)
                    $scope.total_count = filterCount;
            }, function (res) {
                $scope.error = res.data.message;
            });
        };
        $scope.getContactsFilterSummary=function(filterKey,filterCount,toggle){
            if(toggle){
                $scope.contactFilterSummary(filterKey,filterCount,1);
            }else{

                $scope.filterKey=null;
                $scope.find($scope.selectedIndex===0?true:false,1);
            }
        };

        $scope.changeSelectionTab=function (busUnitId,type) {
            $scope.filterKey=null;
            if(type==='Customers'){
                $scope.contacts=[];
                $scope.pageno=1;
                $scope.total_count = 0;
                $scope.itemsPerPage = 10;
                $scope.selectedIndex=0;
                $scope.paginationId='Customers';
                $scope.find(true,$scope.pageno);
            }else if(type==='Suppliers'){
                $scope.contacts=[];

                $scope.pageno=1;
                $scope.total_count = 0;
                $scope.itemsPerPage = 10;
                $scope.selectedIndex=1;
                $scope.paginationId='Suppliers';
                $scope.find(false,$scope.pageno);
            }else if(type==='Groups') {
                $scope.contacts=[];
                $scope.groups=[];
                $scope.pageno=1;
                $scope.total_count = 0;
                $scope.itemsPerPage = 10;
                $scope.selectedIndex=2;
                $scope.paginationGroupId='Groups';
                $scope.findGroup($scope.pageno);

            }
        };
        $scope.getOffersByContact=function (id) {
            contactsQuery.getOffersByContact(id).then(function (response) {
                $scope.contactOffers=response.data;
            },function (errResponse) {
                $scope.error=errResponse.data.message;
            });
        };
        $scope.getOrdersByContact=function (id) {
            contactsQuery.getOrdersByContact(id).then(function (response) {
                $scope.contactOrders=response.data;
            },function (errResponse) {
                $scope.error=errResponse.data.message;
            });
        };
        $scope.getOffersByGroup=function (id) {
            contactsQuery.getOffersByGroup(id).then(function (response) {
                $scope.groupOffers=response.data;
            },function (errResponse) {
                $scope.error=errResponse.data.message;
            });
        };
        $scope.getViewFileTemplate=function (option) {

            $http.get('fileexport' + '?type=' + option.toLowerCase(), {responseType: 'arraybuffer'}).then(function (res) {
                /*var reportMimeType = 'application/pdf';
                if (response.reports[0].format === 'XLS') {
                    reportMimeType = 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet';
                }*/
                var a = document.createElement('a');
                document.body.appendChild(a);
                a.style = 'display: none';
                var reportMimeType = 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet';

                var file = new Blob([res.data], {type: reportMimeType});
                var fileURL = URL.createObjectURL(file);
                a.href = fileURL;
                a.download = 'contacts-import-template.xlsx';
                a.click();
                file = new Blob([res.data], {type: 'text/csv'});
                fileURL = URL.createObjectURL(file);
                window.open(fileURL);
            });
        };

        $scope.massImportContacts = function(contacts){
            contactsQuery.importMultipleContacts(contacts,function(err,res){
                if(err){
                    $scope.showToast(err.data.message,'bottom right','#page-'+$scope.getSelectedPageId(),'error-toast');

                }
                if(res) {
                    if(res.contacts.length>0) {
                        $scope.contactsImportSuccess = true;
                    }
                    $scope.contactImportErrors=res.errors;
                    var onlyCustAdded=-1;

                    for(var i=0;i<res.contacts.length;i++) {
                        //only if first tab add it to customer tab
                        //else add to suppliers tab
                        if(res.contacts[i].contact.customerType==='Supplier' && $scope.selectedIndex===1) {
                            $scope.contacts.push(res.contacts[i].contact);

                        }
                        else if(res.contacts[i].contact.customerType==='Customer' && $scope.selectedIndex===0) {
                            $scope.contacts.push(res.contacts[i].contact);

                        }

                    }

                    if(res.contacts.length===0){
                        $scope.contactImportErrors.push({errors:['No contacts were found to import']});
                        $scope.showToast('No contacts were found to import','bottom right','#page-'+$scope.getSelectedPageId(),'error-toast');
                    }
                    else{
                        $scope.getContactsSummaries($scope.selectedIndex);
                        $scope.showToast('Contacts imported successfully. Click finish to close.','top right','#page-'+$scope.getSelectedPageId(),'success-toast');

                    }
                }

            });
        };
        /* $scope.getTitle=function (tab) {
             if($scope.selectedIndex===2) return  title+($scope.groups.length > 0 ? '( ' + $scope.groups.length + ')' : '');
             else return title+($scope.contacts.length > 0 ? '( ' + $scope.contacts.length + ')' : '');
         };*/
        // Create new Contact
        $scope.create = function (form) {
            /*if(!isValid){
                $scope.error = 'Phone No. or Email is required to create contact';
            }else {*/
            // Create new Contact object
            var contact = new Contacts({
                firstName: this.firstName,
                middleName: this.middleName,
                lastName: this.lastName,
                contactImageURL: this.contactImageURL,
                companyName: this.companyName,
                phones: this.phones,
                emails: this.emails,
                addresses: this.addresses,
                paymentTerm: this.paymentTerm,
                displayName: this.displayName,
            });
            if ($scope.selectedIndex === 0) {
                contact.customerType = 'Customer';
            } else if ($scope.selectedIndex === 1) {
                contact.customerType = 'Supplier';
            }
            if ($scope.isOneContactRequiredUpdate(contact)) {
                contact.phones = contact.phones.filter(function (phones) {
                    return phones.phoneNumber && phones.phoneNumber.length > 0;
                });

                contact.emails = contact.emails.filter(function (emails) {
                    return emails.email && emails.email.length > 0;
                });

                contact.addresses = contact.addresses.filter(function (addresses) {
                    return !(addresses.addressLine === '' &&
                        addresses.city === '' &&
                        addresses.state === '' &&
                        addresses.country === '' &&
                        addresses.pinCode === '');
                });
                if ($scope.selectedGroup.length !== 0) {
                    contact.groups = [];
                    contact.groups.push($scope.selectedGroup._id);
                }
                // Redirect after save
                contact.$save(function (response) {
                    /* $location.path('contacts');*/

                    // Clear form fields
                    $scope.clearFields();

                    //update current list
                    $scope.contacts.push(contact);
                    $scope.selcontact = contact;
                    $scope.updatecontact = angular.copy(contact);
                    /*$scope.panelChange($scope.selcontact, '');*/
                    $scope.contact = contact;
                    $scope.getContactsSummaries($scope.selectedIndex);

                }, function (errorResponse) {
                    $scope.showToast(errorResponse.data.message,'bottom right','#page-'+$scope.getSelectedPageId(),'error-toast');

                });
            } else {
                $scope.showToast('*Email or phone is required for contact creation','bottom right','#page-'+$scope.getSelectedPageId(),'error-toast');

            }
        };

        $scope.reset = function (form) {
            $scope.error = null;
            if (form) {
                form.$setPristine();
                //form.$setUntouch  ed();
            }

        };
        $scope.clearFields = function () {
            $scope.firstName = '';
            $scope.lastName = '';
            $scope.companyName = '';
            $scope.middleName = '';
            $scope.displayName = '';
            $scope.name = '';
            $scope.phones = [{
                phoneNumber: '',
                phoneType: 'Mobile',
                primary: true
            }];
            $scope.emails = [{
                email: '',
                emailType: 'Work',
                primary: true
            }];
            $scope.addresses = [{
                addressLine: '',
                city: '',
                state: '',
                country: '',
                pinCode: '',
                addressType: 'Billing',
                primary: true
            }];
            $scope.error = null;
            $scope.addParticipents=false;
            $mdDialog.hide();
        };

        // Remove group
        $scope.uploader = new FileUploader({
            headers: {'token': $scope.token},
            url: 'contacts/contactPicture'
        });

        $scope.uploader.filters.push({
            name: 'imageFilter',
            fn: function (item, options) {

                var lblError = document.getElementById('lblError');
                var type = '|' + item.type.slice(item.type.lastIndexOf('/') + 1) + '|';
                var validError= '|jpg|png|jpeg|bmp|gif|'.indexOf(type) !== -1;
                lblError.innerHTML='';
                if(!validError) {
                    lblError.innerHTML = 'Please upload files having extensions:jpg, png, jpeg, bmp, gif <b> .Invalid type:'+item.type;
                }else if(validError && item.size >1024 * 1024 * 5){
                    $scope.contactImageURL='modules/contacts/img/default.png';
                    lblError.innerHTML = 'Please upload files size should be less than  5MB <b> .Current File size:'+item.size;
                }
                return validError;
            }
        });

        // Called after the user selected a new picture file
        $scope.uploader.onAfterAddingFile = function (fileItem) {
            if ($window.FileReader) {
                var fileReader = new FileReader();
                fileReader.readAsDataURL(fileItem._file);


                fileReader.onload = function (fileReaderEvent) {
                    $timeout(function () {
                        $scope.contactImageURL = fileReaderEvent.target.result;
                    }, 0);
                };
            }
        };

        // Called after the user has successfully uploaded a new picture
        $scope.uploader.onSuccessItem = function (fileItem, response, status, headers) {
            // Show success message
            $scope.imgsuccess = true;
            var imageURLStr = response.contactImageURL;
            $scope.contactImageURL = imageURLStr.replace(/"/g, '');
            // Clear upload buttons
            $scope.cancelUpload();
        };

        // Called after the user has failed to uploaded a new picture
        $scope.uploader.onErrorItem = function (fileItem, response, status, headers) {
            // Clear upload buttons
            $scope.cancelUpload();

            // Show error message
            $scope.imgerror = response.message;
        };
        $scope.removeGroup = function (group) {
            var i = 0;
            verifyDelete(group).then(function() {
                var parentEl = angular.element(document.querySelector('#content'));
                $scope.updateGroup.$remove(function (result) {
                    for (i in $scope.groups) {
                        if ($scope.groups [i]._id === $scope.selectedGroup._id) {
                            $scope.groups.splice(i, 1);
                            $scope.showEdit = false;
                            $scope.showView = false;
                            $scope.viewAll = true;
                            break;
                        }
                    }
                    /*if ($scope.groups.length > 0) {
                        if (i === '0') {
                            $scope.selectedGroup = $scope.groups[i];
                            $scope.updateGroup = angular.copy($scope.groups[i]);

                        } else if (i <= $scope.groups.length) {

                            $scope.selectedGroup = $scope.groups[i - 1];
                            $scope.updateGroup = angular.copy($scope.groups[i - 1]);

                        } else {

                            $scope.selectedGroup = $scope.groups[i + 1];
                            $scope.updateGroup = angular.copy($scope.groups[i + 1]);


                        }
                        $scope.showEdit = false;

                        $scope.showAddToGroups = false;
                        $scope.showEditGroup = false;
                        $scope.showView = false;
                        $scope.viewAll = true;
                    } else {
                        $scope.allContacts = true;
                        $scope.selectedGroup = '';
                        $scope.find();
                    }*/
                    $scope.showToast('Successfully deleted Offer','bottom right','#page-'+$scope.getSelectedPageId(),'error-toast');

                },function (errorResponse) {
                    $scope.showToast(errorResponse.data.message,'#page-'+$scope.getSelectedPageId(),'error-toast');
                });

            });

        };

        // Remove existing Contact
        $scope.remove = function (contact) {

            if ($scope.selcontact) {
                var i = 0;
                //$scope.selcontact.$remove();
                $scope.updatecontact.$remove();
                for (i in $scope.contacts) {
                    if ($scope.contacts [i] === contact) {
                        $scope.contacts.splice(i, 1);
                        /*	$scope.selcontact = $scope.contacts[i];
                         $scope.showEdit=false;
                         $scope.showView=true;*/
                        break;
                    }
                }
                if ($scope.contacts.length > 0) {
                    if (i === '0') {
                        $scope.selcontact = $scope.contacts[i];
                        $scope.updatecontact = angular.copy($scope.contacts[i]);

                    } else if (i <= $scope.contacts.length) {

                        $scope.selcontact = $scope.contacts[i - 1];
                        $scope.updatecontact = angular.copy($scope.contacts[i - 1]);

                    } else {

                        $scope.selcontact = $scope.contacts[i + 1];
                        $scope.updatecontact = angular.copy($scope.contacts[i + 1]);


                    }
                    $scope.showEdit = false;

                    $scope.showAddToGroups = false;
                    $scope.showEditGroup = false;
                    $scope.showView = false;
                    $scope.viewAll = true;
                }

            } else {
                $scope.selcontact.$remove(function () {
                    $location.path('contacts');
                });
            }

        };
        // Find a list of Groups
        $scope.findGroups = function () {
            $scope.showEditGroup = false;
            $scope.groups = Groups.query();
        };


        $scope.changeSelGroup = function (group, editgroup) {
            $scope.selectedGroup = group;
            $scope.updateGroup = angular.copy(group);
            //$scope.selectedGroup = group;
            $scope.showEditGroup = false;
            /* $scope.find();*/
            if (group.length !== 0) {
                $scope.allContacts = false;
            } else {
                $scope.allContacts = true;
            }
            if (editgroup === true) {
                $scope.showEditGroup = true;
            } else {
                $scope.showEditGroup = false;
            }
        };
        $scope.updateGroupval = function (isValid) {

            var group = new Groups($scope.group);
            if ($scope.selectedContacts && $scope.selectedContacts.length > 0) {
                $scope.selectedContacts.forEach(function (contact) {
                    group.contacts.push({contact: contact});
                });

            }
            /*  }*/
            group.$update(function () {
                for (var j; j<$scope.groups.length;j++) {
                    if ($scope.groups [j]._id === group._id) {
                        $scope.groups[j] = group;
                        $scope.selectedGroup = group;
                        $scope.updateGroup = angular.copy(group);
                        break;
                    }
                }
                $scope.panelChange('', 'cancelView');
                /* $scope.showEdit = false;

                 $scope.showAddToGroups = false;
                 $scope.showAddToGroups = false;
                 $scope.showEditGroup = false;
                 $scope.showView = false;
                 $scope.viewAll = true;*/

            }, function (errorResponse) {
                $scope.error = errorResponse.data.message;
            });
        };
        $scope.updateGroupSel = function (group) {
            $scope.updateGroup = group;
            $scope.selectedGroup = group;
            $scope.showEditGroup = false;
            /* $scope.find();*/
            if (group.length !== 0) {
                $scope.allContacts = false;
            } else {
                $scope.allContacts = true;
            }
        };

        $scope.filterByDisplayName = function (contacts, typedValue) {
            if (typedValue !== undefined) {
                return contacts.filter(function (contact) {
                    var firstName = contact.firstName.indexOf(typedValue) !== -1;
                    var lastName = contact.lastName.indexOf(typedValue) !== -1;
                    var companyName = contact.lastName.indexOf(typedValue) !== -1;

                    return firstName || lastName || companyName;
                });
            } else {
                return true;
            }
        };
        //$scope.addToGroup = (selcontact)
        // Update existing Contact
        $scope.update = function (updatecontact, selcontact, groupval,form) {
            /*   $scope.updatecontact.contactImageURL = $scope.contactImageURL;*/
            if ($scope.isOneContactRequiredUpdate(updatecontact)) {
                updatecontact.phones = updatecontact.phones.filter(function (phones) {
                    return phones.phoneNumber.length > 0;
                });

                updatecontact.emails = updatecontact.emails.filter(function (emails) {
                    return emails.email.length > 0;
                });

                updatecontact.addresses = updatecontact.addresses.filter(function (addresses) {
                    return !(addresses.addressLine === '' &&
                        addresses.city === '' &&
                        addresses.state === '' &&
                        addresses.country === '' &&
                        addresses.pinCode === '');
                });
                if (updatecontact.paymentTerm instanceof Object) {
                    updatecontact.paymentTerm = updatecontact.paymentTerm._id;
                }
                var contact = angular.copy(updatecontact);

                contact.$update(function () {
                    var j = 0;
                    var removedGroup = true;
                    $scope.getContactsSummaries($scope.selectedIndex);
                    for (j in $scope.contacts) {
                        if ($scope.contacts [j]._id === selcontact._id) {
                            if ($scope.allContacts) {
                                removedGroup = false;
                                //$scope.contacts.splice(j, 1);
                                if($scope.selectedIndex === 0){
                                    if(contact.customerType === 'Customer')
                                        $scope.contacts[j] = contact;
                                    else
                                        $scope.contacts.splice(j,1);
                                }
                                if($scope.selectedIndex === 1){
                                    if(contact.customerType === 'Supplier')
                                        $scope.contacts[j] = contact;
                                    else
                                        $scope.contacts.splice(j,1);
                                }
                                $scope.selcontact = contact;
                                $scope.updateContact = contact;
                                $scope.updatecontact = angular.copy(contact);
                            } else if ($scope.selectedGroup._id && ($scope.updatecontact.groups.indexOf($scope.selectedGroup._id) >= 0)) {
                                removedGroup = false;
                                //$scope.contacts.splice(j, 1);
                                $scope.contacts[j] = contact;
                                $scope.selcontact = contact;
                                $scope.updateContact = contact;
                                $scope.updatecontact = angular.copy(contact);

                            } else if (removedGroup) {
                                $scope.contacts.splice(j, 1);
                                if ($scope.contacts.length > 0) {
                                    //
                                    if (j === '0') {
                                        $scope.selcontact = $scope.contacts[j];
                                        $scope.updatecontact = angular.copy($scope.contacts[j]);

                                    } else if (j <= $scope.contacts.length) {

                                        $scope.selcontact = $scope.contacts[j - 1];
                                        $scope.updatecontact = angular.copy($scope.contacts[j - 1]);

                                    } else {

                                        $scope.selcontact = $scope.contacts[j + 1];
                                        $scope.updatecontact = angular.copy($scope.contacts[j + 1]);


                                    }
                                    $scope.showEdit = false;
                                    $scope.showAddToGroups = false;
                                    $scope.showAddToGroups = false;
                                    $scope.showView = false;
                                    $scope.viewAll = true;
                                }

                            }
                            break;
                        }

                    }

                    /* $location.path('contacts');*/
                    /*	if($scope.groupcontact){
                     $scope.viewAll=false;
                     $scope.showEdit=true;
                     }else{
                     $scope.showEdit=false;
                     $scope.viewAll=true;
                     }*/

                    if (!groupval) {
                        $scope.panelChange(contact, 'cancelView');
                    }
                }, function (errorResponse) {

                    $scope.error = errorResponse.data.message;
                });
            }else{
                $scope.showToast('*Email or phone is required for contact creation','#page-'+$scope.getSelectedPageId(),'error-toast');
            }

        };
        $scope.getSelectedPageId=function () {
            return $scope.selectedPageId;
        };
        $scope.showToast = function (message,position,selector,theme) {

            $mdToast.show(
                $mdToast.simple()
                    .textContent(message)
                    .position(position)
                    .theme(theme)
                    .hideDelay(6000)
                    .parent(angular.element(document.querySelector(selector)))
            );
        };


        $scope.batchOperation=function (type,index) {
            if(type===2) {
                var selectedGroups=contactsQuery.getSelectedGroups($scope);
                if(selectedGroups.groups.length>0){
                    contactsQuery.batchGroup(index,selectedGroups).then(function (responce) {
                        if(index===0){
                            $scope.groups = responce.data.group;
                            $scope.total_count = responce.data.total_count;
                        }else {
                            contactsQuery.getUpdatedGroups($scope, responce.data);

                        }
                    });
                }else{
                    $scope.showToast('Please select at least one Group','bottom right','#page-'+$scope.getSelectedPageId(),'error-toast');
                }
                contactsQuery.batchGroup(index,$scope);
            }else{
                var selectedContacts=contactsQuery.getSelectedContacts(type,$scope);
                if(selectedContacts.contacts.length>0) {
                    contactsQuery.batchContact(index,selectedContacts).then(function (responce) {
                        if(index===0){
                            $scope.contacts = responce.data.contact;
                            $scope.total_count = responce.data.total_count;
                        }else{
                            contactsQuery.getUpdatedContacts($scope, responce.data);

                        }
                        $scope.getContactsSummaries($scope.selectedIndex);
                    },function(err){
                        $scope.showToast(err.data.message,'bottom right','#page-'+$scope.getSelectedPageId(),'error-toast');
                    });
                }else{
                    $scope.showToast('Please select at least one Contact','bottom right','#page-'+$scope.getSelectedPageId(),'error-toast');

                }
            }
        };
        $scope.slice=function(item,type){
            if(type==='contact'){
                type=item.customerType;
                var selectedContacts={'contacts':[],type:type};
                selectedContacts.contacts.push({_id:item._id});
                contactsGroupActionQuery.removeContact(selectedContacts,$scope);
            }
            if(type==='group'){
                var selectedGroups={'groups':[]};
                selectedGroups.push({_id:item._id});
                contactsGroupActionQuery.removeGroup(selectedGroups,$scope);
            }
        };
        $scope.updateStatus = function (updatecontact, selcontact) {
            var contact = angular.copy(updatecontact);
            contact.disabled = !contact.disabled;
            contact.$update(function () {
                var j = 0;
                var removedGroup = true;
                for (j in $scope.contacts) {
                    if ($scope.contacts [j]._id === selcontact._id) {
                        if ($scope.allContacts) {
                            removedGroup = false;
                            $scope.contacts.splice(j, 1);
                            $scope.contacts.push(contact);
                            $scope.selcontact = contact;
                            $scope.updateContact = contact;
                            $scope.updatecontact = angular.copy(contact);
                        } else if ($scope.selectedGroup._id && ($scope.updatecontact.groups.indexOf($scope.selectedGroup._id) >= 0)) {
                            removedGroup = false;
                            $scope.contacts.splice(j, 1);
                            $scope.contacts.push(contact);
                            $scope.selcontact = contact;
                            $scope.updateContact = contact;
                            $scope.updatecontact = angular.copy(contact);

                        } else if (removedGroup) {
                            $scope.contacts.splice(j, 1);
                            if ($scope.contacts.length > 0) {
                                //
                                if (j === '0') {
                                    $scope.selcontact = $scope.contacts[j];
                                    $scope.updatecontact = angular.copy($scope.contacts[j]);

                                } else if (j <= $scope.contacts.length) {

                                    $scope.selcontact = $scope.contacts[j - 1];
                                    $scope.updatecontact = angular.copy($scope.contacts[j - 1]);

                                } else {

                                    $scope.selcontact = $scope.contacts[j + 1];
                                    $scope.updatecontact = angular.copy($scope.contacts[j + 1]);


                                }
                                $scope.showEdit = false;

                                $scope.showAddToGroups = false;
                                $scope.showAddToGroups = false;
                                $scope.showView = true;
                            }

                        }
                        break;
                    }

                }
                /* $location.path('contacts');*/
                $scope.panelChange(contact, 'cancelView');
            }, function (errorResponse) {
                $scope.error = errorResponse.data.message;
            });


        };
        // Find a list of Groups
        $scope.findGroup=function (pageno) {
            var paginationfield={};
            if(!isNaN(pageno)){
                paginationfield={page:pageno,limit:$scope.itemsPerPage};
            }
            if(this.searchText && this.searchText.length>0){
                paginationfield.searchText=this.searchText;
            }
            contactsQuery.getGroups($scope,paginationfield).then(function (results) {
                $scope.total_count = results.data.total_count;
                $scope.groups=results.data.group;
            });
        };

        $scope.getGroupContacts=function (group,contacts,done) {
            if(group.contacts.length>0){
                angular.forEach(group.contacts,function (contact) {
                    if(contact.contact){
                        contacts.push(contact.contact);
                    }

                },function (err) {
                    if(err) {
                        done(err, null);
                    }else{
                        done(null,contacts);
                    }
                });
            }else{
                done(null,contacts);
            }
        };


        // Find a list of Contacts
        $scope.find = function (isCustomer,pageno,group,tabPosition) {

            $scope.isFilterSummary=false;
            var paginationfield={};
            if(!isNaN(pageno)){
                paginationfield={page:pageno,limit:$scope.itemsPerPage};
            }
            if(this.searchText && this.searchText.length>0){
                paginationfield.searchText=this.searchText;
            }
            $scope.contacts=[];
            $scope.selfcontact=[];
            if(group && (group.groupClassification.classificationType==='Criteria')){
                $scope.gcontacts=[];
                $scope.getGroupContacts($scope.group,$scope.gcontacts,function (err,finalContacts) {
                    $scope.gcontacts=finalContacts;

                });
            }else {
                contactsQuery.getContacts(isCustomer, paginationfield, false).then(function (results) {
                    $scope.total_count = results.data.total_count;
                    if (group && group.contacts.length > 0) {
                        $scope.gcontacts=[];
                        angular.forEach(results.data.contact, function (contact) {
                            if (contact.nVipaniUser !== null && (contact.nVipaniRegContact !== null && contact.nVipaniRegContact === true && (contact.nVipaniUser === contact.user || contact.nVipaniUser === contact.user._id))) {
                                $scope.selfcontact.push(contact);
                            } else {
                                if (group.contacts.filter(function (groupContact) {
                                    return groupContact.contact._id === contact._id;

                                }).length > 0) {
                                    contact.selected = true;
                                    $scope.gcontacts.push(contact);
                                } else {
                                    $scope.gcontacts.push(contact);
                                }

                            }
                        });
                    } else {
                        setContacts(results.data.contact);
                    }
                    if(results.data.summaryData){
                        if($scope.filterKey){
                            setContacts(results.data.summaryData[$scope.filterKey]);
                        }else {
                            $scope.contactfilters = results.data.summaryData;
                        }
                    }
                }, function (res) {
                    $scope.error = res.data.message;

                });
            }

        };

        // Find a list of Contacts
        $scope.findContacts = function (isCustomer,pageno) {
            var paginationfield={};
            if(!isNaN(pageno)){
                paginationfield={page:pageno,limit:$scope.itemsPerPage};
            }
            if(this.searchText && this.searchText.length>0){
                paginationfield.searchText=this.searchText;
            }
            $scope.contacts=[];
            $scope.selfcontact=[];
            contactsQuery.getContacts(isCustomer,paginationfield,true).then(function (results) {



            });
        };
        $scope.showGroupContacts = function () {
            return ($scope.contacts !== undefined || $scope.contacts.length > 0);
        };

        $scope.searchFilter = function (item) {
            var re = new RegExp($scope.searchText, 'i');
            var phone=[];
            var email=[];
            if(item.phones.length>0) {
                phone = item.phones.filter(function (eachphone) {
                    return eachphone.phoneNumber.indexOf($scope.searchText) !== -1;
                });
            }
            if(item.emails.length>0){
                email=item.emails.filter(function (eachemail) {
                    return eachemail.email.indexOf($scope.searchText) !== -1;
                });
            }
            return !$scope.searchText || re.test(item.firstName) || re.test(item.lastName) || re.test(item.middleName) || re.test(item.firstName +' '+item.middleName + ' '+item.lastName) ||re.test(item.companyName)|| (phone.length>0 && re.test(phone[0].phoneNumber)) ||(email.length>0 && re.test(email[0].email));
        };
        $scope.filterGroup = function (item) {
            var re = new RegExp($scope.searchText, 'i');
            return !$scope.searchText || re.test(item.name);
        };
        $scope.showOnlyGroupEdit = function () {
            return ($scope.group !== undefined && $scope.Edit.length > 0);
        };
        $scope.showSelection = function () {
            return $scope.selcontact !== undefined;
        };
        // Find existing Contact
        $scope.findOne = function (id) {
            $scope.contact = Contacts.get({
                contactId: $stateParams.contactId?$stateParams.contactId:id
            });
            if(!$scope.paymentTerms || $scope.paymentTerms.length===0){
                $scope.paymentTerms();
            }

            /* $scope.contact.$promise.then(function (result) {
                 $scope.contactImageURL = result.contactImageURL;
             });*/
        };
        /* $scope.findOneGroups = function (id) {
             $scope.group = Groups.get({
                 groupId: id
             }).then(function (results) {
                 $scope.group = results.data;
                 $scope.addContacts($scope.group)
             }, function (err) {
                 $scope.error = err.message;
             });
         };*/
        $scope.groupContacts=function (contacts,group) {
            if(group) {
                group.contacts.filter(function (groupContact) {
                    angular.forEach(contacts,function (contact) {
                        if (groupContact.contact._id===contact._id) {
                            contact.selected=true;
                        }

                    });
                });
                return contacts;
            }else{
                var selectedContacts=[];

                contacts.filter(function (contact) {
                    if (contact.selected) {
                        selectedContacts.push({contact: contact._id});
                    }

                });
                return selectedContacts;
            }

        };
        $scope.findContatById = function (id) {
            $scope.contact = Contacts.get({
                contactId: id
            });
        };
        $scope.groupcreate = function (form) {
            var errorMessage;
            if(!form.$valid){
                if(!this.name || this.name===''){
                    errorMessage='Group name is required';
                }
                else if(!this.groupType || this.groupType===''){
                    errorMessage = 'Group type is required';
                }
                else if(this.groupClassification.classificationType === 'Criteria' && this.groupClassification.classificationCriteria.criteriaType ==='Custom' && (!this.groupClassification.classificationCriteria.location.pinCode || this.groupClassification.classificationCriteria.location.pinCode==='')){
                    errorMessage = 'Pincode is required for location criteria';
                }
                if(errorMessage) {
                    $scope.showToast(errorMessage,'bottom right','#page-'+$scope.getSelectedPageId(),'error-toast');

                }

            }
            else {
                // Create new Group object
                $scope.error = null;
                var group = new Groups({
                    name: this.name,
                    description: this.description,
                    groupType: this.groupType,
                    groupImageURL: this.groupImageURL
                });

                if (this.groupClassification) {
                    var newGroupClassification = angular.copy(this.groupClassification);
                    if (newGroupClassification.classificationType === 'Criteria') {
                        group.groupClassification = newGroupClassification;
                    }
                    else if (this.groupClassification.classificationType === 'All') {
                        newGroupClassification.classificationCriteria = 'All';
                        newGroupClassification.classificationType = 'Criteria';
                        group.groupClassification = newGroupClassification;
                    } else {
                        var selectedContacts = $scope.groupContacts($scope.contacts, null);
                        if (selectedContacts.length > 0) {
                            group.contacts = selectedContacts;
                        }
                    }
                }

                // Redirect after save
                group.$save(function (response) {
                    /*$scope.clearFields();*/
                    //form.$setPristine();
                    $scope.addParticipents = false;
                    /*$location.path('contacts');*/

                    // Clear form fields
                    /* $scope.clearFields();*/

                    //update current list
                    $scope.panelChange('', 'cancelView');
                    /*$scope.groups.push(response);*/
                    /*  addParticipents
                      $scope.selectedIndex=2;*/
                    $scope.changeSelectionTab($scope.busUnitId, 'Groups');
                    /* $scope.allContacts = false;*/

                    /*    $scope.selectedGroup = response;
                        $scope.panelChange('', '');
                        $scope.contacts = [];*/
                    /*$location.path('contacts');*/
                }, function (errorResponse) {
                    $scope.showToast(errorResponse.data.message,'bottom right','#page-'+$scope.getSelectedPageId(),'error-toast');

                });
            }

        };
        $scope.filterGroupContacts = function(selectedCat,group){
            return selectedCat.filter(function (contact) {
                contact.selected=false;
                angular.forEach(group.contacts,function (groupContact) {
                    if(groupContact.contact._id===contact._id){
                        contact.selected=true;
                    }

                });
                return contact.selected===false;
            });
        };
        $scope.queryContactsForGroups = function(selectedCat,query){
            var alreadySelectedContacts  = $scope.filterGroupContacts(selectedCat,$scope.group);
            return criteriaChips.querySearchContacts(alreadySelectedContacts,query);
        };
        $scope.addContacts=function (group) {
            $scope.addParticipents = true;
            if (group !== null && angular.isObject(group)) {
                $scope.updategroup = angular.copy(group);
                /* if(group.groupType==='Customer'){
                     $scope.paginationId='GroupCustomers';
                 }else{
                     $scope.paginationId='GroupSuppliers';
                 }*/

                $scope.find(group.groupType === 'Customer' ? true : false, $scope.pageno, group);

            } else {
                /*if($scope.groupType==='Customer'){
                    $scope.paginationId='GroupCustomers';
                }else{
                    $scope.paginationId='GroupSuppliers';
                }*/
                $scope.find(this.groupType === 'Customer' ? true : false, $scope.pageno, group);
            }
        };
        $scope.addPhone = function (phonesArray) {
            phonesArray.push({
                phoneNumber: '',
                phoneType: 'Mobile',
                primary: false
            });
        };

        $scope.addEmail = function (emailsArray) {
            emailsArray.push({
                email: '',
                emailType: 'Work',
                primary: false
            });
        };

        $scope.addAddress = function (addressesArray, type) {
            if (type) {
                addressesArray.push({
                    addressLine: '',
                    city: '',
                    state: '',
                    country: '',
                    pinCode: '',
                    addressType: type,
                    primary: false
                });
            }
            else {
                addressesArray.push({
                    addressLine: '',
                    city: '',
                    state: '',
                    country: '',
                    pinCode: '',
                    addressType: 'Billing',
                    primary: false
                });
            }
        };
        $scope.changeSelContact = function (contact) {
            $scope.selcontact = contact;
            /*$scope.like=!$scope.like;*/
            $scope.updatecontact = angular.copy(contact);
            if (!$scope.paymentTerms || $scope.paymentTerms.length === 0) {
                $scope.paymentTerms();
            }

        };

        $scope.showEdit = false;
        $scope.showAdd = false;
        $scope.showView = false;
        $scope.showAddToGroups = false;
        $scope.showAddGroup = false;
        $scope.showEditGroup = false;

        $scope.init = function () {
            $scope.panelChange('', '');
        };

        $scope.panelChange = function (contact, changeStr, tabPosition) {
            $scope.context = [
                {title: 'Contacts', link: ''}
            ];
            $scope.showEdit = false;
            $scope.showAdd = false;
            $scope.showAddToGroups = false;
            $scope.showAddGroup = false;
            $scope.showEditGroup = false;
            $scope.showView = false;
            $scope.importView = false;
            $scope.viewAll = true;
            $scope.error = null;
            $scope.addParticipents = false;
            $scope.contactImageURL = 'modules/contacts/img/default.png';
            if (changeStr === 'editgroup') {
                $scope.showEditGroup = true;

                $scope.groupTypes = [{text: 'Customer', value: 'Customer'}, {
                    text: 'Supplier',
                    value: 'Supplier'
                }/*, {
                        text: 'Other',
                        value: 'Others'
                    }*/];
                //$scope.selectedGroup=contact;
                if (contact !== null && angular.isObject(contact))
                    $scope.updategroup = angular.copy(contact);
                contactsQuery.findGroupById(contact._id).then(function (results) {
                    $scope.group = results.data;
                    var isCustomer = $scope.group.groupType === 'Customer' ? true : false;
                    $scope.selectedContact = [];
                    $scope.contactsVsGroup = [];
                    criteriaChips.loadChipContacts($scope, true, isCustomer, true, $scope.selectedContact, contactsQuery);
                    $scope.find(isCustomer, $scope.pageno, $scope.group);
                    //$scope.contacts=
                    /*$scope.addContacts($scope.group)*/
                }, function (err) {
                    $scope.error = err.message;
                });

            } else if (changeStr === 'addContactGroup') {
                $scope.showEditGroup = true;
                $scope.addParticipents = true;

                //$scope.selectedGroup=contact;
                if (contact !== null && angular.isObject(contact)) {
                    $scope.updategroup = angular.copy(contact);

                    $scope.find(contact.groupType === 'Customer' ? true : false, $scope.pageno);
                } else {
                    $scope.find($scope.groupType === 'Customer' ? true : false, $scope.pageno);
                }
                /* $scope.showEditGroup = true;*/


            } else if (changeStr === 'edit') {
                if (!$scope.groupTypes) {
                    $scope.groupTypes = [{text: 'Customer', value: 'Customer'}, {
                        text: 'Supplier',
                        value: 'Supplier'
                    }/*, {
                        text: 'Other',
                        value: 'Others'
                    }*/];
                }
                $scope.showEdit = true;
                $scope.importView = false;
                $scope.findOne(contact._id);

            } else if (changeStr === 'add') {
                $scope.clearFields();
                $scope.showAddGroup = false;
                $scope.importView = false;
                $scope.showAdd = true;
                $scope.customerType = 'Customer';
                if ($scope.selectedIndex === 1) {
                    $scope.customerType = 'Supplier';
                }
            } else if (changeStr === 'addToGroups') {
                $scope.showAddToGroups = true;
            } else if (changeStr === 'cancelView') {
                $mdDialog.cancel();
            } else if (changeStr === 'group') {
                $scope.groupTypes = [{text: 'Customers', value: 'Customer'}, {
                    text: 'Suppliers',
                    value: 'Supplier'
                }];
                if (tabPosition === 0) {
                    $scope.groupType = 'Customer';
                }
                else if (tabPosition === 1) {
                    $scope.groupType = 'Supplier';
                } else if (tabPosition === 2) {
                    $scope.contacts = [];
                    $scope.groupType = null;
                }
                $scope.groupClassification = {
                    classificationTypes: [{text: 'Custom', value: 'Custom'}, {
                        text: 'Location Criteria',
                        value: 'Criteria'
                    }, {text: 'All', value: 'All'}],
                    classificationType: 'Custom',
                    classificationCriteria: {
                        criteriaTypes: [{text: 'Custom', value: 'Custom'}, {
                            text: 'All',
                            value: 'All'
                        }],
                        criteriaType: 'Custom',
                        location: [{city: '', state: '', country: '', pinCode: ''}]
                    }
                };
                $scope.importView = false;
                $scope.showAddGroup = true;
            } else if (changeStr === 'import') {
                $scope.showAdd = false;
                $scope.showEdit = false;
                $scope.showAddGroup = false;
                $scope.showEditGroup = false;
                $scope.importView = true;
                $scope.contactImportErrors = {};
                $scope.contactImports = {};
                $scope.contactImports.errors = [];
                $scope.errorsummary = '';
                $scope.contactHeaderError = {};
            } else {
                $scope.changeSelectionTab($scope.busUnitId, 'Customers');
            }
            if ($scope.viewAll && ($scope.showAdd || $scope.showEdit || $scope.showAddGroup || $scope.showAddToGroups || $scope.showEditGroup || $scope.importView) && !$scope.addParticipents) {
                $mdDialog.show({
                    template: '<div class="md-dialog-container trowserView" tabindex="-1"><md-dialog class="trowser"><add-contact-new ng-show="showAdd"></add-contact-new>\n' +
                    '            <edit-contact-new ng-if="showEdit"></edit-contact-new>' +
                    '            <import-contact ng-if="importView"></import-contact>\n' +
                    '            <add-group-contact  ng-if="showAddGroup"></add-group-contact>\n' +
                    '            <edit-contact-group ng-if="showAddToGroups && contacts.length>0"></edit-contact-group>\n' +
                    '            <edit-group ng-if="showEditGroup"></edit-group></md-dialog></div>',
                    /* targetEvent: ev,*/
                    scope: $scope,
                    clickOutsideToClose: true,
                    preserveScope: true
                });
            }


        };

        $scope.group = false;
        $scope.allContacts = true;

        $scope.contactfullName = function (selectContact) {
            if (selectContact) {
                if (selectContact.displayName) {
                    return selectContact.displayName;
                }
                else {
                    return selectContact.firstName + ' ' + selectContact.middleName + ' ' + selectContact.lastName;
                }
            } else {
                return '';
            }


        };
        $scope.paymentTerms = function () {
            if (!$scope.paymentTerms || $scope.paymentTerms.length === 0) {
                PaymentTerms.findPaymentTerms().then(function (results) {
                    $scope.paymentterms = results.data;
                }, function (err) {
                    $scope.error = err.message;
                });
            }
        };

        $scope.contactDetails = function (selectContact) {
            var details = '';
            if (selectContact.companyName.length > 0) {
                details = selectContact.companyName;
            }
            return details;

        };

        //copies selContact into copySelContact
        $scope.createCopy = function (selcontact) {
            $scope.copySelContact = angular.copy(selcontact);
            $scope.updateContact = angular.copy(selcontact);
        };

        /*	$scope.isFirstNameRequired = function (firstname) {
         if ($scope.firstName) {
         return false;
         } else {
         if (!$scope.error) {
         $scope.error = null;
         $scope.error = '* FirstName is must field for the contact creation';
         }
         return true;
         }
         };*/
        function validContactPhones(phones) {
            return phones.filter(function (eachPhone) {
                return eachPhone.phoneNumber && eachPhone.phoneNumber.trim().length>0;
            }).length;

        }

        function validContactEmails(emails) {
            return emails.filter(function (eachEmail) {
                return eachEmail.email && eachEmail.email.trim().length > 0;
            }).length;

        }

        $scope.isOneContactRequired = function () {

            if (validContactEmails($scope.emails) > 0 || validContactPhones($scope.phones) > 0) {
                return true;
            } else {

                $scope.error = '*Email or phone is required for contact creation';

                return false;
            }


        };

        $scope.isOneContactRequiredUpdate = function (contact) {
            if (validContactEmails(contact.emails) > 0 || validContactPhones(contact.phones) > 0) {
                return true;
            } else {
                return false;
            }

        };

        //reverts the changes of selcontact to its version in copyselContact
        $scope.revert = function () {
            for (var i in $scope.contacts) {
                if ($scope.contacts [i] === $scope.selcontact) {
                    $scope.contacts [i] = $scope.copySelContact;
                    //$scope.contacts[i]=$scope.updateContact;
                    $scope.selcontact = $scope.contacts [i];
                    $scope.updatecontact = angular.copy($scope.contacts [i]);
                    break;
                }
            }
        };


        $scope.predicate = 'firstName';


        /* $scope.$on('$viewContentLoaded', function() {
             //call it here
             $scope.pageloaded = true;
             alert("call on image load");
         });*/

        $scope.$on('initialised', function () {
            //call it here
            $scope.pageloaded = true;

        });

        $scope.removeGroupContact = function (index) {
            $scope.group.contacts.splice(index, 1);

        };


        /*$scope.viewContactDetails = $window.innerWidth > 768;
         $scope.viewGroups = $window.innerWidth > 1200;

         $window.onresize = function(event) {
         $scope.viewContactDetails = $window.innerWidth > 768;
         $scope.viewGroups = $window.innerWidth >	 1200;
         };

         $scope.toggleView = function(){
         if($window.innerWidth < 768){
         $scope.viewContactDetails = !$scope.viewContactDetails;
         }
         };

         $scope.toggleGroupView = function(){
         if($window.innerWidth < 1200){
         $scope.viewGroups = !$scope.viewGroups;
         }
         };*/

    }
]);

/*app.controller('ContactsViewController'[$scope,Contacts,Group,function($scope,Contacts,Group){

}]);*/
app.directive('initialisation',['$rootScope',function($rootScope) {
    return {
        restrict: 'A',
        link: function($scope) {
            var to;
            var listener = $scope.$watch(function() {
                clearTimeout(to);
                to = setTimeout(function () {
                    console.log('initialised');
                    listener();
                    $rootScope.$broadcast('initialised');
                }, 30);
            });
        }
    };
}]);

app.directive('importContact', function(){
    return{
        restrict: 'E',
        templateUrl: 'modules/contacts/views/contact/import-contact.client.view.html'
    };
});
app.directive('editContact', function () {
    return {
        restrict: 'E',
        templateUrl: 'modules/contacts/views/contact/edit-contact.client.view.html'
    };
});

app.directive('editContactNew', function () {
    return {
        restrict: 'E',
        templateUrl: 'modules/contacts/views/contact/edit-contacts.client.viewnew.html'
    };
});

app.directive('editContactGroup', function () {
    return {
        restrict: 'E',
        templateUrl: 'modules/contacts/views/groups/edit-contact-groups.client.view.html'
    };
});

app.directive('viewContact', function () {
    return {
        restrict: 'E',
        templateUrl: 'modules/contacts/views/contact/view-contact.client.view.html'
    };
});

app.directive('listContact', function () {
    return {
        restrict: 'E',
        templateUrl: 'modules/contacts/views/contact/list-contacts.client.view.html'
    };
});

app.directive('addContact', function () {
    return {
        restrict: 'E',
        templateUrl: 'modules/contacts/views/contact/create-contact.client.view.html'
    };
});

app.directive('addContactNew', function () {
    return {
        restrict: 'E',
        templateUrl: 'modules/contacts/views/contact/create-contact.client.viewnew.html'
    };
});

app.directive('addGroupContact', function () {
    return {
        restrict: 'E',
        templateUrl: 'modules/contacts/views/groups/create-group.client.view.html'
    };
});

app.directive('editGroup', function () {
    return {
        restrict: 'E',
        templateUrl: 'modules/contacts/views/groups/edit-group.client.view.html'
    };
});
app.directive('viewGroup', function () {
    return {
        restrict: 'E',
        templateUrl: 'modules/contacts/views/groups/view-group.client.view.html'
    };
});

app.directive('viewContacts', function () {
    return {
        restrict: 'E',
        templateUrl: 'modules/contacts/views/contact/contacts-nvc.client.viewnew.html'
    };
});
app.directive('viewSingleGroupContacts', function () {
    return {
        restrict: 'E',
        scope:{
            contacts: '=gcontacts',
            isGroup:'=isgroup',
            isCriteria:'=iscriteria'
        },
        templateUrl: 'modules/contacts/views/contact/contacts-nvc.client.viewnew.html'
    };
});
app.directive('viewContactCards', function(){
    return{
        restrict: 'E',
        templateUrl: 'modules/contacts/views/contact/contact-cards-nvc.client.viewnew.html'
    };
});
app.directive('viewGroupContacts', function () {
    return {
        restrict: 'E',
        templateUrl: 'modules/contacts/views/contact/contacts-group-nvc.client.viewnew.html'
    };
});
app.directive('viewGroups', function () {
    return {
        restrict: 'E',
        templateUrl: 'modules/contacts/views/groups/groups-nvc.client.viewnew.html'
    };
});
app.directive('onlyDigits', function () {
    return {
        restrict: 'A',
        require: '?ngModel',
        link: function (scope, element, attrs, ngModel) {
            if (!ngModel) return;
            ngModel.$parsers.unshift(function (inputValue) {
                var digits = inputValue.split('').filter(function (s) {
                    return (!isNaN(s) && s !== ' ');
                }).join('');
                ngModel.$viewValue = digits;
                ngModel.$render();
                return digits;
            });
        }
    };
});
app.directive('inputValidateNum',function(){
    return{
        restrict:'A',
        require:'?ngModel',
        scope:{
            minlength:'=minlength',
            maxlength:'=maxlength'
        },
        link: function(scope, element, attrs, modelCtrl) {

            modelCtrl.$parsers.push(function (inputValue) {
                var transformedInput = inputValue ? inputValue.replace(/[^\d.]/g,'') : null;

                if (transformedInput!==inputValue) {
                    modelCtrl.$setViewValue(transformedInput);
                    modelCtrl.$render();
                }

                /*   if(inputValue.length<attrs.minlength && inputValue.length>attrs.maxlength){
                       return 'enter valid input';
                   }*/
                return transformedInput;
            });
        }
    };
});
app.directive('inputValidateNum1',function(){
    return{
        restrict:'A',
        require:'?ngModel',
        link: function(scope, element, attrs, modelCtrl) {
            modelCtrl.$parsers.push(function (inputValue) {
                var transformedInput = inputValue ? inputValue.replace(/[^a-zA-Z0-9._-]/g,'') : null;
                if (transformedInput!==inputValue) {
                    modelCtrl.$setViewValue(transformedInput);
                    modelCtrl.$render();
                }
                return transformedInput;
            });
        }
    };
});
app.directive('nanuEmail', function () {
    return {
        restrict: 'A',
        require: 'ngModel',
        link: function (scope, elm, attrs, model) {
            //change this:
            var EMAIL_REGEXP = /^[_a-z0-9]+(\.[_a-z0-9]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,4})$/;
            var emailValidator = function (value) {
                if (!value || EMAIL_REGEXP.test(value)) {
                    model.$setValidity('email', true);
                    return value;
                } else {
                    model.$setValidity('email', false);
                    return undefined;
                }
                model.$parsers.push(emailValidator);
                model.$formatters.push(emailValidator);
            };
        }
    };
});

app.directive('scrollToItem', function () {
    return {
        restrict: 'A',
        scope: {
            scrollTo: '@'
        },
        link: function (scope, elm, attr) {

            elm.on('click', function () {
                angular.element('html,body').animate({scrollTop: angular.element(scope.scrollTo).offset().top}, 'slow');
            });
        }
    };
});


/*app.directive('limitTo', [function() {
    return {
        restrict: 'AE',
        link: function(scope, elem, attrs,ngModel) {
            var limit = parseInt(attrs.limitTo);
            angular.element(elem).on('keydown', function(event) {
                var s = 223;
                var a = 229;

                var charCode = (event.which) ? event.which : event.keyCode;
                alert(charCode);
                if(charCode === 8 || charCode === 46){

                    return true;
                }else if (this.value.length >limit) {

                    return false;
                }else{

                    return true;
                }
            });

           /!* function isNumber(evt) {
                evt = (evt) ? evt : window.event;
                var charCode = (evt.which) ? evt.which : evt.keyCode;
                if (charCode > 31 && (charCode < 48 || charCode > 57)) {
                    return false;
                }
                return true;
            }*!/
        }
    };
}]);
app.directive('regExpRequire', function() {

    var regexp;
    return {
        restrict: 'A',
        link: function(scope, elem, attrs) {
            regexp = eval(attrs.regExpRequire);

            var char;
            elem.on('keypress', function(event) {
                char = String.fromCharCode(event.which)
                if(!regexp.test(elem.val() + char))
                    event.preventDefault();
            })
        }
    };

});
app. directive('confirmClick', ['$q', 'dialogModal', function($q, dialogModal) {
    return {
        link: function (scope, element, attrs) {
            // ngClick won't wait for our modal confirmation window to resolve,
            // so we will grab the other values in the ngClick attribute, which
            // will continue after the modal resolves.
            // modify the confirmClick() action so we don't perform it again
            // looks for either confirmClick() or confirmClick('are you sure?')
            var ngClick = attrs.ngClick.replace('confirmClick()', 'true')
                .replace('confirmClick(', 'confirmClick(true,');

            // setup a confirmation action on the scope
            scope.confirmClick = function(msg) {
                // if the msg was set to true, then return it (this is a workaround to make our dialog work)
                if (msg===true) {
                    return true;
                }
                // msg can be passed directly to confirmClick('are you sure?') in ng-click
                // or through the confirm-click attribute on the <a confirm-click="Are you sure?"></a>
                msg = msg || attrs.confirmClick || 'Are you sure?';
                // open a dialog modal, and then continue ngClick actions if it's confirmed
                dialogModal(msg).result.then(function() {
                    scope.$eval(ngClick);
                });
                // return false to stop the current ng-click flow and wait for our modal answer
                return false;
            };
        }
    };
}]);*/

app.directive('limitTo1',function () {
    return {
        restrict: 'AE',
        scope: {
            watched: '=',
            val: '='
        },
        template: '<span class="text-danger f-15" data-ng-show="addContactForm.firstName.$dirty && addContactForm.firstName.$invalid"><span data-ng-show="addContactForm.firstName.$error.required">First Name is required.</span> <span data-ng-show="addContactForm.firstName.$error.maxlength">First Name length should not exceed 128 characters.</span>',
        link: function (scope, elem,attrs) {
            var limit = parseInt(attrs.limitTo);
            scope.$watch('val',function(newVal,oldVal){
                if(!newVal) return;
                scope.countToType = limit-newVal.length;
                /*if(scope.countToType <0){

                }*/

            });
            elem.on('keypress', function(event) {
                if(scope.countToType <0){
                    event.preventDefault();
                }
            });
        }
    };
});


app.directive('limitTo',function () {
    return {
        restrict: 'A',
        scope: {
            watched: '=',
            val: '='
        },
        link: function (scope, elem,attrs) {
            var limit = parseInt(attrs.limitTo);
            scope.$watch('val',function(newVal,oldVal){
                /*if(!newVal) return;*/
                scope.countToType = limit-newVal.length;
                /*if(scope.countToType <0){

                 }*/

            });
            elem.on('keypress', function(event) {
                if(scope.countToType <0){
                    event.preventDefault();
                }
            });
            elem.on('keyup', function(e){
                if(scope.countToType >0){
                    elem.val('Akila');
                }
            });
        }
    };
});
function isEmpty(value) {
    return angular.isUndefined(value) || value === '' || value === null || value !== value;
}
app.directive('ngMin', function () {
    return {
        restrict: 'A',
        require: 'ngModel',
        link: function (scope, elem, attr, ctrl) {
            scope.$watch(attr.ngMin, function () {
                ctrl.$setViewValue(ctrl.$viewValue);
            });
            var minValidator = function (value) {
                var min = attr.ngMin || 0;
                if (!isEmpty(value) && value < min) {
                    ctrl.$setValidity('ngMin', false);
                    return undefined;
                } else {
                    ctrl.$setValidity('ngMin', true);
                    return value;
                }
            };

            ctrl.$parsers.push(minValidator);
            ctrl.$formatters.push(minValidator);
        }
    };
});

app.directive('ngMax', function () {
    return {
        restrict: 'A',
        require: 'ngModel',
        link: function (scope, elem, attr, ctrl) {
            scope.$watch(attr.ngMax, function () {
                ctrl.$setViewValue(ctrl.$viewValue);
            });
            var maxValidator = function (value) {
                var max =attr.ngMax|| Infinity;
                if (!isEmpty(value) && value > max) {
                    ctrl.$setValidity('ngMax', false);
                    return undefined;
                } else {
                    ctrl.$setValidity('ngMax', true);
                    return value;
                }
            };

            ctrl.$parsers.push(maxValidator);
            ctrl.$formatters.push(maxValidator);
        }
    };
});



app.directive('validationMessage', function () {
    return{
        restrict: 'A',
        template: '<input tooltip tooltip-placement="bottom" >',
        replace: true,
        require: 'ngModel',
        link: function (scope, element, attrs, ctrl) {

            ctrl.$parsers.unshift(function(viewValue) {
                var valid = ctrl.$valid;
                if (valid) {
                    attrs.$set('tooltip', '');
                } else {
                    attrs.$set('tooltip', attrs.validationMessage);
                    scope.tt_isOpen = true; // doesn't work!?
                }
                return viewValue;
            });
        }
    };
})
    .directive('numberFormat', function() {
        return {
            require: 'ngModel',
            link: function(scope, element, attrs, ctrl) {
                ctrl.$parsers.unshift(function (viewValue) {
                    var invalidNumber = /^[0-9]+$/.test(viewValue);
                    if (invalidNumber || viewValue === ''){
                        ctrl.$setValidity('number', true);
                    } else {
                        ctrl.$setValidity('number', false);
                    }
                });
            }
        };
    });

app.directive('floatNumber', function() {
    return {
        require: '?ngModel',
        link: function(scope, element, attrs, ngModelCtrl) {
            if(!ngModelCtrl) {
                return;
            }

            ngModelCtrl.$parsers.push(function(val) {
                if (angular.isUndefined(val)) {
                    val = '';
                }

                var clean = val.replace(/[^-0-9\.]/g, '');
                var decimalCheck = clean.split('.');
                if(!angular.isUndefined(decimalCheck[1])) {
                    decimalCheck[1] = decimalCheck[1].slice(0,2);
                    clean =decimalCheck[0] + '.' + decimalCheck[1];
                }

                if (decimalCheck.length===2 && (decimalCheck[0].length <= 2 || (decimalCheck[0].length > 2 && decimalCheck[1]==='0' || decimalCheck[1].length==='00'))) {
                    clean=clean;
                }else if(decimalCheck.length===1 && decimalCheck[0]>0 && decimalCheck[0]<=100) {
                    clean= clean;
                }else{
                    clean='';
                }
                ngModelCtrl.$setViewValue(clean);
                ngModelCtrl.$render();
                return clean;
            });

            element.bind('keypress', function(event) {
                if(event.keyCode === 32) {
                    event.preventDefault();
                }
            });
        }
    };
});
