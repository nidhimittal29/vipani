'use strict';


// Inventories controller
var app=angular.module('inventories');
app.controller('InventoriesController', ['$filter','$scope','$http', '$stateParams', '$location', '$compile','localStorageService', 'Authentication', 'Inventories','categoryService','inventoryProduct','Products','Offers','Orders','verifyDelete','$mdToast','$mdDialog','$mdMenu', '$mdMedia','contactsQuery','alert','productBrandService','CompanyService','criteriaChips', '$timeout','inventoryProductConversion','inventoryImport',
    function ($filter,$scope,$http, $stateParams, $location,$compile,localStorageService,Authentication, Inventories,categoryService,inventoryProduct,Products,Offers,Orders,verifyDelete,$mdToast,$mdDialog,$mdMenu, $mdMedia,contactsQuery,alert,productBrandService,CompanyService,criteriaChips,$timeout,inventoryProductConversion,inventoryImport) {
        $scope.authentication = Authentication;
        $scope.user = Authentication.user;
        $scope.convertType='Manufactured';
        if(!$scope.user){
            $location.path('/#!/signin');
        }
    $scope.busUnitId=$scope.authentication.companyBusinessUnits.defaultBusinessUnitId;
        $scope.unitMeasureTypes = ['Kg', 'gram', 'litre', 'ml', 'count', 'tonne', 'Bag', 'Box', 'Carton', 'Bale', 'Candy', 'Bundle', 'Case', 'metre', 'dozen', 'inch', 'foot', 'Pack', 'Tray', 'Other'];
        $scope.selectedItem = {};
        $scope.autoopen = false;
        $scope.pages = [];
        $scope.selectedMatchedProducts = 0;
        $scope.location=$location;
        $scope.company={};

        $scope.criteriaChips=criteriaChips;


        $scope.transferType=function (type) {
          $scope.convertType= type;
          if(type==='Repacked'){
              $scope.subCategory1='';
              $scope.subCategory2='';
              $scope.productBrand='';
          }else if(type==='Graded'){
              $scope.subCategory1='';
              $scope.subCategory2='';
              $scope.productBrand='';
          }
        };
        // Create new Inventory
        $scope.create = function () {
            // Create new Inventory object
            var inventory = new Inventories({
                name: this.name
            });

            // Redirect after save
            inventory.$save(function (response) {
                $location.path('inventories/' + response._id);

                // Clear form fields
                $scope.name = '';
            }, function (errorResponse) {
                $scope.error = errorResponse.data.message;
            });
        };
        /*for closing mdDialog*/
        $scope.cancel = function(){
           /* window.location.reload(false);*/
            $mdDialog.cancel();
        };
        $scope.updateInv = function(inventory){
            function isInv(singleInventory) {
                return singleInventory._id === inventory._id;
            }
            var inv = $scope.inventories.indexOf($scope.inventories.find(isInv));
            $scope.inventories[inv] = inventory;
        };
        /* =======  For tracking view =====*/
        $scope.expandView = function(){
            var elem = angular.element(document.querySelector('md-dialog.trowser'));
            if(elem) {
                elem[0].style.minWidth = '100%';
                elem[0].style.display = 'block';
                elem[0].children[0].style.width = '50%';
                elem[0].children[0].style.float = 'left';
            }
        };
        $scope.minimiseView = function(){
            $scope.trackview = false;
            $scope.stockview = false;
            var elem = angular.element(document.querySelector('md-dialog.trowser'));
            if(elem) {
                elem[0].style.minWidth = '50%';
                elem[0].children[0].removeAttribute('style');
            }
        };
        $scope.track = function(inventory,stock,id){
            var trackPathfields={};
            inventoryProduct.getTrackPaths({inventoryId:inventory._id,stockId:stock._id}).then(function (response) {
                $scope.trackPaths=response.data;

                if($scope.trackPaths.destinations.length===0){
                    var elem = angular.element(document.querySelector('#stock-'+id));
                    $mdToast.show(
                        $mdToast.simple()
                            .textContent('No Track Paths')
                            .position('top right')
                            .theme('error-toast')
                            .hideDelay(600)
                            .parent(elem)
                    );

                }else {
                    $scope.expandView();
                    $scope.stockview = false;
                    $scope.trackview = true;
                }
            },function (errorResponse) {
                $scope.error=errorResponse.data.message;
                var elem = angular.element(document.querySelector('#stock-'+id));
                $mdToast.show(
                    $mdToast.simple()
                        .textContent($scope.error)
                        .position('top right')
                        .theme('error-toast')
                        .hideDelay(600)
                        .parent(elem)
                );
            });
        };
        $scope.getInventorySummary=function(businessUnitId){
            inventoryProduct.inventorySummary(businessUnitId).then(function (responce) {
                $scope.inventoryFilters=responce.data;
            },function (err) {
                $scope.error=err.data.message;
            });
        };
        $scope.inventoryFilterSummary=function(filterKey,filterCount,pageno) {
            var paginationfield = {};
            $scope.isFilterSummary = true;
            if (!isNaN(pageno)) {
                paginationfield = {page: pageno, limit: $scope.itemsPerPage};
            }
            if (this.searchInventoryText && this.searchInventoryText.length > 0) {
                paginationfield.searchText = this.searchInventoryText;
            }
            if (filterKey) {
                $scope.filterKey = filterKey;
                $scope.data = {summaryFilters: {}};
                $scope.data.summaryFilters[$scope.filterKey] = true;
            }
            inventoryProduct.filterInventorySummary($scope.busUnitId, paginationfield, $scope.data).then(function (response) {
                $scope.inventories = response.data[$scope.filterKey];
                if (filterCount)
                    $scope.total_count = filterCount;
            }, function (res) {
                $scope.error = res.data.message;
            });
        };
        $scope.getInventoryFilterSummary=function (filterKey,filterCount,toggle) {
            if(toggle) {
                $scope.inventoryFilterSummary(filterKey, filterCount, 0);
            }else{
                $scope.filterKey = null;
                $scope.find(0);
            }
        };
        $scope.getStockSource=function (stock) {
            return ((stock.stockIn && stock.stockIn.inventories && stock.stockIn.inventories.length >0) ||(stock.stockIn && stock.stockIn.orders &&  stock.stockIn.orders.length>0));
        };
        /*======== End of tracking view =====*/
        /*======== Stock view ===============*/
        $scope.openStock = function(stock){
            $scope.currentStock = stock;
            $scope.expandView();
            $scope.trackview = false;
            $scope.stockview = true;
        };
        /*======== End of stock view ========*/
        $scope.changeValidPrice=function (index,sale) {
            if(this.inventory) {
                if (sale) {
                    if(this.inventory.moqAndMargin[index].margin===undefined){
                        this.inventory.moqAndMargin[index].price=this.inventory.MRP;
                    }else {
                        this.inventory.moqAndMargin[index].price = $filter('number')(this.inventory.MRP * (1 - (this.inventory.moqAndMargin[index].margin / 100)), 2);
                    }
                } else {
                    if(this.inventory.moqAndBuyMargin[index].margin===undefined){
                        this.inventory.moqAndBuyMargin[index].price=this.inventory.MRP;
                    }else {
                        this.inventory.moqAndBuyMargin[index].price = $filter('number')(this.inventory.MRP * (1 - (this.inventory.moqAndBuyMargin[index].margin / 100)), 2);
                    }
                }
            }

        };

        $scope.changePrice=function (index) {
            this.inventory=$scope.inventories[index];
            if(this.inventory) {
                if(this.inventory.moqAndMargin[index].margin===undefined){
                    this.inventory.moqAndMargin[index].price=this.inventory.MRP;
                    this.inventory.moqAndBuyMargin[index].price=this.inventory.MRP;
                }else {
                    this.inventory.moqAndMargin[index].price = (this.inventory.MRP * (1 - (this.inventory.moqAndMargin[index].margin / 100))).toFixed('2');
                    this.inventory.moqAndBuyMargin[index].price = (this.inventory.MRP * (1 - (this.inventory.moqAndBuyMargin[index].margin / 100))).toFixed('2');
                }

            }
        };
        $scope.remove = function (inventory,index) {
            if (inventory) {
                verifyDelete(inventory).then(function () {
                    var parentEl2 = angular.element(document.querySelector('.nv-fixed-headline'));
                    var parentEl = angular.element(document.querySelector('#product-'+index));
                    inventory.$remove(function () {


                        for (var i in $scope.inventories) {
                            if ($scope.inventories [i]._id === inventory._id) {
                                $scope.inventories.splice(i, 1);
                            }
                        }
                        $mdToast.show(
                            $mdToast.simple()
                                .textContent('Successfully deleted Inventory')
                                .position('top right')
                                .theme('success-toast')
                                .hideDelay(6000)
                                .parent(parentEl2)
                        );
                    }, function (errorResponse) {

                        $mdToast.show(
                            $mdToast.simple()
                                .textContent(errorResponse.data.message)
                                .position('bottom right')
                                .theme('error-toast')
                                .hideDelay(6000)
                                .parent(parentEl)
                        );


                        //$scope.error = errorResponse.data.message;
                    });
                });
            }
        };
//toggle Category Selection
        function filterSelectedProducts () {

            /*var selectedProducts=$scope.inventories && $scope.inventories.filter(function (inventory) {
                return inventory.selected;
            });*/
            /*$scope.selectedAll=selectedProducts.length === $scope.inventories.length;*/
            return $scope.inventories && $scope.inventories.filter(function (inventory) {
                return inventory.selected;
            });
        }
        $scope.selProductLength=function(){
           return filterSelectedProducts ();
        };
        $scope.batchInventories=function (index) {
            inventoryProduct.batchInventories(index,$scope);
        };

        // Update existing Inventory
        $scope.update = function (isDisabled) {
            var inventory = this.inventory;
            if(isDisabled){
                inventory.disabled=!inventory.disabled;
            }
            inventory.$update(function (inventory) {
                $location.path('products');
            }, function (errorResponse) {
                $scope.error = errorResponse.data.message;
            });
        };



        $scope.getImageLabel=function (inventory) {
            if(inventory) {
                if (inventory.disabled === true) {
                    return 'Disabled';
                } else if (inventory.numberOfUnits === 0 && inventory.maxUnits !== 0) {
                    return 'No Stock';
                } else if (inventory.numberOfUnits === 0 && inventory.maxUnits === 0) {
                    return 'No Use';
                } else {
                    return null;
                }
            }else{
                return null;
            }
        };

        // Find a list of Inventories
        $scope.find = function (pageno) {
            /* $scope.inventories = Inventories.query();*/
            var paginationfield={};
            $scope.isFilterSummary=false;
            if(!isNaN(pageno)){
                paginationfield={page:pageno,limit:$scope.itemsPerPage};
            }
            if(this.searchInventoryText && this.searchInventoryText.length>0){
                paginationfield.searchText=this.searchInventoryText;
            }
            inventoryProduct.getInventoriesByBUnit($scope.busUnitId,paginationfield).then(function (response) {
                if (response.data) {
                    $scope.inventories = response.data.inventory;
                    $scope.total_count = response.data.total_count;
                    if (response.data.summaryData) {
                        if($scope.filterKey){
                            $scope.inventories=response.data.summaryData[$scope.filterKey];
                        }else {
                            $scope.inventoryFilters = response.data.summaryData;
                        }
                    }
                }
                if($scope.location.$$url==='/inventories/selection'){
                    $scope.panelChange('offercreate');
                }
            }, function (res) {
                $scope.error = res.data.message;
            });
        };


        $scope.findSubCategories2 = function () {
            categoryService.findSubCategories2().then(function (response) {
                $scope.sub2Categories = response.data;
            }, function (res) {
                $scope.error = res.data.message;
            });
        };
        $scope.findSubCategories1 = function () {
            categoryService.findSubCategories1().then(function (response) {
                $scope.subCategories1 = response.data;
            }, function (res) {
                $scope.error = res.data.message;
            });
        };
        $scope.tabs = [
            { title:'MY ORDERS', type:'MyOrders'},
            { title:'RECEIVED ORDERS',type:'ReceivedOrders'}
        ];

        $scope.findCategory = function (id) {
            if(id) {
                categoryService.findCategoryById(id).then(function (response) {
                    if (response.data.type === 'MainCategory') {
                        $scope.subCategories1 = response.data.children;
                        $scope.selectedMainCategory=response.data;
                        $scope.selectedSubCategory1Category=null;
                        $scope.selectedSubCategory=null;
                        $scope.subCategories2 = [];
                        $scope.hsncodes=[];
                    } else if (response.data.type === 'SubCategory1') {
                        $scope.subCategories2 = response.data.children;
                        $scope.selectedSubCategory1Category=response.data;
                        $scope.selectedSubCategory=null;
                        $scope.hsncodes=[];
                    } else if (response.data.type === 'SubCategory2') {
                        $scope.selectedSubCategory = response.data;
                        $scope.hsncodes = response.data.hsncodes;
                        $scope.inventoryUoms=response.data.unitOfMeasures;
                        $scope.taxgroups = response.data.taxGroups;
                        $scope.productBrands=  response.data.productBrands;
                    }
                }, function (res) {
                    $scope.error = res.data.message;
                });
            }
        };

        $scope.getInventoriesByBusinessUnit = function(selectedBusinessUnit,pageno){
            $scope.busUnitId = selectedBusinessUnit;
            $scope.filterKey=null;
            $scope.filterCount=0;
            var authdata= localStorageService.get('nvipanilogindata');
            authdata.companyBusinessUnits.defaultBusinessUnitId=$scope.busUnitId;
            localStorageService.set('nvipanilogindata',authdata);
            $scope.find(0);
            /*$scope.isFilterSummary=false;
            var paginationfield={};
            if(!isNaN(pageno)){
                paginationfield={page:pageno,limit:$scope.itemsPerPage};
            }
            if(this.searchInventoryText && this.searchInventoryText.length>0){
                paginationfield.searchText=this.searchInventoryText;
            }
            if($scope.filterKey) {
                $scope.data={summaryFilters:{}};
                $scope.data.summaryFilters[$scope.filterKey] = true;
            }
            //$scope.getInventorySummary(selectedBusinessUnit);
            inventoryProduct.getInventoriesByBUnit(selectedBusinessUnit,paginationfield).then(function(response){
                //returned response
                /!* var retVal = {};
                 retVal.inventories=businessUnit.inventories;
                 retVal.total_count=businessUnit.inventories.length;
                 retVal.businessUnit = businessUnit._id;*!/

                if(response.data) {
                    $scope.inventories=response.data.inventory;
                    $scope.total_count = response.data.total_count;
                    $scope.inventoryFilters=response.data.summaryData;
                    if($scope.location.$$url==='/inventories/selection'){
                        $scope.panelChange('CreateOffer');
                    }
                }
            });*/
        };
        $scope.findBrandChange=function (id) {
            if(id){
                productBrandService.findBrandById(id).then(function (response) {
                    $scope.brandUoms=response.data.unitOfMeasures;
                    $scope.selectedProductBrand=response.data;

                });
            }

        };
        $scope.getAcceptableUOMs = function(inv,inventories,currentUOM){
            //$scope.inventoryArray = inventories
            inv.acceptableUOMs = [];
            var alreadyAdded = [];
            $scope.brandUoms.forEach(function (uom) {
                var alreadyAddedUOM = inventories.filter(function(inventory) {
                    return (inventory.unitOfMeasure && inventory.unitOfMeasure===uom._id) && !(currentUOM && currentUOM===uom._id);
                });
                if(alreadyAddedUOM.length===0) {
                    inv.acceptableUOMs.push(uom);
                }
            });


        };
        $scope.processOfferCreation=function () {
            $location.path('offers/create');
        };
        $scope.processOrderCreation=function(){
            $scope.createOrder();
            // $location.path('orders/create');
        };
        $scope.createOffer= function () {
            for (var i = 0; i < $scope.selOffer.products.length; i++) {
                if($scope.selOffer.offerType==='Sell') {
                    $scope.selOffer.products[i].margin = ((1 - $scope.selOffer.products[i].MOQ.margin) / $scope.selOffer.products[i].MRP) * 100;
                    $scope.selOffer.products[i].MOQ =$scope.selOffer.products[i].MOQ.MOQ;
                }


                if ($scope.selOffer.products[i].product){
                    $scope.selOffer.products[i].category = $scope.selOffer.products[i].product.category ? $scope.selOffer.products[i].product.category._id : null;
                    $scope.selOffer.products[i].subCategory1 = $scope.selOffer.products[i].product.subCategory1 ? $scope.selOffer.products[i].product.subCategory1._id : null;
                    $scope.selOffer.products[i].subCategory2 = $scope.selOffer.products[i].product.subCategory2 ? $scope.selOffer.products[i].product.subCategory2._id : null;
                    $scope.selOffer.products[i].subCategory3 = $scope.selOffer.products[i].product.subCategory3 ? $scope.selOffer.products[i].product.subCategory3._id : null;
                }

                $scope.selOffer.products[i].inventory = $scope.selOffer.products[i]._id;
                if(!$scope.selOffer.products[i].availableDate){
                    $scope.selOffer.products[i].availableDate=new Date();
                }

                $scope.tocontacts = {contacts: [], groups: []};
                $scope.togroups = [];
                $scope.toallcontacts = [];
                $scope.selOffer.offerStatus = 'Opened';
                if($scope.to && $scope.to.contact) {
                    for (var j = 0; j < $scope.to.contact.length; j++) {
                        var singlecontact = angular.fromJson($scope.to.contact[j]);
                        if (singlecontact.firstName) {
                            $scope.tocontacts.contacts.push({'contact': singlecontact._id});
                            $scope.toallcontacts.push({
                                'contact': singlecontact._id,
                                'nVipaniUser': singlecontact.nVipaniUser
                            });
                            $scope.togroups.push(singlecontact);
                        }
                        if (singlecontact.name) {
                            $scope.tocontacts.groups.push({'group': singlecontact._id});
                            for (var k = 0; k < $scope.contactsVsGroup.length; k++) {
                                var groupContacts = angular.fromJson($scope.contactsVsGroup[k]);
                                if (groupContacts.groups && groupContacts.groups.indexOf(singlecontact._id) >= 0)
                                    $scope.toallcontacts.push({'contact': groupContacts._id});
                                $scope.togroups.push(singlecontact);
                            }

                        }
                    }
                }
                $scope.selOffer.toContacts={contacts: $scope.tocontacts.contacts, groups: $scope.tocontacts.groups};
                /*$scope.selectedproducts[i].sampleAvailable = $scope.selectedproducts[i].sampleAvailable.dt;*/
            }

            $scope.selOffer.$save(function (response) {
                $location.path('offers/'+response._id);
            }, function (errorResponse) {
                $scope.error = errorResponse.data.message;
            });
        };
        $scope.addInventoryItem =function(){
            if ($scope.inventoryArray) {
                $scope.inventoryArray.push({
                    unitSize: '',
                    /*unitOfMeasure: '',*/
                    uom:{firstUnitOfMeasure:'',conversion:'',secondUnitOfMeasure:''},
                    addUOM:false,
                    MRP:0.00,
                    buyUnitPrice: '',
                    saleUnitPrice: '',
                    numberOfUnits:'',
                    description: '',
                    moqAndMargin:[{
                        MOQ: '',
                        margin:''
                    }],
                    moqAndBuyMargin:[{
                        MOQ: '',
                        margin:''
                    }],
                    movAndMargin:[{
                        MOQ: '',
                        margin:''
                    }],
                    movAndBuyMargin:[{
                        MOV: '',
                        margin:''
                    }],
                    manufacturingDate:new Date(),
                    packagingDate:new Date(),
                    /* product:'',*/
                    inventoryImageURL1: this.inventoryImageURL1,
                    inventoryImageURL2: this.inventoryImageURL2,
                    inventoryImageURL3: this.inventoryImageURL3,
                    inventoryImageURL4: this.inventoryImageURL4
                });
            }
        };
        $scope.selectedFirstLevelUOM=function (inventory) {
            inventory.addUOM = !inventory.addUOM;
            $scope.findByUom(null, null, null, false);
        };
        /*-------- function to add a animation while deleting the batch -----------*/

        $scope.flip=function (stock){
            var elem = angular.element(document.querySelector('#stock-'+stock));
            elem[0].style.width = elem[0].offsetWidth + 'px';
            elem[0].style.height = elem[0].offsetHeight + 'px';
            var child = elem[0].children;
            angular.element(child).addClass('nvc-card-flip');
            var html = '<div class="back md-padding" ><h4 class="nv-text-center">Are you sure?</h4>' +
                '<div layout="row" layout-padding layout-align="space-around center"><div flex="50"><md-button class="md-primary md-icon-button md-raised img-center" aria-label="delete stock" ng-click="deleteStock('+stock+')"><md-icon class="fa fa-check fa-lg"></md-icon> </md-button> </div>' +
                '<md-divider></md-divider><div flex="50"><md-button class="md-icon-button img-center" aria-label="flip back" ng-click="flipBack('+stock+')"><md-icon class="nv-red fa fa-times fa-2x"></md-icon></md-button></div></div>' +
                '</div>';
            angular.element(elem[0].children[0]).append( $compile(html)($scope) );
            angular.element(child).addClass('flipped');
            $timeout(function(){
                $scope.flipBack(stock);
            },10000);
        };
        $scope.flipBack=function(stock){
            var elem = angular.element(document.querySelector('#stock-'+stock));
            if(elem[0]) {
                elem[0].removeAttribute('style');
                var child = elem[0].children;
                angular.element(child).removeClass('nvc-card-flip flipped')[0].children[1].remove();
            }
        };
        $scope.deleteStock=function (stock){
            $scope.inventory = this.inventory;
            var elem = angular.element(document.querySelector('#stock-'+stock));
            elem.addClass('nvc-shrink');
            $timeout(function(){
                $scope.inventory.stockMasters.splice($scope.inventory.stockMasters.length-stock-1, 1);
                elem.removeClass('nvc-shrink');
                $scope.flipBack(stock);
                $scope.inventory.numberOfUnits = $scope.updateinventoryNumberOfUnits($scope.inventory);
                $scope.inventory.$update(function (response) {
                    $scope.addStock=false;
                    $scope.getInventorySummary($scope.busUnitId);
                },function (errorResponse) {
                    $scope.error = errorResponse.data.message;
                });
            },250);
        };
        /*------------ End of delete function with animation -----------*/
        $scope.updateinventoryNumberOfUnits = function(inventory){
            inventory.numberOfUnits = 0;
            for(var i in inventory.stockMasters){
                if(inventory.stockMasters[i].currentBalance) {
                    inventory.numberOfUnits = inventory.numberOfUnits + inventory.stockMasters[i].currentBalance;
                }
            }
            return inventory.numberOfUnits;
        };
        $scope.cancelStock = function(index){
            $scope.stockIndex = $scope.inventory.stockMasters.indexOf(this.stock);
            $scope.inventory.stockMasters.splice($scope.stockIndex, 1);
            $scope.addStock = false;
        };
        $scope.createStock= function(index){
            $scope.stockIndex=$scope.inventory.stockMasters.indexOf(this.stock);
            inventoryProduct.createStock(this.stock,$scope.busUnitId).then(function (response) {
                $scope.stockIndex=response.data;
                $scope.inventory.stockMasters[$scope.inventory.stockMasters.length-1] = response.data;
                $scope.inventory.numberOfUnits = $scope.updateinventoryNumberOfUnits($scope.inventory);
                $scope.addStock=false;
                $scope.getInventorySummary($scope.busUnitId);
                // Show user success message and clear form
                /*$scope.register.registerusername = null;*/
                /*$scope.panelChange('Edit');*/

            }, function (errorResponse) {
                // Show user error message and clear form
                if (errorResponse) {
                    $scope.error = errorResponse.data.message;
                    $mdToast.show(
                        $mdToast.simple()
                            .textContent($scope.error)
                            .position('top right')
                            .theme('error-toast')
                            .hideDelay(6000)
                            .parent(angular.element('stock-'+$scope.stockIndex))
                    );
                }
            });
        };
        $scope.getQuantityPrices=function (isSeller,eachProduct) {
            if(isSeller)
                return inventoryProduct.getMatchedMoq(eachProduct.moqAndMargin,eachProduct.MOQ,eachProduct.MRP);
            else{
                return inventoryProduct.getMatchedMoq(eachProduct.moqAndBuyMargin,eachProduct.MOQ,eachProduct.MRP);
            }

        };
        $scope.getQuantityMargin=function (eachProduct) {
            return ;
        };

        $scope.getQuantityPrice = function (moqVsPrice, quantity,unitPrice) {
            return inventoryProduct.getQuantityPrice(moqVsPrice,quantity,unitPrice);

        };

        $scope.createProduct = function(isValid) {
            // Create new Product object

            if(isValid) {
                if (this.selectedItem && this.selectedItem._id) {
                    /*if (this.selectedItem.user && this.selectedItem.user._id) {
                     this.selectedItem.user = this.selectedItem.user._id;
                     }

                     if (!$scope.allowEditProduct(this.selectedItem)){
                     this.selectedItem.category = this.category;
                     var selectHsncode = $scope.selectedHsncode(this.selectedItem.hsncode);
                     if (selectHsncode.length > 0)
                     this.selectedItem.hsncodes = selectHsncode[0].hsncode.name;
                     this.selectedItem.subCategory1 = this.subCategory1;
                     this.selectedItem.subCategory2 = this.subCategory2;
                     }*/

                    for (var i in $scope.inventoryArray) {
                        $scope.inventoryArray[i].product = this.selectedItem._id;

                    }


                } else {
                    if (this.selectedItem && this.selectedItem !== null) {
                        if (this.selectedItem.productImageURL1)
                            this.selectedItem.productImageURL1 = this.selectedItem.productImageURL1;
                        if (this.selectedItem.productImageURL2)
                            this.selectedItem.productImageURL2 = this.selectedItem.productImageURL2;
                        if (this.selectedItem.productImageURL3)
                            this.selectedItem.productImageURL3 = this.selectedItem.productImageURL3;
                        if (this.selectedItem.productImageURL4)
                            this.selectedItem.productImageURL4 = this.selectedItem.productImageURL4;
                    } else {
                        if (!this.selectedItem || this.selectedItem === null)
                            this.selectedItem = {};
                        this.selectedItem.productImageURL1 = $scope.imageURL1;
                        this.selectedItem.productImageURL2 = $scope.imageURL1;
                        this.selectedItem.productImageURL3 = $scope.imageURL1;
                        this.selectedItem.productImageURL4 = $scope.imageURL1;
                        /*         this.selectedItem.productImageURL1= $scope.imageURL1;*/
                    }
                    this.selectedItem.name = this.searchText;
                    this.selectedItem.category = this.category;
                    var selectHsncode = $scope.selectedHsncode(this.selectedItem.hsncode);
                    if (selectHsncode && selectHsncode.length > 0 && selectHsncode[0].hsncode.hsncode)
                        this.selectedItem.hsncodes = selectHsncode[0].hsncode.hsncode;
                    this.selectedItem.subCategory1 = this.subCategory1;
                    this.selectedItem.subCategory2 = this.subCategory2;
                }

                /* if(this.keyWord && this.keyWord.length>0){
                 product.keywords=this.keyWord.split(',');
                 }*/
                /*if($scope.getInvalidEntry($scope.inventoryArray)){
                 alert({title:'Error',content:'Please Enter all the fields'});
                 }*/
                //else {

                var productVsInventory = {productBrand:this.productBrand,product: this.selectedItem, businessUnit:$scope.busUnitId,inventory: $scope.inventoryArray};

                // Redirect after save
                inventoryProduct.productInventoryMasterCreate(productVsInventory).then(function (response) {
                    // Show user success message and clear form
                    /*$scope.register.registerusername = null;*/
                    $scope.inventory = response.data;
                    for (var i in $scope.inventory) {
                        $scope.inventories.push($scope.inventory[i]);
                    }
                    $scope.panelChange('cancelViewAll');
                    $scope.getInventorySummary($scope.busUnitId);

                }, function (errorResponse) {
                    // Show user error message and clear form
                    if (errorResponse) {
                        $scope.error = errorResponse.data.message;
                    }
                });

            }
            //}

        };
        $scope.changeIndicativePrice=function () {
            this.singleproduct.unitIndicativePrice=this.singleproduct.MRP*(1-this.singleproduct.margin/100);
        };
        $scope.selectedHsncode=function (id) {
            return $scope.hsncodes && $scope.hsncodes.filter(function (eachhsncode) {
                return eachhsncode.hsncode._id===id;
            });
        };
        $scope.getSelectedItem=function (item) {
            $scope.selectedItem=item;
        };
        $scope.getChangeItem=function () {
            $scope.selectedItem=null;
        };
        $scope.clearProduct=function () {
            //setting md-search-text variable to null
            if($scope.$$childTail) {
                $scope.$$childTail.searchText = '';
                $scope.$$childTail.searchBrandText = '';
                $scope.$$childTail.searchHsnText = '';
                $scope.$$childTail.searchTaxText = '';
                $scope.$$childTail.searchUomText = '';
                /* $scope.$$childTail.searchUomText = '';*/
                $scope.UOMS=[];
                $scope.hsnCodes=[];
                $scope.taxGroups=[];
            }

            $scope.searchText = '';
            $scope.description = '';
            $scope.searchBrandText = '';
            $scope.searchHsnText = '';
            $scope.searchTaxText = '';
            $scope.searchUomText = '';
            /* $scope.$$childTail.searchUomText = '';*/
            $scope.UOMS=[];
            $scope.hsnCodes=[];
            $scope.taxGroups=[];
            /*  $scope.searchText='';
              $scope.searchBrandText='';*/

            $scope.inventoryImageURL1='modules/inventories/img/profile/default.png';
            $scope.inventoryImageURL2 = 'modules/inventories/img/profile/default.png';
            $scope.inventoryImageURL3 = 'modules/inventories/img/profile/default.png';
            $scope.inventoryImageURL4 = 'modules/inventories/img/profile/default.png';
            $scope.error=null;
            if($scope.selectedItem) {
                $scope.selectedItem.brandName = null;
                $scope.selectedItem.fssaiLicenseNumber = null;
                $scope.selectedItem.description = null;
                $scope.selectedItem.fssaiLicenseNumberProof = null;
                $scope.selectedItem.description = null;
                $scope.selectedItem.gradeDefinition = null;
                $scope.selectedItem.qualityDefinition = null;
                /* $scope.selectedItem.hsncode = null;*/
                $scope.selectedItem.hsncodes = null;
                $scope.selectedItem.sampleNumber = null;
                $scope.selectedItem.productImageURL1 = 'modules/products/img/profile/default.png';
                $scope.selectedItem.productImageURL2 = 'modules/products/img/profile/default.png';
                $scope.selectedItem.productImageURL3 = 'modules/products/img/profile/default.png';
                $scope.selectedItem.productImageURL4 = 'modules/products/img/profile/default.png';
            }
            $scope.inventoryArray=[{
                unitSize: '',
                /*unitOfMeasure: '',*/
                MRP: '',
                uom:{firstUnitOfMeasure:'',conversion:'',secondUnitOfMeasure:''},
                addUOM:false,
                buyUnitPrice: '',
                saleUnitPrice: '',
                numberOfUnits:'',
                maxUnits:'',
                description: '',
                /* product:'',*/
                moqAndMargin:[{
                    MOQ:'',
                    margin:''
                }],
                moqAndBuyMargin:[{
                    MOQ: '',
                    margin:''
                }],
                movAndMargin:[{
                    MOV:'',
                    margin:''
                }],
                movAndBuyMargin:[{
                    MOV: '',
                    margin:''
                }],
                manufacturingDate:new Date(),
                packagingDate:new Date(),
                inventoryImageURL1: $scope.inventoryImageURL1,
                inventoryImageURL2: $scope.inventoryImageURL2,
                inventoryImageURL3: $scope.inventoryImageURL3,
                inventoryImageURL4: $scope.inventoryImageURL4
            }];
        };
        $scope.clearCategories=function () {
            $scope.subCategories1=[];
            $scope.subCategories2=[];
            $scope.selectedSubCategory=null;
            $scope.hsncodes=[];
            $scope.taxgroups=[];
            $scope.taxgroups=[];
            $scope.productBrands=[];


            $scope.clearProduct();

        };
        $scope.init=function(){
            $scope.panelChange('viewAll');
            $scope.findBunits();

        };
        $scope.findProduct=function(searchText,id,update){
            var lowercaseQuery = angular.lowercase(searchText);
            var findProducts=[];
            if(searchText && searchText.length>2) {

                var searchkeys={subCategory2Id:id};
                searchkeys.searchKey=searchText;

                return categoryService.findByCategoryProducts(searchkeys).then(function (response) {
                    return response.data;
                });
            }else {
                return findProducts;
            }

        };

        $scope.findByHsncode=function(searchText,id,update){
            var lowercaseQuery = angular.lowercase(searchText);
            $scope.hsnCodes=[];
            if(searchText && searchText.length>2) {
                var searchkeys={subCategory2Id:id};
                searchkeys.hsnKey=searchText;
                return categoryService.findByHsncodes(searchkeys).then(function (response) {
                    $scope.hsnCodes=response.data;
                    return $scope.hsnCodes;
                });
            }else {
                return  $scope.hsnCodes;
            }

        };
        $scope.findByTax=function(searchText,id,update){
            var lowercaseQuery = angular.lowercase(searchText);
            $scope.taxGroups=[];
            if(searchText && searchText.length>1) {
                var searchkeys={};
                searchkeys.taxKey=searchText;

                return categoryService.findByTaxGroups(searchkeys).then(function (response) {
                    $scope.taxGroups=response.data;
                    return $scope.taxGroups;
                });
            }else {
                return $scope.taxGroups;
            }

        };
        $scope.findByUom=function(searchText,id,update,isSimple){
            var lowercaseQuery = angular.lowercase(searchText);
            if(!isSimple){
                if(!$scope.UOMS || $scope.UOMS.length===0) {
                    return categoryService.findByUoms({isSimple: false}).then(function (response) {
                        $scope.UOMS = response.data;
                        return $scope.UOMS;
                    });
                }else return $scope.UOMS;
            }else {
                if (searchText && searchText.length > 2) {

                    var searchkeys = {};
                    if (id) {
                        searchkeys.subCategory2Id = id;
                    }
                    searchkeys.uomKey = searchText;
                    searchkeys.isSimple = isSimple;

                    return categoryService.findByUoms(searchkeys).then(function (response) {
                        $scope.UOMS = response.data;
                        return $scope.UOMS;
                    },function (err) {
                        $scope.error=err.data.message;
                        return $scope.UOMS;
                    });
                } else {
                    return $scope.UOMS;
                }
            }

        };
        /*==== functions for sub-menu actions=====*/
        $scope.stopPropogation = function(event){
            event.stopImmediatePropagation();
        };
        $scope.closeSubMenu = function(event){
            $mdMenu.hide();
        };
        /*==== End of functions for sub-menu actions====*/
        /*==== get height dynamically =====*/
        $scope.getHeight = function(string){
            var height;
            if(string==='convertView'){
                var cards = angular.element(document.querySelectorAll('#convertView md-card'));
                height = window.innerHeight - cards[0].offsetTop - cards[0].clientHeight - cards[1].clientHeight - cards[2].clientHeight;
            }
            return {'max-height': height.toString()+'px'};
        };
        // Find existing Inventory
        $scope.findOne = function () {
            Inventories.get({
                inventoryId: $stateParams.inventoryId
            }).$promise.then(function (result) {
                $scope.inventory=result;
                if($scope.inventory.product)
                    $scope.findCategory($scope.inventory.product.subCategory2._id);
                $scope.showEditProduct = true;
            });
        };
        $scope.selectedCategory=function (categories,selcategoires) {
            categories.filter(function (cat) {
                if(cat.isChecked===true)
                    selcategoires.push({category:cat._id});
                else if(cat.children && cat.children.length>0) {
                    $scope.selectedCategory(cat.children,selcategoires);
                }
            });
            return selcategoires;
        };
        $scope.importInventory=function () {
            $scope.selectedCatValue=[];
            inventoryProduct.getMasterData($scope.selectedCategory($scope.selectedCategories,$scope.selectedCatValue),$scope.busUnitId).then(function (response) {

                $scope.masterRecords=[];
                inventoryProduct.prepareUserItemMaster(response.data,$scope.masterRecords);
                $scope.importType='MassImport';
            });

        };
        $scope.createMaster=function () {
            var data= inventoryProduct.selectedUserItems($scope.masterRecords);
            data.businessUnit=$scope.busUnitId;
            inventoryProduct.createMassUserItemMaster(data).then(function (response) {
                $location.path('/products');
                $scope.panelChange('viewAll');
            }, function (res) {
                $scope.error = res.data.message;
                $scope.showToast($scope.error,'top right','body','error-toast');
            });
        };
        function filterSelectedProductsOffer(isSeller,isAll,done) {
            var selectedProducts= isAll ?$scope.inventories:filterSelectedProducts();
            selectedProducts.forEach(function(eachProduct){

                var matchedMoq=inventoryProduct.getMinQuantity((isSeller?eachProduct.moqAndMargin:eachProduct.moqAndBuyMargin),0);
                if(matchedMoq) {
                    eachProduct.MOQ = matchedMoq;
                    eachProduct.margin = matchedMoq.margin;
                    //selectedProducts[i].numberOfUnits=matchedMoq.MOQ;
                    eachProduct.unitIndicativePrice = eachProduct.MRP *(1-(matchedMoq.margin/100));
                }else{
                    eachProduct.unitIndicativePrice=eachProduct.MRP;
                    eachProduct.MOQ = {MOQ:$scope.minUnits(eachProduct),margin:0};
                    eachProduct.margin = 0;
                    eachProduct.numberOfUnits=$scope.maxUnit(eachProduct);
                }
                selectedProducts[selectedProducts.indexOf(eachProduct)]=eachProduct;
            });
            done(selectedProducts);

        }
        $scope.panelChange = function (changeStr) {
            $scope.context = [
                {title:'Products',link:'/#!/products'}
            ];
            if($location.$$path==='/products/import'){
                changeStr = 'ImportInventory';
            }
            /*$scope.inventoryFilters=[
                { title:'Out of stock', content: 0 , tag: 'outOfStock' },
                { title: 'Low on stock', content: 0, tag: 'lowOnStock' },
                { title:'Inactive', content: 0, tag: 'inactive'},
                { title:'Fast Moving', content: 0 , tag: 'fastMoving' },
                { title: 'Newly Added', content: 0, tag: 'newlyAdded' },
                { title:'Expired', content: 0, tag: 'expired'}
            ];*/
            $scope.error=null;
            $scope.showEditProduct = false;
            $scope.showAddProduct = false;
            $scope.addInventory=false;
            $scope.isProduct=false;
            $scope.isBrand=false;
            $scope.addStock=false;

            if (changeStr === 'viewAll' || changeStr === 'viewAllProducts') {
                $scope.inventories=[];
                $scope.pageno=1;
                $scope.total_count = 0;
                $scope.itemsPerPage = 10;
                $scope.viewAllProduct = false;
                $scope.showEditProduct = false;
                $scope.showAddProduct = false;
                $scope.viewAllProduct = true;
                $scope.showOfferProducts = false;
                $scope.showConversion = false;
                $scope.searchInventoryText='';
                $scope.importType='view';
                $scope.type='view';
                $scope.showEditOnlyProduct = false;
                $scope.showOrderProducts=false;
                $scope.addOrderProducts=false;
                $scope.showConversion=false;
                $scope.clearCategories(changeStr);
                //$scope.getInventorySummary($scope.busUnitId);
                if(changeStr === 'viewAllProducts'){
                    $scope.viewAllProducts = true;
                    $scope.find();
                }else {
                    $scope.find($scope.pageno);

                }
            }else if (changeStr === 'cancelViewAll') {
                $mdDialog.cancel();
                /*$scope.viewAllProduct = true;*/
                /*$scope.find();*/
            }else if (changeStr === 'Edit') {
                $scope.viewAllProduct = true;
                $scope.showEditProduct = true;
                $scope.showAddProduct = false;
                $scope.trackview = false;
                $scope.stockview = false;
                $stateParams.inventoryId = this.inventory._id;
                $scope.findOne();
                $mdDialog.show({
                    template: '<div class="md-dialog-container trowserView" tabindex="-1"><md-dialog class="trowser"><edit-product class="nv-position-top-left"></edit-product>' +
                    '<tracker-view class="bb-wid-50 nvc-lfloat nvc-display-block nv-position-relative nvc-white-background nvc-view-separator" ng-if="trackview"></tracker-view>' +
                    '<stock-details class="bb-wid-50 nvc-rfloat" ng-if="stockview"></stock-details>' +
                    '</md-dialog></div>',
                    /* targetEvent: ev,*/
                    scope: $scope,
                    clickOutsideToClose: true,
                    preserveScope: true
                });



            }else if(changeStr==='Convert' || changeStr==='ConvertRePack' || changeStr==='ConvertGrade'){
                $scope.viewAllProduct = false;
                $scope.showOfferProducts = false;
                $scope.showConversion = true;
                $scope.context = [
                    {title:'Converting Products',link:'.'}
                ];
                /*   $scope.conversionProducts=angular.copy($scope.filterSelectedProducts());
                   for(var i in $scope.conversionProducts){
                       for(var j in $scope.conversionProducts[i].stockMasters){
                           $scope.conversionProducts[i].stockMasters[j]={'_id':$scope.conversionProducts[i].stockMasters[j],
                           'selected': false};
                       }
                   }*/

                inventoryProductConversion.getSourceInventories(angular.copy(filterSelectedProducts()),true).then(function (response) {
                    $scope.conversionProducts=response.data.inventories;
                    if(changeStr==='Convert'){
                        $scope.convertType='Manufactured';

                    }else if(changeStr==='ConvertRePack' &&  $scope.conversionProducts.length===1){
                        $scope.convertType='Repacked';
                        $scope.findCategory($scope.conversionProducts[0].productCategory);
                        $scope.subCategory2=$scope.conversionProducts[0].productCategory;
                        $scope.productBrand=$scope.conversionProducts[0].productBrand;
                        $scope.findBrandChange( $scope.productBrand);

                       /* $scope.subCategory1=$scope.conversionProducts[0].productSubCategory;*/
                       /* $scope.subCategory2=;*/

                    }else if(changeStr==='ConvertGrade'){
                        $scope.convertType='Graded';
                        $scope.findCategory($scope.conversionProducts[0].productCategory);
                        $scope.subCategory2=$scope.conversionProducts[0].productCategory;
                        $scope.productBrand=$scope.conversionProducts[0].productBrand;
                        $scope.findBrandChange( $scope.productBrand);
                    }
                }, function (res) {
                    $scope.error = res.data.message;
                });
                $scope.selectedPageId=0;
            }else if(changeStr==='CreateOffer' || changeStr==='offercreate'){
                $scope.selOffer=Offers;
                filterSelectedProductsOffer($scope.selOffer.offerType === 'Sell', false,function (products) {
                    $scope.offerTypes = [{value: 'Sell', text: 'Seller'}, {value: 'Buy', text: 'Buyer'}];
                    $scope.selOffer.products =angular.copy(products);
                    $location.path('offercreate');
                });



                /*$scope.showOfferProducts=true;*/


                /*  $mdDialog.show({
                      template:'<md-dialog><add-offer-product></add-offer-product></md-dialog>',
                      targetEvent: ev,
                      scope: $scope,
                      clickOutsideToClose: false,
                      fullscreen: useFullScreen,
                      preserveScope: true
                  });*/

            }else if(changeStr==='CreateOrder'){
                $scope.selOrder=new Orders();
                $scope.selOrder.orderType='Sale';
                $scope.showSellerChange=true;
                $scope.showBuyerChange=false;
                $scope.selOrder.products=angular.copy(filterSelectedProducts());
                $scope.offerTypes = [{value: 'Sell', text: 'Seller'}, {value: 'Buy', text: 'Buyer'}];
                //contactsQuery.findContactsVsGroups($scope);
                $scope.showOrderProducts=true;
                $scope.addOrderProducts=true;
                $scope.viewAllProduct=false;
                if($scope.selOrder.products && $scope.selOrder.products.length>0) {
                    $scope.inventories=$scope.selOrder.products;
                    $scope.selOrder.products = angular.copy(filterSelectedProducts());
                }else{
                    $scope.selOrder.products = angular.copy(filterSelectedProducts());
                }
                /*  $mdDialog.show({
                      template:'<md-dialog><add-offer-product></add-offer-product></md-dialog>',
                      targetEvent: ev,
                      scope: $scope,
                      clickOutsideToClose: false,
                      fullscreen: useFullScreen,
                      preserveScope: true
                  });*/

            }else if (changeStr === 'Import' || changeStr === 'ImportInventory') {
                $scope.viewAllProduct = true;
                $scope.showEditProduct = true;
                $scope.showAddProduct = false;
                if (changeStr === 'Import') {
                    $scope.importType = 'product';
                    if($scope.result) {
                        $scope.result = null;
                    }
                    if($scope.matchedCategories) {
                        $scope.matchedCategories=null;
                    }
                    if($scope.matchedProducts) {
                        $scope.matchedProducts = null;
                    }
                    $scope.setImportLevel(0);
                    /* }
                     if (changeStr === 'ImportInventory') {
                         $scope.importType = 'productImport';*/
                    /*
                      }*/

                    $mdDialog.show({
                        template: '<div class="md-dialog-container trowserView" tabindex="-1"><md-dialog class="trowser"><create-file-import></create-file-import></md-dialog></div>',
                        /* targetEvent: ev,*/
                        scope: $scope,
                        clickOutsideToClose: true,
                        preserveScope: true
                    });
                }else{
                    $scope.importType = 'productImport';
                    $scope.context = [
                        {title:'Products',link:'/#!/products'},
                        {title:'Import Inventory',link:''}
                    ];
                    $scope.viewAllProduct=false;
                    $scope.readonly = false;
                    $scope.selectedCategoryItem = null;
                    $scope.searchCategoryText = null;
                    $scope.criteriaChips.loadCategories($scope);
                    $scope.selectedCategories = [];
                    $scope.numberChips = [];
                    $scope.numberChips2 = [];
                    $scope.filterSelected=true;
                    $scope.numberBuffer = '';
                    $scope.autocompleteDemoRequireMatch = false;
                    $scope.transformChip = $scope.criteriaChips.transformChip;
                }

            }else if (changeStr === 'AddStock') {
                $scope.viewAllProduct = true;
                $scope.showEditProduct = true;
                $scope.showAddProduct = false;
                $stateParams.inventoryId = this.inventory._id;
                $scope.stock={inventory:$scope.inventory._id};
                $mdDialog.show({
                    templateUrl: 'modules/products/views/create-stock.client.view.html',
                    /* targetEvent: ev,*/
                    scope: $scope,
                    clickOutsideToClose: true,
                    preserveScope: true
                });

            }else if(changeStr ==='editProduct'){
                $scope.viewAllProduct = true;
                $scope.showEditProduct = false;
                $scope.showAddProduct = false;

                $scope.showEditOnlyProduct = true;
                $scope.showEditProduct = true;
                $scope.imageURL1=$scope.inventory.product.productImageURL1;


            }else if(changeStr==='Add'){
                $scope.viewAllProduct = true;
                $scope.showEditProduct = false;
                $scope.showAddProduct = false;
                $scope.showAddProduct=true;
                $scope.addInventory=true;
                $scope.clearCategories();
                /* $scope.findSubCategories1();*/
                $mdDialog.show({
                    template:'<div class="md-dialog-container trowserView"><md-dialog class="trowser">' +
                    '<add-category-product></add-category-product>' +
                    '<create-product-checkout ng-if="isProduct || isBrand" class="bb-wid-50 nvc-lfloat nvc-display-block nv-position-relative nvc-white-background nvc-view-separator nvc-max-height"></create-product-checkout></md-dialog><div class="md-dialog-focus-trap" tabindex="0"></div></div>',
                    /* targetEvent: $event,*/
                    scope: $scope,
                    clickOutsideToClose: true,
                    preserveScope: true
                });
                /* $scope.findMainCategories();*/
                $scope.clearProduct();
            }else if(changeStr ==='addCategory' || changeStr ==='addBrand'){
                $scope.addCategory=true;
                if(angular.element(document.querySelector('md-dialog')).length>0){
                    $scope.expandView();
                }else{
                    $mdDialog.show({
                        template:'<div class="md-dialog-container trowserView"><md-dialog class="trowser">' +
                        '<create-product-checkout ng-if="isProduct || isBrand" class="bb-height-100"></create-product-checkout></md-dialog><div class="md-dialog-focus-trap" tabindex="0"></div></div>',
                        /* targetEvent: $event,*/
                        scope: $scope,
                        clickOutsideToClose: true,
                        preserveScope: true
                    });
                }

                $scope.selectedPageId=0;
                $scope.clearProduct();
                if(changeStr ==='addCategory') {
                    $scope.createProducts={'product':{gradeDefinition:[{attributeKey:''}],qualityDefinition:[{attributeKey:''}]},'UOMS':[],'Hsncodes':[],'TaxGroups':[]};
                    $scope.isProduct = true;
                    $scope.pages =
                        [
                            {
                                id: 'step1',
                                number: 0,
                                title: ' Product',
                                active: true,
                                view: true,
                                isDifferentUser: true,
                                currentStatus: 'Drafted'
                            },
                            {
                                id: 'step2',
                                number: 1,
                                title: 'Hsncode',
                                active: false,
                                addresspage: true,
                                isDifferentUser: true,
                                currentStatus: 'Drafted'
                            },
                            {
                                id: 'step3',
                                number: 2,
                                title: 'Tax Group',
                                active: false,
                                addresspage: true,
                                isDifferentUser: true,
                                currentStatus: 'Drafted'
                            },
                            {
                                id: 'step4',
                                number: 3,
                                title: 'UOM',
                                active: false,
                                payment: true,
                                isDifferentUser: true,
                                currentStatus: 'Placed'
                            }
                        ];
                }else {
                    $scope.selectedPageId=0;
                    $scope.isBrand = true;
                    $scope.gradeDefinition=$scope.selectedSubCategory.productAttributes.grade.definition;
                    $scope.qualityDefinition=$scope.selectedSubCategory.productAttributes.quality.definition;
                    $scope.hsncodes=$scope.selectedSubCategory.hsnCodes;
                    $scope.UOMS=$scope.selectedSubCategory.unitOfMeasures;
                    $scope.taxGroups=$scope.selectedSubCategory.taxGroups;
                    $scope.createProducts={'brand':{productCategory:$scope.selectedSubCategory._id,gradeDefinition:$scope.gradeDefinition,qualityDefinition:$scope.qualityDefinition},'UOMS':[],'Hsncodes':{},'TaxGroups':{}};
                    $scope.isProduct = false;
                    $scope.pages =
                        [
                            { id: 'step1', number:0,title:' Brand',active:true,view:true,isDifferentUser:true ,currentStatus:'Drafted'},
                            /* { id: 'step2', number:1, title: 'Hsncode',active:false,addresspage:true,isDifferentUser:true ,currentStatus:'Drafted'},
                             { id: 'step3', number:2, title: 'Tax Group',active:false,addresspage:true,isDifferentUser:true ,currentStatus:'Drafted'},*/
                            { id: 'step2', number:1,title: 'UOM',active:false,payment:true,isDifferentUser:true ,currentStatus:'Placed'}
                        ];
                }
                $scope.imageURL1='modules/inventories/img/profile/default.png';

                /*$scope.showAddProduct=true;*/
                /* $mdDialog.show({
                     templateUrl: 'modules/categories/views/create-product-checkout.client.view.html',
                     /!* targetEvent: ev,*!/
                     scope: $scope,
                     clickOutsideToClose: true,
                     preserveScope: true
                 });*/
            }

        };
        $scope.nextConversionStage=function (page) {
            var parentEl2;
            $scope.setSelectedPageId(page);
            if($scope.getSelectedPageId()===-1){
                $scope.setSelectedPageId($scope.getSelectedPageId()+1);
            }else if($scope.getSelectedPageId()===0) {
                //Validation for stock count for each source inventory.
                // Case 1 : Number of units , Case 2: Single Batch is selected for each source  inventory.
                // Case 3: Number units should be greater than current stock.
                if(inventoryProductConversion.validateConvertSource($scope.conversionProducts)){
                    $scope.setSelectedPageId(page + 1);

                } else{
                     parentEl2 = angular.element(document.querySelector('#firstsourcetable'));
                    $mdToast.show(
                        $mdToast.simple()
                            .textContent('No proper source inventory')
                            .position('top right')
                            .theme('error-toast')
                            .hideDelay(6000)
                            .parent(parentEl2)
                    );
                }
            }else if($scope.getSelectedPageId()===1 ){
                if(inventoryProductConversion.validateConvertInventory($scope.conversionProducts,this.subCategory2,this.subCategory1,this.productBrand,this.addUOM?this.uom:this.unitOfMeasure)) {
                    inventoryProductConversion.getConvertInventory({
                        subCategory2: this.subCategory2,
                        subCategory1: this.subCategory1,
                        productBrand: this.productBrand,
                        unitOfMeasure: this.unitOfMeasure._id
                    },$scope.busUnitId).then(function (response) {
                        if (response.data.inventories.length > 0) {
                            $scope.convertProducts = response.data.inventories[0];
                            $scope.convertProductsStockMasters = response.data.inventories[0].stockMasters;
                        }
                        else{
                            $scope.convertProducts = '';
                            $scope.convertProductsStockMasters = '';
                        }
                        $scope.setSelectedPageId(page + 1);

                    }, function (res) {
                        var parentEl2 = angular.element(document.querySelector('#convertproductcategory'));
                        $mdToast.show(
                            $mdToast.simple()
                                .textContent(res.data.message)
                                .position('top right')
                                .theme('error-toast')
                                .hideDelay(6000)
                                .parent(parentEl2)
                        );
                        $scope.error = res.data.message;
                    });
                }else{
                     parentEl2 = angular.element(document.querySelector('#convertproductcategory'));
                    $mdToast.show(
                        $mdToast.simple()
                            .textContent('No proper convert inventory filter')
                            .position('top right')
                            .theme('error-toast')
                            .hideDelay(6000)
                            .parent(parentEl2)
                    );
                }
            }else if($scope.getSelectedPageId()===2) {
                if (inventoryProductConversion.validateConvertInventoryStock(this.stock)) {
                    inventoryProductConversion.createConvertInventories(this.convertProducts, this.productBrand, this.unitOfMeasure, this.stock, this.conversionProducts,this.busUnitId,this.convertType).then(function (response) {
                        if (response.data) {
                            $scope.inventories = response.data.inventories;
                        }
                        $scope.panelChange('viewAll');
                    }, function (res) {
                        var parentEl2 = angular.element(document.querySelector('#convertproductstock'));
                        $mdToast.show(
                            $mdToast.simple()
                                .textContent(res.data.message)
                                .position('top right')
                                .theme('error-toast')
                                .hideDelay(6000)
                                .parent(parentEl2)
                        );
                        $scope.error = res.data.message;
                    });
                }else{
                    parentEl2 = angular.element(document.querySelector('#convertproductstock'));
                    $mdToast.show(
                        $mdToast.simple()
                            .textContent('No proper stock')
                            .position('top right')
                            .theme('error-toast')
                            .hideDelay(6000)
                            .parent(parentEl2)
                    );
                }
            }


        };

        $scope.getDisplaySelection=function () {
            var displayName;
            if($scope.selectedSubCategory1Category && $scope.selectedSubCategory1Category.name){
                displayName='';
                displayName=(displayName && displayName.length>0?'/':'') +$scope.selectedSubCategory.name;

            }
            if($scope.selectedSubCategory && $scope.selectedSubCategory.name){
                if(!displayName)
                displayName='';
                displayName+=(displayName && displayName.length>0?'/':'') +$scope.selectedSubCategory.name;
            }
           /* if($scope.selectedProductBrand && $scope.selectedProductBrand.name){
                displayName=(displayName && displayName.length>0?'/':'') +$scope.selectedProductBrand.name;
            }*/
            if($scope.selectedProductBrand && $scope.selectedProductBrand.name){
                if(!displayName)
                    displayName='';
                displayName+=(displayName && displayName.length>0?'/':'') +$scope.selectedProductBrand.name;
            }
            if(this.unitOfMeasure && this.unitOfMeasure.symbol){
                if(!displayName)
                    displayName='';
                displayName+=(displayName && displayName.length>0?'/':'') +this.unitOfMeasure.symbol;
            }
            return displayName;
        };
        $scope.clearAddProduct = function(form){
            $scope.productForm.$setPristine();
            $scope.changeBack();
            delete this.subCategory1;
            delete this.subCategory2;
            delete this.productBrand;
        };
        $scope.clearFields = function(){
            $scope.result = null;
            $scope.matchedProducts.clear();
            $scope.matchedCategories.clear();
        };
        $scope.changeProductPrice=function (singleproduct,index,isSeller) {
            var moq=$scope.getQuantityPrices((!$scope.selOffer || isSeller), singleproduct);
            if(moq) {
                $scope.selOffer.products[index].unitIndicativePrice = singleproduct.MRP * (1 - (moq.margin / 100));
                $scope.selOffer.products[index].margin = moq.margin;
            }else{
                $scope.selOffer.products[index].unitIndicativePrice= $scope.selOffer.products[index].MRP;
                $scope.selOffer.products[index].MOQ = $scope.selOffer.products[index].MOQ;
                $scope.selOffer.products[index].margin = 0;
            }
        };
        $scope.minUnits=function (singleproduct) {
            var moq = inventoryProduct.getMinQuantity((($scope.selOffer && $scope.selOffer.offerType==='Sell')?singleproduct.moqAndMargin:singleproduct.moqAndBuyMargin),0);
            if (moq) {
                return moq.MOQ;
            } else {
                if (singleproduct.numberOfUnits !== 0 || singleproduct.maxUnits !== 0) {
                    return 1;
                } else {
                    return 0;
                }
            }

        };
        $scope.maxUnit=function (singleproduct) {
            return Math.max(singleproduct.numberOfUnits,singleproduct.maxUnits);

        };

        $scope.maxProductUnits=function (product,isBuy) {
            /* if(product.inventory.moqAndMargin)*/
            return inventoryProduct.getMaxQuantity(product,isBuy);
        };

        $scope.addMoqAndBuyMargin=function (update) {
            this.inventory.moqAndBuyMargin.push({
                MOQ: 0,
                margin:0
            });
        };
        $scope.addStockMasters=function (update) {
            $scope.addStock=true;
            this.inventory.stockMasters.push({
                new:true,
                inventory:this.inventory._id,
                manufacturingDate:new Date(),
                packagingDate:new Date(),
                MRP:this.inventory.MRP
            });
        };
        $scope.autoVProducts=function() {
            return  filterSelectedProducts().length > 0 || $scope.autoopen;
        };
        $scope.addMoqAndMargin=function (update) {

            this.inventory.moqAndMargin.push({
                MOQ: 0,
                margin:0
            });

        };
        $scope.addMovAndMargin=function (update) {

            this.inventory.movAndMargin.push({
                MOV: 0,
                margin:0
            });

        };
        $scope.addMovAndBuyMargin=function (update) {

            this.inventory.movAndBuyMargin.push({
                MOV: 0,
                margin:0
            });

        };
        $scope.openFabView=function (event) {
            if ((event && event.type === 'click')) {
                $scope.autoopen= !($scope.autoopen);

            }

        };
        $scope.setView=function(){
            $scope.page=$scope.pages[$scope.getSelectedPageId()];
            $scope.view=$scope.page.view;
            $scope.payment=$scope.page.payment;
            $scope.addresspage=$scope.page.addresspage;
        };

        $scope.prev=function (oldPages) {
            var oldPage=$scope.pages[$scope.selectedPageId];
            $scope.setSelectedPageId(this.page.number);
            var page = $scope.pages[this.page.number];
        };
        $scope.isActive = function (page){
            $scope.selectedPage = $scope.pages[$scope.selectedPageId];

            return ($scope.selectedPage && $scope.selectedPage.title === page.title);
        };
        $scope.getSelected=function (selectedObjects) {
            var  selectedGroups=[];
            selectedObjects.filter(function (item) {
                if(item.selected === true)
                    selectedGroups.push(item._id);
            });
            return selectedGroups;
        };
        $scope.changeBack=function () {
            if(angular.element(document.querySelector('md-dialog')).length>0){
                $scope.isProduct=false;
                $scope.isBrand=false;
                $scope.addInventory=true;
                $scope.minimiseView();
            }else{
                $scope.cancel();
            }
        };
        $scope.showToast = function (message,position,selector,theme) {
            $mdToast.show(
                $mdToast.simple()
                    .textContent(message)
                    .position(position)
                    .theme(theme)
                    .hideDelay(4000)
                    .parent(angular.element(document.querySelector(selector)))
            );
        };
        $scope.next =function () {
            $scope.isLoading = false;
            if($scope.isProduct){
                if($scope.getSelectedPageId()===0){
                    if(this.searchText){
                        $scope.createProducts.product.name=this.searchText;
                        $scope.createProducts.product.parent=this.subCategory1;
                        $scope.createProducts.product.description=this.description;
                        $scope.createProducts.product.productAttributes={grade:{definition:[]},quality:{definition:[]}};
                        $scope.createProducts.product.productAttributes.grade.definition= $scope.createProducts.product.gradeDefinition;
                        $scope.createProducts.product.productAttributes.quality.definition= $scope.createProducts.product.qualityDefinition;
                        this.searchText = '';
                        $scope.setSelectedPageId($scope.getSelectedPageId()+1);
                    }else{
                        $scope.showToast('Enter Product Name', 'top right', 'body', 'error-toast');
                    }

                }else if($scope.getSelectedPageId()===1){
                    if($scope.getSelected($scope.hsnCodes).length>0) {
                        $scope.createProducts.product.hsnCodes = $scope.getSelected($scope.hsnCodes);
                        this.searchText = '';
                        $scope.setSelectedPageId($scope.getSelectedPageId() + 1);
                    }else{
                        $scope.showToast('Select at least one HSN Code','top right','body','error-toast');
                    }
                }else if($scope.getSelectedPageId()===2){
                    if($scope.getSelected($scope.taxGroups).length>0) {
                        $scope.createProducts.product.taxGroups = $scope.getSelected($scope.taxGroups);
                        this.searchText = '';
                        $scope.setSelectedPageId($scope.getSelectedPageId() + 1);
                    }else{
                        $scope.showToast('Select at least one Tax group','top right','body', 'error-toast');
                    }
                }else if($scope.getSelectedPageId()===3){
                    if($scope.getSelected($scope.UOMS).length>0){
                        $scope.createProducts.product.unitOfMeasures=$scope.getSelected($scope.UOMS);
                        categoryService.createProductMaster($scope.createProducts.product).then(function (response) {
                            $scope.subCategories2=response.data.children;
                            $scope.subCategory1= response.data._id;
                            $scope.subcategory = response.data.children.filter(function (item) {
                                return item.name === $scope.createProducts.product.name;
                            });
                            $scope.subCategory2 = $scope.subcategory[0]._id;
                            $scope.clearProduct();
                            $scope.changeBack();
                        }, function (res) {
                            $scope.error = res.data.message;
                            $scope.showToast($scope.error,'top right', 'body', 'error-toast');
                        });
                    }else{
                        $scope.showToast('Select at least one UOM', 'top right','body','error-toast');
                    }

                }
                /*$scope.createProducts.product.name=this.searchText;
                $scope.createProducts.product.parent=this.subCategory1;
                $scope.createProducts.product.description=this.description;
                $scope.createProducts.product.productAttributes={grade:{definition:[]},quality:{definition:[]}};
                $scope.createProducts.product.productAttributes.grade.definition= $scope.createProducts.product.gradeDefinition;
                $scope.createProducts.product.productAttributes.quality.definition= $scope.createProducts.product.qualityDefinition;
                $scope.createProducts.product.hsnCodes = $scope.getSelected($scope.hsnCodes);
                $scope.createProducts.product.taxGroups=$scope.getSelected($scope.taxGroups);
                $scope.createProducts.product.unitOfMeasures=$scope.getSelected($scope.UOMS);
                categoryService.createProductMaster($scope.createProducts.product).then(function (response) {
                    $scope.subCategories2=response.data.children;
                    $scope.subCategory1= response.data._id;
                    $scope.clearProduct();
                    $scope.changeBack();
                }, function (res) {
                    $scope.error = res.data.message;
                    $mdToast.show(
                        $mdToast.simple()
                            .textContent($scope.error)
                            .position('bottom right')
                            .theme('error-toast')
                            .hideDelay(4000)
                    );
                });*/
            }else if($scope.isBrand){
                if($scope.getSelectedPageId()===0) {
                        $scope.createProducts.brand.name = this.searchBrandText;
                        $scope.createProducts.brand.description = this.description;
                        $scope.createProducts.brand.productCategory = $scope.selectedSubCategory._id;
                    if($scope.createProducts.brand.name && $scope.createProducts.brand.hsncode && $scope.createProducts.brand.taxGroup){
                        $scope.searchText = '';
                        $scope.setSelectedPageId($scope.selectedPageId+1);
                    }else{
                        var err;
                        if(!$scope.createProducts.brand.name){
                            err = 'Enter Brand Name';
                        }else if(!$scope.createProducts.brand.hsncode){
                            err = 'Select HSN Code';
                        }else if(!$scope.createProducts.brand.taxGroup){
                            err = 'Select Tax Group';
                        }
                        $scope.showToast(err,'top right', 'body', 'error-toast');
                    }
                }else if($scope.getSelectedPageId()===1){
                    $scope.createProducts.brand.unitOfMeasures = $scope.getSelected($scope.UOMS);
                    productBrandService.createProductBrandMaster($scope.createProducts.brand).then(function (response) {
                        if ($scope.productBrands) {
                            $scope.productBrands.push(response.data);
                        } else {
                            $scope.productBrands = response.data;
                        }
                        $scope.productBrand = response.data._id;
                        $scope.findBrandChange($scope.productBrand);
                        /*angular.element(document.querySelector('#'+response.data.name)).attr('selected','true');*/
                        //.options.namedItem(response.data.name).selected=true;//.selectedIndex=parseInt($scope.productBrands.length);//.options[parseInt($scope.productBrands.length-1)].selected=true;
                        //select.options.attr('selected','selected',$scope.productBrands.length-1);
                        $scope.unitOfMeasure = response.data.unitOfMeasures;
                        $scope.clearProduct();
                        $scope.changeBack();
                        $scope.setSelectedPageId($scope.selectedPageId+1);
                    }, function (res) {
                        $scope.error = res.data.message;
                        $scope.showToast($scope.error,'top right','body','error-toast');
                    });
                }
            }
            /*if ($scope.getSelectedPageId() === 0 ) {
                if($scope.isProduct) {
                    $scope.createProducts.product.name=this.searchText;
                    $scope.createProducts.product.parent=this.subCategory1;
                    $scope.createProducts.product.description=this.description;
                    $scope.createProducts.product.productAttributes={grade:{definition:[]},quality:{definition:[]}};
                    $scope.createProducts.product.productAttributes.grade.definition= $scope.createProducts.product.gradeDefinition;
                    $scope.createProducts.product.productAttributes.quality.definition= $scope.createProducts.product.qualityDefinition;
                    $scope.setSelectedPageId($scope.getSelectedPageId()+1);
                } else {
                    $scope.createProducts.brand.name=this.searchBrandText;
                    $scope.createProducts.brand.description=this.searchBrandText;
                    $scope.createProducts.brand.productCategory= $scope.selectedSubCategory._id;

                    $scope.setSelectedPageId($scope.getSelectedPageId()+1);
                }
            }else if($scope.getSelectedPageId() === 1) {
                if($scope.isProduct) {
                    $scope.createProducts.product.hsnCodes = $scope.getSelected($scope.hsnCodes);
                    $scope.setSelectedPageId($scope.getSelectedPageId()+1);
                } else {
                    $scope.createProducts.brand.unitOfMeasures=$scope.getSelected($scope.UOMS);
                    productBrandService.createProductBrandMaster( $scope.createProducts.brand).then(function (response) {
                        $scope.productBrands=response.data.productBrands;
                        $scope.unitOfMeasure=response.data.unitOfMeasures;
                        $scope.clearProduct();
                        $scope.changeBack();
                    }, function (res) {
                        $scope.error = res.data.message;
                    });
                }
            }else if($scope.getSelectedPageId() === 2){
                if($scope.isProduct) {
                    $scope.createProducts.product.taxGroups=$scope.getSelected($scope.taxGroups);
                    $scope.setSelectedPageId($scope.getSelectedPageId()+1);
                }
            } else if($scope.getSelectedPageId() === 3){
                if($scope.isProduct) {
                    $scope.createProducts.product.unitOfMeasures=$scope.getSelected($scope.UOMS);
                    categoryService.createProductMaster($scope.createProducts.product).then(function (response) {
                        $scope.subCategories2=response.data.children;
                        $scope.subCategory1= response.data._id;
                        $scope.clearProduct();
                        $scope.changeBack();
                    }, function (res) {
                        $scope.error = res.data.message;
                    });
                }
            }*/

        };
        $scope.isCompleted = function (page){
            var index = $scope.pages.indexOf(page);
            return (index < $scope.selectedPageId);
        };
        $scope.getSelectedPageId=function () {
            return $scope.selectedPageId;
        };
        $scope.setSelectedPageId=function (selectedPageId) {
            $scope.selectedPageId=selectedPageId;
        };
        $scope.showOfferDialog = function (ev) {
            $scope.selOffer=new Offers();
            $scope.selOffer.offerType='Sell';
            $scope.selOffer.isPublic=true;
            $scope.selOffer.validTill=new Date();
            $scope.selOffer.products=angular.copy(filterSelectedProductsOffer($scope.selOffer.offerType==='Sell'));
            var useFullScreen = ($mdMedia('sm') || $mdMedia('xs'));
            $scope.offerTypes = [{value: 'Sell', text: 'Seller'}, {value: 'Buy', text: 'Buyer'}];
            //args for findcontactsvsgroups: scope,isOnlyContacts,isCustomer,isSpecific,done
            contactsQuery.findContactsVsGroups($scope,false,false,false,function (contacts) {});
            $mdDialog.show({
                template:'<md-dialog><add-offer-product></add-offer-product></md-dialog>',
                targetEvent: ev,
                scope: $scope,
                clickOutsideToClose: false,
                fullscreen: useFullScreen,
                preserveScope: true
            });

        };
        $scope.createHsnCode=function (isCreate) {
            inventoryProduct.hsncodeCreate(this.hsncode).then(function (response) {
                // Show user success message and clear form
                /*$scope.register.registerusername = null;*/
                $scope.hsncodes = response.data.hsncodes;
                if ($scope.hsncodes[$scope.hsncodes.length - 1]){
                    if (isCreate) {
                        $scope.selectedItem.hsncode= $scope.hsncodes[$scope.hsncodes.length - 1].hsncode._id;
                    } else {
                        $scope.inventory.product.hsncode = $scope.hsncodes[$scope.hsncodes.length - 1].hsncode._id;
                    }
                }
                $mdDialog.cancel();
            }, function (errorResponse) {
                // Show user error message and clear form
                if (errorResponse) {
                    $scope.error = errorResponse.data.message;
                }
            });
        };

        $scope.showHsnCodeDialog = function (ev,isCreate) {

            var useFullScreen = ($mdMedia('sm') || $mdMedia('xs'));
            $scope.hsncode={};
            $scope.offerTypes = [{value: 'Sell', text: 'Seller'}, {value: 'Buy', text: 'Buyer'}];
            if(isCreate){
                $scope.hsncode.subcategory=this.selectedSubCategory._id;

            }else{
                if($scope.inventory) {
                    $scope.selectedMainCategory = $scope.inventory.product.category;
                    $scope.selectedSubCategory1Category = $scope.inventory.product.subCategory1;
                    $scope.selectedSubCategory = $scope.inventory.product.subCategory2;
                    $scope.hsncode.subcategory = $scope.inventory.product.subCategory2._id;
                }
            }
            $scope.isCreate=isCreate;
            $mdDialog.show({
                template:'<add-hsncode-product></add-hsncode-product>',
                targetEvent: ev,
                scope: $scope,
                clickOutsideToClose: false,
                fullscreen: useFullScreen,
                preserveScope: true
            });

        };

        $scope.showOrderDialog = function (ev) {
            $scope.selOrder=new Orders();
            $scope.selOrder.orderType='Sale';
            /*  $scope.selOffer.isPublic=true;
              $scope.selOffer.validTill=new Date();*/
            $scope.selOrder.products=filterSelectedProducts();
            $scope.showSellerChange=true;
            $scope.showBuyerChange=false;
            contactsQuery.findContacts($scope);
            //$scope.selOrder.seller={contact:$scope.selfcontact[0]};
            var useFullScreen = ($mdMedia('sm') || $mdMedia('xs'));
            $scope.orderTypes = [{value: 'Sale', text: 'Sale'}, {value: 'Purchase', text: 'Purchase'}];

            $mdDialog.show({
                template:'<md-dialog><add-order-product></add-order-product></md-dialog>',
                targetEvent: ev,
                scope: $scope,
                clickOutsideToClose: false,
                fullscreen: useFullScreen,
                preserveScope: true
            });

        };
        $scope.updateProduct = function() {
            if($scope.product && $scope.product.keywords && angular.isString($scope.product.keywords)){
                $scope.product.keywords=$scope.product.keywords.split(',');
            }
            var product;
            if($scope.showEditOnlyProduct){
                product = new Products($scope.inventory.product);
                /*  product.subCategory1=product.subCategory1._id;*/
                product.subCategory2=product.subCategory2._id;
                product.subCategory1=product.subCategory1._id;
                product.category=product.category._id;
                var selectHsncode=$scope.selectedHsncode(product.hsncode);
                if(selectHsncode.length>0 &&  selectHsncode[0].hsncode.hsncode)
                    product.hsncodes=selectHsncode[0].hsncode.hsncode;

                product.$update(function () {
                    $scope.showEditOnlyProduct=false;

                }, function (errorResponse) {
                    $scope.error = errorResponse.data.message;
                });
            }else {
                product = $scope.inventory;
                product.$update(function () {
                    $scope.showToast('Inventory Updated Succesfull','top right', 'md-dialog', 'success-toast');
                }, function (errorResponse) {
                    $scope.error = errorResponse.data+'. Changes will not be saved';
                    $scope.showToast($scope.error,'top right', 'md-dialog', 'error-toast');
                });
            }
        };
        $scope.getContact=function(contactId){
            if($scope.selfcontact[0]._id===contactId){
                return $scope.selfcontact;
            }else {
                return $scope.contacts.filter(function (item) {
                    return item._id === contactId;
                });
            }
        };

        $scope.createOrder= function () {
            $scope.selOrder=new Orders();
            $scope.selOrder.orderType='Sale';
            /* $scope.selOffer.isPublic=true;
              $scope.selOffer.validTill=new Date();*/
            $scope.selOrder.products=filterSelectedProducts();
            $scope.showSellerChange=true;
            $scope.showBuyerChange=false;
            //contactsQuery.findContacts($scope);
            var createOrderProducts=filterSelectedProducts();
            if($scope.selOrder.orderType==='Sale' && $scope.selOrder.buyer && $scope.selOrder.buyer.contact){
                var buyerContact = $scope.getContact( $scope.selOrder.buyer.contact._id);

                if (buyerContact.length> 0) {
                    $scope.selOrder.buyer.contact = buyerContact[0]._id;
                    if (buyerContact[0].nVipaniUser)
                        $scope.selOrder.buyer.nVipaniUser = buyerContact[0].nVipaniUser;
                }
                /*$scope.selOrder.seller.nVipaniUser=$scope.selfcontact[0].nVipaniUser;
                $scope.selOrder.seller.contact._id=$scope.selfcontact[0]._id;*/
            }else if($scope.selOrder.orderType==='Purchase' && $scope.selOrder.seller && $scope.selOrder.seller.contact){
                var sellerContact = $scope.getContact( $scope.selOrder.seller.contact._id);

                if (sellerContact.length> 0) {
                    $scope.selOrder.seller.contact = sellerContact[0]._id;
                    if (sellerContact[0].nVipaniUser)
                        $scope.selOrder.buyer.nVipaniUser = sellerContact[0].nVipaniUser;
                }
                /*$scope.selOrder.buyer={'nVipaniUser':$scope.selfcontact[0].nVipaniUser};
                $scope.selOrder.buyer=contact=$scope.selfcontact[0]._id;*/
            }
            // Redirect after save
            if(createOrderProducts.length>0) {
                inventoryProduct.orderCreateInventory(($scope.selOrder?$scope.selOrder:null),createOrderProducts).then(function (response) {
                    // Show user success message and clear form
                    /*$scope.register.registerusername = null;*/
                    if (response.data._id) {
                        $location.path('orders/' + response.data._id);
                    }
                }, function (errorResponse) {
                    // Show user error message and clear form
                    if (errorResponse) {
                        $scope.error = errorResponse.data.message;
                    }
                });
            }/*else{
             $scope.error ='Please select inventory for Place order';
             }*/
        };
        $scope.addUOMS=function () {
            if(!$scope.UOMS){
                $scope.UOMS=[];
            }
            $scope.UOMS.push({
                name: '',
                symbol: ''
            });
        };
        $scope.addTaxGroups=function () {
            if(!$scope.taxGroups){
                $scope.taxGroups=[];
            }
            $scope.taxGroups.push({
                name: '',
                IGST: '',
                description:''
            });
        };
        $scope.addHsnCodes=function () {
            if(!$scope.hsnCodes){
                $scope.hsnCodes=[];
            }
            $scope.hsnCodes.push({
                hsncode: '',
                description:''
            });
        };
        $scope.addGrade =function(update){
            if(update){
                $scope.selectedSubCategory=this.inventory.product.subCategory2;
                $scope.selectedItem=this.inventory.product;
            }else{
                $scope.selectedItem=this.selectedItem;
            }
            if ($scope.selectedSubCategory.productAttributes.grade.enabled) {

                if(!$scope.selectedItem || !($scope.selectedItem.gradeDefinition)) {
                    if (!$scope.selectedItem) {
                        $scope.selectedItem = {gradeDefinition:[]};
                    }
                    $scope.selectedItem.gradeDefinition=[];
                }
                /*if(!$scope.selectedItem.gradeDefinition) {
                 $scope.selectedItem.gradeDefinition={};
                 }*/
                $scope.selectedItem.gradeDefinition.push({
                    attributeKey: '',
                    attributeValue: ''
                });
                if(update){
                    $scope.inventory.product=$scope.selectedItem;
                }

            }
        };
        $scope.addGrades =function(update){
            if(!$scope.gradeDefinition)
                $scope.gradeDefinition=[];
            $scope.gradeDefinition.push({attributeKey: ''});
        };

        $scope.addQualities =function(update){
            if(!$scope.qualityDefinition)
                $scope.qualityDefinition=[];
            $scope.qualityDefinition.push({attributeKey: ''});
        };
        $scope.allowEditProduct=function (product) {
            return product && product.user && product.user===$scope.authentication.user._id;
        };
        $scope.addQuality =function(update){
            if(update){
                $scope.selectedSubCategory=this.inventory.product.subCategory2;
                $scope.selectedItem=this.inventory.product;
            }else {
                $scope.selectedItem = this.selectedItem;
            }
            if ($scope.selectedSubCategory.productAttributes.quality.enabled) {
                if (!$scope.selectedItem || !($scope.selectedItem.qualityDefinition)) {
                    if (!$scope.selectedItem) {
                        $scope.selectedItem = {qualityDefinition:[]};
                    }
                    $scope.selectedItem.qualityDefinition = [];
                }

                /* if (!$scope.selectedItem.qualityDefinition) {
                     $scope.selectedItem.qualityDefinition = [];
                 }*/
                $scope.selectedItem.qualityDefinition.push({
                    attributeKey: '',
                    attributeValue: ''
                });
                if(update){
                    this.inventory.product=$scope.selectedItem;
                }
            }

        };
        /*$scope.uploadCSVImages = function(){
            var categoriesData = {};
            categoriesData.csvValues=[];
            //flatten out header and values.
            $scope.matchedCategories.forEach(function(item){
                categoriesData.header=item.header;
                item.value.forEach(function(item1)
                {
                    categoriesData.csvValues.push(item1);
                });
            });
            var header = categoriesData.header;
            var lineData = categoriesData.csvValues;
            var inventoryData = $scope.inventory;
            //get the inventoryid of the object
            inventoryData.forEach(function(inventoryObj,index){

                //get the matching linedata
                //get all images to upload
                //upload individual images to server and update progress

                    lineData.forEach(function (csvLine) {
                        var brandImages = [];
                        var itemMasterImages = [];
                        var userItemMasterImages = [];
                        if(csvLine.srcLine==inventoryObj.srcLine) {
                            var imageFields = header.filter(function (field) {
                                return field.indexOf('Image') >= 0;
                            });
                            imageFields.forEach(function (imageField) {
                                if (imageField.startsWith('ProductBrandImage')) {
                                    brandImages.push(csvLine.lineData[header.indexOf(imageField)]);
                                }
                                else if (imageField.startsWith('ItemMasterImage')) {
                                    itemMasterImages.push(csvLine.lineData[header.indexOf(imageField)]);
                                }
                                else if (imageField.startsWith('UserItemMasterImage')) {
                                    userItemMasterImages.push(csvLine.lineData[header.indexOf(imageField)]);
                                }
                            });

                            //inventoryObj has inventory and srcLine
                            uploadImages(csvLine, inventoryObj.inventory, brandImages, itemMasterImages, userItemMasterImages);
                        }
                    });

                });

        };*/
        $scope.setImportLevel = function (level) {
            if(level===0){
                delete $scope.matchedProducts;
                $scope.selectedMatchedCategories = [];
                delete $scope.errorsummary;
                $scope.importButtonText = 'Proceed to upload file';
                $scope.importLevel = level;
            }else if(level===1){
                $scope.importLevel = level;
                delete $scope.matchedProducts;
                $scope.importButtonText = 'Proceed to Import with selected categories';
            }else if(level===2){
                $scope.importLevel = level;
                $scope.selectAllMatchedProducts = false;
                $scope.importButtonText = 'Import selected Inventories';
            }else{
                $scope.importLevel = level;
                $scope.importButtonText = 'Finish';
            }
        };
        $scope.getImportButtonStatus = function(level){
            if(level===0){
                return $scope.result;
            }else if(level===1){
                return ($scope.selectedMatchedCategories && $scope.selectedMatchedCategories.length > 0) ||($scope.result && $scope.results.matchedCategories && $scope.results.matchedCategories.filter(function (value) { return  value.selected && value.selected===true; }).length>0);
            }else if(level===2){
                return $scope.matchedProducts &&($scope.matchedProducts.filter(function (eachData) {
                    return eachData.selected===true;
                }).length>0);
            }else if(level===3){
                return true;
            }
        };
        $scope.getSelectedCategoriesName=function () {
          if($scope.selectedMatchedCategories && $scope.selectedMatchedCategories.length>0){
              var categoriesName='';
                  $scope.selectedMatchedCategories.forEach(function (value) { categoriesName+=value.name; });
              return categoriesName;
          } else{
              return '';
          }
        };
        $scope.importNextStep = function(level){
            if(level===0){
                $scope.setImportLevel(level+1);
            }else if(level===1){
                inventoryImport.updateMatchingProducts($scope);
                $scope.setImportLevel(level +1);
            }else if(level===2){
                //add the selected products to the user item master.
                //sample data to create:
                // {"userinventories":[{"isImport":true,"productBrand":"5a532f03e078bee968ba1ef4",
                // "inventory":[{"numberOfUnits”:"12"}]}]}
                inventoryImport.addToUserItemMaster($scope).then(function (response) {
                    // Show user success message and clear form
                    /*$scope.register.registerusername = null;*/
                   var inventory = response.data;

                    if(inventory && inventory.length>0) {
                        $scope.importCount=inventory.length;
                        angular.forEach(inventory, function(inv){
                            $scope.inventories.push(inv.inventory);
                        });
                        $scope.getInventorySummary($scope.busUnitId);
                        $scope.setImportLevel(level + 1);
                    }else{
                        $mdToast.show(
                            $mdToast.simple()
                                .textContent('Failed to import selected Inventories, Please try again')
                                .position('bottom right')
                                .theme('error-toast')
                                .hideDelay(4000)
                        );
                    }

                }, function (errorResponse) {
                    // Show user error message and clear form

                    if (errorResponse && errorResponse.data.message) {
                        $scope.error=errorResponse.data.message;
                    }else {
                        $scope.error='Failed to import inventory, Please try again';
                    }
                        $mdToast.show(
                            $mdToast.simple()
                                .textContent($scope.error)
                                .position('bottom right')
                                .theme('error-toast')
                                .hideDelay(4000)
                        );
                });

            }else if(level === 3){
                //$scope.getInventorySummary($scope.busUnitId);
                $scope.panelChange('cancelViewAll');
            }
        };
        $scope.getViewFileTemplate=function (option) {

            $http.get('fileexport'+'?type='+option.toLowerCase(),{ responseType: 'arraybuffer' }).then(function (res) {
                /*var reportMimeType = 'application/pdf';
                if (response.reports[0].format === 'XLS') {`
                    reportMimeType = 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet';
                }*/
                var a = document.createElement('a');
                document.body.appendChild(a);
                a.style = 'display: none';
                var reportMimeType = 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet';

                var file = new Blob([res.data], {type: reportMimeType});
                var fileURL = URL.createObjectURL(file);
                a.href = fileURL;
                a.download = 'product-import-template.xlsx';
                a.click();
                window.URL.revokeObjectURL(fileURL);
            });
        };
        $scope.changeSelectedCategory = function(isSingle){
            if($scope.results.matchedCategories && angular.isArray($scope.results.matchedCategories)){
                $scope.selectedMatchedCategories = $scope.results.matchedCategories.filter(function (eachCategory) {
                  return eachCategory.selected;
                });
            }
        };
       /* $scope.changeMatchedProductSelection = function(isSingle){
            if(isSingle) {
                var flag = 0;
                for (var i = 0; i < $scope.matchedProducts.length; i++) {
                    if ($scope.matchedProducts[i].selected) {
                        flag++;
                    }
                }
                $scope.selectedMatchedProducts = flag;
                $scope.selectAllMatchedProducts = flag === $scope.matchedProducts.length;
            }
            else{
                if($scope.selectAllMatchedProducts) {
                    for (i = 0; i < $scope.matchedProducts.length; i++) {
                        $scope.matchedProducts[i].selected = true;
                    }
                    $scope.selectedMatchedProducts = $scope.matchedProducts.length;
                }
                else{
                    for (i = 0; i < $scope.matchedProducts.length; i++) {
                        $scope.matchedProducts[i].selected = false;
                    }
                    $scope.selectedMatchedProducts = 0;
                }
            }
        };*/
        $scope.scrollable = function(event){
            var elem=event.type;
        };
    }
]);
app.controller('TrackerController', [ '$scope','$stateParams','inventoryProduct',function ($scope,$stateParams,inventoryProduct) {
    $scope.scanProductDetails = function () {
        inventoryProduct.getScanProduct($stateParams).then(function (response) {
            $scope.productDetails = response.data;
        }, function (res) {
            $scope.errorDetails = res.data.message;
        });
    };
    $scope.getPublicPaths = function (batchNumber,businessUnit) {
        inventoryProduct.getPublicPaths({batchNumber:batchNumber,businessUnit:businessUnit}).then(function (response) {
            $scope.publicPathResponse = response.data;
        }, function (res) {
            $scope.error = res.data.message;
        });
    };
}]);

app.directive('tree', function($compile) {
    return {
        restrict: 'E',
        scope: {family: '='},
        template:
        '<ul>' +
        '<li ng-repeat="child in family">' +
        '<p>{{child.product.brandName }}</p>'+
        /*'<tree family="child"></tree>' +*/
        '</li>' +
        '</ul>',
        compile: function(tElement, tAttr) {
            var contents = tElement.contents().remove();
            var compiledContents;
            return function(scope, iElement, iAttr) {
                if(!compiledContents) {
                    compiledContents = $compile(contents);
                }
                compiledContents(scope, function(clone, scope) {
                    iElement.append(clone);
                });
            };
        }
    };
});
app.directive('getInvalidEntry', function () {
    return {
        restrict: 'A',
        link: function (scope, elem ,attrs) {
            // set up event handler on the form element
            /* // if (attrs.getInvalidEntry === 'true') {*/
            elem.on('submit', function () {
                // find the first invalid element

                var firstInvalid = elem[0].querySelector('.ng-invalid');
                if (firstInvalid && firstInvalid.tagName.toLowerCase() === 'ng-form') {
                    firstInvalid = firstInvalid.querySelector('.ng-invalid');
                }
                // if we find one, set focus
                if (firstInvalid) {
                    firstInvalid.focus();
                }
            });
            /*   }*/
        }
    };
});
app.directive('trackerView', function(){
    return{
        restrict: 'E',
        templateUrl: 'modules/inventories/views/track-view.client.viewnew.html'
    };
});
app.directive('stockDetails', function(){
    return{
        restrict: 'E',
        templateUrl: 'modules/products/views/stock-view.client.viewnew.html'
    };
});
app.directive('convertView', function(){
    return{
        restrict: 'E',
        templateUrl: 'modules/products/views/convert-view.client.viewnew.html'
    };
});
app.directive('nvcTracker', function ($compile) {
    return {
        restrict: 'E',
        scope: {tree: '=tree',treelength: '=treelength', parent:'=parent'},
        templateUrl: 'modules/inventories/views/tracker-node-view.client.viewnew.html',
        compile: function(element) {
            if(element) {
                element = element.closest('.nvc-tracker');
                return function (element, trackerLink) {
                    if (angular.isFunction(trackerLink)) {
                        trackerLink = {post: trackerLink};
                    }
                var trackerElement = element.contents().remove();
                    var trackerContents;
                    return {
                        pre: (trackerLink && trackerLink.pre) ? trackerLink.pre : null,
                        post: function (scope, element) {
                            if (!trackerContents) {
                                trackerContents = $compile(trackerElement);
                            }
                            trackerContents(scope, function (clone) {
                                element.after(clone);
                            });
                            if (trackerLink && trackerLink.post) {
                                trackerLink.post.apply(null, arguments);
                            }
                        }
                    };
                };
            }
        },
        controller: ['$scope','$compile', function ($scope,$compile) {
            $scope.closeSelectionView = function(){
                var el = angular.element(document.querySelector('.nvc-tracker-prod-selection'));
                el[0].parentNode.removeChild(el[0]);
                var elem = angular.element(document.querySelector('md-dialog.trowser'));
                elem[0].children[0].style.position = 'relative';
                elem[0].children[0].style.opacity = '1';
                elem[0].children[1].style.left = '0';
            };
            $scope.changeNode = function(parent,index,current){
                var temp = parent.destinations[current];
                parent.destinations[current] = parent.destinations[index];
                parent.destinations[index] = temp;
                $scope.parent = parent;
                $scope.closeSelectionView();
            };
            $scope.expandSelectionView = function(parent,index,current){
                var elem = angular.element(document.querySelector('md-dialog.trowser'));
                elem[0].children[0].style.position = 'absolute';
                elem[0].children[0].style.opacity = '0.7';
                elem[0].children[1].style.left = '25%';
                var html = angular.element('<div class="nvc-rfloat bb-wid-25 nvc-tracker-prod-selection nvc-view-separator">' +
                    '<div class="nvc-slider-form">' +
                    '<div class="nvc-max-height">' +
                    '<div layout="row" layout-align="center center" class="nvc-sticky-top">\n' +
                    '            <md-button class="md-icon-button md-mini" ng-click="closeSelectionView()" aria-label="close dialog"><md-icon class="fa fa-times fa-lg"></md-button>\n' +
                    '            <h4 flex class="nv-text-center nv-text-bold">Product List</h4>\n' +
                    '        </div>' +
                    '<div><div ng-repeat="item in parent.destinations track by $index" layout="row" layout-padding layout-align="start center" ng-click="changeNode(parent,$index,0)">' +
                    '<div flex="20"><img ng-src="{{item.inventory.inventoryImageURL1}}" class="bb-wid-100 nvc-primary-border img-circle" square-image/></div>' +
                    '<div flex="75"><h6 class="nv-text-bold">{{item.inventory.productName}}</h6><h6>{{item.stockMasters[0].count}} {{item.inventory.unitOfMeasureName}}</h6></div>' +
                    '<div ng-show="$index===0"><span class="fa fa-check nvc-primary-color"></span></div> </div></div>' +
                    '</div>' +
                    '</div>' +
                    '</div>');
                html = $compile(html)($scope)[0];
                if(elem[0].children[1].nextElementSibling) {
                    elem[0].children[1].nextElementSibling.remove();
                }
                elem[0].children[1].after(html);
            };
        }]
    };
});
/*app.directive("nvcTrackerNode", function(){
    return{
        restrict:'E',
        scope:{tree:'=tree',parent:'=parent',treelength:'=treelength'},
        template:'<nvc-tracker tree="child" treelength="treelength" parent="parent"></nvc-tracker>',
        controller: ['$scope', function($scope){
            $scope.child = '';
            angular.forEach($scope.tree.destinations, function (singleDestination) {
               if(singleDestination.selected){
                   $scope.child = singleDestination;
               }
            });
            if(!$scope.child){
                $scope.child = $scope.tree.destinations[0];
            }
        }]
    }
});*/
