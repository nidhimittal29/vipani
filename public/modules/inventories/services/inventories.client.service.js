'use strict';

//Inventories service used to communicate Inventories REST endpoints
var app=angular.module('inventories');
app.factory('Inventories', ['$resource',
    function ($resource) {
        return $resource('inventories/:inventoryId', {
            inventoryId: '@_id'
        }, {
            update: {
                method: 'PUT'
            }
        });
    }
]);
app.factory('InventoryCommon', ['$resource',
    function ($resource) {
        return $resource('inventories/:inventoryId', {
            inventoryId: '@_id'
        }, {
            update: {
                method: 'PUT'
            }
        });
    }
]);
app.directive('singleProduct',function () {
    return {
        restrict:'A',
        /* scope:{
         contact:'='
         },*/
        templateUrl: 'modules/products/views/product-row.client.view.html'
    };
});
app.directive('createFileImport',function () {
    return {
        restrict:'E',
        templateUrl:'modules/categories/views/create-fileimport.client.view.html'
    };

});
app.directive('addCategoryProduct',function () {
    return {
        restrict:'E',
        templateUrl:'modules/products/views/create-productc.client.viewnew.html'
    };

});
app.directive('viewAllProducts',function () {
    return {
        restrict:'E',
        templateUrl:'modules/products/views/list-inventory.table.client.viewnew.html',
        scope:{
            inventories:'=products',
            paginationId:'@paginationId',
            type:'@type'

        }
    };

});
app.directive('viewProducts',function () {
    return {
        restrict:'E',
        templateUrl:'modules/products/views/list-inventory.table.client.viewnew.html'
    };

});
app.directive('viewProductCards',function () {
    return {
        restrict:'E',
        templateUrl:'modules/products/views/list-inventory.cards.client.viewnew.html'
    };

});
app.directive('mdChips', function($timeout) {
    return {
        restrict: 'E',
        require: 'mdChips', // Extends the original mdChips directive
        link: function(scope, element, attributes, mdChipsCtrl) {

            var mouseUpActions = [];

            mdChipsCtrl.onInputBlur = function(event) {
                this.inputHasFocus = false;

                mouseUpActions.push((function() {
                    var chipBuffer = this.getChipBuffer();
                    if (chipBuffer !== '') { // REQUIRED, OTHERWISE YOU'D GET A BLANK CHIP
                        this.appendChip(chipBuffer);
                        this.resetChipBuffer();
                    }
                }).bind(this));
            };

            window.addEventListener('click', function(event) {
                while (mouseUpActions.length > 0) {
                    var action = mouseUpActions.splice(0, 1)[0];
                    $timeout(function() {
                        $timeout(action);
                    });
                }
            }, false);
        }
    };
});

app.factory('fileImport',['$stateParams', '$http',function($stateParams,$http){
    return {
        createImport: function (result) {
            return $http.post('/importFile', result);
        }
    };
}]);
app.directive('createImport',function () {
    return {
        restrict:'E',
        templateUrl: 'modules/products/views/import-inventory.client.view.html'
    };
});
app.factory('inventoryImport',['$stateParams', '$http','inventoryProduct','categoryService',function($stateParams,$http,inventoryProduct,categoryService){
    function findProductBrandObject(key,brandsArray){
        var brand= brandsArray.filter(function (eachBrand) {
            return eachBrand.brand===key;
        });
        if(brand && brand.length>0){
            return brand[0];
        }else {
            return null;
        }

    }
    function toISODate(date){
        var fields = date.split('-');
        return fields[2]+'-'+fields[1]+'-'+fields[0];
    }

    function validateStockMasters(stockmasters){
        return stockmasters.filter(function (eachStockMaster) {
            return eachStockMaster.batchNumber && eachStockMaster.batchNumber.trim().length>0;
        });

    }

    function getFileProductData(fileLineData,header)
    {
        var indexOfSubCategory1,indexOfProductName,indexOfProductBrand,indexOfHSNCode,indexOfNumUnits;
        var stockMasterHeaderFields=header.filter(function(fieldName){
            return fieldName.startsWith('StockMaster');
        });

        indexOfSubCategory1 = header.indexOf('SubCategory1');
        indexOfProductName = header.indexOf('ProductName');
        indexOfProductBrand = header.indexOf('BrandName');
        indexOfHSNCode = header.indexOf('HSNCode');
        indexOfNumUnits = header.indexOf('NumberOfUnits');
        var stockmasters = [];
        var curCount=-1;
        /* for(var l=0;l<stockMasterHeaderFields.length;l++) {*/
        angular.forEach(stockMasterHeaderFields,function (eachStockMasterField) {

            var l=stockMasterHeaderFields.indexOf(eachStockMasterField);
            var smfields = stockMasterHeaderFields[l].split('.');

            var count = parseInt(smfields[1])-1;
            if(curCount===-1 || curCount!==count){
                curCount=count;
                stockmasters[curCount]={};
            }
            switch(smfields[2])
            {
                case 'OpeningBalance': {
                    stockmasters[curCount].openingBalance = fileLineData.lineData[header.indexOf(stockMasterHeaderFields[l])];
                    break;
                }
                case 'BatchNumber': {
                    /* if(fileLineData.lineData[inx]!=='' && !isNaN(fileLineData.lineData[inx].valueOf())) {*/
                    stockmasters[curCount].batchNumber = fileLineData.lineData[header.indexOf(stockMasterHeaderFields[l])];
                    /* }*/
                    break;
                }
                case 'CurrentBalance': {
                    /*if(fileLineData.lineData[inx]!=='' && !isNaN(fileLineData.lineData[inx].valueOf())) {*/
                    stockmasters[curCount].currentBalance = fileLineData.lineData[header.indexOf(stockMasterHeaderFields[l])];
                    /* }*/
                    break;
                }
                case 'MRP': {
                    /*if(fileLineData.lineData[inx]!=='' && !isNaN(fileLineData.lineData[inx].valueOf())) {*/
                    stockmasters[curCount].MRP = fileLineData.lineData[header.indexOf(stockMasterHeaderFields[l])];
                    /*}*/
                    break;
                }
                case 'MfgDate': {
                    stockmasters[curCount].manufacturingDate = new Date(toISODate(fileLineData.lineData[header.indexOf(stockMasterHeaderFields[l])]));
                    break;
                }
                case 'PkgDate': {
                    /* if(fileLineData.lineData[inx]!=='' && !isNaN(fileLineData.lineData[inx].valueOf())) {*/
                    stockmasters[curCount].packagingDate = new Date(toISODate(fileLineData.lineData[header.indexOf(stockMasterHeaderFields[l])]));
                    /*}*/
                    break;
                }
                case 'BestBefore': {
                    if(fileLineData.lineData[header.indexOf(stockMasterHeaderFields[l])]!=='' && !isNaN(fileLineData.lineData[header.indexOf(stockMasterHeaderFields[l])].valueOf())) {
                        stockmasters[curCount].bestBefore = fileLineData.lineData[header.indexOf(stockMasterHeaderFields[l])].valueOf();
                    }
                    break;
                }
            }
            /* }*/
        });
        return {subCategory1:fileLineData.lineData[indexOfSubCategory1],
            productName:fileLineData.lineData[indexOfProductName],
            productBrand:fileLineData.lineData[indexOfProductBrand],
            hsnCode:fileLineData.lineData[indexOfHSNCode],
            numberOfUnits:fileLineData.lineData[indexOfNumUnits],
            stockMaster:validateStockMasters(stockmasters),
            srcLine:fileLineData.srcLine};
    }
    function getMatchingSourceLine(valueArray,srcLine){
        for(var i=0;i<valueArray.length;i++){
            if(valueArray[i].srcLine===srcLine){
                return valueArray[i];
            }
        }
        return null;
    }

    return {
        addToUserItemMaster:function(scope){
            var userItemMasterData = [];
            for(var i=0;i<scope.matchedProducts.length;i++) {
                var productBrandsObj;
                var inventoryItem;
                var matchedProduct = scope.matchedProducts[i];
                if (matchedProduct.selected) {
                    var uomId = matchedProduct.productData.unitOfMeasure;
                    var productId = matchedProduct.productData.product;
                    var numUnits = matchedProduct.lineData.numberOfUnits;
                    var productBrandId = matchedProduct.productData.productBrandId;

                    productBrandsObj= findProductBrandObject(productBrandId,userItemMasterData);

                    if(productBrandsObj){
                        inventoryItem = {};
                        inventoryItem.srcLine = matchedProduct.lineData.srcLine;
                        inventoryItem.itemMasterId = productId;
                        inventoryItem.unitOfMeasure = uomId;
                        inventoryItem.stockMaster = matchedProduct.lineData.stockMasters;
                        if(numUnits){
                            inventoryItem.numberOfUnits = numUnits;
                        }
                        productBrandsObj.inventory.push(inventoryItem);

                    }
                    else{
                        productBrandsObj = {};
                        var inventoryArray=[];
                        inventoryItem = {};
                        inventoryItem.srcLine = matchedProduct.lineData.srcLine;
                        inventoryItem.itemMasterId = productId;
                        inventoryItem.unitOfMeasure = uomId;
                        inventoryItem.stockMaster = matchedProduct.lineData.stockMaster;
                        if(numUnits){
                            inventoryItem.numberOfUnits = numUnits;
                        }
                        inventoryArray.push(inventoryItem);
                        productBrandsObj.productBrand=productBrandId;
                        productBrandsObj.inventory=inventoryArray;
                        productBrandsObj.isImport=true;

                        userItemMasterData.push(productBrandsObj);

                    }


                }

            }

            var userInventories = {userinventories:userItemMasterData,businessUnit:scope.busUnitId};
            return  inventoryProduct.productImportUserInventory(userInventories);
        },
        updateMatchingProducts:function(scope) {
            scope.matchedProducts = [];
            //console.log(JSON.stringify(selectedCategory));
            var selectedCategories = [];
            for (var k = 0; k < scope.results.matchedCategories.length; k++) {
                var selectedCategory = scope.results.matchedCategories[k];
                if (selectedCategory.selected) {
                    selectedCategory.header = scope.results.header;
                    selectedCategories.push(selectedCategory);
                }
            }
            categoryService.findProductsByName({businessUnit:scope.busUnitId,categories:selectedCategories}).then(function (response) {
                scope.productErrors = [];
                for (var k = 0; k < selectedCategories.length; k++) {
                    var selectedCategory = selectedCategories[k];
                    var productsFromCatalog = response.data[k].value;
                    var productErrors = response.data[k].error;
                    if (productsFromCatalog && productsFromCatalog.length !== 0) {
                        //for each product from list, match the products from catalog.
                        for (var i = 0; i < productsFromCatalog.length; i++) {
                            if (productsFromCatalog[i].product && productsFromCatalog[i].srcLine) {
                                //get matching source line from file
                                var fileLineData = getMatchingSourceLine(selectedCategory.value, productsFromCatalog[i].srcLine);
                                var fileProductData = getFileProductData(fileLineData, selectedCategory.header);
                                if (fileLineData !== null) {
                                    scope.matchedProducts.push({
                                        lineData: fileProductData,
                                        productData: productsFromCatalog[i]
                                    });

                                }
                            }
                        }
                    }
                    if (productErrors) {
                        for (var l = 0; l < productErrors.length; l++) {
                            scope.productErrors.push({
                                srcLine: productErrors[l].data.srcLine,
                                errorMessage: productErrors[l].errorMessage
                            });
                        }
                    }

                }
            });
        }
    };
}]);
app.factory('inventoryProduct',['$stateParams', '$http','$mdToast',function($stateParams,$http,$mdToast){

    function filterSelectedInventories($scope) {
        var selectedInventories={'inventories':[]};
        angular.forEach($scope.inventories,function (inventory) {
            if(inventory.selected){
                selectedInventories.inventories.push({_id:inventory._id});
            }
        });
        return selectedInventories;
    }

    function getUpdatedInventories($scope,updatedInventories) {
        for (var i=0;i<$scope.inventories.length ;i++){
            for (var j=0;j<updatedInventories.length ;j++){
                if($scope.inventories[i]._id===updatedInventories[j]._id) {
                    $scope.inventories[i] = updatedInventories[j];
                }
            }
        }
    }
    return {
        getTrackPaths:function (trackFields){
            return  $http.get('/trackPaths?inventoryTrackId='+trackFields.inventoryId+'&stockMasterTrackId='+trackFields.stockId);
        },
        getScanProduct:function (scanFields) {
            return $http.get('/scanProduct?batchNumber='+scanFields.batchNumber+'&businessUnit='+scanFields.businessUnit);
        },
        getPublicPaths:function (trackFields) {
            return $http.get('/tracker?batchNumber='+trackFields.batchNumber+'&businessUnit='+trackFields.businessUnit);
        },
        massUpdateImages:function(inventoriesToUpload){
            return $http.post('/inventories/massUpdateImages',inventoriesToUpload);
        },
        updateInventory:function(inventory){
            return $http.put('/inventories',inventory);
        },
        findItemMasterById:function(itemMasterId){
            return $http.get('/itemMaster/',itemMasterId);
        },
        updateItemMaster:function(itemMaster){
            return $http.put('/itemMaster/',itemMaster);
        },
        productImportUserInventory:function(userInventories){
            return $http.post('/inventories/createUserItemMaster',userInventories);
        },
        productInventoryCreate:function (productVsInventory) {
            return $http.post('/inventories/createProduct',productVsInventory);
        },
        productInventoryMasterCreate:function (productVsInventory) {
            return $http.post('/inventories/createInventoryMaster',productVsInventory);
        },
        hsncodeCreate:function (hsncodes) {
            return $http.post('/categories/hsncodeCreate',hsncodes);
        },
        orderCreateInventory:function (order,productVsInventory) {
            return $http.post('createProductOrder',{order:order,products:productVsInventory});
        },
        getInventories:function (paginationKeys) {
            return $http.get('/inventories', {params: paginationKeys});
        },
        getInventoriesByBUnit:function (businessUnitId,paginationKeys) {
            return $http.get('/busUnitInventories/'+businessUnitId, {params: paginationKeys});
        },
        productInventoryConversion:function (productVsInventory) {
            return $http.post('/inventories/createInventoryConversion',productVsInventory);
        },
        inventorySelectionQuery:function (productVsInventory) {
            return $http.post('/inventories/matchedinventories',productVsInventory);
        },
        getQuantityPrice : function (moqVsPrice, quantity,unitPrice,index) {
            var price =unitPrice;
            if(moqVsPrice!==undefined && moqVsPrice && moqVsPrice.length>=1) {
                var suitableMOQ;
                var suitableMin =this.getMinQuantityPrice(moqVsPrice,quantity);
                var next = suitableMin;
                if(suitableMin) {
                    if (suitableMin.MOQ === quantity || !quantity) {
                        return unitPrice * (1 - (suitableMin.margin / 100));
                    } else {
                        for (var i = 0; i < moqVsPrice.length; i++) {
                            next = moqVsPrice[i];
                            if (suitableMin.MOQ <= next.MOQ && next.MOQ <= quantity) {
                                suitableMin = moqVsPrice[i];
                            }
                        }
                        return unitPrice * (1 - (suitableMin.margin / 100));
                    }
                }else{
                    return unitPrice;
                }

            }
            return price;
        },
        getMatchedMoq : function (moqVsPrice, quantity,unitPrice,index) {
            if(moqVsPrice!==undefined && moqVsPrice && moqVsPrice.length>=1) {
                var suitableMOQ;
                var suitableMin =this.getMinQuantityPrice(moqVsPrice,quantity);
                var next = suitableMin;
                if(suitableMin) {
                    if (suitableMin.MOQ === quantity || !quantity) {
                        return suitableMin;
                    } else {
                        for (var i = 0; i < moqVsPrice.length; i++) {
                            next = moqVsPrice[i];
                            if (suitableMin.MOQ <= next.MOQ && next.MOQ <= quantity) {
                                suitableMin = moqVsPrice[i];
                            }
                        }
                        return suitableMin;
                    }
                }else{
                    return suitableMin;
                }

            }
        },
        getMinQuantityPrice : function (moqVsPrice,quantity) {
            var suitableMOQ;
            if(moqVsPrice!==undefined && moqVsPrice && moqVsPrice.length>=1) {
                //price = moqVsPrice[0].price;
                for (var i = 0; i < moqVsPrice.length; i++) {
                    var moq = moqVsPrice[i].MOQ;
                    if (moq <=quantity ) {
                        suitableMOQ=  moqVsPrice[i];
                    }
                }
            }
            return suitableMOQ;
        },
        getMaxQuantity : function (product,isBuy) {
            var moqVsPrice=isBuy?product.moqAndBuyMargin:product.moqAndMargin;
            var suitableMOQ=0;
            if(moqVsPrice!==undefined && moqVsPrice && moqVsPrice.length>=1) {
                suitableMOQ=Math.max.apply(null, moqVsPrice.map(function(o) { return o.MOQ; }));
            }
            return Math.max(product.numberOfUnits,product.maxUnits, suitableMOQ);
        },
        getMinQuantity : function (moqVsPrice,quantity) {
            var suitableMOQ;
            if(moqVsPrice!==undefined && moqVsPrice && moqVsPrice.length>=1) {
                if(!quantity || (quantity && quantity===0)) {
                    quantity = Math.min.apply(null, moqVsPrice.map(function (o) {
                        return o.MOQ;
                    }));
                }
                for (var i = 0; i < moqVsPrice.length; i++) {
                    var moq = moqVsPrice[i].MOQ;
                    if (moq<=quantity) {
                        moq=moqVsPrice[i].MOQ;
                        suitableMOQ = moqVsPrice[i];
                    }
                }
            }
            return suitableMOQ;
        },
        getMinMOQ:function (moqVsPrice) {
            return Math.min.apply(null, moqVsPrice.map(function (o) {
                return o.MOQ;
            }));
        },
        getMaxMOQ:function (moqVsPrice) {
            return Math.max.apply(null, moqVsPrice.map(function (o) {
                return o.MOQ;
            }));
        },
        getMOQBasedQuantities : function (moqVsPrice,quantity) {
            var suitableMOQs=[];
            if(moqVsPrice!==undefined && moqVsPrice && moqVsPrice.length>=1) {
                for (var i = 0; i < moqVsPrice.length; i++) {
                    var moq = moqVsPrice[i].MOQ;
                    if (moq>=quantity) {
                        suitableMOQs.push(moqVsPrice[i]);
                    }
                }
            }
            return suitableMOQs;
        },
        createStock:function (stock,businessUnit) {
            if(stock._id){
                return $http.put('stockMaster/'+stock._id, stock);
            }else {
                stock.businessUnit=businessUnit;
                return $http.post('stockMaster', stock);
            }
        },
        getMasterData:function (selectedCategories,businessUnit) {
            return $http.post('/fetchProductMaster',{categories:selectedCategories,businessUnit:businessUnit});
        },
        createMassUserItemMaster:function (selectedInventories) {
            return $http.post('/inventories/createUserItemMaster',selectedInventories);
        },
        prepareUserItemMaster:function (masterData,userItemMasterData) {
            angular.forEach(masterData,function (eachProduct) {
                angular.forEach(eachProduct.productBrands,function (eachProductBrand) {
                    angular.forEach(eachProductBrand.unitOfMeasures,function (eachUom) {
                        userItemMasterData.push({selected:false,'uom':eachUom,brand:eachProductBrand,product:eachProduct,'inventory':{'unitOfMeasure':eachUom._id,'MRP':0.00, 'numberOfUnits':''},'productBrand':eachProductBrand._id});
                        /* uomCallback();*/
                    });
                    /*brandCallback();*/
                });
                /*callback();*/

            });
            return userItemMasterData;
        },
        selectedUserItems: function (masterData) {
            var selectedInventories={'userinventories':[]};
            angular.forEach(masterData,function (records) {
                if(records.selected===true){
                    selectedInventories.userinventories.push({inventory:records.inventory,productBrand:records.productBrand});
                }
            });
            return selectedInventories;
        },
        batchInventories:function (index,scope) {
            var selectedInventories=filterSelectedInventories(scope);
            if(selectedInventories.inventories.length>0) {
                if (index === 0) {

                    return $http.post('/inventoriesremove', selectedInventories).then(function (results) {
                        scope.getInventorySummary(scope.busUnitId);
                        scope.inventories = results.data.inventory;
                        scope.total_count = results.data.total_count;
                    }, function (err) {
                        if (err) {
                            scope.error = err.data.message;
                        }
                    });

                } else if (index === 1) {
                    $http.post('/inventoriesinactive', selectedInventories).then(function (results) {
                        getUpdatedInventories(scope, results.data);
                        scope.getInventorySummary(scope.busUnitId);
                    }, function (err) {
                        if (err) {
                            scope.err = err.data.message;
                        }
                    });
                } else if (index === 2) {
                    $http.post('/inventoriesactive', selectedInventories).then(function (results) {
                        getUpdatedInventories(scope, results.data);
                        scope.getInventorySummary(scope.busUnitId);
                    }, function (err) {
                        if (err) {
                            scope.err = err.data.message;
                        }
                    });
                }
            }else {
                $mdToast.show(
                    $mdToast.simple()
                        .textContent('Please select at least one Inventory')
                        .position('bottom right')
                        .theme('error-toast')
                        .hideDelay(3000)
                );
            }
        },
        inventorySummary:function (businessUnitId) {
            return $http.get('inventorysummary?businessUnitId='+businessUnitId);
        },
        filterInventorySummary:function (businessUnitId, paginationKeys,data) {
            return $http.post('inventorysummary?businessUnitId='+businessUnitId,data,{params:paginationKeys});
        },

    };
}]);
app.factory('inventoryProductConversion',['$http',function ($http) {
    function inventoriesId(selectedInventories,selectedProducts){

        angular.forEach(selectedInventories,function (eachInventory) {
            selectedProducts.push({_id:eachInventory._id});
        });
        return selectedProducts;
    }
    function getSelectedSourceConversionInventories(sourceProducts) {
        var sourceProduct=[];
        angular.forEach(sourceProducts,function (eachProduct) {
            var stockMasters=[];
            /*if(eachProduct.selected) {*/
            angular.forEach(eachProduct.stockMasters, function (eachStockMaster) {
                if(eachStockMaster.selected && eachStockMaster.currentBalance>0)
                    stockMasters.push({'_id': eachStockMaster._id, 'currentBalance': eachStockMaster.currentBalance});

            });
            if(stockMasters.length>0)
                sourceProduct.push({'inventory': {_id: eachProduct._id, convertStockMasters: stockMasters}});
            /* }*/

        });
        return sourceProduct;

    }
    function getSelectedConvertInventories(convertProduct,productBrand,unitOfMeasure,stock,targetBusinessUnit){
        if(convertProduct&& convertProduct._id && convertProduct._id!==null){
            return {productBrand:productBrand,inventory:{businessUnit:targetBusinessUnit,_id:convertProduct._id,MRP:stock.MRP,unitOfMeasure:unitOfMeasure,convertStockMasters:[stock]}};
        }else{
            return {productBrand:productBrand,inventory:{businessUnit:targetBusinessUnit,MRP:stock.MRP,unitOfMeasure:unitOfMeasure,convertStockMasters:[stock]}};
        }

    }
    return {

        getSourceInventories:function (products) {
            var selectedProducts=[];
            return $http.post('/inventories/matchedinventories',{inventories:inventoriesId(products,selectedProducts),issource:true});

        },
        validateConvertSource:function(conversionProducts){
            return getSelectedSourceConversionInventories(conversionProducts).length>0;
        },
        validateConvertInventoryStock:function(stock){
            return stock && stock.batchNumber&& stock.MRP && stock.currentBalance>0;
        },
        validateConvertInventory:function(conversionProducts,subCategory2,subCategory1,productBrand,unitOfMeasure){
            return unitOfMeasure && productBrand && subCategory2 && getSelectedSourceConversionInventories(conversionProducts).length>0;

        },
        createConvertInventories:function (convertProduct,productBrand,unitOfMeasure,stock,sourceProducts,businessUnit,convertType) {
            var convertInventories=getSelectedConvertInventories(convertProduct,productBrand,unitOfMeasure,stock,businessUnit);
            convertInventories.inventories=getSelectedSourceConversionInventories(sourceProducts);
            convertInventories.convertType=convertType;
            return $http.post('/inventories/createInventoryConversion',convertInventories);

        },
        getConvertInventory:function (convertInventory,businessUnit) {
            /* inventoriesId(products,function (products) {*/
            return $http.post('/inventories/matchedinventories', {inventories: [convertInventory],businessUnit:businessUnit, issource: false});
            /* });*/
        }

    };

}]);
app.factory('csvFileParse',function () {
    function headerSubCategoryIndex(field) {
        return  field === 'SubCategory1';
    }
    function headerCategoryIndex(field) {
        return  field === 'Category';
    }
    function getMappedJsonFields(result,done){
        //{header: [], data: [], error: [], status: true};
        var csvfields = result.data;
        var header = result.header;
        var map = {};
        for(var i=0;i<csvfields.length;i++){
            var rowData = csvfields[i].lineData;
            var subcategory = rowData[header.findIndex(headerSubCategoryIndex)];
            var category = rowData[header.findIndex(headerCategoryIndex)];
            if(!map[category]){
                map[category]=[];
            }
            if(!map[category][subcategory]){
                map[category][subcategory]=[];
            }
            map[category][subcategory].push(csvfields[i]);
        }
        done({header:header,map:map,error:result.error});

    }
    function cleanEachField(value) {

        return value && value
            .replace(/^\s*|\s*$/g,'') // remove leading & trailing space
            .replace(/^"|"$/g,'') // remove " on the beginning and end
            .replace(/\r?\n|\r/g,'').trim();
    }
    function isEmptyRowCount(rows) {
        if(rows && angular.isArray(rows)){
            return rows.filter(function (eachValue) {
                return cleanEachField(eachValue)!=='';
            }).length>0?true:false;
        }else{
            return false;
        }
    }

    function headerCleanData(headers,done) {
        if(headers && angular.isArray(headers)){
            headers.forEach(function (eachHeader) {
                headers[headers.indexOf(eachHeader)] = cleanEachField(eachHeader);
            });
            return headers;
        }else{
            return null;
        }
    }
    function productHeaderFields(headers,defaultHeader,done) {
        var mappedHeader=headerCleanData(headers);
        if(mappedHeader && angular.isArray(mappedHeader)){
            done('',mappedHeader);
        }else{
            var headerMissedLength=defaultHeader.filter(function(element) {
                return mappedHeader.indexOf(element) === -1;
            });
            if(headerMissedLength.length>0){
                done(headerMissedLength,mappedHeader);
            }else{
                done(null,mappedHeader);
            }

        }


    }

    /*function eachRequiredDataField(headerConst,fieldValue) {
        headerConst.filter(function (eachMatched) {

        });
    }*/
    // data mapping with the header
    function headerMatchedData(eachLine,headers,requiredFieldsHeader,rowData,errors,done) {
        angular.forEach(headers, function (eachHeader, j) {
            var obj = {};
            obj.name = cleanEachField(eachHeader);
            obj.value = cleanEachField(eachLine[j]);

            if (requiredFieldsHeader.indexOf(headers[j]) >= 0 && obj.value === '') {
                if (errors === '') {
                    errors = 'Column ' + eachHeader + ': The mandatory column ' + eachHeader + ' value is empty';
                } else {
                    errors = errors + '<br/>Column ' + headers[j] + ':The mandatory column ' + eachHeader + ' value is empty';
                }
            }
            if (errors === '') {
                rowData.push(obj.value);
            }
        });

        done(errors,rowData);


    }
    function dataMachRecords(lines,headers,headerConst,separator,result,done) {
        if(lines && angular.isArray(lines)) {
            angular.forEach(lines, function (eachLine, k) {
                if(k>0) {
                    var rowData = [];
                    var errors = '';
                    var currentline = eachLine.split(new RegExp(separator + '(?![^"]*"(?:(?:[^"]*"){2})*[^"]*$)'));

                    if (isEmptyRowCount(currentline)) {
                        if (currentline.length === headers.length) {
                            headerMatchedData(currentline, headers,headerConst, rowData, errors, function (finaErrors, finalRowData) {
                                if (finaErrors === '') {
                                    result.data.push({srcLine: k, lineData: finalRowData});
                                } else {
                                    rowData.push(finaErrors);
                                    result.error.push({srcLine: k, lineData: finalRowData});
                                    result.status = false;
                                }

                            });


                        } else {
                            errors = 'This row column data does not match with header column data';
                            rowData.push(errors);
                            result.error.push({srcLine: k, lineData: rowData});
                            result.status = false;
                        }
                    }
                }

            });
            done(null,result);
        }else{
            done(null,result);
        }
    }
    return {
        csvToJSON : function(content,importType,done) {
//            var lines=content.csv.split(new RegExp('\n(?![^"]*"(?:(?:[^"]*"){2})*[^"]*$)'));
            var lines = content.lines;
            var result = {header: [], data: [], error: [], status: true};
            var start = 0;
            var columnCount = lines[0].split(content.separator).length;

            /* var headerConst = ['Category','SubCategory1','ProductName','ProductCode','BrandName','HSNCode','UOM.FirstUOM','UOM.Conversion','UOM.SecondUOM'];*/

            var headers = [];
            var errors='';
            if (content.header) {
                headers=lines[0].split(content.separator);
                start = 1;
                var headerConst = ['Category','SubCategory1','ProductName','ProductCode','BrandName','HSNCode','UOM.FirstUOM','UOM.Conversion','UOM.SecondUOM'];
                productHeaderFields(headers,headerConst,function (err,finalMappedHeader) {
                    result.header = finalMappedHeader;

                    if(err) {
                        /* errors = 'All mandatory columns required for importing the CSV file is not present.  ' +
                             'Following fields are missing or are not in the correct position:' + headerMissing+
                             'The mandatory columns are : Category, SubCategory1, ProductName, ProductCode, BrandName, ' +
                             'HSNCode, TaxGroup, UOM.FirstUOM, UOM.Conversion and UOM.SecondUOM';*/
                        var headerObj = {};
                        headerObj.srcLine = 0;
                        headerObj.rowError = err;
                        result.error.push(headerObj);
                        result.errorType=1;
                        result.status = false;
                        done(result);
                    }else {

                        dataMachRecords(lines,headers,headerConst,content.separator,result,function (err,data) {
                            getMappedJsonFields(data,function (finalData) {
                                done(finalData);
                            });
                        });

                    }
                });

            }


        },
        cleanCsvValue : function(value) {
            return cleanEachField(value);
        },
        csvHeader: function(strData, strDelimiter) {},
        isRowBlank:function(rows){
            return isEmptyRowCount(rows);
        }


    };
});
app.factory('xlsFileParse',function () {
    return {
        xlsToJSON : function(content) {
            var lines=content.csv.split(new RegExp('\n(?![^"]*"(?:(?:[^"]*"){2})*[^"]*$)'));
            var result = [];
            var start = 0;
            var columnCount = lines[0].split(content.separator).length;

            var headers = [];
            if (content.header) {
                headers=lines[0].split(content.separator);
                start = 1;
            }

            for (var i=start; i<lines.length; i++) {
                var obj = {};
                var currentline=lines[i].split(new RegExp(content.separator+'(?![^"]*"(?:(?:[^"]*"){2})*[^"]*$)'));
                if ( currentline.length === columnCount ) {
                    if (content.header) {
                        for (var j=0; j<headers.length; j++) {
                            obj[headers[j]] = this.cleanCsvValue(currentline[j]);
                        }
                    } else {
                        for (var k=0; k<currentline.length; k++) {
                            obj[k] = this.cleanCsvValue(currentline[k]);
                        }
                    }
                    result.push(obj);
                }
            }
            return result;
        },
        cleanCsvValue : function(value) {
            return value
                .replace(/^\s*|\s*$/g,'') // remove leading & trailing space
                .replace(/^"|"$/g,''); // remove " on the beginning and end
            /* .replace(/""/g,'');*/ // replace "" with "
        },
        csvHeader: function(strData, strDelimiter) {}
    };
});

app.factory('productDetail',function ($mdDialog,$mdMedia,Offers,Orders,Products,Inventories) {
    return function(singleObjet,templateUrl,scope) {
        if(singleObjet instanceof Offers) {
            templateUrl='modules/orders/views/order-status-dialog.html';
        }
        return $mdDialog.show({
            templateUrl: templateUrl,
            scope: scope,
            clickOutsideToClose: true,
            fullscreen: $mdMedia('sm') || $mdMedia('xs'),
            preserveScope: true
        });
    };
});
function isNumber(n){
    return !isNaN(parseFloat(n)) && isFinite(n);
}
app.directive('nvPrice', function($parse,$filter){
    return {
        restrict: 'A',
        link: function(scope, element, attrs){
            function validate(){
                if(element && element[0] && element[0].localName === 'input' && attrs.hasOwnProperty('ngModel') && attrs.hasOwnProperty('nvPrice') && attrs.hasOwnProperty('nvSale')){
                    if(attrs.nvSale==='true')
                        $parse(attrs.ngModel).assign(scope, parseFloat((scope.inventory.moqAndSalePrice[attrs.nvIndex].margin!==undefined?attrs.nvPrice*(1-(scope.inventory.moqAndSalePrice[attrs.nvIndex].margin/100)):(attrs.nvPrice?attrs.nvPrice:0))).toFixed(2));
                    else if(attrs.nvSale==='false')
                        $parse(attrs.ngModel).assign(scope, parseFloat((scope.inventory.moqAndBuyPrice[attrs.nvIndex].margin!==undefined?attrs.nvPrice*(1-(scope.inventory.moqAndBuyPrice[attrs.nvIndex].margin/100)):(attrs.nvPrice?attrs.nvPrice:0))).toFixed(2));
                }
            }
            scope.$watch(function(){
                return attrs.nvPrice + '-' + element[0].value;
            }, function(newVal, oldVal){
                if(newVal !== oldVal)
                    validate();
            });
            validate();
        }
    };
});

app.factory('criteriaChips',['contactsQuery','categoryService',function (contactsQuery,categoryService) {

    /**
     * Return the proper object when the append is called.
     */
    function transformChip(chip) {
        // If it is an object, it's already a known chip
        if (angular.isObject(chip) && !chip.disabled) {
            chip.isChecked = true;
            return chip;
        } else {
            return null;
        }
    }
    /**
     * Create filter function for a query string
     */
    function createFilterFn(query,isContact) {
        var lowercaseQuery = angular.lowercase(query);

        /*return function filterFn(category) {
            return (category._lowername && category._lowername.indexOf(lowercaseQuery) === 0) ||
                (category._lowertype && category._lowertype.indexOf(lowercaseQuery) === 0);
        };*/
        return function filterFn(category) {
            if(isContact) {
                return ((category._lowername &&(category._lowername.indexOf(lowercaseQuery)  >= 0)) ||   category._lowerfirstname && category._lowerfirstname.indexOf(lowercaseQuery)  >= 0) ||
                    (category._lowerlastname && category._lowerlastname.indexOf(lowercaseQuery) >= 0) ||
                    (category._lowerdisplayName && category._lowerdisplayName.indexOf(lowercaseQuery) >= 0) ||
                    (category.emails && category.emails.filter(function(email){ return email.email.toLowerCase().indexOf(lowercaseQuery) >= 0;}).length>0)||
                    (category.phones && category.phones.filter(function(phone){ return phone.phoneNumber.toLowerCase().indexOf(lowercaseQuery) >= 0;}).length>0);
            }else{
                return (category._lowername && category._lowername.indexOf(lowercaseQuery) >= 0) ||
                    (category._lowertype && category._lowertype.indexOf(lowercaseQuery) >= 0) ||
                    (category._lowertype && category._lowertype.indexOf(lowercaseQuery) >= 0) ;
            }
        };


    }

    function contactMap(contacts,selectedContacts) {

        contacts.filter(function (contact) {
            if(contact.displayName && (!contact.nVipaniRegContact || contact.nVipaniRegContact!==true)) {
                if(contact.firstName)
                    contact._lowerfirstname = contact.firstName.toLowerCase();
                if(contact.lastName)
                    contact._lowerlastname = contact.lastName.toLowerCase();
                if(contact.displayName)
                    contact._lowerdisplayName = contact.displayName.toLowerCase();
                selectedContacts.push(contact);
            }else{
                if(contact.name) {
                    contact._lowername = contact.name.toLowerCase();
                    selectedContacts.push(contact);
                }
            }
        });
    }

    function childCategory(categories,selcategoires) {

        categories.filter(function (category) {
            if (category.type === 'SubCategory2') {
                category._lowername = category.name.toLowerCase();
                category._lowertype = category.type.toLowerCase();
                selcategoires.push(category);
            } else {
                category._lowername = category.name.toLowerCase();
                category._lowertype = category.type.toLowerCase();
                selcategoires.push(category);
                childCategory(category.children,selcategoires);

            }
        });
        return selcategoires;
    }


    return {
        /**
         * Return the proper object when the append is called.
         */
        transformChip:function(chip) {
            // If it is an object, it's already a known chip
            return transformChip(chip);
        },
        notFound:function(key) {
            /*console.log("key", key);*/
            // transformChip(key);
        },

        /**
         * Search for Contacts.
         */
        querySearchContacts: function(selectedCat,query) {
            var results = query ? selectedCat.filter(createFilterFn(query,true)) : [];
            return results;
        },

        /**
         * Create filter function for a query string
         */
        createFilterFor:function(query,isContact) {
            return createFilterFn(query,isContact);

        },
        /**
         * Load Contacts and groups for Search
         * @param $scope
         * @param done
         */
        loadChipContacts: function($scope,isOnlyContacts,isCustomer,isSpecific,selectedContact,done){
            if($scope.contactsVsGroup && $scope.contactsVsGroup.length===0) {
                contactsQuery.findContactsVsGroups($scope, isOnlyContacts, isCustomer, isSpecific, function (contactsVsGroup) {
                    $scope.contactsVsGroup = contactsVsGroup;
                    if ($scope.contactsVsGroup)
                        contactMap($scope.contactsVsGroup, selectedContact);
                    /*$scope.querySearchContacts = $scope.querySearchContacts;*/
                    $scope.selectedContactItem = null;
                    $scope.selectedContacts = [];
                    $scope.numberChips = [];
                    $scope.numberChips2 = [];
                    $scope.filterSelected = true;
                    $scope.numberBuffer = '';
                    $scope.autocompleteDemoRequireMatch = false;

                });
            }

        },
        /**
         * Search for Categories.
         */
        querySearchCategory:function(selectedCat,query) {
            var results = query ? selectedCat.filter(createFilterFn(query,false)) : [];
            return results;
        },
        loadCategories:function($scope) {
            categoryService.findMainCategoriesWithSearch().then(function (response) {
                $scope.mainCategories = response.data;
                $scope.selectedCat=[];
                childCategory($scope.mainCategories,$scope.selectedCat);
                $scope.transformChip = transformChip;

            }, function (res) {
                $scope.error = res.data.message;
            });
            return $scope;
        }
    };
}]);
app.directive('searchFields', function ($compile) {
    return {
        restrict: 'A',
        scope: {
            searchFields: '=',
            searchQuery:'=',
            searchId:'='
        },
        link: function($scope, elem, attr, ctrl) {
            if(attr.field==='offer'){
                $scope.searchText ={} ;
                $scope.queryOptions=[{name:'Search By',value:'$'},{name:'Name',value:'name'},{name:'Offer Number',value:'offerNumber'},{name:'Status',value:'offerStatus'}];
                /* $scope.searchQuery ='name' ;*/

            }else if(attr.field==='order'){
                $scope.searchText ={} ;
                $scope.queryOptions=[{name:'Search By',value:'$'},{name:'Type',value:'orderType'},{name:'Order Number',value:'orderNumber'},{name:'Status',value:'currentStatus'}];
                /* $scope.searchQuery ='name' ;*/

            }
            var setval='test_'+attr.id;
            var htmlText = '<div layout="row" layout-xs="row"  layout-sm="row" layout-wrap layout-align="start center"><div flex="40" flex-xs="50" flex-sm="50"><div class="nvc-input-btngrp nv-position-relative">\n' +
                '                        <input placeholder="Search..." type="text" ng-model="searchFields">\n' +
                '                <span><button class="nvc-input-button" type="button" ng-click="find()">\n' +
                '               <i class="fa fa-search"></i>\n' +
                '            </button>\n' +
                '            </span>\n' +
                /*<span class="input-group-btn">\n' +
                '                                <button class="btn bb-btn-search" type="button">\n' +
                '                                    <i class="fa fa-search"></i>\n' +
                '                                </button>\n' +
                '                            </span>\n' +*/
                '                    </div></div><div flex="20" flex-xs="40" flex-sm="30" class="nvc-margin-left-16">\n' +
                '<md-input-container><md-select ng-model="searchQuery" aria-label="search-by" ><md-option ng-repeat="item in queryOptions" value="{{item.value}}">{{item.name}}</md-option></md-select> </md-input-container></div></div>';
            var linkFn = $compile(htmlText);
            elem.append(linkFn($scope));

        }
    };
});
app.directive('editProduct', function(){
    return {
        restrict:'E',
        templateUrl:'modules/products/views/edit-product.client.viewnew.html'
    };
});
app.directive('selectCategory', function(){
    return {
        restrict:'E',
        templateUrl:'modules/categories/views/category-product.client.viewnew.html'
    };
});
app.directive('addProduct', function(){
    return {
        restrict:'E',
        templateUrl:'modules/products/views/create-productc.client.viewnew.html'
    };
});
app.directive('createProductCheckout', function () {
    return {
        restrict: 'E',
        templateUrl: 'modules/categories/views/create-product-checkout.client.view.html'
    };
});
app.directive('createCategory', function(){
    return {
        restrict:'E',
        templateUrl:'modules/categories/views/create-category.client.view.html'
    };
});
app.directive('createProductBrand', function(){
    return {
        restrict:'E',
        templateUrl:'modules/categories/views/create-product-brand.client.view.html'
    };
});
app.directive('createUom', function(){
    return {
        restrict:'E',
        templateUrl:'modules/categories/views/create-uom.client.view.html'
    };
});
app.directive('createTax', function(){
    return {
        restrict:'E',
        templateUrl:'modules/categories/views/create-taxgroup.client.view.html'
    };
});
app.directive('addOfferProduct', function(){
    return {
        restrict:'E',
        templateUrl:'modules/products/views/create-product-offer.client.viewnew.html'
    };
});
app.directive('addOrderProduct', function(){
    return {
        restrict:'E',
        templateUrl:'modules/products/views/create-product-order.client.viewnew.html'
    };
});

app.directive('viewProduct', function(){
    return {
        restrict:'E',
        templateUrl:'modules/products/views/list-products.client.viewnew.html'
    };
});
app.directive('addHsncodeProduct', function(){
    return {
        restrict:'E',
        templateUrl:'modules/products/views/create-hsncode.client.view.html'
    };
});
app.directive('createHsncodesProduct', function(){
    return {
        restrict:'E',
        templateUrl:'modules/categories/views/create-hsncodes.client.view.html'
    };
});
app.directive('inventoryFilter',function () {
    return {
        restrict:'E',
        templateUrl:'modules/inventories/views/inventories.filter.client.view.html'
    };
});

app.service('createOfferService', function() {
    var myList = [];

    var addToSelectedList = function(newObj) {
        myList.push(newObj);
    };

    var getSelectedList = function(){
        return myList;
    };
    return {
        addSelectedList: addToSelectedList,
        getSelectedList: getSelectedList,
    };

});
