'use strict';

//Products service used to communicate Products REST endpoints
angular.module('products').factory('Products', ['$resource',
	function($resource) {
		return $resource('products/:productId', { productId: '@_id'
		}, {
			update: {
				method: 'PUT'
			}
		});
	}
])
	.factory('productService',['$stateParams', '$http',function($stateParams,$http){
    return {
        findBrandsByProductId:function (id) {
            return $http.get('queryProductBrands/'+id);
        }
    };
}]);
/*app.factory('ProductService',function(student){
    var school = {};
    school.students = [];
    school.addStudent = function(){
        var newStudent = new student();
        this.students.push(newStudent);
    };
    return school;
});
app.service('product',function(){
    //anyway to toss in a constructor here?
    this.name = 'name';
    this.description =
    this.getName = function(){
        return this.name;
    };
});*/
