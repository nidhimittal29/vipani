'use strict';

// Products controller
angular.module('products').controller('ProductsController', ['$scope', '$stateParams', '$location', 'Authentication', '$http', '$timeout', '$window', 'FileUploader', 'Products', 'Categories',
	function ($scope, $stateParams, $location, Authentication, $http, $timeout, $window, FileUploader, Products, Categories) {
		$scope.authentication = Authentication;
		$scope.token = Authentication.token;
        $scope.unitMeasureTypes = ['Kg', 'gram', 'litre', 'ml', 'count', 'tonne', 'Bag', 'Box', 'Carton', 'Bale', 'Candy', 'Bundle', 'Case', 'metre', 'dozen', 'inch', 'foot', 'Pack', 'Tray', 'Other'];
		$scope.categories = [];
		$scope.mainCategories = [];
		$scope.subCategories1 = [];
		$scope.subCategories2 = [];
		$scope.subCategories3 = [];
		$scope.imageURL1 = 'modules/products/img/profile/default.png';
		$scope.keyWords = [];
		// Create new Product
		$scope.create = function() {
			// Create new Product object
			var product = new Products ({
				name: this.name,
				category: this.category,
				subCategory1: this.subCategory1,
				subCategory2: this.subCategory2,
				unitSize: this.unitSize,
				unitMeasure: this.unitMeasure,
				unitPrice: this.unitPrice,
				numberOfUnits:this.numberOfUnits,
				description: this.description,
				fssaiLicenseNumber: this.fssaiLicenseNumber,
				fssaiLicenseNumberProof: this.fssaiLicenseNumberProof,
				keywords:this.keyWords,
				productImageURL1:this.imageURL1
			});
			// Redirect after save
			product.$save(function(response) {
				$location.path('settings/editmybusiness');

				// Clear form fields
				$scope.clearFields();
			}, function(errorResponse) {
				$scope.error = errorResponse.data.message;
			});
		};

		//Clear Fields Function
		$scope.clearFields = function () {
			$scope.name = '';
			$scope.category = '';
			$scope.subCategory1 = '';
			$scope.subCategory2 = '';
			$scope.unitSize = '';
			$scope.unitMeasure = '';
			$scope.unitPrice= '';
			$scope.numberOfUnits= '';
			$scope.description = '';
			$scope.fssaiLicenseNumber='';
			$scope.fssaiLicenseNumberProof='';
			$scope.keywords = [];
			$scope.imageURL1 = '';
		};

		$scope.panelChange = function () {
			$location.path('settings/editmybusiness');

			// Clear form fields
			$scope.clearFields();
		};


		// Remove existing Product
		$scope.remove = function(product) {
			if ( product ) {
				product.$remove();

				for (var i in $scope.products) {
					if ($scope.products [i] === product) {
						$scope.products.splice(i, 1);
					}
				}
			} else {
				$scope.product.$remove(function() {
					$location.path('products');
				});
			}
		};

		// Update existing Product
		$scope.update = function() {
			$scope.product.productImageURL1 = $scope.imageURL1;
			var product = $scope.product;
			product.$update(function() {
				$location.path('settings/editmybusiness');
			}, function(errorResponse) {
				$scope.error = errorResponse.data.message;
			});
		};

		$scope.changeCategory = function() {
			if($scope.category) {
				angular.forEach($scope.mainCategories, function (e) {
					if ($scope.category === e._id)
						$scope.subCategories1 = e.children;
				});
			}
		};


		$scope.changeSubCategory1 = function () {
			if ($scope.subCategory1) {
				angular.forEach($scope.subCategories1, function (e) {
					if ($scope.subCategory1 === e._id){
						var subCat1 = Categories.get({
							categoryId: e._id
						});
						subCat1.$promise.then(function (result) {
							$scope.subCategories2 = result.children;
						});
					}

				});
			}
		};


		$scope.reset = function (form) {
			// save the profile picture
			$scope.uploadProfilePicture();
			$scope.error = null;
			if (form) {
				form.$setPristine();
				//form.$setUntouched();
			}

		};

		// Find a list of Products
		$scope.findMainCategories = function () {
            $http.get('queryMainCategories').then(function (response) {
                $scope.mainCategories = response.data;
			});
		};
		// Find a list of Products
		$scope.find = function() {
			$scope.products = Products.query();
		};

		// Find existing Product
		$scope.findOne = function() {
			$scope.product = Products.get({ 
				productId: $stateParams.productId
			});
			$scope.product.$promise.then(function (result) {
				$scope.imageURL1 = result.productImageURL1;
				if (result.category) {
					//var children = $scope.category.children;
					angular.forEach($scope.mainCategories, function (e) {
						if (result.category === e._id) {
							$scope.subCategories1 = e.children;
						}
					});
				}
			});
		};

		// Create file uploader instance
		$scope.uploader = new FileUploader({
			headers: {'token': $scope.token},
			url: 'products/productPicture'
		});

		// Set file uploader image filter
		$scope.uploader.filters.push({
			name: 'imageFilter',
			fn: function (item, options) {
				var type = '|' + item.type.slice(item.type.lastIndexOf('/') + 1) + '|';
				return '|jpg|png|jpeg|bmp|gif|'.indexOf(type) !== -1;
			}
		});

		// Called after the user selected a new picture file
		$scope.uploader.onAfterAddingFile = function (fileItem) {
			if ($window.FileReader) {
				var fileReader = new FileReader();
				fileReader.readAsDataURL(fileItem._file);

				fileReader.onload = function (fileReaderEvent) {
					$timeout(function () {
						$scope.imageURL1 = fileReaderEvent.target.result;
					}, 0);
				};
			}
		};

		// Called after the user has successfully uploaded a new picture
		$scope.uploader.onSuccessItem = function (fileItem, response, status, headers) {
			// Show success message
			$scope.imgsuccess = true;
			var imageURLStr =response.imageURL;
			$scope.imageURL1 = imageURLStr.replace (/"/g,'');
			// Clear upload buttons
			$scope.cancelUpload();
		};

		// Called after the user has failed to uploaded a new picture
		$scope.uploader.onErrorItem = function (fileItem, response, status, headers) {
			// Clear upload buttons
			$scope.cancelUpload();

			// Show error message
			$scope.imgerror = response.message;
		};

		// Change user profile picture
		$scope.uploadProfilePicture = function () {
			// Clear messages
			$scope.imgsuccess = $scope.imgerror = null;

			// Start upload
			$scope.uploader.uploadAll();
		};

		// Cancel the upload process
		$scope.cancelUpload = function () {
			$scope.uploader.clearQueue();
		};

		// Keywords
		$scope.keyWord = null;
		$scope.addKey = function(){
			var index = $scope.keyWords.indexOf($scope.keyWord);
			if(index === -1) // if keyWord doesn't already exist, push it
				$scope.keyWords.push($scope.keyWord);
			$scope.keyWord = null;
		};

		$scope.removeKey = function(key){
			var index = $scope.keyWords.indexOf(key);
			if(index !== -1) // if keyWord exists then delete it
				$scope.keyWords.splice(index, 1);
		};
		$scope.setEditBasedOnUserRole=function (inventory) {
			if($scope.authentication.companyBusinessUnits.userGroup.name === 'Employee'){
				for(var i=0;i<inventory.moqAndMargin.length;i++){
					angular.element(document.querySelector('#saleMoq-'+i)).attr('readonly','readonly');
					angular.element(document.querySelector('#saleMoqMargin-'+i)).attr('readonly','readonly');
                    angular.element(document.querySelector('#saleMov-'+i)).attr('readonly','readonly');
                    angular.element(document.querySelector('#saleMovMargin-'+i)).attr('readonly','readonly');
				}
				for(var j=0;j<inventory.moqAndBuyMargin.length;j++){
                    angular.element(document.querySelector('#buyMoq-'+i)).attr('readonly','readonly');
                    angular.element(document.querySelector('#buyMoqMargin-'+i)).attr('readonly','readonly');
                    angular.element(document.querySelector('#buyMov-'+i)).attr('readonly','readonly');
                    angular.element(document.querySelector('#buyMovMargin-'+i)).attr('readonly','readonly');
				}
			}else {
                for(var a=0;a<inventory.moqAndMargin.length;a++){
                    angular.element(document.querySelector('#saleMoq-'+a)).removeAttr('readonly');
                    angular.element(document.querySelector('#saleMoqMargin-'+a)).removeAttr('readonly' );
                    angular.element(document.querySelector('#saleMov-'+a)).removeAttr('readonly');
                    angular.element(document.querySelector('#saleMovMargin-'+a)).removeAttr('readonly');
                }
                for(var b=0;b<inventory.moqAndBuyMargin.length;b++){
                    angular.element(document.querySelector('#buyMoq-'+b)).removeAttr('readonly');
                    angular.element(document.querySelector('#buyMoqMargin-'+b)).removeAttr('readonly');
                    angular.element(document.querySelector('#buyMov-'+b)).removeAttr('readonly');
                    angular.element(document.querySelector('#buyMovMargin-'+b)).removeAttr('readonly');
                }
			}
        };
	}
]);

