'use strict';

// Configuring the Articles module
angular.module('products',[]).run(['Menus',
	function(Menus) {
		// Set top bar menu items
		Menus.addMenuItem('topbar', 'Products', 'products', 'products',false,false, null, 1);
		/*Menus.addSubMenuItem('topbar', 'products', 'List Products', 'products');
		Menus.addSubMenuItem('topbar', 'products', 'New Product', 'products/create');*/
	}
]);
