'use strict';

//Orders service used to communicate Orders REST endpoints
var app=angular.module('orders');
app.factory('Orders', ['$resource',
	function($resource) {
		return $resource('orders/:orderId', { orderId: '@_id'
		}, {
			update: {
				method: 'PUT'
			}
		});
	}
]);
app.factory('OrdersExternal',['$stateParams', '$http',function($stateParams,$http){
    return {
        generateInvoice:function (orderId) {
			return  $http.get('invoiceOrder/' + orderId, {responseType: 'arraybuffer'});
        },
        getEnums:function (orderId) {
            return  $http.get('ordersEnums');
        }

    };
}]);
app.factory('OrderContacts',[function(){
    return {
        getValidContactSelection: function (orderContact,isSeller,scope) {
            return (((!scope.showSellerChange && isSeller )|| (!isSeller && !scope.showBuyerChange )) && (!orderContact ||  (orderContact && orderContact.contact&& orderContact.contact._id && this.getContact(orderContact.contact._id,scope).length===0)));

        },
        getContact:function(contactId,scope){
            if(scope.selfcontact[0] && scope.selfcontact[0]._id===contactId){
                return scope.selfcontact;
            }else {
                return scope.contacts.filter(function (item) {
                    return item._id === contactId;
                });
            }
        },
        getBusinessUnit:function(bunitId,scope){
           /* if(scope.selfcontact[0] && scope.selfcontact[0]._id===bunitId){
                return scope.selfcontact;
            }else {*/
                return scope.companyBusinessUnits.filter(function (item) {
                    return item && item._id === bunitId;
                });
            /*}*/
        },
        getValidBusinessUnitSection:function (orderBusinessUnit,isSeller,scope) {
            return (((!scope.showSellerChange && isSeller )|| (!isSeller && !scope.showBuyerChange )) && (!orderBusinessUnit ||  (orderBusinessUnit && orderBusinessUnit.businessUnit&& scope.companyBusinessUnits&& scope.companyBusinessUnits.length>0 && orderBusinessUnit.businessUnit._id && this.getBusinessUnit(
                orderBusinessUnit.businessUnit._id,scope).length===0)));


        },
        setShowSellerChange:function(scope){
            if(scope.selOrder){
                return scope.selOrder.currentStatus === 'Drafted' && scope.selOrder.orderType === 'Purchase' && (!scope.selOrder.offer || (scope.selOrder.offer && scope.selOrder.offer.offerType === 'Buy'));

            }else {
                return false;
            }

        },
        setShowBuyerChange:function(scope){
            if(scope.selOrder){
                return scope.selOrder.currentStatus === 'Drafted' && scope.selOrder.orderType === 'Sale' && (!scope.selOrder.offer || (scope.selOrder.offer && scope.selOrder.offer.offerType === 'Sell'));

            }else {
                return false;
            }

        }

    };
}]);
app.factory('OrderProducts',[function(){
    return {
        getValidProductsSelection:function (products,scope) {
            return (products===undefined || products && products !==undefined &&products.length===0 || products.filter(function (product) {
                return product.inventory.disabled  || !product.numberOfUnits || product.numberOfUnits===0 ||  product.unitPrice===0 || product.numberOfUnits<product.MOQ;
            }).length>0 || isNaN(scope.selOrder && scope.selOrder.totalAmount));

        },
        getShowSalePriceOnly:function (scope,isOrder) {
            if(isOrder)
            return ((scope.selOrder.orderType==='Sale' &&  scope.selOrder.user._id!==scope.authentication.user._id) || (scope.selOrder.orderType==='Purchase' && scope.selOrder.user._id===scope.authentication.user._id));
            else  return ((scope.selOffer.offerType==='Sell' &&  scope.selOffer.user._id!==scope.authentication.user._id));
        },
        getShowBuyPriceOnly:function (scope,isOrder) {
            if(isOrder) return ((scope.selOrder.orderType==='Purchase' &&  scope.selOrder.user._id!==scope.authentication.user._id) || ( scope.selOrder.orderType==='Sale' && scope.selOrder.user._id===scope.authentication.user._id));
            else return (scope.selOffer.offerType==='Buy' && scope.selOffer.user._id!==scope.authentication.user._id);
        },
        getShowBothPriceOnly:function (scope,isOrder) {
            if(isOrder) return ((scope.selOrder.orderType==='Sale' &&  scope.selOrder.user._id===scope.authentication.user._id) || (scope.selOrder.orderType==='Purchase' && scope.selOrder.user._id!==scope.authentication.user._id));
            else return (scope.selOffer.user._id===scope.authentication.user._id);
        }
    };
}]);
app.factory('OrderPayments',['$http',function($http){
    return {
       /* getSelectedPayments:function (order) {
            return  $http.get('invoiceOrder/' + orderId, {responseType: 'arraybuffer'});
        },*/
        setSelectedPayments:function (key,scope) {
            scope.selectedTag=false;
            scope.selectedPayMentOptions=angular.copy(scope.selOrder.paymentOptionType);

            if(key==='installments') {
                /*scope.selectedTag=true;*/
                scope.selOrder.selectedPayMentOptions=scope.selectedPayMentOptions.installments;
                /*scope.selOrder.selectedPayMentOptions=scope.selectedPayMentOptions.installments.options[scope.selectedIndex];*/
                var eachStatus=false;
                for(var i=0;i<scope.selectedPayMentOptions.installments.options.length;i++){
                    scope.selOrder.selectedPayMentOptions.options[i].amount=((scope.selOrder.totalAmount * scope.selectedPayMentOptions.installments.options[i].percentage) /100).toFixed('2');
                    if(!eachStatus && (scope.selectedPayMentOptions.installments.options[i].status==='YetToPay' || scope.selectedPayMentOptions.installments.options[i].status==='OverDue') ){
                        scope.selectedPayMentOptions.installments.options[i].statusval=true;
                        eachStatus=true;
                    }else{
                        scope.selectedPayMentOptions.installments.options[i].statusval=false;
                    }

                }


                scope.selOrder.selectedPayMentOptions.key='installments';
                scope.selOrder.selectedPayMentOptions.selection=true;

            }else if(key==='payNow'){
                scope.selOrder.selectedPayMentOptions=scope.selectedPayMentOptions.payNow;
                scope.selOrder.selectedPayMentOptions.key='payNow';
                scope.selOrder.selectedPayMentOptions.selection=true;
                scope.selOrder.selectedPayMentOptions.amount=this.paymentDiscounts(scope.selOrder);
                scope.selOrder.paidAmount=scope.selOrder.selectedPayMentOptions.amount;
                /*scope.selectedPayMentOptions.installments.selection=false;
                 scope.selectedPayMentOptions.payOnDelivery.selection=false;
                 scope.selectedPayMentOptions.lineOfCredit.selection=false;*/

            }else if(key==='payOnDelivery'){
                scope.selOrder.selectedPayMentOptions=scope.selectedPayMentOptions.payOnDelivery;
                scope.selOrder.selectedPayMentOptions.selection=true;
                scope.selOrder.selectedPayMentOptions.amount=this.paymentDiscounts(scope.selOrder);
                /*scope.selectedPayMentOptions.payNow.selection=false;
                 scope.selectedPayMentOptions.installments.selection=false;
                 scope.selectedPayMentOptions.lineOfCredit.selection=false;*/

            }else if(key==='lineOfCredit') {
                scope.selOrder.selectedPayMentOptions=scope.selectedPayMentOptions.lineOfCredit;
                scope.selOrder.selectedPayMentOptions.amount=scope.selOrder.totalAmount;
                scope.selOrder.selectedPayMentOptions.selection = true;
                /*scope.selectedPayMentOptions.payNow.selection=false;
                 scope.selectedPayMentOptions.installments.selection=false;
                 scope.selectedPayMentOptions.payOnDelivery.selection=false;*/
            }
        },
        paymentDiscounts:function (order) {
            var totalAmount=0;
            if(order && order.selectedPayMentOptions && order.selectedPayMentOptions.discount===0){
                return order.totalAmount.toFixed('2');
            }else if(order && order.selectedPayMentOptions && order.selectedPayMentOptions.discount)
                return order.totalAmount *(1-order.selectedPayMentOptions.discount/100).toFixed('2');
            return totalAmount.toFixed('2');
        },
        getSelectedPaymentInstallOption:function (installaments) {
            if (installaments.options.length > 0) {
                var i = 0;
                for (i = 0; i < installaments.options.length; i++) {
                    if (!installaments.options.status || (installaments.options.status && installaments.options.status === 'YetToPay')) {
                        break;
                    }
                }
                return installaments.options[i];
            } else {
                return null;
            }
         },
        getSelectedPaymentOption:function (allpaymentOptions) {

           if (!allpaymentOptions) {
               return null;
           }
           if (allpaymentOptions.payNow && allpaymentOptions.payNow.selection) {
               allpaymentOptions.payNow.key = 'payNow';
               return allpaymentOptions.payNow;
           } else if (allpaymentOptions.payOnDelivery && allpaymentOptions.payOnDelivery.selection) {
               allpaymentOptions.payOnDelivery.key = 'payOnDelivery';
               return allpaymentOptions.payOnDelivery;
           } else if (allpaymentOptions.installments && allpaymentOptions.installments.selection) {
               allpaymentOptions.installments.key = 'installments';
               return allpaymentOptions.installments;
           } else if (allpaymentOptions.lineOfCredit && allpaymentOptions.lineOfCredit.selection) {
               allpaymentOptions.lineOfCredit.key = 'lineOfCredit';
               return allpaymentOptions.lineOfCredit;
           } else {
               return null;
           }

       },
        setSelectedPaymentType:function (order,isPaid,scope) {
                if( order.selectedPayMentOptions && order.selectedPayMentOptions.key && order.currentStatus!=='Drafted'){
                    order.paymentMode=this.paymentMode;

                    if( order.selectedPayMentOptions.key==='payNow'){
                        order.paymentOptionType.payNow= order.selectedPayMentOptions;
                        order.paymentOptionType.payNow.status='Paid';
                        if( order.paidAmount < order.totalAmount && order.selectedPayMentOptions.amount)
                            order.paidAmount= order.paidAmount+ order.selectedPayMentOptions.amount;
                        order.paymentComments=this.paymentComments;
                        order.paymentOptionType.payOnDelivery.selection=false;
                        order.paymentOptionType.lineOfCredit.selection=false;
                        order.paymentOptionType.installments.selection=false;
                    }else if(order.selectedPayMentOptions.key==='payOnDelivery'){
                        order.paymentOptionType.payOnDelivery= order.selectedPayMentOptions;
                        order.paymentOptionType.payNow.selection=false;
                        order.paymentOptionType.lineOfCredit.selection=false;
                        order.paymentOptionType.installments.selection=false;
                        if(isPaid){
                            order.paymentOptionType.payOnDelivery.status='Paid';
                        }
                    }else if( order.selectedPayMentOptions.key==='lineOfCredit'){
                        order.paymentOptionType.lineOfCredit= order.selectedPayMentOptions;
                        /*if(!scope.payment) {
                         order.paymentOptionType.lineOfCredit.status = 'Paid';
                         }*/
                        order.paymentOptionType.payOnDelivery.selection=false;
                        order.paymentOptionType.payNow.selection=false;
                        order.paymentOptionType.installments.selection=false;
                        if(isPaid){
                            order.paymentOptionType.lineOfCredit.status='Paid';
                        }
                    }else if( order.selectedPayMentOptions.key==='installments') {
                        order.paymentOptionType.installments.options[scope.selectedPaymentIndex]=scope.selectedOptions;
                        order.paymentOptionType.installments.options[scope.selectedPaymentIndex].status='Paid';
                        if( order.paidAmount < scope.saveorder.totalAmount)
                            order.paidAmount= order.paidAmount+scope.selectedOptions.amount;
                        order.paymentOptionType.installments.selection=true;
                        order.paymentOptionType.payOnDelivery.selection=false;
                        order.paymentOptionType.payNow.selection=false;
                        order.paymentOptionType.lineOfCredit.selection=false;
                    }
                }
                return order;
            },
        setSelectedPayMentOptions:function (scope) {
            scope.selOrder.paymentOptionType={
                'payNow':scope.selOrder.paymentOptionType.payNow,
                'payOnDelivery':scope.selOrder.paymentOptionType.payOnDelivery,
                'installments':scope.selOrder.paymentOptionType.installments,
                'lineOfCredit':scope.selOrder.paymentOptionType.lineOfCredit
            };
            if(scope.selOrder.paymentOptionType.payNow.selection){
                scope.selOrder.selectedPayMentOptions=scope.selOrder.paymentOptionType.payNow;
                scope.selOrder.selectedPayMentOptions.key='payNow';
            }else if(scope.selOrder.paymentOptionType.payOnDelivery.selection){
                scope.selOrder.selectedPayMentOptions=scope.selOrder.paymentOptionType.payOnDelivery;
                scope.selOrder.selectedPayMentOptions.key='payOnDelivery';
            }else if(scope.selOrder.paymentOptionType.lineOfCredit.selection){
                scope.selOrder.selectedPayMentOptions=scope.selOrder.paymentOptionType.lineOfCredit;
                scope.selOrder.selectedPayMentOptions.key='lineOfCredit';
            }else if(scope.selOrder.paymentOptionType.installments.selection) {
                scope.selOrder.selectedPayMentOptions=scope.selOrder.paymentOptionType.installments;
                scope.selOrder.selectedPayMentOptions.key='installments';
                var eachStatus=false;
                for(var i=0;i<scope.selOrder.selectedPayMentOptions.options.length;i++){
                    scope.selOrder.selectedPayMentOptions.options[i].amount=((scope.selOrder.totalAmount * scope.selOrder.selectedPayMentOptions.options[i].percentage) /100).toFixed('2');
                    if(!eachStatus && (scope.selOrder.selectedPayMentOptions.options[i].status==='YetToPay' || scope.selOrder.selectedPayMentOptions.options[i].status==='Overdue') ){
                        scope.selOrder.selectedPayMentOptions.options[i].statusval=true;
                        eachStatus=true;
                    }else{
                        scope.selOrder.selectedPayMentOptions.options[i].statusval=false;
                    }

                }
            }

        },
        getOrderPaymentTerms:function (orderId) {
            return $http.get('orderpaymentterms?paymentOrderId='+orderId);
        },
        getOrderPaymentModes:function (orderId) {
            return $http.get('orderpaymentmodes?paymentOrderId='+orderId);
        }

    };
}]);
app.factory('OrderList',['$http','Orders',function($http,Orders){
    return {
        listOrdersByBusinessUnit: function (isMyOrders,paginationKeys) {
            if (isMyOrders===0) {
                return $http.get('ordersByBusinessUnit', {params:paginationKeys});
            } else {
                return $http.get('ordersQuery', {params: paginationKeys});
            }
        },
        find: function (busUnitId, tabSelection, scope) {
            if (scope) {
                scope.selectedTab = 'MyOrders';
                scope.selectedIndex = 0;
                scope.selOrder = null;
                scope.company.businessUnit = busUnitId;
                $http.get('ordersByBusinessUnit', {params: {businessUnitId: busUnitId}}).then(function (result) {
                    scope.orders = result.data.order;
                    scope.panelChange(null, 'viewAll');

                });
            }
        },
        findSpecific: function (busUnitId, tabSelection, scope) {
            scope.selectedTab = 'ReceivedOrders';
            scope.selectedIndex = 1;
            scope.orders = [];
            scope.selOrder = null;
            /*	scope.orders=[];*/
            $http.get('ordersQuery').then(function (response) {
                scope.orders = response.data;
                if (scope.orders.length > 0) {
                    if (tabSelection) {
                        scope.findOne(tabSelection);
                    } else {
                        scope.panelChange(null, 'viewAll');
                    }

                }
            });
        },
        ordersSummary:function (index, businessUnitId) {
            if(index === 0){
                return $http.get('sourcesummary?businessUnitId='+businessUnitId);
            }else if(index === 1){
                return $http.get('targetummary?businessUnitId='+businessUnitId);
            }
        },
        ordersFilterSummary:function (index, businessUnitId,paginationKeys,data) {
            if(index === 0){
                return $http.post('sourcesummary?businessUnitId='+businessUnitId,data,{params:paginationKeys});
            }else if(index === 1){
                return $http.post('targetummary?businessUnitId='+businessUnitId,data,{params:paginationKeys});
            }
        }
    };
}]);
app.factory('OrderAddress',[function(){
    return {
       /* getSelectedAddress:function (order) {
            return  $http.get('invoiceOrder/' + orderId, {responseType: 'arraybuffer'});
        },*/
        addAddress:function (scope) {
            var order = scope.selOrder;
            var ShippingAddress=null;
            var billingAddress=null;
            order.isAddress=true;
            if(scope.address.shipping){
                ShippingAddress=angular.copy(scope.address);
                ShippingAddress.addressType='Shipping';
                if(scope.selOrder.buyer.businessUnit && scope.selOrder.buyer.businessUnit.addresses){
                    scope.selOrder.buyer.businessUnit.addresses.push(ShippingAddress);
                }else{
                    if(scope.selOrder.buyer.businessUnit){
                        scope.selOrder.buyer.businessUnit.addresses=[];
                    }
                    if(scope.selOrder.buyer.contact)
                    scope.selOrder.buyer.contact.addresses.push(ShippingAddress);
                }
                ShippingAddress.isNew=true;
            }else{
                ShippingAddress=order.billingAddress;
            }
            if(scope.address.billing){
                billingAddress=angular.copy(scope.address);
                billingAddress.addressType='Billing';
                if(scope.selOrder.buyer.businessUnit){
                    scope.selOrder.buyer.businessUnit.addresses.push(billingAddress);
                }else{
                    if(scope.selOrder.buyer.contact)
                    scope.selOrder.buyer.contact.addresses.push(billingAddress);
                }
                billingAddress.isNew=true;

            }else{
                billingAddress=order.billingAddress;
            }
            scope.selOrder.isAddress=true;
            scope.selOrder.billingAddress=billingAddress;
            scope.selOrder.shippingAddress=ShippingAddress;
        },
        checkboxSelection:function (isBilling) {
           return angular.element(document.querySelectorAll(isBilling?'.billingaddr:checked':'.shippingaddr:checked')).length>0;
        },
        filterAddressLines:function (typedValue,scope) {
            var addresses=null;
            var sellerGstin=null;
            var buyerGstin=null;
            if(scope.selOrder && scope.selOrder.offer && scope.selOrder.offer.offerType==='Sell' && scope.selOrder.buyer && scope.selOrder.buyer.contact){
                if(scope.selOrder.buyer.businessUnit){
                    addresses = scope.selOrder.buyer.businessUnit.addresses;
                    buyerGstin=scope.selOrder.buyer.businessUnit.gstinNumber;
                }
                else{
                    addresses=scope.selOrder.buyer.contact.addresses;
                    buyerGstin=scope.selOrder.buyer.contact.gstinNumber;
                }

            }
            if(scope.selOrder && scope.selOrder.offer && scope.selOrder.offer.offerType==='Buy' && scope.selOrder.seller && scope.selOrder.seller.contact){
                if(scope.selOrder.seller.businessUnit){
                    addresses=scope.selOrder.buyer.businessUnit.addresses;
                    sellerGstin=scope.selOrder.buyer.businessUnit.gstinNumber;
                }
                else{
                    addresses=scope.selOrder.buyer.contact.addresses;
                    sellerGstin=scope.selOrder.buyer.contact.gstinNumber;
                }

            }
            if(scope.selOrder && !scope.selOrder.offer && scope.selOrder.buyer && (scope.selOrder.isIntraStock || scope.selOrder.buyer.contact)){
                if(scope.selOrder.buyer.businessUnit){
                    addresses = scope.selOrder.buyer.businessUnit.addresses;
                    buyerGstin=scope.selOrder.buyer.businessUnit.gstinNumber;
                }
                else{
                    addresses=scope.selOrder.buyer.contact.addresses;
                    buyerGstin=scope.selOrder.buyer.contact.gstinNumber;
                }

            }
            var array = [];
            if(addresses){
                addresses.filter(function (address) {
                    if(address){
                        if(address.addressType===typedValue) {
                            array.push(address);
                        }
                    }
                });
            }
            return array;

        },
        findSingleAddress:function (type,addresses) {
            return addresses.filter(function (address) {
                if (address && type.addressLine === address.addressLine && type.pinCode === address.pinCode && type.city === address.city && type.country === address.country && type.state === address.state) {
                    return true;
                }
            });
        }

    };
}]);
app.factory('OrderStatus',['$http','$mdToast',function($http,$mdToast){
    /*return {
        getSelectedAddress: function (order) {
            return $http.get('invoiceOrder/' + orderId, {responseType: 'arraybuffer'});
        }
    };*/
    function filterSelectedOrders($scope) {
        var selectedOrders={'orders':[],businessUnit:$scope.authentication.companyBusinessUnits.defaultBusinessUnitId};
        angular.forEach($scope.orders,function (order) {
            if(order.selected){
                selectedOrders.orders.push({_id:order._id});
            }
        });
        return selectedOrders;
    }
    return {
        batchOrders: function (index, scope) {
            var selectedOrder =  filterSelectedOrders(scope);
            if(selectedOrder.orders.length>0) {
                if (index === 0) {
                    return $http.post('/ordersremove', selectedOrder).then(function (results) {
                        scope.orders = results.data.order;
                        scope.total_count = results.data.total_count;
                        scope.getOrdersSummary(scope.selectedIndex,scope.busUnitId);
                    }, function (err) {
                        scope.error = err.data.message;
                    });
                }
            }else{
                $mdToast.show(
                    $mdToast.simple()
                        .textContent('Please select at least one Order')
                        .position('bottom right')
                        .theme('error-toast')
                        .hideDelay(3000)
                );
            }
        }
    };

}]);
app.directive('listOrderTable', function(){
    return{
        restrict: 'E',
        templateUrl: 'modules/orders/views/list-orders-table.client.viewnew.html'
    };
});
app.directive('listOrderCards', function () {
    return{
        restrict: 'E',
        templateUrl: 'modules/orders/views/list-orders-cards.client.viewnew.html'
    };
});
