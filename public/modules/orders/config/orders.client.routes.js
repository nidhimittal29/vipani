'use strict';

//Setting up route
angular.module('orders').config(['$stateProvider',
	function($stateProvider) {
		// Orders state routing
		$stateProvider.
		state('listOrders', {
			url: '/orders',
			templateUrl: 'modules/orders/views/list-orders.client.viewnew.html'
		}).
		state('createOrder', {
			url: '/orders/create',
			templateUrl: 'modules/orders/views/create-order.client.view.html'
		}).
		state('viewOrder', {
			url: '/orders/:orderId',
			templateUrl: 'modules/orders/views/list-orders.client.viewnew.html'
		}).
        state('viewOrderComment', {
            url: '/orders/:orderId/:message',
            templateUrl: 'modules/orders/views/list-orders.client.viewnew.html'
        }).
		state('editOrder', {
			url: '/orders/:orderId/edit',
			templateUrl: 'modules/orders/views/list-orders.client.viewnew.html'
		}).
		state('changeOrder', {
			url: '/orders/:orderId/change',
			templateUrl: 'modules/orders/views/change-order.client.view.html'
		});
	}
]);
