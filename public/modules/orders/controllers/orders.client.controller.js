'use strict';

// Orders controller
var app = angular.module('orders');
app.controller('OrdersController', ['$scope', '$sce', '$stateParams', '$filter', '$location', '$http', '$window','localStorageService', 'Authentication', 'Orders','productDetail','OrderList','OrdersExternal' ,'Contacts','contactsQuery','OrderContacts','OrderProducts','OrderAddress','OrderPayments' ,'inventoryProduct', 'Messages', 'OrderMessages', 'DatePicker', '$element', 'panelTrigger', '$timeout', '$mdDialog', '$mdMedia','verifyDelete','$mdToast','alert','OrderStatus','CompanyService',
    function ($scope, $sce, $stateParams, $filter, $location, $http, $window, localStorageService,Authentication, Orders,productDetail,OrderList,OrdersExternal, Contacts,contactsQuery, OrderContacts,OrderProducts,OrderAddress,OrderPayments,inventoryProduct, Messages, OrderMessages, DatePicker, $element, panelTrigger, $timeout, $mdDialog, $mdMedia,verifyDelete,$mdToast,alert,OrderStatus,CompanyService) {
        $scope.authentication = Authentication;
        $scope.user=Authentication.user;
        $scope.company={};
        $scope.busUnitId=$scope.authentication.companyBusinessUnits.defaultBusinessUnitId;
        $scope.addressTypes=[];
        $scope.billing=false;
        $scope.receving=false;
        $scope.shipping=false;
        $scope.invoice=false;
        $scope.showBilling=false;
        $scope.showShipping=false;
        $scope.singleValueShipping=false;
        $scope.singleValueBilling=false;
        $scope.addressTypesVsDetails=[];
        $scope.selectedPageId=0;
        $scope.selectedPage={};
        $scope.view=true;
        $scope.location = $location;
        $scope.payment=false;
        $scope.searchText = '';
        $scope.addresspage=false;
        $scope.orderPaymentOptions=[];
        $scope.selectedIndex=0;
        $scope.selectedTab='MyOrders';
        $scope.paymentOptionselection=[];
        $scope.selectedTag=false;
        $scope.billingAddresses=[];
        $scope.shippingAddresses=[];
        $scope.searchText = {};
        $scope.searchFields={};
        $scope.queryBy = '$';
        $scope.tabs = [
            { title:'My Orders', type:'MyOrders'},
            { title:'Received Orders',type:'ReceivedOrders'}
        ];
        $scope.paymentOptions = {
            installments: 'Installments',
            lineOfCredit: 'Line of Credit',
            payNow: 'Pay Now',
            payOnDelivery: 'Pay On Delivery'
        };
        $scope.pages=[];
        $scope.dt = {};
        $scope.dt = DatePicker.build({});
        $scope.products = [];
        $scope.product = [];
        $scope.metaProducts = [];
        $scope.paymentHistory=[];
        $scope.statusHistory=[];
        $scope.newproducts=[];
        $scope.currentOrderPaymentStatus=[];
        $scope.currentOrderStatus=[];
        $scope.workspaces =[];
        $scope.orderTTypes={
            Sale: 'Sell',
            Purchase: 'Buy'

        };

        $scope.statusTypes=[{value:'Drafted',text:'Drafted'},{value:'Placed',text:'Placed'},{value: 'Cancelled',text:'Cancelled'},{value: 'Confirmed',text:'Confirmed'}, {value: 'Rejected',text:'Rejected'}, {value:'InProgress',text:'InProgress'}, {value:'Shipped',text:'Shipped'},{value:'Delivered',text:'Delivered'},{value:'Completed',text:'Completed'}];


        $scope.orderStatusTypes=
            {'Sell':{
                    'Sale':{
                        'Drafted':[{value:'Placed',text:'Place'},{value:'Confirmed',text:'Confirm'},{value: 'Cancelled',text:'Cancel'}],
                        'Placed':[{value: 'Confirmed',text:'Confirm'},{value: 'Rejected',text:'Reject'},{value: 'Cancelled',text:'Cancel'}],
                        'Confirmed':[{value:'InProgress',text:'In Progress'},{value:'Shipped',text:'Ship'}],
                        'Shipped':[{value:'Delivered',text:'Deliver'}],
                        'Delivered':[{value:'Completed',text:'Complete'}],
                        'Disputed':[],
                        'InProgress':[{value:'Shipped',text:'Ship'},{value:'Delivered',text:'Deliver'}],
                        'Rejected':[],
                        'Completed':[],
                        'Resolved':[{value:'Completed',text:'Complet'}],
                        'Returned':[],
                        'Cancelled':[]
                    },
                    'Purchase':{
                        'Drafted':[{value:'Placed',text:'Place'},{value: 'Cancelled',text:'Cancel'}],
                        'Placed':[{value: 'Cancelled',text:'Cancel'}],
                        'Confirmed':[],
                        'Shipped':[{value:'Delivered',text:'Deliver'}],
                        'Delivered':[{value:'Disputed',text:'Disputed'}],
                        'InProgress':[],
                        'Completed':[],
                        'Rejected':[],
                        'Disputed':[{value:'Resolved',text:'Resolve'},{value:'Returned',text:'Return'}],
                        'Resolved':[],
                        'Returned':[],
                        'Cancelled':[]
                    }},'Buy':{
                    'Sale':{	'Drafted':[{value:'Placed',text:'Place'},{value: 'Cancelled',text:'Cancel'}],
                        'Placed':[{value: 'Cancelled',text:'Cancel'}],
                        'Confirmed':[],
                        'Shipped':[{value:'Delivered',text:'Deliver'}],
                        'Delivered':[{value:'Disputed',text:'Dispute'}],
                        'InProgress':[],
                        'Completed':[],
                        'Rejected':[],
                        'Disputed':[{value:'Resolved',text:'Resolve'},{value:'Returned',text:'Return'}],
                        'Resolved':[],
                        'Returned':[],
                        'Cancelled':[]
                    },'Purchase':{
                        'Drafted':[{value:'Placed',text:'Place'},{value:'Confirmed',text:'Confirm'},{value: 'Cancelled',text:'Cancel'}],
                        'Placed':[{value: 'Confirmed',text:'Confirm'},{value: 'Rejected',text:'Reject'},{value: 'Cancelled',text:'Cancel'}],
                        'Confirmed':[{value:'InProgress',text:'In Progress'},{value:'Shipped',text:'Ship'}],
                        'Shipped':[{value:'Delivered',text:'Deliver'}],
                        'Delivered':[{value:'Completed',text:'Complete'}],
                        'Disputed':[],
                        'InProgress':[{value:'Shipped',text:'Ship'},{value:'Delivered',text:'Deliver'}],
                        'Rejected':[],
                        'Completed':[],
                        'Resolved':[{value:'Completed',text:'Complete'}],
                        'Returned':[],
                        'Cancelled':[]
                    }
                }};
        /* Add Address dialog */
        $scope.changePaymentTerm=function (order) {

            if(order){
                if(order.applicablePaymentTerm.type==='Installments'){
                    order.selectedPayMentOptions=angular.copy(Object.assign(order.applicablePaymentTerm,order.applicablePaymentTerm.installmentPaymentDetails));
                }else{
                    order.selectedPayMentOptions=angular.copy(Object.assign(order.applicablePaymentTerm,order.applicablePaymentTerm.paymentDetails,order.applicablePaymentTerm.paymentMode));
                }
            }
        };
        $scope.showAddressDialog = function(isBilling,update) {
            if(update){
                $scope.address.old=true;
                $scope.billing = false;
                $scope.shipping = false;
                $scope.address=this.address;
                if ($scope.address.addressType==='Billing') {
                    $scope.billing = true;
                    $scope.address.shipping=true;

                } else if ($scope.address.addressType==='Shipping') {
                    $scope.shipping = true;
                    $scope.address.billing=true;
                }
            }else {
                $scope.address={shipping:false,billing:false,pinCode:'',country:'',state:'',city:'',addressLine:''};
                $scope.billing = false;
                $scope.shipping = false;
                if (isBilling) {
                    $scope.billing = true;
                } else {
                    $scope.shipping = true;
                }
            }
            var useFullScreen = ($mdMedia('sm') || $mdMedia('xs'));
            $mdDialog.show({
                templateUrl: 'modules/orders/views/address-dialog.html',
                /* targetEvent: ev,*/
                scope: $scope,
                clickOutsideToClose: true,
                fullscreen: useFullScreen,
                preserveScope: true
            });

        };

        function displayName(player,isIntraStock) {
            var currentName='';
            if(player && player.contact) {
                currentName+=(player.contact.displayName ? player.contact.displayName : player.contact.firstName + ' ' + player.contact.middleName + ' ' + player.contact.lastName)+' \n ';
            }
            if (player && player.nVipaniCompany && player.nVipaniCompany.name) {
                currentName+=player.nVipaniCompany.name;
                if (player && player.businessUnit && player.businessUnit.name) {
                    currentName += '\n ('+player.businessUnit.name+')';
                }
            }
             /*if (player && player.nVipaniUser) {
                currentName+=(currentName.length>0?'/':'')+ player.nVipaniUser.displayName;
            }*/
            return currentName;
        }
        $scope.getSourceOrder = function(order,isSoruce){
            if(order &&((order.buyer && order.buyer.contact) ||(order.seller && order.seller.contact))) {
                /*  return $scope.isTargetPlayer(order) ? (order.buyer && order.buyer.contact?(order.buyer.contact.displayName ? order.buyer.contact.displayName : order.buyer.contact.firstName + ' ' + order.buyer.contact.middleName + ' ' + order.buyer.contact.lastName):''):(order.seller && order.seller.contact ?(order.seller.contact.displayName ? order.seller.contact.displayName : order.seller.contact.firstName + ' ' + order.seller.contact.middleName + ' ' + order.seller.contact.lastName):'');*/
                if ($scope.tabs[$scope.selectedIndex].type === 'MyOrders' && order.orderType === 'Sale' || $scope.tabs[$scope.selectedIndex].type === 'ReceivedOrders' && order.orderType === 'Purchase') {
                    if(isSoruce) {
                        return displayName(order.buyer,order.isIntraStock);
                    }else{
                        return displayName(order.seller,order.isIntraStock);
                    }
                } else if ($scope.tabs[$scope.selectedIndex].type === 'MyOrders' && order.orderType === 'Purchase' || $scope.tabs[$scope.selectedIndex].type === 'ReceivedOrders' && order.orderType === 'Sale') {
                    if(isSoruce) {
                        return displayName(order.seller,order.isIntraStock);
                    }else{
                        return displayName(order.buyer,order.isIntraStock);
                    }
                } else {
                    return '';
                }
            }else{
                return '';
            }

        };
        $scope.isTargetPlayer=function (order) {
            return (order && ((order.isIntraStock && order.buyer && order.buyer.businessUnit && order.buyer.businessUnit._id===$scope.busUnitId) || (!order.isIntraStock && order.owner.company === $scope.authentication.user.company)));
        };
        $scope.getPlayerText = function (order,isSoruce,isText) {
            if (order && ((order.buyer && order.buyer.contact) || (order.seller && order.seller.contact))) {
                if (!$scope.tabs[$scope.selectedIndex]) {
                    return '';
                }else if ($scope.tabs[$scope.selectedIndex].type) {
                    if ($scope.tabs[$scope.selectedIndex].type === 'MyOrders') {
                        if (order.orderType === 'Sale') {
                            if (isSoruce) {
                                    return isText?'Order From :': displayName(order.seller,order.isIntraStock);
                            } else {
                                return isText?'Order To :' :displayName(order.buyer,order.isIntraStock);
                            }

                        } else{
                            if (isSoruce) {
                                return isText ?'Order To : ': displayName(order.seller,order.isIntraStock);
                            } else {
                                return isText?'Order From : ': displayName(order.buyer,order.isIntraStock);
                            }
                        }
                    } else if ($scope.tabs[$scope.selectedIndex].type === 'ReceivedOrders') {
                        if (order.orderType === 'Sale') {
                            if (isSoruce) {
                                return isText?'Order From :' : displayName(order.buyer,order.isIntraStock);
                            } else {
                                return isText?'Order To :': displayName(order.seller,order.isIntraStock);
                            }

                        } else {
                            if (isSoruce) {
                                return isText?'Order To :':displayName(order.seller,order.isIntraStock);
                            } else {
                                return isText?'Order From :': displayName(order.buyer,order.isIntraStock);
                            }
                        }
                    } else {
                        return '';
                    }
                } else {
                    return '';
                }
            }else {
                return '';
            }
        };
        $scope.batchOrders=function (index) {
            OrderStatus.batchOrders(index,$scope);
        };
        $scope.getOrdersByBusinessUnit = function(busUnitId){
            $scope.busUnitId=busUnitId;
            var authdata= localStorageService.get('nvipanilogindata');
            authdata.companyBusinessUnits.defaultBusinessUnitId= $scope.busUnitId;
            localStorageService.set('nvipanilogindata',authdata);
            var type;
            switch($scope.selectedIndex){
                case 0:type = 'MyOrders';break;
                case 1:type = 'ReceivedOrders';break;
            }
            if($scope.selOrder && $scope.selOrder.currentStatus==='Drafted'){
                CompanyService.getBusinessUnit(busUnitId).then(function (response) {
                    $scope.selOrder.buyer.businessUnit=response.data;
                });
            }
            $scope.changeSelectionTab(busUnitId,type);
        };
        $scope.getOrdersSummary=function (index, businessUnitId) {
            OrderList.ordersSummary(index,businessUnitId).then(function (response) {
                $scope.orderfilters=response.data;
            },function (err) {
                $scope.showToast(err.data.message,'top right','body','error-toast');
            });
        };
        $scope.ordersFilterSummary=function(filterKey,filterCount,pageno) {
            var paginationfield = {};
            $scope.isFilterSummary = true;
            if (!isNaN(pageno)) {
                paginationfield = {page: pageno, limit: $scope.itemsPerPage};
            }
            if (this.searchText && this.searchText.length > 0) {
                paginationfield.searchText = this.searchText;
            }
            if (filterKey) {
                $scope.filterKey = filterKey;
                $scope.data = {summaryFilters: {}};
                $scope.data.summaryFilters[$scope.filterKey] = true;
            }
            OrderList.ordersFilterSummary($scope.selectedIndex,$scope.busUnitId, paginationfield, $scope.data).then(function (response) {
                $scope.orders=response.data[$scope.filterKey];
                if (filterCount) {
                    $scope.total_count = filterCount;
                }
            }, function (res) {
                $scope.showToast(res.data.message,'top right','body','error-toast');
            });
        };
        $scope.getOrdersFilterSummary=function(filterKey,filterCount,toggle){
            if(toggle){
                $scope.ordersFilterSummary(filterKey,filterCount,1);
            }else{
                $scope.filterKey=null;
                /*var isMyOrder=$scope.tabs[$scope.selectedIndex].type==='MyOrders';*/
                $scope.find($scope.selectedIndex,1);
            }
        };
        // Find a list of Orders
        $scope.find = function (tabSelection,pagenumber) {
            $scope.isFilterSummary=false;
            var paginationfield={};
            if(!isNaN(pagenumber)){
                paginationfield={page:pagenumber,limit:$scope.itemsPerPage,businessUnitId:$scope.busUnitId};
            }
            if(this.searchText && this.searchText.length>0){
                paginationfield.searchText=this.searchText;
            }

            OrderList.listOrdersByBusinessUnit(tabSelection,paginationfield).then(function(response){
                $scope.orders = response.data.order;
                $scope.paginationId = $scope.tabs[ $scope.selectedIndex].type;
                $scope.total_count=response.data.total_count;
                if(response.data.summaryData){
                    if($scope.filterKey){
                        $scope.orders=response.data.summaryData[$scope.filterKey];
                    }else {
                        $scope.orderfilters = response.data.summaryData;
                    }
                }
            },function (err) {
                $scope.orders = [];
                $scope.orderfilters = [];
                $scope.showToast(err.data.message,'top right', 'body', 'error-toast');
            });
        };
        function getSelectedIndex(type) {
            var filterValue= $scope.tabs.filter(function (eachtab) {
                return eachtab.type===type;
            });
            if(filterValue.length>0){
                return $scope.tabs.indexOf(filterValue[0]);
            }else {
                return 0;
            }
        }
        $scope.changeSelectionTab=function (busUnitId,type) {
            $scope.itemsPerPage=10;
            $scope.filterKey=null;
            $scope.selectedIndex= getSelectedIndex(type);
            if(type==='MyOrders'){
                $scope.find(0,1);
            }else if(type==='ReceivedOrders'){
                $scope.find(1,1);
            }
        };
        $scope.orderUpdateStatusEditButton=function (order) {
            $scope.currentRole='Sale';
            if (this.order){
                if(!this.order.offer ||( this.order.offer &&  this.order.offer.owner.company === this.authentication.user.company)) {
                    this.order.createduser = true;
                    $scope.currentRole = 'Sale';
                    if (!this.order.offer && ((angular.isObject(this.order.owner) && this.order.owner.company !== $scope.authentication.user.company) || (!angular.isObject(this.order.owner) && this.order.owner.company !== this.authentication.user.company))) {
                        $scope.currentRole = 'Purchase';

                    }

                } else {
                    this.order.createduser = false;
                    this.currentRole = 'Purchase';
                }
                if (this.order.offer) {
                    this.order.statusArray = (($scope.orderStatusTypes[this.order.offer.offerType])[this.currentRole])[this.order.currentStatus];
                }else {
                    this.order.statusArray = (($scope.orderStatusTypes.Sell[this.currentRole])[this.order.currentStatus]);
                }
                $scope.currentRole=this.currentRole;
                return (this.order.currentStatus !== 'Drafted' && this.order.statusArray.length > 0);
            }else{
                return false;
            }

        };
        $scope.productDetails=function (products,index) {
            $scope.selectedProduct=angular.copy(products[index].inventory);
            $scope.showBothPrice=OrderProducts.getShowBothPriceOnly($scope,true);
            $scope.showSalePrice=OrderProducts.getShowSalePriceOnly($scope,true);
            if($scope.showSalePrice===true && $scope.showBothPrice!==true && $scope.selOrder.offer){
                $scope.selectedProduct.moqAndMargin=inventoryProduct.getMOQBasedQuantities(products[index].inventory.moqAndMargin,products[index].MOQ);
            }
            if( $scope.selectedProduct.moqAndMargin.length>0)
                productDetail($scope.selectedProduct,'modules/products/views/view-product-details-dialog.html',$scope).then(function () {
                    $scope.tModes=[{value:'Rail',text:'Rail'},{value:'Other',text:'Other'}];

                });


        };
        $scope.showInsuffientProductsView=function () {
            productDetail($scope.insuffientProducts,'modules/orders/views/view-product-insuffientdetails-dialog.html',$scope).then(function () {
                /* $scope.tModes=[{value:'Rail',text:'Rail'},{value:'Other',text:'Other'}];*/

            });
        };
        $scope.showOrderDialog = function (order) {
            $scope.changeStatusOrder(order);
            var useFullScreen = ($mdMedia('sm') || $mdMedia('xs'));
            /* OrdersExternal.getEnums().then(function (response) {
                 $scope.allStatus=response.data.currentStatus;

             },function(errorResponse){
                 $scope.error = errorResponse.data.message;
             });*/
            $mdDialog.show({
                template: '<md-toolbar class="md-primary"><div class="md-toolbar-tools">' +
                '<h4 flex >Change order status</h4>' +
                '<md-button class="md-icon-button" ng-click="cancel()" aria-label="close button"><md-icon class="fa fa-times fa-lg"></md-icon></md-button>' +
                '</div></md-toolbar><order-status-view></order-status-view>',
                scope: $scope,
                clickOutsideToClose: true,
                fullscreen: useFullScreen,
                preserveScope: true
            });

        };

        $scope.changeStatusPriority=function () {
            if($scope.currentStatus==='Confirmed'){
                $scope.selOrder.deliveryDate=new Date();
            }else if ($scope.currentStatus==='Cancelled'){
                $scope.mustRequiredcomments=true;
            }
        };
        $scope.hide = function () {
            $mdDialog.hide();
        };
        $scope.cancel = function () {
            /*$scope.currentStatus=$scope.order.;*/
            $mdDialog.cancel();
        };
        $scope.submitOrder=function(){
            $scope.update();

        };
        $scope.getSelectedPageId=function () {
            return $scope.selectedPageId;
        };
        $scope.setSelectedPageId=function (selectedPageId) {
            $scope.selectedPageId=selectedPageId;
        };

        $scope.saveOrder=function () {
            $scope.isLoading = false;
            $scope.saveorder=angular.copy($scope.selOrder);
            if(OrderContacts.getValidContactSelection($scope.selOrder.buyer,false,$scope)){
                return alert({title:'Error',content:'Please choose the Buyer'});
            }else if(OrderContacts.getValidContactSelection($scope.selOrder.seller,true,$scope)){
                return alert({title:'Error',content:'Please choose the Seller'});
            }else if(OrderProducts.getValidProductsSelection($scope.selOrder.products,$scope)){
                return alert({title:'Error',content:'Please select valid products'});
            }
            $scope.saveorder.totalAmount=$scope.totalPriceFormatter();
            if(!$scope.selOrder.offer || ($scope.selOrder.offer && $scope.selOrder.orderType==='Sale' )) {
                if ($scope.saveorder.seller) {
                    var sellerContact = OrderContacts.getContact($scope.saveorder.seller.contact._id,$scope);

                    if (sellerContact.length > 0) {
                        $scope.saveorder.seller.contact = sellerContact[0]._id;
                        if (sellerContact[0].nVipaniUser) {
                            $scope.saveorder.seller.nVipaniUser = sellerContact[0].nVipaniUser;
                        }
                        $scope.saveorder.seller.businessUnit = $scope.selOrder.offer.businessUnit;

                    }

                }
                if ($scope.saveorder.buyer) {
                    var buyerContact =  OrderContacts.getContact($scope.saveorder.buyer.contact._id,$scope);


                    if (buyerContact.length > 0) {
                        $scope.saveorder.buyer.contact = buyerContact[0]._id;
                        if (buyerContact[0].nVipaniUser) {
                            $scope.saveorder.buyer.nVipaniUser = buyerContact[0].nVipaniUser;
                        }
                        $scope.saveorder.buyer.businessUnit = $scope.selOrder.buyer.businessUnit;
                    }

                }
                if ($scope.saveorder.mediator) {
                    var mediatorContact =  OrderContacts.getContact($scope.saveorder.mediator.contact._id,$scope);
                    if (mediatorContact.length > 0) {
                        $scope.saveorder.mediator.contact = mediatorContact[0]._id;
                        if (mediatorContact[0].nVipaniUser)
                            $scope.saveorder.mediator.nVipaniUser = mediatorContact[0].nVipaniUser;
                        /*if(mediatorContact[0].businessUnit){
                            $scope.saveorder.buyer.businessUnit=mediatorContact[0].businessUnit;
                        }*/
                    }

                }
            }

            /* $scope.orderUpdate($scope.saveorder,$scope.getSelectedPageId()+1);
         }else if($scope.getSelectedPageId() === 1) {*/
            var shipping=OrderAddress.checkboxSelection(false);
            var billing=OrderAddress.checkboxSelection(true);
            if(!(shipping) && billing){
                verifyDelete({billing:true}).then(function () {
                    $scope.saveorder.isAddress=true;
                    var ShippingAddress=angular.copy($scope.saveorder.billingAddress);
                    ShippingAddress.addressType='Shipping';
                    $scope.saveorder.buyer.contact.addresses.push(ShippingAddress);
                    ShippingAddress.isNew=true;
                    $scope.saveorder.shippingAddress=ShippingAddress;
                    $scope.orderUpdate($scope.saveorder,$scope.getSelectedPageId()+1);
                });
                return;
            }else if(!(billing) && shipping){
                verifyDelete({shipping:true}).then(function () {
                    $scope.saveorder.isAddress=true;
                    var BillingAddress=angular.copy($scope.saveorder.shippingAddress);
                    BillingAddress.addressType='Billing';
                    $scope.saveorder.buyer.contact.addresses.push(BillingAddress);
                    BillingAddress.isNew=true;
                    $scope.saveorder.billingAddress=BillingAddress;
                    $scope.orderUpdate($scope.saveorder,$scope.getSelectedPageId()+1);
                });
            }else if (!(shipping && billing)) {
                return alert({title: 'Error', content: 'Please select Shipping/Billing Address'});
            }
            /*}else{
                $scope.orderUpdate($scope.saveorder,$scope.getSelectedPageId()+1);
            }

        }else if($scope.getSelectedPageId() === 2){*/
            var selectedPaymentOptionType = OrderPayments.getSelectedPaymentOption($scope.selectedPayMentOptions);
            var selectedPaymentInstallOption = OrderPayments.getSelectedPaymentInstallOption($scope.selectedPayMentOptions.installments);
            if (!(selectedPaymentOptionType)) {
                return alert({title: 'Error', content: 'Please select atleast one Payment Option'});
            } else if (selectedPaymentOptionType.selection && ((selectedPaymentOptionType.key === 'PayNow' && !selectedPaymentOptionType.paymentMode) || (selectedPaymentOptionType.key === 'installments' && !($scope.selectedOptions && $scope.selectedOptions.paymentMode)))) {
                return alert({
                    title: 'Error',
                    content: 'Please select Payment Mode for' + selectedPaymentOptionType.key
                });
            }
            $scope.saveorder.currentStatus = $scope.pages[$scope.getSelectedPageId()].currentStatus;

            if (!$scope.selectedPayMentOptions) {
                $scope.saveorder.paymentOptionType = $scope.selectedPayMentOptions;
            }
            if ($scope.saveorder.selectedPayMentOptions && $scope.saveorder.selectedPayMentOptions.key && $scope.saveorder.currentStatus !== 'Drafted') {
                $scope.saveorder.paymentMode = this.paymentMode;
                $scope.saveorder = OrderPayments.setSelectedPaymentType($scope.saveorder, false, $scope);
            }


            $scope.orderUpdate($scope.saveorder,4);



        };
        $scope.showToast = function (message,position,selector,theme) {

            $mdToast.show(
                $mdToast.simple()
                    .textContent(message)
                    .position(position)
                    .theme(theme)
                    .hideDelay(6000)
                    .parent(angular.element(document.querySelector(selector)))
            );
        };
        function setPlayersOrder(){
            if(($scope.saveorder.orderType==='Buy' ||  $scope.saveorder.orderType==='Purchase') && !$scope.showSellerChange){
                $scope.saveorder.seller.contact=$scope.saveorder.seller.contact._id;
                if($scope.saveorder.seller.businessUnit && $scope.saveorder.seller.businessUnit._id) {
                    $scope.saveorder.seller.businessUnit = $scope.saveorder.seller.businessUnit._id;
                }
            }
            if(!$scope.showBuyerChange && $scope.saveorder.orderType==='Sale'){
                if($scope.saveorder.buyer.contact && $scope.saveorder.buyer.contact.nVipaniUser){
                    $scope.saveorder.buyer.nVipaniUser = $scope.saveorder.buyer.contact.nVipaniUser;

                    if($scope.saveorder.buyer.businessUnit && $scope.saveorder.buyer.businessUnit._id) {
                        $scope.saveorder.buyer.businessUnit = $scope.saveorder.buyer.businessUnit._id;
                    }else if((!$scope.saveorder.buyer.businessUnit) && $scope.contactBusinessUnits && $scope.contactBusinessUnits.length===1){
                        $scope.saveorder.buyer.businessUnit = $scope.contactBusinessUnits[0]._id;
                    }else{
                        return $scope.showToast('Please select Business Unit','top right', '#page-'+$scope.getSelectedPageId(), 'error-toast');
                    }

                }
                if($scope.saveorder.buyer && $scope.saveorder.buyer.businessUnit && $scope.saveorder.buyer.businessUnit._id) {
                   /* $scope.saveorder.buyer.nVipaniCompany = $scope.saveorder.buyer.businessUnit.company;*/
                    $scope.saveorder.buyer.businessUnit = $scope.saveorder.buyer.businessUnit._id;
                }else if($scope.saveorder.buyer && $scope.saveorder.buyer.contact) {
                    $scope.saveorder.buyer.contact = $scope.saveorder.buyer.contact._id ?$scope.saveorder.buyer.contact._id:$scope.saveorder.buyer.contact;
                }else{
                    return $scope.showToast('Please select Buyer','top right', '#page-'+$scope.getSelectedPageId(), 'error-toast');
                }
            }
            if($scope.saveorder.mediator && $scope.saveorder.mediator.contact === ''){
                $scope.saveorder.mediator.contact = undefined;
            }

            $scope.orderUpdate($scope.saveorder,$scope.getSelectedPageId()+1);
        }
        $scope.orderPaymentTerms=function(orderId) {
            OrderPayments.getOrderPaymentTerms(orderId).then(function (paymentTerms) {
                $scope.orderApplicableTerms= paymentTerms.data;
            },function (paymentTermErr) {
                $scope.showToast(paymentTermErr.data.message,'top right', '#page-'+$scope.getSelectedPageId(), 'error-toast');
            });
        };
        $scope.orderPaymentModes=function(orderId) {
            OrderPayments.getOrderPaymentModes(orderId).then(function (paymentModes) {
                $scope.orderApplicableModes= paymentModes.data;
            },function (paymentErr) {
                $scope.showToast(paymentErr.message,'top right', '#page-'+$scope.getSelectedPageId(), 'error-toast');
            });
        };
        $scope.next=function () {
            $scope.isLoading = false;
            $scope.saveorder=angular.copy($scope.selOrder);
            if($scope.getSelectedPageId()===0){
                if(OrderProducts.getValidProductsSelection($scope.selOrder.products,$scope)){

                    return $scope.showToast('Please select valid products','top right', '#page-'+$scope.getSelectedPageId(), 'error-toast');
                }
                $scope.saveorder.totalAmount=$scope.totalPriceFormatter();
                if(($scope.saveorder.orderType === 'Sale' && $scope.saveorder.buyer) || ($scope.saveorder.mediator)){
                    $scope.saveorder.buyer = undefined;
                    $scope.saveorder.mediator = undefined;
                }
                $scope.orderUpdate($scope.saveorder,$scope.getSelectedPageId()+1);
            }
            if ($scope.getSelectedPageId() === 1) {
                var shipping=OrderAddress.checkboxSelection(false);
                var billing=OrderAddress.checkboxSelection(true);
                if((!$scope.selOrder.isIntraStock && OrderContacts.getValidContactSelection($scope.selOrder.buyer,false,$scope))){
                    return $scope.showToast('Please choose buyer','top right', '#page-'+$scope.getSelectedPageId(), 'error-toast');
                }else if(($scope.selOrder.isIntraStock && OrderContacts.getValidBusinessUnitSection($scope.selOrder.buyer,false,$scope))) {
                    return $scope.showToast('Please choose buyer','top right', '#page-'+$scope.getSelectedPageId(), 'error-toast');
                }else if((!$scope.selOrder.isIntraStock && OrderContacts.getValidContactSelection($scope.selOrder.seller,true,$scope))|| ($scope.selOrder.isIntraStock && OrderContacts.getValidBusinessUnitSection($scope.selOrder.seller,false,$scope))){
                    return $scope.showToast('Please choose seller','top right', '#page-'+$scope.getSelectedPageId(), 'error-toast');
                }else if (!(shipping || billing)){
                    return $scope.showToast('Please select any Address','top right', '#page-'+$scope.getSelectedPageId(), 'error-toast');
                }else{
                    if(!(shipping) && billing){
                        verifyDelete({billing:true}).then(function () {
                            $scope.saveorder.isAddress=true;
                            var ShippingAddress=angular.copy($scope.saveorder.billingAddress);
                            ShippingAddress.addressType='Shipping';
                            if($scope.saveorder.buyer.businessUnit) {
                                $scope.saveorder.buyer.businessUnit.addresses.push(ShippingAddress);
                            }else{
                                $scope.saveorder.buyer.contact.addresses.push(ShippingAddress);
                            }
                            ShippingAddress.isNew=true;
                            $scope.saveorder.shippingAddress=ShippingAddress;
                            setPlayersOrder();
                        });
                    }else if(!(billing) && shipping){
                        verifyDelete({shipping:true}).then(function () {
                            $scope.saveorder.isAddress=true;
                            var BillingAddress=angular.copy($scope.saveorder.shippingAddress);
                            BillingAddress.addressType='Billing';
                            if($scope.saveorder.buyer.businessUnit) {
                                $scope.saveorder.buyer.businessUnit.addresses.push(BillingAddress);
                            }else{
                                $scope.saveorder.buyer.contact.addresses.push(BillingAddress);
                            }
                            BillingAddress.isNew=true;
                            $scope.saveorder.billingAddress=BillingAddress;
                            setPlayersOrder();
                        });
                    }else if(billing && shipping){
                        setPlayersOrder();
                    }
                }
            }
            else if($scope.getSelectedPageId() === 2){
                var selectedPaymentOptionType;

                if($scope.saveorder.applicablePaymentTerm && $scope.saveorder.applicablePaymentTerm.paymentTerm){
                    if($scope.saveorder.applicablePaymentTerm.type==='Installments'){
                        $scope.saveorder.applicablePaymentTerm.installmentPaymentDetails[0].amountPaid=$scope.saveorder.applicablePaymentTerm.installmentPaymentDetails[0].amount;
                        $scope.saveorder.applicablePaymentTerm.installmentPaymentDetails[0].status='Paid';
                    }/*else{
                        $scope.saveorder.applicablePaymentTerm.paymentDetails


                    }
                    if($scope.saveorder.applicablePaymentTerm.paymentTerm._id) {
                        $scope.saveorder.applicablePaymentTerm.paymentTerm = $scope.saveorder.applicablePaymentTerm.paymentTerm._id;
                    }
                    if($scope.saveorder.applicablePaymentTerm.paymentDetails.paymentMode && $scope.saveorder.applicablePaymentTerm.paymentDetails.paymentMode._id) {
                        $scope.saveorder.applicablePaymentTerm.paymentDetails.paymentMode = $scope.saveorder.applicablePaymentTerm.paymentDetails.paymentMode._id;
                    }*/
                    $scope.saveorder=$scope.setPaymentTermDetails($scope.saveorder);
                    $scope.saveorder.currentStatus=$scope.pages[$scope.getSelectedPageId()].currentStatus;

                    $scope.orderUpdate($scope.saveorder,4);
                }else{
                    return $scope.showToast('Please select one Payment Option','top right', '#page-'+$scope.getSelectedPageId(), 'error-toast');
                }


            }
        };

        $scope.checkboxSelection=function (isBilling) {
            if(isBilling)
                return angular.element(document.querySelectorAll(isBilling?'.billingaddr:checked':'.shippingaddr:checked')).length>0;

        };

        $scope.orderUpdate=function (order,page) {
            /*  if (order.seller && order.seller.contact && order.seller.contact._id) {
                  order.seller.contact = order.seller.contact._id;
              }
              if (order.buyer && order.buyer.contact && order.buyer.contact._id) {
                  order.buyer.contact = order.buyer.contact._id;
              }*/

            order.$update(function(orderResponse) {
                $scope.selOrder=orderResponse;
                $scope.saveorder=orderResponse;
                $scope.selOrder.totalAmountWithoutDiscount = $scope.getTotalAmountWithoutDiscount();

                if(page===4){
                    if($scope.selOrder.currentStatus!=='Drafted'){
                        $scope.getOrdersByBusinessUnit($scope.busUnitId);
                    }
                    $scope.panelChange($scope.selOrder,'view');
                }else {
                    /*  if(page===2){*/
                    /* orderPaymentTerms($scope.selOrder._id,function (termErr,paymentTerms) {
                         if(termErr){
                             return $scope.showToast(termErr.data.message,'top right', '#page-'+$scope.getSelectedPageId(), 'error-toast');
                         }else {
                             $scope.orderApplicableTerms=paymentTerms;
                             orderPaymentModes($scope.selOrder._id, function (modeErr, paymentModes) {
                                 if(modeErr){
                                     return $scope.showToast(modeErr.data.message, '#page-'+$scope.getSelectedPageId(), 'error-toast');
                                 }else{
                                     $scope.orderApplicableModes=paymentModes;
                                 }

                             });
                         }
                     });
                 }*/
                    $scope.setSelectedPageId(page);
                    $scope.setView();
                }
            });
        };

        $scope.setView=function(){
            $scope.page=$scope.pages[$scope.getSelectedPageId()];
            /*$scope.view=$scope.page.view;
            $scope.payment=$scope.page.payment;
            $scope.addresspage=$scope.page.addresspage;*/
        };

        $scope.prev=function (oldPages) {
            var oldPage=$scope.pages[$scope.selectedPageId];
            $scope.setSelectedPageId(this.page.number);
            var page = $scope.pages[this.page.number];
            $scope.orderUpdate($scope.selOrder,page.number);
        };
        $scope.isActive = function (page){
            $scope.selectedPage = $scope.pages[$scope.selectedPageId];

            return ($scope.selectedPage && $scope.selectedPage.title === page.title);
        };
        $scope.isCompleted = function (page){
            var index = $scope.pages.indexOf(page);
            return (index < $scope.selectedPageId);
        };
        $scope.nextShouldBeDisabled = function (){
            if(isNaN(this.selOrder && this.selOrder.totalAmount)){
                return true;
            }else if($scope.selectedPageId >= $scope.pages.length - 1){
                return true;
            }else if($scope.view){
                return $scope.orderValidation(true);
            }else if ($scope.payment){
                return !$scope.selOrder.selectedPayMentOptions.selection;
            }else if($scope.addresspage){
                return !$scope.selOrder.billingAddress || $scope.selOrder.billingAddress.length===0 || !$scope.selOrder.shippingAddress || $scope.selOrder.shippingAddress.length===0 || $scope.selOrder.billingAddress.pinCode.length===0 || $scope.selOrder.shippingAddress.pinCode.length===0;

            }

        };

        $scope.getValidProductsSelection=function (products) {
            if(products===undefined || products && products !==undefined &&products.length===0 || products.filter(function (product) {
                    return product.inventory.disabled;
                }).length > 0 || isNaN($scope.selOrder && $scope.selOrder.totalAmount)) {
                return true;
            }
        };
        $scope.prevShouldBeDisabled = function (){
            return ($scope.selectedPageId <= 0);
        };
        function findStatusObject(singleobject,trackDetails,completed,statushistoryLastStatus) {
            if ($scope.selOrder.statusHistory.length > 0) {
                var item = $scope.selOrder.statusHistory.filter(function (item) {
                    return item.status === singleobject.currentStatus;
                });
                if (item.length > 0 && !completed && trackDetails.filter(function (eachitem) {
                        return eachitem.currentStatus === singleobject.currentStatus;
                    }).length === 0) {
                    singleobject.complete = true;
                    singleobject.date = item[0].date;
                    $scope.trackdetails.push(singleobject);
                    if ($scope.selOrder.statusHistory.length > 0 && $scope.selOrder.statusHistory[$scope.selOrder.statusHistory.length-1].status === singleobject.currentStatus) {
                        completed = true;
                        statushistoryLastStatus=singleobject.currentStatus;
                        if (singleobject.active) {
                            return;
                        }else{
                            return;
                        }

                    }

                }if($scope.trackdetails.length===0){
                    return ;
                }

                if (completed) {
                    if (!singleobject.active && statushistoryLastStatus===singleobject.parentStatus){
                        statushistoryLastStatus=singleobject.currentStatus;
                        trackDetails.push(singleobject);
                    }
                }
            }
        }

        $scope.setCurrentTab=function(tabPosition) {
            switch (tabPosition) {
                case 0:
                    if($scope.selOrder.currentStatus==='Drafted'){
                        for(var i=0;i<$scope.selOrder.products.length;i++){
                            if($scope.selOrder.products[i].inventory)
                                $scope.selOrder.products[i].unitPrice=inventoryProduct.getQuantityPrice($scope.getMoqArray($scope.selOrder.products[i]), $scope.selOrder.products[i].numberOfUnits,$scope.selOrder.products[i].inventory.MRP,i);
                        }
                        if(! $scope.selOrder.offer || $scope.selOrder.offer.offerType==='Sell'){
                            $scope.pages =
                                [
                                    { id: 'step1', number:0,title: $scope.selOrder.orderType+' Order',active:true,view:true,isDifferentUser:true ,currentStatus:'Drafted'},
                                    { id: 'step2', number:1, title: 'Addresses',active:false,addresspage:true,isDifferentUser:true ,currentStatus:'Drafted'},
                                    { id: 'step3', number:2,title: 'Payment Details',active:false,payment:true,isDifferentUser:true ,currentStatus:'Placed'}
                                ];
                        }else if($scope.selOrder.offer.offerType==='Buy'){
                            $scope.pages =
                                [
                                    { id: 'step1',  number:0, title: $scope.selOrder.orderType+' Order',active:true,view:true,isDifferentUser:true ,currentStatus:'Drafted'},
                                    { id: 'step2', number:1, title: 'Addresses',active:false,addresspage:true,isDifferentUser:true ,currentStatus:'Drafted'},
                                    { id: 'step3', number:2, title: 'Payment Details',active:false,payment:true,isDifferentUser:true ,currentStatus:'Placed'}
                                ];
                        }


                    }else{
                        $scope.alldetails =[
                            { id: 'step1', title: 'Placed Order',active:false,isDifferentUser:false,pageIndex:0,parentStatus:'Placed',complete:false,currentStatus:'Placed',date:''},
                            { id: 'step2', title: 'Cancel Order',active:true,isDifferentUser:false,pageIndex:1,parentStatus:'Placed',complete:false,currentStatus:'Cancelled',date:''},
                            { id: 'step3', title: 'Confirmed Order',active:false,isDifferentUser:false,pageIndex:2,parentStatus:'Placed',complete:false,currentStatus:'Confirmed',date:''},
                            { id: 'step4', title: 'Reject Order',active:true,isDifferentUser:false,pageIndex:3,parentStatus:'Placed',complete:false,currentStatus:'Rejected',date:''},
                            { id: 'step5', title: 'InProgress Order',active:false,isDifferentUser:false,pageIndex:4,parentStatus:'Confirmed',complete:false,currentStatus:'InProgress',date:''},
                            { id: 'step6', title: 'Shipped Order',active:false,isDifferentUser:false,pageIndex:5,parentStatus:'InProgress',complete:false,currentStatus:'Shipped',date:''},
                            { id: 'step7', title: 'Shipped Order',active:false,isDifferentUser:false,pageIndex:6,parentStatus:'Confirmed',complete:false,currentStatus:'Shipped',date:''},
                            { id: 'step8', title: 'Delivered Order',active:false,isDifferentUser:false,pageIndex:7,parentStatus:'Shipped',complete:false,currentStatus:'Delivered',date:''},
                            { id: 'step9', title: 'Disputed Order',active:true,isDifferentUser:false,pageIndex:8,parentStatus:'Delivered',complete:false,currentStatus:'Disputed',date:''},
                            {id: 'step10', title: 'Completed Order', active: false, isDifferentUser: false, pageIndex: 9, parentStatus: 'Delivered', complete: false, currentStatus: 'Completed', date: ''},
                            {id: 'step11', title: 'Pending Order', active: false, isDifferentUser: false, pageIndex: 10, parentStatus: 'Disputed', complete: false, currentStatus: 'Pending', date: ''},
                            {id: 'step12', title: 'Resolved Order', active: false, isDifferentUser: false, pageIndex: 11, parentStatus: 'Disputed', complete: false, currentStatus: 'Resolved', date: ''},
                            {id: 'step13', title: 'Completed Order', active: false, isDifferentUser: false, pageIndex: 12, parentStatus: 'Resolved', complete: false, currentStatus: 'Completed', date: ''},
                            {id: 'step14', title: 'Returned Order', active: true, isDifferentUser: false, pageIndex: 13, parentStatus: 'Disputed', complete: false, currentStatus: 'Returned', date: ''},
                            {id: 'step15', title: 'Completed Order', active: false, isDifferentUser: false, pageIndex: 14, parentStatus: 'Returned', complete: false, currentStatus: 'Completed', date: ''},

                        ];
                        $scope.trackdetails=[];
                        var completed=false;
                        var statushistoryLastStatus='';
                        for(var j=0;j<$scope.alldetails.length;j++) {
                            var singleobject = $scope.alldetails[j];
                            if ($scope.selOrder.statusHistory.length > 0) {
                                var item = $scope.selOrder.statusHistory.filter(function (item) {
                                    return item.status === singleobject.currentStatus;
                        });
                                if (item.length > 0 && !completed && $scope.trackdetails.filter(function (eachitem) {
                                        return eachitem.currentStatus === singleobject.currentStatus;
                                    }).length === 0) {
                                    singleobject.complete = true;
                                    singleobject.date = item[0].date;
                                    $scope.trackdetails.push(singleobject);
                                    if ($scope.selOrder.statusHistory.length > 0 && $scope.selOrder.statusHistory[$scope.selOrder.statusHistory.length-1].status === singleobject.currentStatus) {
                                        completed = true;
                                        statushistoryLastStatus=singleobject.currentStatus;
                                        if (singleobject.active) {
                                            break;
                                        }else{
                                            continue;
                                        }

                                    }

                                }if($scope.trackdetails.length===0){
                                    continue;
                                }

                                if (completed) {
                                    if (!singleobject.active && statushistoryLastStatus===singleobject.parentStatus){
                                        statushistoryLastStatus=singleobject.currentStatus;
                                        $scope.trackdetails.push(singleobject);
                                    }
                                }
                            }
                        }
                        if($scope.trackdetails.length===0){
                            $scope.trackdetails = $scope.alldetails.filter(function (item) {
                                return !item.active;
                            });
                        }

                        for(var k=0;k<$scope.trackdetails.length;k++){
                            $scope.trackdetails[k].complete=true;
                            if($scope.trackdetails[k].currentStatus===$scope.selOrder.currentStatus){
                                break;
                            }

                        }
                    }
                    // $scope.selectTabs(0);
                    break;
                case 1:
                    /*    btnNext.setText('NEXT');
                        enableSpinner(false);*/
                    // $scope.selectTabs(1);
                    break;
                case 2:
                    /* btnNext.setText('PLACE ORDER');
                     enableSpinner(false);*/
                    // $scope.selectTabs(2);
                    break;
            }
        };
        $scope.selectTabs=function () {
            $scope.setCurrentTab($scope.getSelectedPageId());

        };

        $scope.addAddress=function () {
            $scope.address=this.address;
            if($scope.billing){
                $scope.address.billing=true;
            }else if($scope.shipping){
                $scope.address.shipping=true;
            }
            $scope.updateAddress();
            $scope.cancel();
        };

        $scope.paymentType=[{value:'Cash',text:'Cash'}, {value:'Cheque',text:'Cheque'}, {value:'Card',text:'Card'}, {value:'NEFT',text:'NEFT'},{value: 'IMPS',text:'IMPS'},{value: 'RTGS',text:'RTGS'}, {value: 'Other',text:'Other'}];



        $scope.paymentStatusList={
            'YetToPay':[{value:'AdvancePaid',text:'AdvancePaid'}, {value:'PartialPayment',text:'PartialPayment'}, {value:'Paid',text:'Paid'}],
            'AdvancePaid':[{value:'PartialPayment',text:'PartialPayment'}, {value:'Paid',text:'Paid'}],
            'PartialPayment':[{value:'PartialPayment',text:'PartialPayment'},{value:'Paid',text:'Paid'}],
        };
        $scope.roles = ['Buyer', 'Seller','Mediator'];
        //$scope.role=['Buyer'];
        $scope.my = { role: 'Buyer' };

        $scope.getBusinessUnitForContact = function(player){

            if(player.contact && player.contact.nVipaniUser) {
                var userid =  player.contact.nVipaniUser;
                CompanyService.getUserBusinessUnits(userid).then(function (response) {
                    $scope.contactBusinessUnits = response.data;
                });
                player.nVipaniCompany=player.contact.nVipaniCompany;
            }
        };

        $scope.invoice=function(){
            OrdersExternal.generateInvoice($scope.selOrder._id).then(function (response) {

                var file = new Blob([response.data], {type: 'application/pdf'});
                var fileURL = URL.createObjectURL(file);
                var temp = $window.open(fileURL,'_blank','true');
                return true;
            },function(errorResponse){
                $scope.error = errorResponse.data.message;
            });
        };
        $scope.orderUpdateFields =function(){
            $scope.selectedTag=false;
            for (var j in $scope.orders) {
                if ($scope.orders [j]._id === $scope.selOrder._id) {
                    $scope.orders[j] = $scope.selOrder;
                }
            }

            $scope.selOrder.notConfirmed = ($scope.selOrder.currentStatus === 'Placed' || $scope.selOrder.currentStatus === 'Drafted');
            if(!$scope.selOrder.onchangestatus){
                $scope.selOrder.onchangestatus=false;
            }

            /*if($scope.selOrder.offer.offerType === 'Seller'){
             currentRole=$scope.selOrder.orderType;
             if($scope.selOrder.orderType==='Sale'){
             $scope.selOrder.createduser=true;
             }else{
             $scope.selOrder.createduser=false;
             }
             }else{
             $scope.selOrder.createduser=false;
             currentRole='Purchase';
             if($scope.selOrder.orderType === 'Sale'){

             } else{
             //Purchase
             }
             }*/
            $scope.currentRole='Sale';
            if(!$scope.selOrder.offer ||( $scope.selOrder.offer && $scope.selOrder.offer.owner.company === this.authentication.user.company)) {
                $scope.selOrder.createduser = true;
                $scope.currentRole = 'Sale';
                if (!$scope.selOrder.offer && ((angular.isObject($scope.selOrder.owner) && $scope.selOrder.owner.company !== $scope.authentication.user.company) || (!angular.isObject($scope.selOrder.owner) && $scope.selOrder.owner.company !== $scope.authentication.user.company))) {
                    $scope.currentRole = 'Purchase';

                }

            }else{
                $scope.selOrder.createduser=false;
                $scope.currentRole='Purchase';
            }
            if($scope.selOrder.offer) {
                $scope.selOrder.statusArray = (($scope.orderStatusTypes[$scope.selOrder.offer.offerType])[$scope.currentRole])[$scope.selOrder.currentStatus];
            }else{
                $scope.selOrder.statusArray = ($scope.orderStatusTypes.Sell[$scope.currentRole])[$scope.selOrder.currentStatus];
            }
            if($scope.selOrder.role==='Seller'){
                if($scope.selOrder.seller) {
                    if ($scope.selOrder.seller.contact.firstName) {
                        $scope.selOrder.sellerDisplayName = $scope.selOrder.seller.contact.firstName + ' ' + $scope.selOrder.seller.contact.lastName;
                    } else {
                        $scope.selOrder.sellerDisplayName = $scope.selfcontact[0].firstName + ' ' + $scope.selfcontact[0].lastName;
                    }
                }
            }

            if($scope.selOrder.role==='Buyer') {
                if($scope.selOrder.buyer) {
                    if ($scope.selOrder.buyer.contact.firstName) {
                        $scope.selOrder.buyerDisplayName = $scope.selOrder.buyer.contact.firstName + ' ' + $scope.selOrder.buyer.contact.lastName;
                    } else {
                        $scope.selOrder.buyerDisplayName = $scope.selfcontact[0].firstName + ' ' + $scope.selfcontact[0].lastName;
                    }
                }
            }
            if($scope.selOrder.role==='Mediator'){
                if($scope.selOrder.mediator){
                    if($scope.selOrder.mediator.contact.firstName) {
                        $scope.selOrder.mediatorDisplayName = $scope.selOrder.mediator.contact.firstName + ' ' + $scope.selOrder.mediator.contact.lastName;
                    }else{
                        $scope.selOrder.mediatorDisplayName = $scope.selfcontact[0].firstName + ' ' + $scope.selfcontact[0].lastName;
                    }
                }
            }
            $scope.selOrder.totalAmount=$scope.totalPriceFormatter();
            $scope.selOrder.changeStatusDisplay=$scope.selOrder.onchangestatus===false && $scope.selOrder.statusArray.length>0 && $scope.selOrder.statusArray!==undefined && $scope.selOrder.totalAmount>0;

            $scope.selOrder.onchangestatus=false;
            $scope.selOrder.onchangespaymentstatus=false;
            /*  OrderPayments.setSelectedPayMentOptions($scope);*/

            $scope.amount=$scope.selOrder.totalAmount-$scope.selOrder.paidAmount;
            $scope.selOrder.changePaymentDisplay=$scope.selOrder.onchangespaymentstatus===false && $scope.selOrder.paymentArray!==undefined && $scope.selOrder.totalAmount>0 && $scope.selOrder.totalAmount-$scope.selOrder.paidAmount>0 && !($scope.selOrder.currentStatus==='Drafted' || $scope.selOrder.currentStatus==='Placed' || $scope.selOrder.currentStatus==='Cancelled' || $scope.selOrder.currentStatus==='Rejected');
            $scope.showSellerChange = ($scope.selOrder && (!$scope.selOrder.offer || $scope.selOrder.createduser) && $scope.selOrder.currentStatus === 'Drafted' && $scope.selOrder.offer && $scope.selOrder.offer.offerType === 'Sell' && $scope.selOrder.orderType === 'Sale') || ($scope.selOrder && $scope.selOrder.currentStatus === 'Drafted' && (!$scope.selOrder.offer || ! $scope.selOrder.createduser) && $scope.selOrder.seller && $scope.selOrder.seller.contact && $scope.selOrder.seller.contact.nVipaniCompany===$scope.authentication.user.company);
            $scope.showBuyerChange = ( $scope.selOrder && $scope.selOrder.createduser && $scope.selOrder.currentStatus === 'Drafted' && $scope.selOrder.offer && $scope.selOrder.offer.offerType === 'Buy' && $scope.selOrder.orderType === 'Purchase') || ($scope.selOrder && $scope.selOrder.currentStatus === 'Drafted' &&  ! $scope.selOrder.createduser && $scope.selOrder.seller && $scope.selOrder.seller.contact);

            $scope.showSellerChange=!OrderContacts.setShowSellerChange($scope);
            $scope.showBuyerChange=!OrderContacts.setShowBuyerChange($scope);

        };

        /* $scope.changeFields=function() {
             if ($scope.order.role === 'Buyer') {
                 $scope.selOrder.buyerDisplayName=$scope.selfcontact[0].firstName+' '+$scope.selfcontact[0].lastName;

                 $scope.selOrder.buyer.contact=$scope.selfcontact[0];
                 $scope.selOrder.mediator=null;
                 $scope.selOrder.seller=null;
                 $scope.selOrder.sellerDisplayName=null;
                 $scope.selOrder.mediatorDisplayName=null;
                 $scope.selOrder.buyer.nVipaniUser=$scope.selfcontact[0].nVipaniUser;
             }else if ($scope.selOrder.role === 'Seller'){
                 $scope.selOrder.sellerDisplayName=$scope.selfcontact[0].firstName+' '+$scope.selfcontact[0].lastName;

                 $scope.selOrder.seller.contact=$scope.selfcontact[0];
                 $scope.selOrder.mediator=null;
                 $scope.selOrder.buyer=null;
                 $scope.selOrder.buyerDisplayName=null;
                 $scope.selOrder.mediatorDisplayName=null;
                 $scope.selOrder.seller.nVipaniUser=$scope.selfcontact[0].nVipaniUser;
             }else if($scope.selOrder.role ==='Mediator') {
                 $scope.selOrder.mediatorDisplayName=$scope.selfcontact[0].firstName+' '+$scope.selfcontact[0].lastName;

                 $scope.selOrder.mediator.contact=$scope.selfcontact[0];
                 $scope.selOrder.sellerDisplayName=null;
                 $scope.selOrder.buyerDisplayName=null;
                 $scope.selOrder.seller=null;
                 $scope.selOrder.buyer=null;
                 $scope.selOrder.mediator.nVipaniUser=$scope.selfcontact[0].nVipaniUser;
             }
         };*/

        /*  $scope.showOrderPaymentStatusHistorys = function() {
              var selected = [];
              if($scope.currentOrder.paymentStatus) {
                  selected = $scope.paymentStatusList[$scope.currentOrder.paymentStatus];
              }
              return selected.length ? selected : 'Not set';
          };*/
        // Create new Message
        $scope.createMessage = function (orderId) {
            // Create new Message object
            var message = new Messages({
                body: this.body,
                order: orderId
            });

            // Redirect after save
            message.$save(function (response) {
                response.user = $scope.authentication.user;
                $scope.orderMessages.unshift(response);
                /*$location.path('orders/' + orderId);*/

                // Clear form fields
                $scope.body = '';
            }, function (errorResponse) {
                $scope.error = errorResponse.data.message;
            });
        };

        //Clear Fields Function
        $scope.clearFields = function () {
            $scope.buyer = '';
            $scope.seller = '';
            $scope.mediator = '';
            $scope.products = [];
            $scope.status = '';
            $scope.paymentStatus = '';
            $scope.offer = '';
            $scope.statusComments='';
            $scope.currentStatus='';
            $scope.loadText='';

        };

        $scope.totalPriceFormatter=function() {
            if($scope.selOrder){
                $scope.selOrder.totalAmount=0;
                angular.forEach($scope.selOrder.products, function (row) {
                    /*angular.foreach($scope.order.products, function (i, row) {
                     */
                    if(row.inventory && !row.inventory.disabled) {
                        $scope.selOrder.totalAmount += +(row.unitPrice) * row.numberOfUnits;
                    }
                });
                return  $scope.selOrder.totalAmount;
            }else{
                return 0;
            }
        };
        $scope.getTotalAmountWithoutDiscount = function(){
            if($scope.selOrder){
                var totalPrice = 0;
                angular.forEach($scope.selOrder.products, function (row) {
                    if(row.inventory && !row.inventory.disable){
                        totalPrice += +(row.unitMrpPrice) * row.numberOfUnits;
                    }
                });
                return totalPrice;
            }
            else{
                return 0;
            }
        };
        $scope.orderRedirection=function(){
            $location.path('orders');
        };
        $scope.orderValidation=function(update){
            if($scope.selOrder){

                if($scope.selOrder.offer) {
                    $scope.currentrole = $scope.selOrder.offer.offerType;
                }else{
                    $scope.currentrole = 'Sell';
                }
                if(update){
                    $scope.seller=$scope.selOrder.seller;
                    $scope.buyer=$scope.selOrder.buyer;
                    $scope.mediator=$scope.selOrder.mediator;
                    $scope.products=$scope.selOrder.products;
                    $scope.deliveryDate=$scope.selOrder.deliveryDate;
                }

                if($scope.products===undefined || $scope.products && $scope.products !==undefined && $scope.products.length===0 || $scope.products.filter(function (product) {
                        return product.inventory.disabled;
                    }).length > 0) {
                    return true;
                }else if($scope.currentrole==='Buy' ) {
                    if(($scope.seller && $scope.seller.contact && $scope.seller.contact!=='' && $scope.seller.contact._id) ||  ($scope.mediator && $scope.mediator.contact && $scope.mediator.contact!==''&&  $scope.mediator.contact._id)){
                        return false;
                    }else{
                        return true;
                    }

                }else if(!$scope.currentrole || $scope.currentrole==='Sell')
                {
                    if(($scope.buyer && $scope.buyer.contact && $scope.buyer.contact!=='' &&  $scope.buyer.contact._id) || ($scope.mediator && $scope.mediator.contact && $scope.mediator.contact!=='' && $scope.mediator.contact._id)){
                        return false;
                    }else{
                        return true;
                    }
                }else {
                    return false;
                }
            }else {
                return false;
            }
        };


        $scope.addOrderPayment=function(onchangespaymentstatus){
            $scope.onchangespaymentstatus=onchangespaymentstatus;
            return onchangespaymentstatus;
        };
        $scope.addOrderStatus=function(onchangestatus){
            $scope.onchangestatus=onchangestatus;
        };
        // Remove existing Order
        $scope.remove = function(order) {

            if (order) {
                verifyDelete(order).then(function () {
                    var parentEl = angular.element(document.querySelector('#tab-content'));
                    order.$remove(function () {

                        for (var i in $scope.orders) {
                            if ($scope.orders [i]._id === order._id) {
                                $scope.orders.splice(i, 1);
                            }
                        }
                        $mdToast.show(
                            $mdToast.simple()
                                .textContent('Successfully deleted Order')
                                .position('top right')
                                .theme('success-toast')
                                .hideDelay(6000)
                                .parent(parentEl)
                        );
                    }, function (errorResponse) {
                        $mdToast.show(
                            $mdToast.simple()
                                .textContent(errorResponse.data.message)
                                .position('top right')
                                .theme('error-toast')
                                .hideDelay(6000)
                                .parent(parentEl)
                        );


                        $scope.error = errorResponse.data.message;
                    });
                });
            }
        };

        $scope.getDisabledMode=function (order) {
            if($scope.showView) {
                return order && order.selectedPayMentOptions && order.selectedPayMentOptions.paymentDetails && order.selectedPayMentOptions.paymentDetails.status&& order.selectedPayMentOptions.paymentDetails.status === 'Paid';
            }else{
                return false;
            }
        };
        $scope.setPaymentTermDetails=function (order) {
            if (order.applicablePaymentTerm.paymentTerm && order.applicablePaymentTerm.paymentTerm._id) {
                order.applicablePaymentTerm.paymentDetails = order.selectedPayMentOptions;
                order.applicablePaymentTerm.paymentDetails.description = order.paymentComments;
                if (order.selectedPayMentOptions && order.selectedPayMentOptions.paymentMode && order.selectedPayMentOptions.paymentMode._id){
                    order.applicablePaymentTerm.paymentDetails.paymentMode = order.selectedPayMentOptions.paymentMode._id;
                }
                order.applicablePaymentTerm.name = order.applicablePaymentTerm.paymentTerm.name;
                order.applicablePaymentTerm.type = order.applicablePaymentTerm.paymentTerm.type;
                order.applicablePaymentTerm.paymentTerm = order.applicablePaymentTerm.paymentTerm._id;
            }
            return order;
        };
        $scope.updateOrderStatus= function (statusUpdate,status) {
            $scope.loadText='Updating Please Wait...';
            var order=$scope.selOrder;
            if(!this.currentStatus){
                this.currentStatus = status;
            }
            $scope.selectedTag=false;
            order.totalAmount=$scope.totalPriceFormatter();
            if(statusUpdate && this.currentStatus === 'Delivered' && $scope.selOrder.applicablePaymentTerm.type === 'PayOnDelivery'){
                $scope.myTabIndex=2;
            }else {
                if (statusUpdate) {
                    order = new Orders({_id: order._id});
                    order.currentStatus = this.currentStatus;
                    order.statusComments = this.statusComments;
                    if (order.currentStatus === 'Confirmed') {
                        order.deliveryDate = $scope.selOrder.deliveryDate;
                    }

                } else {

                    /*order.paymentMode=this.paymentMode;*/
                    order.singleamount = this.amount;

                    order.paymentComments = this.paymentComments;

                    if (order.applicablePaymentTerm.type === 'Installments') {
                        var index = order.applicablePaymentTerm.paymentDetails.indexOf($scope.selectedOptions);
                        order.applicablePaymentTerm.paymentDetails[index].amountPaid = order.applicablePaymentTerm.paymentDetails[index].amount;
                        order.applicablePaymentTerm.paymentDetails[index].status = 'Paid';
                    }
                    if (order.applicablePaymentTerm && order.applicablePaymentTerm.paymentTerm._id) {

                        order = $scope.setPaymentTermDetails(order);
                    }
                    order.applicablePaymentTerm.isPaymentUpdate = true;
                    /* order=OrderPayments.setSelected`PaymentType(order,true,$scope);*/
                    if (this.currentStatus === 'Delivered') {
                        order = new Orders({
                            _id: order._id,
                            applicablePaymentTerm: order.applicablePaymentTerm,
                            paymentComments: this.paymentComments,
                            paidAmount: order.paidAmount,
                            currentStatus:this.currentStatus,
                            statusComments:this.statusComments
                        });
                    }else {
                        order = new Orders({
                            _id: order._id,
                            applicablePaymentTerm: order.applicablePaymentTerm,
                            paymentComments: this.paymentComments,
                            paidAmount: order.paidAmount
                        });
                    }
                }
                order.$update(function () {
                    order.onchangestatus = false;
                    order.onchangespaymentstatus = false;
                    $scope.currentStatus = null;
                    $scope.selOrder = order;
                    $scope.clearFields();
                    $scope.selectTabs();
                    $scope.getOrdersSummary($scope.selectedIndex, $scope.busUnitId);
                    $scope.changeStatusOrder($scope.selOrder);
                    $mdToast.show(
                        $mdToast.simple()
                            .textContent('Order Status changed successfully')
                            .position('top right')
                            .theme('success-toast')
                            .hideDelay(3000)
                    );
                }, function (errorResponse) {
                    $scope.error = errorResponse.data.message;
                    if(!$scope.error){
                        $scope.error = 'Error while updating Order status';
                    }
                    var parentEl = angular.element(document.querySelector('#tab-content'));
                    $mdToast.show(
                        $mdToast.simple()
                            .textContent($scope.error)
                            .position('top right')
                            .theme('error-toast')
                            .hideDelay(6000)
                            .parent(parentEl)
                    );
                });
            }
        };
        function calculateAmount(paymentTermsList,order) {
            var paymentResult=null;
            angular.forEach(paymentTermsList,function (paymentTerm) {
                if(!paymentResult)
                    paymentResult=[];
                paymentTerm.amount=order.totalAmount *paymentTerm.percentage/100;
                paymentTerm.status='YetToPay';
                paymentResult.push(paymentTerm);
            });
            return paymentResult;
        }
        function calculateNonInstallmentAmount(paymentTerm,order) {

            /* delete paymentTerm._id;*/
            paymentTerm.amount=order.totalAmount;
            return paymentTerm;
        }
        $scope.getChangedPaymentDetails=function (order) {
            if(order) {
                if(order.applicablePaymentTerm)
                    $scope.currentPayment = order.applicablePaymentTerm.paymentTerm;
                $scope.selectedPayment = $scope.currentPayment;

                if ($scope.currentPayment && $scope.currentPayment.type === 'Installments') {
                    $scope.selectedPayment.paymentTerm = $scope.currentPayment;
                    order.applicablePaymentTerm = $scope.selectedPayment;
                    order.applicablePaymentTerm.installmentPaymentDetails = calculateAmount($scope.currentPayment.installments, order);
                } else {
                    /*order.applicablePaymentTerm.paymentTerm=$scope.selectedPayment._id;*/
                    if($scope.selectedPayment) {
                        order.applicablePaymentTerm.name = $scope.selectedPayment.name;
                        order.applicablePaymentTerm.type = $scope.selectedPayment.type;
                    }
                    if($scope.selectedPayment && $scope.selectedPayment._id)

                        order.applicablePaymentTerm.paymentDetails = calculateNonInstallmentAmount($scope.selectedPayment,order);
                    /*order.applicablePaymentTerm.paymentDetails = $scope.selectedPayment;*/

                }
                order.selectedPayMentOptions=$scope.selectedPayment;
            }
        };
        /*$scope.paymentModeSelected=function () {
            return  $scope.paymentMode && $scope.paymentMode instanceof Array &&  $scope.paymentMode.filter(function (paymentMode) {
                return paymentMode.isDisplay;
            });
        };*/
        $scope.updateAddress = function() {
            OrderAddress.addAddress($scope);

        };
        $scope.update = function() {
            var order = $scope.selOrder;
            order.totalAmount=$scope.totalPriceFormatter();
            /*            if(order.currentStatus==='Drafted') {

                            /!*selOrder.seller.contact._id*!/
                            if (order.buyer.contact && order.buyer.contact._id) {
                                var buyerContact = $scope.contacts.filter(function (contact) {
                                    return contact._id === order.buyer.contact._id;
                                });
                                if (buyerContact.length > 0 && buyerContact[0].nVipaniUser) {
                                    order.buyer.nVipaniUser = buyerContact[0].nVipaniUser;
                                    order.buyer.contact = buyerContact[0]._id;
                                }
                            }
                            if (order.seller && order.seller.contact._id) {
                                var sellerContact = $scope.contacts.filter(function (contact) {
                                    return contact._id === order.seller.contact._id;
                                });
                                if (sellerContact.length > 0 && sellerContact[0].nVipaniUser) {
                                    order.seller.nVipaniUser = sellerContact[0].nVipaniUser;
                                    order.seller.contact = sellerContact[0]._id;
                                }
                            }
                            if (order.mediator && order.mediator.contact._id) {
                                var mediatorContact = $scope.contacts.filter(function (contact) {
                                    return contact._id === order.mediator.contact._id;
                                });
                                if (mediatorContact.length > 0 && mediatorContact[0].nVipaniUser) {
                                    order.mediator.nVipaniUser = mediatorContact[0].nVipaniUser;
                                    order.mediator.contact = mediatorContact[0]._id;
                                }
                            }
                        }*/

            if($scope.selectedPageId > $scope.pages.length && $scope.payment){
                order.currentStatus=$scope.pages[3].currentStatus;
            }
            /*alert($scope.selectedPageId);*/
            if($scope.selectedPageId===1){
                order.currentStatus=$scope.pages[$scope.selectedPageId].currentStatus;
            }
            /*	if(order.currentStatus==='Drafted'){*/
            if($scope.selectedPayMentOptions){
                order.paymentOptionType=$scope.selectedPayMentOptions;
                /*if(order.selectedPayMentOptions.key===''){
                 }*/
            }

            if(order.selectedPayMentOptions && order.selectedPayMentOptions.key && order.currentStatus!=='Drafted'){
                order=OrderPayments.setSelectedPaymentType(order,false,$scope);
            }
            if(order.role==='Buyer') {
                $scope.buyer = {};
                if ($scope.selfcontact.length>0) {
                    order.buyer.contact = $scope.selfcontact[0]._id;
                    order.buyer.nVipaniUser = $scope.selfcontact[0].nVipaniUser;
                }
            }else if (angular.isObject(order.buyer) && order.buyer && order.buyer.contact!==undefined && order.buyer.contact!==''&& order.buyer.contact._id ) {
                if(order.buyer.contact.nVipaniUser){
                    order.buyer.nVipaniUser = order.buyer.contact.nVipaniUser;
                }
                order.buyer.contact = order.buyer.contact._id;
            }

            if(order.role==='Seller') {
                order.seller={};
                if ($scope.selfcontact.length>0) {
                    order.seller.contact = $scope.selfcontact[0]._id;
                    order.seller.nVipaniUser = $scope.selfcontact[0].nVipaniUser;
                }
            } else if (angular.isObject(order.seller) && order.seller && order.seller.contact && order.seller.contact !== undefined && order.seller.contact !== '' && order.seller.contact._id) {
                if(order.seller.contact.nVipaniUser){
                    order.seller.nVipaniUser = order.seller.contact.nVipaniUser;
                }
                order.seller.contact = order.seller.contact._id;
            }

            if(order.role==='Mediator') {
                order.mediator={};
                if ($scope.selfcontact.length>0) {
                    order.mediator.contact = $scope.selfcontact[0]._id;
                    order.mediator.nVipaniUser = $scope.selfcontact[0].nVipaniUser;
                }
            }else if (angular.isObject(order.mediator) && order.mediator && order.mediator.contact &&  order.mediator.contact !==undefined  && order.mediator.contact!=='' && order.mediator.contact._id && order.mediator.contact) {

                if(order.mediator.contact.nVipaniUser){
                    order.mediator.nVipaniUser = order.mediator.contact.nVipaniUser;
                }
                order.mediator.contact = order.mediator.contact._id;
            }
            /*}*/


            /*		if($scope.address.billing){
             /!*$scope.address.addressType='Billing';*!/
             $scope.billingContact=Contacts.get({
             contactId: order.seller.contact
             });

             $scope.billingContact.$promise.then(function (result1) {
             if (result1.addresses && $scope.findSingleAddress($scope.address,result1.addresses).length===0) {
             var billingAddress=angular.copy($scope.address);
             billingAddress.addressType='Billing';
             result1.addresses.push($scope.billingAddress);
             result1.$update(function (response) {
             order.billingAddress=billingAddress;
             }, function (errorContact1Response) {
             $scope.error = errorContact1Response.data.message;
             });
             }

             });


             }*/
            /*	if($scope.address && ($scope.address.shipping || $scope.address.shipping)){
             $scope.shippingContact= Contacts.get({
             contactId: order.buyer.contact
             });
             var ShippingAddress=null;
             var billingAddress=null;
             $scope.shippingContact.$promise.then(function (result2) {

             if($scope.address.shipping){
             ShippingAddress=angular.copy($scope.address);
             ShippingAddress.addressType='Shipping';
             }
             if($scope.address.billing){
             billingAddress=angular.copy($scope.address);
             billingAddress.addressType='Billing';
             }
             if ($scope.address.shipping && result2.addresses) {
             result2.addresses.push(ShippingAddress);
             /!*}else{
             order.shippingAddress = ShippingAddress;
             }*!/
             }
             if ($scope.address.billing && result2.addresses){
             //var oldAddress=$scope.findSingleAddress(billingAddress,result2.addresses);

             result2.addresses.push(billingAddress);
             /!*result2.$update(function (response) {
             order.billingAddress=billingAddress;
             }, function (errorcontact1Response) {
             $scope.error = errorcontact1Response.data.message;
             });*!/
             }
             result2.$update(function (response) {
             order.shippingAddress = ShippingAddress;
             order.billingAddress = billingAddress;
             }, function (errorcontactResponse) {
             $scope.error = errorcontactResponse.data.message;
             });
             });

             }*/



            order.$update(function(orderResponse) {
                /*$scope.order=orderResponse;*/

                /*orderResponse.$promise.then(function (order) {*/
                $scope.selOrder=orderResponse;

                if(order.shippingAddress.addressLine.length>0){
                    $scope.showShipping=false;
                    $scope.shipping=false;
                }else{
                    $scope.showShipping=true;
                    $scope.shipping=true;

                }
                if(order.billingAddress.addressLine.length>0){
                    $scope.showBilling=false;
                    $scope.billing=false;
                }else{
                    $scope.showBilling=true;
                    $scope.billing=true;

                }
                /*	$scope.orderUpdateFields();


                 });*/
                /*$scope.findOne();*/
                /*if($scope.order.currentStatus==='Drafted'){
                 $location.path('orders/' + $scope.order._id+'/edit');
                 }/!*else if($scope.order.currentStatus==='Placed') {
                 $location.path('orders/' + $scope.order._id + '/change');
                 }*!/else{*/

                $scope.selectTabs();

                $scope.panelChange($scope.selOrder,'view');
                /*$location.path('orders/' + $scope.order._id);*/
                /*}*/
            }, function(errorResponse) {
                $scope.error = errorResponse.data.message;
                /*});*/
            });
        };

        $scope.updatePaymentHistory=function() {
            var order = $scope.order;
            var history=order.paymentHistory;
            for(var i=0;i<$scope.paymentHistory.length;i++) {
                delete $scope.paymentHistory[i].date;
                history.push($scope.paymentHistory[i]);
                order.paymentStatus=$scope.paymentHistory[i].status;
                order.paymentMode=$scope.paymentHistory[i].paymentMode;
            }
            $scope.paymentHistory=[];
            //order.paymentHistory=history;
            order.$update(function(response) {
                $scope.paymentHistory=[];
                $location.path('orders' + response._id);
            }, function(errorResponse) {
                $scope.error = errorResponse.data.message;

            });
        };

        $scope.updateStatusHistory=function() {
            var order = $scope.order;
            var history=order.statusHistory;
            for(var i=0;i<$scope.statusHistory.length;i++) {
                delete $scope.statusHistory[i].date;
                order.currentStatus=$scope.statusHistory[i].status;
                history.push($scope.statusHistory[i]);
            }
            order.statusHistory=history;
            $scope.statusHistory=[];
            order.$update(function	(response) {
                $scope.order=response;
                $scope.statusHistory=[];
                $location.path('orders');
            }, function(errorResponse) {
                $scope.error = errorResponse.data.message;

            });
        };
        $scope.changeStatus = function(status){
            $scope.myTabIndex = 4;
            $scope.currentStatus = status;
        };
        $scope.findInit=function () {
            $scope.context = [
                {title:'Orders',link:'/#!/orders'}
            ];
            if($stateParams.orderId!==undefined){
                $scope.preOrder = Orders.get({
                    orderId: $stateParams.orderId
                });

                $scope.preOrder.$promise.then(function (result) {
                    if($scope.preOrder.owner.company===$scope.authentication.user.company || $scope.preOrder.buyer.nVipaniCompany===$scope.authentication.user.company){
                        $scope.selectedTab = 'MyOrders';
                        $scope.selectedIndex=0;
                    }else{
                        $scope.selectedTab = 'ReceivedOrders';
                        $scope.selectedIndex=1;
                    }
                    if($scope.preOrder.currentStatus!=='Drafted'){
                        $scope.getOrdersByBusinessUnit($scope.busUnitId);
                    }
                    $scope.findOne(true);
                });
            }else{
                $scope.viewAllOrders=true;
                /*OrderList.find($scope.busUnitId,true,$scope);*/
                $scope.getOrdersByBusinessUnit($scope.busUnitId);
            }

        };

        $scope.panelChange = function (order, changeStr) {
            $scope.context = [
                {title:'Orders',link:'/#!/orders'}
            ];
            $scope.showEdit = false;
            $scope.showAddOffer = false;
            $scope.showAddToGroups = false;
            $scope.showAddGroup = false;
            $scope.showEditGroup = false;
            $scope.showView = false;
            $scope.viewAllOrders=true;

            if (order !== null && angular.isObject(order)) {
                if (typeof changeStr === 'string' ) {
                    if (changeStr === 'edit' ) {
                        $scope.selOrder=angular.copy(order);
                        /*$scope.selOffer=angular.copy(offer);*/
                        $scope.showEdit = true;
                        /*$scope.findOne();*/
                    } else if (changeStr === 'add') {
                        $scope.showAddOrder = true;


                    }else if (changeStr === 'view') {
                        $scope.selOrder=angular.copy(order);
                        $scope.orderUpdateFields();
                        /*$scope.findOne();*/
                        if($scope.selOrder.currentStatus==='Drafted'){
                            $scope.viewAllOrders=false;
                            $scope.showEdit = true;
                            contactsQuery.findContacts($scope);
                            $location.path('orders/' + $scope.selOrder._id);
                            if($scope.selOrder.buyer && $scope.selOrder.buyer.contact && $scope.selOrder.seller && $scope.selOrder.seller.contact){
                                $scope.selectedPageId = 1;
                                if($scope.selOrder.billingAddress.pinCode!=='' && $scope.selOrder.shippingAddress.pinCode!==''){
                                    $scope.selectedPageId = 2;
                                }
                            }
                            $scope.context.push(
                                {title:$scope.selOrder.orderNumber, link:''}
                            );
                            /*$scope.contacts=[{ firstName: 'Add New Value', _id: -1 }].concat($scope.contacts);*/
                        }else{
                            $scope.myTabIndex = 0;
                            if($stateParams.message){
                                $scope.myTabIndex = 3;
                            }
                            $scope.viewAllOrders=true;
                            $scope.showView = true;
                            $mdDialog.show({
                                template: '<div class="md-dialog-container trowserView"><md-dialog class="trowser" aria-label="order view">' +
                                '<md-dialog-content>' +
                                '            <order-details data-ng-show="showView"></order-details>' +
                                '</md-dialog-content></md-dialog><div class="md-dialog-focus-trap" tabindex="0"></div></div>',
                                /* targetEvent: ev,*/
                                scope: $scope,
                                clickOutsideToClose: true,
                                preserveScope: true
                            });
                        }
                    }

                } else {
                    $scope.selOrder=angular.copy(order);
                    if($scope.selOrder){
                        $scope.showView = true;
                    }

                }
            } else {
                if (changeStr === 'add') {
                    $scope.showAddOrder = true;
                } else if(changeStr==='viewAll'){
                    $scope.viewAllOrders=true;
                }
            }
        };
        $scope.createContact=function (contact) {
            if(contact===-1)
                $mdDialog.show({
                    templateUrl: 'modules/contacts/views/contact/create-contact.client.view.html',
                    /* targetEvent: ev,*/
                    scope: $scope,
                    clickOutsideToClose: true,
                    fullscreen: true,
                    preserveScope: true,
                    controller: 'ContactsController'
                });

        };
        $scope.changeSelOrder = function (order) {
            $scope.selOrder = angular.copy(order);
            $scope.findOne(false);

        };
        $scope.changeStatusOrder = function (order) {
            $scope.selOrder = angular.copy(order);
            $scope.orderUpdateFields();
            $scope.tModes=[{value:'Rail',text:'Rail'},{value:'Other',text:'Other'}];
            $scope.tMode=[{value:'Truck',text:'Truck'},{value:'Rail',text:'Rail'},{value:'Air',text:'Air'},{value:'Ship',text:'Ship'},{value:'Other',text:'Other'}];
            $scope.tTypes=[{value:'Self',text:'Self'},{value:'Courier',text:'Courier'},{value:'Parcel',text:'Parcel'},{value:'Other',text:'Other'}];
            $scope.outOfStock=null;
            $scope.statusComments=null;
        };
        $scope.paymentDiscount=function (selOrder,isDiscount) {
            if(this.selOrder && this.selOrder.$resolved && this.selOrder.totalAmoun) {
                if (isDiscount) {
                    return (this.selOrder.totalAmount - OrderPayments.paymentDiscounts(this.selOrder)).toFixed('2');
                }else {
                    return this.selOrder.totalAmount.toFixed('2');
                }
            }else return 0.00;
        };
        $scope.checkUserRoleToChangeStatus=function (selOffer) {
            if(($scope.authentication.companyBusinessUnits.userGroup.name === 'Admin' || $scope.authentication.companyBusinessUnits.userGroup.name === 'Manager')){
                $scope.myTabIndex=4;
                $scope.changeStatusOrder(selOffer);
            }
        };
        // Find a list of Orders
        $scope.findOrderMessages = function (orderId) {
            $scope.orderMessages = OrderMessages.query({
                orderId: $stateParams.orderId?$stateParams.orderId :orderId
            });
        };
        $scope.findSpecific = function(tabSelection) {
            $scope.selectedTab = 'ReceivedOrders';
            $scope.selectedIndex=1;
            $scope.orders=[];
            $scope.selOrder=null;
            /*	$scope.orders=[];*/
            $http.get('ordersQuery').then(function (response) {
                $scope.orders = response.data;
                if($scope.orders.length>0) {
                    if(tabSelection){
                        $scope.findOne(tabSelection);
                    }else {
                        $scope.panelChange(null, 'viewAll');
                    }

                }
            });
        };

        $scope.findInvoice= function () {
            $http.get('ordersQuery1').then(function (response) {

                /*jsreport.render('<h1>Hello world</h1>').then(function (out) {
                 out.stream.pipe(response);*/
            });
        };

        $scope.getInsuffientStock=function (products) {
            $scope.insuffientProducts=products.filter(function (product) {
                return  product.numberOfUnits > product.inventory.numberOfUnits;
                /* $scope.errorInsuffientStock='';
                 $scope.errorInsuffientStock +='Insuffient Stock for product :\n'+ product.inventory.product.name+' ' + product.inventory.unitSize +'('+product.inventory.unitMeasure+')' +'Required :'+product.numberOfUnits +' Available :'+product.inventory.numberOfUnits;
           } */
            });
            $scope.outOfStock=$scope.insuffientProducts.length>0?true:false;
            return $scope.outOfStock;
        };

        $scope.getOutStockLabel=function (product) {
            if(product.inventory)
                return product.numberOfUnits > product.inventory.numberOfUnits;
        };
        $scope.expandAddress=function(typeValue){
            if(typeValue==='Billing'){
                $scope.showBilling=!$scope.showBilling;
                if($scope.showBilling===true){
                    $scope.billing = false;
                }else{
                    $scope.billing = true;
                }
            }
            if(typeValue==='Shipping'){
                $scope.showShipping=!$scope.showShipping;
                if($scope.showShipping===true){
                    $scope.shipping = false;
                }else{
                    $scope.shipping = true;
                }
            }
        };
        $scope.paymentClick=function(index){
            $scope.selectedOptions=$scope.selOrder.applicablePaymentTerm.installmentPaymentDetails[index];
            $scope.selectedTag=true;
            $scope.selectedPaymentIndex=index;

        };
        $scope.paymentChanges=function(key,singleKey){
            if(singleKey){
                $scope.selectedTag=singleKey;
            }
        };
        $scope.paymentChange=function(key){
            OrderPayments.setSelectedPayments(key,$scope);
        };

        // Find existing Order
        $scope.findOne = function(selOrderId) {
            var orderId=null;
            $scope.selectedPageId=0;
            $scope.addresspage=false;
            $scope.payment=false;
            $scope.view=true;
            // $scope.selOrder=null;
            if(selOrderId && $stateParams.orderId){
                orderId=$stateParams.orderId;
            }/*else if(selOrderId) {
             orderId=null;
             }*/else if($scope.selOrder) {
                orderId=$scope.selOrder._id;
            }
            if(orderId){
                $scope.selOrder = Orders.get({
                    orderId: orderId
                });
                $scope.selOrder.$promise.then(function (result) {
                    $scope.selOrder=result;
                    $scope.selOrder.onchangestatus=false;
                    $scope.selOrder.onchangespaymentstatus=false;
                    $scope.selOrder.totalAmountWithoutDiscount = $scope.getTotalAmountWithoutDiscount();
                    $scope.selOrder.totalAmount = $scope.totalPriceFormatter();
                    if($scope.selOrder){
                        $scope.singleBilling=result.shippingAddress.sameAsBilling;

                        $scope.findOrderMessages($scope.selOrder._id);

                        if($scope.selOrder.shippingAddress.addressLine.length>0){
                            $scope.showShipping=false;
                            $scope.shipping=false;
                            /*	if($scope.order.seller.addresses.length>0){
                             $scope.shipping=false;
                             }else{
                             $scope.shipping=true;
                             }*/
                        }else{
                            $scope.showShipping=true;
                            $scope.shipping=true;

                        }
                        if($scope.selOrder.billingAddress.addressLine.length>0 ||$scope.selOrder.shippingAddress.sameAsBilling){
                            $scope.showBilling=false;
                            $scope.billing = false;

                        }else{
                            $scope.showBilling=true;

                            $scope.billing=true;
                        }


                        $scope.selectTabs();
                        $scope.panelChange($scope.selOrder,'view');
                    }
                });
            }else{
                $location.path('orders');
            }
        };
        $scope.getCompanyBusinessUnits=function () {
            CompanyService.getCompanyBusinessUnits().then(function (response) {
                $scope.companyBusinessunits=response.data;
            },function (err) {
                $scope.error=err.data.message;
            });
        };
        $scope.filterAddressLines = function (typedValue) {
            return OrderAddress.filterAddressLines(typedValue,$scope);
        };
        $scope.getMyOrderIndex=function (orderTabType) {
            return $scope.tabs[$scope.selectedIndex].type===orderTabType;
        };

        $scope.getInvoiceFilePath=function () {
            if($scope.selOrder) {
                $scope.selectedKey = OrderPayments.getSelectedPaymentOption($scope.selOrder.paymentOptionType);
                if ($scope.selectedKey && $scope.selectedKey.key) {
                    if ($scope.selectedKey.key === 'installments') {
                        return $scope.selectedKey.options[0].invoiceFile;
                    } else {
                        return $scope.selectedKey.invoiceFile;
                    }
                } else return null;
            }else return null;

        };

        $scope.addPaymentHistory = function () {
            $scope.paymentHistory.push({
                status: '',
                amount: '',
                date: '',
                paymentMode: '',
                user:$scope.authentication.user._id,
                displayName:$scope.authentication.user.displayName
            });
        };
        $scope.addStatusHistory = function () {
            $scope.statusHistory.push({
                status: '',
                date: '',
                user:$scope.authentication.user._id,
                displayName:$scope.authentication.user.displayName
            });
        };
        $scope.selectProduct = function (metaProduct, index) {
            $scope.products[index].product = metaProduct;
            $scope.products[index].unitSize = metaProduct.unitSize;
            $scope.products[index].unitMeasure = metaProduct.unitMeasure;
            $scope.products[index].unitPrice = metaProduct.unitPrice;
        };
        $scope.maxProductUnits=function (product,isBuy) {
            /* if(product.inventory.moqAndSalePrice)*/
            if(product)
                return inventoryProduct.getMaxQuantity(product,isBuy);
            else return 0;
        };
        $scope.getMoqArray=function (singleproduct) {
            if(singleproduct.inventory)
                return (!$scope.selOrder.offer || $scope.selOrder.offer.offerType==='Sell')? singleproduct.inventory.moqAndMargin:singleproduct.inventory.moqAndBuyMargin;
            else return [];
        };
        $scope.changeProductPrice=function (singleproduct,index) {
            if(singleproduct.numberOfUnits) {
                var price = inventoryProduct.getQuantityPrice($scope.getMoqArray(singleproduct), singleproduct.numberOfUnits, singleproduct.inventory.MRP, index);
                var suitableMin = inventoryProduct.getMinQuantityPrice($scope.getMoqArray(singleproduct), singleproduct.numberOfUnits);
                $scope.selOrder.products[index].MRP = price;
                $scope.selOrder.products[index].unitMrpPrice = $scope.selOrder.products[index].inventory.MRP;
                /*$scope.selOrder.products[index].margin=(singleproduct.inventory.MRP -$scope.selOrder.products[index].unitPrice)*100/singleproduct.inventory.MRP;*/
                if(suitableMin){
                    $scope.selOrder.products[index].margin = suitableMin.margin;
                    $scope.selOrder.products[index].MOQ = suitableMin.MOQ;
                }
                $scope.selOrder.totalAmountWithoutDiscount = $scope.getTotalAmountWithoutDiscount();
                $scope.selOrder.totalAmount = $scope.totalPriceFormatter();
            }
        };
        $scope.getMinMOQ = function(singleproduct){
            var moq=$scope.getMoqArray(singleproduct);
            if(moq && moq.length>0){
                return inventoryProduct.getMinMOQ($scope.getMoqArray(singleproduct));
            }else {
                return inventoryProduct.numberOfUnits>0 ?1 :0;
            }
        };
        $scope.changeProductUnitMeasure = function (index,each) {


            $scope.products[index].unitMeasure=each.$data;
            /*$scope.apply();*/
        };
        $scope.changeProductUnitSize = function (index,each) {

            //totalPrice = numberOfUnits * unitPrice
            $scope.products[index].unitSize=each.$data;
            $scope.products[index].totalQuantity = $scope.products[index].unitSize * $scope.products[index].numberOfUnits;
            $scope.products[index].totalPrice = $scope.products[index].unitPrice * $scope.products[index].numberOfUnits;
            /*$scope.product.totalQuantity=$scope.products[index].totalQuantity;*/

        };
        $scope.changeProductSampleNumber= function (index,each) {

            $scope.products[index].sampleNumber=each.$data;

        };
        $scope.changeProductUnitPrice = function (index,each) {
            $scope.products[index].unitPrice=each.$data;
            $scope.products[index].totalPrice = $scope.products[index].unitPrice * $scope.products[index].numberOfUnits;

        };

        $scope.changeProductNumberOfUnits = function (index,each) {
            $scope.products[index].numberOfUnits=each.$data;
            $scope.products[index].totalQuantity = $scope.products[index].unitSize * $scope.products[index].numberOfUnits;
            $scope.products[index].totalPrice = $scope.products[index].unitPrice * $scope.products[index].numberOfUnits;
        };

        $scope.calculateTotalQuantity = function(index) {
            var quantity = $scope.products[index].totalQuantity;
            return (isNaN(quantity)) ? 0 : quantity;
        };

        $scope.calculateTotalPrice = function(index) {
            var amount = $scope.products[index].totalPrice;
            return (isNaN(amount)) ? 0 : amount;
        };

        $scope.displayCalculateTotalQuantity = function(product) {
            var quantity = product.totalQuantity;
            return (isNaN(quantity)) ? 0 : quantity;
        };

        $scope.calculateTotalPrice = function(index) {
            var amount = $scope.products[index].totalPrice;
            return (isNaN(amount)) ? 0 : amount;
        };
        $scope.clearPaymentData = function(){
            /*$scope.amount=order.totalPrice-order.paidAmount;*/
            $scope.paymentStatus='';
            $scope.paymentComments='';
            $scope.paymentMode='';
            //$scope.amount=$scope.order.totalPrice-$scope.order.paidAmount;
        };

        $scope.clearStatusData = function(){
            //$scope.onchangespaymentstatus=true;
            $scope.statusComments='';
            $scope.currentStatus='';
        };

        $scope.filterByName = function (contacts, typedValue) {
            return contacts.filter(function (contact) {
                var matches_firstvsLastName = angular.lowercase(contact.firstName +' '+contact.lastName).indexOf(angular.lowercase(typedValue)) !== -1;

                return !contact.disabled && matches_firstvsLastName;
            });
        };

        $scope.filterByProductName=function (products, typedValue) {
            return products.filter(function (product) {
                var matches_name = angular.lowercase(product.name).indexOf(angular.lowercase(typedValue)) !== -1;
                return matches_name ;
            });
        };
        $scope.addProduct = function () {
            $scope.products.push({
                product: '',
                sampleNumber: '',
                unitSize: '',
                unitMeasure: '',
                numberOfUnits: '',
                unitPrice: '',
                totalQuantity: '',
                totalPrice: ''
            });
        };
        $scope.updateProducts = function (order) {
            $scope.order.products.push({
                product: '',
                sampleNumber: '',
                unitSize: '',
                unitMeasure: '',
                numberOfUnits: '',
                unitPrice: '',
                totalQuantity: '',
                totalPrice: ''
            });
            $scope.products=$scope.order.products;
        };

        $scope.updateProduct = function (order) {
            $scope.products=$scope.order.products;
        };

        $scope.showModal = false;
        $scope.toggleModal = function(){
            $scope.showModal = !$scope.showModal;
        };

        $scope.showModal2 = false;
        $scope.toggleModal2 = function(){
            $scope.showModal2 = !$scope.showModal2;
        };

        $scope.filterOrder = function (item) {
            var re = new RegExp($scope.searchText, 'i');
            return !$scope.searchText || re.test(item.currentStatus) || re.test(item.orderNumber);
        };

        $scope.toggleSideNavigation = function (id,val) {

            if(val === ''){
                $timeout(function () {
                    panelTrigger(id).open().then(function () {
                        /*  console.log('Trigger is done');*/
                    });
                });
            }else{
                val=val;
            }
            if (val === 'toggle') {
                panelTrigger(id).toggle().then(function () {
                    /* console.log('Trigger is done');*/
                });
            }else if (val === 'open') {
                panelTrigger(id).open().then(function () {
                    /* console.log('Trigger is done');*/
                });
            }else if (val === 'close') {
                panelTrigger(id).close().then(function () {
                    /* console.log('Trigger is done');*/
                });
            }else if (val === 'isOpen') {
                panelTrigger(id).isOpen().then(function () {
                    /*  console.log('Trigger is done');*/
                });
            }
        };

    }
]);
app.controller('OrdersViewController', []);
app.controller('OrdersEditController', []);

app.directive('mdDatepicker', function($mdDateLocale) {
    return {
        require: '?ngModel',
        restrict: 'E',
        link: function ($scope, $element, $attrs, ngModelController) {
            var inputType = angular.lowercase($attrs.type);

            if (!ngModelController || inputType === 'radio' ||
                inputType === 'checkbox') {
                return;
            }

            ngModelController.$formatters.unshift(function (value) {
                if (!ngModelController.$invalid && !angular.isUndefined(value) && typeof ngModelController.$modelValue === 'string') {
                    return $mdDateLocale.parseDate(ngModelController.$modelValue);
                } else {
                    return value;
                }
            });
        }
    };
});

app.directive('addStatusHistory', function(){
    return {
        restrict:'E',
        templateUrl:'modules/orders/views/create-ordersstatus.client.view.html'
    };
});

app.directive('addPaymentHistory', function(){
    return {
        restrict:'E',
        templateUrl:'modules/orders/views/create-orderpaymenthistory.client.view.html'
    };
});
app.directive('updateOrder', function(){
    return {
        restrict:'E',
        templateUrl:'modules/orders/views/edit-order.client.view.html'
    };
});

app.directive('viewOrder', function(){
    return {
        restrict:'E',
        templateUrl:'modules/orders/views/view-order.client.viewnew.html'
    };
});
app.directive('checkOut', function(){
    return {
        restrict:'E',
        templateUrl:'modules/orders/views/checkout-order.client.view.html'
    };
});
app.directive('addressOrder', function(){
    return {
        restrict:'E',
        templateUrl:'modules/orders/views/address-order.client.view.html'
    };
});
app.directive('paymentOrder', function(){
    return {
        restrict:'E',
        templateUrl:'modules/orders/views/payment-order.client.view.html'
    };
});
app.directive('orderDetails', function(){
    return {
        restrict:'E',
        templateUrl:'modules/orders/views/view-details-order.client.view.html'
    };
});
app.directive('paymentOrderView', function(){
    return {
        restrict:'E',
        templateUrl:'modules/orders/views/payment-view-order.client.view.html'
    };
});
app.directive('orderStatusView', function(){
    return{
        restrict: 'E',
        templateUrl: 'modules/orders/views/order-status-dialog.html'
    };
});
app.directive('isNumber', function () {
    return {
        require: 'ngModel',
        restrict: 'A',
        link: function (scope, element, attr, ctrl) {
            function inputValue(val) {
                if (val) {
                    var digits = val.replace(/[^0-9.]/g, '');

                    if (digits !== val) {
                        ctrl.$setViewValue(digits);
                        ctrl.$render();
                    }
                    return parseFloat(digits);
                }
                return undefined;
            }
            ctrl.$parsers.push(inputValue);
        }
    };
});

app.directive('radioTrackBy', function(){
    return {
        restrict: 'A',
        scope: {
            ngModel: '=',
            ngValue: '='
        },
        link: function (ng) {
            if (ng.ngModel && ng.ngValue.addressLine ===ng.ngModel.addressLine && ng.ngValue.pinCode === ng.ngModel.pinCode && ng.ngValue.city === ng.ngModel.city && ng.ngValue.country === ng.ngModel.country && ng.ngValue.state === ng.ngModel.state) {
                ng.ngModel = ng.ngValue;
            }


        }
    };
});

app.directive('paymentOptions', function(){
    return {
        restrict: 'A',
        scope: {
            ngModel: '=',
            ngValue: '='

        },
        link: function (scope,ng) {
            /*if (ng.ngValue.selection ===ng.ngModel.selection &&  ng.ngValue.enabled === ng.ngModel.enabled) {
             ng.ngModel = ng.ngValue;
             }*/
            if(ng.ngModel.selection===true){
                ng.ngModel=ng.ngValue;
            }


        }
    };
});

app.directive('emailValidation', function () {
    return {
        require: 'ngModel',
        restrict: 'A',
        link: function (scope, element, attr, ctrl) {
            function inputValue(val) {
                if (val) {
                    var digits = val.replace(/[^0-9.]/g, '');

                    if (digits !== val) {
                        ctrl.$setViewValue(digits);
                        ctrl.$render();

                    }
                    //ctrl.$__error("Valid");
                    return parseFloat(digits);
                }
                //ctrl.$__error("Valid");
                return undefined;
            }
            ctrl.$parsers.push(inputValue);
        }
    };
});
app.directive('paymentmodeOptions',function () {
    return {
        restrict: 'A',
        link: function (scope, element, attr, ctrl) {

        }
    };
});
app.directive('slideable', function () {
    return {
        restrict: 'C',
        compile: function (element, attr) {
            // wrap tag
            var contents = element.html();
            element.html('<div class="slideable_content" style="margin:0 !important; padding:0 !important" >' + contents + '</div>');

            return function postLink(scope, element, attrs) {
                // default properties
                attrs.duration = (!attrs.duration) ? '1s' : attrs.duration;
                attrs.easing = (!attrs.easing) ? 'ease-in-out' : attrs.easing;
                element.css({
                    'overflow': 'hidden',
                    'height': '0px',
                    'transitionProperty': 'height',
                    'transitionDuration': attrs.duration,
                    'transitionTimingFunction': attrs.easing
                });
            };
        }
    };
})
    .directive('slideToggle', function () {
        return {
            restrict: 'A',
            link: function (scope, element, attrs) {
                var target = document.querySelector(attrs.slideToggle);
                attrs.expanded = false;
                element.bind('click', function () {
                    var content = target.querySelector('.slideable_content');
                    if (!attrs.expanded) {
                        content.style.border = '1px solid rgba(0,0,0,0)';
                        var y = content.clientHeight;
                        content.style.border = 0;
                        target.style.height = y + 'px';
                    } else {
                        target.style.height = '0px';
                    }
                    attrs.expanded = !attrs.expanded;
                });
            }
        };
    });

app.directive('nvMax', function () {
    return {
        restrict: 'A',
        require: 'ngModel',
        link: function (scope, element, attributes, ngModelController) {
            var otherValue;

            scope.$watch(attributes.nvMax, function (value) {
                otherValue = value;

                ngModelController.$validate();
            });

            ngModelController.$parsers.unshift(function (viewValue) {
                ngModelController.$setValidity('nvMax', !viewValue || !otherValue || parseFloat(viewValue) <= parseFloat(otherValue));

                return viewValue;
            });
        }
    };
});
app.directive('nvMin', function () {
    return {
        restrict: 'A',
        require: 'ngModel',
        link: function (scope, element, attributes, ngModelController) {
            var otherValue;

            scope.$watch(attributes.nvMin, function (value) {
                otherValue = value;

                ngModelController.$validate();
            });

            ngModelController.$parsers.unshift(function (viewValue) {
                ngModelController.$setValidity('nvMin', !viewValue || !otherValue || parseFloat(viewValue) >= parseFloat(otherValue));

                return viewValue;
            });
        }
    };
});
app.directive('players',function () {
    return {
        restrict: 'AE',
        scope: {
            order: '='

        },
        template: '<div layout="row" layout-align="start center"><h6>{{$parent.getPlayerText(order,true,true)+"&nbsp;&nbsp; "}}</h6>' +
        '<label class="bb-truncate">{{$parent.getPlayerText(order,true,false)}}<md-tooltip>{{$parent.getPlayerText(order,true,false)}}</md-tooltip></label></div>' +
        '<div layout="row" layout-align="start center"><h6 ng-show="order.mediator.contact">Mediator:&nbsp;</h6><label class="nv-text-bold bb-truncate" ng-show="order.mediator.contact">{{order.mediator.contact.displayName?order.mediator.contact.displayName:order.mediator.contact.firstName+\'&nbsp;\'+order.mediator.contact.lastName}}<span>&nbsp;|&nbsp;</span></label>' +
        '<h6 ng-show="order.deliveryDate">Exp. Delivery:&nbsp;</h6><label>{{order.deliveryDate | date:"dd-MM-yyyy"}}</label></div>' +
        '<div layout="row" layout-align="start center"><h6>{{$parent.getPlayerText(order,false,true)+"&nbsp;&nbsp; "}}</h6><label class="bb-truncate">{{$parent.getPlayerText(order,false,false)}}<md-tooltip>{{$parent.getPlayerText(order,false,false)}}</md-tooltip></label></div> '
    };


});
app.directive('productImage', function($compile){
    return{
        restrict: 'E',
        scope: {
            images: '=images',
            id: '=id',
            isSquare: '=shape'
        },
        template: '<div class="bb-wid-100 "><div class="carousel slide nvc-order-carousel carousel-hover" id="order-image-carousel-{{id}}" data-ride="carousel" data-interval="false" ng-mouseenter="showPop(id)" ng-mouseleave="hidePop(id)">' +
        '<ol class="carousel-indicators">' +
        '<li ng-repeat="image in images track by $index" data-target="#order-image-carousel-{{id}}" data-slide-to="{{$index}}" ng-class="{ \'active \':$index===0}"></li></ol>' +
        '<div class="carousel-inner">' +
        '<div class="item bb-height-100" ng-repeat="image in images track by $index" ng-class="{ \'active \':$index===0}" >' +
        '<img ng-src="{{image}}" alt="image-{{$index}}" class=" bb-wid-100 bb-height-100" ng-class="isSquare===\'square\'?\'\':\'nvc-shape-pill\'" square-image ng-click="openImage($index)" ></div></div>' +
        //'<a href="#order-image-carousel-{{id}}" class="left carousel-control" data-slide="prev"><span class="fa fa-chevron-left"></span></a>' +
        //'<a href="#order-image-carousel-{{id}}" class="right carousel-control" data-slide="next"><span class="fa fa-chevron-right"></span></a>' +
        '</div>' +
        '' +
        '</div>',
        controller: function ($scope) {
            $scope.showPop = function (id) {
                var elem = angular.element(document.querySelector('#order-image-carousel-'+id));
                /*elem.addClass('carousel-hover');*/ /*--- can add this to have an animation effect on hover ----*/
                angular.element(elem[0]).carousel({
                    interval:1000,
                    pause:null
                });
            };
            $scope.hidePop = function (id) {
                var elem = angular.element(document.querySelector('#order-image-carousel-'+id));
                /*elem.removeClass('carousel-hover');*/
                angular.element(elem[0]).carousel('pause');
            };
            $scope.openImage = function (index) {
                var elem = angular.element(document.querySelector('body'));
                var galleryElem = $compile('<div class="nvc-image-gallery"><image-gallery images="images" index="' + index + '"></image-gallery></div>');
                elem.append(galleryElem($scope));
                /*if(elem[0].style.top) {
                    var top = Number(elem[0].style.top.replace('px', ''));
                    angular.element(document.querySelector('.nvc-image-gallery'))[0].style.top = -top + 'px';
                }
                galleryElem[0].addEventListener('touchmove', function(e) {
                    e.preventDefault();
                }, false);*/
            };
        }
    };
});
app.directive('imageGallery', function () {
    return{
        restrict: 'E',
        scope: {
            images: '=images',
            index: '=index',
        },
        templateUrl: 'modules/core/views/nvc-image-gallery.client.view.html',
        controller: function ($scope) {
            $scope.closeImage = function () {
                var elem = angular.element(document.querySelector('.nvc-image-gallery'));
                elem[0].remove();
                var el2 = angular.element(document.querySelector('body'));
                el2[0].style.overflowY = 'auto';
            };
            document.onkeydown = function(evt) {
                evt = evt || window.event;
                if (evt.keyCode === 27) {
                    $scope.closeImage();
                }
            };

        }
    };
});
app.directive('squareImage', function ($compile) {
    return{
        restrict: 'A',
        link: function (scope, element) {
            angular.element(element).addClass('nvc-square-box-content');
            angular.element(element)[0].removeAttribute('square-image');
            var elem2 = element[0].outerHTML;
            var el = '<div class="nvc-square-box">'+elem2+'</div>';
            el = $compile(el);
            element.replaceWith(el(scope));
        }
    };
});
