'use strict';

// Companies controller
angular.module('companies').controller('CompaniesController', ['$scope', '$stateParams', '$location', 'Authentication', 'Companies','uuid','deviceDetector','LoginService','inventoryProduct','$http',
    function ($scope, $stateParams, $location, Authentication, Companies,deviceDetector,uuid,LoginService,inventoryProduct,$http) {
        $scope.authentication = Authentication;
        $scope.credentials={username:'',password:''};
        $scope.employeePassword='';
$scope.contactExists=false;
        // Create new Company
        $scope.create = function () {
            // Create new Company object
            var company = new Companies({
                name: this.name
            });

            // Redirect after save
            company.$save(function (response) {
                $location.path('companies/' + response._id);

                // Clear form fields
                $scope.name = '';
            }, function (errorResponse) {
                $scope.error = errorResponse.data.message;
            });
        };

        // Remove existing Company
        $scope.remove = function (company) {
            if (company) {
                company.$remove();

                for (var i in $scope.companies) {
                    if ($scope.companies [i] === company) {
                        $scope.companies.splice(i, 1);
                    }
                }
            } else {
                $scope.company.$remove(function () {
                    $location.path('companies');
                });
            }
        };

        $scope.getBusinessCategories=function (company) {
            var categories=[];
            if(this.company){
               if(this.company.category.seller){
                   categories.push('Seller');
               }
               if(this.company.category.buyer){
                   categories.push('Buyer');
               }
               if(this.company.category.mediator){
                   categories.push('Mediator');
               }
            }
            if(categories.length>0){
                return categories.toString();
            }
        };


        $scope.getPrice=function(offer,singleproduct){
            return $scope.getQuantityPrice(((offer.offerType==='Sell')? singleproduct.inventory.moqAndMargin:singleproduct.inventory.moqAndBuyMargin),singleproduct.MOQ,singleproduct.inventory.unitPrice);

        };
        /*$scope.changeProductPrice=function (singleproduct,index) {
            var price=$scope.getQuantityPrice(((!$scope.selOffer || $scope.selOffer.offerType==='Sell')? singleproduct.inventory.moqAndSalePrice:singleproduct.inventory.moqAndBuyPrice),singleproduct.MOQ,singleproduct.inventory.unitPrice);
            $scope.selOffer.products[index].unitIndicativePrice=price;
            $scope.selOffer.products[index].margin=(singleproduct.unitPrice-price/singleproduct.unitPrice)*100;
        };*/
        $scope.getQuantityPrice = function (moqVsPrice, quantity,unitPrice) {
            if (!quantity || !moqVsPrice ||  moqVsPrice.length === 0) {
                return unitPrice;
            }
            else if (moqVsPrice &&  moqVsPrice.length === 1) {
                return moqVsPrice[0].price;
            } else {
                var price = moqVsPrice[0].price;
                var suitableMOQ = moqVsPrice[0].MOQ;
                for (var i = 0; i < moqVsPrice.length; i++) {
                    var moq = moqVsPrice[i].MOQ;
                    if (moq === quantity) {
                        price = moqVsPrice[i].price;
                        suitableMOQ = moq;
                    } else if (moq < quantity && suitableMOQ < moq && suitableMOQ < quantity) {
                        suitableMOQ = moq;
                        price = moqVsPrice[i].price;
                    }
                }

                return price;
            }
        };
        //Create Contacts for the company

        $scope.createCompanyContacts=function (company) {


            $http.put('createContacts/' + company._id).then(function (response) {
                /*if (response._id) {
                    $location.path('orders' + '/' + response._id);
                } else {
                    $location.path('orders');
                }*/
            }, function (response) {
                $scope.error = response.data.message;
            });

        };

        // Update existing Company
        // Company Contact is present in the current organization.
      /*  $scope.getCompanyContactPresent=function (company){

        };*/

        $scope.update = function () {
            var company = $scope.company;

            company.$update(function () {
                $location.path('companies/' + company._id);
            }, function (errorResponse) {
                $scope.error = errorResponse.data.message;
            });
        };
        $scope.createOrder = function (categoryOffer) {
            var offer = categoryOffer ? categoryOffer : $scope.selOffer;
            $http.put('createOrder/' + offer._id, offer).then(function (response) {
                if (response.data._id) {
                    $location.path('orders' + '/' + response.data._id);
                } else {
                    $location.path('orders');
                }
            }, function (response) {
                $scope.error = response.data.message;
            });
        };

        $scope.orderProductCount=function(selOffer){
            $scope.selectedproducts=[];
            if(selOffer){
                $scope.selectedproducts =selOffer.products.filter(function (product) {
                    return product.selected ;
                });
                return $scope.selectedproducts.length <= 0;

            }
            if($scope.selectedproducts.length>1){
                return false;
            }else{
                return true;
            }
        };
        $scope.showCategory = function(category){
            $scope.companyInventories = category.inventories;
            $location.hash('companyInventoryTab');
        };
        $scope.getCategoryInventories = function(id,done){
            inventoryProduct.inventorySelectionQuery({inventories:[{subCategory2:id}],businessUnit:$scope.defaultBusinessUnitId}).then(function (response) {
                done(response.data.inventories);
            });
        };
        // Find a list of Companies
        $scope.find = function () {
            $scope.companies = Companies.query();
        };

        // Find existing Company
        $scope.findOne = function () {
            var body = angular.element(document.querySelector('.content-push'));
            angular.element(body).removeClass('content-push');
            $scope.findOneCompany();
        };
        $scope.findOneCompany = function () {
           /* $scope.company = Companies.get({
                name: $stateParams.name
            });*/
            $http.get('companyPublicPage?companyProfileUrl=' + $stateParams.companyName+'&bunit='+($scope.defaultBusinessUnitId?$scope.defaultBusinessUnitId:'')).then(function (response) {
                $scope.companyOffers = response.data.offers;
                $scope.company = response.data.company;
                if($scope.defaultBusinessUnitId) {
                    $scope.businessUnit = response.data.company.businessUnits.filter(function (bunit) {
                        return bunit.businessUnit._id === $scope.defaultBusinessUnitId;
                    })[0].businessUnit;
                }else{
                    $scope.businessUnit = $scope.company.businessUnits[0].businessUnit;
                    $scope.defaultBusinessUnitId = $scope.company.businessUnits[0].businessUnit._id;
                }
                $scope.companyCategories = response.data.products;
                angular.forEach($scope.companyCategories, function (category) {
                    $scope.getCategoryInventories(category._id, function (inventories) {
                        category.inventories = inventories;
                    });
                });
               if($scope.authentication && $scope.authentication.token && $scope.authentication.token.length!==0)
                   $http.get('contactscheck/' + $scope.company._id).then(function (response) {
                       if (response.data.contactExists) {
                           $scope.contactExists = response.data.contactExists;
                   }else{
                       $scope.contactExists=false;
                   }
               });

            });
        };

        $scope.getEmployeeDetails=function () {
            $http.get('/auth/employeeDetailsForActivate/'+$stateParams.statusToken).then(function (res) {
                var employee=res.data;
                $scope.credentials.username=employee.username;
                $scope.employeeCompanyName=employee.company.name;
            },function (err) {
                $scope.error = err.data.message;
            });
        };
        $scope.businessUserActivation=function () {
            $http.post('/auth/activateUser/'+$stateParams.statusToken,$scope.credentials).then(function (res) {
                if(res.data) {
                    LoginService.login($scope);
                }else{
                    $scope.error = 'Activation failed';
                }
            },function (err) {
                $scope.error = err.data.message;
            });
        };
    }
]);
