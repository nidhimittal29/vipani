'use strict';

//Companies service used to communicate Companies REST endpoints
var app=angular.module('companies');
app.factory('Companies', ['$resource',
    function ($resource) {
        return $resource('companies/:companyId', {
            companyId: '@_id'
        }, {
            update: {
                method: 'PUT'
            }
        });
    }
]);
app.factory('CompanyService',['$http','businessUserGroupActionQueries','businessUnitGroupActionQueries',function ($http,businessUserGroupActionQueries,businessUnitGroupActionQueries) {
    function getSelectedEmployee(employees){
        var selectedEmployees=[];
        angular.forEach(employees,function (eachEmployee) {
            if(eachEmployee.selected && eachEmployee.user.userType !== 'User'){
                selectedEmployees.push(eachEmployee.user._id);
            }
        });

        return selectedEmployees;
    }
    function getSelectedBusinessUnits(scope){
        var selectedBus={'businessUnits':[]};
        angular.forEach(scope.company.businessUnits,function (businessUnit) {
            if(businessUnit.selected && !businessUnit.defaultBusinessUnit){
                selectedBus.businessUnits.push({_id:businessUnit.businessUnit._id});
            }
        });
        return selectedBus;
    }
    function transformChip(chip) {
        // If it is an object, it's already a known chip
        if (angular.isObject(chip)) {
            chip.isChecked = true;
            return chip;
        } else {
            return null;
        }
    }
    function createFilterFn(query) {
        var lowercaseQuery = angular.lowercase(query);

        return function filterFn(category) {
            return (category.user._lowerusername && category.user._lowerusername.indexOf(lowercaseQuery) >= 0);
        };


    }
    function userMap(companyUsers,scope) {

        companyUsers.filter(function (eachUser) {
            if(eachUser.user.username) {
                eachUser.user._lowerusername = eachUser.user.username.toLowerCase();
                scope.selectedUser.push(eachUser);
            }
        });
    }
    return {
        getSelectedEmployeeCount:function (employees) {
            return getSelectedEmployee(employees).length>0 ?true:false;

        },
        getSelectedBusinessUnitCount:function (scope,isCount) {
            if(isCount)
                return getSelectedBusinessUnits(scope).businessUnits.length>0?true:false;
            else
                return getSelectedBusinessUnits(scope).businessUnits;
        },
        addUser: function (data) {
            return $http.post('/auth/adduser',data);
        },
        addMultipleUserToUnit:function (data) {
            return $http.post('/auth/addMassUsersToUnit',data);
        },
        createBusinessUnit: function (data) {
            return $http.post('/branches',data);
        },
        updateUser: function (data,isValue) {
            var updateUser={'company':data.company,'group':(data.userGroup && data.userGroup._id?data.userGroup._id:''),user:(data.user&&data.user._id?data.user._id:''),employeeId:data._id};
            if(isValue===1){
                updateUser.isGroup=true;
            }else if(isValue===2) {
                updateUser.isActive=true;
                updateUser.status=data.status;
            }else if(isValue===3){
                updateUser.isDisabled=true;
            }else if(isValue===4){
                updateUser.isRemove=true;
            }
            return $http.put('/auth/updateUser',updateUser);
        },
        getUserBusinessUnits:function(data){
            return $http.get('/userBusinessUnits/'+data);
        },
        getCompanyBusinessUnits:function(){
            return $http.get('/branches');
        },
        updateBusinessUnit: function (data,isValue) {
            var updateUnit={businessUnit:data};
            if(isValue===1){
                updateUnit.isUpdate=true;
            }else if(isValue===2){
                updateUnit.isActive=true;
            }else if(isValue===3){
                updateUnit.isDisable=true;
            }else if(isValue===4){
                updateUnit.isRemove=true;
            }else if(isValue===5){
                updateUnit.isIncharge=true;
            }
            return $http.put('/branches/'+data._id,updateUnit);
        },
        statusChangeBusinessUnit: function (data) {
            return $http.put('/branches',data);
        },
        getUserGroup: function (data) {
            return $http.get('/auth/userGroups');
        },
        getBusinessUsersByCompany: function (companyId,paginationKeys) {
            return $http.get('/companyBusinessUsers/'+companyId,{params:paginationKeys});
        },
        getBusinessUnits: function (data) {
            return $http.get('/companyBusinessUnits/'+data);
        },
        getBUInchargeUsers: function (data) {
            return $http.get('/companyInchargeUsers/'+data);
        },
        getBusinessUnit:function (data) {
            return $http.get('/branches/'+data);
        },
        getBusinessUnitUsers:function (data) {
            return $http.get('/brancheusers/'+data);
        },
        getBusinessUser:function (data) {
            return $http.get('/companyUserUpdate/'+data);
        },
        batchCompanyEmployees:function (index,scope) {
            if(index === 0){
                return businessUserGroupActionQueries.removeUserAtCompany(getSelectedEmployee(scope.company.employees));
            }else if(index === 1){
                return businessUserGroupActionQueries.enableUserAtCompany(getSelectedEmployee(scope.company.employees));
            }else if(index === 2){
                return businessUserGroupActionQueries.disableUserAtCompany(getSelectedEmployee(scope.company.employees));
            }
        },
        batchBusinessUnitEmployees:function (unitEmployees,unitId) {
          return  $http.post('/auth/businessUnitMassActions',{businessUnitEmployees:unitEmployees,businessUnit:unitId,isRemove:true});
        },
        batchBusinessUnit:function(index,scope){

            if(index === 0){
                return businessUnitGroupActionQueries.removeAtUnit(getSelectedBusinessUnits(scope));
            }else if(index === 1){
                return businessUnitGroupActionQueries.enableAtUnit(getSelectedBusinessUnits(scope));
            }else if(index === 2){
                return businessUnitGroupActionQueries.disableAtUnit(getSelectedBusinessUnits(scope));
            }
        },
        transformChip:function(chip) {
            // If it is an object, it's already a known chip
            return transformChip(chip);
        },
        querySearchBusinessUsers: function(selectedUsers,query) {
            var results = query ? selectedUsers.filter(createFilterFn(query)) : [];
            return results;
        },
        loadChipUsers: function($scope,selectedUser,companyUsers){
            $scope.users=companyUsers;
            $scope.selectedUser=selectedUser;
            if($scope.users)
                userMap($scope.users,$scope);
            /*$scope.querySearchContacts = $scope.querySearchContacts;*/
            $scope.selectedUserItem= null;
            $scope.selectedUsers = [];
            $scope.numberChips = [];
            $scope.numberChips2 = [];
            $scope.filterSelected=true;
            $scope.numberBuffer = '';
            $scope.autocompleteDemoRequireMatch = false;

        },
        createPaymentTerm:function (paymentTerm) {
            return $http.post('/paymentterms',paymentTerm);
        }
    };

}]);
app.factory('businessUserGroupActionQueries',['$http',function ($http) {


    return {
        removeUserAtCompany: function (employees) {
                return $http.post('/auth/companyUserMassActions',{companyEmployees:employees,isRemove:true,isEnable:false,isDisable:false});
        },
        enableUserAtCompany: function (employees) {
            return $http.post('/auth/companyUserMassActions',{companyEmployees:employees,isRemove:false,isEnable:true,isDisable:false});
        },
        disableUserAtCompany: function (employees) {
            return $http.post('/auth/companyUserMassActions',{companyEmployees:employees,isRemove:false,isEnable:false,isDisable:true});
        }
    };
}]);

app.factory('businessUnitGroupActionQueries',['$http',function ($http) {

    return {

        removeAtUnit: function (businessUnits) {
            return $http.post('/businessUnitsRemove',businessUnits);
        },
        enableAtUnit: function (businessUnits) {
            return $http.post('/businessUnitsEnable',businessUnits);
        },
        disableAtUnit: function (businessUnits) {
            return $http.post('/businessUnitsDisable',businessUnits);
        }

    };
}]);

app.directive('nvBuValidation', function ($compile) {
    return {
        restrict: 'A',
        require: 'ngModel',

        link: function (scope, element, attrs, ctrl) {
            ctrl.$parsers.unshift(function (viewValue) {
                var exp ;
                var error;
                if(attrs.nvBuValidation) {
                    /*if (attrs.nvBuValidation === 'incharge') {
                        exp = /^.+$/;
                        error = 'Select Business Unit Incharge';
                    }else if (attrs.nvBuValidation === 'bunitName') {
                        exp = /^.*$/;
                        error = 'Business Unit Name required.';
                    }else */if (attrs.nvBuValidation === 'mail1') {
                        exp =/^[_a-zA-Z0-9]+(\.[_a-zA-Z0-9]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,4})$/;
                        error = 'Invalid Input..';
                    }else if (attrs.nvBuValidation === 'phonenumber') {
                        exp = /^[0-9]{10,11}$/;
                        error = 'Invalid Input..';
                    }else if (attrs.nvBuValidation === 'otp') {
                        exp = /^[0-9]{1,6}$/;
                        error = 'Invalid Input..';
                    }else if (attrs.nvBuValidation === 'num') {
                        exp = /^[0-9]{1,128}$/;
                        error = 'Invalid Input..';
                    } else if (attrs.nvBuValidation === 'float') {
                        exp =  /^(?=.*\d)\d*(\.\d+)?$/;
                        if(parseFloat(viewValue) && parseFloat(viewValue).toString()===viewValue)
                            viewValue=parseFloat(viewValue).toFixed('2');
                        error = 'Invalid input Ex:25 or 0.5 etc.,';
                    }else if (attrs.nvBuValidation === 'key') {
                        exp = /^[a-zA-Z0-9. _-]{1,128}$/;
                        error = 'Enter Proper key..';
                    }else if (attrs.nvBuValidation === 'value') {
                        exp = /^[a-zA-Z0-9. _-]{1,128}$/;
                        error = 'Enter Proper Value..';
                    }else if (attrs.nvBuValidation === 'margin') {
                        exp = /^0*(?=.*[0-9])\d{0,2}(?:\.\d+|100)?$/;
                        if(parseFloat(viewValue) && parseFloat(viewValue).toString()===viewValue)
                            viewValue=parseFloat(viewValue).toFixed('2');
                        error = 'Enter Proper input..';
                    }

                }
                var elements=element.parent().find('.fa-info-circle');
                if (exp && exp.test(viewValue) || (!attrs.required && !viewValue)) { // it is valid
                    if(elements.length !==0){
                        elements.remove();
                    }

                    ctrl.$setValidity('gstinNumber', true);
                    ctrl.$viewValue =viewValue;
                    // ctrl.$render();
                    return viewValue;
                } else { // it is invalid, return undefined (no model update)
                    ctrl.$setValidity('gstinNumber', false);
                    if(elements.length ===0) {
                        var findelement=element.parent().find('.md-char-counter');
                        var htmlText;
                        if(findelement.length >0)
                            htmlText = '<i class="fa fa-info-circle pass-icons nv-red" tabindex="1" data="'+error+'"></i>';
                        else htmlText = '<i class="fa fa-info-circle pass-icon nv-red" tabindex="1" data="'+error+'"></i>';
                        var linkFn = $compile(htmlText);

                        if(findelement.length >0) {
                            element.parent().find('.md-errors-spacer').append(linkFn(scope));
                        }else{
                            element.parent().append(linkFn(scope));
                        }

                    }
                    return undefined;
                }
            });

        }
    };
});
