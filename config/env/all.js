'use strict';

module.exports = {
	app: {
		title: 'nVipani - Commit to Summit your Business',
        description: 'Sell or Buy the Agricultural Produce',
        keywords: 'nVipani,nVipani,Supply Chain Management,SCM,Marketplace,nVipani,Agriculture,Agriculture Products,Agriculture Produce,Buy,Sell,Mediate,Sample Mediation,Cotton,Pulses,Cereals,Spices,Vegetable,Yarn,Fabric,Apparel,FMCG,Consumer Goods,Invoice,Order,Payments,Textiles'
	},
  /*  db: {
        promise: global.Promise
    },*/
	port: process.env.PORT || 3000,
    host: process.env.HOST || '0.0.0.0',
    // DOMAIN config should be set to the fully qualified application accessible
    // URL. For example: https://www.myapp.com (including port if required).
    domain: process.env.DOMAIN,
    // Session Cookie settings
    sessionCookie: {
        // session expiration is set by default to 24 hours
        maxAge: 24 * (60 * 60 * 1000),
        // httpOnly flag makes sure the cookie is only accessed
        // through the HTTP protocol and not JS/browser
        httpOnly: true,
        // secure cookie should be turned to true to provide additional
        // layer of security so that the cookie is set only when working
        // in HTTPS mode.
        secure: false
    },
	reportPort: 4000,
    production: false,
	templateEngine: 'swig',
	sessionSecret: 'MEAN',
	sessionCollection: 'sessions',
    assets: {
		lib: {
			css: [
				'public/lib/bootstrap/dist/css/bootstrap.min.css',
				'public/lib/bootstrap/dist/css/bootstrap.css',
				'public/lib/bootstrap/dist/css/bootstrap-theme.css',
				'public/lib/angular-xeditable/dist/css/xeditable.css',
				'public/lib/flexslider/flexslider.css',
				'public/lib/select2/select2.css',
				'public/lib/select2/select2-bootstrap.css',
				'public/lib/font-awesome/css/font-awesome.min.css',
				'public/lib/angualar-wizard/dist/angular-wizard.css',
				'public/lib/angular-ui-notification/dist/angular-ui-notification.css',
				'public/lib/angular-material/angular-material.css',
				'public/lib/flexslider/flexslider.css'
			],
			js: [
				'public/lib/jquery/dist/jquery.min.js',
				'public/lib/bootstrap/dist/js/bootstrap.min.js',
				'public/lib/angular-ui-select-suggest/dist/select.min.js',
				'public/lib/angular/angular.js',
				'public/lib/angular-resource/angular-resource.js', 
				'public/lib/angular-cookies/angular-cookies.js', 
				'public/lib/angular-animate/angular-animate.js', 
				'public/lib/angular-touch/angular-touch.js', 
				'public/lib/angular-sanitize/angular-sanitize.js',
				'public/lib/angular-ui-router/release/angular-ui-router.js',
				'public/lib/angular-ui-util/index.js',
                'public/lib/angular-filter/dist/angular-filter.js/angular-filter.min.js',
				'public/lib/flexslider/jquery-flexslider.js',
				'public/lib/moment/moment.js',
				/*
				'public/lib/angular-ui-event/dist/event.js',
				'public/lib/angular-ui-indeterminate/indeterminate.js',
				'public/lib/angular-ui-mask/mask.js',*/
				'public/lib/angular-ui-scroll/ui-scroll.js',
				'public/lib/angular-bootstrap/ui-bootstrap.js',
				'public/lib/angular-xeditable/dist/js/xeditable.js',
				'public/lib/checklist-model/checklist-model.js',
				'public/lib/angular-local-storage/dist/angular-local-storage.min.js',
				'public/lib/angular-file-upload/dist/angular-file-upload.min.js',
				'public/lib/flexslider/jquery.flexslider-min.js',
				'public/lib/select2/select2.js',
				'public/lib/angular-ui-select2/src/select2.js',
				'public/lib/angular-slimscroll/angular-slimscroll.js',
				'public/lib/async/dist/async.js',
				'public/lib/angular-slimscroll/angular-slimscroll.js',
				'public/lib/angualar-wizard/dist/angular-wizard.js',
                'public/lib/angular-messages/angular-messages.js',
				'public/lib/angular-ui-notification/dist/angular-ui-notification.js',
                'public/lib/angularUtils-pagination/dirPagination.js',
                'public/lib/ng-device-detector/ng-device-detector.js',
                'public/lib/re-tree/re-tree.js',
                'public/lib/ua-device-detector/ua-device-detector.js',
                'public/lib/angular-uuids/angular-uuid.js',
				'public/lib/angular-material/angular-material.js',
				'public/lib/angular-aria/angular-aria.js',
                'public/lib/vsGoogleAutocomplete/dist/vs-google-autocomplete.js',
                'public/lib/js-xlsx/dist/xlsx.full.min.js'
			]
		},
		css: [
			'public/modules/**/css/*.css'
		],
		js: [
			'public/config.js',
			'public/application.js',
			'public/modules/*/*.js',
			'public/modules/*/*/*.js',
			'public/modules/*/*/*/*.js',
			'!public/modules/*/tests/*.js'
		]
		/*tests: [
			'public/lib/angular-mocks/angular-mocks.js',
			'public/modules/!*!/tests/!*.js'
		]*/
    }
};
