// Remove categories Data
db.categories.remove({});

// Agriculture Main Category
db.categories.insert({'name': 'Agriculture', 'type': 'MainCategory'});

// Sub Categories of Agriculture
db.categories.insert({'name': 'Agricultural Equipment', 'type': 'SubCategory1'});
db.categories.insert({'name': 'Agricultural Waste', 'type': 'SubCategory1'});
db.categories.insert({'name': 'Agriculture Machinery Parts', 'type': 'SubCategory1'});
db.categories.insert({'name': 'Agrochemicals & Pesticides', 'type': 'SubCategory1'});
db.categories.insert({'name': 'Animal Feed', 'type': 'SubCategory1'});
db.categories.insert({'name': 'Animal Products', 'type': 'SubCategory1'});
db.categories.insert({'name': 'Beans', 'type': 'SubCategory1'});
db.categories.insert({'name': 'Cacao Beans', 'type': 'SubCategory1'});
db.categories.insert({'name': 'Coffee Beans', 'type': 'SubCategory1'});
db.categories.insert({'name': 'Fertilizer', 'type': 'SubCategory1'});
db.categories.insert({'name': 'Forestry Machinery', 'type': 'SubCategory1'});
db.categories.insert({'name': 'Fresh Fruit', 'type': 'SubCategory1'});
db.categories.insert({'name': 'Fresh Vegetables', 'type': 'SubCategory1'});
db.categories.insert({'name': 'Grain', 'type': 'SubCategory1'});
db.categories.insert({'name': 'Herbal Cigars & Cigarettes', 'type': 'SubCategory1'});
db.categories.insert({'name': 'Mushrooms & Truffles', 'type': 'SubCategory1'});
db.categories.insert({'name': 'Nuts & Kernels', 'type': 'SubCategory1'});
db.categories.insert({'name': 'Ornamental Plants', 'type': 'SubCategory1'});
db.categories.insert({'name': 'Other Agriculture Products', 'type': 'SubCategory1'});
db.categories.insert({'name': 'Plant & Animal Oil', 'type': 'SubCategory1'});
db.categories.insert({'name': 'Plant Seeds & Bulbs', 'type': 'SubCategory1'});
db.categories.insert({'name': 'Timber Raw Materials', 'type': 'SubCategory1'});
db.categories.insert({'name': 'Vanilla Beans', 'type': 'SubCategory1'});

// Adding Sub Category Agricultural Equipment to Agriculture category
db.categories.update({
'name': 'Agriculture',
'type': 'MainCategory'
}, {$addToSet: {'children': db.categories.findOne({'name': 'Agricultural Equipment', 'type': 'SubCategory1'})._id}});

// Sub Categories of Agricultural Equipment
db.categories.insert({'name': 'Agricultural Greenhouses', 'type': 'SubCategory2'});
db.categories.insert({'name': 'Animal & Poultry Husbandry Equipment', 'type': 'SubCategory2'});
db.categories.insert({'name': 'Other Agricultural Equipment', 'type': 'SubCategory2'});
db.categories.insert({'name': 'Aquaculture Equipment', 'type': 'SubCategory2'});
db.categories.insert({'name': 'Irrigation & Hydroponics Eqiupment', 'type': 'SubCategory2'});

// Adding Sub Categories to Agricultural Equipment category
db.categories.update({
'name': 'Agricultural Equipment',
'type': 'SubCategory1'
}, {$addToSet: {'children': db.categories.findOne({'name': 'Agricultural Greenhouses', 'type': 'SubCategory2'})._id}});
db.categories.update({
'name': 'Agricultural Equipment',
'type': 'SubCategory1'
}, {
$addToSet: {
'children': db.categories.findOne({
'name': 'Animal & Poultry Husbandry Equipment',
'type': 'SubCategory2'
})._id
}
});
db.categories.update({
'name': 'Agricultural Equipment',
'type': 'SubCategory1'
}, {
$addToSet: {
'children': db.categories.findOne({
'name': 'Other Agricultural Equipment',
'type': 'SubCategory2'
})._id
}
});
db.categories.update({
'name': 'Agricultural Equipment',
'type': 'SubCategory1'
}, {$addToSet: {'children': db.categories.findOne({'name': 'Aquaculture Equipment', 'type': 'SubCategory2'})._id}});
db.categories.update({
'name': 'Agricultural Equipment',
'type': 'SubCategory1'
}, {
$addToSet: {
'children': db.categories.findOne({
'name': 'Irrigation & Hydroponics Eqiupment',
'type': 'SubCategory2'
})._id
}
});

// Adding Sub Category Agricultural Waste to Agriculture category
db.categories.update({
'name': 'Agriculture',
'type': 'MainCategory'
}, {$addToSet: {'children': db.categories.findOne({'name': 'Agricultural Waste', 'type': 'SubCategory1'})._id}});
// Adding Sub Category Agriculture Machinery Parts to Agriculture category
db.categories.update({
'name': 'Agriculture',
'type': 'MainCategory'
}, {
$addToSet: {
'children': db.categories.findOne({
'name': 'Agriculture Machinery Parts',
'type': 'SubCategory1'
})._id
}
});
// Adding Sub Category Agrochemicals & Pesticides to Agriculture category
db.categories.update({
'name': 'Agriculture',
'type': 'MainCategory'
}, {
$addToSet: {
'children': db.categories.findOne({
'name': 'Agrochemicals & Pesticides',
'type': 'SubCategory1'
})._id
}
});
// Adding Sub Category Animal Feed to Agriculture category
db.categories.update({
'name': 'Agriculture',
'type': 'MainCategory'
}, {$addToSet: {'children': db.categories.findOne({'name': 'Animal Feed', 'type': 'SubCategory1'})._id}});
// Adding Sub Category Animal Products to Agriculture category
db.categories.update({
'name': 'Agriculture',
'type': 'MainCategory'
}, {$addToSet: {'children': db.categories.findOne({'name': 'Animal Products', 'type': 'SubCategory1'})._id}});

// Sub Categories of Animal Products
db.categories.insert({'name': 'Eggs', 'type': 'SubCategory2'});
db.categories.insert({'name': 'Fowl & Livestock', 'type': 'SubCategory2'});
db.categories.insert({'name': 'Other Animal Products', 'type': 'SubCategory2'});

// Adding Sub Categories to Animal Products category
db.categories.update({
'name': 'Animal Products',
'type': 'SubCategory1'
}, {$addToSet: {'children': db.categories.findOne({'name': 'Eggs', 'type': 'SubCategory2'})._id}});
db.categories.update({
'name': 'Animal Products',
'type': 'SubCategory1'
}, {$addToSet: {'children': db.categories.findOne({'name': 'Fowl & Livestock', 'type': 'SubCategory2'})._id}});
db.categories.update({
'name': 'Animal Products',
'type': 'SubCategory1'
}, {$addToSet: {'children': db.categories.findOne({'name': 'Other Animal Products', 'type': 'SubCategory2'})._id}});


// Adding Sub Category Beans to Agriculture category
db.categories.update({
'name': 'Agriculture',
'type': 'MainCategory'
}, {$addToSet: {'children': db.categories.findOne({'name': 'Beans', 'type': 'SubCategory1'})._id}});

// Sub Categories of Beans
db.categories.insert({'name': 'Broad Beans', 'type': 'SubCategory2'});
db.categories.insert({'name': 'Butter Beans', 'type': 'SubCategory2'});
db.categories.insert({'name': 'Other Beans', 'type': 'SubCategory2'});
db.categories.insert({'name': 'Peas', 'type': 'SubCategory2'});
db.categories.insert({'name': 'Soybeans', 'type': 'SubCategory2'});

// Adding Sub Categories to Animal Products category
db.categories.update({
'name': 'Beans',
'type': 'SubCategory1'
}, {$addToSet: {'children': db.categories.findOne({'name': 'Broad Beans', 'type': 'SubCategory2'})._id}});
db.categories.update({
'name': 'Beans',
'type': 'SubCategory1'
}, {$addToSet: {'children': db.categories.findOne({'name': 'Butter Beans', 'type': 'SubCategory2'})._id}});
db.categories.update({
'name': 'Beans',
'type': 'SubCategory1'
}, {$addToSet: {'children': db.categories.findOne({'name': 'Other Beans', 'type': 'SubCategory2'})._id}});
db.categories.update({
'name': 'Beans',
'type': 'SubCategory1'
}, {$addToSet: {'children': db.categories.findOne({'name': 'Peas', 'type': 'SubCategory2'})._id}});
db.categories.update({
'name': 'Beans',
'type': 'SubCategory1'
}, {$addToSet: {'children': db.categories.findOne({'name': 'Soybeans', 'type': 'SubCategory2'})._id}});


// Adding Sub Category Cacao Beans to Agriculture category
db.categories.update({
'name': 'Agriculture',
'type': 'MainCategory'
}, {$addToSet: {'children': db.categories.findOne({'name': 'Cacao Beans', 'type': 'SubCategory1'})._id}});
// Adding Sub Category Coffee Beans to Agriculture category
db.categories.update({
'name': 'Agriculture',
'type': 'MainCategory'
}, {$addToSet: {'children': db.categories.findOne({'name': 'Coffee Beans', 'type': 'SubCategory1'})._id}});
// Adding Sub Category Fertilizer to Agriculture category
db.categories.update({
'name': 'Agriculture',
'type': 'MainCategory'
}, {$addToSet: {'children': db.categories.findOne({'name': 'Fertilizer', 'type': 'SubCategory1'})._id}});

// Sub Categories of Fertilizer
db.categories.insert({'name': 'Biological Fertilizer', 'type': 'SubCategory2'});
db.categories.insert({'name': 'Compound Fertilizer', 'type': 'SubCategory2'});
db.categories.insert({'name': 'Nitrogen Fertilizer', 'type': 'SubCategory2'});
db.categories.insert({'name': 'Organic Fertilizer', 'type': 'SubCategory2'});
db.categories.insert({'name': 'Other Fertilizers', 'type': 'SubCategory2'});
db.categories.insert({'name': 'Plant Food', 'type': 'SubCategory2'});
db.categories.insert({'name': 'Potassium Fertilizer', 'type': 'SubCategory2'});

// Adding Sub Categories to Fertilizer category
db.categories.update({
'name': 'Fertilizer',
'type': 'SubCategory1'
}, {$addToSet: {'children': db.categories.findOne({'name': 'Biological Fertilizer', 'type': 'SubCategory2'})._id}});
db.categories.update({
'name': 'Fertilizer',
'type': 'SubCategory1'
}, {$addToSet: {'children': db.categories.findOne({'name': 'Compound Fertilizer', 'type': 'SubCategory2'})._id}});
db.categories.update({
'name': 'Fertilizer',
'type': 'SubCategory1'
}, {$addToSet: {'children': db.categories.findOne({'name': 'Nitrogen Fertilizer', 'type': 'SubCategory2'})._id}});
db.categories.update({
'name': 'Fertilizer',
'type': 'SubCategory1'
}, {$addToSet: {'children': db.categories.findOne({'name': 'Organic Fertilizer', 'type': 'SubCategory2'})._id}});
db.categories.update({
'name': 'Fertilizer',
'type': 'SubCategory1'
}, {$addToSet: {'children': db.categories.findOne({'name': 'Other Fertilizers', 'type': 'SubCategory2'})._id}});
db.categories.update({
'name': 'Fertilizer',
'type': 'SubCategory1'
}, {$addToSet: {'children': db.categories.findOne({'name': 'Plant Food', 'type': 'SubCategory2'})._id}});
db.categories.update({
'name': 'Fertilizer',
'type': 'SubCategory1'
}, {$addToSet: {'children': db.categories.findOne({'name': 'Potassium Fertilizer', 'type': 'SubCategory2'})._id}});

// Adding Sub Category Forestry Machinery to Agriculture category
db.categories.update({
'name': 'Agriculture',
'type': 'MainCategory'
}, {$addToSet: {'children': db.categories.findOne({'name': 'Forestry Machinery', 'type': 'SubCategory1'})._id}});

// Adding Sub Category Fresh Fruit to Agriculture category
db.categories.update({
'name': 'Agriculture',
'type': 'MainCategory'
}, {$addToSet: {'children': db.categories.findOne({'name': 'Fresh Fruit', 'type': 'SubCategory1'})._id}});

// Sub Categories of Fresh Fruit

db.categories.insert({'name': 'Fresh Apples', 'type': 'SubCategory2'});
db.categories.insert({'name': 'Fresh Apricots', 'type': 'SubCategory2'});
db.categories.insert({'name': 'Fresh Avocados', 'type': 'SubCategory2'});
db.categories.insert({'name': 'Fresh Bananas', 'type': 'SubCategory2'});
db.categories.insert({'name': 'Fresh Berries', 'type': 'SubCategory2'});
db.categories.insert({'name': 'Fresh Cherries', 'type': 'SubCategory2'});
db.categories.insert({'name': 'Fresh Citrus Fruit', 'type': 'SubCategory2'});
db.categories.insert({'name': 'Fresh Coconuts', 'type': 'SubCategory2'});
db.categories.insert({'name': 'Fresh Dragon Fruit', 'type': 'SubCategory2'});
db.categories.insert({'name': 'Fresh Durians', 'type': 'SubCategory2'});
db.categories.insert({'name': 'Fresh Grapes', 'type': 'SubCategory2'});
db.categories.insert({'name': 'Fresh Guava', 'type': 'SubCategory2'});
db.categories.insert({'name': 'Fresh Kiwi Fruit', 'type': 'SubCategory2'});
db.categories.insert({'name': 'Fresh Mangos', 'type': 'SubCategory2'});
db.categories.insert({'name': 'Fresh Melons', 'type': 'SubCategory2'});
db.categories.insert({'name': 'Fresh Olives', 'type': 'SubCategory2'});
db.categories.insert({'name': 'Fresh Papaya', 'type': 'SubCategory2'});
db.categories.insert({'name': 'Fresh Peaches', 'type': 'SubCategory2'});
db.categories.insert({'name': 'Fresh Pears', 'type': 'SubCategory2'});
db.categories.insert({'name': 'Fresh Pineapples', 'type': 'SubCategory2'});
db.categories.insert({'name': 'Fresh Plums', 'type': 'SubCategory2'});
db.categories.insert({'name': 'Fresh Pomegranates', 'type': 'SubCategory2'});
db.categories.insert({'name': 'Other Fresh Fruit', 'type': 'SubCategory2'});

// Adding Sub Categories to Fresh Fruit
db.categories.update({
'name': 'Fresh Fruit',
'type': 'SubCategory1'
}, {$addToSet: {'children': db.categories.findOne({'name': 'Fresh Apples', 'type': 'SubCategory2'})._id}});
db.categories.update({
'name': 'Fresh Fruit',
'type': 'SubCategory1'
}, {$addToSet: {'children': db.categories.findOne({'name': 'Fresh Avocados', 'type': 'SubCategory2'})._id}});
db.categories.update({
'name': 'Fresh Fruit',
'type': 'SubCategory1'
}, {$addToSet: {'children': db.categories.findOne({'name': 'Fresh Bananas', 'type': 'SubCategory2'})._id}});
db.categories.update({
'name': 'Fresh Fruit',
'type': 'SubCategory1'
}, {$addToSet: {'children': db.categories.findOne({'name': 'Fresh Berries', 'type': 'SubCategory2'})._id}});
db.categories.update({
'name': 'Fresh Fruit',
'type': 'SubCategory1'
}, {$addToSet: {'children': db.categories.findOne({'name': 'Fresh Cherries', 'type': 'SubCategory2'})._id}});
db.categories.update({
'name': 'Fresh Fruit',
'type': 'SubCategory1'
}, {$addToSet: {'children': db.categories.findOne({'name': 'Fresh Citrus Fruit', 'type': 'SubCategory2'})._id}});
db.categories.update({
'name': 'Fresh Fruit',
'type': 'SubCategory1'
}, {$addToSet: {'children': db.categories.findOne({'name': 'Fresh Dragon Fruit', 'type': 'SubCategory2'})._id}});
db.categories.update({
'name': 'Fresh Fruit',
'type': 'SubCategory1'
}, {$addToSet: {'children': db.categories.findOne({'name': 'Fresh Durians', 'type': 'SubCategory2'})._id}});
db.categories.update({
'name': 'Fresh Fruit',
'type': 'SubCategory1'
}, {$addToSet: {'children': db.categories.findOne({'name': 'Fresh Grapes', 'type': 'SubCategory2'})._id}});
db.categories.update({
'name': 'Fresh Fruit',
'type': 'SubCategory1'
}, {$addToSet: {'children': db.categories.findOne({'name': 'Fresh Guava', 'type': 'SubCategory2'})._id}});
db.categories.update({
'name': 'Fresh Fruit',
'type': 'SubCategory1'
}, {$addToSet: {'children': db.categories.findOne({'name': 'Fresh Kiwi Fruit', 'type': 'SubCategory2'})._id}});
db.categories.update({
'name': 'Fresh Fruit',
'type': 'SubCategory1'
}, {$addToSet: {'children': db.categories.findOne({'name': 'Fresh Mangos', 'type': 'SubCategory2'})._id}});
db.categories.update({
'name': 'Fresh Fruit',
'type': 'SubCategory1'
}, {$addToSet: {'children': db.categories.findOne({'name': 'Fresh Melons', 'type': 'SubCategory2'})._id}});
db.categories.update({
'name': 'Fresh Fruit',
'type': 'SubCategory1'
}, {$addToSet: {'children': db.categories.findOne({'name': 'Fresh Olives', 'type': 'SubCategory2'})._id}});
db.categories.update({
'name': 'Fresh Fruit',
'type': 'SubCategory1'
}, {$addToSet: {'children': db.categories.findOne({'name': 'Fresh Papaya', 'type': 'SubCategory2'})._id}});
db.categories.update({
'name': 'Fresh Fruit',
'type': 'SubCategory1'
}, {$addToSet: {'children': db.categories.findOne({'name': 'Fresh Peaches', 'type': 'SubCategory2'})._id}});
db.categories.update({
'name': 'Fresh Fruit',
'type': 'SubCategory1'
}, {$addToSet: {'children': db.categories.findOne({'name': 'Fresh Pears', 'type': 'SubCategory2'})._id}});
db.categories.update({
'name': 'Fresh Fruit',
'type': 'SubCategory1'
}, {$addToSet: {'children': db.categories.findOne({'name': 'Fresh Pineapples', 'type': 'SubCategory2'})._id}});
db.categories.update({
'name': 'Fresh Fruit',
'type': 'SubCategory1'
}, {$addToSet: {'children': db.categories.findOne({'name': 'Fresh Plums', 'type': 'SubCategory2'})._id}});
db.categories.update({
'name': 'Fresh Fruit',
'type': 'SubCategory1'
}, {$addToSet: {'children': db.categories.findOne({'name': 'Fresh Pomegranates', 'type': 'SubCategory2'})._id}});
db.categories.update({
'name': 'Fresh Fruit',
'type': 'SubCategory1'
}, {$addToSet: {'children': db.categories.findOne({'name': 'Other Fresh Fruit', 'type': 'SubCategory2'})._id}});

// Adding Sub Category Fresh Vegetables to Agriculture category
db.categories.update({
'name': 'Agriculture',
'type': 'MainCategory'
}, {$addToSet: {'children': db.categories.findOne({'name': 'Fresh Vegetables', 'type': 'SubCategory1'})._id}});

// Sub Categories of Fresh Vegetables

db.categories.insert({'name': 'Fresh Asparagus', 'type': 'SubCategory2'});
db.categories.insert({'name': 'Fresh Broccoli', 'type': 'SubCategory2'});
db.categories.insert({'name': 'Fresh Burdock', 'type': 'SubCategory2'});
db.categories.insert({'name': 'Fresh Cabbages', 'type': 'SubCategory2'});
db.categories.insert({'name': 'Fresh Capsicum', 'type': 'SubCategory2'});
db.categories.insert({'name': 'Fresh Carrots', 'type': 'SubCategory2'});
db.categories.insert({'name': 'Fresh Cassava', 'type': 'SubCategory2'});
db.categories.insert({'name': 'Fresh Cauliflower', 'type': 'SubCategory2'});
db.categories.insert({'name': 'Fresh Celery Cabbage', 'type': 'SubCategory2'});
db.categories.insert({'name': 'Fresh Cucumber', 'type': 'SubCategory2'});
db.categories.insert({'name': 'Fresh Garlic', 'type': 'SubCategory2'});
db.categories.insert({'name': 'Fresh Ginger', 'type': 'SubCategory2'});
db.categories.insert({'name': 'Fresh Okra', 'type': 'SubCategory2'});
db.categories.insert({'name': 'Fresh Onions', 'type': 'SubCategory2'});
db.categories.insert({'name': 'Fresh Potatoes', 'type': 'SubCategory2'});
db.categories.insert({'name': 'Fresh Pumpkins', 'type': 'SubCategory2'});
db.categories.insert({'name': 'Fresh Radish', 'type': 'SubCategory2'});
db.categories.insert({'name': 'Fresh Scallions', 'type': 'SubCategory2'});
db.categories.insert({'name': 'Fresh Sweet Potatoes', 'type': 'SubCategory2'});
db.categories.insert({'name': 'Fresh Taro', 'type': 'SubCategory2'});
db.categories.insert({'name': 'Fresh Tomatoes', 'type': 'SubCategory2'});
db.categories.insert({'name': 'Other Fresh Vegetables', 'type': 'SubCategory2'});

// Adding Sub Categories to Fresh Vegetables

db.categories.update({
'name': 'Fresh Vegetables',
'type': 'SubCategory1'
}, {$addToSet: {'children': db.categories.findOne({'name': 'Fresh Asparagus', 'type': 'SubCategory2'})._id}});
db.categories.update({
'name': 'Fresh Vegetables',
'type': 'SubCategory1'
}, {$addToSet: {'children': db.categories.findOne({'name': 'Fresh Broccoli', 'type': 'SubCategory2'})._id}});
db.categories.update({
'name': 'Fresh Vegetables',
'type': 'SubCategory1'
}, {$addToSet: {'children': db.categories.findOne({'name': 'Fresh Burdock', 'type': 'SubCategory2'})._id}});
db.categories.update({
'name': 'Fresh Vegetables',
'type': 'SubCategory1'
}, {$addToSet: {'children': db.categories.findOne({'name': 'Fresh Cabbages', 'type': 'SubCategory2'})._id}});
db.categories.update({
'name': 'Fresh Vegetables',
'type': 'SubCategory1'
}, {$addToSet: {'children': db.categories.findOne({'name': 'Fresh Capsicum', 'type': 'SubCategory2'})._id}});
db.categories.update({
'name': 'Fresh Vegetables',
'type': 'SubCategory1'
}, {$addToSet: {'children': db.categories.findOne({'name': 'Fresh Carrots', 'type': 'SubCategory2'})._id}});
db.categories.update({
'name': 'Fresh Vegetables',
'type': 'SubCategory1'
}, {$addToSet: {'children': db.categories.findOne({'name': 'Fresh Cassava', 'type': 'SubCategory2'})._id}});
db.categories.update({
'name': 'Fresh Vegetables',
'type': 'SubCategory1'
}, {$addToSet: {'children': db.categories.findOne({'name': 'Fresh Cauliflower', 'type': 'SubCategory2'})._id}});
db.categories.update({
'name': 'Fresh Vegetables',
'type': 'SubCategory1'
}, {$addToSet: {'children': db.categories.findOne({'name': 'Fresh Celery Cabbage', 'type': 'SubCategory2'})._id}});
db.categories.update({
'name': 'Fresh Vegetables',
'type': 'SubCategory1'
}, {$addToSet: {'children': db.categories.findOne({'name': 'Fresh Cucumber', 'type': 'SubCategory2'})._id}});
db.categories.update({
'name': 'Fresh Vegetables',
'type': 'SubCategory1'
}, {$addToSet: {'children': db.categories.findOne({'name': 'Fresh Garlic', 'type': 'SubCategory2'})._id}});
db.categories.update({
'name': 'Fresh Vegetables',
'type': 'SubCategory1'
}, {$addToSet: {'children': db.categories.findOne({'name': 'Fresh Ginger', 'type': 'SubCategory2'})._id}});
db.categories.update({
'name': 'Fresh Vegetables',
'type': 'SubCategory1'
}, {$addToSet: {'children': db.categories.findOne({'name': 'Fresh Okra', 'type': 'SubCategory2'})._id}});
db.categories.update({
'name': 'Fresh Vegetables',
'type': 'SubCategory1'
}, {$addToSet: {'children': db.categories.findOne({'name': 'Fresh Onions', 'type': 'SubCategory2'})._id}});
db.categories.update({
'name': 'Fresh Vegetables',
'type': 'SubCategory1'
}, {$addToSet: {'children': db.categories.findOne({'name': 'Fresh Potatoes', 'type': 'SubCategory2'})._id}});
db.categories.update({
'name': 'Fresh Vegetables',
'type': 'SubCategory1'
}, {$addToSet: {'children': db.categories.findOne({'name': 'Fresh Pumpkins', 'type': 'SubCategory2'})._id}});
db.categories.update({
'name': 'Fresh Vegetables',
'type': 'SubCategory1'
}, {$addToSet: {'children': db.categories.findOne({'name': 'Fresh Radish', 'type': 'SubCategory2'})._id}});
db.categories.update({
'name': 'Fresh Vegetables',
'type': 'SubCategory1'
}, {$addToSet: {'children': db.categories.findOne({'name': 'Fresh Scallions', 'type': 'SubCategory2'})._id}});
db.categories.update({
'name': 'Fresh Vegetables',
'type': 'SubCategory1'
}, {$addToSet: {'children': db.categories.findOne({'name': 'Fresh Sweet Potatoes', 'type': 'SubCategory2'})._id}});
db.categories.update({
'name': 'Fresh Vegetables',
'type': 'SubCategory1'
}, {$addToSet: {'children': db.categories.findOne({'name': 'Fresh Taro', 'type': 'SubCategory2'})._id}});
db.categories.update({
'name': 'Fresh Vegetables',
'type': 'SubCategory1'
}, {$addToSet: {'children': db.categories.findOne({'name': 'Fresh Tomatoes', 'type': 'SubCategory2'})._id}});
db.categories.update({
'name': 'Fresh Vegetables',
'type': 'SubCategory1'
}, {$addToSet: {'children': db.categories.findOne({'name': 'Other Fresh Vegetables', 'type': 'SubCategory2'})._id}});

// Adding Sub Category Grain to Agriculture category
db.categories.update({
'name': 'Agriculture',
'type': 'MainCategory'
}, {$addToSet: {'children': db.categories.findOne({'name': 'Grain', 'type': 'SubCategory1'})._id}});

// Sub Categories of Grain
db.categories.insert({'name': 'Barley', 'type': 'SubCategory2'});
db.categories.insert({'name': 'Buckwheat', 'type': 'SubCategory2'});
db.categories.insert({'name': 'Corn', 'type': 'SubCategory2'});
db.categories.insert({'name': 'Millet', 'type': 'SubCategory2'});
db.categories.insert({'name': 'Oats', 'type': 'SubCategory2'});
db.categories.insert({'name': 'Other Grain', 'type': 'SubCategory2'});
db.categories.insert({'name': 'Quinoa', 'type': 'SubCategory2'});
db.categories.insert({'name': 'Rice', 'type': 'SubCategory2'});
db.categories.insert({'name': 'Sorghum', 'type': 'SubCategory2'});
db.categories.insert({'name': 'Wheat', 'type': 'SubCategory2'});

// Adding Sub Categories to Grain

db.categories.update({
'name': 'Grain',
'type': 'SubCategory1'
}, {$addToSet: {'children': db.categories.findOne({'name': 'Barley', 'type': 'SubCategory2'})._id}});
db.categories.update({
'name': 'Grain',
'type': 'SubCategory1'
}, {$addToSet: {'children': db.categories.findOne({'name': 'Buckwheat', 'type': 'SubCategory2'})._id}});
db.categories.update({
'name': 'Grain',
'type': 'SubCategory1'
}, {$addToSet: {'children': db.categories.findOne({'name': 'Corn', 'type': 'SubCategory2'})._id}});
db.categories.update({
'name': 'Grain',
'type': 'SubCategory1'
}, {$addToSet: {'children': db.categories.findOne({'name': 'Millet', 'type': 'SubCategory2'})._id}});
db.categories.update({
'name': 'Grain',
'type': 'SubCategory1'
}, {$addToSet: {'children': db.categories.findOne({'name': 'Oats', 'type': 'SubCategory2'})._id}});
db.categories.update({
'name': 'Grain',
'type': 'SubCategory1'
}, {$addToSet: {'children': db.categories.findOne({'name': 'Other Grain', 'type': 'SubCategory2'})._id}});
db.categories.update({
'name': 'Grain',
'type': 'SubCategory1'
}, {$addToSet: {'children': db.categories.findOne({'name': 'Quinoa', 'type': 'SubCategory2'})._id}});
db.categories.update({
'name': 'Grain',
'type': 'SubCategory1'
}, {$addToSet: {'children': db.categories.findOne({'name': 'Rice', 'type': 'SubCategory2'})._id}});
db.categories.update({
'name': 'Grain',
'type': 'SubCategory1'
}, {$addToSet: {'children': db.categories.findOne({'name': 'Sorghum', 'type': 'SubCategory2'})._id}});
db.categories.update({
'name': 'Grain',
'type': 'SubCategory1'
}, {$addToSet: {'children': db.categories.findOne({'name': 'Wheat', 'type': 'SubCategory2'})._id}});

// Adding Sub Category Cigars & Cigarettes to Agriculture category
db.categories.update({
'name': 'Agriculture',
'type': 'MainCategory'
}, {$addToSet: {'children': db.categories.findOne({'name': 'Herbal Cigars & Cigarettes', 'type': 'SubCategory1'})._id}});

// Adding Sub Category Mushrooms & Truffles to Agriculture category
db.categories.update({
'name': 'Agriculture',
'type': 'MainCategory'
}, {$addToSet: {'children': db.categories.findOne({'name': 'Mushrooms & Truffles', 'type': 'SubCategory1'})._id}});

// Sub Categories of Mushrooms & Truffles
db.categories.insert({'name': 'Mushrooms', 'type': 'SubCategory2'});
db.categories.insert({'name': 'Truffles', 'type': 'SubCategory2'});

// Adding Sub Categories to Mushrooms & Truffles

db.categories.update({
'name': 'Mushrooms & Truffles',
'type': 'SubCategory1'
}, {$addToSet: {'children': db.categories.findOne({'name': 'Mushrooms', 'type': 'SubCategory2'})._id}});
db.categories.update({
'name': 'Mushrooms & Truffles',
'type': 'SubCategory1'
}, {$addToSet: {'children': db.categories.findOne({'name': 'Truffles', 'type': 'SubCategory2'})._id}});

// Adding Sub Category Nuts & Kernels to Agriculture category
db.categories.update({
'name': 'Agriculture',
'type': 'MainCategory'
}, {$addToSet: {'children': db.categories.findOne({'name': 'Nuts & Kernels', 'type': 'SubCategory1'})._id}});

// Sub Categories of Nuts & Kernels
db.categories.insert({'name': 'Almond', 'type': 'SubCategory2'});
db.categories.insert({'name': 'Apricot Kernels', 'type': 'SubCategory2'});
db.categories.insert({'name': 'Betel Nuts', 'type': 'SubCategory2'});
db.categories.insert({'name': 'Brazil Nuts', 'type': 'SubCategory2'});
db.categories.insert({'name': 'Cashew Nuts', 'type': 'SubCategory2'});
db.categories.insert({'name': 'Ginkgo Nuts', 'type': 'SubCategory2'});
db.categories.insert({'name': 'Hazelnuts', 'type': 'SubCategory2'});
db.categories.insert({'name': 'Macadamia Nuts', 'type': 'SubCategory2'});
db.categories.insert({'name': 'Melon Seeds', 'type': 'SubCategory2'});
db.categories.insert({'name': 'Other Nuts', 'type': 'SubCategory2'});
db.categories.insert({'name': 'Peanuts', 'type': 'SubCategory2'});
db.categories.insert({'name': 'Pecan Nuts', 'type': 'SubCategory2'});
db.categories.insert({'name': 'Pine Nuts', 'type': 'SubCategory2'});
db.categories.insert({'name': 'Pistachio Nuts', 'type': 'SubCategory2'});
db.categories.insert({'name': 'Pumpkin Kernels', 'type': 'SubCategory2'});
db.categories.insert({'name': 'Walnuts', 'type': 'SubCategory2'});

// Adding Sub Categories to Nuts & Kernels
db.categories.update({
'name': 'Nuts & Kernels',
'type': 'SubCategory1'
}, {$addToSet: {'children': db.categories.findOne({'name': 'Almond', 'type': 'SubCategory2'})._id}});
db.categories.update({
'name': 'Nuts & Kernels',
'type': 'SubCategory1'
}, {$addToSet: {'children': db.categories.findOne({'name': 'Apricot Kernels', 'type': 'SubCategory2'})._id}});
db.categories.update({
'name': 'Nuts & Kernels',
'type': 'SubCategory1'
}, {$addToSet: {'children': db.categories.findOne({'name': 'Betel Nuts', 'type': 'SubCategory2'})._id}});
db.categories.update({
'name': 'Nuts & Kernels',
'type': 'SubCategory1'
}, {$addToSet: {'children': db.categories.findOne({'name': 'Brazil Nuts', 'type': 'SubCategory2'})._id}});
db.categories.update({
'name': 'Nuts & Kernels',
'type': 'SubCategory1'
}, {$addToSet: {'children': db.categories.findOne({'name': 'Cashew Nuts', 'type': 'SubCategory2'})._id}});
db.categories.update({
'name': 'Nuts & Kernels',
'type': 'SubCategory1'
}, {$addToSet: {'children': db.categories.findOne({'name': 'Ginkgo Nuts', 'type': 'SubCategory2'})._id}});
db.categories.update({
'name': 'Nuts & Kernels',
'type': 'SubCategory1'
}, {$addToSet: {'children': db.categories.findOne({'name': 'Hazelnuts', 'type': 'SubCategory2'})._id}});
db.categories.update({
'name': 'Nuts & Kernels',
'type': 'SubCategory1'
}, {$addToSet: {'children': db.categories.findOne({'name': 'Macadamia Nuts', 'type': 'SubCategory2'})._id}});
db.categories.update({
'name': 'Nuts & Kernels',
'type': 'SubCategory1'
}, {$addToSet: {'children': db.categories.findOne({'name': 'Melon Seeds', 'type': 'SubCategory2'})._id}});
db.categories.update({
'name': 'Nuts & Kernels',
'type': 'SubCategory1'
}, {$addToSet: {'children': db.categories.findOne({'name': 'Other Nuts', 'type': 'SubCategory2'})._id}});
db.categories.update({
'name': 'Nuts & Kernels',
'type': 'SubCategory1'
}, {$addToSet: {'children': db.categories.findOne({'name': 'Peanuts', 'type': 'SubCategory2'})._id}});
db.categories.update({
'name': 'Nuts & Kernels',
'type': 'SubCategory1'
}, {$addToSet: {'children': db.categories.findOne({'name': 'Pecan Nuts', 'type': 'SubCategory2'})._id}});
db.categories.update({
'name': 'Nuts & Kernels',
'type': 'SubCategory1'
}, {$addToSet: {'children': db.categories.findOne({'name': 'Pine Nuts', 'type': 'SubCategory2'})._id}});
db.categories.update({
'name': 'Nuts & Kernels',
'type': 'SubCategory1'
}, {$addToSet: {'children': db.categories.findOne({'name': 'Pistachio Nuts', 'type': 'SubCategory2'})._id}});
db.categories.update({
'name': 'Nuts & Kernels',
'type': 'SubCategory1'
}, {$addToSet: {'children': db.categories.findOne({'name': 'Pumpkin Kernels', 'type': 'SubCategory2'})._id}});
db.categories.update({
'name': 'Nuts & Kernels',
'type': 'SubCategory1'
}, {$addToSet: {'children': db.categories.findOne({'name': 'Walnuts', 'type': 'SubCategory2'})._id}});

// Adding Sub Category Ornamental Plants to Agriculture category
db.categories.update({
'name': 'Agriculture',
'type': 'MainCategory'
}, {$addToSet: {'children': db.categories.findOne({'name': 'Ornamental Plants', 'type': 'SubCategory1'})._id}});

// Sub Categories of Ornamental Plants

db.categories.insert({'name': 'Bonsai', 'type': 'SubCategory2'});
db.categories.insert({'name': 'Dried Flowers', 'type': 'SubCategory2'});
db.categories.insert({'name': 'Fresh Cut Flowers', 'type': 'SubCategory2'});
db.categories.insert({'name': 'Natural Plants', 'type': 'SubCategory2'});

// Adding Sub Categories to Ornamental Plants
db.categories.update({
'name': 'Ornamental Plants',
'type': 'SubCategory1'
}, {$addToSet: {'children': db.categories.findOne({'name': 'Bonsai', 'type': 'SubCategory2'})._id}});
db.categories.update({
'name': 'Ornamental Plants',
'type': 'SubCategory1'
}, {$addToSet: {'children': db.categories.findOne({'name': 'Dried Flowers', 'type': 'SubCategory2'})._id}});
db.categories.update({
'name': 'Ornamental Plants',
'type': 'SubCategory1'
}, {$addToSet: {'children': db.categories.findOne({'name': 'Fresh Cut Flowers', 'type': 'SubCategory2'})._id}});
db.categories.update({
'name': 'Ornamental Plants',
'type': 'SubCategory1'
}, {$addToSet: {'children': db.categories.findOne({'name': 'Natural Plants', 'type': 'SubCategory2'})._id}});

// Adding Sub Category Other Agriculture Products to Agriculture category
db.categories.update({
'name': 'Agriculture',
'type': 'MainCategory'
}, {
$addToSet: {'children': db.categories.findOne({'name': 'Other Agriculture Products','type': 'SubCategory1'})._id}});

// Adding Sub Category Plant & Animal Oil to Agriculture category
db.categories.update({
'name': 'Agriculture',
'type': 'MainCategory'
}, {$addToSet: {'children': db.categories.findOne({'name': 'Plant & Animal Oil', 'type': 'SubCategory1'})._id}});
// Sub Categories of Plant & Animal Oil

db.categories.insert({'name': 'Animal Oil', 'type': 'SubCategory2'});
db.categories.insert({'name': 'Other Plant', 'type': 'SubCategory2'});
db.categories.insert({'name': 'Plant Oil', 'type': 'SubCategory2'});
// Adding Sub Categories to Plant & Animal Oil
db.categories.update({
'name': 'Plant & Animal Oil',
'type': 'SubCategory1'
}, {$addToSet: {'children': db.categories.findOne({'name': 'Animal Oil', 'type': 'SubCategory2'})._id}});
db.categories.update({
'name': 'Plant & Animal Oil',
'type': 'SubCategory1'
}, {$addToSet: {'children': db.categories.findOne({'name': 'Other Plant', 'type': 'SubCategory2'})._id}});
db.categories.update({
'name': 'Plant & Animal Oil',
'type': 'SubCategory1'
}, {$addToSet: {'children': db.categories.findOne({'name': 'Plant Oil', 'type': 'SubCategory2'})._id}});

// Adding Sub Category Plant Seeds & Bulbs to Agriculture category
db.categories.update({
'name': 'Agriculture',
'type': 'MainCategory'
}, {$addToSet: {'children': db.categories.findOne({'name': 'Plant Seeds & Bulbs', 'type': 'SubCategory1'})._id}});

// Sub Categories of Plant Seeds & Bulbs

db.categories.insert({'name': 'Crop Seeds', 'type': 'SubCategory2'});
db.categories.insert({'name': 'Flower Bulbs', 'type': 'SubCategory2'});
db.categories.insert({'name': 'Forage Seeds', 'type': 'SubCategory2'});
db.categories.insert({'name': 'Fruit Grafts', 'type': 'SubCategory2'});
db.categories.insert({'name': 'Oil Seeds', 'type': 'SubCategory2'});
db.categories.insert({'name': 'Vegetable Seeds', 'type': 'SubCategory2'});

// Adding Sub Categories to Plant Seeds & Bulbs
db.categories.update({
'name': 'Plant Seeds & Bulbs',
'type': 'SubCategory1'
}, {$addToSet: {'children': db.categories.findOne({'name': 'Crop Seeds', 'type': 'SubCategory2'})._id}});
db.categories.update({
'name': 'Plant Seeds & Bulbs',
'type': 'SubCategory1'
}, {$addToSet: {'children': db.categories.findOne({'name': 'Flower Bulbs', 'type': 'SubCategory2'})._id}});
db.categories.update({
'name': 'Plant Seeds & Bulbs',
'type': 'SubCategory1'
}, {$addToSet: {'children': db.categories.findOne({'name': 'Forage Seeds', 'type': 'SubCategory2'})._id}});
db.categories.update({
'name': 'Plant Seeds & Bulbs',
'type': 'SubCategory1'
}, {$addToSet: {'children': db.categories.findOne({'name': 'Fruit Grafts', 'type': 'SubCategory2'})._id}});
db.categories.update({
'name': 'Plant Seeds & Bulbs',
'type': 'SubCategory1'
}, {$addToSet: {'children': db.categories.findOne({'name': 'Oil Seeds', 'type': 'SubCategory2'})._id}});
db.categories.update({
'name': 'Plant Seeds & Bulbs',
'type': 'SubCategory1'
}, {$addToSet: {'children': db.categories.findOne({'name': 'Vegetable Seeds', 'type': 'SubCategory2'})._id}});

// Adding Sub Category Timber Raw Materials to Agriculture category
db.categories.update({
'name': 'Agriculture',
'type': 'MainCategory'
}, {$addToSet: {'children': db.categories.findOne({'name': 'Timber Raw Materials', 'type': 'SubCategory1'})._id}});
// Sub Categories of Timber Raw Materials
db.categories.insert({'name': 'Bamboo Raw Materials', 'type': 'SubCategory2'});
db.categories.insert({'name': 'Logs', 'type': 'SubCategory2'});
// Adding Sub Categories to Timber Raw Materials
db.categories.update({
'name': 'Timber Raw Materials',
'type': 'SubCategory1'
}, {$addToSet: {'children': db.categories.findOne({'name': 'Bamboo Raw Materials', 'type': 'SubCategory2'})._id}});
db.categories.update({
'name': 'Timber Raw Materials',
'type': 'SubCategory1'
}, {$addToSet: {'children': db.categories.findOne({'name': 'Logs', 'type': 'SubCategory2'})._id}});
// Adding Sub Category Vanilla Beans to Agriculture category
db.categories.update({
'name': 'Agriculture',
'type': 'MainCategory'
}, {$addToSet: {'children': db.categories.findOne({'name': 'Vanilla Beans', 'type': 'SubCategory1'})._id}});

// Food & Beverage Main Category
db.categories.insert({'name': 'Food & Beverage', 'type': 'MainCategory'});

// Sub Categories of Food & Beverage
db.categories.insert({'name': 'Alcoholic Beverage', 'type': 'SubCategory1'});
db.categories.insert({'name': 'Baby Food', 'type': 'SubCategory1'});
db.categories.insert({'name': 'Baked Goods', 'type': 'SubCategory1'});
db.categories.insert({'name': 'Bean Products', 'type': 'SubCategory1'});
db.categories.insert({'name': 'Canned Food', 'type': 'SubCategory1'});
db.categories.insert({'name': 'Coffee', 'type': 'SubCategory1'});
db.categories.insert({'name': 'Confectionery', 'type': 'SubCategory1'});
db.categories.insert({'name': 'Dairy Products', 'type': 'SubCategory1'});
db.categories.insert({'name': 'Drinking Water', 'type': 'SubCategory1'});
db.categories.insert({'name': 'Egg & Egg Products', 'type': 'SubCategory1'});
db.categories.insert({'name': 'Food Ingredients', 'type': 'SubCategory1'});
db.categories.insert({'name': 'Fruit Products', 'type': 'SubCategory1'});
db.categories.insert({'name': 'Grain Products', 'type': 'SubCategory1'});
db.categories.insert({'name': 'Honey Products', 'type': 'SubCategory1'});
db.categories.insert({'name': 'Instant Food', 'type': 'SubCategory1'});
db.categories.insert({'name': 'Meat & Poultry', 'type': 'SubCategory1'});
db.categories.insert({'name': 'Other Food & Beverage', 'type': 'SubCategory1'});
db.categories.insert({'name': 'Sea Food', 'type': 'SubCategory1'});
db.categories.insert({'name': 'Seasonings & Condiments', 'type': 'SubCategory1'});
db.categories.insert({'name': 'Slimming Food', 'type': 'SubCategory1'});
db.categories.insert({'name': 'Snack Food', 'type': 'SubCategory1'});
db.categories.insert({'name': 'Soft Drinks', 'type': 'SubCategory1'});
db.categories.insert({'name': 'Tea', 'type': 'SubCategory1'});
db.categories.insert({'name': 'Vegetable Products', 'type': 'SubCategory1'});

// Adding Sub Category Alcoholic Beverage to Food & Beverage category
db.categories.update({
'name': 'Food & Beverage',
'type': 'MainCategory'
}, {$addToSet: {'children': db.categories.findOne({'name': 'Alcoholic Beverage', 'type': 'SubCategory1'})._id}});

// Sub Categories of Alcoholic Beverage
db.categories.insert({'name': 'Beer', 'type': 'SubCategory2'});
db.categories.insert({'name': 'Cocktails', 'type': 'SubCategory2'});
db.categories.insert({'name': 'Fruit Wine', 'type': 'SubCategory2'});
db.categories.insert({'name': 'Liqueurs', 'type': 'SubCategory2'});
db.categories.insert({'name': 'Rice Wine', 'type': 'SubCategory2'});
db.categories.insert({'name': 'Spirits', 'type': 'SubCategory2'});
db.categories.insert({'name': 'Wine', 'type': 'SubCategory2'});

// Adding Sub Categories to Alcoholic Beverage category
db.categories.update({
'name': 'Alcoholic Beverage',
'type': 'SubCategory1'
}, {$addToSet: {'children': db.categories.findOne({'name': 'Beer', 'type': 'SubCategory2'})._id}});
db.categories.update({
'name': 'Alcoholic Beverage',
'type': 'SubCategory1'
}, {$addToSet: {'children': db.categories.findOne({'name': 'Cocktails', 'type': 'SubCategory2'})._id}});
db.categories.update({
'name': 'Alcoholic Beverage',
'type': 'SubCategory1'
}, {$addToSet: {'children': db.categories.findOne({'name': 'Fruit Wine', 'type': 'SubCategory2'})._id}});
db.categories.update({
'name': 'Alcoholic Beverage',
'type': 'SubCategory1'
}, {$addToSet: {'children': db.categories.findOne({'name': 'Liqueurs', 'type': 'SubCategory2'})._id}});
db.categories.update({
'name': 'Alcoholic Beverage',
'type': 'SubCategory1'
}, {$addToSet: {'children': db.categories.findOne({'name': 'Rice Wine', 'type': 'SubCategory2'})._id}});
db.categories.update({
'name': 'Alcoholic Beverage',
'type': 'SubCategory1'
}, {$addToSet: {'children': db.categories.findOne({'name': 'Spirits', 'type': 'SubCategory2'})._id}});
db.categories.update({
'name': 'Alcoholic Beverage',
'type': 'SubCategory1'
}, {$addToSet: {'children': db.categories.findOne({'name': 'Wine', 'type': 'SubCategory2'})._id}});

// Adding Sub Category Baby Food to Food & Beverage category
db.categories.update({
'name': 'Food & Beverage',
'type': 'MainCategory'
}, {$addToSet: {'children': db.categories.findOne({'name': 'Baby Food', 'type': 'SubCategory1'})._id}});

// Sub Categories of Baby Food

db.categories.insert({'name': 'Baby Biscuits', 'type': 'SubCategory2'});
db.categories.insert({'name': 'Baby Cereal', 'type': 'SubCategory2'});
db.categories.insert({'name': 'Baby Formula', 'type': 'SubCategory2'});
db.categories.insert({'name': 'Baby Juice', 'type': 'SubCategory2'});
db.categories.insert({'name': 'Baby Noodles', 'type': 'SubCategory2'});
db.categories.insert({'name': 'Baby Puree', 'type': 'SubCategory2'});
db.categories.insert({'name': 'Meat', 'type': 'SubCategory2'});

// Adding Sub Categories to Baby Food category
db.categories.update({
'name': 'Baby Food',
'type': 'SubCategory1'
}, {$addToSet: {'children': db.categories.findOne({'name': 'Baby Biscuits', 'type': 'SubCategory2'})._id}});
db.categories.update({
'name': 'Baby Food',
'type': 'SubCategory1'
}, {$addToSet: {'children': db.categories.findOne({'name': 'Baby Cereal', 'type': 'SubCategory2'})._id}});
db.categories.update({
'name': 'Baby Food',
'type': 'SubCategory1'
}, {$addToSet: {'children': db.categories.findOne({'name': 'Baby Formula', 'type': 'SubCategory2'})._id}});
db.categories.update({
'name': 'Baby Food',
'type': 'SubCategory1'
}, {$addToSet: {'children': db.categories.findOne({'name': 'Baby Juice', 'type': 'SubCategory2'})._id}});
db.categories.update({
'name': 'Baby Food',
'type': 'SubCategory1'
}, {$addToSet: {'children': db.categories.findOne({'name': 'Baby Noodles', 'type': 'SubCategory2'})._id}});
db.categories.update({
'name': 'Baby Food',
'type': 'SubCategory1'
}, {$addToSet: {'children': db.categories.findOne({'name': 'Baby Puree', 'type': 'SubCategory2'})._id}});
db.categories.update({
'name': 'Baby Food',
'type': 'SubCategory1'
}, {$addToSet: {'children': db.categories.findOne({'name': 'Meat', 'type': 'SubCategory2'})._id}});

// Adding Sub Category  Baked Goods to Food & Beverage category
db.categories.update({
'name': 'Food & Beverage',
'type': 'MainCategory'
}, {$addToSet: {'children': db.categories.findOne({'name': 'Baked Goods', 'type': 'SubCategory1'})._id}});

// Sub Categories of Baked Goods

db.categories.insert({'name': 'Biscuits', 'type': 'SubCategory2'});
db.categories.insert({'name': 'Bread', 'type': 'SubCategory2'});
db.categories.insert({'name': 'Breadcrumbs', 'type': 'SubCategory2'});
db.categories.insert({'name': 'Cakes', 'type': 'SubCategory2'});
db.categories.insert({'name': 'Ice cream cones', 'type': 'SubCategory2'});
db.categories.insert({'name': 'Mooncakes', 'type': 'SubCategory2'});
db.categories.insert({'name': 'Pastries', 'type': 'SubCategory2'});
db.categories.insert({'name': 'Pizza', 'type': 'SubCategory2'});
db.categories.insert({'name': 'Tortillas', 'type': 'SubCategory2'});

// Adding Sub Categories to Baked Goods category
db.categories.update({
'name': 'Baked Goods',
'type': 'SubCategory1'
}, {$addToSet: {'children': db.categories.findOne({'name': 'Biscuits', 'type': 'SubCategory2'})._id}});
db.categories.update({
'name': 'Baked Goods',
'type': 'SubCategory1'
}, {$addToSet: {'children': db.categories.findOne({'name': 'Bread', 'type': 'SubCategory2'})._id}});
db.categories.update({
'name': 'Baked Goods',
'type': 'SubCategory1'
}, {$addToSet: {'children': db.categories.findOne({'name': 'Breadcrumbs', 'type': 'SubCategory2'})._id}});
db.categories.update({
'name': 'Baked Goods',
'type': 'SubCategory1'
}, {$addToSet: {'children': db.categories.findOne({'name': 'Cakes', 'type': 'SubCategory2'})._id}});
db.categories.update({
'name': 'Baked Goods',
'type': 'SubCategory1'
}, {$addToSet: {'children': db.categories.findOne({'name': 'Ice cream cones', 'type': 'SubCategory2'})._id}});
db.categories.update({
'name': 'Baked Goods',
'type': 'SubCategory1'
}, {$addToSet: {'children': db.categories.findOne({'name': 'Mooncakes', 'type': 'SubCategory2'})._id}});
db.categories.update({
'name': 'Baked Goods',
'type': 'SubCategory1'
}, {$addToSet: {'children': db.categories.findOne({'name': 'Pastries', 'type': 'SubCategory2'})._id}});
db.categories.update({
'name': 'Baked Goods',
'type': 'SubCategory1'
}, {$addToSet: {'children': db.categories.findOne({'name': 'Pizza', 'type': 'SubCategory2'})._id}});
db.categories.update({
'name': 'Baked Goods',
'type': 'SubCategory1'
}, {$addToSet: {'children': db.categories.findOne({'name': 'Tortillas', 'type': 'SubCategory2'})._id}});

// Adding Sub Category Bean Products to Food & Beverage category

db.categories.update({
'name': 'Food & Beverage',
'type': 'MainCategory'
}, {$addToSet: {'children': db.categories.findOne({'name': 'Bean Products', 'type': 'SubCategory1'})._id}});

// Sub Categories of Bean Products

db.categories.insert({'name': 'Bean Sprouts', 'type': 'SubCategory2'});
db.categories.insert({'name': 'Natto', 'type': 'SubCategory2'});
db.categories.insert({'name': 'Soy Milk', 'type': 'SubCategory2'});

// Adding Sub Categories to Bean Products category
db.categories.update({
'name': 'Bean Products',
'type': 'SubCategory1'
}, {$addToSet: {'children': db.categories.findOne({'name': 'Bean Sprouts', 'type': 'SubCategory2'})._id}});
db.categories.update({
'name': 'Bean Products',
'type': 'SubCategory1'
}, {$addToSet: {'children': db.categories.findOne({'name': 'Natto', 'type': 'SubCategory2'})._id}});
db.categories.update({
'name': 'Bean Products',
'type': 'SubCategory1'
}, {$addToSet: {'children': db.categories.findOne({'name': 'Soy Milk', 'type': 'SubCategory2'})._id}});

// Adding Sub Category Canned Food to Food & Beverage category

db.categories.update({
'name': 'Food & Beverage',
'type': 'MainCategory'
}, {$addToSet: {'children': db.categories.findOne({'name': 'Canned Food', 'type': 'SubCategory1'})._id}});

// Adding Sub Category Coffee to Food & Beverage category

db.categories.update({
'name': 'Food & Beverage',
'type': 'MainCategory'
}, {$addToSet: {'children': db.categories.findOne({'name': 'Coffee', 'type': 'SubCategory1'})._id}});

// Sub Categories of Coffee

db.categories.insert({'name': 'Coffee Beans', 'type': 'SubCategory2'});
db.categories.insert({'name': 'Ground Coffee', 'type': 'SubCategory2'});
db.categories.insert({'name': 'Instant Coffee', 'type': 'SubCategory2'});
db.categories.insert({'name': 'Whole Bean Coffee', 'type': 'SubCategory2'});

// Adding Sub Categories to Coffee category
db.categories.update({
'name': 'Coffee',
'type': 'SubCategory1'
}, {$addToSet: {'children': db.categories.findOne({'name': 'Coffee Beans', 'type': 'SubCategory2'})._id}});
db.categories.update({
'name': 'Coffee',
'type': 'SubCategory1'
}, {$addToSet: {'children': db.categories.findOne({'name': 'Ground Coffee', 'type': 'SubCategory2'})._id}});
db.categories.update({
'name': 'Coffee',
'type': 'SubCategory1'
}, {$addToSet: {'children': db.categories.findOne({'name': 'Instant Coffee', 'type': 'SubCategory2'})._id}});
db.categories.update({
'name': 'Coffee',
'type': 'SubCategory1'
}, {$addToSet: {'children': db.categories.findOne({'name': 'Whole Bean Coffee', 'type': 'SubCategory2'})._id}});

// Adding Sub Category Confectionery to Food & Beverage category

db.categories.update({
'name': 'Food & Beverage',
'type': 'MainCategory'
}, {$addToSet: {'children': db.categories.findOne({'name': 'Confectionery', 'type': 'SubCategory1'})._id}});

// Sub Categories of Confectionery
db.categories.insert({'name': 'Candy', 'type': 'SubCategory2'});
db.categories.insert({'name': 'Chocolate', 'type': 'SubCategory2'});
db.categories.insert({'name': 'Gum', 'type': 'SubCategory2'});
db.categories.insert({'name': 'Liquid Candy', 'type': 'SubCategory2'});

// Adding Sub Categories to Confectionery category
db.categories.update({
'name': 'Confectionery',
'type': 'SubCategory1'
}, {$addToSet: {'children': db.categories.findOne({'name': 'Candy', 'type': 'SubCategory2'})._id}}); 
db.categories.update({
'name': 'Confectionery',
'type': 'SubCategory1'
}, {$addToSet: {'children': db.categories.findOne({'name': 'Chocolate', 'type': 'SubCategory2'})._id}});
db.categories.update({
'name': 'Confectionery',
'type': 'SubCategory1'
}, {$addToSet: {'children': db.categories.findOne({'name': 'Gum', 'type': 'SubCategory2'})._id}});
db.categories.update({
'name': 'Confectionery',
'type': 'SubCategory1'
}, {$addToSet: {'children': db.categories.findOne({'name': 'Liquid Candy', 'type': 'SubCategory2'})._id}});

// Adding Sub Category Dairy Products to Food & Beverage category

db.categories.update({
'name': 'Food & Beverage',
'type': 'MainCategory'
}, {$addToSet: {'children': db.categories.findOne({'name': 'Dairy Products', 'type': 'SubCategory1'})._id}});

// Sub Categories of Dairy Products
db.categories.insert({'name': 'Butter', 'type': 'SubCategory2'});
db.categories.insert({'name': 'Cheese', 'type': 'SubCategory2'});
db.categories.insert({'name': 'Condensed Milk', 'type': 'SubCategory2'});
db.categories.insert({'name': 'Cream', 'type': 'SubCategory2'});
db.categories.insert({'name': 'Dairy Extractive', 'type': 'SubCategory2'});
db.categories.insert({'name': 'Ice Cream', 'type': 'SubCategory2'});
db.categories.insert({'name': 'Milk Powder', 'type': 'SubCategory2'});
db.categories.insert({'name': 'Yogurt', 'type': 'SubCategory2'});

// Adding Sub Categories to Dairy Products category
db.categories.update({
'name': 'Dairy Products',
'type': 'SubCategory1'
}, {$addToSet: {'children': db.categories.findOne({'name': 'Butter', 'type': 'SubCategory2'})._id}}); 
db.categories.update({
'name': 'Dairy Products',
'type': 'SubCategory1'
}, {$addToSet: {'children': db.categories.findOne({'name': 'Cheese', 'type': 'SubCategory2'})._id}}); 
db.categories.update({
'name': 'Dairy Products',
'type': 'SubCategory1'
}, {$addToSet: {'children': db.categories.findOne({'name': 'Condensed Milk', 'type': 'SubCategory2'})._id}}); 
db.categories.update({
'name': 'Dairy Products',
'type': 'SubCategory1'
}, {$addToSet: {'children': db.categories.findOne({'name': 'Cream', 'type': 'SubCategory2'})._id}}); 
db.categories.update({
'name': 'Dairy Products',
'type': 'SubCategory1'
}, {$addToSet: {'children': db.categories.findOne({'name': 'Dairy Extractive', 'type': 'SubCategory2'})._id}}); 
db.categories.update({
'name': 'Dairy Products',
'type': 'SubCategory1'
}, {$addToSet: {'children': db.categories.findOne({'name': 'Ice Cream', 'type': 'SubCategory2'})._id}}); 
db.categories.update({
'name': 'Dairy Products',
'type': 'SubCategory1'
}, {$addToSet: {'children': db.categories.findOne({'name': 'Milk Powder', 'type': 'SubCategory2'})._id}}); 
db.categories.update({
'name': 'Dairy Products',
'type': 'SubCategory1'
}, {$addToSet: {'children': db.categories.findOne({'name': 'Yogurt', 'type': 'SubCategory2'})._id}}); 

// Adding Sub Category Drinking Water to Food & Beverage category
db.categories.update({
'name': 'Food & Beverage',
'type': 'MainCategory'
}, {$addToSet: {'children': db.categories.findOne({'name': 'Drinking Water', 'type': 'SubCategory1'})._id}});

// Sub Categories of Dairy Products
db.categories.insert({'name': 'Mineral Water', 'type': 'SubCategory2'});
db.categories.insert({'name': 'Pure Water', 'type': 'SubCategory2'});
db.categories.insert({'name': 'Sparkling Water', 'type': 'SubCategory2'});

// Adding Sub Categories to Drinking Water category
db.categories.update({
'name': 'Drinking Water',
'type': 'SubCategory1'
}, {$addToSet: {'children': db.categories.findOne({'name': 'Mineral Water', 'type': 'SubCategory2'})._id}}); 
db.categories.update({
'name': 'Drinking Water',
'type': 'SubCategory1'
}, {$addToSet: {'children': db.categories.findOne({'name': 'Pure Water', 'type': 'SubCategory2'})._id}}); 
db.categories.update({
'name': 'Drinking Water',
'type': 'SubCategory1'
}, {$addToSet: {'children': db.categories.findOne({'name': 'Sparkling Water', 'type': 'SubCategory2'})._id}}); 

// Adding Sub Category Egg & Egg Products to Food & Beverage category
db.categories.update({
'name': 'Food & Beverage',
'type': 'MainCategory'
}, {$addToSet: {'children': db.categories.findOne({'name': 'Egg & Egg Products', 'type': 'SubCategory1'})._id}});

// Adding Sub Category Food Ingredients to Food & Beverage category
db.categories.update({
'name': 'Food & Beverage',
'type': 'MainCategory'
}, {$addToSet: {'children': db.categories.findOne({'name': 'Food Ingredients', 'type': 'SubCategory1'})._id}});

// Adding Sub Category Fruit Products to Food & Beverage category
db.categories.update({
'name': 'Food & Beverage',
'type': 'MainCategory'
}, {$addToSet: {'children': db.categories.findOne({'name': 'Fruit Products', 'type': 'SubCategory1'})._id}});

// Sub Categories of Fruit Products
db.categories.insert({'name': 'Dried Fruit', 'type': 'SubCategory2'});
db.categories.insert({'name': 'Frozen Fruit', 'type': 'SubCategory2'});
db.categories.insert({'name': 'Preserved Fruit', 'type': 'SubCategory2'});

// Adding Sub Categories to Fruit Products category
db.categories.update({
'name': 'Fruit Products',
'type': 'SubCategory1'
}, {$addToSet: {'children': db.categories.findOne({'name': 'Dried Fruit', 'type': 'SubCategory2'})._id}}); 
db.categories.update({
'name': 'Fruit Products',
'type': 'SubCategory1'
}, {$addToSet: {'children': db.categories.findOne({'name': 'Frozen Fruit', 'type': 'SubCategory2'})._id}}); 
db.categories.update({
'name': 'Fruit Products',
'type': 'SubCategory1'
}, {$addToSet: {'children': db.categories.findOne({'name': 'Preserved Fruit', 'type': 'SubCategory2'})._id}}); 

// Adding Sub Category Grain Products to Food & Beverage category
db.categories.update({
'name': 'Food & Beverage',
'type': 'MainCategory'
}, {$addToSet: {'children': db.categories.findOne({'name': 'Grain Products', 'type': 'SubCategory1'})._id}});

// Sub Categories of Grain Products
db.categories.insert({'name': 'Breakfast Cereal', 'type': 'SubCategory2'});
db.categories.insert({'name': 'Chinese Snack', 'type': 'SubCategory2'});
db.categories.insert({'name': 'Coarse Cereal Products', 'type': 'SubCategory2'});
db.categories.insert({'name': 'Flour', 'type': 'SubCategory2'});
db.categories.insert({'name': 'Gluten', 'type': 'SubCategory2'});
db.categories.insert({'name': 'Noodles', 'type': 'SubCategory2'});
db.categories.insert({'name': 'Pasta', 'type': 'SubCategory2'});
db.categories.insert({'name': 'Rice Noodle', 'type': 'SubCategory2'});

// Adding Sub Categories to Grain Products category
db.categories.update({
'name': 'Grain Products',
'type': 'SubCategory1'
}, {$addToSet: {'children': db.categories.findOne({'name': 'Breakfast Cereal', 'type': 'SubCategory2'})._id}}); 
db.categories.update({
'name': 'Grain Products',
'type': 'SubCategory1'
}, {$addToSet: {'children': db.categories.findOne({'name': 'Chinese Snack', 'type': 'SubCategory2'})._id}});
db.categories.update({
'name': 'Grain Products',
'type': 'SubCategory1'
}, {$addToSet: {'children': db.categories.findOne({'name': 'Coarse Cereal Products', 'type': 'SubCategory2'})._id}});
db.categories.update({
'name': 'Grain Products',
'type': 'SubCategory1'
}, {$addToSet: {'children': db.categories.findOne({'name': 'Flour', 'type': 'SubCategory2'})._id}});
db.categories.update({
'name': 'Grain Products',
'type': 'SubCategory1'
}, {$addToSet: {'children': db.categories.findOne({'name': 'Gluten', 'type': 'SubCategory2'})._id}});
db.categories.update({
'name': 'Grain Products',
'type': 'SubCategory1'
}, {$addToSet: {'children': db.categories.findOne({'name': 'Noodles', 'type': 'SubCategory2'})._id}});
db.categories.update({
'name': 'Grain Products',
'type': 'SubCategory1'
}, {$addToSet: {'children': db.categories.findOne({'name': 'Pasta', 'type': 'SubCategory2'})._id}});
db.categories.update({
'name': 'Grain Products',
'type': 'SubCategory1'
}, {$addToSet: {'children': db.categories.findOne({'name': 'Rice Noodle', 'type': 'SubCategory2'})._id}});
 
// Adding Sub Category Honey Products to Food & Beverage category
db.categories.update({
'name': 'Food & Beverage',
'type': 'MainCategory'
}, {$addToSet: {'children': db.categories.findOne({'name': 'Honey Products', 'type': 'SubCategory1'})._id}});

// Sub Categories of Honey Products
db.categories.insert({'name': 'Bee Pollen', 'type': 'SubCategory2'});
db.categories.insert({'name': 'Bee Wax', 'type': 'SubCategory2'});
db.categories.insert({'name': 'Honey', 'type': 'SubCategory2'});
db.categories.insert({'name': 'Honey Syrup', 'type': 'SubCategory2'});
db.categories.insert({'name': 'Propolis', 'type': 'SubCategory2'});
db.categories.insert({'name': 'Royal Jelly', 'type': 'SubCategory2'});

// Adding Sub Categories to Honey Products category
db.categories.update({
'name': 'Honey Products',
'type': 'SubCategory1'
}, {$addToSet: {'children': db.categories.findOne({'name': 'Bee Pollen', 'type': 'SubCategory2'})._id}}); 
db.categories.update({
'name': 'Honey Products',
'type': 'SubCategory1'
}, {$addToSet: {'children': db.categories.findOne({'name': 'Bee Wax', 'type': 'SubCategory2'})._id}}); 
db.categories.update({
'name': 'Honey Products',
'type': 'SubCategory1'
}, {$addToSet: {'children': db.categories.findOne({'name': 'Honey', 'type': 'SubCategory2'})._id}}); 
db.categories.update({
'name': 'Honey Products',
'type': 'SubCategory1'
}, {$addToSet: {'children': db.categories.findOne({'name': 'Honey Syrup', 'type': 'SubCategory2'})._id}}); 
db.categories.update({
'name': 'Honey Products',
'type': 'SubCategory1'
}, {$addToSet: {'children': db.categories.findOne({'name': 'Propolis', 'type': 'SubCategory2'})._id}}); 
db.categories.update({
'name': 'Honey Products',
'type': 'SubCategory1'
}, {$addToSet: {'children': db.categories.findOne({'name': 'Royal Jelly', 'type': 'SubCategory2'})._id}}); 

// Adding Sub Category Instant Food to Food & Beverage category
db.categories.update({
'name': 'Food & Beverage',
'type': 'MainCategory'
}, {$addToSet: {'children': db.categories.findOne({'name': 'Instant Food', 'type': 'SubCategory1'})._id}});

// Sub Categories of Instant Food
db.categories.insert({'name': 'Hamburger', 'type': 'SubCategory2'});
db.categories.insert({'name': 'Instant Noodles', 'type': 'SubCategory2'});
db.categories.insert({'name': 'Instant Rice', 'type': 'SubCategory2'});
db.categories.insert({'name': 'Instant Soup', 'type': 'SubCategory2'});
db.categories.insert({'name': 'Meat Dishes', 'type': 'SubCategory2'});
db.categories.insert({'name': 'Sandwiches', 'type': 'SubCategory2'}); 	
db.categories.insert({'name': 'Seafood Dishes', 'type': 'SubCategory2'});
db.categories.insert({'name': 'Sushi', 'type': 'SubCategory2'});
db.categories.insert({'name': 'Vegetable Dishes', 'type': 'SubCategory2'});
 
// Adding Sub Categories to Instant Food category
db.categories.update({
'name': 'Instant Food',
'type': 'SubCategory1'
}, {$addToSet: {'children': db.categories.findOne({'name': 'Hamburger', 'type': 'SubCategory2'})._id}}); 
db.categories.update({
'name': 'Instant Food',
'type': 'SubCategory1'
}, {$addToSet: {'children': db.categories.findOne({'name': 'Instant Noodles', 'type': 'SubCategory2'})._id}}); 

db.categories.update({
'name': 'Instant Food',
'type': 'SubCategory1'
}, {$addToSet: {'children': db.categories.findOne({'name': 'Instant Rice', 'type': 'SubCategory2'})._id}}); 

db.categories.update({
'name': 'Instant Food',
'type': 'SubCategory1'
}, {$addToSet: {'children': db.categories.findOne({'name': 'Instant Soup', 'type': 'SubCategory2'})._id}}); 

db.categories.update({
'name': 'Instant Food',
'type': 'SubCategory1'
}, {$addToSet: {'children': db.categories.findOne({'name': 'Meat Dishes', 'type': 'SubCategory2'})._id}}); 

db.categories.update({
'name': 'Instant Food',
'type': 'SubCategory1'
}, {$addToSet: {'children': db.categories.findOne({'name': 'Sandwiches', 'type': 'SubCategory2'})._id}}); 

db.categories.update({
'name': 'Instant Food',
'type': 'SubCategory1'
}, {$addToSet: {'children': db.categories.findOne({'name': 'Seafood Dishes', 'type': 'SubCategory2'})._id}}); 

db.categories.update({
'name': 'Instant Food',
'type': 'SubCategory1'
}, {$addToSet: {'children': db.categories.findOne({'name': 'Sushi', 'type': 'SubCategory2'})._id}}); 

db.categories.update({
'name': 'Instant Food',
'type': 'SubCategory1'
}, {$addToSet: {'children': db.categories.findOne({'name': 'Vegetable Dishes', 'type': 'SubCategory2'})._id}}); 

// Adding Sub Category Meat & Poultry to Food & Beverage category
db.categories.update({
'name': 'Food & Beverage',
'type': 'MainCategory'
}, {$addToSet: {'children': db.categories.findOne({'name': 'Meat & Poultry', 'type': 'SubCategory1'})._id}});

// Sub Categories of Meat & Poultry
db.categories.insert({'name': 'Chicken', 'type': 'SubCategory2'});
db.categories.insert({'name': 'Goat Meat', 'type': 'SubCategory2'});
db.categories.insert({'name': 'Meat Product', 'type': 'SubCategory2'});
db.categories.insert({'name': 'Pork', 'type': 'SubCategory2'});
db.categories.insert({'name': 'Poultry Meat', 'type': 'SubCategory2'});
db.categories.insert({'name': 'Sheep Meat', 'type': 'SubCategory2'});
db.categories.insert({'name': 'Vension', 'type': 'SubCategory2'});

// Adding Sub Categories to Meat & Poultry category
db.categories.update({
'name': 'Meat & Poultry',
'type': 'SubCategory1'
}, {$addToSet: {'children': db.categories.findOne({'name': 'Chicken', 'type': 'SubCategory2'})._id}}); 
db.categories.update({
'name': 'Meat & Poultry',
'type': 'SubCategory1'
}, {$addToSet: {'children': db.categories.findOne({'name': 'Goat Meat', 'type': 'SubCategory2'})._id}}); 
db.categories.update({
'name': 'Meat & Poultry',
'type': 'SubCategory1'
}, {$addToSet: {'children': db.categories.findOne({'name': 'Meat Product', 'type': 'SubCategory2'})._id}}); 
db.categories.update({
'name': 'Meat & Poultry',
'type': 'SubCategory1'
}, {$addToSet: {'children': db.categories.findOne({'name': 'Pork', 'type': 'SubCategory2'})._id}}); 
db.categories.update({
'name': 'Meat & Poultry',
'type': 'SubCategory1'
}, {$addToSet: {'children': db.categories.findOne({'name': 'Poultry Meat', 'type': 'SubCategory2'})._id}}); 
db.categories.update({
'name': 'Meat & Poultry',
'type': 'SubCategory1'
}, {$addToSet: {'children': db.categories.findOne({'name': 'Sheep Meat', 'type': 'SubCategory2'})._id}}); 
db.categories.update({
'name': 'Meat & Poultry',
'type': 'SubCategory1'
}, {$addToSet: {'children': db.categories.findOne({'name': 'Vension', 'type': 'SubCategory2'})._id}}); 
 
// Adding Sub Category Other Food & Beverage to Food & Beverage category
db.categories.update({
'name': 'Food & Beverage',
'type': 'MainCategory'
}, {$addToSet: {'children': db.categories.findOne({'name': 'Other Food & Beverage', 'type': 'SubCategory1'})._id}});

// Adding Sub Category Sea Food to Food & Beverage category
db.categories.update({
'name': 'Food & Beverage',
'type': 'MainCategory'
}, {$addToSet: {'children': db.categories.findOne({'name': 'Sea Food', 'type': 'SubCategory1'})._id}});

// Sub Categories of Sea Food
db.categories.insert({'name': 'Crab', 'type': 'SubCategory2'});
db.categories.insert({'name': 'Cuttlefish', 'type': 'SubCategory2'});
db.categories.insert({'name': 'Fish', 'type': 'SubCategory2'});
db.categories.insert({'name': 'Fish Ball', 'type': 'SubCategory2'});
db.categories.insert({'name': 'Fish Sausage', 'type': 'SubCategory2'});
db.categories.insert({'name': 'Other Sea Food', 'type': 'SubCategory2'});

// Adding Sub Categories to Sea Food category
db.categories.update({
'name': 'Sea Food',
'type': 'SubCategory1'
}, {$addToSet: {'children': db.categories.findOne({'name': 'Crab', 'type': 'SubCategory2'})._id}}); 
db.categories.update({
'name': 'Sea Food',
'type': 'SubCategory1'
}, {$addToSet: {'children': db.categories.findOne({'name': 'Cuttlefish', 'type': 'SubCategory2'})._id}}); 
db.categories.update({
'name': 'Sea Food',
'type': 'SubCategory1'
}, {$addToSet: {'children': db.categories.findOne({'name': 'Fish', 'type': 'SubCategory2'})._id}}); 
db.categories.update({
'name': 'Sea Food',
'type': 'SubCategory1'
}, {$addToSet: {'children': db.categories.findOne({'name': 'Fish Ball', 'type': 'SubCategory2'})._id}}); 
db.categories.update({
'name': 'Sea Food',
'type': 'SubCategory1'
}, {$addToSet: {'children': db.categories.findOne({'name': 'Fish Sausage', 'type': 'SubCategory2'})._id}}); 
db.categories.update({
'name': 'Sea Food',
'type': 'SubCategory1'
}, {$addToSet: {'children': db.categories.findOne({'name': 'Other Sea Food', 'type': 'SubCategory2'})._id}}); 

// Adding Sub Category Seasonings & Condiments to Food & Beverage category
db.categories.update({
'name': 'Food & Beverage',
'type': 'MainCategory'
}, {$addToSet: {'children': db.categories.findOne({'name': 'Seasonings & Condiments', 'type': 'SubCategory1'})._id}});

// Sub Categories of Seasonings & Condiments
db.categories.insert({'name': 'Black Bean Sauce', 'type': 'SubCategory2'});
db.categories.insert({'name': 'Fermented Bean Curd', 'type': 'SubCategory2'});
db.categories.insert({'name': 'Hot Pot Condiments', 'type': 'SubCategory2'});
db.categories.insert({'name': 'Pickles', 'type': 'SubCategory2'});
db.categories.insert({'name': 'Puree', 'type': 'SubCategory2'});
db.categories.insert({'name': 'Salt', 'type': 'SubCategory2'});
db.categories.insert({'name': 'Sauce', 'type': 'SubCategory2'});
db.categories.insert({'name': 'Seafood Condiment', 'type': 'SubCategory2'});
db.categories.insert({'name': 'Soy Sauce', 'type': 'SubCategory2'});
db.categories.insert({'name': 'Spices', 'type': 'SubCategory2'});
db.categories.insert({'name': 'Sugar', 'type': 'SubCategory2'});
db.categories.insert({'name': 'Vinegar', 'type': 'SubCategory2'});

// Adding Sub Categories to Seasonings & Condiments category
db.categories.update({
'name': 'Seasonings & Condiments',
'type': 'SubCategory1'
}, {$addToSet: {'children': db.categories.findOne({'name': 'Black Bean Sauce', 'type': 'SubCategory2'})._id}}); 
db.categories.update({
'name': 'Seasonings & Condiments',
'type': 'SubCategory1'
}, {$addToSet: {'children': db.categories.findOne({'name': 'Fermented Bean Curd', 'type': 'SubCategory2'})._id}}); 
db.categories.update({
'name': 'Seasonings & Condiments',
'type': 'SubCategory1'
}, {$addToSet: {'children': db.categories.findOne({'name': 'Hot Pot Condiments', 'type': 'SubCategory2'})._id}}); 
db.categories.update({
'name': 'Seasonings & Condiments',
'type': 'SubCategory1'
}, {$addToSet: {'children': db.categories.findOne({'name': 'Pickles', 'type': 'SubCategory2'})._id}}); 
db.categories.update({
'name': 'Seasonings & Condiments',
'type': 'SubCategory1'
}, {$addToSet: {'children': db.categories.findOne({'name': 'Puree', 'type': 'SubCategory2'})._id}}); 
db.categories.update({
'name': 'Seasonings & Condiments',
'type': 'SubCategory1'
}, {$addToSet: {'children': db.categories.findOne({'name': 'Salt', 'type': 'SubCategory2'})._id}}); 
db.categories.update({
'name': 'Seasonings & Condiments',
'type': 'SubCategory1'
}, {$addToSet: {'children': db.categories.findOne({'name': 'Sauce', 'type': 'SubCategory2'})._id}}); 
db.categories.update({
'name': 'Seasonings & Condiments',
'type': 'SubCategory1'
}, {$addToSet: {'children': db.categories.findOne({'name': 'Seafood Condiment', 'type': 'SubCategory2'})._id}}); 
db.categories.update({
'name': 'Seasonings & Condiments',
'type': 'SubCategory1'
}, {$addToSet: {'children': db.categories.findOne({'name': 'Soy Sauce', 'type': 'SubCategory2'})._id}}); 
db.categories.update({
'name': 'Seasonings & Condiments',
'type': 'SubCategory1'
}, {$addToSet: {'children': db.categories.findOne({'name': 'Spices', 'type': 'SubCategory2'})._id}}); 
db.categories.update({
'name': 'Seasonings & Condiments',
'type': 'SubCategory1'
}, {$addToSet: {'children': db.categories.findOne({'name': 'Sugar', 'type': 'SubCategory2'})._id}}); 
db.categories.update({
'name': 'Seasonings & Condiments',
'type': 'SubCategory1'
}, {$addToSet: {'children': db.categories.findOne({'name': 'Vinegar', 'type': 'SubCategory2'})._id}}); 
 
// Adding Sub Category Slimming Food to Food & Beverage category
db.categories.update({
'name': 'Food & Beverage',
'type': 'MainCategory'
}, {$addToSet: {'children': db.categories.findOne({'name': 'Slimming Food', 'type': 'SubCategory1'})._id}}); 

// Adding Sub Category Snack Food to Food & Beverage category
db.categories.update({
'name': 'Food & Beverage',
'type': 'MainCategory'
}, {$addToSet: {'children': db.categories.findOne({'name': 'Snack Food', 'type': 'SubCategory1'})._id}}); 

// Sub Categories of Snack Food
db.categories.insert({'name': 'Bean Snacks', 'type': 'SubCategory2'});
db.categories.insert({'name': 'Dairy Snacks', 'type': 'SubCategory2'});
db.categories.insert({'name': 'Egg Snacks', 'type': 'SubCategory2'});
db.categories.insert({'name': 'Fruit', 'type': 'SubCategory2'});
db.categories.insert({'name': 'Grain Snacks', 'type': 'SubCategory2'});
db.categories.insert({'name': 'Jelly', 'type': 'SubCategory2'});
db.categories.insert({'name': 'Meat Snacks', 'type': 'SubCategory2'});
db.categories.insert({'name': 'Nut', 'type': 'SubCategory2'});
db.categories.insert({'name': 'Seafood Snacks', 'type': 'SubCategory2'});

// Adding Sub Categories to Snack Food category
db.categories.update({
'name': 'Snack Food',
'type': 'SubCategory1'
}, {$addToSet: {'children': db.categories.findOne({'name': 'Bean Snacks', 'type': 'SubCategory2'})._id}});  
db.categories.update({
'name': 'Snack Food',
'type': 'SubCategory1'
}, {$addToSet: {'children': db.categories.findOne({'name': 'Dairy Snacks', 'type': 'SubCategory2'})._id}});  
db.categories.update({
'name': 'Snack Food',
'type': 'SubCategory1'
}, {$addToSet: {'children': db.categories.findOne({'name': 'Egg Snacks', 'type': 'SubCategory2'})._id}});  
db.categories.update({
'name': 'Snack Food',
'type': 'SubCategory1'
}, {$addToSet: {'children': db.categories.findOne({'name': 'Fruit', 'type': 'SubCategory2'})._id}});  
db.categories.update({
'name': 'Snack Food',
'type': 'SubCategory1'
}, {$addToSet: {'children': db.categories.findOne({'name': 'Grain Snacks', 'type': 'SubCategory2'})._id}});  
db.categories.update({
'name': 'Snack Food',
'type': 'SubCategory1'
}, {$addToSet: {'children': db.categories.findOne({'name': 'Jelly', 'type': 'SubCategory2'})._id}});  
db.categories.update({
'name': 'Snack Food',
'type': 'SubCategory1'
}, {$addToSet: {'children': db.categories.findOne({'name': 'Meat Snacks', 'type': 'SubCategory2'})._id}});  
db.categories.update({
'name': 'Snack Food',
'type': 'SubCategory1'
}, {$addToSet: {'children': db.categories.findOne({'name': 'Nut', 'type': 'SubCategory2'})._id}});  
db.categories.update({
'name': 'Snack Food',
'type': 'SubCategory1'
}, {$addToSet: {'children': db.categories.findOne({'name': 'Seafood Snacks', 'type': 'SubCategory2'})._id}});  

// Adding Sub Category Soft Drinks to Food & Beverage category
db.categories.update({
'name': 'Food & Beverage',
'type': 'MainCategory'
}, {$addToSet: {'children': db.categories.findOne({'name': 'Soft Drinks', 'type': 'SubCategory1'})._id}});
	
// Sub Categories of Soft Drinks
db.categories.insert({'name': 'Bubble Tea Drinks', 'type': 'SubCategory2'});
db.categories.insert({'name': 'Cacao Drinks', 'type': 'SubCategory2'});
db.categories.insert({'name': 'Coffee Drinks', 'type': 'SubCategory2'});
db.categories.insert({'name': 'Energy Drinks', 'type': 'SubCategory2'});
db.categories.insert({'name': 'Fruit', 'type': 'SubCategory2'});
db.categories.insert({'name': 'Grain', 'type': 'SubCategory2'});
db.categories.insert({'name': 'Tea Drinks', 'type': 'SubCategory2'});
// Adding Sub Categories to Soft Drinks category
db.categories.update({
'name': 'Soft Drinks',
'type': 'SubCategory1'
}, {$addToSet: {'children': db.categories.findOne({'name': 'Bubble Tea Drinks', 'type': 'SubCategory2'})._id}});  
db.categories.update({
'name': 'Soft Drinks',
'type': 'SubCategory1'
}, {$addToSet: {'children': db.categories.findOne({'name': 'Cacao Drinks', 'type': 'SubCategory2'})._id}});
db.categories.update({
'name': 'Soft Drinks',
'type': 'SubCategory1'
}, {$addToSet: {'children': db.categories.findOne({'name': 'Coffee Drinks', 'type': 'SubCategory2'})._id}});
db.categories.update({
'name': 'Soft Drinks',
'type': 'SubCategory1'
}, {$addToSet: {'children': db.categories.findOne({'name': 'Energy Drinks', 'type': 'SubCategory2'})._id}});
db.categories.update({
'name': 'Soft Drinks',
'type': 'SubCategory1'
}, {$addToSet: {'children': db.categories.findOne({'name': 'Fruit', 'type': 'SubCategory2'})._id}});
db.categories.update({
'name': 'Soft Drinks',
'type': 'SubCategory1'
}, {$addToSet: {'children': db.categories.findOne({'name': 'Grain', 'type': 'SubCategory2'})._id}});
db.categories.update({
'name': 'Soft Drinks',
'type': 'SubCategory1'
}, {$addToSet: {'children': db.categories.findOne({'name': 'Tea Drinks', 'type': 'SubCategory2'})._id}});

// Adding Sub Category Tea to Food & Beverage category
db.categories.update({
'name': 'Food & Beverage',
'type': 'MainCategory'
}, {$addToSet: {'children': db.categories.findOne({'name': 'Tea', 'type': 'SubCategory1'})._id}});

// Sub Categories of Tea
db.categories.insert({'name': 'Black Tea', 'type': 'SubCategory2'});
db.categories.insert({'name': 'Blooming Tea', 'type': 'SubCategory2'});
db.categories.insert({'name': 'Flavor Tea', 'type': 'SubCategory2'});
db.categories.insert({'name': 'Green Tea', 'type': 'SubCategory2'});
db.categories.insert({'name': 'White Tea', 'type': 'SubCategory2'});
db.categories.insert({'name': 'Yellow Tea', 'type': 'SubCategory2'}); 	

// Adding Sub Categories to Tea category
db.categories.update({
'name': 'Tea',
'type': 'SubCategory1'
}, {$addToSet: {'children': db.categories.findOne({'name': 'Black Tea', 'type': 'SubCategory2'})._id}});
db.categories.update({
'name': 'Tea',
'type': 'SubCategory1'
}, {$addToSet: {'children': db.categories.findOne({'name': 'Blooming Tea', 'type': 'SubCategory2'})._id}});
db.categories.update({
'name': 'Tea',
'type': 'SubCategory1'
}, {$addToSet: {'children': db.categories.findOne({'name': 'Flavor Tea', 'type': 'SubCategory2'})._id}});
db.categories.update({
'name': 'Tea',
'type': 'SubCategory1'
}, {$addToSet: {'children': db.categories.findOne({'name': 'Green Tea', 'type': 'SubCategory2'})._id}});
db.categories.update({
'name': 'Tea',
'type': 'SubCategory1'
}, {$addToSet: {'children': db.categories.findOne({'name': 'White Tea', 'type': 'SubCategory2'})._id}});
db.categories.update({
'name': 'Tea',
'type': 'SubCategory1'
}, {$addToSet: {'children': db.categories.findOne({'name': 'Yellow Tea', 'type': 'SubCategory2'})._id}});

// Adding Sub Category Vegetable Product to Food & Beverage category
db.categories.update({
'name': 'Food & Beverage',
'type': 'MainCategory'
}, {$addToSet: {'children': db.categories.findOne({'name': 'Vegetable Products', 'type': 'SubCategory1'})._id}});

// Sub Categories of Vegetable Products
db.categories.insert({'name': 'Dried Vegetables', 'type': 'SubCategory2'});
db.categories.insert({'name': 'Frozen Vegetabless', 'type': 'SubCategory2'});
db.categories.insert({'name': 'Preserved Vegetables', 'type': 'SubCategory2'});

// Adding Sub Categories to Vegetable Products category
db.categories.update({
'name': 'Vegetable Products',
'type': 'SubCategory1'
}, {$addToSet: {'children': db.categories.findOne({'name': 'Dried Vegetables', 'type': 'SubCategory2'})._id}}); 
db.categories.update({
'name': 'Vegetable Products',
'type': 'SubCategory1'
}, {$addToSet: {'children': db.categories.findOne({'name': 'Frozen Vegetabless', 'type': 'SubCategory2'})._id}}); 
db.categories.update({
'name': 'Vegetable Products',
'type': 'SubCategory1'
}, {$addToSet: {'children': db.categories.findOne({'name': 'Preserved Vegetables', 'type': 'SubCategory2'})._id}}); 

// Apparel Main Category
db.categories.insert({'name': 'Apparel ', 'type': 'MainCategory'});
// Sub Categories of Apparel
db.categories.insert({'name': 'Apparel Design Services', 'type': 'SubCategory1'});
db.categories.insert({'name': 'Apparel Processing Services', 'type': 'SubCategory1'});
db.categories.insert({'name': 'Apparel Stock', 'type': 'SubCategory1'});
db.categories.insert({'name': 'Boy’s Clothing', 'type': 'SubCategory1'});
db.categories.insert({'name': 'Children’s Clothing', 'type': 'SubCategory1'});
db.categories.insert({'name': 'Coats', 'type': 'SubCategory1'});
db.categories.insert({'name': 'Costumes', 'type': 'SubCategory1'});
db.categories.insert({'name': 'Dresses', 'type': 'SubCategory1'});
db.categories.insert({'name': 'Ethnic Clothing', 'type': 'SubCategory1'});
db.categories.insert({'name': 'Garment Accessories', 'type': 'SubCategory1'});
db.categories.insert({'name': 'Girls’ Clothing', 'type': 'SubCategory1'});
db.categories.insert({'name': 'Hoodies & Sweatshirts', 'type': 'SubCategory1'});
db.categories.insert({'name': 'Hosiery', 'type': 'SubCategory1'});
db.categories.insert({'name': 'Infant & Toddlers Clothing', 'type': 'SubCategory1'});
db.categories.insert({'name': 'Jackets', 'type': 'SubCategory1'});
db.categories.insert({'name': 'Jeans', 'type': 'SubCategory1'});
db.categories.insert({'name': 'Ladies’ Blouses & Tops', 'type': 'SubCategory1'});
db.categories.insert({'name': 'Mannequins', 'type': 'SubCategory1'});
db.categories.insert({'name': 'Maternity Clothing', 'type': 'SubCategory1'});
db.categories.insert({'name': 'Men’s Clothing', 'type': 'SubCategory1'});
db.categories.insert({'name': 'Men’s Shirts', 'type': 'SubCategory1'});
db.categories.insert({'name': 'Organic Cotton Clothing', 'type': 'SubCategory1'});
db.categories.insert({'name': 'Other Apparel', 'type': 'SubCategory1'});
db.categories.insert({'name': 'Pants & Trousers', 'type': 'SubCategory1'});
db.categories.insert({'name': 'Plus Size Clothing', 'type': 'SubCategory1'});
db.categories.insert({'name': 'Sewing Supplies', 'type': 'SubCategory1'});
db.categories.insert({'name': 'Shorts', 'type': 'SubCategory1'});
db.categories.insert({'name': 'Skirts', 'type': 'SubCategory1'});
db.categories.insert({'name': 'Sleepwear', 'type': 'SubCategory1'});
db.categories.insert({'name': 'Sportswear', 'type': 'SubCategory1'});
db.categories.insert({'name': 'Stage & Dance Wear', 'type': 'SubCategory1'});
db.categories.insert({'name': 'Suits & Tuxedo', 'type': 'SubCategory1'});
db.categories.insert({'name': 'Sweaters', 'type': 'SubCategory1'});
db.categories.insert({'name': 'Tag Guns', 'type': 'SubCategory1'});
db.categories.insert({'name': 'Tank Tops', 'type': 'SubCategory1'});
db.categories.insert({'name': 'T-Shirts', 'type': 'SubCategory1'});
db.categories.insert({'name': 'Underwear', 'type': 'SubCategory1'});
db.categories.insert({'name': 'Uniforms', 'type': 'SubCategory1'});
db.categories.insert({'name': 'Used Clothes', 'type': 'SubCategory1'});
db.categories.insert({'name': 'Vests & Waistcoats', 'type': 'SubCategory1'});
db.categories.insert({'name': 'Wedding Apparel & Accessories', 'type': 'SubCategory1'});
db.categories.insert({'name': 'Women’s Clothing', 'type': 'SubCategory1'});
db.categories.insert({'name': 'Workwear', 'type': 'SubCategory1'});
// Handicrafts & Gifts Main Category
db.categories.insert({'name': 'Handicrafts & Gifts', 'type': 'MainCategory'});
// Sub Categories of Handicrafts & Gifts
db.categories.insert({'name': 'Antique Imitation Crafts', 'type': 'SubCategory1'});
db.categories.insert({'name': 'Art & Collectible', 'type': 'SubCategory1'});
db.categories.insert({'name': 'Artificial Crafts', 'type': 'SubCategory1'});
db.categories.insert({'name': 'Arts & Crafts Stocks', 'type': 'SubCategory1'});
db.categories.insert({'name': 'Bamboo Crafts', 'type': 'SubCategory1'});
db.categories.insert({'name': 'Carving Crafts', 'type': 'SubCategory1'});
db.categories.insert({'name': 'Clay Crafts', 'type': 'SubCategory1'});
db.categories.insert({'name': 'Cross Stitch', 'type': 'SubCategory1'});
db.categories.insert({'name': 'Crystal Crafts', 'type': 'SubCategory1'});
db.categories.insert({'name': 'Embroidery Crafts', 'type': 'SubCategory1'});
db.categories.insert({'name': 'Feng Shui Crafts', 'type': 'SubCategory1'});
db.categories.insert({'name': 'Festive & Party Supplies', 'type': 'SubCategory1'});
db.categories.insert({'name': 'Flags, Banners & Accessories', 'type': 'SubCategory1'});
db.categories.insert({'name': 'Folk Crafts', 'type': 'SubCategory1'});
db.categories.insert({'name': 'Gift Sets', 'type': 'SubCategory1'});
db.categories.insert({'name': 'Glass Crafts', 'type': 'SubCategory1'});
db.categories.insert({'name': 'Gold Leaf Crafts', 'type': 'SubCategory1'});
db.categories.insert({'name': 'Holiday Gifts', 'type': 'SubCategory1'});
db.categories.insert({'name': 'Home Decoration', 'type': 'SubCategory1'});
db.categories.insert({'name': 'Key Chains', 'type': 'SubCategory1'});
db.categories.insert({'name': 'Knitting & Crocheting', 'type': 'SubCategory1'});
db.categories.insert({'name': 'Lacquerware', 'type': 'SubCategory1'});
db.categories.insert({'name': 'Lanyard', 'type': 'SubCategory1'});
db.categories.insert({'name': 'Leather Crafts', 'type': 'SubCategory1'});
db.categories.insert({'name': 'Metal Crafts', 'type': 'SubCategory1'});
db.categories.insert({'name': 'Money Boxes', 'type': 'SubCategory1'});
db.categories.insert({'name': 'Music Boxes', 'type': 'SubCategory1'});
db.categories.insert({'name': 'Natural Crafts', 'type': 'SubCategory1'});
db.categories.insert({'name': 'Nautical Crafts', 'type': 'SubCategory1'});
db.categories.insert({'name': 'Other Gifts & Crafts', 'type': 'SubCategory1'});
db.categories.insert({'name': 'Paper Crafts', 'type': 'SubCategory1'});
db.categories.insert({'name': 'Plastic Crafts', 'type': 'SubCategory1'});
db.categories.insert({'name': 'Pottery & Enamel', 'type': 'SubCategory1'});
db.categories.insert({'name': 'Religious Crafts', 'type': 'SubCategory1'});
db.categories.insert({'name': 'Resin Crafts', 'type': 'SubCategory1'});
db.categories.insert({'name': 'Sculptures', 'type': 'SubCategory1'});
db.categories.insert({'name': 'Semi-Precious Stone Crafts', 'type': 'SubCategory1'});
db.categories.insert({'name': 'Souvenirs', 'type': 'SubCategory1'});
db.categories.insert({'name': 'Stickers', 'type': 'SubCategory1'});
db.categories.insert({'name': 'Stone Crafts', 'type': 'SubCategory1'});
db.categories.insert({'name': 'Textile & Fabric Crafts', 'type': 'SubCategory1'});
db.categories.insert({'name': 'Wedding Decorations & Gifts', 'type': 'SubCategory1'});
db.categories.insert({'name': 'Wicker Crafts', 'type': 'SubCategory1'});
db.categories.insert({'name': 'Wood Crafts', 'type': 'SubCategory1'});

