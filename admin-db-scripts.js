// The following are the scripts for disabling user
db.users.update(
    {
        username: 'ndbabu@icloud.com'
    },
    {
        $set: {
            'status': 'Blocked'
        }
    }, {upsert: false, multi: true}
);

db.companies.update(
    {
        user: db.users.findOne({username: 'ndbabu@icloud.com'})._id
    },
    {
        $set: {
            'disabled': true
        }
    }, {upsert: false, multi: true}
);


// The following are the scripts for enabling user
db.users.update(
    {
        username: 'ndbabu@icloud.com'
    },
    {
        $set: {
            'status': 'Registered'
        }
    }, {upsert: false, multi: true}
);

db.companies.update(
    {
        user: db.users.findOne({username: 'ndbabu@icloud.com'})._id
    },
    {
        $set: {
            'disabled': false
        }
    }, {upsert: false, multi: true}
);
