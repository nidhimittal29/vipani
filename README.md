# nVipani Source Code Repo
This is official nVipani code repository. Welcome to our repository

nVipani is a product based IT start-up pioneered in providing comprehensive Supply Chain Management to Agricultural, Agricultural related FMCG, Manufacturing and Textile industries. nVipani operates from Bengaluru, India and is established in 2015. At nVipani, intent to be a world leader in the B2B market place targeted to Agricultural, Agricultural related FMCG, Manufacturing and Textile industries in next 4 years. nVipani, a flagship product of nVipani, has made early inroads into the B2B market. nVipani is a niche product and an efficient supply chain management solution and that encompasses Business automation, Offer, Contact, Order, and Catalogue management, Inventory, Warehouse and Transportation management, support for Value Added services.

A B2B Marketpalce implemented with MongoDB, Express, AngularJS and Node.js, a.k.a MEAN stack. 

Live demo - Access the app at  http://www.nvipani.com

Company Website - http://www.nvipani.com

## Integration with Build Tools
We are integrating the build tools into repo to improve the health of the code base.

### Build Status
CodeShip Status - ( https://codeship.com/projects/123937/status?branch=master)

[![Build Status](https://travis-ci.org/ndbabu/nvipani.svg?branch=master)](https://travis-ci.org/ndbabu/nvipani)
[![Linux Build Status](https://img.shields.io/travis/jshint/jshint/master.svg?style=flat&label=Linux%20build)](https://travis-ci.org/jshint/jshint)
[![Windows Build status](https://img.shields.io/appveyor/ci/jshint/jshint/master.svg?style=flat&label=Windows%20build)](https://ci.appveyor.com/project/jshint/jshint/branch/master)


### Code Coverage
[![Coverage Status](https://img.shields.io/coveralls/ndbabu/nvipani.svg?style=flat)](https://coveralls.io/r/ndbabu/nvipani?branch=master)
[![Coverage Status](https://img.shields.io/coveralls/ndbabu/nvipani.svg?style=flat)](https://coveralls.io/r/ndbabu/nvipani?branch=master)


### Static Analysis


### Code Climate Metrics

Code Climate <a href="https://codeclimate.com/repos/567a85f4bd3f3b1a7a0010e2/feed"><img src="https://codeclimate.com/repos/567a85f4bd3f3b1a7a0010e2/badges/b236fc1047c4efac1187/gpa.svg" /></a>

Test Coverage <a href="https://codeclimate.com/repos/567a85f4bd3f3b1a7a0010e2/coverage"><img src="https://codeclimate.com/repos/567a85f4bd3f3b1a7a0010e2/badges/b236fc1047c4efac1187/coverage.svg" /></a>

Issues Count <a href="https://codeclimate.com/repos/567a85f4bd3f3b1a7a0010e2/feed"><img src="https://codeclimate.com/repos/567a85f4bd3f3b1a7a0010e2/badges/b236fc1047c4efac1187/issue_count.svg" /></a>

### KLOC Metrics

Versions: Display Versions Information here that are used in this project - http://shields.io/

### Dependencies
[![Dependency Status](https://img.shields.io/david/ndbabu/nvipani.svg?style=flat)](https://david-dm.org/ndbabu/nvipani)
[![devDependency Status](https://img.shields.io/david/dev/ndbabu/nvipani.svg?style=flat)](https://david-dm.org/ndbabu/nvipani#info=devDependencies)

[![Dependency Status](https://david-dm.org/ndbabu/nvipani.svg?theme=shields.io)](https://david-dm.org/ndbabu/nvipani)
[![devDependency Status](https://david-dm.org/ndbabu/nvipani/dev-status.svg?theme=shields.io)](https://david-dm.org/ndbabu/nvipani#info=devDependencies)

## How to Setup your Build
## Clone the repository from github.
$ git clone https://github.com/ndbabu/nvipani.git 

$ cd ./nvipani

# General instructions for install.
prefix sudo to the command to install any product in ubuntu in case of access denied errors.
Refer to package.json for the correct versions of products to install.

# Follow the steps in the link below to install mongo db on ubuntu
https://www.howtoforge.com/tutorial/install-mongodb-on-ubuntu-16.04/

# Install robomongo for mongo client. This is optional.

# Install bower
$ npm install -g bower

# Install grunt
$ npm install -g grunt-cli

# Install all packages
$ npm install

## How to start the nVipani server from command line.

$ node server.js

or

$ grunt

## Install webstorm
# Follow the instructions to Download and install webstorm

https://www.jetbrains.com/help/webstorm/installing-and-launching.html

# Start nvipani server from webstorm
Create an edit configuration from Run Menu. The configuration is automatically picked up and apply.
Run the app from command line if you want to work client alone.  To debug server or in order to work on server, run the server from webstorm.

# Launch nVipani Website
On a browser enter the url: http://localhost:3000

# Test Code Ship integration

## How to run end to end tests

$ npm install -g protractor@2.0.0

$ mkdir -p /var/log/nvipani

  Ensure that mongo is up and running before issuing the below command

$ grunt test:e2e

## How to login to Production - AWS instance of nVipani

sudo ssh -i bb-production.pem ubuntu@ec2-54-254-215-136.ap-southeast-1.compute.amazonaws.com

## How to login to Staging - AWS instance of nVipani

sudo ssh -i nvipani.pem ubuntu@ec2-52-88-172-240.us-west-2.compute.amazonaws.com

## Deploying Steps - Staging and Production

1. git pull
2. grep node and grunt
2.1 ps ax | grep node
3. kill both node and grunt
3.1 ps ax | grep grunt
4. sudo kill -9 16182 31448 31447 31445
5. sudo grunt build
6. sudo nohup grunt production >myprogram55.out &


## How to setup database
# Production

./resetdb.sh nvipani

# For Development Environment run the following scripts from command line.

./resetdb.sh

## How to allow the production user registration.
$ mongo nvipani
 db.users.update({ "username" : "xxxx@nvipani.com" },
   { $set: { "allowRegistration": "true" } });
## How to upgrade the production database.
$ mongo nvipani dbscripts/data-base-upgrade.js
## How to upgrade the production database with specific version.
$ mongo nvipani dbupgradescripts/Beta-<version Number>/data-base-upgrade.js

## How to run mocha test cases.

 $ npm install grunt-mocha-test
 $ webdriver-manager update
 $ grunt test:e2e

 # Release
  git tag -a <tag-name> -m <comments>
  git push origin <tag-name>


## © 2016 Copyright nVipani Technology Solutions Private Limited

