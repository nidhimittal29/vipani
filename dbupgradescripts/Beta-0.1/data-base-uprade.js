// This file will have the data base upgrade scripts.
db.companies.update(
    {},
    {
        $unset: {products: '', bankAccountDetails: ''}, $set: {
        'inventories': [],
        'settings.paymentModes.cash': true,
        'settings.paymentModes.cheque': true,
        'settings.paymentModes.neft': true,
        'settings.paymentModes.rtgs': true,
        'settings.paymentModes.imps': true,
        'settings.paymentModes.online.enabled': false,
        'settings.paymentModes.online.paymentgateway': [],
    }, $rename: {'settings.payments': 'settings.paymentOptions'}
    }, {upsert: false, multi: true}
);

db.companies.update(
    {},
    {
        $set: {
            'bankAccountDetails.bankAccountNumber': '',
            'bankAccountDetails.accountType': 'Current',
            'bankAccountDetails.bankName': '',
            'bankAccountDetails.bankBranchName': '',
            'bankAccountDetails.ifscCode': ''
        }, $rename: {'settings.payments': 'settings.paymentOptions'}
    }, {upsert: false, multi: true}
);
