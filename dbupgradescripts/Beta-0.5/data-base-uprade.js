db.notifications.update(
    {},
    {
        $set: {
            'smsProcessed': false,
            'pushProcessed': false,
            'smsStatusMessage': '',
            'pushStatusMessage': '',
            'criticality': 'Low'
        }
    }, {upsert: false, multi: true}
);

db.notifications.find({}).forEach(function (notificationDoc) {
    if (notificationDoc.processed) {
        notificationDoc.smsProcessed = true;
        notificationDoc.pushProcessed = true;
    }

    db.notifications.save(notificationDoc);
});

// Add millets sub category

db.categories.insert({
    'name': 'Barnyard Millet',
    'code': 'BYMLT',
    'type': 'SubCategory2',
    'categoryImageURL1': 'modules/categories/img/subcategory2/barnyardmillet.png',
    'productCategory': true,
    'productAttributes': {
        brand: true,
        grade: {enabled: true},
        quality: {enabled: true},
        sampleNumber: true,
        fssaiLicenceNumber: true
    },
    'inventoryAttributes': {
        barcode: true,
        batchNumber: true,
        manufactureDate: true,
        packagingDate: true,
        bestBefore: true,
        unitMeasure: ['Kg', 'gms', 'Ton', 'Bag', 'Case', 'Pack']
    },
    'parent': db.categories.findOne({'name': 'Cereals', 'type': 'SubCategory1'})._id
});

db.categories.insert({
    'name': 'Foxtail Millet',
    'code': 'FTMLT',
    'type': 'SubCategory2',
    'categoryImageURL1': 'modules/categories/img/subcategory2/foxtailmillet.png',
    'productCategory': true,
    'productAttributes': {
        brand: true,
        grade: {enabled: true},
        quality: {enabled: true},
        sampleNumber: true,
        fssaiLicenceNumber: true
    },
    'inventoryAttributes': {
        barcode: true,
        batchNumber: true,
        manufactureDate: true,
        packagingDate: true,
        bestBefore: true,
        unitMeasure: ['Kg', 'gms', 'Ton', 'Bag', 'Case', 'Pack']
    },
    'parent': db.categories.findOne({'name': 'Cereals', 'type': 'SubCategory1'})._id
});

db.categories.insert({
    'name': 'Kodo Millet',
    'code': 'KOMLT',
    'type': 'SubCategory2',
    'categoryImageURL1': 'modules/categories/img/subcategory2/kodomillet.png',
    'productCategory': true,
    'productAttributes': {
        brand: true,
        grade: {enabled: true},
        quality: {enabled: true},
        sampleNumber: true,
        fssaiLicenceNumber: true
    },
    'inventoryAttributes': {
        barcode: true,
        batchNumber: true,
        manufactureDate: true,
        packagingDate: true,
        bestBefore: true,
        unitMeasure: ['Kg', 'gms', 'Ton', 'Bag', 'Case', 'Pack']
    },
    'parent': db.categories.findOne({'name': 'Cereals', 'type': 'SubCategory1'})._id
});

db.categories.insert({
    'name': 'Little Millet',
    'code': 'LTMLT',
    'type': 'SubCategory2',
    'categoryImageURL1': 'modules/categories/img/subcategory2/littlemillet.png',
    'productCategory': true,
    'productAttributes': {
        brand: true,
        grade: {enabled: true},
        quality: {enabled: true},
        sampleNumber: true,
        fssaiLicenceNumber: true
    },
    'inventoryAttributes': {
        barcode: true,
        batchNumber: true,
        manufactureDate: true,
        packagingDate: true,
        bestBefore: true,
        unitMeasure: ['Kg', 'gms', 'Ton', 'Bag', 'Case', 'Pack']
    },
    'parent': db.categories.findOne({'name': 'Cereals', 'type': 'SubCategory1'})._id
});

db.categories.insert({
    'name': 'Proso Millet',
    'code': 'PSMLT',
    'type': 'SubCategory2',
    'categoryImageURL1': 'modules/categories/img/subcategory2/prosomillet.png',
    'productCategory': true,
    'productAttributes': {
        brand: true,
        grade: {enabled: true},
        quality: {enabled: true},
        sampleNumber: true,
        fssaiLicenceNumber: true
    },
    'inventoryAttributes': {
        barcode: true,
        batchNumber: true,
        manufactureDate: true,
        packagingDate: true,
        bestBefore: true,
        unitMeasure: ['Kg', 'gms', 'Ton', 'Bag', 'Case', 'Pack']
    },
    'parent': db.categories.findOne({'name': 'Cereals', 'type': 'SubCategory1'})._id
});

// Adding Sub Categories to Yarn category
db.categories.update({
    'name': 'Cereals',
    'type': 'SubCategory1'
}, {$addToSet: {'children': db.categories.findOne({'name': 'Barnyard Millet', 'type': 'SubCategory2'})._id}});

db.categories.update({
    'name': 'Cereals',
    'type': 'SubCategory1'
}, {$addToSet: {'children': db.categories.findOne({'name': 'Foxtail Millet', 'type': 'SubCategory2'})._id}});

db.categories.update({
    'name': 'Cereals',
    'type': 'SubCategory1'
}, {$addToSet: {'children': db.categories.findOne({'name': 'Kodo Millet', 'type': 'SubCategory2'})._id}});

db.categories.update({
    'name': 'Cereals',
    'type': 'SubCategory1'
}, {$addToSet: {'children': db.categories.findOne({'name': 'Little Millet', 'type': 'SubCategory2'})._id}});

db.categories.update({
    'name': 'Cereals',
    'type': 'SubCategory1'
}, {$addToSet: {'children': db.categories.findOne({'name': 'Proso Millet', 'type': 'SubCategory2'})._id}});

db.categories.update(
    {
        'code': {$in: ['RCE', 'WHT', 'PML', 'SOB', 'COR', 'FML', 'TDL', 'UDL', 'MSL', 'CDL', 'KCD', 'HGR', 'SSM', 'PES', 'RGM', 'GNT', 'CDR', 'JER', 'MTD', 'MTH', 'DCL', 'TMD', 'CLV', 'BPR', 'GNR', 'CSD', 'BLF', 'GCM', 'DCN', 'SLT', 'CSI', 'TMC', 'JGP']},
        'type': 'SubCategory2',
    },
    {
        $set: {
            'productAttributes': {
                brand: true,
                grade: {enabled: true},
                quality: {enabled: true},
                sampleNumber: true,
                fssaiLicenceNumber: true
            },
            'inventoryAttributes': {
                barcode: true,
                batchNumber: true,
                manufactureDate: true,
                packagingDate: true,
                bestBefore: true,
                unitMeasure: ['Kg', 'gms', 'Ton', 'Bag', 'Case', 'Pack']
            }
        }
    }, {upsert: false, multi: true}
);


db.categories.update(
    {
        'code': {$in: ['CTB']},
        'type': 'SubCategory2',
    },
    {
        $set: {
            'productAttributes': {
                brand: true,
                grade: {enabled: true},
                quality: {enabled: true},
                sampleNumber: true
            },
            'inventoryAttributes': {
                barcode: true,
                batchNumber: true,
                manufactureDate: true,
                packagingDate: true,
                bestBefore: true,
                unitMeasure: ['Kg', 'Bale', 'Ton', 'Bag', 'Case', 'Pack']
            }
        }
    }, {upsert: false, multi: true}
);

db.categories.update(
    {
        'code': {$in: ['CNS']},
        'type': 'SubCategory2',
    },
    {
        $set: {
            'productAttributes': {
                brand: true,
                grade: {enabled: true},
                quality: {enabled: true},
                sampleNumber: true
            },
            'inventoryAttributes': {
                barcode: true,
                batchNumber: true,
                manufactureDate: true,
                packagingDate: true,
                bestBefore: true,
                unitMeasure: ['Kg', 'gms', 'Ton', 'Bag', 'Case', 'Pack']
            }
        }
    }, {upsert: false, multi: true}
);

