db.categories.update(
    {
        'type': 'SubCategory2'
    },
    {
        $set: {
            'productAttributes.testCertifcate': false,
            'inventoryAttributes.inventoryTestCertifcate': false
        }
    }, {upsert: false, multi: true}
);

db.categories.update(
    {
        'name': 'Agriculture',
        'code': 'AGR',
        'type': 'MainCategory'
    },
    {
        $set: {
            'code': 'AGRIC'
        }
    }, {upsert: false, multi: true}
);

db.categories.update(
    {
        'name': 'Cereals',
        'code': 'CRL',
        'type': 'SubCategory1'
    },
    {
        $set: {
            'code': 'CERLS'
        }
    }, {upsert: false, multi: true}
);

db.categories.update(
    {
        'name': 'Rice',
        'code': 'RCE',
        'type': 'SubCategory2'
    },
    {
        $set: {
            'code': 'RICEG'
        }
    }, {upsert: false, multi: true}
);

db.categories.update(
    {
        'name': 'Wheat',
        'code': 'WHT',
        'type': 'SubCategory2'
    },
    {
        $set: {
            'code': 'WHEAT'
        }
    }, {upsert: false, multi: true}
);

db.categories.update(
    {
        'name': 'Pearl Millet',
        'code': 'PML',
        'type': 'SubCategory2'
    },
    {
        $set: {
            'code': 'PRMLT'
        }
    }, {upsert: false, multi: true}
);

db.categories.update(
    {
        'name': 'Sorghum Bicolor',
        'code': 'SOB',
        'type': 'SubCategory2'
    },
    {
        $set: {
            'code': 'SGMBI'
        }
    }, {upsert: false, multi: true}
);

db.categories.update(
    {
        'name': 'Corn',
        'code': 'COR',
        'type': 'SubCategory2'
    },
    {
        $set: {
            'code': 'CORNG'
        }
    }, {upsert: false, multi: true}
);

db.categories.update(
    {
        'name': 'Finger Millet',
        'code': 'FML',
        'type': 'SubCategory2'
    },
    {
        $set: {
            'code': 'FGMLT'
        }
    }, {upsert: false, multi: true}
);

db.categories.update(
    {
        'name': 'Pulses',
        'code': 'PLS',
        'type': 'SubCategory1'
    },
    {
        $set: {
            'code': 'PULSS'
        }
    }, {upsert: false, multi: true}
);

db.categories.update(
    {
        'name': 'Toor Dal',
        'code': 'TDL',
        'type': 'SubCategory2'
    },
    {
        $set: {
            'code': 'TRDAL'
        }
    }, {upsert: false, multi: true}
);

db.categories.update(
    {
        'name': 'Urad Dal',
        'code': 'UDL',
        'type': 'SubCategory2'
    },
    {
        $set: {
            'name': 'Black Urad Dal Whole',
            'code': 'BUDLW'
        }
    }, {upsert: false, multi: true}
);

db.categories.insert({
    'name': 'Black Urad Dal Split',
    'code': 'BUDLS',
    'type': 'SubCategory2',
    'categoryImageURL1': 'modules/categories/img/subcategory2/blackuraddalsplit.png',
    'productCategory': true,
    'productAttributes': {
        brand: true,
        grade: {enabled: true},
        quality: {enabled: true},
        sampleNumber: true,
        fssaiLicenceNumber: true
    },
    'inventoryAttributes': {
        barcode: true,
        batchNumber: true,
        manufactureDate: true,
        packagingDate: true,
        bestBefore: true,
        unitMeasure: ['Kg', 'gms', 'Ton', 'Bag', 'Case', 'Pack']
    },
    'parent': db.categories.findOne({'name': 'Pulses', 'type': 'SubCategory1'})._id
});

db.categories.update({
    'name': 'Pulses',
    'type': 'SubCategory1'
}, {$addToSet: {'children': db.categories.findOne({'name': 'Black Urad Dal Split', 'type': 'SubCategory2'})._id}});

db.categories.insert({
    'name': 'White Urad Dal Whole',
    'code': 'WUDLW',
    'type': 'SubCategory2',
    'categoryImageURL1': 'modules/categories/img/subcategory2/uraddalwhole.png',
    'productCategory': true,
    'productAttributes': {
        brand: true,
        grade: {enabled: true},
        quality: {enabled: true},
        sampleNumber: true,
        fssaiLicenceNumber: true
    },
    'inventoryAttributes': {
        barcode: true,
        batchNumber: true,
        manufactureDate: true,
        packagingDate: true,
        bestBefore: true,
        unitMeasure: ['Kg', 'gms', 'Ton', 'Bag', 'Case', 'Pack']
    },
    'parent': db.categories.findOne({'name': 'Pulses', 'type': 'SubCategory1'})._id
});

db.categories.insert({
    'name': 'White Urad Dal Split',
    'code': 'WUDLS',
    'type': 'SubCategory2',
    'categoryImageURL1': 'modules/categories/img/subcategory2/uraddalsplit.png',
    'productCategory': true,
    'productAttributes': {
        brand: true,
        grade: {enabled: true},
        quality: {enabled: true},
        sampleNumber: true,
        fssaiLicenceNumber: true
    },
    'inventoryAttributes': {
        barcode: true,
        batchNumber: true,
        manufactureDate: true,
        packagingDate: true,
        bestBefore: true,
        unitMeasure: ['Kg', 'gms', 'Ton', 'Bag', 'Case', 'Pack']
    },
    'parent': db.categories.findOne({'name': 'Pulses', 'type': 'SubCategory1'})._id
});

db.categories.update({
    'name': 'Pulses',
    'type': 'SubCategory1'
}, {$addToSet: {'children': db.categories.findOne({'name': 'White Urad Dal Whole', 'type': 'SubCategory2'})._id}});

db.categories.update({
    'name': 'Pulses',
    'type': 'SubCategory1'
}, {$addToSet: {'children': db.categories.findOne({'name': 'White Urad Dal Split', 'type': 'SubCategory2'})._id}});

db.categories.update(
    {
        'name': 'Masoor Dal',
        'code': 'MSL',
        'type': 'SubCategory2'
    },
    {
        $set: {
            'code': 'MRDAL'
        }
    }, {upsert: false, multi: true}
);

db.categories.update(
    {
        'name': 'Moong Dal',
        'code': 'MDL',
        'type': 'SubCategory2'
    },
    {
        $set: {
            'name': 'Green Moong Dal Whole',
            'code': 'MGDLW'
        }
    }, {upsert: false, multi: true}
);

db.categories.insert({
    'name': 'Green Moong Dal Split',
    'code': 'MGDLS',
    'type': 'SubCategory2',
    'categoryImageURL1': 'modules/categories/img/subcategory2/greenmoongsplit.png',
    'productCategory': true,
    'productAttributes': {
        brand: true,
        grade: {enabled: true},
        quality: {enabled: true},
        sampleNumber: true,
        fssaiLicenceNumber: true
    },
    'inventoryAttributes': {
        barcode: true,
        batchNumber: true,
        manufactureDate: true,
        packagingDate: true,
        bestBefore: true,
        unitMeasure: ['Kg', 'gms', 'Ton', 'Bag', 'Case', 'Pack']
    },
    'parent': db.categories.findOne({'name': 'Pulses', 'type': 'SubCategory1'})._id
});

db.categories.update({
    'name': 'Pulses',
    'type': 'SubCategory1'
}, {$addToSet: {'children': db.categories.findOne({'name': 'Green Moong Dal Split', 'type': 'SubCategory2'})._id}});

db.categories.insert({
    'name': 'Yellow Moong Dal Split',
    'code': 'MGDLY',
    'type': 'SubCategory2',
    'categoryImageURL1': 'modules/categories/img/subcategory2/yellowmoongdal.png',
    'productCategory': true,
    'productAttributes': {
        brand: true,
        grade: {enabled: true},
        quality: {enabled: true},
        sampleNumber: true,
        fssaiLicenceNumber: true
    },
    'inventoryAttributes': {
        barcode: true,
        batchNumber: true,
        manufactureDate: true,
        packagingDate: true,
        bestBefore: true,
        unitMeasure: ['Kg', 'gms', 'Ton', 'Bag', 'Case', 'Pack']
    },
    'parent': db.categories.findOne({'name': 'Pulses', 'type': 'SubCategory1'})._id
});

db.categories.update({
    'name': 'Pulses',
    'type': 'SubCategory1'
}, {$addToSet: {'children': db.categories.findOne({'name': 'Yellow Moong Dal Split', 'type': 'SubCategory2'})._id}});

db.categories.update(
    {
        'name': 'Channa Dal',
        'code': 'CDL',
        'type': 'SubCategory2'
    },
    {
        $set: {
            'code': 'CHDAL'
        }
    }, {upsert: false, multi: true}
);

db.categories.update(
    {
        'name': 'Kabuli Chana Dal',
        'code': 'KCD',
        'type': 'SubCategory2'
    },
    {
        $set: {
            'name': 'Kabuli Chana Dal White',
            'code': 'KWDAL'
        }
    }, {upsert: false, multi: true}
);


db.categories.insert({
    'name': 'Kabuli Chana Dal Brown',
    'code': 'KBDAL',
    'type': 'SubCategory2',
    'categoryImageURL1': 'modules/categories/img/subcategory2/kabulichanabrown.png',
    'productCategory': true,
    'productAttributes': {
        brand: true,
        grade: {enabled: true},
        quality: {enabled: true},
        sampleNumber: true,
        fssaiLicenceNumber: true
    },
    'inventoryAttributes': {
        barcode: true,
        batchNumber: true,
        manufactureDate: true,
        packagingDate: true,
        bestBefore: true,
        unitMeasure: ['Kg', 'gms', 'Ton', 'Bag', 'Case', 'Pack']
    },
    'parent': db.categories.findOne({'name': 'Pulses', 'type': 'SubCategory1'})._id
});

db.categories.update({
    'name': 'Pulses',
    'type': 'SubCategory1'
}, {$addToSet: {'children': db.categories.findOne({'name': 'Kabuli Chana Dal Brown', 'type': 'SubCategory2'})._id}});

db.categories.update(
    {
        'name': 'Horse Gram',
        'code': 'HGR',
        'type': 'SubCategory2'
    },
    {
        $set: {
            'code': 'HGDAL'
        }
    }, {upsert: false, multi: true}
);

db.categories.update(
    {
        'name': 'Soya Bean',
        'code': 'SOB',
        'type': 'SubCategory2'
    },
    {
        $set: {
            'code': 'SOYBN'
        }
    }, {upsert: false, multi: true}
);

db.categories.update(
    {
        'name': 'Cowpea',
        'code': 'CWP',
        'type': 'SubCategory2'
    },
    {
        $set: {
            'code': 'COWPE'
        }
    }, {upsert: false, multi: true}
);

db.categories.update(
    {
        'name': 'Sesame',
        'code': 'SSM',
        'type': 'SubCategory2'
    },
    {
        $set: {
            'code': 'SESAM'
        }
    }, {upsert: false, multi: true}
);

db.categories.update(
    {
        'name': 'Peas',
        'code': 'PES',
        'type': 'SubCategory2'
    },
    {
        $set: {
            'code': 'PEAS'
        }
    }, {upsert: false, multi: true}
);

db.categories.update(
    {
        'name': 'Red Gram',
        'code': 'RGM',
        'type': 'SubCategory2'
    },
    {
        $set: {
            'code': 'REDGM'
        }
    }, {upsert: false, multi: true}
);

db.categories.update(
    {
        'name': 'Groundnut',
        'code': 'GNT',
        'type': 'SubCategory2'
    },
    {
        $set: {
            'code': 'GRDNT'
        }
    }, {upsert: false, multi: true}
);

db.categories.update(
    {
        'name': 'Spices',
        'code': 'SPS',
        'type': 'SubCategory1'
    },
    {
        $set: {
            'code': 'SPICE'
        }
    }, {upsert: false, multi: true}
);

db.categories.update(
    {
        'name': 'Coriander',
        'code': 'CDR',
        'type': 'SubCategory2'
    },
    {
        $set: {
            'code': 'CRNDR'
        }
    }, {upsert: false, multi: true}
);

db.categories.update(
    {
        'name': 'Jeera',
        'code': 'JER',
        'type': 'SubCategory2'
    },
    {
        $set: {
            'code': 'JEERA'
        }
    }, {upsert: false, multi: true}
);

db.categories.update(
    {
        'name': 'Mustard',
        'code': 'MTD',
        'type': 'SubCategory2'
    },
    {
        $set: {
            'code': 'MUSTD'
        }
    }, {upsert: false, multi: true}
);

db.categories.update(
    {
        'name': 'Methi',
        'code': 'MTH',
        'type': 'SubCategory2'
    },
    {
        $set: {
            'code': 'METHI'
        }
    }, {upsert: false, multi: true}
);

db.categories.update(
    {
        'name': 'Dry Chilli',
        'code': 'DCL',
        'type': 'SubCategory2'
    },
    {
        $set: {
            'code': 'DRCHL'
        }
    }, {upsert: false, multi: true}
);

db.categories.update(
    {
        'name': 'Tamarind',
        'code': 'TMD',
        'type': 'SubCategory2'
    },
    {
        $set: {
            'code': 'TMRND'
        }
    }, {upsert: false, multi: true}
);

db.categories.update(
    {
        'name': 'Clove',
        'code': 'CLV',
        'type': 'SubCategory2'
    },
    {
        $set: {
            'code': 'CLOVE'
        }
    }, {upsert: false, multi: true}
);

db.categories.update(
    {
        'name': 'Black Pepper',
        'code': 'BPR',
        'type': 'SubCategory2'
    },
    {
        $set: {
            'code': 'BLKPR'
        }
    }, {upsert: false, multi: true}
);

db.categories.update(
    {
        'name': 'Ginger',
        'code': 'GNR',
        'type': 'SubCategory2'
    },
    {
        $set: {
            'code': 'GINGR'
        }
    }, {upsert: false, multi: true}
);

db.categories.update(
    {
        'name': 'Carom Seed',
        'code': 'CSD',
        'type': 'SubCategory2'
    },
    {
        $set: {
            'code': 'CRMSD'
        }
    }, {upsert: false, multi: true}
);

db.categories.update(
    {
        'name': 'Bay Leaf',
        'code': 'BLF',
        'type': 'SubCategory2'
    },
    {
        $set: {
            'code': 'BAYLF'
        }
    }, {upsert: false, multi: true}
);

db.categories.update(
    {
        'name': 'Green Cardamom',
        'code': 'GCM',
        'type': 'SubCategory2'
    },
    {
        $set: {
            'code': 'GRMCM'
        }
    }, {upsert: false, multi: true}
);

db.categories.update(
    {
        'name': 'Dry Coconut',
        'code': 'DCN',
        'type': 'SubCategory2'
    },
    {
        $set: {
            'code': 'DRYCN'
        }
    }, {upsert: false, multi: true}
);

db.categories.update(
    {
        'name': 'Salt',
        'code': 'SLT',
        'type': 'SubCategory2'
    },
    {
        $set: {
            'code': 'SALTS'
        }
    }, {upsert: false, multi: true}
);

db.categories.update(
    {
        'name': 'Cassia',
        'code': 'CSI',
        'type': 'SubCategory2'
    },
    {
        $set: {
            'code': 'CASSI'
        }
    }, {upsert: false, multi: true}
);

db.categories.update(
    {
        'name': 'Turmeric',
        'code': 'TMC',
        'type': 'SubCategory2'
    },
    {
        $set: {
            'code': 'TURMC'
        }
    }, {upsert: false, multi: true}
);

db.categories.update(
    {
        'name': 'Jaggery Powder',
        'code': 'JGP',
        'type': 'SubCategory2'
    },
    {
        $set: {
            'code': 'JGYPD'
        }
    }, {upsert: false, multi: true}
);

db.categories.update(
    {
        'name': 'Cotton',
        'code': 'CTN',
        'type': 'SubCategory1'
    },
    {
        $set: {
            'code': 'COTTN'
        }
    }, {upsert: false, multi: true}
);

db.categories.update(
    {
        'name': 'Cotton Bales',
        'code': 'CTB',
        'type': 'SubCategory2'
    },
    {
        $set: {
            'productAttributes': {
                brand: true,
                grade: {enabled: true},
                quality: {enabled: true},
                testCertifcate: true,
                sampleNumber: true
            },
            'inventoryAttributes': {
                barcode: true,
                batchNumber: true,
                manufactureDate: true,
                packagingDate: true,
                inventoryTestCertifcate: true,
                bestBefore: true,
                unitMeasure: ['Kg', 'Bale', 'Ton', 'Bag', 'Case', 'Pack']
            },
            'code': 'COTBL'
        }
    }, {upsert: false, multi: true}
);

db.categories.update(
    {
        'name': 'Cotton Seeds',
        'code': 'CNS',
        'type': 'SubCategory2'
    },
    {
        $set: {
            'code': 'COTSD'
        }
    }, {upsert: false, multi: true}
);

db.categories.update(
    {
        'name': 'Textiles',
        'code': 'TEX',
        'type': 'MainCategory'
    },
    {
        $set: {
            'code': 'TEXTL'
        }
    }, {upsert: false, multi: true}
);

db.categories.update(
    {
        'name': 'Yarn',
        'code': 'YRN',
        'type': 'SubCategory1'
    },
    {
        $set: {
            'code': 'YARNT'
        }
    }, {upsert: false, multi: true}
);

db.categories.update(
    {
        'name': 'Cotton Yarn',
        'code': 'CYN',
        'type': 'SubCategory2'
    },
    {
        $set: {
            'code': 'COTYN'
        }
    }, {upsert: false, multi: true}
);

db.categories.update(
    {
        'name': 'Silk Yarn',
        'code': 'SYN',
        'type': 'SubCategory2'
    },
    {
        $set: {
            'code': 'SLKYN'
        }
    }, {upsert: false, multi: true}
);

db.categories.update(
    {
        'name': 'Wool Yarn',
        'code': 'WYN',
        'type': 'SubCategory2'
    },
    {
        $set: {
            'code': 'WOLYN'
        }
    }, {upsert: false, multi: true}
);

db.companies.update(
    {},
    {
        $set: {
            'disabled': false
        }
    }, {upsert: false, multi: true}
);

db.orders.find({}).forEach(function (ordersDoc) {
    ordersDoc.products.forEach(function (product) {
        product.numberOfUnitsAvailable = NumberInt(0);
    });
    db.orders.save(ordersDoc);
});


db.products.find({}).forEach(function (productDoc) {

    var subCategory2 = db.categories.findOne({_id: productDoc.subCategory2});
    if (subCategory2) {
        var counter = db.counters.findOne({});
        counter.productSeqNumber = counter.productSeqNumber + 1;
        if (!counter.productSampleSeqNumber) {
            counter.productSampleSeqNumber = 1;
        } else {
            counter.productSampleSeqNumber = counter.productSampleSeqNumber + 1;
        }
        db.counters.save(counter);
        var productUID = subCategory2.code + '-' + (counter.productSeqNumber);
        var sampleNumber = 'SM-' + subCategory2.code + '-' + (counter.productSampleSeqNumber);
        productDoc.productUID = productUID;
        productDoc.sampleNumber = sampleNumber;
        db.products.save(productDoc);
    }
});

