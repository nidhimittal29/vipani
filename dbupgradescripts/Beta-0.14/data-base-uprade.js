// FMCG Start

db.categories.insert({
    'name': 'FMCG',
    'code': 'FMCGC',
    'type': 'MainCategory'
});

// Sub Categories of FMCG

// Start - Health Care
db.categories.insert({
    'name': 'Health Care',
    'code': 'HCARE',
    'type': 'SubCategory1',
    'categoryImageURL1': 'modules/categories/img/subcategory1/healthcare.png',
    'parent': db.categories.findOne({'name': 'FMCG', 'code': 'FMCGC', 'type': 'MainCategory'})._id
});

db.categories.insert({
    'name': 'Digestives',
    'code': 'DGSTV',
    'type': 'SubCategory2',
    'categoryImageURL1': 'modules/categories/img/subcategory2/digestive.png',
    'productCategory': true,
    'productAttributes': {
        brand: true,
        grade: {enabled: true},
        quality: {enabled: true},
        sampleNumber: true,
        fssaiLicenceNumber: true
    },
    'inventoryAttributes': {
        barcode: true,
        batchNumber: true,
        manufactureDate: true,
        packagingDate: true,
        bestBefore: true,
        unitMeasure: ['Kg', 'gram', 'litre', 'ml', 'count', 'Bag', 'Case', 'Pack']
    },
    'parent': db.categories.findOne({'name': 'Health Care', 'code': 'HCARE', 'type': 'SubCategory1'})._id
});

db.categories.update({
    'name': 'Health Care',
    'code': 'HCARE',
    'type': 'SubCategory1'
}, {
    $addToSet: {
        'children': db.categories.findOne({
            'name': 'Digestives',
            'code': 'DGSTV',
            'type': 'SubCategory2'
        })._id
    }
});

db.categories.insert({
    'name': 'Health Drinks',
    'code': 'HDRNK',
    'type': 'SubCategory2',
    'categoryImageURL1': 'modules/categories/img/subcategory2/healthdrinks.png',
    'productCategory': true,
    'productAttributes': {
        brand: true,
        grade: {enabled: true},
        quality: {enabled: true},
        sampleNumber: true,
        fssaiLicenceNumber: true
    },
    'inventoryAttributes': {
        barcode: true,
        batchNumber: true,
        manufactureDate: true,
        packagingDate: true,
        bestBefore: true,
        unitMeasure: ['litre', 'ml', 'count', 'Bag', 'Case', 'Pack']
    },
    'parent': db.categories.findOne({'name': 'Health Care', 'code': 'HCARE', 'type': 'SubCategory1'})._id
});

db.categories.update({
    'name': 'Health Care',
    'code': 'HCARE',
    'type': 'SubCategory1'
}, {
    $addToSet: {
        'children': db.categories.findOne({
            'name': 'Health Drinks',
            'code': 'HDRNK',
            'type': 'SubCategory2'
        })._id
    }
});

db.categories.insert({
    'name': 'Honey',
    'code': 'HONEY',
    'type': 'SubCategory2',
    'categoryImageURL1': 'modules/categories/img/subcategory2/honey.png',
    'productCategory': true,
    'productAttributes': {
        brand: true,
        grade: {enabled: true},
        quality: {enabled: true},
        sampleNumber: true,
        fssaiLicenceNumber: true
    },
    'inventoryAttributes': {
        barcode: true,
        batchNumber: true,
        manufactureDate: true,
        packagingDate: true,
        bestBefore: true,
        unitMeasure: ['Kg', 'gram', 'litre', 'ml', 'count', 'Bag', 'Case', 'Pack']
    },
    'parent': db.categories.findOne({'name': 'Health Care', 'code': 'HCARE', 'type': 'SubCategory1'})._id
});

db.categories.update({
    'name': 'Health Care',
    'code': 'HCARE',
    'type': 'SubCategory1'
}, {$addToSet: {'children': db.categories.findOne({'name': 'Honey', 'code': 'HONEY', 'type': 'SubCategory2'})._id}});

db.categories.insert({
    'name': 'Health and Wellness',
    'code': 'HWELL',
    'type': 'SubCategory2',
    'categoryImageURL1': 'modules/categories/img/subcategory2/healthandwellness.png',
    'productCategory': true,
    'productAttributes': {
        brand: true,
        grade: {enabled: true},
        quality: {enabled: true},
        sampleNumber: true,
        fssaiLicenceNumber: true
    },
    'inventoryAttributes': {
        barcode: true,
        batchNumber: true,
        manufactureDate: true,
        packagingDate: true,
        bestBefore: true,
        unitMeasure: ['Kg', 'gram', 'litre', 'ml', 'count', 'Bag', 'Case', 'Pack']
    },
    'parent': db.categories.findOne({'name': 'Health Care', 'code': 'HCARE', 'type': 'SubCategory1'})._id
});

db.categories.update({
    'name': 'Health Care',
    'code': 'HCARE',
    'type': 'SubCategory1'
}, {
    $addToSet: {
        'children': db.categories.findOne({
            'name': 'Health and Wellness',
            'code': 'HWELL',
            'type': 'SubCategory2'
        })._id
    }
});

db.categories.insert({
    'name': 'Immune Enhancers',
    'code': 'IMMUN',
    'type': 'SubCategory2',
    'categoryImageURL1': 'modules/categories/img/subcategory2/immuneenhancer.png',
    'productCategory': true,
    'productAttributes': {
        brand: true,
        grade: {enabled: true},
        quality: {enabled: true},
        sampleNumber: true,
        fssaiLicenceNumber: true
    },
    'inventoryAttributes': {
        barcode: true,
        batchNumber: true,
        manufactureDate: true,
        packagingDate: true,
        bestBefore: true,
        unitMeasure: ['Kg', 'gram', 'litre', 'ml', 'count', 'Bag', 'Case', 'Pack']
    },
    'parent': db.categories.findOne({'name': 'Health Care', 'code': 'HCARE', 'type': 'SubCategory1'})._id
});

db.categories.update({
    'name': 'Health Care',
    'code': 'HCARE',
    'type': 'SubCategory1'
}, {
    $addToSet: {
        'children': db.categories.findOne({
            'name': 'Immune Enhancers',
            'code': 'IMMUN',
            'type': 'SubCategory2'
        })._id
    }
});

// END - Health Care

// Start - Food Products
db.categories.insert({
    'name': 'Food Products',
    'code': 'FOODP',
    'type': 'SubCategory1',
    'categoryImageURL1': 'modules/categories/img/subcategory1/foodproducts.png',
    'parent': db.categories.findOne({'name': 'FMCG', 'code': 'FMCGC', 'type': 'MainCategory'})._id
});


db.categories.insert({
    'name': 'Biscuits And Cookies',
    'code': 'BISCK',
    'type': 'SubCategory2',
    'categoryImageURL1': 'modules/categories/img/subcategory2/biscuitsandcookies.png',
    'productCategory': true,
    'productAttributes': {
        brand: true,
        grade: {enabled: true},
        quality: {enabled: true},
        sampleNumber: true,
        fssaiLicenceNumber: true
    },
    'inventoryAttributes': {
        barcode: true,
        batchNumber: true,
        manufactureDate: true,
        packagingDate: true,
        bestBefore: true,
        unitMeasure: ['Kg', 'gram', 'count', 'Bag', 'Case', 'Pack']
    },
    'parent': db.categories.findOne({'name': 'Food Products', 'code': 'FOODP', 'type': 'SubCategory1'})._id
});

db.categories.update({
    'name': 'Food Products',
    'code': 'FOODP',
    'type': 'SubCategory1'
}, {
    $addToSet: {
        'children': db.categories.findOne({
            'name': 'Biscuits And Cookies',
            'code': 'BISCK',
            'type': 'SubCategory2'
        })._id
    }
});


db.categories.insert({
    'name': 'Dairy',
    'code': 'DAIRY',
    'type': 'SubCategory2',
    'categoryImageURL1': 'modules/categories/img/subcategory2/dairy.png',
    'productCategory': true,
    'productAttributes': {
        brand: true,
        grade: {enabled: true},
        quality: {enabled: true},
        sampleNumber: true,
        fssaiLicenceNumber: true
    },
    'inventoryAttributes': {
        barcode: true,
        batchNumber: true,
        manufactureDate: true,
        packagingDate: true,
        bestBefore: true,
        unitMeasure: ['Kg', 'gram', 'litre', 'ml', 'count', 'Bag', 'Case', 'Pack']
    },
    'parent': db.categories.findOne({'name': 'Food Products', 'code': 'FOODP', 'type': 'SubCategory1'})._id
});

db.categories.update({
    'name': 'Food Products',
    'code': 'FOODP',
    'type': 'SubCategory1'
}, {
    $addToSet: {
        'children': db.categories.findOne({
            'name': 'Dairy',
            'code': 'DAIRY',
            'type': 'SubCategory2'
        })._id
    }
});

db.categories.insert({
    'name': 'Masalas and Spices',
    'code': 'MSPIC',
    'type': 'SubCategory2',
    'categoryImageURL1': 'modules/categories/img/subcategory2/masalasandspices.png',
    'productCategory': true,
    'productAttributes': {
        brand: true,
        grade: {enabled: true},
        quality: {enabled: true},
        sampleNumber: true,
        fssaiLicenceNumber: true
    },
    'inventoryAttributes': {
        barcode: true,
        batchNumber: true,
        manufactureDate: true,
        packagingDate: true,
        bestBefore: true,
        unitMeasure: ['Kg', 'gram', 'litre', 'ml', 'count', 'Bag', 'Case', 'Pack']
    },
    'parent': db.categories.findOne({'name': 'Food Products', 'code': 'FOODP', 'type': 'SubCategory1'})._id
});

db.categories.update({
    'name': 'Food Products',
    'code': 'FOODP',
    'type': 'SubCategory1'
}, {
    $addToSet: {
        'children': db.categories.findOne({
            'name': 'Masalas and Spices',
            'code': 'MSPIC',
            'type': 'SubCategory2'
        })._id
    }
});

db.categories.insert({
    'name': 'Wheat Atta',
    'code': 'WATTA',
    'type': 'SubCategory2',
    'categoryImageURL1': 'modules/categories/img/subcategory2/wheatatta.png',
    'productCategory': true,
    'productAttributes': {
        brand: true,
        grade: {enabled: true},
        quality: {enabled: true},
        sampleNumber: true,
        fssaiLicenceNumber: true
    },
    'inventoryAttributes': {
        barcode: true,
        batchNumber: true,
        manufactureDate: true,
        packagingDate: true,
        bestBefore: true,
        unitMeasure: ['Kg', 'gram', 'count', 'Bag', 'Case', 'Pack']
    },
    'parent': db.categories.findOne({'name': 'Food Products', 'code': 'FOODP', 'type': 'SubCategory1'})._id
});

db.categories.update({
    'name': 'Food Products',
    'code': 'FOODP',
    'type': 'SubCategory1'
}, {
    $addToSet: {
        'children': db.categories.findOne({
            'name': 'Wheat Atta',
            'code': 'WATTA',
            'type': 'SubCategory2'
        })._id
    }
});

db.categories.insert({
    'name': 'Flours',
    'code': 'FLOUR',
    'type': 'SubCategory2',
    'categoryImageURL1': 'modules/categories/img/subcategory2/flours.png',
    'productCategory': true,
    'productAttributes': {
        brand: true,
        grade: {enabled: true},
        quality: {enabled: true},
        sampleNumber: true,
        fssaiLicenceNumber: true
    },
    'inventoryAttributes': {
        barcode: true,
        batchNumber: true,
        manufactureDate: true,
        packagingDate: true,
        bestBefore: true,
        unitMeasure: ['Kg', 'gram', 'count', 'Bag', 'Case', 'Pack']
    },
    'parent': db.categories.findOne({'name': 'Food Products', 'code': 'FOODP', 'type': 'SubCategory1'})._id
});

db.categories.update({
    'name': 'Food Products',
    'code': 'FOODP',
    'type': 'SubCategory1'
}, {$addToSet: {'children': db.categories.findOne({'name': 'Flours', 'code': 'FLOUR', 'type': 'SubCategory2'})._id}});


db.categories.insert({
    'name': 'Oils',
    'code': 'FOILS',
    'type': 'SubCategory2',
    'categoryImageURL1': 'modules/categories/img/subcategory2/oils.png',
    'productCategory': true,
    'productAttributes': {
        brand: true,
        grade: {enabled: true},
        quality: {enabled: true},
        sampleNumber: true,
        fssaiLicenceNumber: true
    },
    'inventoryAttributes': {
        barcode: true,
        batchNumber: true,
        manufactureDate: true,
        packagingDate: true,
        bestBefore: true,
        unitMeasure: ['Kg', 'gram', 'litre', 'ml', 'count', 'Bag', 'Case', 'Pack']
    },
    'parent': db.categories.findOne({'name': 'Food Products', 'code': 'FOODP', 'type': 'SubCategory1'})._id
});

db.categories.update({
    'name': 'Food Products',
    'code': 'FOODP',
    'type': 'SubCategory1'
}, {$addToSet: {'children': db.categories.findOne({'name': 'Oils', 'code': 'FOILS', 'type': 'SubCategory2'})._id}});

db.categories.insert({
    'name': 'Sooji and Dalia',
    'code': 'SOOJI',
    'type': 'SubCategory2',
    'categoryImageURL1': 'modules/categories/img/subcategory2/sooji.png',
    'productCategory': true,
    'productAttributes': {
        brand: true,
        grade: {enabled: true},
        quality: {enabled: true},
        sampleNumber: true,
        fssaiLicenceNumber: true
    },
    'inventoryAttributes': {
        barcode: true,
        batchNumber: true,
        manufactureDate: true,
        packagingDate: true,
        bestBefore: true,
        unitMeasure: ['Kg', 'gram', 'count', 'Bag', 'Case', 'Pack']
    },
    'parent': db.categories.findOne({'name': 'Food Products', 'code': 'FOODP', 'type': 'SubCategory1'})._id
});

db.categories.update({
    'name': 'Food Products',
    'code': 'FOODP',
    'type': 'SubCategory1'
}, {
    $addToSet: {
        'children': db.categories.findOne({
            'name': 'Sooji and Dalia',
            'code': 'SOOJI',
            'type': 'SubCategory2'
        })._id
    }
});

db.categories.insert({
    'name': 'Corn Flakes',
    'code': 'CORNF',
    'type': 'SubCategory2',
    'categoryImageURL1': 'modules/categories/img/subcategory2/cornflakes.png',
    'productCategory': true,
    'productAttributes': {
        brand: true,
        grade: {enabled: true},
        quality: {enabled: true},
        sampleNumber: true,
        fssaiLicenceNumber: true
    },
    'inventoryAttributes': {
        barcode: true,
        batchNumber: true,
        manufactureDate: true,
        packagingDate: true,
        bestBefore: true,
        unitMeasure: ['Kg', 'gram', 'count', 'Bag', 'Case', 'Pack']
    },
    'parent': db.categories.findOne({'name': 'Food Products', 'code': 'FOODP', 'type': 'SubCategory1'})._id
});

db.categories.update({
    'name': 'Food Products',
    'code': 'FOODP',
    'type': 'SubCategory1'
}, {
    $addToSet: {
        'children': db.categories.findOne({
            'name': 'Corn Flakes',
            'code': 'CORNF',
            'type': 'SubCategory2'
        })._id
    }
});

db.categories.insert({
    'name': 'Noodles',
    'code': 'NOODL',
    'type': 'SubCategory2',
    'categoryImageURL1': 'modules/categories/img/subcategory2/noodles.png',
    'productCategory': true,
    'productAttributes': {
        brand: true,
        grade: {enabled: true},
        quality: {enabled: true},
        sampleNumber: true,
        fssaiLicenceNumber: true
    },
    'inventoryAttributes': {
        barcode: true,
        batchNumber: true,
        manufactureDate: true,
        packagingDate: true,
        bestBefore: true,
        unitMeasure: ['Kg', 'gram', 'count', 'Bag', 'Case', 'Pack']
    },
    'parent': db.categories.findOne({'name': 'Food Products', 'code': 'FOODP', 'type': 'SubCategory1'})._id
});

db.categories.update({
    'name': 'Food Products',
    'code': 'FOODP',
    'type': 'SubCategory1'
}, {$addToSet: {'children': db.categories.findOne({'name': 'Noodles', 'code': 'NOODL', 'type': 'SubCategory2'})._id}});

db.categories.insert({
    'name': 'Candies and Chocolates',
    'code': 'CANCH',
    'type': 'SubCategory2',
    'categoryImageURL1': 'modules/categories/img/subcategory2/candiesandchacolates.png',
    'productCategory': true,
    'productAttributes': {
        brand: true,
        grade: {enabled: true},
        quality: {enabled: true},
        sampleNumber: true,
        fssaiLicenceNumber: true
    },
    'inventoryAttributes': {
        barcode: true,
        batchNumber: true,
        manufactureDate: true,
        packagingDate: true,
        bestBefore: true,
        unitMeasure: ['Kg', 'gram', 'count', 'Bag', 'Case', 'Pack']
    },
    'parent': db.categories.findOne({'name': 'Food Products', 'code': 'FOODP', 'type': 'SubCategory1'})._id
});

db.categories.update({
    'name': 'Food Products',
    'code': 'FOODP',
    'type': 'SubCategory1'
}, {
    $addToSet: {
        'children': db.categories.findOne({
            'name': 'Candies and Chocolates',
            'code': 'CANCH',
            'type': 'SubCategory2'
        })._id
    }
});

db.categories.insert({
    'name': 'Sugar',
    'code': 'SUGAR',
    'type': 'SubCategory2',
    'categoryImageURL1': 'modules/categories/img/subcategory2/sugar.png',
    'productCategory': true,
    'productAttributes': {
        brand: true,
        grade: {enabled: true},
        quality: {enabled: true},
        sampleNumber: true,
        fssaiLicenceNumber: true
    },
    'inventoryAttributes': {
        barcode: true,
        batchNumber: true,
        manufactureDate: true,
        packagingDate: true,
        bestBefore: true,
        unitMeasure: ['Kg', 'gram', 'count', 'Bag', 'Case', 'Pack']
    },
    'parent': db.categories.findOne({'name': 'Food Products', 'code': 'FOODP', 'type': 'SubCategory1'})._id
});

db.categories.update({
    'name': 'Food Products',
    'code': 'FOODP',
    'type': 'SubCategory1'
}, {$addToSet: {'children': db.categories.findOne({'name': 'Sugar', 'code': 'SUGAR', 'type': 'SubCategory2'})._id}});

db.categories.insert({
    'name': 'Maida',
    'code': 'MAIDA',
    'type': 'SubCategory2',
    'categoryImageURL1': 'modules/categories/img/subcategory2/maida.png',
    'productCategory': true,
    'productAttributes': {
        brand: true,
        grade: {enabled: true},
        quality: {enabled: true},
        sampleNumber: true,
        fssaiLicenceNumber: true
    },
    'inventoryAttributes': {
        barcode: true,
        batchNumber: true,
        manufactureDate: true,
        packagingDate: true,
        bestBefore: true,
        unitMeasure: ['Kg', 'gram', 'count', 'Bag', 'Case', 'Pack']
    },
    'parent': db.categories.findOne({'name': 'Food Products', 'code': 'FOODP', 'type': 'SubCategory1'})._id
});

db.categories.update({
    'name': 'Food Products',
    'code': 'FOODP',
    'type': 'SubCategory1'
}, {$addToSet: {'children': db.categories.findOne({'name': 'Maida', 'code': 'MAIDA', 'type': 'SubCategory2'})._id}});

db.categories.insert({
    'name': 'Jaggery',
    'code': 'JAGRY',
    'type': 'SubCategory2',
    'categoryImageURL1': 'modules/categories/img/subcategory2/jaggerypowder.png',
    'productCategory': true,
    'productAttributes': {
        brand: true,
        grade: {enabled: true},
        quality: {enabled: true},
        sampleNumber: true,
        fssaiLicenceNumber: true
    },
    'inventoryAttributes': {
        barcode: true,
        batchNumber: true,
        manufactureDate: true,
        packagingDate: true,
        bestBefore: true,
        unitMeasure: ['Kg', 'gram', 'count', 'Bag', 'Case', 'Pack']
    },
    'parent': db.categories.findOne({'name': 'Food Products', 'code': 'FOODP', 'type': 'SubCategory1'})._id
});

db.categories.update({
    'name': 'Food Products',
    'code': 'FOODP',
    'type': 'SubCategory1'
}, {$addToSet: {'children': db.categories.findOne({'name': 'Jaggery', 'code': 'JAGRY', 'type': 'SubCategory2'})._id}});

db.categories.insert({
    'name': 'Ice Creams',
    'code': 'ICCRM',
    'type': 'SubCategory2',
    'categoryImageURL1': 'modules/categories/img/subcategory2/icecreams.png',
    'productCategory': true,
    'productAttributes': {
        brand: true,
        grade: {enabled: true},
        quality: {enabled: true},
        sampleNumber: true,
        fssaiLicenceNumber: true
    },
    'inventoryAttributes': {
        barcode: true,
        batchNumber: true,
        manufactureDate: true,
        packagingDate: true,
        bestBefore: true,
        unitMeasure: ['Kg', 'gram', 'litre', 'ml', 'count', 'Bag', 'Case', 'Pack']
    },
    'parent': db.categories.findOne({'name': 'Food Products', 'code': 'FOODP', 'type': 'SubCategory1'})._id
});

db.categories.update({
    'name': 'Food Products',
    'code': 'FOODP',
    'type': 'SubCategory1'
}, {
    $addToSet: {
        'children': db.categories.findOne({
            'name': 'Ice Creams',
            'code': 'ICCRM',
            'type': 'SubCategory2'
        })._id
    }
});

db.categories.insert({
    'name': 'Sweets',
    'code': 'SWEET',
    'type': 'SubCategory2',
    'categoryImageURL1': 'modules/categories/img/subcategory2/sweets.png',
    'productCategory': true,
    'productAttributes': {
        brand: true,
        grade: {enabled: true},
        quality: {enabled: true},
        sampleNumber: true,
        fssaiLicenceNumber: true
    },
    'inventoryAttributes': {
        barcode: true,
        batchNumber: true,
        manufactureDate: true,
        packagingDate: true,
        bestBefore: true,
        unitMeasure: ['Kg', 'gram', 'count', 'Bag', 'Case', 'Pack']
    },
    'parent': db.categories.findOne({'name': 'Food Products', 'code': 'FOODP', 'type': 'SubCategory1'})._id
});

db.categories.update({
    'name': 'Food Products',
    'code': 'FOODP',
    'type': 'SubCategory1'
}, {$addToSet: {'children': db.categories.findOne({'name': 'Sweets', 'code': 'SWEET', 'type': 'SubCategory2'})._id}});

db.categories.insert({
    'name': 'Oats',
    'code': 'OATSS',
    'type': 'SubCategory2',
    'categoryImageURL1': 'modules/categories/img/subcategory2/oats.png',
    'productCategory': true,
    'productAttributes': {
        brand: true,
        grade: {enabled: true},
        quality: {enabled: true},
        sampleNumber: true,
        fssaiLicenceNumber: true
    },
    'inventoryAttributes': {
        barcode: true,
        batchNumber: true,
        manufactureDate: true,
        packagingDate: true,
        bestBefore: true,
        unitMeasure: ['Kg', 'gram', 'count', 'Bag', 'Case', 'Pack']
    },
    'parent': db.categories.findOne({'name': 'Food Products', 'code': 'FOODP', 'type': 'SubCategory1'})._id
});

db.categories.update({
    'name': 'Food Products',
    'code': 'FOODP',
    'type': 'SubCategory1'
}, {$addToSet: {'children': db.categories.findOne({'name': 'Oats', 'code': 'OATSS', 'type': 'SubCategory2'})._id}});

db.categories.insert({
    'name': 'Namkeen',
    'code': 'NAMKN',
    'type': 'SubCategory2',
    'categoryImageURL1': 'modules/categories/img/subcategory2/namkeen.png',
    'productCategory': true,
    'productAttributes': {
        brand: true,
        grade: {enabled: true},
        quality: {enabled: true},
        sampleNumber: true,
        fssaiLicenceNumber: true
    },
    'inventoryAttributes': {
        barcode: true,
        batchNumber: true,
        manufactureDate: true,
        packagingDate: true,
        bestBefore: true,
        unitMeasure: ['Kg', 'gram', 'count', 'Bag', 'Case', 'Pack']
    },
    'parent': db.categories.findOne({'name': 'Food Products', 'code': 'FOODP', 'type': 'SubCategory1'})._id
});

db.categories.update({
    'name': 'Food Products',
    'code': 'FOODP',
    'type': 'SubCategory1'
}, {$addToSet: {'children': db.categories.findOne({'name': 'Namkeen', 'code': 'NAMKN', 'type': 'SubCategory2'})._id}});

db.categories.insert({
    'name': 'Papads',
    'code': 'PAPAD',
    'type': 'SubCategory2',
    'categoryImageURL1': 'modules/categories/img/subcategory2/papads.png',
    'productCategory': true,
    'productAttributes': {
        brand: true,
        grade: {enabled: true},
        quality: {enabled: true},
        sampleNumber: true,
        fssaiLicenceNumber: true
    },
    'inventoryAttributes': {
        barcode: true,
        batchNumber: true,
        manufactureDate: true,
        packagingDate: true,
        bestBefore: true,
        unitMeasure: ['Kg', 'gram', 'count', 'Bag', 'Case', 'Pack']
    },
    'parent': db.categories.findOne({'name': 'Food Products', 'code': 'FOODP', 'type': 'SubCategory1'})._id
});

db.categories.update({
    'name': 'Food Products',
    'code': 'FOODP',
    'type': 'SubCategory1'
}, {$addToSet: {'children': db.categories.findOne({'name': 'Papads', 'code': 'PAPAD', 'type': 'SubCategory2'})._id}});

db.categories.insert({
    'name': 'Pickles',
    'code': 'PICKL',
    'type': 'SubCategory2',
    'categoryImageURL1': 'modules/categories/img/subcategory2/pickles.png',
    'productCategory': true,
    'productAttributes': {
        brand: true,
        grade: {enabled: true},
        quality: {enabled: true},
        sampleNumber: true,
        fssaiLicenceNumber: true
    },
    'inventoryAttributes': {
        barcode: true,
        batchNumber: true,
        manufactureDate: true,
        packagingDate: true,
        bestBefore: true,
        unitMeasure: ['Kg', 'gram', 'count', 'Bag', 'Case', 'Pack']
    },
    'parent': db.categories.findOne({'name': 'Food Products', 'code': 'FOODP', 'type': 'SubCategory1'})._id
});

db.categories.update({
    'name': 'Food Products',
    'code': 'FOODP',
    'type': 'SubCategory1'
}, {$addToSet: {'children': db.categories.findOne({'name': 'Pickles', 'code': 'PICKL', 'type': 'SubCategory2'})._id}});

db.categories.insert({
    'name': 'Jams',
    'code': 'JAMSS',
    'type': 'SubCategory2',
    'categoryImageURL1': 'modules/categories/img/subcategory2/jams.png',
    'productCategory': true,
    'productAttributes': {
        brand: true,
        grade: {enabled: true},
        quality: {enabled: true},
        sampleNumber: true,
        fssaiLicenceNumber: true
    },
    'inventoryAttributes': {
        barcode: true,
        batchNumber: true,
        manufactureDate: true,
        packagingDate: true,
        bestBefore: true,
        unitMeasure: ['Kg', 'gram', 'count', 'Bag', 'Case', 'Pack']
    },
    'parent': db.categories.findOne({'name': 'Food Products', 'code': 'FOODP', 'type': 'SubCategory1'})._id
});

db.categories.update({
    'name': 'Food Products',
    'code': 'FOODP',
    'type': 'SubCategory1'
}, {$addToSet: {'children': db.categories.findOne({'name': 'Jams', 'code': 'JAMSS', 'type': 'SubCategory2'})._id}});

db.categories.insert({
    'name': 'Sauces',
    'code': 'SAUCE',
    'type': 'SubCategory2',
    'categoryImageURL1': 'modules/categories/img/subcategory2/sauces.png',
    'productCategory': true,
    'productAttributes': {
        brand: true,
        grade: {enabled: true},
        quality: {enabled: true},
        sampleNumber: true,
        fssaiLicenceNumber: true
    },
    'inventoryAttributes': {
        barcode: true,
        batchNumber: true,
        manufactureDate: true,
        packagingDate: true,
        bestBefore: true,
        unitMeasure: ['Kg', 'gram', 'count', 'Bag', 'Case', 'Pack']
    },
    'parent': db.categories.findOne({'name': 'Food Products', 'code': 'FOODP', 'type': 'SubCategory1'})._id
});

db.categories.update({
    'name': 'Food Products',
    'code': 'FOODP',
    'type': 'SubCategory1'
}, {$addToSet: {'children': db.categories.findOne({'name': 'Sauces', 'code': 'SAUCE', 'type': 'SubCategory2'})._id}});
// END - Food Products

// Start - Beverages

db.categories.insert({
    'name': 'Beverages',
    'code': 'BVRGS',
    'type': 'SubCategory1',
    'categoryImageURL1': 'modules/categories/img/subcategory1/beverages.png',
    'parent': db.categories.findOne({'name': 'FMCG', 'code': 'FMCGC', 'type': 'MainCategory'})._id
});

db.categories.insert({
    'name': 'Tea and Coffee',
    'code': 'TEACF',
    'type': 'SubCategory2',
    'categoryImageURL1': 'modules/categories/img/subcategory2/teaandcoffee.png',
    'productCategory': true,
    'productAttributes': {
        brand: true,
        grade: {enabled: true},
        quality: {enabled: true},
        sampleNumber: true,
        fssaiLicenceNumber: true
    },
    'inventoryAttributes': {
        barcode: true,
        batchNumber: true,
        manufactureDate: true,
        packagingDate: true,
        bestBefore: true,
        unitMeasure: ['Kg', 'gram', 'count', 'Bag', 'Case', 'Pack']
    },
    'parent': db.categories.findOne({'name': 'Beverages', 'code': 'BVRGS', 'type': 'SubCategory1'})._id
});

db.categories.update({
    'name': 'Beverages',
    'code': 'BVRGS',
    'type': 'SubCategory1'
}, {
    $addToSet: {
        'children': db.categories.findOne({
            'name': 'Tea and Coffee',
            'code': 'TEACF',
            'type': 'SubCategory2'
        })._id
    }
});

db.categories.insert({
    'name': 'Juices',
    'code': 'JUICE',
    'type': 'SubCategory2',
    'categoryImageURL1': 'modules/categories/img/subcategory2/juices.png',
    'productCategory': true,
    'productAttributes': {
        brand: true,
        grade: {enabled: true},
        quality: {enabled: true},
        sampleNumber: true,
        fssaiLicenceNumber: true
    },
    'inventoryAttributes': {
        barcode: true,
        batchNumber: true,
        manufactureDate: true,
        packagingDate: true,
        bestBefore: true,
        unitMeasure: ['Kg', 'gram', 'litre', 'ml', 'count', 'Bag', 'Case', 'Pack']
    },
    'parent': db.categories.findOne({'name': 'Beverages', 'code': 'BVRGS', 'type': 'SubCategory1'})._id
});

db.categories.update({
    'name': 'Beverages',
    'code': 'BVRGS',
    'type': 'SubCategory1'
}, {$addToSet: {'children': db.categories.findOne({'name': 'Juices', 'code': 'JUICE', 'type': 'SubCategory2'})._id}});

db.categories.insert({
    'name': 'Softdrinks',
    'code': 'SOFTD',
    'type': 'SubCategory2',
    'categoryImageURL1': 'modules/categories/img/subcategory2/softdrinks.png',
    'productCategory': true,
    'productAttributes': {
        brand: true,
        grade: {enabled: true},
        quality: {enabled: true},
        sampleNumber: true,
        fssaiLicenceNumber: true
    },
    'inventoryAttributes': {
        barcode: true,
        batchNumber: true,
        manufactureDate: true,
        packagingDate: true,
        bestBefore: true,
        unitMeasure: ['litre', 'ml', 'count', 'Bag', 'Case', 'Pack']
    },
    'parent': db.categories.findOne({'name': 'Beverages', 'code': 'BVRGS', 'type': 'SubCategory1'})._id
});

db.categories.update({
    'name': 'Beverages',
    'code': 'BVRGS',
    'type': 'SubCategory1'
}, {
    $addToSet: {
        'children': db.categories.findOne({
            'name': 'Softdrinks',
            'code': 'SOFTD',
            'type': 'SubCategory2'
        })._id
    }
});

// END - Beverages
// Start - Home Care

db.categories.insert({
    'name': 'Home Care',
    'code': 'HOMEC',
    'type': 'SubCategory1',
    'categoryImageURL1': 'modules/categories/img/subcategory1/homecare.png',
    'parent': db.categories.findOne({'name': 'FMCG', 'code': 'FMCGC', 'type': 'MainCategory'})._id
});

db.categories.insert({
    'name': 'Laundry',
    'code': 'LNDRY',
    'type': 'SubCategory2',
    'categoryImageURL1': 'modules/categories/img/subcategory2/laundry.png',
    'productCategory': true,
    'productAttributes': {
        brand: true,
        grade: {enabled: true},
        quality: {enabled: true},
        sampleNumber: true,
        fssaiLicenceNumber: true
    },
    'inventoryAttributes': {
        barcode: true,
        batchNumber: true,
        manufactureDate: true,
        packagingDate: true,
        bestBefore: true,
        unitMeasure: ['Kg', 'gram', 'litre', 'ml', 'count', 'Bag', 'Case', 'Pack']
    },
    'parent': db.categories.findOne({'name': 'Home Care', 'code': 'HOMEC', 'type': 'SubCategory1'})._id
});

db.categories.update({
    'name': 'Home Care',
    'code': 'HOMEC',
    'type': 'SubCategory1'
}, {$addToSet: {'children': db.categories.findOne({'name': 'Laundry', 'code': 'LNDRY', 'type': 'SubCategory2'})._id}});


db.categories.insert({
    'name': 'Kitchen and Dining',
    'code': 'KTDNG',
    'type': 'SubCategory2',
    'categoryImageURL1': 'modules/categories/img/subcategory2/kitchenanddining.png',
    'productCategory': true,
    'productAttributes': {
        brand: true,
        grade: {enabled: true},
        quality: {enabled: true},
        sampleNumber: true,
        fssaiLicenceNumber: true
    },
    'inventoryAttributes': {
        barcode: true,
        batchNumber: true,
        manufactureDate: true,
        packagingDate: true,
        bestBefore: true,
        unitMeasure: ['Kg', 'gram', 'litre', 'ml', 'count', 'Bag', 'Case', 'Pack']
    },
    'parent': db.categories.findOne({'name': 'Home Care', 'code': 'HOMEC', 'type': 'SubCategory1'})._id
});

db.categories.update({
    'name': 'Home Care',
    'code': 'HOMEC',
    'type': 'SubCategory1'
}, {
    $addToSet: {
        'children': db.categories.findOne({
            'name': 'Kitchen and Dining',
            'code': 'KTDNG',
            'type': 'SubCategory2'
        })._id
    }
});

db.categories.insert({
    'name': 'Repellents',
    'code': 'RPLNS',
    'type': 'SubCategory2',
    'categoryImageURL1': 'modules/categories/img/subcategory2/repellents.png',
    'productCategory': true,
    'productAttributes': {
        brand: true,
        grade: {enabled: true},
        quality: {enabled: true},
        sampleNumber: true,
        fssaiLicenceNumber: true
    },
    'inventoryAttributes': {
        barcode: true,
        batchNumber: true,
        manufactureDate: true,
        packagingDate: true,
        bestBefore: true,
        unitMeasure: ['Kg', 'gram', 'litre', 'ml', 'count', 'Bag', 'Case', 'Pack']
    },
    'parent': db.categories.findOne({'name': 'Home Care', 'code': 'HOMEC', 'type': 'SubCategory1'})._id
});

db.categories.update({
    'name': 'Home Care',
    'code': 'HOMEC',
    'type': 'SubCategory1'
}, {
    $addToSet: {
        'children': db.categories.findOne({
            'name': 'Repellents',
            'code': 'RPLNS',
            'type': 'SubCategory2'
        })._id
    }
});


db.categories.insert({
    'name': 'Toilets and Surface Cleaners',
    'code': 'TOASC',
    'type': 'SubCategory2',
    'categoryImageURL1': 'modules/categories/img/subcategory2/toiletsandsurfacecleaners.png',
    'productCategory': true,
    'productAttributes': {
        brand: true,
        grade: {enabled: true},
        quality: {enabled: true},
        sampleNumber: true,
        fssaiLicenceNumber: true
    },
    'inventoryAttributes': {
        barcode: true,
        batchNumber: true,
        manufactureDate: true,
        packagingDate: true,
        bestBefore: true,
        unitMeasure: ['Kg', 'gram', 'litre', 'ml', 'count', 'Bag', 'Case', 'Pack']
    },
    'parent': db.categories.findOne({'name': 'Home Care', 'code': 'HOMEC', 'type': 'SubCategory1'})._id
});

db.categories.update({
    'name': 'Home Care',
    'code': 'HOMEC',
    'type': 'SubCategory1'
}, {
    $addToSet: {
        'children': db.categories.findOne({
            'name': 'Toilets and Surface Cleaners',
            'code': 'TOASC',
            'type': 'SubCategory2'
        })._id
    }
});


db.categories.insert({
    'name': 'Air Care and Cleaning',
    'code': 'ACACL',
    'type': 'SubCategory2',
    'categoryImageURL1': 'modules/categories/img/subcategory2/aircareandcleaning.png',
    'productCategory': true,
    'productAttributes': {
        brand: true,
        grade: {enabled: true},
        quality: {enabled: true},
        sampleNumber: true,
        fssaiLicenceNumber: true
    },
    'inventoryAttributes': {
        barcode: true,
        batchNumber: true,
        manufactureDate: true,
        packagingDate: true,
        bestBefore: true,
        unitMeasure: ['Kg', 'gram', 'litre', 'ml', 'count', 'Bag', 'Case', 'Pack']
    },
    'parent': db.categories.findOne({'name': 'Home Care', 'code': 'HOMEC', 'type': 'SubCategory1'})._id
});

db.categories.update({
    'name': 'Home Care',
    'code': 'HOMEC',
    'type': 'SubCategory1'
}, {
    $addToSet: {
        'children': db.categories.findOne({
            'name': 'Air Care and Cleaning',
            'code': 'ACACL',
            'type': 'SubCategory2'
        })._id
    }
});


db.categories.insert({
    'name': 'Detergents',
    'code': 'DTGNT',
    'type': 'SubCategory2',
    'categoryImageURL1': 'modules/categories/img/subcategory2/detergents.png',
    'productCategory': true,
    'productAttributes': {
        brand: true,
        grade: {enabled: true},
        quality: {enabled: true},
        sampleNumber: true,
        fssaiLicenceNumber: true
    },
    'inventoryAttributes': {
        barcode: true,
        batchNumber: true,
        manufactureDate: true,
        packagingDate: true,
        bestBefore: true,
        unitMeasure: ['Kg', 'gram', 'litre', 'ml', 'count', 'Bag', 'Case', 'Pack']
    },
    'parent': db.categories.findOne({'name': 'Home Care', 'code': 'HOMEC', 'type': 'SubCategory1'})._id
});

db.categories.update({
    'name': 'Home Care',
    'code': 'HOMEC',
    'type': 'SubCategory1'
}, {
    $addToSet: {
        'children': db.categories.findOne({
            'name': 'Detergents',
            'code': 'DTGNT',
            'type': 'SubCategory2'
        })._id
    }
});


db.categories.insert({
    'name': 'Pet Care',
    'code': 'PETCR',
    'type': 'SubCategory2',
    'categoryImageURL1': 'modules/categories/img/subcategory2/petcare.png',
    'productCategory': true,
    'productAttributes': {
        brand: true,
        grade: {enabled: true},
        quality: {enabled: true},
        sampleNumber: true,
        fssaiLicenceNumber: true
    },
    'inventoryAttributes': {
        barcode: true,
        batchNumber: true,
        manufactureDate: true,
        packagingDate: true,
        bestBefore: true,
        unitMeasure: ['Kg', 'gram', 'litre', 'ml', 'count', 'Bag', 'Case', 'Pack']
    },
    'parent': db.categories.findOne({'name': 'Home Care', 'code': 'HOMEC', 'type': 'SubCategory1'})._id
});

db.categories.update({
    'name': 'Home Care',
    'code': 'HOMEC',
    'type': 'SubCategory1'
}, {$addToSet: {'children': db.categories.findOne({'name': 'Pet Care', 'code': 'PETCR', 'type': 'SubCategory2'})._id}});


db.categories.insert({
    'name': 'Paper and Disposable',
    'code': 'PPRDL',
    'type': 'SubCategory2',
    'categoryImageURL1': 'modules/categories/img/subcategory2/paperanddisposable.png',
    'productCategory': true,
    'productAttributes': {
        brand: true,
        grade: {enabled: true},
        quality: {enabled: true},
        sampleNumber: true,
        fssaiLicenceNumber: true
    },
    'inventoryAttributes': {
        barcode: true,
        batchNumber: true,
        manufactureDate: true,
        packagingDate: true,
        bestBefore: true,
        unitMeasure: ['Kg', 'gram', 'count', 'Bag', 'Case', 'Pack']
    },
    'parent': db.categories.findOne({'name': 'Home Care', 'code': 'HOMEC', 'type': 'SubCategory1'})._id
});

db.categories.update({
    'name': 'Home Care',
    'code': 'HOMEC',
    'type': 'SubCategory1'
}, {
    $addToSet: {
        'children': db.categories.findOne({
            'name': 'Paper and Disposable',
            'code': 'PPRDL',
            'type': 'SubCategory2'
        })._id
    }
});


db.categories.insert({
    'name': 'Pooja Needs',
    'code': 'PJNDS',
    'type': 'SubCategory2',
    'categoryImageURL1': 'modules/categories/img/subcategory2/poojaneeds.png',
    'productCategory': true,
    'productAttributes': {
        brand: true,
        grade: {enabled: true},
        quality: {enabled: true},
        sampleNumber: true,
        fssaiLicenceNumber: true
    },
    'inventoryAttributes': {
        barcode: true,
        batchNumber: true,
        manufactureDate: true,
        packagingDate: true,
        bestBefore: true,
        unitMeasure: ['Kg', 'gram', 'litre', 'ml', 'count', 'Bag', 'Case', 'Pack']
    },
    'parent': db.categories.findOne({'name': 'Home Care', 'code': 'HOMEC', 'type': 'SubCategory1'})._id
});

db.categories.update({
    'name': 'Home Care',
    'code': 'HOMEC',
    'type': 'SubCategory1'
}, {
    $addToSet: {
        'children': db.categories.findOne({
            'name': 'Pooja Needs',
            'code': 'PJNDS',
            'type': 'SubCategory2'
        })._id
    }
});

// END - Home Care

// Start - Personal Care
db.categories.insert({
    'name': 'Personal Care',
    'code': 'PRSNL',
    'type': 'SubCategory1',
    'categoryImageURL1': 'modules/categories/img/subcategory1/personalcare.png',
    'parent': db.categories.findOne({'name': 'FMCG', 'code': 'FMCGC', 'type': 'MainCategory'})._id
});

db.categories.insert({
    'name': 'Cosmetics and Toiletries',
    'code': 'COSMT',
    'type': 'SubCategory2',
    'categoryImageURL1': 'modules/categories/img/subcategory2/cosmeticsandtoiletries.png',
    'productCategory': true,
    'productAttributes': {
        brand: true,
        grade: {enabled: true},
        quality: {enabled: true},
        sampleNumber: true,
        fssaiLicenceNumber: true
    },
    'inventoryAttributes': {
        barcode: true,
        batchNumber: true,
        manufactureDate: true,
        packagingDate: true,
        bestBefore: true,
        unitMeasure: ['Kg', 'gram', 'litre', 'ml', 'count', 'Bag', 'Case', 'Pack']
    },
    'parent': db.categories.findOne({'name': 'Personal Care', 'code': 'PRSNL', 'type': 'SubCategory1'})._id
});

db.categories.update({
    'name': 'Personal Care',
    'code': 'PRSNL',
    'type': 'SubCategory1'
}, {
    $addToSet: {
        'children': db.categories.findOne({
            'name': 'Cosmetics and Toiletries',
            'code': 'COSMT',
            'type': 'SubCategory2'
        })._id
    }
});

db.categories.insert({
    'name': 'Skin Care',
    'code': 'SKINC',
    'type': 'SubCategory2',
    'categoryImageURL1': 'modules/categories/img/subcategory2/skincare.png',
    'productCategory': true,
    'productAttributes': {
        brand: true,
        grade: {enabled: true},
        quality: {enabled: true},
        sampleNumber: true,
        fssaiLicenceNumber: true
    },
    'inventoryAttributes': {
        barcode: true,
        batchNumber: true,
        manufactureDate: true,
        packagingDate: true,
        bestBefore: true,
        unitMeasure: ['Kg', 'gram', 'litre', 'ml', 'count', 'Bag', 'Case', 'Pack']
    },
    'parent': db.categories.findOne({'name': 'Personal Care', 'code': 'PRSNL', 'type': 'SubCategory1'})._id
});

db.categories.update({
    'name': 'Personal Care',
    'code': 'PRSNL',
    'type': 'SubCategory1'
}, {
    $addToSet: {
        'children': db.categories.findOne({
            'name': 'Skin Care',
            'code': 'SKINC',
            'type': 'SubCategory2'
        })._id
    }
});

db.categories.insert({
    'name': 'Oral Care',
    'code': 'ORALC',
    'type': 'SubCategory2',
    'categoryImageURL1': 'modules/categories/img/subcategory2/oralcare.png',
    'productCategory': true,
    'productAttributes': {
        brand: true,
        grade: {enabled: true},
        quality: {enabled: true},
        sampleNumber: true,
        fssaiLicenceNumber: true
    },
    'inventoryAttributes': {
        barcode: true,
        batchNumber: true,
        manufactureDate: true,
        packagingDate: true,
        bestBefore: true,
        unitMeasure: ['Kg', 'gram', 'litre', 'ml', 'count', 'Bag', 'Case', 'Pack']
    },
    'parent': db.categories.findOne({'name': 'Personal Care', 'code': 'PRSNL', 'type': 'SubCategory1'})._id
});

db.categories.update({
    'name': 'Personal Care',
    'code': 'PRSNL',
    'type': 'SubCategory1'
}, {
    $addToSet: {
        'children': db.categories.findOne({
            'name': 'Oral Care',
            'code': 'ORALC',
            'type': 'SubCategory2'
        })._id
    }
});

db.categories.insert({
    'name': 'Hair Care',
    'code': 'HAIRC',
    'type': 'SubCategory2',
    'categoryImageURL1': 'modules/categories/img/subcategory2/haircare.png',
    'productCategory': true,
    'productAttributes': {
        brand: true,
        grade: {enabled: true},
        quality: {enabled: true},
        sampleNumber: true,
        fssaiLicenceNumber: true
    },
    'inventoryAttributes': {
        barcode: true,
        batchNumber: true,
        manufactureDate: true,
        packagingDate: true,
        bestBefore: true,
        unitMeasure: ['Kg', 'gram', 'litre', 'ml', 'count', 'Bag', 'Case', 'Pack']
    },
    'parent': db.categories.findOne({'name': 'Personal Care', 'code': 'PRSNL', 'type': 'SubCategory1'})._id
});

db.categories.update({
    'name': 'Personal Care',
    'code': 'PRSNL',
    'type': 'SubCategory1'
}, {
    $addToSet: {
        'children': db.categories.findOne({
            'name': 'Hair Care',
            'code': 'HAIRC',
            'type': 'SubCategory2'
        })._id
    }
});


db.categories.insert({
    'name': 'Perfumes and Deodrants',
    'code': 'PERDT',
    'type': 'SubCategory2',
    'categoryImageURL1': 'modules/categories/img/subcategory2/perfumesanddeodrants.png',
    'productCategory': true,
    'productAttributes': {
        brand: true,
        grade: {enabled: true},
        quality: {enabled: true},
        sampleNumber: true,
        fssaiLicenceNumber: true
    },
    'inventoryAttributes': {
        barcode: true,
        batchNumber: true,
        manufactureDate: true,
        packagingDate: true,
        bestBefore: true,
        unitMeasure: ['Kg', 'gram', 'litre', 'ml', 'count', 'Bag', 'Case', 'Pack']
    },
    'parent': db.categories.findOne({'name': 'Personal Care', 'code': 'PRSNL', 'type': 'SubCategory1'})._id
});

db.categories.update({
    'name': 'Personal Care',
    'code': 'PRSNL',
    'type': 'SubCategory1'
}, {
    $addToSet: {
        'children': db.categories.findOne({
            'name': 'Perfumes and Deodrants',
            'code': 'PERDT',
            'type': 'SubCategory2'
        })._id
    }
});
// END - Personal Care


// Adding sub categories to FMCG
db.categories.update({
    'name': 'FMCG',
    'code': 'FMCGC',
    'type': 'MainCategory'
}, {
    $addToSet: {
        'children': db.categories.findOne({
            'name': 'Health Care',
            'code': 'HCARE',
            'type': 'SubCategory1'
        })._id
    }
});

db.categories.update({
    'name': 'FMCG',
    'code': 'FMCGC',
    'type': 'MainCategory'
}, {
    $addToSet: {
        'children': db.categories.findOne({
            'name': 'Food Products',
            'code': 'FOODP',
            'type': 'SubCategory1'
        })._id
    }
});

db.categories.update({
    'name': 'FMCG',
    'code': 'FMCGC',
    'type': 'MainCategory'
}, {
    $addToSet: {
        'children': db.categories.findOne({
            'name': 'Beverages',
            'code': 'BVRGS',
            'type': 'SubCategory1'
        })._id
    }
});

db.categories.update({
    'name': 'FMCG',
    'code': 'FMCGC',
    'type': 'MainCategory'
}, {
    $addToSet: {
        'children': db.categories.findOne({
            'name': 'Home Care',
            'code': 'HOMEC',
            'type': 'SubCategory1'
        })._id
    }
});

db.categories.update({
    'name': 'FMCG',
    'code': 'FMCGC',
    'type': 'MainCategory'
}, {
    $addToSet: {
        'children': db.categories.findOne({
            'name': 'Personal Care',
            'code': 'PRSNL',
            'type': 'SubCategory1'
        })._id
    }
});

// FMCG - End
