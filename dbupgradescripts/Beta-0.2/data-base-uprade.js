db.categories.insert({
    'name': 'Cotton',
    'code': 'CTN',
    'type': 'SubCategory1',
    'categoryImageURL1': 'modules/categories/img/subcategory1/cottons.png',
    'parent': db.categories.findOne({'name': 'Agriculture', 'type': 'MainCategory'})._id
});
db.categories.update({
    'name': 'Agriculture',
    'type': 'MainCategory'
}, {$addToSet: {'children': db.categories.findOne({'name': 'Cotton', 'type': 'SubCategory1'})._id}});

db.categories.insert({
    'name': 'Cotton Bales',
    'code': 'CTB',
    'type': 'SubCategory2',
    'categoryImageURL1': 'modules/categories/img/subcategory2/cotton.png',
    'parent': db.categories.findOne({'name': 'Cotton', 'type': 'SubCategory1'})._id
});
db.categories.update({
    'name': 'Cotton',
    'type': 'SubCategory1'
}, {$addToSet: {'children': db.categories.findOne({'name': 'Cotton Bales', 'type': 'SubCategory2'})._id}});

db.categories.insert({
    'name': 'Cotton Seeds',
    'code': 'CNS',
    'type': 'SubCategory2',
    'categoryImageURL1': 'modules/categories/img/subcategory2/cottonseeds.png',
    'parent': db.categories.findOne({'name': 'Cotton', 'type': 'SubCategory1'})._id
});
db.categories.update({
    'name': 'Cotton',
    'type': 'SubCategory1'
}, {$addToSet: {'children': db.categories.findOne({'name': 'Cotton Seeds', 'type': 'SubCategory2'})._id}});


db.categories.update({
    'name': 'Groundnut',
    'type': 'SubCategory2'
}, {$set: {parent: db.categories.findOne({'name': 'Pulses', 'type': 'SubCategory1'})._id}});

db.categories.update({
    'name': 'Cereals',
    'type': 'SubCategory1'
}, {$pull: {'children': db.categories.findOne({'name': 'Groundnut', 'type': 'SubCategory2'})._id}});

db.categories.update({
    'name': 'Pulses',
    'type': 'SubCategory1'
}, {$addToSet: {'children': db.categories.findOne({'name': 'Groundnut', 'type': 'SubCategory2'})._id}});
