db.categories.insert({
    'name': 'Rajma Beans',
    'code': 'RAJBE',
    'type': 'SubCategory2',
    'categoryImageURL1': 'modules/categories/img/subcategory2/rajmabeans.png',
    'productCategory': true,
    'productAttributes': {
        brand: true,
        grade: {enabled: true},
        quality: {enabled: true},
        sampleNumber: true,
        fssaiLicenceNumber: true
    },
    'inventoryAttributes': {
        barcode: true,
        batchNumber: true,
        manufactureDate: true,
        packagingDate: true,
        bestBefore: true,
        unitMeasure: ['Kg', 'gram', 'tonne', 'Bag', 'Case', 'Pack']
    },
    'parent': db.categories.findOne({'name': 'Pulses', 'type': 'SubCategory1'})._id
});
db.categories.update({
    'name': 'Pulses',
    'type': 'SubCategory1'
}, {$addToSet: {'children': db.categories.findOne({'name': 'Rajma Beans', 'type': 'SubCategory2'})._id}});
