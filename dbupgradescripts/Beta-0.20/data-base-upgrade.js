db.inventories.find({}).forEach(function (inventoryDoc) {
    if (inventoryDoc.moqAndBuyPrice){
        for (var i = 0; i < inventoryDoc.moqAndBuyPrice.length; i++) {
            if(inventoryDoc.unitPrice && inventoryDoc.unitPrice>0)
                inventoryDoc.moqAndBuyPrice[i].margin =  Math.round((1-(inventoryDoc.moqAndBuyPrice[i].price/inventoryDoc.unitPrice))*100);
            else{
                inventoryDoc.moqAndBuyPrice[i].margin=0;
            }
        }

    }
    if (inventoryDoc.moqAndSalePrice){
        for (var i = 0; i < inventoryDoc.moqAndSalePrice.length; i++) {
            if(inventoryDoc.unitPrice && inventoryDoc.unitPrice>0)
                inventoryDoc.moqAndSalePrice[i].margin = Math.round((1-(inventoryDoc.moqAndSalePrice[i].price/inventoryDoc.unitPrice))*100);
            else{
                inventoryDoc.moqAndSalePrice[i].margin=0;
            }
        }

    }
    if (inventoryDoc.moqAndBuyPrice || inventoryDoc.moqAndBuyPrice){
        db.inventories.save(inventoryDoc);
    }
});
db.hsncodes.insert({
    'name': 'Rice',
    'hsncode': '10089010',
    'description': 'Basmati Rice - White',
    'CGST':0,
    'SGST':0,
    'IGST':0
    });
db.categories.update({
    'name': 'Rice',
    'code': 'RICEG',
    'type': 'SubCategory2'
}, {$addToSet: {'hsncodes':{ hsncode: db.hsncodes.findOne({'hsncode': '10089010'})._id}}});
db.hsncodes.insert({
    'name': 'Rice',
    'hsncode': '10061090',
    'description': 'Basmati Paddy (taraori)',
    'CGST':0,
    'SGST':0,
    'IGST':0
});
db.categories.update({
    'name': 'Rice',
    'code': 'RICEG',
    'type': 'SubCategory2'
}, {$addToSet: {'hsncodes':{ hsncode: db.hsncodes.findOne({'hsncode': '10061090'})._id}}});

db.hsncodes.insert({
    'name': 'Rice',
    'hsncode': '10063020',
    'description': 'Basmati Rice - Brown',
    'CGST':0,
    'SGST':0,
    'IGST':0
});
db.categories.update({
    'name': 'Rice',
    'code': 'RICEG',
    'type': 'SubCategory2'
}, {$addToSet: {'hsncodes':{ hsncode: db.hsncodes.findOne({'hsncode': '10063020'})._id}}});
db.hsncodes.insert({
    'name': 'Rice',
    'hsncode': '11051000',
    'description': 'Hand Pounded Rice',
    'CGST':0,
    'SGST':0,
    'IGST':0
});
db.categories.update({
    'name': 'Rice',
    'code': 'RICEG',
    'type': 'SubCategory2'
}, {$addToSet: {'hsncodes':{ hsncode: db.hsncodes.findOne({'hsncode': '11051000'})._id}}});


db.hsncodes.insert({
    'name': 'Rice',
    'hsncode': '10062000',
    'description': 'Brown Rice',
    'CGST':0,
    'SGST':0,
    'IGST':0
});
db.categories.update({
    'name': 'Rice',
    'code': 'RICEG',
    'type': 'SubCategory2'
}, {$addToSet: {'hsncodes':{ hsncode: db.hsncodes.findOne({'hsncode': '10062000'})._id}}});

db.hsncodes.insert({
    'name': 'Rice',
    'hsncode': '10063090',
    'description': 'Red Rice',
    'CGST':0,
    'SGST':0,
    'IGST':0
});
db.categories.update({
    'name': 'Rice',
    'code': 'RICEG',
    'type': 'SubCategory2'
}, {$addToSet: {'hsncodes':{ hsncode: db.hsncodes.findOne({'hsncode': '10063090'})._id}}});
db.hsncodes.insert({
    'name': 'Rice',
    'hsncode': '10063090',
    'description': 'Sonamasuri White or Red Rice or Sonamasuri Brown',
    'CGST':0,
    'SGST':0,
    'IGST':0
});
db.categories.update({
    'name': 'Rice',
    'code': 'RICEG',
    'type': 'SubCategory2'
}, {$addToSet: {'hsncodes':{ hsncode: db.hsncodes.findOne({'hsncode': '10063090'})._id}}});
db.hsncodes.insert({
    'name': 'Rice',
    'hsncode': '10063010',
    'description': 'Non-basmati Rice',
    'CGST':0,
    'SGST':0,
    'IGST':0
});
db.categories.update({
    'name': 'Rice',
    'code': 'RICEG',
    'type': 'SubCategory2'
}, {$addToSet: {'hsncodes':{ hsncode: db.hsncodes.findOne({'hsncode': '10063010'})._id}}});



