db.categories.update(
    {
        'parent': db.categories.findOne({'name': 'Vegetable', 'type': 'SubCategory1'})._id
    },
    {
        $set: {
            'inventoryAttributes': {
                barcode: true,
                batchNumber: true,
                manufactureDate: true,
                packagingDate: true,
                bestBefore: true,
                unitMeasure: ['Kg', 'gram', 'tonne', 'Bag', 'Case', 'Pack']
            }
        }
    }, {upsert: false, multi: true}
);

db.categories.update(
    {
        'parent': db.categories.findOne({'name': 'Cereals', 'type': 'SubCategory1'})._id
    },
    {
        $set: {
            'inventoryAttributes': {
                barcode: true,
                batchNumber: true,
                manufactureDate: true,
                packagingDate: true,
                bestBefore: true,
                unitMeasure: ['Kg', 'gram', 'tonne', 'Bag', 'Case', 'Pack']
            }
        }
    }, {upsert: false, multi: true}
);

db.categories.update(
    {
        'parent': db.categories.findOne({'name': 'Pulses', 'type': 'SubCategory1'})._id
    },
    {
        $set: {
            'inventoryAttributes': {
                barcode: true,
                batchNumber: true,
                manufactureDate: true,
                packagingDate: true,
                bestBefore: true,
                unitMeasure: ['Kg', 'gram', 'tonne', 'Bag', 'Case', 'Pack']
            }
        }
    }, {upsert: false, multi: true}
);

db.categories.update(
    {
        'parent': db.categories.findOne({'name': 'Spices', 'type': 'SubCategory1'})._id
    },
    {
        $set: {
            'inventoryAttributes': {
                barcode: true,
                batchNumber: true,
                manufactureDate: true,
                packagingDate: true,
                bestBefore: true,
                unitMeasure: ['Kg', 'gram', 'tonne', 'Bag', 'Case', 'Pack']
            }
        }
    }, {upsert: false, multi: true}
);

db.categories.update(
    {
        'name': 'Cotton Bales',
        'code': 'COTBL',
        'type': 'SubCategory2'
    },
    {
        $set: {
            'inventoryAttributes': {
                barcode: true,
                batchNumber: true,
                manufactureDate: true,
                packagingDate: true,
                inventoryTestCertifcate: true,
                bestBefore: true,
                unitMeasure: ['Kg', 'Bale', 'Candy', 'tonne', 'Bag', 'Case', 'Pack']
            }
        }
    }, {upsert: false, multi: true}
);

db.categories.update(
    {
        'name': 'Cotton Seeds',
        'code': 'COTSD',
        'type': 'SubCategory2'
    },
    {
        $set: {
            'inventoryAttributes': {
                barcode: true,
                batchNumber: true,
                manufactureDate: true,
                packagingDate: true,
                bestBefore: true,
                unitMeasure: ['Kg', 'gram', 'tonne', 'Bag', 'Case', 'Pack']
            }
        }
    }, {upsert: false, multi: true}
);

db.categories.update(
    {
        'parent': db.categories.findOne({'name': 'Yarn', 'type': 'SubCategory1'})._id
    },
    {
        $set: {
            'inventoryAttributes': {
                barcode: true,
                batchNumber: true,
                manufactureDate: true,
                packagingDate: true,
                inventoryTestCertifcate: true,
                bestBefore: true,
                unitMeasure: ['Kg', 'Bundle', 'tonne', 'Bag', 'Case', 'Pack']
            }
        }
    }, {upsert: false, multi: true}
);

db.categories.update(
    {
        'parent': db.categories.findOne({'name': 'Fabric', 'type': 'SubCategory1'})._id
    },
    {
        $set: {
            'inventoryAttributes': {
                barcode: true,
                batchNumber: true,
                manufactureDate: true,
                packagingDate: true,
                inventoryTestCertifcate: true,
                bestBefore: true,
                unitMeasure: ['Kg', 'Bundle', 'tonne', 'Bag', 'Case', 'Pack']
            }
        }
    }, {upsert: false, multi: true}
);

db.categories.update(
    {
        'parent': db.categories.findOne({'name': 'Apparel', 'type': 'SubCategory1'})._id
    },
    {
        $set: {
            'inventoryAttributes': {
                barcode: true,
                batchNumber: true,
                manufactureDate: true,
                packagingDate: true,
                inventoryTestCertifcate: true,
                bestBefore: true,
                unitMeasure: ['Kg', 'Bundle', 'tonne', 'Bag', 'Case', 'Pack']
            }
        }
    }, {upsert: false, multi: true}
);


