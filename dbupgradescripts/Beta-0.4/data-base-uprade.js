db.users.update(
    {},
    {
        $set: {
            'mobileVerified': false,
            'emailVerified': false
        }
    }, {upsert: false, multi: true}
);


db.users.find({}).forEach(function (userDoc) {
    if (userDoc.mobile) {
        db.contacts.update({nVipaniUser: userDoc._id, nVipaniRegContact: true}, {
            $push: {
                phones: {
                    phoneNumber: userDoc.mobile,
                    phoneType: 'Mobile',
                    primary: true
                }
            }
        })
    }
});

db.users.find({}).forEach(function (userDoc) {
    if (userDoc.mobile === userDoc.username) {
        userDoc.mobileVerified = true;
    }
    if (userDoc.email === userDoc.username) {
        userDoc.emailVerified = true;
    }
    db.users.save(userDoc);
});

db.categories.update(
    {
        'name': 'Cotton Bales',
        'code': 'CTB',
        'type': 'SubCategory2'
    },
    {
        $set: {
            'productCategory': true
        }
    }, {upsert: false, multi: true}
);

db.categories.update(
    {
        'name': 'Cotton Seeds',
        'code': 'CNS',
        'type': 'SubCategory2',
    },
    {
        $set: {
            'productCategory': true
        }
    }, {upsert: false, multi: true}
);

// Textiles Main Category

db.categories.insert({'name': 'Textiles', 'code': 'TEX', 'type': 'MainCategory'});

db.categories.insert({
    'name': 'Yarn',
    'code': 'YRN',
    'type': 'SubCategory1',
    'categoryImageURL1': 'modules/categories/img/subcategory1/yarn.png',
    'parent': db.categories.findOne({'name': 'Textiles', 'type': 'MainCategory'})._id
});

db.categories.update({
    'name': 'Textiles',
    'type': 'MainCategory'
}, {$addToSet: {'children': db.categories.findOne({'name': 'Yarn', 'type': 'SubCategory1'})._id}});

db.categories.insert({
    'name': 'Cotton Yarn',
    'code': 'CYN',
    'type': 'SubCategory2',
    'categoryImageURL1': 'modules/categories/img/subcategory2/cottonyarn.png',
    'productCategory': true,
    'parent': db.categories.findOne({'name': 'Yarn', 'type': 'SubCategory1'})._id
});

db.categories.insert({
    'name': 'Silk Yarn',
    'code': 'SYN',
    'type': 'SubCategory2',
    'categoryImageURL1': 'modules/categories/img/subcategory2/silkyarn.png',
    'productCategory': true,
    'parent': db.categories.findOne({'name': 'Yarn', 'type': 'SubCategory1'})._id
});

db.categories.insert({
    'name': 'Wool Yarn',
    'code': 'WYN',
    'type': 'SubCategory2',
    'categoryImageURL1': 'modules/categories/img/subcategory2/woolyarn.png',
    'productCategory': true,
    'parent': db.categories.findOne({'name': 'Yarn', 'type': 'SubCategory1'})._id
});

// Adding Sub Categories to Yarn category
db.categories.update({
    'name': 'Yarn',
    'type': 'SubCategory1'
}, {$addToSet: {'children': db.categories.findOne({'name': 'Cotton Yarn', 'type': 'SubCategory2'})._id}});

db.categories.update({
    'name': 'Yarn',
    'type': 'SubCategory1'
}, {$addToSet: {'children': db.categories.findOne({'name': 'Silk Yarn', 'type': 'SubCategory2'})._id}});

db.categories.update({
    'name': 'Yarn',
    'type': 'SubCategory1'
}, {$addToSet: {'children': db.categories.findOne({'name': 'Wool Yarn', 'type': 'SubCategory2'})._id}});
