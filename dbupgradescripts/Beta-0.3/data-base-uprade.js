//
db.categories.update(
    {},
    {
        $set: {
            'productAttributes.brand': false,
            'productAttributes.grade.enabled': false,
            'productAttributes.grade.definition': [],
            'productAttributes.quality.enabled': false,
            'productAttributes.quality.definition': [],
            'productAttributes.sampleNumber': false,
            'productAttributes.fssaiLicenceNumber': false,

            'inventoryAttributes.barcode': false,
            'inventoryAttributes.batchNumber': false,
            'inventoryAttributes.manufactureDate': false,
            'inventoryAttributes.packagingDate': false,
            'inventoryAttributes.bestBefore': false,
            'inventoryAttributes.unitMeasure': [],

            'tax.VAT': 0,
            'tax.ServiceTax': 0,
            'searchFilters.brand': false,
            'searchFilters.grade': false,
            'searchFilters.quality': false,
            'searchFilters.price': false,
            'searchFilters.moq': false,
            'searchFilters.quantity': false,

            'sortCriteria.brand': false,
            'sortCriteria.grade': false,
            'sortCriteria.quality': false,
            'sortCriteria.price': false,
            'sortCriteria.moq': false,
            'sortCriteria.quantity': false
        }
    }, {upsert: false, multi: true}
);

db.inventories.update(
    {},
    {
        $set: {
            'croppedInventoryImageURL1': 'modules/inventories/img/profile/default.png',
            'croppedInventoryImageURL2': 'modules/inventories/img/profile/default.png',
            'croppedInventoryImageURL3': 'modules/inventories/img/profile/default.png',
            'croppedInventoryImageURL4': 'modules/inventories/img/profile/default.png',
            'buyOfferUnits': 0,
            'buyOfferUnitsHistory': []
        }
    }, {upsert: false, multi: true}
);

db.users.update(
    {},
    {
        $set: {
            'otp': ''
        }
    }, {upsert: false, multi: true}
);

db.products.update(
    {},
    {
        $set: {
            'croppedProductImageURL1': 'modules/products/img/profile/default.png',
            'croppedProductImageURL2': 'modules/products/img/profile/default.png',
            'croppedProductImageURL3': 'modules/products/img/profile/default.png',
            'croppedProductImageURL4': 'modules/products/img/profile/default.png',
            'gradeDefinition': [],
            'qualityDefinition': []
        }
    }, {upsert: false, multi: true}
);

db.products.find({}).forEach(function (productDoc) {
    productDoc.gradeDefinition = [{attributeKey: 'grade', attributeValue: productDoc.grade}];
    productDoc.qualityDefinition = [{attributeKey: 'quality', attributeValue: productDoc.quality}];
    //delete productDoc.grade;
    //delete productDoc.quality;
    db.products.save(productDoc);
});
