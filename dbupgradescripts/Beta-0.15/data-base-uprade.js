db.categories.insert({
    'name': 'Onion',
    'code': 'ONION',
    'type': 'SubCategory2',
    'categoryImageURL1': 'modules/categories/img/subcategory2/onion.png',
    'productCategory': true,
    'productAttributes': {
        brand: true,
        grade: {enabled: true},
        quality: {enabled: true},
        sampleNumber: true
    },
    'inventoryAttributes': {
        barcode: true,
        batchNumber: true,
        manufactureDate: true,
        packagingDate: true,
        bestBefore: true,
        unitMeasure: ['Kg', 'gram', 'tonne', 'Bag', 'Case', 'Pack']
    },
    'parent': db.categories.findOne({'name': 'Vegetable', 'code': 'VGTBL', 'type': 'SubCategory1'})._id
});


db.categories.update({
    'name': 'Vegetable',
    'type': 'SubCategory1'
}, {$addToSet: {'children': db.categories.findOne({'name': 'Onion', 'code': 'ONION', 'type': 'SubCategory2'})._id}});

db.categories.insert({
    'name': 'Garlic',
    'code': 'GARLC',
    'type': 'SubCategory2',
    'categoryImageURL1': 'modules/categories/img/subcategory2/garlic.png',
    'productCategory': true,
    'productAttributes': {
        brand: true,
        grade: {enabled: true},
        quality: {enabled: true},
        sampleNumber: true
    },
    'inventoryAttributes': {
        barcode: true,
        batchNumber: true,
        manufactureDate: true,
        packagingDate: true,
        bestBefore: true,
        unitMeasure: ['Kg', 'gram', 'tonne', 'Bag', 'Case', 'Pack']
    },
    'parent': db.categories.findOne({'name': 'Vegetable', 'code': 'VGTBL', 'type': 'SubCategory1'})._id
});


db.categories.update({
    'name': 'Vegetable',
    'type': 'SubCategory1'
}, {$addToSet: {'children': db.categories.findOne({'name': 'Garlic', 'code': 'GARLC', 'type': 'SubCategory2'})._id}});

db.categories.insert({
    'name': 'Potato',
    'code': 'POTAT',
    'type': 'SubCategory2',
    'categoryImageURL1': 'modules/categories/img/subcategory2/potato.png',
    'productCategory': true,
    'productAttributes': {
        brand: true,
        grade: {enabled: true},
        quality: {enabled: true},
        sampleNumber: true
    },
    'inventoryAttributes': {
        barcode: true,
        batchNumber: true,
        manufactureDate: true,
        packagingDate: true,
        bestBefore: true,
        unitMeasure: ['Kg', 'gram', 'tonne', 'Bag', 'Case', 'Pack']
    },
    'parent': db.categories.findOne({'name': 'Vegetable', 'code': 'VGTBL', 'type': 'SubCategory1'})._id
});


db.categories.update({
    'name': 'Vegetable',
    'type': 'SubCategory1'
}, {$addToSet: {'children': db.categories.findOne({'name': 'Potato', 'code': 'POTAT', 'type': 'SubCategory2'})._id}});


db.categories.insert({
    'name': 'Cashew',
    'code': 'CASHW',
    'type': 'SubCategory2',
    'categoryImageURL1': 'modules/categories/img/subcategory2/cashew.png',
    'productCategory': true,
    'productAttributes': {
        brand: true,
        grade: {enabled: true},
        quality: {enabled: true},
        sampleNumber: true,
        fssaiLicenceNumber: true
    },
    'inventoryAttributes': {
        barcode: true,
        batchNumber: true,
        manufactureDate: true,
        packagingDate: true,
        bestBefore: true,
        unitMeasure: ['Kg', 'gram', 'count', 'Bag', 'Case', 'Pack']
    },
    'parent': db.categories.findOne({'name': 'Food Products', 'code': 'FOODP', 'type': 'SubCategory1'})._id
});

db.categories.update({
    'name': 'Food Products',
    'code': 'FOODP',
    'type': 'SubCategory1'
}, {$addToSet: {'children': db.categories.findOne({'name': 'Cashew', 'code': 'CASHW', 'type': 'SubCategory2'})._id}});

db.categories.insert({
    'name': 'Dry Grapes (Raisins)',
    'code': 'DGRPS',
    'type': 'SubCategory2',
    'categoryImageURL1': 'modules/categories/img/subcategory2/drygrapes.png',
    'productCategory': true,
    'productAttributes': {
        brand: true,
        grade: {enabled: true},
        quality: {enabled: true},
        sampleNumber: true,
        fssaiLicenceNumber: true
    },
    'inventoryAttributes': {
        barcode: true,
        batchNumber: true,
        manufactureDate: true,
        packagingDate: true,
        bestBefore: true,
        unitMeasure: ['Kg', 'gram', 'count', 'Bag', 'Case', 'Pack']
    },
    'parent': db.categories.findOne({'name': 'Food Products', 'code': 'FOODP', 'type': 'SubCategory1'})._id
});

db.categories.update({
    'name': 'Food Products',
    'code': 'FOODP',
    'type': 'SubCategory1'
}, {
    $addToSet: {
        'children': db.categories.findOne({
            'name': 'Dry Grapes (Raisins)',
            'code': 'DGRPS',
            'type': 'SubCategory2'
        })._id
    }
});

db.categories.insert({
    'name': 'Almonds (Badam)',
    'code': 'ALMND',
    'type': 'SubCategory2',
    'categoryImageURL1': 'modules/categories/img/subcategory2/almonds.png',
    'productCategory': true,
    'productAttributes': {
        brand: true,
        grade: {enabled: true},
        quality: {enabled: true},
        sampleNumber: true,
        fssaiLicenceNumber: true
    },
    'inventoryAttributes': {
        barcode: true,
        batchNumber: true,
        manufactureDate: true,
        packagingDate: true,
        bestBefore: true,
        unitMeasure: ['Kg', 'gram', 'count', 'Bag', 'Case', 'Pack']
    },
    'parent': db.categories.findOne({'name': 'Food Products', 'code': 'FOODP', 'type': 'SubCategory1'})._id
});

db.categories.update({
    'name': 'Food Products',
    'code': 'FOODP',
    'type': 'SubCategory1'
}, {
    $addToSet: {
        'children': db.categories.findOne({
            'name': 'Almonds (Badam)',
            'code': 'ALMND',
            'type': 'SubCategory2'
        })._id
    }
});

db.categories.insert({
    'name': 'Dates',
    'code': 'DATES',
    'type': 'SubCategory2',
    'categoryImageURL1': 'modules/categories/img/subcategory2/dates.png',
    'productCategory': true,
    'productAttributes': {
        brand: true,
        grade: {enabled: true},
        quality: {enabled: true},
        sampleNumber: true,
        fssaiLicenceNumber: true
    },
    'inventoryAttributes': {
        barcode: true,
        batchNumber: true,
        manufactureDate: true,
        packagingDate: true,
        bestBefore: true,
        unitMeasure: ['Kg', 'gram', 'count', 'Bag', 'Case', 'Pack']
    },
    'parent': db.categories.findOne({'name': 'Food Products', 'code': 'FOODP', 'type': 'SubCategory1'})._id
});

db.categories.update({
    'name': 'Food Products',
    'code': 'FOODP',
    'type': 'SubCategory1'
}, {$addToSet: {'children': db.categories.findOne({'name': 'Dates', 'code': 'DATES', 'type': 'SubCategory2'})._id}});

db.categories.insert({
    'name': 'Apricot',
    'code': 'APRCT',
    'type': 'SubCategory2',
    'categoryImageURL1': 'modules/categories/img/subcategory2/apricot.png',
    'productCategory': true,
    'productAttributes': {
        brand: true,
        grade: {enabled: true},
        quality: {enabled: true},
        sampleNumber: true,
        fssaiLicenceNumber: true
    },
    'inventoryAttributes': {
        barcode: true,
        batchNumber: true,
        manufactureDate: true,
        packagingDate: true,
        bestBefore: true,
        unitMeasure: ['Kg', 'gram', 'count', 'Bag', 'Case', 'Pack']
    },
    'parent': db.categories.findOne({'name': 'Food Products', 'code': 'FOODP', 'type': 'SubCategory1'})._id
});

db.categories.update({
    'name': 'Food Products',
    'code': 'FOODP',
    'type': 'SubCategory1'
}, {$addToSet: {'children': db.categories.findOne({'name': 'Apricot', 'code': 'APRCT', 'type': 'SubCategory2'})._id}});

db.categories.insert({
    'name': 'Anjeer (Fig)',
    'code': 'ANJER',
    'type': 'SubCategory2',
    'categoryImageURL1': 'modules/categories/img/subcategory2/anjeer.png',
    'productCategory': true,
    'productAttributes': {
        brand: true,
        grade: {enabled: true},
        quality: {enabled: true},
        sampleNumber: true,
        fssaiLicenceNumber: true
    },
    'inventoryAttributes': {
        barcode: true,
        batchNumber: true,
        manufactureDate: true,
        packagingDate: true,
        bestBefore: true,
        unitMeasure: ['Kg', 'gram', 'count', 'Bag', 'Case', 'Pack']
    },
    'parent': db.categories.findOne({'name': 'Food Products', 'code': 'FOODP', 'type': 'SubCategory1'})._id
});

db.categories.update({
    'name': 'Food Products',
    'code': 'FOODP',
    'type': 'SubCategory1'
}, {
    $addToSet: {
        'children': db.categories.findOne({
            'name': 'Anjeer (Fig)',
            'code': 'ANJER',
            'type': 'SubCategory2'
        })._id
    }
});

db.categories.insert({
    'name': 'Pistachios',
    'code': 'PSTCH',
    'type': 'SubCategory2',
    'categoryImageURL1': 'modules/categories/img/subcategory2/pistachios.png',
    'productCategory': true,
    'productAttributes': {
        brand: true,
        grade: {enabled: true},
        quality: {enabled: true},
        sampleNumber: true,
        fssaiLicenceNumber: true
    },
    'inventoryAttributes': {
        barcode: true,
        batchNumber: true,
        manufactureDate: true,
        packagingDate: true,
        bestBefore: true,
        unitMeasure: ['Kg', 'gram', 'count', 'Bag', 'Case', 'Pack']
    },
    'parent': db.categories.findOne({'name': 'Food Products', 'code': 'FOODP', 'type': 'SubCategory1'})._id
});

db.categories.update({
    'name': 'Food Products',
    'code': 'FOODP',
    'type': 'SubCategory1'
}, {
    $addToSet: {
        'children': db.categories.findOne({
            'name': 'Pistachios',
            'code': 'PSTCH',
            'type': 'SubCategory2'
        })._id
    }
});


db.categories.insert({
    'name': 'Walnuts',
    'code': 'WLNTS',
    'type': 'SubCategory2',
    'categoryImageURL1': 'modules/categories/img/subcategory2/walnuts.png',
    'productCategory': true,
    'productAttributes': {
        brand: true,
        grade: {enabled: true},
        quality: {enabled: true},
        sampleNumber: true,
        fssaiLicenceNumber: true
    },
    'inventoryAttributes': {
        barcode: true,
        batchNumber: true,
        manufactureDate: true,
        packagingDate: true,
        bestBefore: true,
        unitMeasure: ['Kg', 'gram', 'count', 'Bag', 'Case', 'Pack']
    },
    'parent': db.categories.findOne({'name': 'Food Products', 'code': 'FOODP', 'type': 'SubCategory1'})._id
});

db.categories.update({
    'name': 'Food Products',
    'code': 'FOODP',
    'type': 'SubCategory1'
}, {$addToSet: {'children': db.categories.findOne({'name': 'Walnuts', 'code': 'WLNTS', 'type': 'SubCategory2'})._id}});

db.categories.insert({
    'name': 'Prunes',
    'code': 'PRUNS',
    'type': 'SubCategory2',
    'categoryImageURL1': 'modules/categories/img/subcategory2/prunes.png',
    'productCategory': true,
    'productAttributes': {
        brand: true,
        grade: {enabled: true},
        quality: {enabled: true},
        sampleNumber: true,
        fssaiLicenceNumber: true
    },
    'inventoryAttributes': {
        barcode: true,
        batchNumber: true,
        manufactureDate: true,
        packagingDate: true,
        bestBefore: true,
        unitMeasure: ['Kg', 'gram', 'count', 'Bag', 'Case', 'Pack']
    },
    'parent': db.categories.findOne({'name': 'Food Products', 'code': 'FOODP', 'type': 'SubCategory1'})._id
});

db.categories.update({
    'name': 'Food Products',
    'code': 'FOODP',
    'type': 'SubCategory1'
}, {$addToSet: {'children': db.categories.findOne({'name': 'Prunes', 'code': 'PRUNS', 'type': 'SubCategory2'})._id}});

db.categories.insert({
    'name': 'Other Dry Fruits',
    'code': 'ODFRT',
    'type': 'SubCategory2',
    'categoryImageURL1': 'modules/categories/img/subcategory2/otherdryfruits.png',
    'productCategory': true,
    'productAttributes': {
        brand: true,
        grade: {enabled: true},
        quality: {enabled: true},
        sampleNumber: true,
        fssaiLicenceNumber: true
    },
    'inventoryAttributes': {
        barcode: true,
        batchNumber: true,
        manufactureDate: true,
        packagingDate: true,
        bestBefore: true,
        unitMeasure: ['Kg', 'gram', 'count', 'Bag', 'Case', 'Pack']
    },
    'parent': db.categories.findOne({'name': 'Food Products', 'code': 'FOODP', 'type': 'SubCategory1'})._id
});

db.categories.update({
    'name': 'Food Products',
    'code': 'FOODP',
    'type': 'SubCategory1'
}, {
    $addToSet: {
        'children': db.categories.findOne({
            'name': 'Other Dry Fruits',
            'code': 'ODFRT',
            'type': 'SubCategory2'
        })._id
    }
});
