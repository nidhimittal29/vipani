// Sub Categories of Spices
db.categories.insert({
    'name': 'Sunflower Seeds',
    'code': 'SUNSD',
    'type': 'SubCategory2',
    'categoryImageURL1': 'modules/categories/img/subcategory2/sunflowerseeds.png',
    'productCategory': true,
    'productAttributes': {
        brand: true,
        grade: {enabled: true},
        quality: {enabled: true},
        sampleNumber: true
    },
    'inventoryAttributes': {
        barcode: true,
        batchNumber: true,
        manufactureDate: true,
        packagingDate: true,
        bestBefore: true,
        unitMeasure:['Kg', 'gram', 'tonne', 'Bag', 'Pack']
    },
    'parent': db.categories.findOne({'name': 'Spices', 'type': 'SubCategory1'})._id
});


db.categories.update({
    'name': 'Spices',
    'type': 'SubCategory1'
}, {$addToSet: {'children': db.categories.findOne({'name': 'Sunflower Seeds', 'type': 'SubCategory2'})._id}});


// Sub Categories of Spices
db.categories.insert({
    'name': 'Flax Seeds',
    'code': 'FLXSD',
    'type': 'SubCategory2',
    'categoryImageURL1': 'modules/categories/img/subcategory2/flaxseeds.png',
    'productCategory': true,
    'productAttributes': {
        brand: true,
        grade: {enabled: true},
        quality: {enabled: true},
        sampleNumber: true
    },
    'inventoryAttributes': {
        barcode: true,
        batchNumber: true,
        manufactureDate: true,
        packagingDate: true,
        bestBefore: true,
        unitMeasure:['Kg', 'gram', 'tonne', 'Bag', 'Pack']
    },
    'parent': db.categories.findOne({'name': 'Spices', 'type': 'SubCategory1'})._id
});


db.categories.update({
    'name': 'Spices',
    'type': 'SubCategory1'
}, {$addToSet: {'children': db.categories.findOne({'name': 'Flax Seeds', 'type': 'SubCategory2'})._id}});
db.categories.insert({
    'name': 'Husk',
    'code': 'HUSK',
    'type': 'SubCategory2',
    'categoryImageURL1': 'modules/categories/img/subcategory2/husk.png',
    'productCategory': true,
    'productAttributes': {
        brand: true,
        grade: {enabled: true},
        quality: {enabled: true},
        sampleNumber: true
    },
    'inventoryAttributes': {
        barcode: true,
        batchNumber: true,
        manufactureDate: true,
        packagingDate: true,
        bestBefore: true,
        unitMeasure:['Kg', 'gram', 'tonne', 'Bag', 'Pack']
    },
    'parent': db.categories.findOne({'name': 'Cereals', 'type': 'SubCategory1'})._id
});


db.categories.update({
    'name': 'Cereals',
    'type': 'SubCategory1'
}, {$addToSet: {'children': db.categories.findOne({'name': 'Husk', 'type': 'SubCategory2'})._id}});



/// Update croppedCategoryImageURL1 in categories.

db.categories.update(
    {"name" : "Agriculture",
        "type" : "MainCategory"},
    {
        $set: {
            'croppedCategoryImageURL1': 'modules/categories/img/maincategory/mobileagriculture.png'
        }
    }, {upsert: false, multi: false}
);


db.categories.update(
    {"name" : "Cereals",
        "type" : "SubCategory1"},
    {
        $set: {
            'croppedCategoryImageURL1': 'modules/categories/img/subcategory1/mobilecereals.png'
        }
    }, {upsert: false, multi: false}
);



db.categories.update(
    {"name" : "Vegetable",
        "type" : "SubCategory1"},
    {
        $set: {
            'croppedCategoryImageURL1': 'modules/categories/img/subcategory1/mobilevegetables.png'
        }
    }, {upsert: false, multi: false}
);


db.categories.update(
    {"name" : "Raw Corn",
    "type" : "SubCategory2"},
    {
        $set: {
            'croppedCategoryImageURL1': 'modules/categories/img/subcategory2/mobilerawcorn.png'
        }
    }, {upsert: false, multi: false}
);


db.categories.update(
    {"name" : "Lemon",
    "type" : "SubCategory2"},
    {
        $set: {
            'croppedCategoryImageURL1': 'modules/categories/img/subcategory2/mobilelemon.png'
        }
    }, {upsert: false, multi: false}
);

db.categories.update(
    {"name" : "Onion",
    "type" : "SubCategory2"},
    {
        $set: {
            'croppedCategoryImageURL1': 'modules/categories/img/subcategory2/mobileonion.png'
        }
    }, {upsert: false, multi: false}
);

db.categories.update(
    {"name" : "Garlic",
    "type" : "SubCategory2"},
    {
        $set: {
            'croppedCategoryImageURL1': 'modules/categories/img/subcategory2/mobilegarlic.png'
        }
    }, {upsert: false, multi: false}
);

db.categories.update(
    {"name" : "Potato",
    "type" : "SubCategory2"},
    {
        $set: {
            'croppedCategoryImageURL1': 'modules/categories/img/subcategory2/mobilepotato.png'
        }
    }, {upsert: false, multi: false}
);

db.categories.update(
    {"name" : "Rice",
    "type" : "SubCategory2"},
    {
        $set: {
            'croppedCategoryImageURL1': 'modules/categories/img/subcategory2/mobilerice.png'
        }
    }, {upsert: false, multi: false}
);

db.categories.update(
    {
    "name" : "Wheat",
    "type" : "SubCategory2"},
    {
        $set: {
            'croppedCategoryImageURL1': 'modules/categories/img/subcategory2/mobilewheat.png'
        }
    }, {upsert: false, multi: false}
);

db.categories.update(
    {"name" : "Pearl Millet",
    "type" : "SubCategory2"},
    {
        $set: {
            'croppedCategoryImageURL1': 'modules/categories/img/subcategory2/mobilepearlmillet.png'
        }
    }, {upsert: false, multi: false}
);

db.categories.update(
    {"name" : "Sorghum Bicolor",
    "type" : "SubCategory2"},
    {
        $set: {
            'croppedCategoryImageURL1': 'modules/categories/img/subcategory2/mobilesorghumbicolor.png'
        }
    }, {upsert: false, multi: false}
);

db.categories.update(
    {"name" : "Corn",
    "type" : "SubCategory2"},
    {
        $set: {
            'croppedCategoryImageURL1': 'modules/categories/img/subcategory2/mobilecorn.png'
        }
    }, {upsert: false, multi: false}
);

db.categories.update(
    {"name" : "Finger Millet",
    "type" : "SubCategory2"},
    {
        $set: {
            'croppedCategoryImageURL1': 'modules/categories/img/subcategory2/mobilefingermillet.png'
        }
    }, {upsert: false, multi: false}
);

db.categories.update(
    {"name" : "Pulses",
    "type" : "SubCategory1"},
    {
        $set: {
            'croppedCategoryImageURL1': 'modules/categories/img/subcategory2/mobilepulses.png'
        }
    }, {upsert: false, multi: false}
);

db.categories.update(
    {"name" : "Toor Dal",
    "type" : "SubCategory2"},
    {
        $set: {
            'croppedCategoryImageURL1': 'modules/categories/img/subcategory2/mobiletoordal.png'
        }
    }, {upsert: false, multi: false}
);

db.categories.update(
    {"name" : "Rajma Beans",
    "type" : "SubCategory2"},
    {
        $set: {
            'croppedCategoryImageURL1': 'modules/categories/img/subcategory2/mobilerajmabeans.png'
        }
    }, {upsert: false, multi: false}
);

db.categories.update(
    {"name" : "Black Urad Dal Whole",
    "type" : "SubCategory2"},
    {
        $set: {
            'croppedCategoryImageURL1': 'modules/categories/img/subcategory2/mobileuraddal.png'
        }
    }, {upsert: false, multi: false}
);

db.categories.update(
    {"name" : "Black Urad Dal Split",
    "type" : "SubCategory2"},
    {
        $set: {
            'croppedCategoryImageURL1': 'modules/categories/img/subcategory2/mobileblackuraddalsplit.png'
        }
    }, {upsert: false, multi: false}
);

db.categories.update(
    {"name" : "White Urad Dal Whole",
    "type" : "SubCategory2"},
    {
        $set: {
            'croppedCategoryImageURL1': 'modules/categories/img/subcategory2/mobileuraddalwhole.png'
        }
    }, {upsert: false, multi: false}
);

db.categories.update(
    {"name" : "White Urad Dal Split",
    "type" : "SubCategory2"},
    {
        $set: {
            'croppedCategoryImageURL1': 'modules/categories/img/subcategory2/mobileuraddalsplit.png'
        }
    }, {upsert: false, multi: false}
);

db.categories.update(
    {"name" : "Masoor Dal",
    "type" : "SubCategory2"},
    {
        $set: {
            'croppedCategoryImageURL1': 'modules/categories/img/subcategory2/mobilemasoordal.png'
        }
    }, {upsert: false, multi: false}
);

db.categories.update(
    {"name" : "Green Moong Dal Whole",
    "type" : "SubCategory2"},
    {
        $set: {
            'croppedCategoryImageURL1': 'modules/categories/img/subcategory2/mobilemoongdal.png'
        }
    }, {upsert: false, multi: false}
);

db.categories.update(
    {"name" : "Green Moong Dal Split",
    "type" : "SubCategory2"},
    {
        $set: {
            'croppedCategoryImageURL1': 'modules/categories/img/subcategory2/mobilegreenmoongsplit.png'
        }
    }, {upsert: false, multi: false}
);

db.categories.update(
    {"name" : "Yellow Moong Dal Split",
    "type" : "SubCategory2"},
    {
        $set: {
            'croppedCategoryImageURL1': 'modules/categories/img/subcategory2/mobileyellowmoongdal.png'
        }
    }, {upsert: false, multi: false}
);

db.categories.update(
    {"name" : "Channa Dal",
    "type" : "SubCategory2"},
    {
        $set: {
            'croppedCategoryImageURL1': 'modules/categories/img/subcategory2/mobilechanna.png'
        }
    }, {upsert: false, multi: false}
);

/* 27 */
db.categories.update(
    {"name" : "Kabuli Chana Dal White",
    "type" : "SubCategory2"},
    {
        $set: {
            'croppedCategoryImageURL1': 'modules/categories/img/subcategory2/mobilekabulichana.png'
        }
    }, {upsert: false, multi: false}
);


db.categories.update(
    {"name" : "Horse Gram",
    "type" : "SubCategory2"},
    {
        $set: {
            'croppedCategoryImageURL1': 'modules/categories/img/subcategory2/mobilekabulichanabrown.png'
        }
    }, {upsert: false, multi: false}
);

db.categories.update(
    {"name" : "Horse Gram",
    "type" : "SubCategory2"},
    {
        $set: {
            'croppedCategoryImageURL1': 'modules/categories/img/subcategory2/mobilehorsegram.png'
        }
    }, {upsert: false, multi: false}
);

db.categories.update(
    {"name" : "Soya Bean",
    "type" : "SubCategory2"},
    {
        $set: {
            'croppedCategoryImageURL1': 'modules/categories/img/subcategory2/mobilersoyabean.png'
        }
    }, {upsert: false, multi: false}
);

db.categories.update(
    {"name" : "Cowpea",
    "type" : "SubCategory2"},
    {
        $set: {
            'croppedCategoryImageURL1': 'modules/categories/img/subcategory2/mobilecowpea.png'
        }
    }, {upsert: false, multi: false}
);

db.categories.update(
    {"name" : "Sesame",
    "type" : "SubCategory2"},
    {
        $set: {
            'croppedCategoryImageURL1': 'modules/categories/img/subcategory2/mobilesesame.png'
        }
    }, {upsert: false, multi: false}
);

db.categories.update(
    {"name" : "Peas",
    "type" : "SubCategory2"},
    {
        $set: {
            'croppedCategoryImageURL1': 'modules/categories/img/subcategory2/mobilepeas.png'
        }
    }, {upsert: false, multi: false}
);

db.categories.update(
    {"name" : "Red Gram",
    "type" : "SubCategory2"},
    {
        $set: {
            'croppedCategoryImageURL1': 'modules/categories/img/subcategory2/mobileredgram.png'

        }
    }, {upsert: false, multi: false}
);

db.categories.update(
    { "name" : "Groundnut",
    "type" : "SubCategory2"},
    {
        $set: {
            'croppedCategoryImageURL1': 'modules/categories/img/subcategory2/mobilegroundnut.png'
        }
    }, {upsert: false, multi: false}
);

db.categories.update(
    {"name" : "Spices",
    "type" : "SubCategory1"},
    {
        $set: {
            'croppedCategoryImageURL1': 'modules/categories/img/subcategory1/mobilespices.png'
        }
    }, {upsert: false, multi: false}
);

db.categories.update(
    {"name" : "Coriander",
    "type" : "SubCategory2"},
    {
        $set: {
            'croppedCategoryImageURL1': 'modules/categories/img/subcategory2/mobilecoriander.png'
        }
    }, {upsert: false, multi: false}
);

db.categories.update(
    {"name" : "Jeera",
    "type" : "SubCategory2"},
    {
        $set: {
            'croppedCategoryImageURL1': 'modules/categories/img/subcategory2/mobilejeera.png'
        }
    }, {upsert: false, multi: false}
);

db.categories.update(
    {"name" : "Mustard",
    "type" : "SubCategory2"},
    {
        $set: {
            'croppedCategoryImageURL1': 'modules/categories/img/subcategory2/mobilemustard.png'
        }
    }, {upsert: false, multi: false}
);

db.categories.update(
    {"name" : "Methi",
    "type" : "SubCategory2"},
    {
        $set: {
            'croppedCategoryImageURL1': 'modules/categories/img/subcategory2/mobilemethi.png'
        }
    }, {upsert: false, multi: false}
);

db.categories.update(
    {"name" : "Dry Chilli",
    "type" : "SubCategory2"},
    {
        $set: {
            'croppedCategoryImageURL1': 'modules/categories/img/subcategory2/mobiledrychilli.png'
        }
    }, {upsert: false, multi: false}
);

db.categories.update(
    {"name" : "Tamarind",
    "type" : "SubCategory2"},
    {
        $set: {
            'croppedCategoryImageURL1': 'modules/categories/img/subcategory2/mobiletamarind.png'
        }
    }, {upsert: false, multi: false}
);

db.categories.update(
    {"name" : "Clove",
    "type" : "SubCategory2"},
    {
        $set: {
            'croppedCategoryImageURL1': 'modules/categories/img/subcategory2/mobileclove.png'
        }
    }, {upsert: false, multi: false}
);

db.categories.update(
    {"name" : "Black Pepper",
    "type" : "SubCategory2"},
    {
        $set: {
            'croppedCategoryImageURL1': 'modules/categories/img/subcategory2/mobileblackpepper.png'
        }
    }, {upsert: false, multi: false}
);

db.categories.update(
    {"name" : "Ginger",
    "type" : "SubCategory2"},
    {
        $set: {
            'croppedCategoryImageURL1': 'modules/categories/img/subcategory2/mobileginger.png'
        }
    }, {upsert: false, multi: false}
);

db.categories.update(
    {"name" : "Carom Seed",
    "type" : "SubCategory2"},
    {
        $set: {
            'croppedCategoryImageURL1': 'modules/categories/img/subcategory2/mobilecaromseed.png'
        }
    }, {upsert: false, multi: false}
);

db.categories.update(
    {"name" : "Bay Leaf",
    "type" : "SubCategory2"},
    {
        $set: {
            'croppedCategoryImageURL1': 'modules/categories/img/subcategory2/mobilebayleaf.png'
        }
    }, {upsert: false, multi: false}
);
db.categories.update(
    {"name" : "Green Cardamom",
    "type" : "SubCategory2"},
    {
        $set: {
            'croppedCategoryImageURL1': 'modules/categories/img/subcategory2/mobilegreencardamom.png'
        }
    }, {upsert: false, multi: false}
);
db.categories.update(
    {"name" : "Dry Coconut",
    "type" : "SubCategory2"},
    {
        $set: {
            'croppedCategoryImageURL1': 'modules/categories/img/subcategory2/mobiledrycoconut.png'
        }
    }, {upsert: false, multi: false}
);
db.categories.update(
    {"name" : "Salt",
    "type" : "SubCategory2"},
    {
        $set: {
            'croppedCategoryImageURL1': 'modules/categories/img/subcategory2/mobilesalt.png'
        }
    }, {upsert: false, multi: false}
);
db.categories.update(
    {"name" : "Cassia",
    "type" : "SubCategory2"},
    {
        $set: {
            'croppedCategoryImageURL1': 'modules/categories/img/subcategory2/mobilecassia.png'
        }
    }, {upsert: false, multi: false}
);
db.categories.update(
    {"name" : "Turmeric",
    "type" : "SubCategory2"},
    {
        $set: {
            'croppedCategoryImageURL1': 'modules/categories/img/subcategory2/mobileturmeric.png'
        }
    }, {upsert: false, multi: false}
);
db.categories.update(
    {"name" : "Jaggery Powder",
    "type" : "SubCategory2"},
    {
        $set: {
            'croppedCategoryImageURL1': 'modules/categories/img/subcategory2/mobilejaggerypowder.png'
        }
    }, {upsert: false, multi: false}
);
db.categories.update(
    {"name" : "Black Till",
    "type" : "SubCategory2"},
    {
        $set: {
            'croppedCategoryImageURL1': 'modules/categories/img/subcategory2/mobileblacktill.png'
        }
    }, {upsert: false, multi: false}
);
db.categories.update(
    {"name" : "White Till",
    "type" : "SubCategory2"},
    {
        $set: {
            'croppedCategoryImageURL1': 'modules/categories/img/subcategory2/mobilewhitetill.png'
        }
    }, {upsert: false, multi: false}
);
db.categories.update(
    {"name" : "Poppy Seeds",
    "type" : "SubCategory2"},
    {
        $set: {
            'croppedCategoryImageURL1': 'modules/categories/img/subcategory2/mobilepoppyseeds.png'
        }
    }, {upsert: false, multi: false}
);
db.categories.update(
    {"name" : "Javitri",
    "type" : "SubCategory2"},
    {
        $set: {
            'croppedCategoryImageURL1': 'modules/categories/img/subcategory2/mobilejavitri.png'
        }
    }, {upsert: false, multi: false}
);
db.categories.update(
    {"name" : "Sabja",
    "type" : "SubCategory2"},
    {
        $set: {
            'croppedCategoryImageURL1': 'modules/categories/img/subcategory2/mobilesabja.png'
        }
    }, {upsert: false, multi: false}
);
db.categories.update(
    {"name" : "Sara Pappu",
    "type" : "SubCategory2"},
    {
        $set: {
            'croppedCategoryImageURL1': 'modules/categories/img/subcategory2/mobilesarapappu.png'
        }
    }, {upsert: false, multi: false}
);
db.categories.update(
    {"name" : "Melon Seeds",
    "type" : "SubCategory2"},
    {
        $set: {
            'croppedCategoryImageURL1': 'modules/categories/img/subcategory2/mobilemelonseeds.png'
        }
    }, {upsert: false, multi: false}
);
db.categories.update(
    {"name" : "Jakayi",
    "type" : "SubCategory2"},
    {
        $set: {
            'croppedCategoryImageURL1': 'modules/categories/img/subcategory2/mobilejakayi.png'
        }
    }, {upsert: false, multi: false}
);
db.categories.update(
    {"name" : "Cotton",
    "type" : "SubCategory1"},
    {
        $set: {
            'croppedCategoryImageURL1': 'modules/categories/img/subcategory1/mobilecottons.png'
        }
    }, {upsert: false, multi: false}
);
db.categories.update(
    {"name" : "Cotton Bales",
    "type" : "SubCategory2"},
    {
        $set: {
            'croppedCategoryImageURL1': 'modules/categories/img/subcategory2/mobilecotton.png'
        }
    }, {upsert: false, multi: false}
);
db.categories.update(
    {"name" : "Cotton Seeds",
    "type" : "SubCategory2"},
    {
        $set: {
            'croppedCategoryImageURL1': 'modules/categories/img/subcategory2/mobilecottonseeds.png'
        }
    }, {upsert: false, multi: false}
);
db.categories.update(
    {"name" : "TEXTILES",
        "type" : "MainCategory"},
    {
        $set: {
            'croppedCategoryImageURL1': 'modules/categories/img/maincategory/mobiletextiles.png',
            'categoryImageURL1': 'modules/categories/img/maincategory/textiles.png'
        }
    }, {upsert: false, multi: false}
);
db.categories.update(
    {"name" : "Yarn",
    "type" : "SubCategory1"},
    {
        $set: {
            'croppedCategoryImageURL1': 'modules/categories/img/subcategory1/mobileyarn.png'
        }
    }, {upsert: false, multi: false}
);
db.categories.update(
    {"name" : "Cotton Yarn",
    "type" : "SubCategory2"},
    {
        $set: {
            'croppedCategoryImageURL1': 'modules/categories/img/subcategory2/mobilecottonyarn.png'
        }
    }, {upsert: false, multi: false}
);
db.categories.update(
    {"name" : "Silk Yarn",
    "type" : "SubCategory2"},
    {
        $set: {
            'croppedCategoryImageURL1': 'modules/categories/img/subcategory2/mobilesilkyarn.png'
        }
    }, {upsert: false, multi: false}
);
db.categories.update(
    {"name" : "Wool Yarn",
    "type" : "SubCategory2"},
    {
        $set: {
            'croppedCategoryImageURL1': 'modules/categories/img/subcategory2/mobilewoolyarn.png'
        }
    }, {upsert: false, multi: false}
);
db.categories.update(
    {"name" : "Fabric",
    "type" : "SubCategory1"},
    {
        $set: {
            'croppedCategoryImageURL1': 'modules/categories/img/subcategory1/mobilefabric.png'
        }
    }, {upsert: false, multi: false}
);
db.categories.update(
    {"name" : "Cotton Fabric",
    "type" : "SubCategory2"},
    {
        $set: {
            'croppedCategoryImageURL1': 'modules/categories/img/subcategory2/mobilecottonfabric.png'
        }
    }, {upsert: false, multi: false}
);
db.categories.update(
    {"name" : "Silk Fabric",
    "type" : "SubCategory2"},
    {
        $set: {
            'croppedCategoryImageURL1': 'modules/categories/img/subcategory2/mobilesilkfabric.png'
        }
    }, {upsert: false, multi: false}
);
db.categories.update(
    {"name" : "Wool Fabric",
    "type" : "SubCategory2"},
    {
        $set: {
            'croppedCategoryImageURL1': 'modules/categories/img/subcategory2/mobilewoolfabric.png'
        }
    }, {upsert: false, multi: false}
);
db.categories.update(
    {"name" : "Apparel",
    "type" : "SubCategory1"},
    {
        $set: {
            'croppedCategoryImageURL1': 'modules/categories/img/subcategory1/mobileapparel.png'
        }
    }, {upsert: false, multi: false}
);
db.categories.update(
    {"name" : "Cotton Apparel",
    "type" : "SubCategory2"},
    {
        $set: {
            'croppedCategoryImageURL1': 'modules/categories/img/subcategory2/mobilecottonapparel.png'
        }
    }, {upsert: false, multi: false}
);
db.categories.update(
    {"name" : "Silk Apparel",
    "type" : "SubCategory2"},
    {
        $set: {
            'croppedCategoryImageURL1': 'modules/categories/img/subcategory2/mobilesilkapparel.png'
        }
    }, {upsert: false, multi: false}
);
db.categories.update(
    {"name" : "Wool Apparel",
    "type" : "SubCategory2"},
    {
        $set: {
            'croppedCategoryImageURL1': 'modules/categories/img/subcategory2/mobilewoolapparel.png'
        }
    }, {upsert: false, multi: false}
);

db.categories.update(
    {"name" : "Barnyard Millet",
    "type" : "SubCategory2"},
    {
        $set: {
            'croppedCategoryImageURL1': 'modules/categories/img/subcategory2/mobilebarnyardmillet.png'
        }
    }, {upsert: false, multi: false}
);               
db.categories.update(
    {"name" : "Foxtail Millet",
    "type" : "SubCategory2"},
    {
        $set: {
            'croppedCategoryImageURL1': 'modules/categories/img/subcategory2/mobilefoxtailmillet.png'
        }
    }, {upsert: false, multi: false}
);
db.categories.update(
    {"name" : "Kodo Millet",
    "type" : "SubCategory2"},
    {
        $set: {
            'croppedCategoryImageURL1': 'modules/categories/img/subcategory2/mobilekodomillet.png'
        }
    }, {upsert: false, multi: false}
);
db.categories.update(
    {"name" : "Little Millet",
    "type" : "SubCategory2"},
    {
        $set: {
            'croppedCategoryImageURL1': 'modules/categories/img/subcategory2/mobilelittlemillet.png'
        }
    }, {upsert: false, multi: false}
);

db.categories.update(
    {"name" : "Proso Millet",
    "type" : "SubCategory2"},
    {
        $set: {
            'croppedCategoryImageURL1': 'modules/categories/img/subcategory2/mobileprosomillet.png'
        }
    }, {upsert: false, multi: false}
);

db.categories.update(
    {"name" : "FMCG",
        "type" : "MainCategory"},
    {
        $set: {
            'croppedCategoryImageURL1': 'modules/categories/img/maincategory/mobiletextiles.png',
            'categoryImageURL1': 'modules/categories/img/maincategory/fmcg.png'
        }
    }, {upsert: false, multi: false}
);

db.categories.update(
    {"name" : "Health Care",
    "type" : "SubCategory1"},
    {
        $set: {
            'croppedCategoryImageURL1': 'modules/categories/img/subcategory1/mobilehealthcare.png'
        }
    }, {upsert: false, multi: false}
);

db.categories.update(
    {"name" : "Digestives",
    "type" : "SubCategory2"},
    {
        $set: {
            'croppedCategoryImageURL1': 'modules/categories/img/subcategory2/mobiledigestive.png'
        }
    }, {upsert: false, multi: false}
);

db.categories.update(
    {"name" : "Health Drinks",
    "type" : "SubCategory2"},
    {
        $set: {
            'croppedCategoryImageURL1': 'modules/categories/img/subcategory2/mobilehealthdrinks.png'
        }
    }, {upsert: false, multi: false}
);

db.categories.update(
    {"name" : "Honey",
    "type" : "SubCategory2"},
    {
        $set: {
            'croppedCategoryImageURL1': 'modules/categories/img/subcategory2/mobilehoney.png'
        }
    }, {upsert: false, multi: false}
);

db.categories.update(
    {"name" : "Health and Wellness",
    "type" : "SubCategory2"},
    {
        $set: {
            'croppedCategoryImageURL1': 'modules/categories/img/subcategory2/mobilehealthandwellness.png'
        }
    }, {upsert: false, multi: false}
);

db.categories.update(
    {"name" : "Immune Enhancers",
    "type" : "SubCategory2"},
    {
        $set: {
            'croppedCategoryImageURL1': 'modules/categories/img/subcategory2/mobileimmuneenhancer.png'
        }
    }, {upsert: false, multi: false}
);

db.categories.update(
    {"name" : "Food Products",
    "type" : "SubCategory1"},
    {
        $set: {
            'croppedCategoryImageURL1': 'modules/categories/img/subcategory1/mobilefoofproducts.png'
        }
    }, {upsert: false, multi: false}
);

db.categories.update(
    {"name" : "Biscuits And Cookies",
    "type" : "SubCategory2"},
    {
        $set: {
            'croppedCategoryImageURL1': 'modules/categories/img/subcategory2/mobilebiscuitsandcookies.png'
        }
    }, {upsert: false, multi: false}
);

db.categories.update(
    {"name" : "Dairy",
    "type" : "SubCategory2"},
    {
        $set: {
            'croppedCategoryImageURL1': 'modules/categories/img/subcategory2/mobiledairy.png'
        }
    }, {upsert: false, multi: false}
);

db.categories.update(
    {"name" : "Masalas and Spices",
    "type" : "SubCategory2"},
    {
        $set: {
            'croppedCategoryImageURL1': 'modules/categories/img/subcategory2/mobilemasalasandspices.png'
        }
    }, {upsert: false, multi: false}
);

db.categories.update(
    {"name" : "Wheat Atta",
    "type" : "SubCategory2"},
    {
        $set: {
            'croppedCategoryImageURL1': 'modules/categories/img/subcategory2/mobilewheatatta.png'
        }
    }, {upsert: false, multi: false}
);

db.categories.update(
    {"name" : "Flours",
    "type" : "SubCategory2"},
    {
        $set: {
            'croppedCategoryImageURL1': 'modules/categories/img/subcategory2/mobileflours.png'
        }
    }, {upsert: false, multi: false}
);

db.categories.update(
    {"name" : "Oils",
    "type" : "SubCategory2"},
    {
        $set: {
            'croppedCategoryImageURL1': 'modules/categories/img/subcategory2/mobileoils.png'
        }
    }, {upsert: false, multi: false}
);

db.categories.update(
    {"name" : "Sooji and Dalia",
    "type" : "SubCategory2"},
    {
        $set: {
            'croppedCategoryImageURL1': 'modules/categories/img/subcategory2/mobilesooji.png'
        }
    }, {upsert: false, multi: false}
);

db.categories.update(
    {"name" : "Corn Flakes",
    "type" : "SubCategory2"},
    {
        $set: {
            'croppedCategoryImageURL1': 'modules/categories/img/subcategory2/mobilecornflakes.png'
        }
    }, {upsert: false, multi: false}
);

db.categories.update(
    {"name" : "Noodles",
    "type" : "SubCategory2"},
    {
        $set: {
            'croppedCategoryImageURL1': 'modules/categories/img/subcategory2/mobilenoodles.png'
        }
    }, {upsert: false, multi: false}
);

db.categories.update(
    {"name" : "Candies and Chocolates",
    "type" : "SubCategory2"},
    {
        $set: {
            'croppedCategoryImageURL1': 'modules/categories/img/subcategory2/mobilecandiesandchacolates.png'
        }
    }, {upsert: false, multi: false}
);

db.categories.update(
    {"name" : "Sugar",
    "type" : "SubCategory2"},
    {
        $set: {
            'croppedCategoryImageURL1': 'modules/categories/img/subcategory2/mobiesugar.png'
        }
    }, {upsert: false, multi: false}
);

db.categories.update(
    {"name" : "Maida",
    "type" : "SubCategory2"},
    {
        $set: {
            'croppedCategoryImageURL1': 'modules/categories/img/subcategory2/mobilemaida.png'
        }
    }, {upsert: false, multi: false}
);

db.categories.update(
    {"name" : "Jaggery",
    "type" : "SubCategory2"},
    {
        $set: {
            'croppedCategoryImageURL1': 'modules/categories/img/subcategory2/mobilejaggerypowder.png'
        }
    }, {upsert: false, multi: false}
);

db.categories.update(
    {"name" : "Ice Creams",
    "type" : "SubCategory2"},
    {
        $set: {
            'croppedCategoryImageURL1': 'modules/categories/img/subcategory2/mobileicecreamss.png'
        }
    }, {upsert: false, multi: false}
);

db.categories.update(
    {"name" : "Sweets",
    "type" : "SubCategory2"},
    {
        $set: {
            'croppedCategoryImageURL1': 'modules/categories/img/subcategory2/mobilesweets.png'
        }
    }, {upsert: false, multi: false}
);

db.categories.update(
    {"name" : "Oats",
    "type" : "SubCategory2"},
    {
        $set: {
            'croppedCategoryImageURL1': 'modules/categories/img/subcategory2/mobileoats.png'
        }
    }, {upsert: false, multi: false}
);

db.categories.update(
    {"name" : "Namkeen",
    "type" : "SubCategory2"},
    {
        $set: {
            'croppedCategoryImageURL1': 'modules/categories/img/subcategory2/mobilenamkeen.png'
        }
    }, {upsert: false, multi: false}
);

db.categories.update(
    {"name" : "Papads",
    "type" : "SubCategory2"},
    {
        $set: {
            'croppedCategoryImageURL1': 'modules/categories/img/subcategory2/mobilepapads.png'
        }
    }, {upsert: false, multi: false}
);

db.categories.update(
    {"name" : "Pickles",
    "type" : "SubCategory2"},
    {
        $set: {
            'croppedCategoryImageURL1': 'modules/categories/img/subcategory2/mobilepickles.png'
        }
    }, {upsert: false, multi: false}
);

db.categories.update(
    {"name" : "Jams",
    "type" : "SubCategory2"},
    {
        $set: {
            'croppedCategoryImageURL1': 'modules/categories/img/subcategory2/mobilejams.png'
        }
    }, {upsert: false, multi: false}
);

db.categories.update(
    {"name" : "Sauces",
    "type" : "SubCategory2"},
    {
        $set: {
            'croppedCategoryImageURL1': 'modules/categories/img/subcategory2/mobilesauces.png'
        }
    }, {upsert: false, multi: false}
);

db.categories.update(
    {"name" : "Cashew",
    "type" : "SubCategory2"},
    {
        $set: {
            'croppedCategoryImageURL1': 'modules/categories/img/subcategory2/mobilecashew.png'
        }
    }, {upsert: false, multi: false}
);

db.categories.update(
    {"name" : "Dry Grapes (Raisins)",
    "type" : "SubCategory2"},
    {
        $set: {
            'croppedCategoryImageURL1': 'modules/categories/img/subcategory2/mobiledrygrapes.png'
        }
    }, {upsert: false, multi: false}
);

db.categories.update(
    {"name" : "Almonds (Badam)",
    "type" : "SubCategory2"},
    {
        $set: {
            'croppedCategoryImageURL1': 'modules/categories/img/subcategory2/mobilealmonds.png'
        }
    }, {upsert: false, multi: false}
);

db.categories.update(
    {"name" : "Dates",
    "type" : "SubCategory2"},
    {
        $set: {
            'croppedCategoryImageURL1': 'modules/categories/img/subcategory2/mobiledates.png'
        }
    }, {upsert: false, multi: false}
);

db.categories.update(
    {"name" : "Apricot",
    "type" : "SubCategory2"},
    {
        $set: {
            'croppedCategoryImageURL1': 'modules/categories/img/subcategory2/mobileapricot.png'
        }
    }, {upsert: false, multi: false}
);

db.categories.update(
    {"name" : "Anjeer (Fig)",
    "type" : "SubCategory2"},
    {
        $set: {
            'croppedCategoryImageURL1': 'modules/categories/img/subcategory2/mobileanjeer.png'
        }
    }, {upsert: false, multi: false}
);

db.categories.update(
    {"name" : "Pistachios",
    "type" : "SubCategory2"},
    {
        $set: {
            'croppedCategoryImageURL1': 'modules/categories/img/subcategory2/mobilepistachios.png'
        }
    }, {upsert: false, multi: false}
);

db.categories.update(
    {"name" : "Walnuts",
    "type" : "SubCategory2"},
    {
        $set: {
            'croppedCategoryImageURL1': 'modules/categories/img/subcategory2/mobilewalnuts.png'
        }
    }, {upsert: false, multi: false}
);

db.categories.update(
    {"name" : "Prunes",
    "type" : "SubCategory2"},
    {
        $set: {
            'croppedCategoryImageURL1': 'modules/categories/img/subcategory2/mobileprunes.png'
        }
    }, {upsert: false, multi: false}
);

db.categories.update(
    {"name" : "Other Dry Fruits",
    "type" : "SubCategory2"},
    {
        $set: {
            'croppedCategoryImageURL1': 'modules/categories/img/subcategory2/mobileotherdryfruits.png'
        }
   }, {upsert: false, multi: false}
);

db.categories.update(
    {"name" : "Beverages",
    "type" : "SubCategory1"},
    {
        $set: {
            'croppedCategoryImageURL1': 'modules/categories/img/subcategory1/mobilebeverages.png'
        }
    }, {upsert: false, multi: false}
);

db.categories.update(
    {"name" : "Tea",
    "type" : "SubCategory2"},
    {
        $set: {
            'croppedCategoryImageURL1': 'modules/categories/img/subcategory2/mobileteaandcoffee.png'
        }
    }, {upsert: false, multi: false}
);

db.categories.update(
    {"name" : "Juices",
    "type" : "SubCategory2"},
    {
        $set: {
            'croppedCategoryImageURL1': 'modules/categories/img/subcategory2/mobilejuices.png'
        }
    }, {upsert: false, multi: false}
);

db.categories.update(
    {"name" : "Softdrinks",
    "type" : "SubCategory2"},
    {
        $set: {
            'croppedCategoryImageURL1': 'modules/categories/img/subcategory2/mobilesoftdrinks.png'
        }
    }, {upsert: false, multi: false}
);

db.categories.update(
    {"name" : "Home Care",
    "type" : "SubCategory1"},
    {
        $set: {
            'croppedCategoryImageURL1': 'modules/categories/img/subcategory1/mobilehomecare.png'
        }
    }, {upsert: false, multi: false}
);

db.categories.update(
    {"name" : "Laundry",
    "type" : "SubCategory2"},
    {
        $set: {
            'croppedCategoryImageURL1': 'modules/categories/img/subcategory2/mobilelaundry.png'
        }
    }, {upsert: false, multi: false}
);

db.categories.update(
    {"name" : "Kitchen and Dining",
    "type" : "SubCategory2"},
    {
        $set: {
            'croppedCategoryImageURL1': 'modules/categories/img/subcategory2/mobilekitchenanddining.png'
        }
    }, {upsert: false, multi: false}
);

db.categories.update(
    {"name" : "Repellents",
    "type" : "SubCategory2"},
    {
        $set: {
            'croppedCategoryImageURL1': 'modules/categories/img/subcategory2/mobilerepellents.png'
        }
    }, {upsert: false, multi: false}
);

db.categories.update(
    {"name" : "Toilets and Surface Cleaners",
    "type" : "SubCategory2"},
    {
        $set: {
            'croppedCategoryImageURL1': 'modules/categories/img/subcategory2/mobiletoiletsandsurfacecleaners.png'
        }
    }, {upsert: false, multi: false}
);

db.categories.update(
    {"name" : "Air Care and Cleaning",
    "type" : "SubCategory2"},
    {
        $set: {
            'croppedCategoryImageURL1': 'modules/categories/img/subcategory2/mobileaircareandcleaning.png'
        }
    }, {upsert: false, multi: false}
);

db.categories.update(
    {"name" : "Detergents",
    "type" : "SubCategory2"},
    {
        $set: {
            'croppedCategoryImageURL1': 'modules/categories/img/subcategory2/mobiledetergents.png'
        }
    }, {upsert: false, multi: false}
);

db.categories.update(
    {"name" : "Pet Care",
    "type" : "SubCategory2"},
    {
        $set: {
            'croppedCategoryImageURL1': 'modules/categories/img/subcategory2/mobilepetcare.png'
        }
    }, {upsert: false, multi: false}
);

db.categories.update(
    {"name" : "Paper and Disposable",
    "type" : "SubCategory2"},
    {
        $set: {
            'croppedCategoryImageURL1': 'modules/categories/img/subcategory2/mobilepaperanddisposable.png'
        }
    }, {upsert: false, multi: false}
);

db.categories.update(
    {"name" : "Pooja Needs",
    "type" : "SubCategory2"},
    {
        $set: {
            'croppedCategoryImageURL1': 'modules/categories/img/subcategory2/mobilepoojaneeds.png'
        }
    }, {upsert: false, multi: false}
);

db.categories.update(
    {"name" : "Personal Care",
    "type" : "SubCategory1"},
    {
        $set: {
            'croppedCategoryImageURL1': 'modules/categories/img/subcategory1/mobilepersonalcare.png'
        }
    }, {upsert: false, multi: false}
);

db.categories.update(
    {"name" : "Cosmetics and Toiletries",
    "type" : "SubCategory2"},
    {
        $set: {
            'croppedCategoryImageURL1': 'modules/categories/img/subcategory2/mobilecosmeticsandtoiletries.png'
        }
    }, {upsert: false, multi: false}
);

db.categories.update(
    {"name" : "Skin Care",
    "type" : "SubCategory2"},
    {
        $set: {
            'croppedCategoryImageURL1': 'modules/categories/img/subcategory2/mobileskincare.png'
        }
    }, {upsert: false, multi: false}
);

db.categories.update(
    {"name" : "Oral Care",
    "type" : "SubCategory2"},
    {
        $set: {
            'croppedCategoryImageURL1': 'modules/categories/img/subcategory2/mobileoralcare.png'
        }
    }, {upsert: false, multi: false}
);

db.categories.update(
    {"name" : "Hair Care",
    "type" : "SubCategory2"},
    {
        $set: {
            'croppedCategoryImageURL1': 'modules/categories/img/subcategory2/mobilehaircare.png'
        }
    }, {upsert: false, multi: false}
);

db.categories.update(
    {"name" : "Perfumes and Deodrants",
    "type" : "SubCategory2"},
    {
        $set: {
            'croppedCategoryImageURL1': 'modules/categories/img/subcategory2/mobileperfumesanddeodrants.png'
        }
    }, {upsert: false, multi: false}
);

db.categories.update(
    {"name" : "Coffee",
    "type" : "SubCategory1"},
    {
        $set: {
            'croppedCategoryImageURL1': 'modules/categories/img/subcategory1/mobilecoffee.png'
        }
    }, {upsert: false, multi: false}
);

db.categories.update(
    {"name" : "Coffee Berries",
    "type" : "SubCategory2"},
    {
        $set: {
            'croppedCategoryImageURL1': 'modules/categories/img/subcategory2/mobilecoffeeberries.png'
        }
    }, {upsert: false, multi: false}
);

db.categories.update(
    {"name" : "Green Coffee Beans",
    "type" : "SubCategory2"},
    {
        $set: {
            'croppedCategoryImageURL1': 'modules/categories/img/subcategory2/mobilegreencoffeebeans.png'
        }
    }, {upsert: false, multi: false}
);


db.categories.update(
    {"name" : "Roasted Coffee Beans",
    "type" : "SubCategory2"},
    {
        $set: {
            'croppedCategoryImageURL1': 'modules/categories/img/subcategory2/mobileroastedcoffeebeans.png'
        }
    }, {upsert: false, multi: false}
);


db.categories.update(
    {"name" : "Coffee Beans",
    "type" : "SubCategory2"},
    {
        $set: {
            'croppedCategoryImageURL1': 'modules/categories/img/subcategory2/mobilecoffeebeans.png'
        }
    }, {upsert: false, multi: false}
);


db.categories.update(
    {"name" : "Coffee Powder",
    "type" : "SubCategory2"},
    {
        $set: {
            'croppedCategoryImageURL1': 'modules/categories/img/subcategory2/mobilecoffeepowder.png'
        }
    }, {upsert: false, multi: false}
);


db.categories.update(
    {"name" : "Coffee",
    "type" : "SubCategory1"},
    {
        $set: {
            'croppedCategoryImageURL1': 'modules/categories/img/subcategory1/mobilecoffee.png'
        }
    }, {upsert: false, multi: false}
);


db.categories.update(
    {"name" : "Coffee Berries",
    "type" : "SubCategory2"},
    {
        $set: {
            'croppedCategoryImageURL1': 'modules/categories/img/subcategory2/mobilecoffeeberries.png'
        }
    }, {upsert: false, multi: false}
);


db.categories.update(
    {"name" : "Green Coffee Beans",
    "type" : "SubCategory2"},
    {
        $set: {
            'croppedCategoryImageURL1': 'modules/categories/img/subcategory2/mobilegreencoffeebeans.png'
        }
    }, {upsert: false, multi: false}
);


db.categories.update(
    {"name" : "Roasted Coffee Beans",
    "type" : "SubCategory2"},
    {
        $set: {
            'croppedCategoryImageURL1': 'modules/categories/img/subcategory2/mobileroastedcoffeebeans.png'
        }
    }, {upsert: false, multi: false}
);


db.categories.update(
    {"name" : "Sunflower Seeds",
    "type" : "SubCategory2"},
    {
        $set: {
            'croppedCategoryImageURL1': 'modules/categories/img/subcategory2/mobilesunflowerseeds.png'
        }
    }, {upsert: false, multi: false}
);


db.categories.update(
    {"name" : "Flax Seeds",
    "type" : "SubCategory2"},
    {
        $set: {
            'croppedCategoryImageURL1': 'modules/categories/img/subcategory2/mobileflaxseeds.png'
        }
    }, {upsert: false, multi: false}
);
