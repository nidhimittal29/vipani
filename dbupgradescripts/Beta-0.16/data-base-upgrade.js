/*db.companies.find({}).forEach(function (companyDoc) {

    var payOnDeliveryEnabled = companyDoc.settings.paymentOptions.payOnDelivery;

    delete companyDoc.settings.paymentOptions.payOnDelivery;

    companyDoc.settings.paymentOptions.payOnDelivery = {'enabled': payOnDeliveryEnabled, 'discount': 0};

    var payNowEnabled = companyDoc.settings.paymentOptions.payNow;

    delete companyDoc.settings.paymentOptions.payNow;

    companyDoc.settings.paymentOptions.payNow = {'enabled': payNowEnabled, 'discount': 0};

    companyDoc.settings.paymentOptions.lineOfCredit.discount = 0;

    companyDoc.settings.paymentOptions.installments.discount = 0;

    db.companies.save(companyDoc);

    db.offers.find({user: companyDoc.user}).forEach(function (offerDoc) {
        offerDoc.paymentOptions = companyDoc.settings.paymentOptions;
        db.offers.save(offerDoc);
    });
});*/

/*
 db.companies.find({}).forEach(function (companyDoc) {

 if(!companyDoc.settings.paymentOptions) {
 companyDoc.settings.paymentOptions = {};
 companyDoc.settings.paymentOptions.payOnDelivery = {'enabled': true, 'discount': 0};
 companyDoc.settings.paymentOptions.payNow = {'enabled': true, 'discount': 0};
 companyDoc.settings.paymentOptions.lineOfCredit = { "enabled" : true, "days":30,  "discount" : 0};
 companyDoc.settings.paymentOptions.installments = { "options" : [], "enabled" : true, "discount" : 0 };
 companyDoc.settings.paymentOptions.installments.options.push({ "percentage" : 40, 	"days" : 0, 	"description" : "Advance Payment" });
 companyDoc.settings.paymentOptions.installments.options.push({ "percentage" : 40, 	"days" : 30, 	"description" : "First Payment" });
 companyDoc.settings.paymentOptions.installments.options.push({ "percentage" : 20, 	"days" : 45, 	"description" : "Final Payment" });
 db.companies.save(companyDoc);
 }
 });

 db.companies.find({}).forEach(function (companyDoc) {

 if(companyDoc.settings.paymentOptions && !companyDoc.settings.paymentOptions.lineOfCredit.discount) {
 companyDoc.settings.paymentOptions.lineOfCredit.discount = 0;
 }
 if(companyDoc.settings.paymentOptions && !companyDoc.settings.paymentOptions.installments.discount) {
 companyDoc.settings.paymentOptions.installments.discount = 0;
 }

 if(companyDoc.settings.paymentOptions && !companyDoc.settings.paymentOptions.payNow.enabled) {
 var payNowEnabled = companyDoc.settings.paymentOptions.payNow;
 delete companyDoc.settings.paymentOptions.payNow;
 companyDoc.settings.paymentOptions.payNow = {'enabled': payNowEnabled, 'discount': 0};
 }
 if(companyDoc.settings.paymentOptions && !companyDoc.settings.paymentOptions.payOnDelivery.enabled) {
 var payOnDeliveryEnabled = companyDoc.settings.paymentOptions.payOnDelivery;
 delete companyDoc.settings.paymentOptions.payOnDelivery;
 companyDoc.settings.paymentOptions.payOnDelivery = {'enabled': payOnDeliveryEnabled, 'discount': 0};
 }

 db.companies.save(companyDoc);
 });
 */


db.orders.find({}).forEach(function (orderDoc) {
    orderDoc.paymentOptionType.payOnDelivery.discount = 0;
    orderDoc.paymentOptionType.payNow.discount = 0;
    orderDoc.paymentOptionType.lineOfCredit.discount = 0;
    orderDoc.paymentOptionType.installments.discount = 0;
    db.orders.save(orderDoc);
});

db.inventories.find({}).forEach(function (inventoryDoc) {
    inventoryDoc.maxUnits = inventoryDoc.numberOfUnits;
    db.inventories.save(inventoryDoc);
});

db.categories.insert({
    'name': 'Black Till',
    'code': 'BAKTL',
    'type': 'SubCategory2',
    'categoryImageURL1': 'modules/categories/img/subcategory2/blacktill.png',
    'productCategory': true,
    'productAttributes': {
        brand: true,
        grade: {enabled: true},
        quality: {enabled: true},
        sampleNumber: true,
        fssaiLicenceNumber: true
    },
    'inventoryAttributes': {
        barcode: true,
        batchNumber: true,
        manufactureDate: true,
        packagingDate: true,
        bestBefore: true,
        unitMeasure: ['Kg', 'gram', 'tonne', 'Bag', 'Case', 'Pack']
    },
    'parent': db.categories.findOne({'name': 'Spices', 'type': 'SubCategory1'})._id
});
db.categories.insert({
    'name': 'White Till',
    'code': 'WIETL',
    'type': 'SubCategory2',
    'categoryImageURL1': 'modules/categories/img/subcategory2/whitetill.png',
    'productCategory': true,
    'productAttributes': {
        brand: true,
        grade: {enabled: true},
        quality: {enabled: true},
        sampleNumber: true,
        fssaiLicenceNumber: true
    },
    'inventoryAttributes': {
        barcode: true,
        batchNumber: true,
        manufactureDate: true,
        packagingDate: true,
        bestBefore: true,
        unitMeasure: ['Kg', 'gram', 'tonne', 'Bag', 'Case', 'Pack']
    },
    'parent': db.categories.findOne({'name': 'Spices', 'type': 'SubCategory1'})._id
});
db.categories.insert({
    'name': 'Poppy Seeds',
    'code': 'POYSD',
    'type': 'SubCategory2',
    'categoryImageURL1': 'modules/categories/img/subcategory2/poppyseeds.png',
    'productCategory': true,
    'productAttributes': {
        brand: true,
        grade: {enabled: true},
        quality: {enabled: true},
        sampleNumber: true,
        fssaiLicenceNumber: true
    },
    'inventoryAttributes': {
        barcode: true,
        batchNumber: true,
        manufactureDate: true,
        packagingDate: true,
        bestBefore: true,
        unitMeasure: ['Kg', 'gram', 'tonne', 'Bag', 'Case', 'Pack']
    },
    'parent': db.categories.findOne({'name': 'Spices', 'type': 'SubCategory1'})._id
});
db.categories.insert({
    'name': 'Javitri',
    'code': 'JAVTI',
    'type': 'SubCategory2',
    'categoryImageURL1': 'modules/categories/img/subcategory2/javitri.png',
    'productCategory': true,
    'productAttributes': {
        brand: true,
        grade: {enabled: true},
        quality: {enabled: true},
        sampleNumber: true,
        fssaiLicenceNumber: true
    },
    'inventoryAttributes': {
        barcode: true,
        batchNumber: true,
        manufactureDate: true,
        packagingDate: true,
        bestBefore: true,
        unitMeasure: ['Kg', 'gram', 'tonne', 'Bag', 'Case', 'Pack']
    },
    'parent': db.categories.findOne({'name': 'Spices', 'type': 'SubCategory1'})._id
});
db.categories.insert({
    'name': 'Sabja',
    'code': 'SABJA',
    'type': 'SubCategory2',
    'categoryImageURL1': 'modules/categories/img/subcategory2/sabja.png',
    'productCategory': true,
    'productAttributes': {
        brand: true,
        grade: {enabled: true},
        quality: {enabled: true},
        sampleNumber: true,
        fssaiLicenceNumber: true
    },
    'inventoryAttributes': {
        barcode: true,
        batchNumber: true,
        manufactureDate: true,
        packagingDate: true,
        bestBefore: true,
        unitMeasure: ['Kg', 'gram', 'tonne', 'Bag', 'Case', 'Pack']
    },
    'parent': db.categories.findOne({'name': 'Spices', 'type': 'SubCategory1'})._id
});
db.categories.insert({
    'name': 'Sara Pappu',
    'code': 'SARPU',
    'type': 'SubCategory2',
    'categoryImageURL1': 'modules/categories/img/subcategory2/sarapappu.png',
    'productCategory': true,
    'productAttributes': {
        brand: true,
        grade: {enabled: true},
        quality: {enabled: true},
        sampleNumber: true,
        fssaiLicenceNumber: true
    },
    'inventoryAttributes': {
        barcode: true,
        batchNumber: true,
        manufactureDate: true,
        packagingDate: true,
        bestBefore: true,
        unitMeasure: ['Kg', 'gram', 'tonne', 'Bag', 'Case', 'Pack']
    },
    'parent': db.categories.findOne({'name': 'Spices', 'type': 'SubCategory1'})._id
});
db.categories.insert({
    'name': 'Melon Seeds',
    'code': 'MLNSD',
    'type': 'SubCategory2',
    'categoryImageURL1': 'modules/categories/img/subcategory2/melonseeds.png',
    'productCategory': true,
    'productAttributes': {
        brand: true,
        grade: {enabled: true},
        quality: {enabled: true},
        sampleNumber: true,
        fssaiLicenceNumber: true
    },
    'inventoryAttributes': {
        barcode: true,
        batchNumber: true,
        manufactureDate: true,
        packagingDate: true,
        bestBefore: true,
        unitMeasure: ['Kg', 'gram', 'tonne', 'Bag', 'Case', 'Pack']
    },
    'parent': db.categories.findOne({'name': 'Spices', 'type': 'SubCategory1'})._id
});
db.categories.insert({
    'name': 'Jakayi',
    'code': 'JAKYI',
    'type': 'SubCategory2',
    'categoryImageURL1': 'modules/categories/img/subcategory2/jakayi.png',
    'productCategory': true,
    'productAttributes': {
        brand: true,
        grade: {enabled: true},
        quality: {enabled: true},
        sampleNumber: true,
        fssaiLicenceNumber: true
    },
    'inventoryAttributes': {
        barcode: true,
        batchNumber: true,
        manufactureDate: true,
        packagingDate: true,
        bestBefore: true,
        unitMeasure: ['Kg', 'gram', 'tonne', 'Bag', 'Case', 'Pack']
    },
    'parent': db.categories.findOne({'name': 'Spices', 'type': 'SubCategory1'})._id
});
db.categories.update({
    'name': 'Spices',
    'type': 'SubCategory1'
}, {$addToSet: {'children': db.categories.findOne({'name': 'Black Till', 'type': 'SubCategory2'})._id}});

db.categories.update({
    'name': 'Spices',
    'type': 'SubCategory1'
}, {$addToSet: {'children': db.categories.findOne({'name': 'White Till', 'type': 'SubCategory2'})._id}});

db.categories.update({
    'name': 'Spices',
    'type': 'SubCategory1'
}, {$addToSet: {'children': db.categories.findOne({'name': 'Poppy Seeds', 'type': 'SubCategory2'})._id}});

db.categories.update({
    'name': 'Spices',
    'type': 'SubCategory1'
}, {$addToSet: {'children': db.categories.findOne({'name': 'Javitri', 'type': 'SubCategory2'})._id}});

db.categories.update({
    'name': 'Spices',
    'type': 'SubCategory1'
}, {$addToSet: {'children': db.categories.findOne({'name': 'Sabja','type': 'SubCategory2'})._id}});

db.categories.update({
    'name': 'Spices',
    'type': 'SubCategory1'
}, {$addToSet: {'children': db.categories.findOne({'name': 'Sara Pappu','type': 'SubCategory2'})._id}});

db.categories.update({
    'name': 'Spices',
    'type': 'SubCategory1'
}, {$addToSet: {'children': db.categories.findOne({'name': 'Melon Seeds','type': 'SubCategory2'})._id}});

db.categories.update({
    'name': 'Spices',
    'type': 'SubCategory1'
}, {$addToSet: {'children': db.categories.findOne({'name': 'Jakayi','type': 'SubCategory2'})._id}});


