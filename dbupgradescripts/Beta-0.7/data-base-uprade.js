// Sub Categories of Agriculture
db.categories.insert({
    'name': 'Vegetable',
    'code': 'VGTBL',
    'type': 'SubCategory1',
    'categoryImageURL1': 'modules/categories/img/subcategory1/vegetables.png',
    'parent': db.categories.findOne({'name': 'Agriculture', 'type': 'MainCategory'})._id
});

db.categories.update({
    'name': 'Agriculture',
    'type': 'MainCategory'
}, {$addToSet: {'children': db.categories.findOne({'name': 'Vegetable', 'type': 'SubCategory1'})._id}});

// Sub Categories of Vegetables
db.categories.insert({
    'name': 'Raw Corn',
    'code': 'RCORN',
    'type': 'SubCategory2',
    'categoryImageURL1': 'modules/categories/img/subcategory2/rawcorn.png',
    'productCategory': true,
    'productAttributes': {
        brand: true,
        grade: {enabled: true},
        quality: {enabled: true},
        sampleNumber: true
    },
    'inventoryAttributes': {
        barcode: true,
        batchNumber: true,
        manufactureDate: true,
        packagingDate: true,
        bestBefore: true,
        unitMeasure: ['Kg', 'gms', 'Ton', 'Bag', 'Case', 'Pack']
    },
    'parent': db.categories.findOne({'name': 'Vegetable', 'type': 'SubCategory1'})._id
});
db.categories.insert({
    'name': 'Lemon',
    'code': 'LEMON',
    'type': 'SubCategory2',
    'categoryImageURL1': 'modules/categories/img/subcategory2/lemon.png',
    'productCategory': true,
    'productAttributes': {
        brand: true,
        grade: {enabled: true},
        quality: {enabled: true},
        sampleNumber: true
    },
    'inventoryAttributes': {
        barcode: true,
        batchNumber: true,
        manufactureDate: true,
        packagingDate: true,
        bestBefore: true,
        unitMeasure: ['Kg', 'gms', 'Ton', 'Bag', 'Case', 'Pack']
    },
    'parent': db.categories.findOne({'name': 'Vegetable', 'type': 'SubCategory1'})._id
});

// Adding Sub Categories to Cereals category
db.categories.update({
    'name': 'Vegetable',
    'type': 'SubCategory1'
}, {$addToSet: {'children': db.categories.findOne({'name': 'Raw Corn', 'type': 'SubCategory2'})._id}});

db.categories.update({
    'name': 'Vegetable',
    'type': 'SubCategory1'
}, {$addToSet: {'children': db.categories.findOne({'name': 'Lemon', 'type': 'SubCategory2'})._id}});

// Adding Fabric SubCategory1
db.categories.insert({
    'name': 'Fabric',
    'code': 'FABRC',
    'type': 'SubCategory1',
    'categoryImageURL1': 'modules/categories/img/subcategory1/fabric.png',
    'parent': db.categories.findOne({'name': 'Textiles', 'type': 'MainCategory'})._id
});

db.categories.update({
    'name': 'Textiles',
    'type': 'MainCategory'
}, {$addToSet: {'children': db.categories.findOne({'name': 'Fabric', 'type': 'SubCategory1'})._id}});

db.categories.insert({
    'name': 'Cotton Fabric',
    'code': 'COTFB',
    'type': 'SubCategory2',
    'categoryImageURL1': 'modules/categories/img/subcategory2/cottonfabric.png',
    'productCategory': true,
    'parent': db.categories.findOne({'name': 'Fabric', 'type': 'SubCategory1'})._id
});

db.categories.insert({
    'name': 'Silk Fabric',
    'code': 'SLKFB',
    'type': 'SubCategory2',
    'categoryImageURL1': 'modules/categories/img/subcategory2/silkfabric.png',
    'productCategory': true,
    'parent': db.categories.findOne({'name': 'Fabric', 'type': 'SubCategory1'})._id
});

db.categories.insert({
    'name': 'Wool Fabric',
    'code': 'WOLFB',
    'type': 'SubCategory2',
    'categoryImageURL1': 'modules/categories/img/subcategory2/woolfabric.png',
    'productCategory': true,
    'parent': db.categories.findOne({'name': 'Fabric', 'type': 'SubCategory1'})._id
});

// Adding Sub Categories to Fabric category
db.categories.update({
    'name': 'Fabric',
    'type': 'SubCategory1'
}, {$addToSet: {'children': db.categories.findOne({'name': 'Cotton Fabric', 'type': 'SubCategory2'})._id}});

db.categories.update({
    'name': 'Fabric',
    'type': 'SubCategory1'
}, {$addToSet: {'children': db.categories.findOne({'name': 'Silk Fabric', 'type': 'SubCategory2'})._id}});

db.categories.update({
    'name': 'Fabric',
    'type': 'SubCategory1'
}, {$addToSet: {'children': db.categories.findOne({'name': 'Wool Fabric', 'type': 'SubCategory2'})._id}});


// Adding Apparel SubCategory1
db.categories.insert({
    'name': 'Apparel',
    'code': 'APPRL',
    'type': 'SubCategory1',
    'categoryImageURL1': 'modules/categories/img/subcategory1/apparel.png',
    'parent': db.categories.findOne({'name': 'Textiles', 'type': 'MainCategory'})._id
});

db.categories.update({
    'name': 'Textiles',
    'type': 'MainCategory'
}, {$addToSet: {'children': db.categories.findOne({'name': 'Apparel', 'type': 'SubCategory1'})._id}});

db.categories.insert({
    'name': 'Cotton Apparel',
    'code': 'COTAP',
    'type': 'SubCategory2',
    'categoryImageURL1': 'modules/categories/img/subcategory2/cottonapparel.png',
    'productCategory': true,
    'parent': db.categories.findOne({'name': 'Apparel', 'type': 'SubCategory1'})._id
});

db.categories.insert({
    'name': 'Silk Apparel',
    'code': 'SLKAP',
    'type': 'SubCategory2',
    'categoryImageURL1': 'modules/categories/img/subcategory2/silkapparel.png',
    'productCategory': true,
    'parent': db.categories.findOne({'name': 'Apparel', 'type': 'SubCategory1'})._id
});

db.categories.insert({
    'name': 'Wool Apparel',
    'code': 'WOLAP',
    'type': 'SubCategory2',
    'categoryImageURL1': 'modules/categories/img/subcategory2/woolapparel.png',
    'productCategory': true,
    'parent': db.categories.findOne({'name': 'Apparel', 'type': 'SubCategory1'})._id
});

// Adding Sub Categories to Apparel category
db.categories.update({
    'name': 'Apparel',
    'type': 'SubCategory1'
}, {$addToSet: {'children': db.categories.findOne({'name': 'Cotton Apparel', 'type': 'SubCategory2'})._id}});

db.categories.update({
    'name': 'Apparel',
    'type': 'SubCategory1'
}, {$addToSet: {'children': db.categories.findOne({'name': 'Silk Apparel', 'type': 'SubCategory2'})._id}});

db.categories.update({
    'name': 'Apparel',
    'type': 'SubCategory1'
}, {$addToSet: {'children': db.categories.findOne({'name': 'Wool Apparel', 'type': 'SubCategory2'})._id}});

