db.orders.update(
    {},
    {
        $unset: {'invoice.invoiceNo': '', 'invoice.date': ''},
        $set: {
            'paymentOptionType.payOnDelivery.invoiceNo': '',
            'paymentOptionType.payOnDelivery.invoiceFile': '',
            'paymentOptionType.payOnDelivery.invoiceDate': '',
            'paymentOptionType.payNow.invoiceNo': '',
            'paymentOptionType.payNow.invoiceFile': '',
            'paymentOptionType.payNow.invoiceDate': '',
            'paymentOptionType.lineOfCredit.invoiceNo': '',
            'paymentOptionType.lineOfCredit.invoiceFile': '',
            'paymentOptionType.lineOfCredit.invoiceDate': ''
        }
    }, {upsert: false, multi: true}
);

db.orders.find({}).forEach(function (orderDoc) {
    if (orderDoc.paymentOptionType && orderDoc.paymentOptionType.installments
        && orderDoc.paymentOptionType.installments.enabled && orderDoc.paymentOptionType.installments.selection) {
        for (var i = 0; i < orderDoc.paymentOptionType.installments.options.length; i++) {
            orderDoc.paymentOptionType.installments.options[i].invoiceNo = '';
            orderDoc.paymentOptionType.installments.options[i].invoiceFile = '';
            orderDoc.paymentOptionType.installments.options[i].invoiceDate = '';
        }
        db.orders.save(orderDoc);
    }
});

db.companies.update(
    {},
    {
        $set: {
            'croppedProfileImageURL': 'modules/companies/img/profile/default-resize-240-240.png'
        }
    }, {upsert: false, multi: true}
);

db.contacts.update(
    {},
    {
        $set: {
            'croppedContactImageURL': 'modules/contacts/img/default-resize-240-240.png'
        }
    }, {upsert: false, multi: true}
);

db.users.update(
    {},
    {
        $set: {
            'croppedProfileImageURL': 'modules/users/img/profile/default-resize-240-240.png'
        }
    }, {upsert: false, multi: true}
);

db.categories.find({code: 'COTBL'}).forEach(function (categoryDoc) {
    categoryDoc.inventoryAttributes.unitMeasure.push('Candy');
    db.categories.save(categoryDoc);
});

db.categories.update(
    {
        'name': 'Cotton Yarn',
        'code': 'COTYN',
        'type': 'SubCategory2'
    },
    {
        $set: {
            'productAttributes': {
                brand: true,
                grade: {enabled: true},
                quality: {enabled: true},
                testCertifcate: true,
                sampleNumber: true
            },
            'inventoryAttributes': {
                barcode: true,
                batchNumber: true,
                manufactureDate: true,
                packagingDate: true,
                inventoryTestCertifcate: true,
                bestBefore: true,
                unitMeasure: ['Kg', 'Bundle', 'Ton', 'Bag', 'Case', 'Pack']
            },
            'productAttributes': {
                brand: true,
                grade: {enabled: true},
                quality: {enabled: true},
                testCertifcate: true,
                sampleNumber: true
            }
        }
    }, {upsert: false, multi: true}
);

db.categories.update(
    {
        'name': 'Silk Yarn',
        'code': 'SLKYN',
        'type': 'SubCategory2'
    },
    {
        $set: {
            'productAttributes': {
                brand: true,
                grade: {enabled: true},
                quality: {enabled: true},
                testCertifcate: true,
                sampleNumber: true
            },
            'inventoryAttributes': {
                barcode: true,
                batchNumber: true,
                manufactureDate: true,
                packagingDate: true,
                inventoryTestCertifcate: true,
                bestBefore: true,
                unitMeasure: ['Kg', 'Bundle', 'Ton', 'Bag', 'Case', 'Pack']
            },
            'productAttributes': {
                brand: true,
                grade: {enabled: true},
                quality: {enabled: true},
                testCertifcate: true,
                sampleNumber: true
            }
        }
    }, {upsert: false, multi: true}
);

db.categories.update(
    {
        'name': 'Wool Yarn',
        'code': 'WOLYN',
        'type': 'SubCategory2'
    },
    {
        $set: {
            'productAttributes': {
                brand: true,
                grade: {enabled: true},
                quality: {enabled: true},
                testCertifcate: true,
                sampleNumber: true
            },
            'inventoryAttributes': {
                barcode: true,
                batchNumber: true,
                manufactureDate: true,
                packagingDate: true,
                inventoryTestCertifcate: true,
                bestBefore: true,
                unitMeasure: ['Kg', 'Bundle', 'Ton', 'Bag', 'Case', 'Pack']
            },
            'productAttributes': {
                brand: true,
                grade: {enabled: true},
                quality: {enabled: true},
                testCertifcate: true,
                sampleNumber: true
            }
        }
    }, {upsert: false, multi: true}
);

db.categories.update(
    {
        'name': 'Cotton Fabric',
        'code': 'COTFB',
        'type': 'SubCategory2'
    },
    {
        $set: {
            'productAttributes': {
                brand: true,
                grade: {enabled: true},
                quality: {enabled: true},
                testCertifcate: true,
                sampleNumber: true
            },
            'inventoryAttributes': {
                barcode: true,
                batchNumber: true,
                manufactureDate: true,
                packagingDate: true,
                inventoryTestCertifcate: true,
                bestBefore: true,
                unitMeasure: ['Kg', 'Bundle', 'Ton', 'Bag', 'Case', 'Pack']
            },
            'productAttributes': {
                brand: true,
                grade: {enabled: true},
                quality: {enabled: true},
                testCertifcate: true,
                sampleNumber: true
            }
        }
    }, {upsert: false, multi: true}
);

db.categories.update(
    {
        'name': 'Silk Fabric',
        'code': 'SLKFB',
        'type': 'SubCategory2'
    },
    {
        $set: {
            'productAttributes': {
                brand: true,
                grade: {enabled: true},
                quality: {enabled: true},
                testCertifcate: true,
                sampleNumber: true
            },
            'inventoryAttributes': {
                barcode: true,
                batchNumber: true,
                manufactureDate: true,
                packagingDate: true,
                inventoryTestCertifcate: true,
                bestBefore: true,
                unitMeasure: ['Kg', 'Bundle', 'Ton', 'Bag', 'Case', 'Pack']
            },
            'productAttributes': {
                brand: true,
                grade: {enabled: true},
                quality: {enabled: true},
                testCertifcate: true,
                sampleNumber: true
            }
        }
    }, {upsert: false, multi: true}
);

db.categories.update(
    {
        'name': 'Wool Fabric',
        'code': 'WOLFB',
        'type': 'SubCategory2'
    },
    {
        $set: {
            'productAttributes': {
                brand: true,
                grade: {enabled: true},
                quality: {enabled: true},
                testCertifcate: true,
                sampleNumber: true
            },
            'inventoryAttributes': {
                barcode: true,
                batchNumber: true,
                manufactureDate: true,
                packagingDate: true,
                inventoryTestCertifcate: true,
                bestBefore: true,
                unitMeasure: ['Kg', 'Bundle', 'Ton', 'Bag', 'Case', 'Pack']
            },
            'productAttributes': {
                brand: true,
                grade: {enabled: true},
                quality: {enabled: true},
                testCertifcate: true,
                sampleNumber: true
            }
        }
    }, {upsert: false, multi: true}
);

db.categories.update(
    {
        'name': 'Cotton Apparel',
        'code': 'COTAP',
        'type': 'SubCategory2'
    },
    {
        $set: {
            'productAttributes': {
                brand: true,
                grade: {enabled: true},
                quality: {enabled: true},
                testCertifcate: true,
                sampleNumber: true
            },
            'inventoryAttributes': {
                barcode: true,
                batchNumber: true,
                manufactureDate: true,
                packagingDate: true,
                inventoryTestCertifcate: true,
                bestBefore: true,
                unitMeasure: ['Kg', 'Bundle', 'Ton', 'Bag', 'Case', 'Pack']
            },
            'productAttributes': {
                brand: true,
                grade: {enabled: true},
                quality: {enabled: true},
                testCertifcate: true,
                sampleNumber: true
            }
        }
    }, {upsert: false, multi: true}
);

db.categories.update(
    {
        'name': 'Silk Apparel',
        'code': 'SLKAP',
        'type': 'SubCategory2'
    },
    {
        $set: {
            'productAttributes': {
                brand: true,
                grade: {enabled: true},
                quality: {enabled: true},
                testCertifcate: true,
                sampleNumber: true
            },
            'inventoryAttributes': {
                barcode: true,
                batchNumber: true,
                manufactureDate: true,
                packagingDate: true,
                inventoryTestCertifcate: true,
                bestBefore: true,
                unitMeasure: ['Kg', 'Bundle', 'Ton', 'Bag', 'Case', 'Pack']
            },
            'productAttributes': {
                brand: true,
                grade: {enabled: true},
                quality: {enabled: true},
                testCertifcate: true,
                sampleNumber: true
            }
        }
    }, {upsert: false, multi: true}
);

db.categories.update(
    {
        'name': 'Wool Apparel',
        'code': 'WOLAP',
        'type': 'SubCategory2'
    },
    {
        $set: {
            'productAttributes': {
                brand: true,
                grade: {enabled: true},
                quality: {enabled: true},
                testCertifcate: true,
                sampleNumber: true
            },
            'inventoryAttributes': {
                barcode: true,
                batchNumber: true,
                manufactureDate: true,
                packagingDate: true,
                inventoryTestCertifcate: true,
                bestBefore: true,
                unitMeasure: ['Kg', 'Bundle', 'Ton', 'Bag', 'Case', 'Pack']
            },
            'productAttributes': {
                brand: true,
                grade: {enabled: true},
                quality: {enabled: true},
                testCertifcate: true,
                sampleNumber: true
            }
        }
    }, {upsert: false, multi: true}
);

