//
// Sub Categories of Agriculture
db.categories.insert({
    'name': 'Coffee',
    'code': 'COFFE',
    'type': 'SubCategory1',
    'categoryImageURL1': 'modules/categories/img/subcategory1/coffee.png',
    'parent': db.categories.findOne({'name': 'Agriculture', 'type': 'MainCategory'})._id
});

db.categories.update({
    'name': 'Agriculture',
    'type': 'MainCategory'
}, {$addToSet: {'children': db.categories.findOne({'name': 'Coffee', 'type': 'SubCategory1'})._id}});

// Sub Categories of Coffee
db.categories.insert({
    'name': 'Coffee Berries',
    'code': 'COBRS',
    'type': 'SubCategory2',
    'categoryImageURL1': 'modules/categories/img/subcategory2/coffeeberries.png',
    'productCategory': true,
    'productAttributes': {
        brand: true,
        grade: {enabled: true},
        quality: {enabled: true},
        sampleNumber: true
    },
    'inventoryAttributes': {
        barcode: true,
        batchNumber: true,
        manufactureDate: true,
        packagingDate: true,
        bestBefore: true,
        unitMeasure:['Kg', 'gram', 'tonne', 'Bag', 'Case', 'Pack']
    },
    'parent': db.categories.findOne({'name': 'Coffee','code':'COFFE', 'type': 'SubCategory1'})._id
});
db.categories.update({
    'name': 'Coffee',
    'type': 'SubCategory1'
}, {$addToSet: {'children': db.categories.findOne({'name': 'Coffee Berries', 'type': 'SubCategory2'})._id}});


// Sub Categories of Coffee
db.categories.insert({
    'name': 'Green Coffee Beans',
    'code': 'GCBNS',
    'type': 'SubCategory2',
    'categoryImageURL1': 'modules/categories/img/subcategory2/greencoffeebeans.png',
    'productCategory': true,
    'productAttributes': {
        brand: true,
        grade: {enabled: true},
        quality: {enabled: true},
        sampleNumber: true
    },
    'inventoryAttributes': {
        barcode: true,
        batchNumber: true,
        manufactureDate: true,
        packagingDate: true,
        bestBefore: true,
        unitMeasure: ['Kg', 'gram', 'tonne', 'Bag', 'Case', 'Pack']
    },
    'parent': db.categories.findOne({'name': 'Coffee', 'code': 'COFFE', 'type': 'SubCategory1'})._id
});
db.categories.update({
    'name': 'Coffee',
    'type': 'SubCategory1'
}, {$addToSet: {'children': db.categories.findOne({'name': 'Green Coffee Beans', 'type': 'SubCategory2'})._id}});


// Sub Categories of Coffee
db.categories.insert({
    'name': 'Roasted Coffee Beans',
    'code': 'RCBNS',
    'type': 'SubCategory2',
    'categoryImageURL1': 'modules/categories/img/subcategory2/roastedcoffeebeans.png',
    'productCategory': true,
    'productAttributes': {
        brand: true,
        grade: {enabled: true},
        quality: {enabled: true},
        sampleNumber: true
    },
    'inventoryAttributes': {
        barcode: true,
        batchNumber: true,
        manufactureDate: true,
        packagingDate: true,
        bestBefore: true,
        unitMeasure: ['Kg', 'gram', 'tonne', 'Bag', 'Case', 'Pack']
    },
    'parent': db.categories.findOne({'name': 'Coffee', 'code': 'COFFE', 'type': 'SubCategory1'})._id
});
db.categories.update({
    'name': 'Coffee',
    'type': 'SubCategory1'
}, {$addToSet: {'children': db.categories.findOne({'name': 'Roasted Coffee Beans', 'type': 'SubCategory2'})._id}});


//
// Sub Categories of Beverages
db.categories.insert({
    'name': 'Coffee Beans',
    'code': 'COFBN',
    'type': 'SubCategory2',
    'categoryImageURL1': 'modules/categories/img/subcategory2/coffeebeans.png',
    'productCategory': true,
    'productAttributes': {
        brand: true,
        grade: {enabled: true},
        quality: {enabled: true},
        sampleNumber: true
    },
    'inventoryAttributes': {
        barcode: true,
        batchNumber: true,
        manufactureDate: true,
        packagingDate: true,
        bestBefore: true,
        unitMeasure:['Kg', 'gram', 'tonne', 'Bag', 'Case', 'Pack']
    },
    'parent': db.categories.findOne({'name': 'Beverages', 'type': 'SubCategory1'})._id
});


db.categories.update({
    'name': 'Beverages',
    'type': 'SubCategory1'
}, {$addToSet: {'children': db.categories.findOne({'name': 'Coffee Beans', 'type': 'SubCategory2'})._id}});

// Sub Categories of Beverages
db.categories.insert({
    'name': 'Coffee Powder',
    'code': 'COPDR',
    'type': 'SubCategory2',
    'categoryImageURL1': 'modules/categories/img/subcategory2/coffeepowder.png',
    'productCategory': true,
    'productAttributes': {
        brand: true,
        grade: {enabled: true},
        quality: {enabled: true},
        sampleNumber: true
    },
    'inventoryAttributes': {
        barcode: true,
        batchNumber: true,
        manufactureDate: true,
        packagingDate: true,
        bestBefore: true,
        unitMeasure:['Kg', 'gram', 'tonne', 'Bag', 'Case', 'Pack']
    },
    'parent': db.categories.findOne({'name': 'Beverages', 'type': 'SubCategory1'})._id
});


db.categories.update({
    'name': 'Beverages',
    'type': 'SubCategory1'
}, {$addToSet: {'children': db.categories.findOne({'name': 'Coffee Powder', 'type': 'SubCategory2'})._id}});

db.categories.update({
    'name': 'Tea and Coffee',
    'type': 'SubCategory2'
}, {$set: {'name': 'Tea','code': 'TEAPD'}});
