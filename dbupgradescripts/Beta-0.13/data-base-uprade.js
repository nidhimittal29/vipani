// This file will have the data base upgrade scripts.
db.companies.update(
    {},
    {
        $set: {
            'reports': {
                'dailyStock': {
                    enabled: true,
                    format: ['PDF']
                }
            }
        }
    }, {upsert: false, multi: true}
);

db.inventories.update(
    {},
    {
        $set: {
            'tax': {
                VAT: 0,
                CST: 0,
                serviceTax: 0
            }
        }
    }, {upsert: false, multi: true}
);
