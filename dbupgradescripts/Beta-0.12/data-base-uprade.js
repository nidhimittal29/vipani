// This file will have the data base upgrade scripts.
db.inventories.update(
    {},
    {
        $rename: {'public': 'isPublic'}
    }, {upsert: false, multi: true}
);

db.companies.find({}).forEach(function (companyDoc) {
    if (companyDoc.emails) {
        for (var i = 0; i < companyDoc.emails.length; i++) {
            companyDoc.emails[i].isPublic = companyDoc.emails[i].public;
            delete companyDoc.emails[i].public;
        }
    }
    if (companyDoc.addresses) {
        for (var i = 0; i < companyDoc.addresses.length; i++) {
            companyDoc.addresses[i].isPublic = companyDoc.addresses[i].public;
            delete companyDoc.addresses[i].public;
        }
    }
    if (companyDoc.phones) {
        for (var i = 0; i < companyDoc.phones.length; i++) {
            companyDoc.phones[i].isPublic = companyDoc.phones[i].public;
            delete companyDoc.phones[i].public;
        }
    }
    db.companies.save(companyDoc);
});
