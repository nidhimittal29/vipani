'use strict';

module.exports = function (app) {
    var users = require('../../app/controllers/users.server.controller');
    var companies = require('../../app/controllers/companies.server.controller');

    // Companies Routes
    app.route('/companies')
        .get(companies.list)
        .post(users.requiresLogin, companies.create);
    app.route('/companies/profilePicture').post(companies.changeProfilePicture);
    app.route('/userCompanies')
        .get(users.requiresLogin, companies.userCompanies);
    app.route('/companies/:companyId')
        .get(companies.read)
        .put(users.requiresLogin, companies.hasAuthorization, companies.update);
        /*.delete(users.requiresLogin, companies.hasAuthorization, companies.delete);*/
    app.route('/companyPublicPage').get(companies.companyByProfileUrl);
    app.route('/companyProducts/:companyProducts').get(companies.read);
   /* app.param('companyProfileUrl', companies.companyByProfileUrl);*/
    app.param('companyRegistrations', companies.companyRegistrationSegments);
    app.param('companyProducts', companies.companyProducts);

    app.route('/companyInfo/:companyInfo').get(companies.read);
    app.route('/companyBusinessUnits/:companyBusinessUnit').get(companies.read);
    app.route('/companyBusinessUsers/:companyBusinessUser').get(companies.read);
    app.route('/companyInchargeUsers/:companyInchargeUser').get(companies.read);
    app.route('/companyDefaultBusinessUnit').get(companies.companyDefaultBusinessUnit);
    app.param('companyBusinessUnit', companies.companyBusinessUnits);
    app.param('companyBusinessUser', companies.companyBusinessUsers);
    app.param('companyInchargeUser', companies.companyInchargeUsers);
    app.param('companyInfo',companies.companyInfo);
    app.route('/createContacts/:companyId')
        .get(companies.createContacts)
        .put(companies.createContacts);
    app.route('/userBusinessUnits/:nvipaniRegUser')
        .get(users.requiresLogin,companies.listUserBusinessUnits);
    app.route('/companies')
        .get(companies.list)
        .post(users.requiresLogin, companies.create);
    // Finish by binding the Company middleware
    app.param('companyId', companies.companyByID);
};
