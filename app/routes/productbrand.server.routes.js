'use strict';

module.exports = function (app) {
    var users = require('../../app/controllers/users.server.controller');
    var productBrand = require('../../app/controllers/productbrand.server.controller');

    // Todos Routes
    app.route('/productBrand')
        .get(productBrand.list)
        .post(users.requiresLogin, productBrand.createProductBrand);

    app.route('/productBrand/:brandId')
        .get(productBrand.read)
        .put(productBrand.update)
        /*.delete(users.requiresLogin, productBrand.hasAuthorization, productBrand.delete)*/;

    // Finish by binding the Todo middleware
    app.param('brandId', productBrand.brandById);
};
