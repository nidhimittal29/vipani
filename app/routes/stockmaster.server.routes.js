'use strict';

module.exports = function (app) {
    var users = require('../../app/controllers/users.server.controller');
    var stockMaster = require('../../app/controllers/stockmaster.server.controller');

    // Todos Routes
    app.route('/stockMaster')
        .get(stockMaster.list)
        .post(users.requiresLogin, stockMaster.create);
    app.route('/stockMastersMassImport')
        .get(stockMaster.list)
        .post(users.requiresLogin, stockMaster.createMassInsert);
    app.route('/stockMaster/:stockMasterById')
        .get(stockMaster.read)
        .put(stockMaster.update)
        /*.delete(users.requiresLogin, stockMaster.hasAuthorization, stockMaster.delete)*/;
    // Finish by binding the Todo middleware
    app.param('stockMasterById', stockMaster.stockMasterById);
};
