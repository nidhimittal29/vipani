'use strict';

module.exports = function (app) {
    var users = require('../../app/controllers/users.server.controller');
    var inventories = require('../../app/controllers/inventories.server.controller');

    // Inventories Routes
    app.route('/inventories')
        .get(users.requiresLogin,inventories.list)
        .post(users.requiresLogin, inventories.create);

    app.route('/inventories/:inventoryId')
        .get(inventories.read)
        .put(users.requiresLogin, inventories.hasAuthorization, inventories.update);
    /*.delete(users.requiresLogin, inventories.hasAuthorization, inventories.delete);*/

    // Finish by binding the Inventory middleware
    app.route('/inventories/createProduct').post(users.requiresLogin,inventories.createProduct);
    app.route('/inventories/createInventoryMaster').post(users.requiresLogin,inventories.createInventoryMaster);
    app.route('/inventories/massUpdateImages').post(users.requiresLogin,inventories.massUpdateImages);
    app.route('/inventories/createUserItemMaster').post(users.requiresLogin,inventories.createUserInventoryImport);
    app.route('/inventories/createInventoryConversion').post(users.requiresLogin,inventories.convertInventory);
    app.route('/inventories/matchedinventories').post(users.requiresLogin,inventories.searchInventory);
    app.route('/busUnitInventories/:busUnitId').get(users.requiresLogin,inventories.inventoryByBusinessUnit);
    app.route('/inventoriesremove').post(users.requiresLogin,inventories.massDelete);
    app.route('/inventoriesactive').post(users.requiresLogin,inventories.massEnable);
    app.route('/inventoriesinactive').post(users.requiresLogin,inventories.massDisable);
    app.param('inventoryId', inventories.inventoryByID);
    app.route('/inventories/:inventoryTrackId').get(users.requiresLogin,inventories.list);
    app.route('/productInventories/:category').get(users.requiresLogin,inventories.productList);
    app.route('/trackPaths')
        .get(inventories.getTrackerPaths);
    app.route('/trackForwardPaths')
        .get(inventories.getForwardTrackerPaths);
    app.route('/tracker').get(inventories.getPublicPaths);
    app.route('/scanProduct').get(inventories.scanProduct);
    app.route('/redirectTracker').get(inventories.redirectPublicPaths);
    app.route('/inventorysummary')
        .get(inventories.getInventorySummaries)
        .post(inventories.getInventorySummaries);
};
