'use strict';

module.exports = function(app) {
	var users = require('../../app/controllers/users.server.controller');
	var offers = require('../../app/controllers/offers.server.controller');

	// Offers Routes
	app.route('/offers')
		.get(users.requiresLogin, offers.listByBusinessUnit)
		.post(users.requiresLogin, offers.create);

	/*app.route('/offersQuery')
			.get(users.requiresLogin, offers.listPublicOffers);
	 */
	app.route('/receivedOffers')
		.get(users.requiresLogin, offers.listReceivedOffers);

	app.route('/offers/:offerId')
		.get(users.requiresLogin, offers.hasAuthorization,offers.read)
		.put(users.requiresLogin, offers.hasAuthorization, offers.update)
		.delete(users.requiresLogin, offers.hasAuthorization, offers.delete);

	// Finish by binding the Offer middleware
	app.param('offerId', offers.offerByID);

	app.route('/offersByBusinessUnit').get(users.requiresLogin,offers.listByBusinessUnit);
    //app.route('/receivedOffersByBusinessUnit').get(users.requiresLogin,offers.listReceivedOffersByBusinessUnit);


	app.route('/createOrder/:offerId')
		.get(users.requiresLogin, offers.hasAuthorization,offers.createOrder)
		.put(users.requiresLogin, offers.hasAuthorization,offers.createOrder);
		/*.delete(users.requiresLogin, offers.hasAuthorization, offers.delete);*/
	app.route('/cloneOffer/:offerId')
		.get(users.requiresLogin, offers.hasAuthorization,offers.cloneOffer)
		.put(users.requiresLogin, offers.hasAuthorization, offers.update);
		/*.delete(users.requiresLogin, offers.hasAuthorization, offers.delete);*/
    app.route('/createProductOrder')
        .get(users.requiresLogin, offers.createOrder)
        .post(users.requiresLogin, offers.createOrder);
	app.route('/subCategory2Offers/:subCategory2Id').get(offers.listSubCategory2Offers);

	app.route('/publicOffersBySegments').get(offers.findByPublicOffers);
    app.route('/publicOffersBySegments/:segment').get(offers.findByPublicOffers);
    app.route('/offersremove').post(users.requiresLogin,offers.massDelete);
    app.route('/offersactive').post(users.requiresLogin,offers.massEnable);
    app.route('/offersinactive').post(users.requiresLogin,offers.massDisable);
    app.route('/offersnotify').post(users.requiresLogin,offers.massSendNotification);
    app.route('/offerusercontacts').get(offers.findByFromContactOffers);
    app.route('/offerusercontacts/:contactCompany').get(offers.findByFromContactOffers);
    app.route('/offerssourcesummary')
		.get(offers.getSourceSummaries)
		.post(offers.getSourceSummaries);
    app.route('/offerstargetsummary')
		.get(users.requiresLogin,offers.getTargetSummaries)
		.post(users.requiresLogin,offers.getTargetSummaries);

};
