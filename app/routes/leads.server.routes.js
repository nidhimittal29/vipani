'use strict';

module.exports = function(app) {
	var users = require('../../app/controllers/users.server.controller');
	var leads = require('../../app/controllers/leads.server.controller');

	// Products Routes
	app.route('/leads')
		.get(leads.list)
		.post(leads.create);

	app.route('/leads/:leadId')
		.get(leads.read)
		.put(leads.update);
		/*.delete(leads.delete);*/

	// Finish by binding the Product middleware
	app.param('leadId', leads.leadByID);
};
