'use strict';

module.exports = function(app) {
	var users = require('../../app/controllers/users.server.controller');
	var products = require('../../app/controllers/products.server.controller');

	// Products Routes
	app.route('/products')
		.get(users.requiresLogin, products.list)
		.post(users.requiresLogin, products.createProduct);

	app.route('/products/productPicture').post(products.changeProductPicture);
	app.route('/products/:productId')
		.get(users.requiresLogin, products.read)
		.put(users.requiresLogin, products.hasAuthorization, products.update)
		/*.delete(users.requiresLogin, products.hasAuthorization, products.delete)*/;
	app.route('/subcategory2inventory')
		.get(users.requiresLogin,products.subCategory2InventoryLists);
    app.route('/productSearch')
        .get(users.requiresLogin,products.productSearch);
	// Finish by binding the Product middleware
	app.param('productId', products.productByID);
    app.param('searchKey', products.subCategory2InventoryLists);
};
