'use strict';

module.exports = function (app) {
    var users = require('../../app/controllers/users.server.controller');
    var reports = require('../../app/controllers/reports.server.controller');

    // Reports Routes
    app.route('/reports')
        .get(reports.list)
        .post(users.requiresLogin, reports.create);

    app.route('/reports/:reportId')
        .get(reports.read)
        .put(users.requiresLogin, reports.hasAuthorization, reports.update)
        /*.delete(users.requiresLogin, reports.hasAuthorization, reports.delete)*/;


   /* app.route('/reports/createReport')
        .post(users.requiresLogin, reports.generateReport());*/
    // Finish by binding the Report middleware
    app.param('reportId', reports.reportByID);
    app.route('/companyReports')
        .get(users.requiresLogin,reports.generateReport);
    app.route('/reportPath/:reportId')
        .get(users.requiresLogin, reports.getReportPath);
    app.route('/files')
        .get(users.requiresSpecificLogin, reports.getFiles);
};
