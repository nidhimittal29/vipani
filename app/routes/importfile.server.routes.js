'use strict';

module.exports = function (app) {
    var users = require('../../app/controllers/users.server.controller');
    var importFile = require('../../app/controllers/importfiles.server.controller');

    // Todos Routes
    app.route('/importFile')
        .get(importFile.list)
        .post(users.requiresLogin, importFile.create);

    app.route('/importFile/:importFileId')
        .get(importFile.read)
        .put(importFile.update);
        /*.delete(users.requiresLogin, importFile.hasAuthorization, importFile.delete);*/

    // Finish by binding the Todo middleware
    app.param('importFileId', importFile.importFileByID);
};
