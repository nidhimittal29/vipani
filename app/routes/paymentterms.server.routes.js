'use strict';

module.exports = function (app) {
    var users = require('../controllers/users.server.controller');
    var paymentterms = require('../controllers/paymentterms.server.controller');

    // PaymentTerms Routes
    app.route('/paymentterms')
        .get(users.requiresLogin, paymentterms.list)
        .post(users.requiresLogin, paymentterms.create);
    app.route('/paymentterms/:paymenttermId')
        .get(users.requiresLogin, paymentterms.read)
        .put(users.requiresLogin, paymentterms.hasAuthorization, paymentterms.update)
        /*.delete(users.requiresLogin, paymentterms.hasAuthorization, paymentterms.delete)*/;
    app.route('/companypaymentterms').get(paymentterms.getPaymentTerms);
    app.route('/companypaymentmodes').get(paymentterms.getPaymentModes);
    app.param('paymenttermId', paymentterms.paymentTermByID);
};
