
'use strict';

module.exports = function(app) {
	var users = require('../../app/controllers/users.server.controller');
	var contacts = require('../../app/controllers/contacts.server.controller');

	// Contacts Routes
	app.route('/contacts')
		.get(users.requiresLogin, contacts.list)
		.post(users.requiresLogin, contacts.create);
	app.route('/contacts/contactPicture').post(contacts.changeContactProfilePicture);
    app.route('/contacts/customers').get(users.requiresLogin,contacts.queryByCustomer);
    app.route('/contacts/suppliers').get(users.requiresLogin,contacts.queryBySupplier);
   /* app.route('/masscontacts').post(users.requiresLogin, contacts.massDelete);*/
    app.route('/contactsremove').post(users.requiresLogin,contacts.massDelete);
    app.route('/contactsactive').post(users.requiresLogin,contacts.massEnable);
    app.route('/contactsinactive').post(users.requiresLogin,contacts.massDisable);
    app.route('/contactsimport').post(users.requiresLogin,contacts.massImport);
    app.route('/contactsPreimport').post(users.requiresLogin,contacts.preMassImportValidate);
	app.route('/contacts/:contactId')
		.get(users.requiresLogin, contacts.read)
		.put(users.requiresLogin, contacts.hasAuthorization, contacts.update);

		/*.delete(users.requiresLogin, contacts.hasAuthorization, contacts.delete);*/
	app.route('/contactscheck/:companyId')
		.get(contacts.getValidContact);
	// Finish by binding the Contact middleware
	app.param('contactId', contacts.contactByID);
    app.route('/contactOffers/:offerContactId').get(users.requiresLogin,contacts.getContactOffers);
    app.route('/contactOrders/:orderContactId').get(users.requiresLogin,contacts.getContactOrders);
    app.route('/groupOffers/:offerGroupId').get(users.requiresLogin,contacts.getGroupOffers);
    app.route('/customercontactssummary')
        .get(contacts.getCustomerContactsSummaries)
        .post(contacts.getCustomerContactsSummaries);
    app.route('/suppliercontactssummary')
        .get(users.requiresLogin,contacts.getSupplierContactsSummaries)
        .post(users.requiresLogin,contacts.getSupplierContactsSummaries);
};
