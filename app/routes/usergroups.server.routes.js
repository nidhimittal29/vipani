'use strict';

module.exports = function (app) {
    var users = require('../controllers/users.server.controller');
    var userGroups = require('../controllers/usergroups.server.controller');

    // userGroups Routes
    app.route('/usergroups')
        .get(users.requiresLogin, userGroups.list)
        .post(users.requiresLogin, userGroups.create);
    app.route('/usergroups/:userGroupId')
        .get(users.requiresLogin, userGroups.read)
        .put(users.requiresLogin, userGroups.hasAuthorization, userGroups.update)
        /*.delete(users.requiresLogin, userGroups.hasAuthorization, paymentterms.delete)*/;
    app.param('userGroupId', userGroups.userGroupByID);
};
