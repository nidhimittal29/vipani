'use strict';

module.exports = function (app) {
    var users = require('../controllers/users.server.controller');
    var subscriptions = require('../controllers/subscriptions.server.controller');

    // PaymentTerms Routes
    app.route('/subscriptions')
        .get(users.requiresLogin, subscriptions.list)
        .post(users.requiresLogin, subscriptions.create);
    app.route('/subscriptions/:subscriptionId')
        .get(users.requiresLogin, subscriptions.read)
        .put(users.requiresLogin, subscriptions.hasAuthorization, subscriptions.update)
        /*.delete(users.requiresLogin, subscriptions.hasAuthorization, subscriptions.delete)*/;
    app.param('subscriptionId', subscriptions.subscriptionByID);
};
