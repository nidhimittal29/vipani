'use strict';

module.exports = function (app) {
    var users = require('../../app/controllers/users.server.controller');
    var businessUnit = require('../../app/controllers/businessunit.server.controller');

    // Business unit Routes

    /**
     * input for get: nothing
     * input for post: businessunit object,refer businessunit model for required fields
     *
     */
    app.route('/branches')
        .get(businessUnit.list)
        .post(users.requiresLogin, businessUnit.create)
        .put(users.requiresLogin,businessUnit.update);
    app.route('/businessUnitsRemove')
        .post(users.requiresLogin,businessUnit.massDelete);
    app.route('/businessUnitsEnable')
        .post(users.requiresLogin,businessUnit.massEnable);
    app.route('/businessUnitsDisable')
        .post(users.requiresLogin,businessUnit.massDisable);



    app.route('/branches/:bunitId')
        .get(businessUnit.read)
        .put(users.requiresLogin, businessUnit.hasAuthorization, businessUnit.update);
    // Finish by binding the Business unit middleware
    app.param('bunitId', businessUnit.businessUnitByID);
    app.route('/brancheusers/:bunitIdUsers')
        .get(businessUnit.read);
    app.param('bunitIdUsers', businessUnit.getBusinessUsersByBusinessId);

};
