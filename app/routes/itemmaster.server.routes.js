'use strict';

module.exports = function (app) {
    var users = require('../../app/controllers/users.server.controller');
    var itemMaster = require('../../app/controllers/itemmaster.server.controller');

    // Todos Routes
    app.route('/itemMaster')
        .get(itemMaster.list)
        .post(users.requiresLogin, itemMaster.create);

    app.route('/itemMaster/:itemId')
        .get(itemMaster.read)
        .put(itemMaster.update);
        /*.delete(users.requiresLogin, itemMaster.hasAuthorization, itemMaster.delete);*/

    // Finish by binding the Todo middleware
    app.param('itemId', itemMaster.itemById);
};
