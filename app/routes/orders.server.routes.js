'use strict';

module.exports = function(app) {
	var users = require('../../app/controllers/users.server.controller');
	var orders = require('../../app/controllers/orders.server.controller');

	// Orders Routes
	app.route('/orders')
		/*.get(users.requiresLogin, orders.list)*/
		.post(users.requiresLogin, orders.create); //create is never used. instead use /createOrder/offerId
	app.route('/ordersQuery')
		.get(users.requiresLogin, orders.listSpecificOrder);
    app.route('/ordersEnums')
        .get(users.requiresLogin, orders.orderEnumValues);
	app.route('/orders/:orderId')
		.get(users.requiresLogin,orders.hasAuthorization, orders.read)
		.put(users.requiresLogin, orders.hasAuthorization,orders.update)
		/*.delete(users.requiresLogin, orders.hasAuthorization, orders.delete)*/;
	app.route('/invoiceOrder/:orderId')/*
		.get(users.requiresLogin,orders.hasAuthorization, orders.read)*/
		.get(users.requiresLogin,orders.hasAuthorization, orders.invoiceReport);

    app.route('/ordersByBusinessUnit')
        .get(users.requiresLogin, orders.listOrdersByBusinessUnit);

	app.param('orderId', orders.orderByID);

    app.route('/ordersremove').post(users.requiresLogin,orders.massDelete);
    app.route('/offerOrders/:orderOfferId').get(users.requiresLogin,orders.getOrdersByOffer);
    app.route('/sourcesummary')
		.get(orders.getSourceSummaries)
		.post(orders.getSourceSummaries);
    app.route('/targetummary')
		.get(users.requiresLogin,orders.getTargetSummaries)
		.post(users.requiresLogin,orders.getTargetSummaries);
    app.route('/orderpaymentterms').get(orders.getPaymentTerms);
    app.route('/orderpaymentmodes').get(orders.getPaymentModes);

};
