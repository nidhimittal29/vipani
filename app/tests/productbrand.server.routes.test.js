'use strict';

/**
 * Module dependencies.
 */
var should = require('should'),
    request = require('supertest'),
    app = require('../../server'),
    mongoose = require('mongoose'),
    User = mongoose.model('User'),
    Inventory = mongoose.model('Inventory'),
    Contact = mongoose.model('Contact'),
    Company = mongoose.model('Company'),
    Category = mongoose.model('Category'),
    ProductBrand = mongoose.model('ProductBrand'),
    agent = request.agent(app),
    async = require('async');
/**
 * Globals
 */
var credentials,user,token, users, inventory, productInventory,savedContact,offer,order,productNum,offers,productCategory;

/**
 * Unit tests
 */
describe('User Model Unit Tests:', function() {

    /*before(function(done) {
        user = {
            /!*	firstName: 'Full',
                lastName: 'Name',
                displayName: 'Full Name',*!/
            username: 'hema@nvipani.com',
            issendotp:false,
            isverifyotp:false,
            acceptTerms:true/!*,
			username: 'username',
			password: 'password',
			provider: 'local'*!/
        };
        user.issendotp=true;
        agent.post('/user/sendpresignupotp')
            .send(user)
            .expect(200)
            .end(function (presignupErr, presignupRes) {
                //console.log("Presignup Response - "+JSON.stringify(presignupRes.message));

                if (presignupErr) done(presignupErr);
                /!* user.issendotp=true;
                 user.isverifyotp=true;*!/
                else {
                    (presignupRes.body.user.status).should.equal('Register Request');
                    user.otp = presignupRes.body.otp;
                    user.issendotp = false;
                    user.isverifyotp = true;
                    agent.post('/user/sendpresignupotp')
                        .send(user)
                        .expect(200)
                        .end(function (presignupErr, verifiedOtp) {
                            //console.log("Presignup Response - "+JSON.stringify(presignupRes.message));
                            if (presignupErr) done(presignupErr);
                            (verifiedOtp.body.user.status).should.equal('Verified');
                            if(verifiedOtp.body.user.username===verifiedOtp.body.user.email) {
                                (verifiedOtp.body.user.emailVerified).should.equal(true);
                            }else if(verifiedOtp.body.username===verifiedOtp.body.mobile) {
                                (verifiedOtp.body.user.mobileVerified).should.equal(true);

                            }

                            user.password = 'welcome';
                            user.confirmPassword = 'welcome';
                            user.mobile = '9739206255';
                            user.issendotp = false;
                            user.isverifyotp = false;
                            user.ispassword = true;
                            agent.post('/user/sendpresignupotp')
                                .send(user)
                                .expect(200)
                                .end(function (presignupErr, changePassword) {
                                    //console.log("Presignup Response - "+JSON.stringify(presignupRes.message));
                                    if (presignupErr) done(presignupErr);
                                    (changePassword.body.user.status).should.equal('Registered');
                                     token=changePassword.body.token;
                                    var mainCategory = {
                                        name: 'Agriculture',
                                        code: 'AGR',
                                        type: 'MainCategory'
                                    };
                                    agent.post('/categories')
                                        .send(mainCategory)
                                        .set('Content-Type', 'application/json')
                                        .set('token',token )
                                        .expect(200)
                                        .end(function (categorySaveErr, categorySaveRes) {
                                            // Handle Category save error
                                            if (categorySaveErr) done(categorySaveErr);

                                            mainCategory = categorySaveRes.body;

                                            var subCategory1 = {
                                                name: 'Vegetables',
                                                code: 'VEG',
                                                type: 'SubCategory1',
                                                categoryImageURL1: 'modules/categories/img/subcategory1/vegetables.png'
                                            };

                                            // Save a new Category
                                            //category.token = token;
                                            agent.post('/categories')
                                                .send(subCategory1)
                                                .set('Content-Type', 'application/json')
                                                .set('token', token)
                                                .expect(200)
                                                .end(function (subCategorySaveErr, subCategorySaveRes) {
                                                    // Handle Category save error
                                                    if (subCategorySaveErr) done(subCategorySaveErr);

                                                    var subCategory1 = subCategorySaveRes.body;

                                                    mainCategory.children.push(subCategory1);

                                                    // Update existing Category
                                                    agent.put('/categories/' + mainCategory._id)
                                                        .send(mainCategory)
                                                        .set('Content-Type', 'application/json')
                                                        .set('token', token)
                                                        .expect(200)
                                                        .end(function (categoryUpdateErr, categoryUpdateRes) {
                                                            // Handle Category update error
                                                            if (categoryUpdateErr) done(categoryUpdateErr);

                                                            var subCategory2 = {
                                                                name: 'Lemon',
                                                                code: 'LEM',
                                                                type: 'SubCategory2',
                                                                categoryImageURL1: 'modules/categories/img/subcategory2/lemon.png'
                                                            };

                                                            // Save a new Category
                                                            //category.token = token;
                                                            agent.post('/categories')
                                                                .send(subCategory2)
                                                                .set('Content-Type', 'application/json')
                                                                .set('token', token)
                                                                .expect(200)
                                                                .end(function (subCategory2SaveErr, subCategory2SaveRes) {
                                                                    // Handle Category save error
                                                                    if (subCategory2SaveErr) done(subCategory2SaveErr);

                                                                    subCategory2 = subCategory2SaveRes.body;

                                                                    subCategory1.children.push(subCategory2);

                                                                    // Update existing Category
                                                                    agent.put('/categories/' + subCategory1._id)
                                                                        .send(subCategory1)
                                                                        .set('Content-Type', 'application/json')
                                                                        .set('token', token)
                                                                        .expect(200)
                                                                        .end(function (subCategoryUpdateErr, subCategoryUpdateRes) {
                                                                            // Handle Category update error
                                                                            if (subCategoryUpdateErr) done(subCategoryUpdateErr);
                                                                            else {
                                                                                productCategory=subCategory2;
                                                                                done();
                                                                            }

                                                                        });
                                                                });
                                                        });
                                                });
                                        });
                                });
                        });
                }
            });

    });*/
    var credentials, user, contact;
    beforeEach(function(done) {
        Category.remove().exec();
        var regUser = {
            username: 'test1@test.com',
            companyName: 'nVipani',
            businessType: 'Trader',
            categorySeller: true,
            categoryBuyer: true,
            categoryMediator: true,
            mobile: '0123456789',
            acceptTerms: true
        };
        agent.post('/user/sendpresignupotp')
            .send({username: regUser.username, password: 'password', acceptTerms: true,issendotp:true})
            .expect(200)
            .end(function (presignupPasswordErr, presignupPasswordRes) {
                if (presignupPasswordErr) done(presignupPasswordErr);
                if (presignupPasswordRes.body.user.status === 'Register Request') {
                    (presignupPasswordRes.body.user.status).should.equal('Register Request');
                    console.log('OTP:'+presignupPasswordRes.body.otp);
                    regUser.otp = presignupPasswordRes.body.otp;
                    regUser.isverifyotp = true;
                    regUser.issendotp=false;
                    agent.post('/user/sendpresignupotp')
                        .send(regUser)
                        .expect(200)
                        .end(function (verifiedOtpErr, verifiedOtpFirstUser) {
                            if (verifiedOtpErr) done(verifiedOtpErr);
                            (verifiedOtpFirstUser.body.user.status).should.equal('Verified');

                            regUser.registrationCategories = verifiedOtpFirstUser.body.registrationCategories;
                            regUser.segments = verifiedOtpFirstUser.body.segments;
                            regUser.categories = verifiedOtpFirstUser.body.categories;
                            var selectedSegments = [];
                            //for rice no categories.
                            selectedSegments.push({
                                segment: regUser.segments[0]._id,
                                categories: []
                            });
                            agent.post('/user/sendpresignupotp')
                                .send({
                                    username: regUser.username,
                                    registrationCategory: regUser.registrationCategories[0]._id,
                                    ispassword: true,
                                    selectedSegments: selectedSegments
                                })
                                .expect(200)
                                .end(function (sendBusinessSegmentPasswordErr, verifiedSendBusinessSegmentPasswordUser) {
                                    if (sendBusinessSegmentPasswordErr) done(sendBusinessSegmentPasswordErr);
                                    verifiedSendBusinessSegmentPasswordUser.body.status.should.equal(true);
                                    //We need to set proper message for the final stage.
                                    verifiedSendBusinessSegmentPasswordUser.body.message.should.equal('An OTP has been ' + verifiedSendBusinessSegmentPasswordUser.body.user.status + ' for the ' + regUser.username);
                                    user = verifiedSendBusinessSegmentPasswordUser.body.user;
                                    var mainCategory = {
                                        name: 'Agriculture',
                                        code: 'AGR',
                                        type: 'MainCategory'
                                    };
                                    agent.post('/auth/signin')
                                        .send(credentials)
                                        .expect(200)
                                        .end(function (err, signinres) {
                                            should.not.exist(err);
                                            token = signinres.body.token;
                                            agent.post('/createMainCategory')
                                                .send(mainCategory)
                                                .set('Content-Type', 'application/json')
                                                .set('token', token)
                                                .expect(200)
                                                .end(function (categorySaveErr, categorySaveRes) {
                                                    // Handle Category save error
                                                    if (categorySaveErr)
                                                    {
                                                        done(categorySaveErr);
                                                    }

                                                    mainCategory = categorySaveRes.body;

                                                    var subCategory1 = {
                                                        name: 'Vegetables',
                                                        code: 'VEG',
                                                        type: 'SubCategory1',
                                                        categoryImageURL1: 'modules/categories/img/subcategory1/vegetables.png'
                                                    };

                                                    // Save a new Category
                                                    //category.token = token;
                                                    agent.post('/createMainCategory')
                                                        .send(subCategory1)
                                                        .set('Content-Type', 'application/json')
                                                        .set('token', token)
                                                        .expect(200)
                                                        .end(function (subCategorySaveErr, subCategorySaveRes) {
                                                            // Handle Category save error
                                                            if (subCategorySaveErr) {

                                                                console.log(subCategorySaveErr.stack);
                                                                done(subCategorySaveErr);
                                                            }

                                                            var subCategory1 = subCategorySaveRes.body;

                                                            mainCategory.children.push(subCategory1);

                                                            // Update existing Category
                                                            agent.put('/createMainCategory/' + mainCategory._id)
                                                                .send(mainCategory)
                                                                .set('Content-Type', 'application/json')
                                                                .set('token', token)
                                                                .expect(200)
                                                                .end(function (categoryUpdateErr, categoryUpdateRes) {
                                                                    // Handle Category update error
                                                                    if (categoryUpdateErr){
                                                                        console.log(categoryUpdateErr.stack);
                                                                        done(categoryUpdateErr);
                                                                    }

                                                                    var subCategory2 = {
                                                                        name: 'Lemon',
                                                                        code: 'LEM',
                                                                        type: 'SubCategory2',
                                                                        categoryImageURL1: 'modules/categories/img/subcategory2/lemon.png'
                                                                    };

                                                                    // Save a new Category
                                                                    //category.token = token;
                                                                    agent.post('/createMainCategory')
                                                                        .send(subCategory2)
                                                                        .set('Content-Type', 'application/json')
                                                                        .set('token', token)
                                                                        .expect(200)
                                                                        .end(function (subCategory2SaveErr, subCategory2SaveRes) {
                                                                            // Handle Category save error
                                                                            if (subCategory2SaveErr){
                                                                                console.log(subCategory2SaveErr.stack);
                                                                                done(subCategory2SaveErr);
                                                                            }

                                                                            subCategory2 = subCategory2SaveRes.body;

                                                                            subCategory1.children.push(subCategory2);

                                                                            // Update existing Category
                                                                            agent.put('/createMainCategory/' + subCategory1._id)
                                                                                .send(subCategory1)
                                                                                .set('Content-Type', 'application/json')
                                                                                .set('token', token)
                                                                                .expect(200)
                                                                                .end(function (subCategoryUpdateErr, subCategoryUpdateRes) {
                                                                                    // Handle Category update error
                                                                                    if (subCategoryUpdateErr){
                                                                                        console.log(subCategoryUpdateErr.stack);
                                                                                        done(subCategoryUpdateErr);
                                                                                    }
                                                                                    else {
                                                                                        productCategory = subCategory2;
                                                                                        done();
                                                                                    }

                                                                                });
                                                                        });
                                                                });
                                                        });
                                                });

                                        });
                                });
                            });
                    }
                });
                credentials = {
                    username: 'test1@test.com',
                    password: 'password'
                };
    });

    it('should be able to create Product Brand', function (done) {

        var productBrand = {
            name: 'NLR Rice',
            productCategory: productCategory._id,
            productBrandImageURL1: 'modules/categories/img/subcategory2/rice.png'
        };

        agent.post('/auth/signin')
            .send(credentials)
            .expect(200)
            .end(function(signerr,signinRes) {
                if (signerr) {
                    console.log(signerr.stack);
                    done(signerr);
                }
                else {
                    token = signinRes.body.token;
                    agent.post('/productBrand')
                        .send(productBrand)
                        .set('token', token)
                        .set('Content-Type', 'application/json')
                        .expect(200)
                        .end(function (subCategoryUpdateErr, productBrandUpdateRes) {
                            // Handle Category update error
                            if (subCategoryUpdateErr) {
                                done(subCategoryUpdateErr);
                            }

                            var eachBrand = productBrandUpdateRes.body;
                            console.log(eachBrand);
                            eachBrand.name = 'Nellore Rice';
                            agent.put('/productBrand/' + eachBrand.productBrands[0]._id)
                                .send({name:'Nellore Rice'})
                                .set('Content-Type', 'application/json')
                                .set('token', token)
                                .expect(200)
                                .end(function (productBrandSaveErr, updateProductBrand) {
                                    // Handle Product Brand update error
                                    if (productBrandSaveErr) {
                                        done(productBrandSaveErr);
                                    }
                                    else {
                                        updateProductBrand.body.name.should.equal(eachBrand.name);
                                        done();
                                    }
                                });
                        });
                }
            });
    });
    it('should be able to list Product Brands', function (done) {

        var productBrand1 = {
            name: 'NLR Rice',
            productCategory: productCategory._id,
            productBrandImageURL1: 'modules/categories/img/subcategory2/rice.png'
        };

    var productBrand2 = {
        name: 'Jeera Rice',
        productCategory: productCategory._id,
        productBrandImageURL1: 'modules/categories/img/subcategory2/rice.png'
    };

        var productBrand3 = {
            name: 'Sona Rice',
            productCategory: productCategory._id,
            productBrandImageURL1: 'modules/categories/img/subcategory2/rice.png'
        };

        agent.post('/auth/signin')
            .send(credentials)
            .expect(200)
            .end(function(signerr,signinRes) {
                if (signerr) {
                    console.log(signerr.stack);
                    done(signerr);
                }
                else {
                    token = signinRes.body.token;
                    agent.post('/productBrand')
                        .send(productBrand1)
                        .set('token', token)
                        .set('Content-Type', 'application/json')
                        .expect(200)
                        .end(function (subCategoryUpdateErr, productBrandUpdateRes) {
                            // Handle Category update error
                            if (subCategoryUpdateErr) {
                                done(subCategoryUpdateErr);
                            }
                            agent.post('/productBrand')
                                .send(productBrand2)
                                .set('token', token)
                                .set('Content-Type', 'application/json')
                                .expect(200)
                                .end(function (subCategoryUpdateErr, productBrandUpdateRes) {
                                    // Handle Category update error
                                    should.not.exist(subCategoryUpdateErr);
                                    agent.post('/productBrand')
                                        .send(productBrand3)
                                        .set('token', token)
                                        .set('Content-Type', 'application/json')
                                        .expect(200)
                                        .end(function (subCategoryUpdateErr, productBrandUpdateRes) {
                                            // Handle Category update error
                                            should.not.exist(subCategoryUpdateErr);
                                            agent.get('/productBrand')
                                                .set('token',token)
                                                .expect(200)
                                                .end(function(err,res){
                                                    should.not.exist(err);
                                                    console.log(res.body);
                                                    var brands = res.body;
                                                    brands.length.should.equal(3);
                                                    done();
                                                });


                                        });

                                });


                        });
                }
            });
    });



	afterEach(function(done) {
        User.remove().exec();
        Company.remove().exec();
        Contact.remove().exec();
        ProductBrand.remove().exec();

		done();
	});
});
