'use strict';

var should = require('should'),
    request = require('supertest'),
    app = require('../../server'),
    mongoose = require('mongoose'),
    User = mongoose.model('User'),
    Company=mongoose.model('Company'),
    async=require('async'),
    Inventory = mongoose.model('Inventory'),
    agent = request.agent(app),
    commonBatchUserUtil=require('./utils/common.batch.users.utils'),
    commonBatchInventoriesUtil=require('./utils/common.batch.inventories.utils'),
    commonBatchCompanyUtil=require('./utils/common.batch.company.utils'),
    commonBatchConversionInventoryUtil=require('./utils/common.batch.conversion.inventories.utils'),
    commonBatchTrackerInventoryUtil=require('./utils/common.batch.tracker.inventories.utils'),
    commonUtil=require('./utils/common.utils');

/**
 * Globals
 */
var credentials, user, inventory, productCategory1,UOMS1,UOMS2,productBrandsLists=[],allProducts,products,conversionProducts,allInventories,businessUnit;

/**
 * Inventory routes tests
 */
describe('Inventory CRUD tests', function () {
    before(function (done) {
        var regUser = {
            username: 'test@test.com',
            password: 'password',
            ConfirmPassword: 'password',
            firstName: 'First Name',
            lastName: 'Last Name',
            companyName: 'nVipani',
            businessType: 'Trader',
            registrationCategory: 'Manufacturer',
            selectedSegments: ['Coffee'],
            categorySeller: true,
            categoryBuyer: true,
            categoryMediator: true,
            mobile: '0123456789',
            acceptTerms: true
        };

        commonBatchUserUtil.createUser(regUser,agent,function (sendBusinessSegmentPasswordErr, user) {
            if (sendBusinessSegmentPasswordErr) done(sendBusinessSegmentPasswordErr);
            user.status.should.equal('Registered');
            /* index++;*/
            allProducts=require('./data/products');
            products=allProducts.inventories;
            conversionProducts=allProducts.convertInventories;
            commonBatchUserUtil.getUser(commonBatchUserUtil.getSelectedUser('test@test.com')[0],agent,function (signinErr, signinRes) {
                // Handle signin error
                if (signinErr) done(signinErr);
                else {
                    var token = signinRes.body.token;
                    commonBatchCompanyUtil.getUserCompanyDefaultBusinessUnit(token,agent,function (defaultBusinessUnitErr,bunit) {
                        if (defaultBusinessUnitErr) {
                            done(defaultBusinessUnitErr);
                        } else {
                            businessUnit = bunit.body.toString();
                            allProducts.businessUnit = bunit.body.toString();
                            var index=0;
                            commonBatchInventoriesUtil.batchCreate(allProducts, index,token, agent, function (productError) {
                                if (productError) {
                                    done(productError);
                                } else {
                                    products = commonBatchInventoriesUtil.getProducts();
                                    productBrandsLists = commonBatchInventoriesUtil.getProductBrands();
                                    done();
                                }

                            });
                        }
                    });

                }


            });
        });


    });


    beforeEach(function (done) {
        commonBatchUserUtil.getUser(commonBatchUserUtil.getSelectedUser('test@test.com')[0],agent,function (signinErr, signinRes) {
            // Handle signin error
            if (signinErr) done(signinErr);
            else {
                var token = signinRes.body.token;
                // get Inventory lists from user
                commonBatchInventoriesUtil.getInventories(token,businessUnit, agent, function (inventorySaveErr, inventories) {
                    allInventories = inventories.body.inventory;
                    // allInventories = inventories.body.inventory;
                    allInventories.should.be.instanceof(Array);
                    done();
                });
            }
        });
    });

    /**
     * Try to create with existing product Brands
     * case 1:Incorrect data Sent as part of conversion and source inventory
     */
    it('should Not able to save conversion inventory without user token in', function (done) {
        commonBatchUserUtil.getUser(commonBatchUserUtil.getSelectedUser('test@test.com')[0],agent,function (signinErr, signinRes) {
            // Handle signin error
            if (signinErr) done(signinErr);

            var token = signinRes.body.token;

            // get Inventory lists from user
            commonBatchInventoriesUtil.getInventories(null,businessUnit,agent,function (inventorySaveErr, inventories) {
                inventories.body.status.should.equal(false);
                inventories.body.message.should.equal('User is not logged in');
                done();
            });



        });
    });
    /**
     * Try to create with existing product Brands
     * case 1:Incorrect data Sent as part of conversion and source inventory
     */
    it('should be able to create conversion inventory without data if logged in', function (done) {
        commonBatchUserUtil.getUser(commonBatchUserUtil.getSelectedUser('test@test.com')[0],agent,function (signinErr, signinRes) {
            // Handle signin error
            if (signinErr) done(signinErr);

            var token = signinRes.body.token;
            async.forEachSeries(allInventories,function (eachInventory,callback) {
                if(eachInventory._id){
                    agent.post('/inventories/createInventoryConversion')
                        .set('Content-Type', 'application/json')
                        .set('token', token)
                        .expect(400)
                        .end(function (inventorySaveErr, convertInventory) {
                            convertInventory.body.status.should.equal(true);
                            callback();
                        });

                }else{
                    callback();
                }


            },function (err) {
                done(err);
            });


        });
    });


    /**
     * Create  conversion inventory with existing UOM
     */
    /*it('should be able to  Create conversion inventory with existing UOM if logged in', function (done) {
        agent.post('/auth/signin')
            .send(credentials)
            .expect(200)
            .end(function (signinErr, signinRes) {
                // Handle signin error
                if (signinErr) done(signinErr);

                var token = signinRes.body.token;

                // get Inventory lists from user
                agent.get('/inventories')
                    .set('Content-Type', 'application/json')
                    .set('token', token)
                    .expect(200)
                    .end(function (inventorySaveErr, inventories) {
                        var allInventories=inventories.body.inventory;
                        allInventories.should.be.instanceof(Array);
                        /!*async.forEachSeries(allInventories,function (eachInventory,callback) {*!/
                        var eachInventory=allInventories[0];
                           /!* if(eachInventory._id){*!/
                                agent.post('/inventories/createInventoryConversion')
                                    .send({'productBrand':eachInventory.productBrand,'inventory':{'convertStockMasters':[{'currentBalance':'4'}],'MRP':'20','unitOfMeasure':productBrandsLists[0].unitOfMeasures[1]},'inventories':[{'inventory':{'_id':eachInventory._id,'convertStockMasters':[{'_id':eachInventory.stockMasters[0],'currentBalance':2}]}}]})
                                    .set('Content-Type', 'application/json')
                                    .set('token', token)
                                    .expect(200)
                                    .end(function (inventorySaveErr, convertInventory) {
                                        var allInventories = inventories.body;
                                        done();
                                    });

                            /!*}else{
                                callback();
                            }
    *!/

                       /!* },function (err) {
                            done(err);
                        });*!/
                    });



            });
    });*/
    /**
     * Create  conversion inventory with new UOM with the existing brand
     */
    it('should be able to create conversion inventory with single source for the existing brand if logged in', function (done) {
        commonBatchUserUtil.getUser(commonBatchUserUtil.getSelectedUser('test@test.com')[0],agent,function (signinErr, signinRes) {
            // Handle signin error
            if (signinErr) done(signinErr);
            else {
                var token = signinRes.body.token;
                var eachInventory = allInventories[0];
                var conversionInventoryFields = {
                    'productBrand': allInventories[1].productBrand,
                    'inventory': {
                        'businessUnit':businessUnit,
                        'convertStockMasters': [{'batchNumber':'ABC-Test','currentBalance': '4'}],
                        'MRP': '20',
                        'unitOfMeasure': productBrandsLists[1].unitOfMeasures[1]
                    },
                    'inventories': [{
                        'inventory': {
                            '_id': eachInventory._id,
                            'convertStockMasters': [{'_id': eachInventory.stockMasters[0], 'currentBalance': 2}]
                        }
                    }]
                };
                commonBatchConversionInventoryUtil.conversionCreate(conversionInventoryFields, token, agent, function (inventorySaveErr, convertInventoryResult) {
                    should.not.exist(convertInventoryResult.body.message);
                    convertInventoryResult.body.stockMasters.should.be.instanceof(Array).and.have.lengthOf(1);
                    var conversionStockMaster = convertInventoryResult.body.stockMasters[0];
                    var convertInventory = convertInventoryResult.body;
                    commonBatchInventoriesUtil.getInventoryById(eachInventory._id, token, agent, function (inventorySaveErr, sourceInventoryResult) {
                        var sourceInventory = sourceInventoryResult.body;
                        convertInventory.stockMasters.should.be.instanceof(Array).and.have.lengthOf(1);
                        convertInventory.stockMasters[0].stockIn.inventories.should.be.instanceof(Array).and.have.lengthOf(1);
                        convertInventory.stockMasters[0].stockIn.inventories[0].inventory.toString().should.not.equal(convertInventory._id);
                        convertInventory.stockMasters[0].stockIn.inventories[0].inventory.toString().should.be.equal(sourceInventory._id);
                        convertInventory.stockMasters[0].stockIn.inventories[0].stockMasters.should.be.instanceof(Array).and.have.lengthOf(1);
                        convertInventory.stockMasters[0].stockIn.inventories[0].stockMasters[0].stockMaster.toString().should.not.equal(convertInventory.stockMasters[0]._id);
                        convertInventory.stockMasters[0].stockIn.inventories[0].stockMasters[0].stockMaster.toString().should.be.equal(sourceInventory.stockMasters[0]._id);
                        sourceInventory.stockMasters.should.be.instanceof(Array).and.have.lengthOf(1);
                        sourceInventory.stockMasters[0].stockOut.inventories.should.be.instanceof(Array).and.have.lengthOf(1);
                        sourceInventory.stockMasters[0].stockOut.inventories[0].inventory.toString().should.not.equal(sourceInventory._id);
                        sourceInventory.stockMasters[0].stockOut.inventories[0].inventory.toString().should.be.equal(convertInventory._id);
                        sourceInventory.stockMasters[0].stockOut.inventories[0].stockMasters.should.be.instanceof(Array).and.have.lengthOf(1);
                        sourceInventory.stockMasters[0].stockOut.inventories[0].stockMasters[0].stockMaster.toString().should.not.equal(sourceInventory.stockMasters[0]._id);
                        sourceInventory.stockMasters[0].stockOut.inventories[0].stockMasters[0].stockMaster.toString().should.be.equal(convertInventory.stockMasters[0]._id);
                        convertInventory.stockMasters[0].stockIn.inventories[0].stockMasters[0].count.should.not.equal(convertInventory.stockMasters[0].currentBalance);
                        convertInventory.stockMasters[0].stockIn.inventories[0].stockMasters[0].count.should.be.equal(sourceInventory.stockMasters[0].stockOut.inventories[0].stockMasters[0].count);
                        done();
                    });
                });



            }

        });
    });
    it('Add stock to source inventory and use the two stocks for the new conversion inventory',function (done) {
        commonBatchUserUtil.getUser(commonBatchUserUtil.getSelectedUser('test@test.com')[0],agent,function (signinErr, signinRes) {
            // Handle signin error
            if (signinErr) done(signinErr);
            else {
                var token = signinRes.body.token;
                (allInventories.length).should.be.above(0);

                allInventories[0]._id.should.not.be.instanceof(undefined);
                (allInventories[0].stockMasters.length).should.be.above(0);
                agent.post('/stockMaster')
                    .send({
                        inventory: allInventories[0]._id,
                        currentBalance: 4,
                        BatchNumber: 'Batch 10kg Rice  2',
                        MRP: allInventories[0].MRP
                    })
                    .set('Content-Type', 'application/json')
                    .set('token', token)
                    .expect(200)
                    .end(function (inventorySaveErr, sourceInventoryWithStockMasterResult) {
                        agent.get('/inventories/' + sourceInventoryWithStockMasterResult.body.inventory)
                            .set('Content-Type', 'application/json')
                            .set('token', token)
                            .expect(200)
                            .end(function (inventorySaveErr, sourceInventoryResult) {
                                sourceInventoryResult.body._id.should.not.be.instanceof(undefined);
                                var eachInventory = sourceInventoryResult.body;
                                var conversionInventoryFields = {
                                    'productBrand': productBrandsLists[2]._id,
                                    'inventory': {
                                        'businessUnit':businessUnit,
                                        'convertStockMasters': [{'batchNumber':'ABC-2-Test','currentBalance': '4'}],
                                        'MRP': '20',
                                        'unitOfMeasure': productBrandsLists[2].unitOfMeasures[0]
                                    },
                                    'inventories': [{
                                        'inventory': {
                                            '_id': eachInventory._id,
                                            'convertStockMasters': [{
                                                '_id': eachInventory.stockMasters[0]._id,
                                                'currentBalance': 1
                                            }, {'_id': eachInventory.stockMasters[1]._id, 'currentBalance': 2}]
                                        }
                                    }]
                                };
                                agent.post('/inventories/createInventoryConversion')
                                    .send(conversionInventoryFields)
                                    .set('Content-Type', 'application/json')
                                    .set('token', token)
                                    .expect(200)
                                    .end(function (inventorySaveErr, convertInventoryResult) {
                                        should.not.exist(convertInventoryResult.body.message);
                                        convertInventoryResult.body.stockMasters.should.be.instanceof(Array).and.have.lengthOf(1);
                                        var conversionStockMaster = convertInventoryResult.body.stockMasters[0];
                                        var convertInventory = convertInventoryResult.body;
                                        agent.get('/inventories/' + eachInventory._id)
                                            .set('Content-Type', 'application/json')
                                            .set('token', token)
                                            .expect(200)
                                            .end(function (inventorySaveErr, sourceInventoryResult) {
                                                var sourceInventory = sourceInventoryResult.body;
                                                convertInventory.stockMasters.should.be.instanceof(Array).and.have.lengthOf(1);
                                                convertInventory.stockMasters[0].stockIn.inventories.should.be.instanceof(Array).and.have.lengthOf(1);
                                                convertInventory.stockMasters[0].stockIn.inventories[0].inventory.toString().should.not.equal(convertInventory._id);
                                                convertInventory.stockMasters[0].stockIn.inventories[0].inventory.toString().should.be.equal(sourceInventory._id);
                                                convertInventory.stockMasters[0].stockIn.inventories[0].stockMasters.should.be.instanceof(Array).and.have.lengthOf(2);
                                                convertInventory.stockMasters[0].stockIn.inventories[0].stockMasters[0].stockMaster.toString().should.not.equal(convertInventory.stockMasters[0]._id);
                                                convertInventory.stockMasters[0].stockIn.inventories[0].stockMasters[0].stockMaster.toString().should.be.equal(sourceInventory.stockMasters[0]._id);
                                                sourceInventory.stockMasters.should.be.instanceof(Array).and.have.lengthOf(2);
                                                sourceInventory.stockMasters[0].stockOut.inventories.should.be.instanceof(Array).and.have.lengthOf(1);
                                                sourceInventory.stockMasters[0].stockOut.inventories[0].inventory.toString().should.not.equal(sourceInventory._id);
                                                sourceInventory.stockMasters[0].stockOut.inventories[0].inventory.toString().should.be.equal(convertInventory._id);
                                                sourceInventory.stockMasters[0].stockOut.inventories[0].stockMasters.should.be.instanceof(Array).and.have.lengthOf(1);
                                                sourceInventory.stockMasters[0].stockOut.inventories[0].stockMasters[0].stockMaster.toString().should.not.equal(sourceInventory.stockMasters[0]._id);
                                                sourceInventory.stockMasters[0].stockOut.inventories[0].stockMasters[0].stockMaster.toString().should.be.equal(convertInventory.stockMasters[0]._id);
                                                convertInventory.stockMasters[0].stockIn.inventories[0].stockMasters[0].count.should.not.equal(convertInventory.stockMasters[0].currentBalance);
                                                convertInventory.stockMasters[0].stockIn.inventories[0].stockMasters[0].count.should.be.equal(sourceInventory.stockMasters[0].stockOut.inventories[0].stockMasters[0].count);
                                                sourceInventory.stockMasters[0].stockOut.inventories.should.be.instanceof(Array).and.have.lengthOf(1);
                                                sourceInventory.stockMasters[1].stockOut.inventories[0].stockMasters[0].stockMaster.toString().should.not.equal(sourceInventory.stockMasters[1]._id);
                                                sourceInventory.stockMasters[1].stockOut.inventories[0].stockMasters[0].stockMaster.toString().should.be.equal(convertInventory.stockMasters[0]._id);
                                                convertInventory.stockMasters[0].stockIn.inventories[0].stockMasters[0].count.should.not.equal(convertInventory.stockMasters[0].currentBalance);
                                                convertInventory.stockMasters[0].stockIn.inventories[0].stockMasters[1].count.should.be.equal(sourceInventory.stockMasters[1].stockOut.inventories[0].stockMasters[0].count);
                                                done();
                                            });
                                    });
                            });

                    });

            }

        });
    });


    it('Add stock to source inventory  and use the multiple inventories for the new conversion inventory with less than current stock',function (done) {
        commonBatchUserUtil.getUser(commonBatchUserUtil.getSelectedUser('test@test.com')[0],agent,function (signinErr, signinRes) {
            // Handle signin error
            if (signinErr) done(signinErr);
            else {
                var token = signinRes.body.token;
                // Handle signin error
                if (signinErr) done(signinErr);

                token = signinRes.body.token;
                (allInventories.length).should.be.above(2);

                allInventories[3]._id.should.not.be.instanceof(undefined);
                (allInventories[3].stockMasters.length).should.be.above(0);
                agent.post('/stockMaster')
                    .send({
                        inventory: allInventories[3]._id,
                        currentBalance: 4,
                        batchNumber: 'Batch 10kg Rice  2',
                        MRP: allInventories[3].MRP
                    })
                    .set('Content-Type', 'application/json')
                    .set('token', token)
                    .expect(200)
                    .end(function (inventorySaveErr, sourceInventoryWithStockMasterResult) {


                        commonBatchInventoriesUtil.getInventoryById(sourceInventoryWithStockMasterResult.body.inventory, token, agent, function (inventorySaveErr, sourceInventoryResult) {
                            sourceInventoryResult.body._id.should.not.be.instanceof(undefined);
                            var eachInventory = sourceInventoryResult.body;
                            var conversionInventoryFields = {
                                'productBrand': productBrandsLists[2]._id,
                                'inventory': {
                                    'businessUnit':businessUnit,
                                    'convertStockMasters': [{'currentBalance': '4'}],
                                    'MRP': '20',
                                    'unitOfMeasure': productBrandsLists[2].unitOfMeasures[1]
                                },
                                'inventories': [{
                                    'inventory': {
                                        '_id': eachInventory._id,
                                        'convertStockMasters': [{
                                            '_id': eachInventory.stockMasters[0]._id,
                                            'currentBalance': 25
                                        }, {'_id': eachInventory.stockMasters[1]._id, 'currentBalance': 3}]
                                    }
                                }, {
                                    'inventory': {
                                        '_id': allInventories[2]._id,
                                        'convertStockMasters': [{
                                            '_id': allInventories[2].stockMasters[0],
                                            'currentBalance': 2
                                        }]
                                    }
                                }]
                            };
                            commonBatchConversionInventoryUtil.conversionCreate(conversionInventoryFields,token,agent,function (inventorySaveErr, convertInventoryResult) {
                                convertInventoryResult.body.message.should.equal('No Sufficient Stock at source inventory data');
                                done();
                            });
                        });

                    });


            }
        });
    });
    /**
     *
     */
    it('Add stock to source inventory  and use the multiple inventories for the new conversion inventory',function (done) {
        commonBatchUserUtil.getUser(commonBatchUserUtil.getSelectedUser('test@test.com')[0],agent,function (signinErr, signinRes) {
            // Handle signin error
            if (signinErr) done(signinErr);
            else {
                var token = signinRes.body.token;
                (allInventories.length).should.be.above(2);

                allInventories[3]._id.should.not.be.instanceof(undefined);

                commonBatchInventoriesUtil.getInventoryById(allInventories[3]._id, token, agent, function (inventorySaveErr, sourceInventoryResult) {
                    sourceInventoryResult.body._id.should.not.be.instanceof(undefined);
                    var eachInventory = sourceInventoryResult.body;
                    var conversionInventoryFields = {
                        'productBrand': productBrandsLists[2]._id,
                        'inventory': {
                            'businessUnit':businessUnit,
                            'convertStockMasters': [{'batchNumber':'ABC-3-Test','currentBalance': '4'}],
                            'MRP': '20',
                            'unitOfMeasure': productBrandsLists[2].unitOfMeasures[1]
                        },
                        'inventories': [{
                            'inventory': {
                                '_id': eachInventory._id,
                                'convertStockMasters': [{
                                    '_id': eachInventory.stockMasters[0]._id,
                                    'currentBalance': 2
                                }, {'_id': eachInventory.stockMasters[1]._id, 'currentBalance': 3}]
                            }
                        }, {
                            'inventory': {
                                '_id': allInventories[2]._id,
                                'convertStockMasters': [{'_id': allInventories[2].stockMasters[0], 'currentBalance': 2}]
                            }
                        }]
                    };
                    commonBatchConversionInventoryUtil.conversionCreate(conversionInventoryFields, token, agent, function (inventorySaveErr, convertInventoryResult) {
                        convertInventoryResult.body.stockMasters.should.be.instanceof(Array).and.have.lengthOf(1);
                        var conversionStockMaster = convertInventoryResult.body.stockMasters[0];
                        var convertInventory = convertInventoryResult.body;

                        commonBatchInventoriesUtil.getInventoryById(eachInventory._id, token, agent, function (inventorySaveErr, sourceInventoryResult) {
                            var sourceInventory = sourceInventoryResult.body;
                            convertInventory.stockMasters.should.be.instanceof(Array).and.have.lengthOf(1);
                            convertInventory.stockMasters[0].stockIn.inventories.should.be.instanceof(Array).and.have.lengthOf(2);
                            convertInventory.stockMasters[0].stockIn.inventories[0].inventory.toString().should.not.equal(convertInventory._id);
                            convertInventory.stockMasters[0].stockIn.inventories[0].inventory.toString().should.be.equal(sourceInventory._id);
                            convertInventory.stockMasters[0].stockIn.inventories[0].stockMasters.should.be.instanceof(Array).and.have.lengthOf(2);
                            convertInventory.stockMasters[0].stockIn.inventories[0].stockMasters[0].stockMaster.toString().should.not.equal(convertInventory.stockMasters[0]._id);
                            convertInventory.stockMasters[0].stockIn.inventories[0].stockMasters[0].stockMaster.toString().should.be.equal(sourceInventory.stockMasters[0]._id);
                            sourceInventory.stockMasters.should.be.instanceof(Array).and.have.lengthOf(2);
                            sourceInventory.stockMasters[0].stockOut.inventories.should.be.instanceof(Array).and.have.lengthOf(1);
                            sourceInventory.stockMasters[0].stockOut.inventories[0].inventory.toString().should.not.equal(sourceInventory._id);
                            sourceInventory.stockMasters[0].stockOut.inventories[0].inventory.toString().should.be.equal(convertInventory._id);
                            sourceInventory.stockMasters[0].stockOut.inventories[0].stockMasters.should.be.instanceof(Array).and.have.lengthOf(1);
                            sourceInventory.stockMasters[0].stockOut.inventories[0].stockMasters[0].stockMaster.toString().should.not.equal(sourceInventory.stockMasters[0]._id);
                            sourceInventory.stockMasters[0].stockOut.inventories[0].stockMasters[0].stockMaster.toString().should.be.equal(convertInventory.stockMasters[0]._id);
                            convertInventory.stockMasters[0].stockIn.inventories[0].stockMasters[0].count.should.not.equal(convertInventory.stockMasters[0].currentBalance);
                            convertInventory.stockMasters[0].stockIn.inventories[0].stockMasters[0].count.should.be.equal(sourceInventory.stockMasters[0].stockOut.inventories[0].stockMasters[0].count);
                            sourceInventory.stockMasters[0].stockOut.inventories.should.be.instanceof(Array).and.have.lengthOf(1);
                            sourceInventory.stockMasters[1].stockOut.inventories[0].stockMasters[0].stockMaster.toString().should.not.equal(sourceInventory.stockMasters[1]._id);
                            sourceInventory.stockMasters[1].stockOut.inventories[0].stockMasters[0].stockMaster.toString().should.be.equal(convertInventory.stockMasters[0]._id);
                            convertInventory.stockMasters[0].stockIn.inventories[0].stockMasters[0].count.should.not.equal(convertInventory.stockMasters[0].currentBalance);
                            convertInventory.stockMasters[0].stockIn.inventories[1].inventory.should.be.equal(allInventories[2]._id);
                            convertInventory.stockMasters[0].stockIn.inventories[0].stockMasters[1].count.should.be.equal(sourceInventory.stockMasters[1].stockOut.inventories[0].stockMasters[0].count);
                            //done();

                            /*  agent.get('/trackPaths?stockMasterTrackId=' + convertInventory.stockMasters[0]._id + '&inventoryTrackId=' + convertInventory._id)
                                  .set('Content-Type', 'application/json')
                                  .set('token', token)
                                  .expect(200)
                                  .end(function (inventorySaveErr, convertInventoryResultValue) {
                                      /!*convertInventoryResultValue.body.should.exist;*!/
                                      done();
                                  });*/

                            commonBatchTrackerInventoryUtil.getTracker({inventoryId:convertInventory._id,stockMasterId:convertInventory.stockMasters[0]._id},token,agent,function (trackFetchErr, trackResults) {
                                done();
                            });
                        });
                    });
                });
            }
        });
    });
    /**
     * Add Stock to an existing inventory and product brand with search by product brand and uom and then add then add the stock Masters
     */

    it('Add Stock to an existing inventory and product brand inventories for the new conversion inventory with source inventories empty without business unit',function (done) {
        commonBatchUserUtil.getUser(commonBatchUserUtil.getSelectedUser('test@test.com')[0],agent,function (signinErr, signinRes) {
            // Handle signin error
            if (signinErr) done(signinErr);
            else {
                var token = signinRes.body.token;
                var fetch = [{_id: allInventories[0]._id}, {'_id': allInventories[2]._id}];
                commonBatchInventoriesUtil.fetchInventoriesByFilter(fetch,businessUnit,token,agent, function(inventorySaveErr, sourceInventoryResult) {
                    should.not.exist(sourceInventoryResult.body.message);
                    var sourceInventory = sourceInventoryResult.body.inventories;
                    fetch = [{productBrand: allInventories[4].productBrand}];
                    commonBatchInventoriesUtil.fetchInventoriesByFilter(fetch,businessUnit,token,agent, function (inventorySaveErr, convertInventoryResult) {
                        should.not.exist(convertInventoryResult.body.message);
                        var convertInventory = convertInventoryResult.body.inventories[0];

                        var conversionInventoryFields = {
                            'productBrand': convertInventory.productBrand,
                            'inventory': {
                                'businessUnit':businessUnit,
                                _id: convertInventory._id,
                                'convertStockMasters': [{
                                    _id: convertInventory.stockMasters[0]._id,
                                    'currentBalance': '4'
                                }],
                                'MRP': '20'
                            },
                            'inventories': []
                        };
                        commonBatchConversionInventoryUtil.conversionCreate(conversionInventoryFields, token, agent, function (inventorySaveErr, convertInventoryResult) {
                            /* agent.post('/inventories/createInventoryConversion')
                                 .send(conversionInventoryFields)
                                 .set('Content-Type', 'application/json')
                                 .set('token', token)
                                 .expect(200)
                                 .end(function (inventorySaveErr, convertInventoryResult) {*/
                            var convertInventory = convertInventoryResult.body;
                            convertInventory.message.should.be.equal('No Source inventory at the conversion Data');
                            /*convertInventory.stockMasters.should.be.instanceof(Array).and.have.lengthOf(2);
                            convertInventory.stockMasters[0].stockIn.inventories.should.be.instanceof(Array).and.have.lengthOf(3);
                            convertInventory.stockMasters[0].stockIn.inventories[0].inventory.toString().should.not.equal(convertInventory._id);
                           */
                            done();
                        });
                    });

                });
            }
        });
    });
    it('Add Stock to an existing inventory and product brand inventories for the new conversion inventory with source inventories empty with business unit',function (done) {
        commonBatchUserUtil.getUser(commonBatchUserUtil.getSelectedUser('test@test.com')[0],agent,function (signinErr, signinRes) {
            // Handle signin error
            if (signinErr) done(signinErr);
            else {
                var token = signinRes.body.token;
                var fetch = [{_id: allInventories[0]._id}, {'_id': allInventories[2]._id}];
                commonBatchInventoriesUtil.fetchInventoriesByFilter(fetch,businessUnit,token,agent, function(inventorySaveErr, sourceInventoryResult) {
                    should.not.exist(sourceInventoryResult.body.message);
                    var sourceInventory = sourceInventoryResult.body.inventories;
                    fetch = [{productBrand: allInventories[4].productBrand}];
                    commonBatchInventoriesUtil.fetchInventoriesByFilter(fetch,businessUnit,token,agent, function (inventorySaveErr, convertInventoryResult) {
                        should.not.exist(convertInventoryResult.body.message);
                        var convertInventory = convertInventoryResult.body.inventories[0];

                        var conversionInventoryFields = {
                            'productBrand': convertInventory.productBrand,
                            'inventory': {
                                _id: convertInventory._id,
                                'convertStockMasters': [{
                                    _id: convertInventory.stockMasters[0]._id,
                                    'currentBalance': '4'
                                }],
                                'MRP': '20'
                            },
                            'inventories': []
                        };
                        commonBatchConversionInventoryUtil.conversionCreate(conversionInventoryFields, token, agent, function (inventorySaveErr, convertInventoryResult) {
                            /* agent.post('/inventories/createInventoryConversion')
                                 .send(conversionInventoryFields)
                                 .set('Content-Type', 'application/json')
                                 .set('token', token)
                                 .expect(200)
                                 .end(function (inventorySaveErr, convertInventoryResult) {*/
                            var convertInventory = convertInventoryResult.body;
                            convertInventory.message.should.be.equal('No Source inventory at the conversion Data');
                            /*convertInventory.stockMasters.should.be.instanceof(Array).and.have.lengthOf(2);
                            convertInventory.stockMasters[0].stockIn.inventories.should.be.instanceof(Array).and.have.lengthOf(3);
                            convertInventory.stockMasters[0].stockIn.inventories[0].inventory.toString().should.not.equal(convertInventory._id);
                           */
                            done();
                        });
                    });

                });
            }
        });
    });
    it('Add Stock to an existing inventory and product brand inventories for the with same source and  conversion inventory',function (done) {
        commonBatchUserUtil.getUser(commonBatchUserUtil.getSelectedUser('test@test.com')[0],agent,function (signinErr, signinRes) {
            // Handle signin error
            if (signinErr) done(signinErr);
            else {
                var token = signinRes.body.token;
                var fetch = [{_id: allInventories[0]._id}, {'_id': allInventories[2]._id}];
                commonBatchInventoriesUtil.fetchInventoriesByFilter(fetch,businessUnit,token,agent, function(inventorySaveErr, sourceInventoryResult) {
                    should.not.exist(sourceInventoryResult.body.message);
                    var sourceInventory = sourceInventoryResult.body.inventories;
                    fetch = [{productBrand: allInventories[4].productBrand}];
                    commonBatchInventoriesUtil.fetchInventoriesByFilter(fetch,businessUnit,token,agent, function (inventorySaveErr, convertInventoryResult) {
                        should.not.exist(convertInventoryResult.body.message);
                        var oldConvertInventory=convertInventoryResult.body.inventories[0];
                        var conversionInventoryFields = {
                            'productBrand': oldConvertInventory.productBrand,
                            'inventory': {
                                _id: oldConvertInventory._id,
                                'convertStockMasters': [{
                                    _id: oldConvertInventory.stockMasters[0]._id,
                                    'currentBalance': '4'
                                }],
                                'MRP': '20'
                            },
                            'inventories': [{
                                'inventory': {
                                    '_id': sourceInventory[0]._id,
                                    'convertStockMasters': [{
                                        '_id': sourceInventory[0].stockMasters[0]._id,
                                        'currentBalance': 2
                                    }]
                                }
                            }, {
                                'inventory': {
                                    '_id': sourceInventory[1]._id,
                                    'convertStockMasters': [{
                                        '_id': sourceInventory[1].stockMasters[0]._id,
                                        'currentBalance': 2
                                    }]
                                }
                            }]
                        };
                        commonBatchConversionInventoryUtil.conversionCreate(conversionInventoryFields, token, agent, function (convertInventorySaveErr, convertInventoryResultValue) {

                            convertInventoryResultValue.body.message.should.be.equal('Conversion is  possible with in same business unit . Place order between business Units ');
                            done();
                        });
                    });

                });
            }
        });
    });
    it('Add Stock to an existing inventory and product brand inventories for the new conversion inventory',function (done) {
        commonBatchUserUtil.getUser(commonBatchUserUtil.getSelectedUser('test@test.com')[0],agent,function (signinErr, signinRes) {
            // Handle signin error
            if (signinErr) done(signinErr);
            else {
                var token = signinRes.body.token;
                var fetch = [{_id: allInventories[0]._id}, {'_id': allInventories[2]._id}];
                agent.post('/inventories/matchedinventories')
                    .send({inventories: fetch})
                    .set('Content-Type', 'application/json')
                    .set('token', token)
                    .expect(200)
                    .end(function (inventorySaveErr, sourceInventoryResult) {
                        var sourceInventory = sourceInventoryResult.body.inventories;
                        fetch = [{productBrand: allInventories[4].productBrand}];
                        agent.post('/inventories/matchedinventories')
                            .send({inventories: fetch,businessUnit:businessUnit})
                            .set('Content-Type', 'application/json')
                            .set('token', token)
                            .expect(200)
                            .end(function (inventorySaveErr, convertInventoryResult) {
                                should.not.exist(convertInventoryResult.body.message);
                                var oldConvertInventory = convertInventoryResult.body.inventories[1];
                                var conversionInventoryFields = {
                                    'productBrand': oldConvertInventory.productBrand,
                                    'inventory': {
                                        '_id': oldConvertInventory._id,
                                        'businessUnit':businessUnit,
                                        'convertStockMasters': [{
                                            '_id': oldConvertInventory.stockMasters[0]._id,
                                            'currentBalance': '4'
                                        }],
                                        'MRP': '20'
                                    },
                                    'inventories': [{
                                        'inventory': {
                                            '_id': sourceInventory[0]._id,
                                            'convertStockMasters': [{
                                                '_id': sourceInventory[0].stockMasters[0]._id,
                                                'currentBalance': 2
                                            }]
                                        }
                                    }, {
                                        'inventory': {
                                            '_id': sourceInventory[1]._id,
                                            'convertStockMasters': [{
                                                '_id': sourceInventory[1].stockMasters[0]._id,
                                                'currentBalance': 2
                                            }]
                                        }
                                    }]
                                };
                                commonBatchConversionInventoryUtil.conversionCreate(conversionInventoryFields, token, agent, function (inventorySaveErr, convertInventoryResultValue) {

                                    var convertInventory = convertInventoryResultValue.body;
                                    convertInventory.message.should.be.equal('Conversion is done from the same inventory with same batch Number. Try with the new Batch');
                                    commonBatchTrackerInventoryUtil.getTracker({inventoryId:sourceInventory[0]._id,stockMasterId:sourceInventory[0].stockMasters[0]._id},token,agent,function (trackFetchErr, trackResults) {
                                        done();
                                    });

                                });
                            });

                    });
            }
        });
    });
    it('Stock In update to an existing convert inventory with existing stock master',function (done) {
        commonBatchUserUtil.getUser(commonBatchUserUtil.getSelectedUser('test@test.com')[0],agent,function (signinErr, signinRes) {
            // Handle signin error
            if (signinErr) done(signinErr);
            else {
                var token = signinRes.body.token;
                var fetch = [{'_id': allInventories[0]._id}, {'_id': allInventories[2]._id}];
                commonBatchInventoriesUtil.fetchInventoriesByFilter(fetch,businessUnit,token,agent, function (inventorySaveErr, sourceInventoryResult) {
                    var sourceInventory = sourceInventoryResult.body.inventories;
                    fetch = [{productBrand: allInventories[1].productBrand}];
                    commonBatchInventoriesUtil.fetchInventoriesByFilter(fetch,businessUnit,token,agent, function (inventorySaveErr, convertInventoryResult) {
                        should.not.exist(convertInventoryResult.body.message);
                        var oldConvertInventory = convertInventoryResult.body.inventories[1];
                        var conversionInventoryFields = {
                            'productBrand': oldConvertInventory.productBrand,
                            'inventory': {
                                '_id': oldConvertInventory._id,
                                'businessUnit':businessUnit,
                                'convertStockMasters': [{
                                    '_id': oldConvertInventory.stockMasters[0]._id,
                                    'currentBalance': '4'
                                }],
                                'MRP': '20'
                            },
                            'inventories': [{
                                'inventory': {
                                    '_id': sourceInventory[0]._id,
                                    'convertStockMasters': [{
                                        '_id': sourceInventory[0].stockMasters[0]._id,
                                        'currentBalance': 2
                                    }]
                                }
                            }]
                        };
                        commonBatchConversionInventoryUtil.conversionCreate(conversionInventoryFields, token, agent, function (inventorySaveErr, convertInventoryResultValue) {

                            var convertInventory = convertInventoryResultValue.body;
                            should.not.exist(convertInventory.message);
                            var conversionInventoryFields = {
                                'productBrand': oldConvertInventory.productBrand,
                                'inventory': {
                                    '_id': oldConvertInventory._id,
                                    'businessUnit':businessUnit,
                                    'convertStockMasters': [{
                                        '_id': oldConvertInventory.stockMasters[0]._id,
                                        'currentBalance': '8'
                                    }],
                                    'MRP': '20'
                                },
                                'inventories': [{
                                    'inventory': {
                                        '_id': sourceInventory[0]._id,
                                        'convertStockMasters': [{
                                            '_id': sourceInventory[0].stockMasters[0]._id,
                                            'currentBalance': 2
                                        }]
                                    }
                                }]
                            };
                            commonBatchConversionInventoryUtil.conversionCreate(conversionInventoryFields, token, agent, function (inventorySaveErr, convertInventorySecondTimeValue) {
                                should.not.exist(convertInventorySecondTimeValue.body.message);
                                convertInventory.stockMasters[0].stockIn.inventories[1].stockMasters[0].count.should.be.equal(2);
                                convertInventorySecondTimeValue.body.stockMasters[0].currentBalance.should.be.equal(oldConvertInventory.stockMasters[0].currentBalance+8+4);
                                convertInventorySecondTimeValue.body.stockMasters[0].stockIn.inventories[1].stockMasters[0].count.should.not.be.equal(2);
                                convertInventorySecondTimeValue.body.stockMasters[0].stockIn.inventories[1].stockMasters[0].count.should.be.equal(4);
                                done();
                            });

                        });
                    });

                });
            }
        });
    });

    it('New Stock record for the converted inventory',function (done) {
        commonBatchUserUtil.getUser(commonBatchUserUtil.getSelectedUser('test@test.com')[0],agent,function (signinErr, signinRes) {
            // Handle signin error
            if (signinErr) done(signinErr);
            else {
                var token = signinRes.body.token;
                var fetch = [{'_id': allInventories[2]._id}];
                commonBatchInventoriesUtil.fetchInventoriesByFilter(fetch,businessUnit,token,agent, function (inventorySaveErr, sourceInventoryResult) {
                    var sourceInventory = sourceInventoryResult.body.inventories;
                    fetch = [{productBrand: allInventories[1].productBrand}];
                    commonBatchInventoriesUtil.fetchInventoriesByFilter(fetch,businessUnit,token,agent, function (inventorySaveErr, convertInventoryResult) {
                        should.not.exist(convertInventoryResult.body.message);
                        var oldConvertInventory = convertInventoryResult.body.inventories[0];
                        var conversionInventoryFields = {
                            'productBrand': oldConvertInventory.productBrand,
                            'inventory': {
                                '_id': oldConvertInventory._id,
                                'businessUnit':businessUnit,
                                'convertStockMasters': [{
                                    'batchNumber':'ABC12',
                                    'currentBalance': '4'
                                }],
                                'MRP': '20'
                            },
                            'inventories': [{
                                'inventory': {
                                    '_id': sourceInventory[0]._id,
                                    'convertStockMasters': [{
                                        '_id': sourceInventory[0].stockMasters[0]._id,
                                        'currentBalance': 2
                                    }]
                                }
                            }]
                        };
                        commonBatchConversionInventoryUtil.conversionCreate(conversionInventoryFields, token, agent, function (inventorySaveErr, convertInventoryResultValue) {

                            var convertInventory = convertInventoryResultValue.body;

                            should.not.exist(convertInventory.message);
                            convertInventory.stockMasters.length.should.not.be.equal(oldConvertInventory.stockMasters.length);
                            convertInventory.stockMasters[1].stockIn.inventories[0].inventory.should.be.equal(sourceInventory[0]._id);

                            convertInventory.stockMasters[1].stockIn.inventories[0].stockMasters[0].count.should.be.equal(2);
                            convertInventory.stockMasters.length.should.be.equal(oldConvertInventory.stockMasters.length+1);
                            convertInventory.stockMasters[oldConvertInventory.stockMasters.length].stockIn.inventories.length.should.not.equal(0);
                            done();

                        });
                    });

                });
            }
        });
    });
    afterEach(function (done) {
        done();
    });

    after(function (done) {
        done();
    });
});
