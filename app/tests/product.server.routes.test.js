'use strict';

var should = require('should'),
	request = require('supertest'),
	app = require('../../server'),
	mongoose = require('mongoose'),
	User = mongoose.model('User'),
	Inventory = mongoose.model('Inventory'),
	Contact = mongoose.model('Contact'),
	Company = mongoose.model('Company'),
	Category = mongoose.model('Category'),
	Product = mongoose.model('Product'),
	Notification = mongoose.model('Notification'),
	Product = mongoose.model('Product'),
	agent = request.agent(app);

/**
 * Globals
 */
var credentials, user, product;

/**
 * Product routes tests
 */
describe('Product CRUD tests', function() {
	before(function (done) {
		var regUser = {
			username: 'test@test.com',
			password: 'password',
			ConfirmPassword: 'password',
			firstName: 'First Name',
			lastName: 'Last Name',
			companyName: 'nVipani',
			businessType: 'Trader',
			categorySeller: true,
			categoryBuyer: true,
			categoryMediator: true,
			mobile: '0123456789',
			acceptTerms: true
		};
		agent.post('/user/presignup')
			.send(regUser)
			.expect(200)
			.end(function (presignupErr, presignupRes) {
				//console.log("Presignup Response - "+JSON.stringify(presignupRes.message));
				if (presignupErr) done(presignupErr);

				User.findOne({
					username: credentials.username
				}, '-salt -password', function (err, resUser) {
					if (err) {
						done(err);
					}
					user = resUser;
					if (resUser) {
						agent.get('/user/register/' + resUser.statusToken)
							.expect(200)
							.end(function (validationGetErr, validationGetRes) {
								//console.log("Presignup Response - "+JSON.stringify(presignupRes.message));
								if (validationGetErr) done(validationGetErr);
								done();
							});
					}
				});
			});

		credentials = {
			username: 'test@test.com',
			password: 'password'
		};
	});

	beforeEach(function (done) {
		agent.post('/auth/signin')
			.send(credentials)
			.expect(200)
			.end(function (signinErr, signinRes) {
				// Handle signin error
				if (signinErr) done(signinErr);

				var token = signinRes.body.token;

				var mainCategory = {
					name: 'Agriculture',
					code: 'AGR',
					type: 'MainCategory'
				};

				// Save a new Category
				//category.token = token;
				agent.post('/categories')
					.send(mainCategory)
					.set('Content-Type', 'application/json')
					.set('token', token)
					.expect(200)
					.end(function (categorySaveErr, categorySaveRes) {
						// Handle Category save error
						if (categorySaveErr) done(categorySaveErr);

						mainCategory = categorySaveRes.body;

						var subCategory1 = {
							name: 'Vegetables',
							code: 'VEG',
							type: 'SubCategory1',
							categoryImageURL1: 'modules/categories/img/subcategory1/vegetables.png'
						};

						// Save a new Category
						//category.token = token;
						agent.post('/categories')
							.send(subCategory1)
							.set('Content-Type', 'application/json')
							.set('token', token)
							.expect(200)
							.end(function (subCategorySaveErr, subCategorySaveRes) {
								// Handle Category save error
								if (subCategorySaveErr) done(subCategorySaveErr);

								var subCategory1 = subCategorySaveRes.body;

								mainCategory.children.push(subCategory1);

								// Update existing Category
								agent.put('/categories/' + mainCategory._id)
									.send(mainCategory)
									.set('Content-Type', 'application/json')
									.set('token', token)
									.expect(200)
									.end(function (categoryUpdateErr, categoryUpdateRes) {
										// Handle Category update error
										if (categoryUpdateErr) done(categoryUpdateErr);

										var subCategory2 = {
											name: 'Lemon',
											code: 'LEM',
											type: 'SubCategory2',
											categoryImageURL1: 'modules/categories/img/subcategory2/lemon.png'
										};

										// Save a new Category
										//category.token = token;
										agent.post('/categories')
											.send(subCategory2)
											.set('Content-Type', 'application/json')
											.set('token', token)
											.expect(200)
											.end(function (subCategory2SaveErr, subCategory2SaveRes) {
												// Handle Category save error
												if (subCategory2SaveErr) done(subCategory2SaveErr);

												subCategory2 = subCategory2SaveRes.body;

												subCategory1.children.push(subCategory2);

												// Update existing Category
												agent.put('/categories/' + subCategory1._id)
													.send(subCategory1)
													.set('Content-Type', 'application/json')
													.set('token', token)
													.expect(200)
													.end(function (subCategoryUpdateErr, subCategoryUpdateRes) {
														// Handle Category update error
														if (subCategoryUpdateErr) done(subCategoryUpdateErr);

														product = {
															name: 'Acid Lemon',
															description: 'Naturally farmed gudur Acid Lemon',
															category: mainCategory._id,
															subCategory1: subCategory1._id,
															subCategory2: subCategory2._id,
															keywords: [{
																attributeKey: 'FarmingType',
																attributeValue: 'Natural Farming'
															}]
														};

														done();
													});
											});
									});
							});
					});
			});
	});

	it('should be able to save Product instance if logged in', function(done) {
		agent.post('/auth/signin')
			.send(credentials)
			.expect(200)
			.end(function(signinErr, signinRes) {
				// Handle signin error
				if (signinErr) done(signinErr);

				var token = signinRes.body.token;

				// Save a new Product
				agent.post('/products')
					.send(product)
					.set('Content-Type', 'application/json')
					.set('token', token)
					.expect(200)
					.end(function(productSaveErr, productSaveRes) {
						// Handle Product save error
						if (productSaveErr) done(productSaveErr);

						// Get a list of Products
						agent.get('/products')
							.set('Content-Type', 'application/json')
							.set('token', token)
							.end(function(productsGetErr, productsGetRes) {
								// Handle Product save error
								if (productsGetErr) done(productsGetErr);

								// Get Products list
								var products = productsGetRes.body;

								// Set assertions
								(products[0].user._id).should.equal(user.id);
								(products[0].name).should.match('Acid Lemon');
								(products[0].category._id).should.match(product.category);
								(products[0].subCategory1._id).should.match(product.subCategory1);
								(products[0].subCategory2._id).should.match(product.subCategory2);
								(products[0].description).should.match('Naturally farmed gudur Acid Lemon');

								// Call the assertion callback
								done();
							});
					});
			});
	});

	it('should not be able to save Product instance if not logged in', function(done) {
		agent.post('/products')
			.send(product)
			.expect(401)
			.end(function(productSaveErr, productSaveRes) {
				// Call the assertion callback
				done(productSaveErr);
			});
	});

	it('should not be able to save Product instance if no name is provided', function(done) {
		// Invalidate name field
		product.name = '';

		agent.post('/auth/signin')
			.send(credentials)
			.expect(200)
			.end(function(signinErr, signinRes) {
				// Handle signin error
				if (signinErr) done(signinErr);

				var token = signinRes.body.token;

				// Save a new Product
				agent.post('/products')
					.send(product)
					.set('Content-Type', 'application/json')
					.set('token', token)
					.expect(400)
					.end(function(productSaveErr, productSaveRes) {
						// Set message assertion
						(productSaveRes.body.message).should.match('Please fill Product name\b');
						
						// Handle Product save error
						done(productSaveErr);
					});
			});
	});

	it('should be able to update Product instance if signed in', function(done) {
		agent.post('/auth/signin')
			.send(credentials)
			.expect(200)
			.end(function(signinErr, signinRes) {
				// Handle signin error
				if (signinErr) done(signinErr);

				var token = signinRes.body.token;

				// Save a new Product
				agent.post('/products')
					.send(product)
					.set('Content-Type', 'application/json')
					.set('token', token)
					.expect(200)
					.end(function(productSaveErr, productSaveRes) {
						// Handle Product save error
						if (productSaveErr) done(productSaveErr);

						// Update Product name
						product.name = 'WHY YOU GOTTA BE SO MEAN?';

						// Update existing Product
						agent.put('/products/' + productSaveRes.body._id)
							.send(product)
							.set('Content-Type', 'application/json')
							.set('token', token)
							.expect(200)
							.end(function(productUpdateErr, productUpdateRes) {
								// Handle Product update error
								if (productUpdateErr) done(productUpdateErr);

								// Set assertions
								(productUpdateRes.body._id).should.equal(productSaveRes.body._id);
								(productUpdateRes.body.name).should.match('WHY YOU GOTTA BE SO MEAN?');

								// Call the assertion callback
								done();
							});
					});
			});
	});

	it('should be able to get a list of Products if not signed in', function(done) {
		// Create new Product model instance
		var productObj = new Product(product);

		// Save the Product
		productObj.save(function() {
			// Request Products
			request(app).get('/products')
				.end(function(req, res) {
					// Set assertion
					res.body.message.should.match('User is not logged in');

					// Call the assertion callback
					done();
				});

		});
	});


	it('should not be able to get a single Product if not signed in', function (done) {
		// Create new Product model instance
		var productObj = new Product(product);

		// Save the Product
		productObj.save(function() {
			request(app).get('/products/' + productObj._id)
				.end(function(req, res) {
					// Set assertion
					res.body.message.should.match('User is not logged in');

					// Call the assertion callback
					done();
				});
		});
	});

	it('should be able to delete Product instance if signed in', function(done) {
		agent.post('/auth/signin')
			.send(credentials)
			.expect(200)
			.end(function(signinErr, signinRes) {
				// Handle signin error
				if (signinErr) done(signinErr);

				var token = signinRes.body.token;

				// Save a new Product
				agent.post('/products')
					.send(product)
					.set('Content-Type', 'application/json')
					.set('token', token)
					.expect(200)
					.end(function(productSaveErr, productSaveRes) {
						// Handle Product save error
						if (productSaveErr) done(productSaveErr);

						// Delete existing Product
						agent.delete('/products/' + productSaveRes.body._id)
							.send(product)
							.set('Content-Type', 'application/json')
							.set('token', token)
							.expect(200)
							.end(function(productDeleteErr, productDeleteRes) {
								// Handle Product error error
								if (productDeleteErr) done(productDeleteErr);

								// Set assertions
								(productDeleteRes.body._id).should.equal(productSaveRes.body._id);

								// Call the assertion callback
								done();
							});
					});
			});
	});

	it('should not be able to delete Product instance if not signed in', function(done) {
		// Set Product user 
		product.user = user;

		// Create new Product model instance
		var productObj = new Product(product);

		// Save the Product
		productObj.save(function() {
			// Try deleting Product
			request(app).delete('/products/' + productObj._id)
			.expect(401)
			.end(function(productDeleteErr, productDeleteRes) {
				// Set message assertion
				(productDeleteRes.body.message).should.match('User is not logged in');

				// Handle Product error error
				done(productDeleteErr);
			});

		});
	});

	afterEach(function (done) {
		Product.remove().exec();
		done();
	});

	after(function (done) {
		User.remove().exec();
		Company.remove().exec();
		Contact.remove().exec();
		Category.remove().exec();
		Product.remove().exec();
		Inventory.remove().exec();
		Notification.remove().exec();
		done();
	});
});
