'use strict';

var should = require('should'),
	request = require('supertest'),
	app = require('../../server'),
	mongoose = require('mongoose'),
	User = mongoose.model('User'),
	Contact = mongoose.model('Contact'),
	Company = mongoose.model('Company'),
	Group = mongoose.model('Group'),
    commonutil=require('../tests/utils/common.batch.users.utils'),
	agent = request.agent(app);

/**
 * Globals
 */
var credentials, user, token,group,customGroup,criteriaGroup,contact;

/**
 * Group routes tests
 */
describe('Group CRUD tests', function() {
	before(function(done){
        var regUser = {
            username: 'test@test.com',
            password: 'password',
            ConfirmPassword: 'password',
            firstName: 'First Name',
            lastName: 'Last Name',
            companyName: 'nVipani',
            businessType: 'Trader',
            registrationCategory: 'Manufacturer',
            selectedSegments: ['Coffee','Rice'],
            mobile: '0123456789',
            acceptTerms: true
        };
        commonutil.createUser(regUser, agent, function(err,res) {
            should.not.exist(err);
            user=res;
            credentials = {
                username: 'test@test.com',
                password: 'password'
            };
            done();
        });
	});
	beforeEach(function(done) {
        agent.post('/auth/signin')
            .send(credentials)
            .expect(200)
            .end(function (signinErr, signinRes) {
                // Handle signin error
                if (signinErr) done(signinErr);
                token = signinRes.body.token;
	            var contact1 = {
	            	displayName:'DisplayName',
					firstName: 'First Name',
					lastName: 'Last Name',
					contactImageURL: 'modules/contacts/img/default.png',
					companyName: 'Company Name',
					phones: [
						{
							phoneNumber: '0123456789',
							phoneType: 'Mobile',
							primary: true
						}
					],
					emails: [
						{
							email: 'email@mail.com',
							emailType: 'Work',
							primary: true
						}
					],
					addresses: [
						{
							addressLine: '23, 5th Cross',
							city: 'Bangalore',
							state: 'Karnataka',
							country: 'India',
							pinCode: '560075',
							addressType: 'Billing',
							primary: true
						}
					]
	            };
            	agent.post('/contacts').set('token', token).send(contact1).expect(200)
                .end(function (err, contactSave) {
                	var contactId = contactSave.body._id;
                    group = {
                        name: 'Group Name',
                        user: user._id,
						type:'Customer'
                    };
                    customGroup = {
                        name: 'Custom Participants',
                        user: user._id,
                        contacts: [{contact:contactSave.body._id}],
                        type: 'Customer'
                    };
                    criteriaGroup = {
                        name: 'Criteria Group',
                        user: user._id,
                        groupClassification:
                            {
                                classificationType: 'Criteria',
                                classificationCriteria: {
                                    criteriaType: 'Custom',
                                    location: [{
                                        city: 'Bangalore',
                                        state: 'Karnataka',
                                        country: 'India',
                                        pinCode: '560075'
                                    }]
                                }
                            }
                    };
                    done();
                });
			});
	});


	it('should be able to save Group instance if logged in', function(done) {
		agent.post('/auth/signin')
			.send(credentials)
			.expect(200)
			.end(function(signinErr, signinRes) {
				// Handle signin error
				if (signinErr) done(signinErr);

				console.log('signinRes.body-' + JSON.stringify(signinRes.body));

				token = signinRes.body.token;



				// Save a new Group
				agent.post('/groups')
					.send(group)
					.set('token', token)
					.expect(200)
					.end(function(groupSaveErr, groupSaveRes) {
						// Handle Group save error
						if (groupSaveErr) done(groupSaveErr);

						// Get a list of Groups
						agent.get('/groups')
							.set('token', token)
							.end(function(groupsGetErr, groupsGetRes) {
								// Handle Group save error
								if (groupsGetErr) done(groupsGetErr);

								// Get Groups list
								var groups = groupsGetRes.body;
								//console.log('groups-' + JSON.stringify(groups));
								// Set assertions
								groups.total_count.should.equal(1);
								(groups.group[0].user._id).should.equal(user._id);
								(groups.group[0].name).should.match('Group Name');

								// Call the assertion callback
								done();
							});
					});
			});
	});

	it('should not be able to save Group instance if not logged in', function(done) {
		agent.post('/groups')
			.send(group)
			.expect(401)
			.end(function(groupSaveErr, groupSaveRes) {
				// Call the assertion callback
				done(groupSaveErr);
			});
	});

	it('should not be able to save Group instance if no name is provided', function(done) {
		// Invalidate name field
		group.name = '';

		agent.post('/auth/signin')
			.send(credentials)
			.expect(200)
			.end(function(signinErr, signinRes) {
				// Handle signin error
				if (signinErr) done(signinErr);

				var token = signinRes.body.token;


				// Save a new Group
				agent.post('/groups')
					.send(group)
					.set('token', token)
					.expect(400)
					.end(function(groupSaveErr, groupSaveRes) {
						// Set message assertion
						(groupSaveRes.body.message).should.match('Name is required for create group');

						// Handle Group save error
						done(groupSaveErr);
					});
			});
	});

	it('should not create group if location criteria is specifed and no location is specified',function(done){
        //create contacts first
		var location = criteriaGroup.groupClassification.classificationCriteria.location;
		delete criteriaGroup.groupClassification.classificationCriteria.location;
        agent.post('/auth/signin')
            .send(credentials)
            .expect(200)
            .end(function (signinErr, signinRes) {
                // Handle signin error
                if (signinErr) done(signinErr);

                var token = signinRes.body.token;

                agent.post('/groups')
                    .set('token',token)
                    .send(criteriaGroup)
                    .expect(200)
                    .end(function (err, groupSaveRes) {
                        should.exist(err);
                        groupSaveRes.body.message.should.equal('Location must be set while creating if group is a criteria group');
                        done();

                    });
            });
	});
    it('should be able to update Group instance if name is not set', function(done) {
    	var groupname = group.name;
        agent.post('/auth/signin')
            .send(credentials)
            .expect(200)
            .end(function(signinErr, signinRes) {
                // Handle signin error
                if (signinErr) done(signinErr);

                var token = signinRes.body.token;


                // Save a new Group
                agent.post('/groups')
                    .send(group)
                    .set('token', token)
                    .expect(200)
                    .end(function(groupSaveErr, groupSaveRes) {
                        // Handle Group save error
                        if (groupSaveErr) done(groupSaveErr);

                        // Update Group name
                        group.name = '';

                        // Update existing Group
                        agent.put('/groups/' + groupSaveRes.body._id)
                            .set('token', token)
                            .send(group)
                            .expect(200)
                            .end(function(groupUpdateErr, groupUpdateRes) {
                                // Handle Group update error
                                should.exist(groupUpdateErr);
                                groupUpdateRes.body.message.should.equal('Name is required for update group');
								group.name='group updated';
                                // Call the assertion callback
                                done();
                            });
                    });
            });
    });
    it('should be able to update Group if location is not set and is a criteria group', function(done) {
        var groupname = criteriaGroup.name;
        agent.post('/auth/signin')
            .send(credentials)
            .expect(200)
            .end(function(signinErr, signinRes) {
                // Handle signin error
                if (signinErr) done(signinErr);

                var token = signinRes.body.token;


                // Save a new Group
                agent.post('/groups')
                    .send(criteriaGroup)
                    .set('token', token)
                    .expect(200)
                    .end(function(groupSaveErr, groupSaveRes) {
                        // Handle Group save error
                        if (groupSaveErr) done(groupSaveErr);

                        // Update Group name
                        delete criteriaGroup.groupClassification.classificationCriteria.location;

                        // Update existing Group
                        agent.put('/groups/' + groupSaveRes.body._id)
                            .set('token', token)
                            .send(criteriaGroup)
                            .expect(200)
                            .end(function(groupUpdateErr, groupUpdateRes) {
                                // Handle Group update error
                                should.exist(groupUpdateErr);
                                groupUpdateRes.body.message.should.equal('Location must be set while updating if group is a criteria group');

                                // Call the assertion callback
                                done();
                            });
                    });
            });
    });

	it('should be able to update Group instance if signed in', function(done) {
		agent.post('/auth/signin')
			.send(credentials)
			.expect(200)
			.end(function(signinErr, signinRes) {
				// Handle signin error
				if (signinErr) done(signinErr);

				var token = signinRes.body.token;


				// Save a new Group
				agent.post('/groups')
					.send(group)
					.set('token', token)
					.expect(200)
					.end(function(groupSaveErr, groupSaveRes) {
						// Handle Group save error
						if (groupSaveErr) done(groupSaveErr);

						// Update Group name
						group.name = 'group updated';

						// Update existing Group
						agent.put('/groups/' + groupSaveRes.body._id)
							.set('token', token)
							.send(group)
							.expect(200)
							.end(function(groupUpdateErr, groupUpdateRes) {
								// Handle Group update error
								if (groupUpdateErr) done(groupUpdateErr);

								// Set assertions
								(groupUpdateRes.body._id).should.equal(groupSaveRes.body._id);
								(groupUpdateRes.body.name).should.match('group updated');

								// Call the assertion callback
								done();
							});
					});
			});
	});

	it('should be able to get a list of Groups if signed in', function (done) {
		agent.post('/auth/signin')
			.send(credentials)
			.expect(200)
			.end(function (signinErr, signinRes) {
				// Handle signin error
				if (signinErr) done(signinErr);

				var token = signinRes.body.token;


				// Save a new Group
				agent.post('/groups')
					.send(group)
					.set('token', token)
					.expect(200)
					.end(function (groupSaveErr, groupSaveRes) {
						// Handle Group save error
						if (groupSaveErr) done(groupSaveErr);

						// Get a list of Groups
						agent.get('/groups')
							.set('token', token)
							.end(function (groupsGetErr, groupsGetRes) {
								// Handle Group save error
								if (groupsGetErr) done(groupsGetErr);

								// Get Groups list
								var groups = groupsGetRes.body;
								groups.group.should.be.instanceof(Array).and.have.lengthOf(1);
								groups.total_count.should.equal(1);
								done();
							});
					});
			});
	});


	it('should be able to get a single Group if not signed in', function(done) {
		agent.post('/auth/signin')
			.send(credentials)
			.expect(200)
			.end(function (signinErr, signinRes) {
				// Handle signin error
				if (signinErr) done(signinErr);

				var token = signinRes.body.token;


				// Save a new Group
				agent.post('/groups')
					.send(group)
					.set('token', token)
					.expect(200)
					.end(function (groupSaveErr, groupSaveRes) {
						// Handle Group save error
						if (groupSaveErr) done(groupSaveErr);

						// Create new Group model instance
						agent.get('/groups/' + groupSaveRes.body._id)
							.set('token', token)
							.end(function (req, res) {
								// Set assertion
								res.body.should.be.an.Object.with.property('name', group.name);


								// Call the assertion callback
								done();
							});
					});
			});
	});

    it('should be able to list contacts in a custom group',function(done){
        agent.post('/auth/signin')
            .send(credentials)
            .expect(200)
            .end(function (signinErr, signinRes) {
                // Handle signin error
                if (signinErr) done(signinErr);

                var token = signinRes.body.token;


                // Save a new Group
                agent.post('/groups')
                    .send(customGroup)
                    .set('token', token)
					.expect(200)
					.end(function(err,groupSaveRes) {
                        should.not.exist(err);
						if(err){
							done();
						}
						else {
                            agent.get('/groups/' + groupSaveRes.body._id)
                                .set('token', token)
                                .expect(200)
                                .end(function (req, res) {
                                    // Set assertion
                                    res.body.should.be.an.Object.with.property('name', customGroup.name);
                                    var customGroup1 = res.body;
                                    customGroup1.contacts.length.should.equal(1);

                                    // Call the assertion callback
                                    done();
                                });
                        }
                });
        });
    });
    it('should be able to list contacts in a criteria group',function(done) {
        //create contacts first
        agent.post('/auth/signin')
            .send(credentials)
            .expect(200)
            .end(function (signinErr, signinRes) {
                // Handle signin error
                if (signinErr) done(signinErr);

                var token = signinRes.body.token;

                agent.post('/groups')
					.set('token',token)
					.send(criteriaGroup)
					.expect(200)
					.end(function (err, groupSaveRes) {
						should.not.exist(err);
						console.log('ID:'+groupSaveRes.body._id);
						agent.get('/groups/' + groupSaveRes.body._id)
							.set('token', token)
							.expect(200)
							.end(function (req, res) {
								// Set assertion
								res.body.should.be.an.Object.with.property('name', criteriaGroup.name);
								var customGroup1 = res.body;
								console.log(res.body);
								customGroup1.contacts.length.should.equal(1);
								// Call the assertion callback
								done();
							});

                });
            });
    });
    describe('Group action tests',function(done){
    	var token,groups;
    	beforeEach(function(done){
            agent.post('/auth/signin')
                .send(credentials)
                .expect(200)
                .end(function (signinErr, signinRes) {
                    // Handle signin error
                    if (signinErr) {
                        done(signinErr);
                    }
                    else {
                        token = signinRes.body.token;
                        agent.post('/groups')
                            .send(group)
                            .set('token', token)
                            .expect(200)
                            .end(function (groupSaveErr, groupSaveRes) {
                                // Handle Group save error
                                if (groupSaveErr) done(groupSaveErr);

                                // Get a list of Groups
                                agent.get('/groups')
                                    .set('token', token)
                                    .end(function (groupsGetErr, groupsGetRes) {
                                        // Handle Group save error
                                        if (groupsGetErr) done(groupsGetErr);

                                        // Get Groups list
                                        groups = groupsGetRes.body.group;
                                        done();
                                    });
                            });
                    }
                });
		});
    	it('should be able to inactivate multiple groups',function(done){
                //console.log('groups-' + JSON.stringify(groups))
                var groupids = {groups: [groups[0]._id]};
                //inactivate contacts in array
                agent.post('/groupsinactive')
                    .set('token', token)
                    .send(groupids)
                    .expect(200)
                    .end(function (req, res) {
                        //inactivate the contacts by id
                        //get contacts and assert
                        console.log(res.body);

                        agent.get('/groups')
                            .set('token', token)
                            .expect(200).end(function (fetchErr, fetchRes) {
                            console.log(fetchRes.body);
                            fetchRes.body.group.should.be.instanceof(Array).and.have.lengthOf(1);
                            var rxContacts = fetchRes.body.group;
                            rxContacts[0].disabled.should.equal(true);
                            done();
                        });
                    });
            });
        it('should be able to activate multiple groups',function(done){
            //console.log('groups-' + JSON.stringify(groups))
            var groupids = {groups: [groups[0]._id]};
            //inactivate contacts in array
            agent.post('/groupsinactive')
                .set('token', token)
                .send(groupids)
                .expect(200)
                .end(function (req, res) {
                    //inactivate the contacts by id
                    //get contacts and assert
                    console.log(res.body);

                    agent.get('/groups')
                        .set('token', token)
                        .expect(200).end(function (fetchErr, fetchRes) {
                        console.log(fetchRes.body);
                        fetchRes.body.group.should.be.instanceof(Array).and.have.lengthOf(1);
                        var rxContacts = fetchRes.body.group;
                        rxContacts[0].disabled.should.equal(true);
                        agent.post('/groupsactive')
                            .set('token', token)
                            .send(groupids)
                            .expect(200)
                            .end(function (req, res) {
                                //inactivate the contacts by id
                                //get contacts and assert
                                console.log(res.body);
                                agent.get('/groups')
                                    .set('token', token)
                                    .expect(200).end(function (fetchErr, fetchRes) {
                                    console.log(fetchRes.body);
                                    fetchRes.body.group.should.be.instanceof(Array).and.have.lengthOf(1);
                                    var rxContacts = fetchRes.body.group;
                                    rxContacts[0].disabled.should.equal(false);
                                    done();
                                });
                            });

                    });
                });
        });
        it('should be able to delete multiple groups',function(done){
            //console.log('groups-' + JSON.stringify(groups))
            var groupids = {groups: [groups[0]._id]};
            //inactivate contacts in array
            agent.post('/groupsremove')
                .set('token', token)
                .send(groupids)
                .expect(200)
                .end(function (req, res) {
                    //inactivate the contacts by id
                    //get contacts and assert
                    console.log(res.body);
                    agent.get('/groups')
                        .set('token', token)
                        .expect(200).end(function (fetchErr, fetchRes) {
                        console.log(fetchRes.body);
                        fetchRes.body.group.should.be.instanceof(Array).and.have.lengthOf(0);
                        done();
                    });
                });
        });

        });


    afterEach(function(done) {
		Contact.remove().exec();
		Group.remove().exec();
		done();
	});
    after(function(done){
        User.remove().exec();
        Company.remove().exec();
		done();
    });
});
