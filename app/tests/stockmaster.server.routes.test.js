'use strict';

var should = require('should'),
    request = require('supertest'),
    app = require('../../server'),
    mongoose = require('mongoose'),
    User = mongoose.model('User'),
    Contact = mongoose.model('Contact'),
    UnitOfMeasure = mongoose.model('UnitOfMeasure'),
    StockMaster = mongoose.model('StockMaster'),
    agent = request.agent(app);

/**
 * Globals
 */
var credentials, user, unitOfMeasure, unitOfMeasure1,productCategory,productBrand,itemMasterData,token,itemMaster,mainCategory,subCategory1,subCategory2;

/**
 * Item master routes tests
 */
describe('Stock Master CRUD tests', function () {
    before(function (done) {
        var regUser = {
            username: 'test@test.com',
            issendotp: false,
            isverifyotp: false,
            acceptTerms: true
        };
        regUser.issendotp = true;
        agent.post('/user/sendpresignupotp')
            .send(regUser)
            .expect(200)
            .end(function (presignupErr, presignupRes) {
                //console.log("Presignup Response - "+JSON.stringify(presignupRes.message));

                if (presignupErr) done(presignupErr);
                /* user.issendotp=true;
                 user.isverifyotp=true;*/
                else {
                    (presignupRes.body.user.status).should.equal('Register Request');
                    regUser.otp = presignupRes.body.otp;
                    regUser.issendotp = false;
                    regUser.isverifyotp = true;
                    agent.post('/user/sendpresignupotp')
                        .send(regUser)
                        .expect(200)
                        .end(function (presignupErr, verifiedOtp) {
                            //console.log("Presignup Response - "+JSON.stringify(presignupRes.message));
                            if (presignupErr) done(presignupErr);
                            (verifiedOtp.body.user.status).should.equal('Verified');
                            if (verifiedOtp.body.user.username === verifiedOtp.body.user.email) {
                                (verifiedOtp.body.user.emailVerified).should.equal(true);
                            } else if (verifiedOtp.body.username === verifiedOtp.body.mobile) {
                                (verifiedOtp.body.user.mobileVerified).should.equal(true);

                            }
                            regUser.password = 'password';
                            regUser.confirmPassword = 'password';
                            regUser.mobile = '0123456789';
                            regUser.issendotp = false;
                            regUser.isverifyotp = false;
                            regUser.ispassword = true;
                            regUser.acceptTerms = true;
                            agent.post('/user/sendpresignupotp')
                                .send(regUser)
                                .expect(200)
                                .end(function (presignupErr, changePassword) {
                                    //console.log("Presignup Response - "+JSON.stringify(presignupRes.message));
                                    if (presignupErr) done(presignupErr);
                                    (changePassword.body.user.status).should.equal('Registered');
                                    token=changePassword.body.token;
                                    User.findOne({
                                        username: regUser.username
                                    }, '-salt -password', function (err, resUser) {
                                        if (err) {
                                            done(err);
                                        }
                                        user = resUser;
                                        if (resUser) {
                                            user = resUser;


                                             mainCategory = {
                                                name: 'Agriculture',
                                                code: 'AGR',
                                                type: 'MainCategory'
                                            };
                                            agent.post('/categories')
                                                .send(mainCategory)
                                                .set('Content-Type', 'application/json')
                                                .set('token',token )
                                                .expect(200)
                                                .end(function (categorySaveErr, categorySaveRes) {
                                                    // Handle Category save error
                                                    if (categorySaveErr) done(categorySaveErr);

                                                    mainCategory = categorySaveRes.body;

                                                     subCategory1 = {
                                                        name: 'Vegetables',
                                                        code: 'VEG',
                                                        type: 'SubCategory1',
                                                        categoryImageURL1: 'modules/categories/img/subcategory1/vegetables.png'
                                                    };

                                                    // Save a new Category
                                                    //category.token = token;
                                                    agent.post('/categories')
                                                        .send(subCategory1)
                                                        .set('Content-Type', 'application/json')
                                                        .set('token', token)
                                                        .expect(200)
                                                        .end(function (subCategorySaveErr, subCategorySaveRes) {
                                                            // Handle Category save error
                                                            if (subCategorySaveErr) done(subCategorySaveErr);

                                                            var subCategory1 = subCategorySaveRes.body;

                                                            mainCategory.children.push(subCategory1);

                                                            // Update existing Category
                                                            agent.put('/categories/' + mainCategory._id)
                                                                .send(mainCategory)
                                                                .set('Content-Type', 'application/json')
                                                                .set('token', token)
                                                                .expect(200)
                                                                .end(function (categoryUpdateErr, categoryUpdateRes) {
                                                                    // Handle Category update error
                                                                    if (categoryUpdateErr) done(categoryUpdateErr);

                                                                     subCategory2 = {
                                                                        name: 'Lemon',
                                                                        code: 'LEM',
                                                                        type: 'SubCategory2',
                                                                        categoryImageURL1: 'modules/categories/img/subcategory2/lemon.png'
                                                                    };
                                                                    agent.post('/categories')
                                                                        .send(subCategory2)
                                                                        .set('Content-Type', 'application/json')
                                                                        .set('token', token)
                                                                        .expect(200)
                                                                        .end(function (subCategory2SaveErr, subCategory2SaveRes) {
                                                                            // Handle Category save error
                                                                            if (subCategory2SaveErr) done(subCategory2SaveErr);

                                                                            subCategory2 = subCategory2SaveRes.body;

                                                                            subCategory1.children.push(subCategory2);

                                                                            // Update existing Category
                                                                            agent.put('/categories/' + subCategory1._id)
                                                                                .send(subCategory1)
                                                                                .set('Content-Type', 'application/json')
                                                                                .set('token', token)
                                                                                .expect(200)
                                                                                .end(function (subCategoryUpdateErr, subCategoryUpdateRes) {
                                                                                    // Handle Category update error
                                                                                    if (subCategoryUpdateErr) done(subCategoryUpdateErr);
                                                                                    else {
                                                                                        productCategory=subCategory2;
                                                                                        done();
                                                                                    }

                                                                                });
                                                                        });
                                                                });
                                                        });
                                                });
                                        }
                                    });
                                });
                        });
                }
            });

        credentials = {
            username: 'test@test.com',
            password: 'password'
        };
    });
    beforeEach(function (done) {
        unitOfMeasure = {
            name: 'Kg',
            symbol: 'Kg',
            type: 'Simple'
        };
        unitOfMeasure1 = {
            name: 'Litre',
            symbol: 'Ltr',
            type: 'Simple',
            numberOfDecimalPlaces: 2
        };
        itemMasterData={
            MRP:20
        };
         productBrand = {
            name: 'NLR Rice',
            productCategory: productCategory._id,
            productBrandImageURL1: 'modules/categories/img/subcategory2/rice.png'
        };
        agent.post('/productBrand')
            .send(productBrand)
            .set('token', token)
            .set('Content-Type', 'application/json')
            .expect(200)
            .end(function (productBrandErr, productBrandRes) {
                // Handle Category update error
                if (productBrandErr) done(productBrandErr);
                productBrand = productBrandRes.body;
                itemMasterData.productBrand=productBrand._id;
                agent.post('/auth/signin')
                    .send(credentials)
                    .expect(200)
                    .end(function (signinErr, signinRes) {
                        // Handle signin error
                        if (signinErr) done(signinErr);

                        var token = signinRes.body.token;


                        // Save a new Contact
                        agent.post('/itemMaster')
                            .send(itemMasterData)
                            .set('token', token)
                            .expect(200)
                            .end(function (itemMasterSaveErr, itemMasterSaveRes) {
                                // Set message assertion
                                if(itemMasterSaveErr) done(itemMasterSaveErr);
                                itemMaster=itemMasterSaveRes.body;
                                // Handle Item Master save error
                                done();
                            });
                    });

            });
    });

    it('should not be able to save Stock Master With existing Existing Inventory is provided', function (done) {
        var inventory= {
            unitSize: 50,
                unitMeasure: 'Kg',
                itemMaster:itemMaster._id,
                unitPrice: 5000.23,
                numberOfUnits: 1000,
                moqAndSalePrice: [{
                MOQ: 5,
                margin: 30,
                price: 5000.23 *(1- 30/100).toFixed('2')
            }]
        };

        agent.post('/inventories')
            .send(itemMasterData)
            .set('token', token)
            .expect(200)
            .end(function (inventoryErr, inventorySaveRes) {
                // Set message assertion
                if(inventoryErr) done(inventoryErr);
                inventory=inventorySaveRes.body;
                // Handle Inventory Master save error
                done();
            });

    });


    afterEach(function (done) {
      /*  User.remove().exec();*/
        //Company.remove().exec();
       /* StockMaster.remove().exec();*/
        done();
    });
    after(function (done) {
        done();
    });
});
