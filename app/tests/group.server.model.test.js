'use strict';

/**
 * Module dependencies.
 */
var should = require('should'),
	mongoose = require('mongoose'),
    app = require('../../server'),
	User = mongoose.model('User'),
	Group = mongoose.model('Group'),
	Contact = mongoose.model('Contact');

/**
 * Globals
 */
var user, group,customParticipantsGroup,criteriaGroup,contact,contactId;

/**
 * Unit tests
 */
describe('Group Model Unit Tests:', function() {
	beforeEach(function(done) {
		user = new User({
			firstName: 'Full',
			lastName: 'Name',
			displayName: 'Full Name',
			email: 'test@test.com',
			username: 'test@test.com',
			password: 'password',
			status: 'Registered',
			provider: 'local'
		});


		user.save(function() {
            var contact1 = {
                firstName: 'First Name',
                lastName: 'Last Name',
                contactImageURL: 'modules/contacts/img/default.png',
                companyName: 'Company Name',
                phones: [
                    {
                        phoneNumber: '0123456789',
                        phoneType: 'Mobile',
                        primary: true
                    }
                ],
                emails: [
                    {
                        email: 'email@mail.com',
                        emailType: 'Work',
                        primary: true
                    }
                ],
                addresses: [
                    {
                        addressLine: '23, 5th Cross',
                        city: 'Bangalore',
                        state: 'Karnataka',
                        country: 'India',
                        pinCode: '560075',
                        addressType: 'Billing',
                        primary: true
                    }
                ]
            };
            contact = new Contact(contact1);
            contact.save(function(err){
            	if(err) {
            		should.not.exist(err);
                }
                else{
                    contactId = contact._id;
                    group = new Group({
                        name: 'Group Name',
                        user: user
                    });

                    customParticipantsGroup = new Group({
                        name: 'Custom Participants',
                        user: user,
                        contacts:[contact._id]
                    });
                    criteriaGroup = new Group({
                        name: 'Criteria Group',
                        user: user
                    });
				}
				done();
			});


		});
	});

	describe('Method Save', function() {
		it('should be able to save without problems', function(done) {
			return group.save(function(err) {
				should.not.exist(err);
				done();
			});
		});
		it('should be able to save criteria group without problems',function(done){
			criteriaGroup.type='Customer';
            criteriaGroup.groupClassification = {classificationType:'Criteria',
				classificationCriteria:{location:[{city:'Bangalore',state:'Karnataka',country:'India',
                    pinCode:'560075'}]}};
			return criteriaGroup.save(function(err){
				should.not.exist(err);
				should.exist(criteriaGroup._id);
				done();
			});
		});
        it('should be able to save custom group without problems',function(done){
        	//create contacts first

            customParticipantsGroup.type='Customer';
            customParticipantsGroup.groupClassification = {classificationType:'Criteria',
                classificationCriteria:{location:[{city:'Bangalore',state:'Karnataka',country:'India',pinCode:560075}]}};

            return customParticipantsGroup.save(function(err){
                should.not.exist(err);
                done();
            });
        });

		/*it('should be able to show an error when try to save without name', function(done) {
			group.name = '';

			return criteriaGroup.save(function(err) {
				should.exist(err);
				done();
			});
		});*/
	});

	afterEach(function(done) { 
		Group.remove().exec();
		User.remove().exec();

		done();
	});
});
