'use strict';

var should = require('should'),
    request = require('supertest'),
    app = require('../../server'),
    mongoose = require('mongoose'),
    User = mongoose.model('User'),
    Contact = mongoose.model('Contact'),
    Company = mongoose.model('Company'),
    Notification = mongoose.model('Notification'),
    agent = request.agent(app);

/**
 * Globals
 */
var credentials, user, notification;

/**
 * Notification routes tests
 */
describe('Notification CRUD tests', function () {
    beforeEach(function (done) {
        var regUser = {
            username: 'test@test.com',
            password: 'password',
            ConfirmPassword: 'password',
            firstName: 'First Name',
            lastName: 'Last Name',
            companyName: 'nVipani',
            businessType: 'Trader',
            categorySeller: true,
            categoryBuyer: true,
            categoryMediator: true,
            mobile: '0123456789',
            acceptTerms: true
        };
        agent.post('/user/presignup')
            .send(regUser)
            .expect(200)
            .end(function (presignupErr, presignupRes) {
                //console.log("Presignup Response - "+JSON.stringify(presignupRes.message));
                if (presignupErr) done(presignupErr);

                User.findOne({
                    username: credentials.username
                }, '-salt -password', function (err, resUser) {
                    if (err) {
                        done(err);
                    }
                    user = resUser;
                    if (resUser) {
                        agent.get('/user/register/' + resUser.statusToken)
                            .expect(200)
                            .end(function (validationGetErr, validationGetRes) {
                                //console.log("Presignup Response - "+JSON.stringify(presignupRes.message));
                                if (validationGetErr) done(validationGetErr);

                                notification = {
                                    title: 'Title of the notification'
                                };

                                done();
                            });
                    }
                });
            });

        credentials = {
            username: 'test@test.com',
            password: 'password'
        };
    });

    it('should be able to save Notification instance if logged in', function (done) {
        agent.post('/auth/signin')
            .send(credentials)
            .expect(200)
            .end(function (signinErr, signinRes) {
                // Handle signin error
                if (signinErr) done(signinErr);

                var token = signinRes.body.token;

                // Save a new Notification
                agent.post('/notifications')
                    .send(notification)
                    .set('Content-Type', 'application/json')
                    .set('token', token)
                    .expect(200)
                    .end(function (notificationSaveErr, notificationSaveRes) {
                        // Handle Notification save error
                        if (notificationSaveErr) done(notificationSaveErr);

                        var notification = notificationSaveRes.body;

                        (notification.user).should.equal(user.id);
                        (notification.title).should.match('Title of the notification');
                        done();
                        // Get a list of Notifications
                        /*                        agent.get('/notifications')
                         .set('Content-Type', 'application/json')
                         .set('token', token)
                            .end(function (notificationsGetErr, notificationsGetRes) {
                                // Handle Notification save error
                                if (notificationsGetErr) done(notificationsGetErr);

                                // Get Notifications list
                                var notifications = notificationsGetRes.body;

                                // Set assertions
                         (notifications[0].user._id).should.equal(user.id);
                         (notifications[0].title).should.match('Title of the notification');

                                // Call the assertion callback
                                done();
                         });*/
                    });
            });
    });

    it('should not be able to save Notification instance if not logged in', function (done) {
        agent.post('/notifications')
            .send(notification)
            .expect(401)
            .end(function (notificationSaveErr, notificationSaveRes) {
                // Call the assertion callback
                done(notificationSaveErr);
            });
    });

    it('should not be able to save Notification instance if no name is provided', function (done) {
        // Invalidate name field
        notification.title = '';

        agent.post('/auth/signin')
            .send(credentials)
            .expect(200)
            .end(function (signinErr, signinRes) {
                // Handle signin error
                if (signinErr) done(signinErr);

                var token = signinRes.body.token;

                // Save a new Notification
                agent.post('/notifications')
                    .send(notification)
                    .set('Content-Type', 'application/json')
                    .set('token', token)
                    .expect(400)
                    .end(function (notificationSaveErr, notificationSaveRes) {
                        // Set message assertion
                        (notificationSaveRes.body.message).should.match('Please fill Notification title\b');

                        // Handle Notification save error
                        done(notificationSaveErr);
                    });
            });
    });

    it('should be able to update Notification instance if signed in', function (done) {
        agent.post('/auth/signin')
            .send(credentials)
            .expect(200)
            .end(function (signinErr, signinRes) {
                // Handle signin error
                if (signinErr) done(signinErr);

                var token = signinRes.body.token;

                // Save a new Notification
                agent.post('/notifications')
                    .send(notification)
                    .set('Content-Type', 'application/json')
                    .set('token', token)
                    .expect(200)
                    .end(function (notificationSaveErr, notificationSaveRes) {
                        // Handle Notification save error
                        if (notificationSaveErr) done(notificationSaveErr);

                        // Update Notification name
                        notification.title = 'WHY YOU GOTTA BE SO MEAN?';

                        // Update existing Notification
                        agent.put('/notifications/' + notificationSaveRes.body._id)
                            .send(notification)
                            .set('Content-Type', 'application/json')
                            .set('token', token)
                            .expect(200)
                            .end(function (notificationUpdateErr, notificationUpdateRes) {
                                // Handle Notification update error
                                if (notificationUpdateErr) done(notificationUpdateErr);

                                // Set assertions
                                (notificationUpdateRes.body._id).should.equal(notificationSaveRes.body._id);
                                (notificationUpdateRes.body.title).should.match('WHY YOU GOTTA BE SO MEAN?');

                                // Call the assertion callback
                                done();
                            });
                    });
            });
    });

    /*    it('should be able to get a list of Notifications if not signed in', function (done) {
        // Create new Notification model instance
        var notificationObj = new Notification(notification);

        // Save the Notification
        notificationObj.save(function () {
            // Request Notifications
            request(app).get('/notifications')
                .end(function (req, res) {
                    // Set assertion
                    res.body.should.be.instanceof(Array).and.have.lengthOf(1);

                    // Call the assertion callback
                    done();
                });

        });
    });


    it('should be able to get a single Notification if not signed in', function (done) {
        // Create new Notification model instance
        var notificationObj = new Notification(notification);

        // Save the Notification
        notificationObj.save(function () {
            request(app).get('/notifications/' + notificationObj._id)
                .end(function (req, res) {
                    // Set assertion
                    res.body.should.be.an.Object.with.property('name', notification.name);

                    // Call the assertion callback
                    done();
                });
        });
     });*/

    it('should be able to delete Notification instance if signed in', function (done) {
        agent.post('/auth/signin')
            .send(credentials)
            .expect(200)
            .end(function (signinErr, signinRes) {
                // Handle signin error
                if (signinErr) done(signinErr);

                var token = signinRes.body.token;

                // Save a new Notification
                agent.post('/notifications')
                    .send(notification)
                    .set('Content-Type', 'application/json')
                    .set('token', token)
                    .expect(200)
                    .end(function (notificationSaveErr, notificationSaveRes) {
                        // Handle Notification save error
                        if (notificationSaveErr) done(notificationSaveErr);

                        // Delete existing Notification
                        agent.delete('/notifications/' + notificationSaveRes.body._id)
                            .send(notification)
                            .set('Content-Type', 'application/json')
                            .set('token', token)
                            .expect(200)
                            .end(function (notificationDeleteErr, notificationDeleteRes) {
                                // Handle Notification error error
                                if (notificationDeleteErr) done(notificationDeleteErr);

                                // Set assertions
                                (notificationDeleteRes.body._id).should.equal(notificationSaveRes.body._id);

                                // Call the assertion callback
                                done();
                            });
                    });
            });
    });

    it('should not be able to delete Notification instance if not signed in', function (done) {
        // Set Notification user
        notification.user = user;

        // Create new Notification model instance
        var notificationObj = new Notification(notification);

        // Save the Notification
        notificationObj.save(function () {
            // Try deleting Notification
            request(app).delete('/notifications/' + notificationObj._id)
                .expect(401)
                .end(function (notificationDeleteErr, notificationDeleteRes) {
                    // Set message assertion
                    (notificationDeleteRes.body.message).should.match('User is not logged in');

                    // Handle Notification error error
                    done(notificationDeleteErr);
                });

        });
    });

    afterEach(function (done) {
        User.remove().exec();
        Company.remove().exec();
        Contact.remove().exec();
        Notification.remove().exec();
        done();
    });
});
