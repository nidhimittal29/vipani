'use strict';

var should = require('should'),
    request = require('supertest'),
    app = require('../../server'),
    mongoose = require('mongoose'),
	commonUtils=require('./utils/common.utils'),
    User = mongoose.model('User'),
    Contact = mongoose.model('Contact'),
    Company = mongoose.model('Company'),
    async = require('async'),
    testcommonutil=require('../tests/utils/common.batch.users.utils'),
    agent = request.agent(app);

/**
 * Globals
 */
var credentials,user,user2, customerContact,contactWithoutPhoneOrEmail,businessUnit,token;

/*
 * Contact routes tests
 */
describe('Contact CRUD tests', function() {

    before(function (done) {
        var regUsers = [{
            username: 'test1@test.com',
            companyName: 'nVipani',
            businessType: 'Trader',
            categorySeller: true,
            categoryBuyer: true,
            categoryMediator: true,
            mobile: '0123456789',
            registrationCategory: 'Distributor',
            selectedSegments: ['Rice', 'Coffee'],
            acceptTerms: true
        }, {
            username: 'test2@test.com',
            companyName: 'nVipani1',
            businessType: 'Trader',
            categorySeller: true,
            categoryBuyer: true,
            categoryMediator: true,
            mobile: '0123456790',
            registrationCategory: 'Distributor',
            selectedSegments: ['Rice', 'Coffee'],
            acceptTerms: true
        }];

        var index = 0;

        async.forEachSeries(regUsers, function (user1, callback) {
            testcommonutil.createUser(user1, agent, function (err, res) {
                should.not.exist(err);
                user = res;
                credentials = {
                    username: 'test1@test.com',
                    password: 'password'
                };

                businessUnit = {
                    name: 'Bangalore Godown',
                    type: 'Stockpoint',
                    emails: [{email: 'blrgdn@test.com', isPrimary: true, emailType: 'Work'}],
                    addresses: [{
                        addressLine: 'address line1',
                        city: 'bangalore',
                        state: 'Karnataka',
                        country: 'India',
                        pinCode: 560075,
                        primary: true
                    }],

                    disabled: false,
                    deleted: false,
                    company: user.company,
                    user: user._id
                };

                customerContact = {
                    customerType:'Customer',
                    displayName: 'DisplayName',
                    firstName: 'First Name',
                    lastName: 'Last Name',
                    contactImageURL: 'modules/contacts/img/default.png',
                    companyName: 'Company Name',
                    phones: [
                        {
                            phoneNumber: '0123456789',
                            phoneType: 'Mobile',
                            primary: true
                        }
                    ],
                    emails: [
                        {
                            email: 'email@mail.com',
                            emailType: 'Work',
                            primary: true
                        }
                    ],
                    addresses: [
                        {
                            addressLine: '23, 5th Cross',
                            city: 'City',
                            state: 'State',
                            country: 'India',
                            pinCode: '560075',
                            addressType: 'Billing',
                            primary: true
                        }
                    ]
                };
                contactWithoutPhoneOrEmail = {
                    displayName: 'DispName',
                    firstName: 'First Name',
                    lastName: 'Last Name',
                    contactImageURL: 'modules/contacts/img/default.png',
                    companyName: 'Company Name'
                };
                done();
            });
        });
    });

    beforeEach(function (done) {
        agent.post('/auth/signin')
            .send(credentials)
            .expect(200)
            .end(function (err, res) {
                // Handle signin error
                should.not.exist(err);
                var userres = res.body;
                token = userres.token;
                done();
            });
    });

    /**
     * case 1:Create contact with required fields +ve test case
     */
    it('should be able to Create contact',function (done) {
        agent.get('/contacts')
            .set('token', token)
            .end(function(contactsGetErr, contactsGetRes){
                // Handle Contact save error
                should.not.exist(contactsGetErr);
                should.exist(contactsGetRes);

                // Get Contacts list
                contactsGetRes.body.length.should.be.equal(1); //Default contact
                // Save a new Contact
                agent.post('/contacts')
                    .send(customerContact)
                    .set('token', token)
                    .expect(200)
                    .end(function (contactSaveErr, contactSaveRes) {
                        // Handle Contact save error
                        should.not.exist(contactSaveErr);

                        // Get a list of Contacts
                        agent.get('/contacts')
                            .set('token', token)
                            .end(function (contactsGetErr, contactsGetRes) {
                                // Handle Contact save error
                                if (contactsGetErr) done(contactsGetErr);

                                // Get Contacts list
                                var contacts = contactsGetRes.body;
                                // Set assertions
                                (contacts[0].user._id).should.equal(user._id);
                                (contacts[0].firstName).should.match('First Name');
                                (contacts[0].lastName).should.match('Last Name');
                                (contacts[0].companyName).should.match('Company Name');

                                (contacts[0].phones[0].phoneNumber).should.match('0123456789');
                                (contacts[0].phones[0].phoneType).should.match('Mobile');
                                (contacts[0].phones[0].primary).should.match(true);

                                (contacts[0].emails[0].email).should.match('email@mail.com');
                                (contacts[0].emails[0].emailType).should.match('Work');
                                (contacts[0].emails[0].primary).should.match(true);


                                (contacts[0].addresses[0].addressLine).should.match('23, 5th Cross');
                                (contacts[0].addresses[0].city).should.match('City');
                                (contacts[0].addresses[0].state).should.match('State');
                                (contacts[0].addresses[0].country).should.match('India');
                                (contacts[0].addresses[0].pinCode).should.match('560075');
                                (contacts[0].addresses[0].addressType).should.match('Billing');
                                (contacts[0].addresses[0].primary).should.match(true);

                                var data = {summaryFilters: {NewlyAdded: true}};
                                agent.get('/customercontactssummary?&page=1&limit=10')
                                    .send(data)
                                    .set('token', token)
                                    .end(function (contactSummaryErr, contactSummary) {
                                        should.not.exist(contactSummaryErr);
                                        should.exist(contactSummary.body);
                                        contactSummary.body.NewlyAdded.should.be.instanceof(Array).and.have.lengthOf(1);
                                        agent.get('/customercontactssummary')
                                            .set('token', token)
                                            .end(function (contactSummaryErr, contactSummary) {
                                                should.not.exist(contactSummaryErr);
                                                should.exist(contactSummary.body);
                                                contactSummary.body.NewlyAdded.should.match(1);
                                                // Call the assertion callback
                                                done();
                                            });
                                    });
                            });
                    });
            });
    });

    it('should not be able to save contact instance if display name is not set',function(done) {
        customerContact.displayName = '';

        agent.get('/contacts')
            .set('token', token)
            .end(function (contactsGetErr, contactsGetRes) {
                // Handle Contact save error
                should.not.exist(contactsGetErr);
                should.exist(contactsGetRes);

                // Get Contacts list
                contactsGetRes.body.length.should.be.equal(0);
                // Save a new Contact
                agent.post('/contacts')
                    .send(customerContact)
                    .set('token', token)
                    .expect(200)
                    .end(function (contactSaveErr, contactSaveRes) {
                        // Handle Contact save error
                        should.exist(contactSaveErr);
                        contactSaveRes.body.message.should.equal('Display name is required for create contact');
                        done();

                    });
            });
    });
    it('should not be able to save contact instance if display name is not a string',function(done) {
        customerContact.displayName = 25;

        agent.get('/contacts')
            .set('token', token)
            .end(function (contactsGetErr, contactsGetRes) {
                // Handle Contact save error
                should.not.exist(contactsGetErr);
                should.exist(contactsGetRes);

                // Get Contacts list
                contactsGetRes.body.length.should.be.equal(0);
                // Save a new Contact
                agent.post('/contacts')
                    .send(customerContact)
                    .set('token', token)
                    .expect(200)
                    .end(function (contactSaveErr, contactSaveRes) {
                        // Handle Contact save error
                        should.exist(contactSaveErr);
                        contactSaveRes.body.message.should.be.equal('Display name is required as string for create contact');
                        customerContact.displayName='DisplayName';
                        done();
                    });
            });
    });

    it('should be able to save contact with minimal fields',function(done){
        var minimalFieldContact = {
            displayName:'minimal contact',
            phones: [
                {
                    phoneNumber: '0123456789',
                    phoneType: 'Mobile',
                    primary: true
                }
            ]
        };
        agent.post('/auth/signin')
            .send(credentials)
            .expect(200)
            .end(function(signinErr, signinRes) {
                // Handle signin error
                if (signinErr) done(signinErr);

                // Get the token
                token = signinRes.body.token;

                // Fetch contacts for the Default registration contact.
                agent.get('/contacts')
                    .set('token', token)
                    .end(function(contactsGetRErr, contactsGetReg) {
                        // Handle Contact save error
                        if (contactsGetRErr) done(contactsGetRErr);

                        // Get Contacts list
                        // Save a new Contact
                        agent.post('/contacts')
                            .send(minimalFieldContact)
                            .set('token', token)
                            .expect(200)
                            .end(function (contactSaveErr, contactSaveRes) {
                                // Handle Contact save error
                                if (contactSaveErr) done(contactSaveErr);

                                // Get a list of Contacts
                                agent.get('/contacts')
                                    .set('token', token)
                                    .end(function (contactsGetErr, contactsGetRes) {
                                        // Handle Contact save error
                                        if (contactsGetErr) done(contactsGetErr);

                                        // Get Contacts list
                                        var contacts = contactsGetRes.body;
                                        // Set assertions
                                        (contacts[0].user._id).should.equal(user._id);
                                        (contacts[0].displayName).should.match(minimalFieldContact.displayName);
                                        (contacts[0].phones[0].phoneNumber).should.match('0123456789');
                                        (contacts[0].phones[0].phoneType).should.match('Mobile');
                                        (contacts[0].phones[0].primary).should.match(true);
                                        // Call the assertion callback
                                        done();
                                    });
                            });
                    });
            });
    });
    it('should be able to save contact with all addresses',function(done){
        var allAddressesContact = {
            customerType:'Customer',
            displayName: 'all addresses contact',
            firstName: 'First Name',
            lastName: 'Last Name',
            contactImageURL: 'modules/contacts/img/default.png',
            companyName: 'Company Name',
            phones: [
                {
                    phoneNumber: '0123456789',
                    phoneType: 'Mobile',
                    primary: true
                }
            ],
            emails: [
                {
                    email: 'email@mail.com',
                    emailType: 'Work',
                    primary: true
                }
            ],
            addresses: [
                {
                    addressLine: '23, 5th Cross',
                    city: 'City',
                    state: 'State',
                    country: 'India',
                    pinCode: '560075',
                    addressType: 'Billing',
                    primary: true
                },
                {
                    addressLine: '23, 5th Cross',
                    city: 'City',
                    state: 'State',
                    country: 'India',
                    pinCode: '560035',
                    addressType: 'Shipping',
                    primary: true
                },
                {
                    addressLine: '23, 5th Cross',
                    city: 'City',
                    state: 'State',
                    country: 'India',
                    pinCode: '560025',
                    addressType: 'Receiving',
                    primary: true
                },
                {
                    addressLine: '23, 5th Cross',
                    city: 'City',
                    state: 'State',
                    country: 'India',
                    pinCode: '560085',
                    addressType: 'Invoice',
                    primary: true
                }

            ]
        };
        agent.post('/auth/signin')
            .send(credentials)
            .expect(200)
            .end(function(signinErr, signinRes) {
                // Handle signin error
                if (signinErr) done(signinErr);

                // Get the token
                token = signinRes.body.token;

                // Fetch contacts for the Default registration contact.
                agent.get('/contacts')
                    .set('token', token)
                    .end(function(contactsGetRErr, contactsGetReg) {
                        // Handle Contact save error
                        if (contactsGetRErr) done(contactsGetRErr);

                        // Get Contacts list
                        // Save a new Contact
                        agent.post('/contacts')
                            .send(allAddressesContact)
                            .set('token', token)
                            .expect(200)
                            .end(function (contactSaveErr, contactSaveRes) {
                                // Handle Contact save error
                                if (contactSaveErr) done(contactSaveErr);

                                // Get a list of Contacts
                                agent.get('/contacts')
                                    .set('token', token)
                                    .end(function (contactsGetErr, contactsGetRes) {
                                        // Handle Contact save error
                                        if (contactsGetErr) done(contactsGetErr);

                                        // Get Contacts list
                                        var contacts = contactsGetRes.body;
                                        // Set assertions
                                        (contacts[0].user._id).should.equal(user._id);
                                        (contacts[0].displayName).should.match(allAddressesContact.displayName);
                                        (contacts[0].phones[0].phoneNumber).should.match('0123456789');
                                        (contacts[0].phones[0].phoneType).should.match('Mobile');
                                        (contacts[0].phones[0].primary).should.match(true);
                                        (contacts[0].addresses.length.should.equal(4));
                                        contacts[0].addresses[0].addressType.should.equal('Billing');
                                        contacts[0].addresses[0].pinCode.should.equal('560075');
                                        contacts[0].addresses[1].addressType.should.equal('Shipping');
                                        contacts[0].addresses[1].pinCode.should.equal('560035');
                                        contacts[0].addresses[2].addressType.should.equal('Receiving');
                                        contacts[0].addresses[2].pinCode.should.equal('560025');
                                        contacts[0].addresses[3].addressType.should.equal('Invoice');
                                        contacts[0].addresses[3].pinCode.should.equal('560085');
                                        // Call the assertion callback
                                        done();
                                    });
                            });
                    });
            });
    });

    it('should be able to save contact with multiple addresses of same type',function(done){
        var allAddressesContact = {
            customerType:'Customer',
            displayName: 'all addresses contact',
            firstName: 'First Name',
            lastName: 'Last Name',
            contactImageURL: 'modules/contacts/img/default.png',
            companyName: 'Company Name',
            phones: [
                {
                    phoneNumber: '0123456789',
                    phoneType: 'Mobile',
                    primary: true
                }
            ],
            emails: [
                {
                    email: 'email@mail.com',
                    emailType: 'Work',
                    primary: true
                }
            ],
            addresses: [
                {
                    addressLine: '23, 5th Cross',
                    city: 'City',
                    state: 'State',
                    country: 'India',
                    pinCode: '560075',
                    addressType: 'Billing',
                    primary: true
                },
                {
                    addressLine: '2312, 15th Cross',
                    city: 'City',
                    state: 'State',
                    country: 'India',
                    pinCode: '560175',
                    addressType: 'Billing',
                    primary: false
                },
            ]
        };
        agent.post('/auth/signin')
            .send(credentials)
            .expect(200)
            .end(function(signinErr, signinRes) {
                // Handle signin error
                if (signinErr) done(signinErr);

                // Get the token
                token = signinRes.body.token;

                // Fetch contacts for the Default registration contact.
                agent.get('/contacts')
                    .set('token', token)
                    .end(function(contactsGetRErr, contactsGetReg) {
                        // Handle Contact save error
                        if (contactsGetRErr) done(contactsGetRErr);

                        // Get Contacts list
                        // Save a new Contact
                        agent.post('/contacts')
                            .send(allAddressesContact)
                            .set('token', token)
                            .expect(200)
                            .end(function (contactSaveErr, contactSaveRes) {
                                // Handle Contact save error
                                if (contactSaveErr) done(contactSaveErr);

                                // Get a list of Contacts
                                agent.get('/contacts')
                                    .set('token', token)
                                    .end(function (contactsGetErr, contactsGetRes) {
                                        // Handle Contact save error
                                        if (contactsGetErr) done(contactsGetErr);

                                        // Get Contacts list
                                        var contacts = contactsGetRes.body;
                                        // Set assertions
                                        (contacts[0].user._id).should.equal(user._id);
                                        (contacts[0].displayName).should.match(allAddressesContact.displayName);
                                        (contacts[0].phones[0].phoneNumber).should.match('0123456789');
                                        (contacts[0].phones[0].phoneType).should.match('Mobile');
                                        (contacts[0].phones[0].primary).should.match(true);
                                        (contacts[0].addresses.length.should.equal(2));
                                        contacts[0].addresses[0].addressType.should.equal('Billing');
                                        contacts[0].addresses[0].pinCode.should.equal('560075');
                                        contacts[0].addresses[1].addressType.should.equal('Billing');
                                        contacts[0].addresses[1].pinCode.should.equal('560175');
                                        // Call the assertion callback
                                        done();
                                    });
                            });
                    });
            });
    });


    it('should be able to save Contact instance if logged in', function(done) {
        agent.post('/auth/signin')
            .send(credentials)
            .expect(200)
            .end(function(signinErr, signinRes) {
                // Handle signin error
                if (signinErr) done(signinErr);

                // Get the token
                token = signinRes.body.token;

                    // Fetch contacts for the Default registration contact.
                agent.get('/contacts')
                    .set('token', token)
                    .end(function(contactsGetRErr, contactsGetReg) {
                        // Handle Contact save error
                        if (contactsGetRErr) done(contactsGetRErr);

                        // Get Contacts list
                        // Save a new Contact
                        agent.post('/contacts')
                            .send(customerContact)
                            .set('token', token)
                            .expect(200)
                            .end(function (contactSaveErr, contactSaveRes) {
                                // Handle Contact save error
                                if (contactSaveErr) done(contactSaveErr);

                                // Get a list of Contacts
                                agent.get('/contacts')
                                    .set('token', token)
                                    .end(function (contactsGetErr, contactsGetRes) {
                                        // Handle Contact save error
                                        if (contactsGetErr) done(contactsGetErr);

                                        // Get Contacts list
                                        var contacts = contactsGetRes.body;
                                        // Set assertions
                                        (contacts[0].user._id).should.equal(user._id);
                                        (contacts[0].firstName).should.match('First Name');
                                        (contacts[0].lastName).should.match('Last Name');
                                        (contacts[0].companyName).should.match('Company Name');

                                        (contacts[0].phones[0].phoneNumber).should.match('0123456789');
                                        (contacts[0].phones[0].phoneType).should.match('Mobile');
                                        (contacts[0].phones[0].primary).should.match(true);

                                        (contacts[0].emails[0].email).should.match('email@mail.com');
                                        (contacts[0].emails[0].emailType).should.match('Work');
                                        (contacts[0].emails[0].primary).should.match(true);


                                        (contacts[0].addresses[0].addressLine).should.match('23, 5th Cross');
                                        (contacts[0].addresses[0].city).should.match('City');
                                        (contacts[0].addresses[0].state).should.match('State');
                                        (contacts[0].addresses[0].country).should.match('India');
                                        (contacts[0].addresses[0].pinCode).should.match('560075');
                                        (contacts[0].addresses[0].addressType).should.match('Billing');
                                        (contacts[0].addresses[0].primary).should.match(true);

                                        // Call the assertion callback
                                        done();
                                    });
                            });
                    });
            });
    });

    it('should not be able to save Contact if phone or email not provided', function(done) {
        agent.post('/auth/signin')
            .send(credentials)
            .expect(200)
            .end(function(signinErr, signinRes) {
                // Handle signin error
                if (signinErr) done(signinErr);

                // Get the token
                var token = signinRes.body.token;
                agent.post('/contacts')
                    .set('token', token)
                    .send(contactWithoutPhoneOrEmail)
                    .expect(400)
                    .end(function (contactSaveErr, contactSaveRes) {
                        // Call the assertion callback
                        contactSaveRes.body.message.should.be.equal('Phone number or email id is required for create contact');
                       //should.contactSaveRes.body.message.equal('Email or phone is required for contact creation');
                        done();
                    });
            });
    });

    it('should not be able to save Contact instance if not logged in', function(done) {
        agent.post('/contacts')
            .send(customerContact)
            .expect(401)
            .end(function(contactSaveErr, contactSaveRes) {
                // Call the assertion callback
                done(contactSaveErr);
            });
    });

    it('should be able to add multiple contacts if signed in',function(done){
        agent.post('/auth/signin')
            .send(credentials)
            .expect(200)
            .end(function(signinErr, signinRes) {
                // Handle signin error
                if (signinErr) done(signinErr);

                var token = signinRes.body.token;
                var contactsToImport={contacts:[
                    {   contact:{
                        customerType:'Customer',
                        firstName: 'Abc1',
                        lastName: 'xyz1',
                        contactImageURL: 'modules/contacts/img/default.png',
                        companyName: 'Company Name',
                        phones: [
                            {
                                phoneNumber: '0123456789',
                                phoneType: 'Mobile',
                                primary: true
                            }
                        ],
                        emails: [
                            {
                                email: 'email1@mail1.com',
                                emailType: 'Work',
                                primary: true
                            }
                        ],
                        addresses: [
                            {
                                addressLine: '23, 5th Cross',
                                city: 'City',
                                state: 'State',
                                country: 'India',
                                pinCode: '560075',
                                addressType: 'Billing',
                                primary: true
                            }

                        ]
                    },srcLine:1},
                    {contact:{
                        customerType:'Customer',
                        firstName: 'Abc2',
                        lastName: 'xyz2',
                        contactImageURL: 'modules/contacts/img/default.png',
                        companyName: 'Company Name1',
                        phones: [
                            {
                                phoneNumber: '0123356789',
                                phoneType: 'Mobile',
                                primary: true
                            }
                        ],
                        emails: [
                            {
                                email: 'email1@mail2.com',
                                emailType: 'Work',
                                primary: true
                            }
                        ],
                        addresses: [
                            {
                                addressLine: '23, 5th Cross',
                                city: 'City',
                                state: 'State',
                                country: 'India',
                                pinCode: '560075',
                                addressType: 'Shipping',
                                primary: true
                            }
                        ]},srcLine:2},
                    {contact:{firstName: 'Abc3',
                        customerType:'Customer',
                        lastName: 'xyz3',
                        contactImageURL: 'modules/contacts/img/default.png',
                        companyName: 'Company Name',
                        phones: [
                            {
                                phoneNumber: '0123454789',
                                phoneType: 'Mobile',
                                primary: true
                            }
                        ],
                        emails: [
                            {
                                email: 'email1@mail4.com',
                                emailType: 'Work',
                                primary: true
                            }
                        ],
                        addresses: [
                            {
                                addressLine: '23, 5th Cross',
                                city: 'City',
                                state: 'State',
                                country: 'India',
                                pinCode: '560075',
                                addressType: 'Billing',
                                primary: true
                            },
                            {
                                addressLine: '23, 5th Cross',
                                city: 'City',
                                state: 'State',
                                country: 'India',
                                pinCode: '560075',
                                addressType: 'Shipping',
                                primary: true
                            }
                        ]},srcLine:3}
                ]};

                // Save a new ContactS
                agent.post('/contactsimport')
                    .send(contactsToImport)
                    .set('token', token)
                    .expect(200)
                    .end(function (contactSaveErr, contactSaveRes) {
                        // Handle Contact save error

                        if (contactSaveErr) done(contactSaveErr);
                        contactSaveRes.body.contacts.length.should.equal(3);
                        contactSaveRes.body.contacts[0].contact.phones.length.should.equal(1);
                        contactSaveRes.body.contacts[0].contact.emails.length.should.equal(1);
                        contactSaveRes.body.contacts[0].contact.addresses.length.should.equal(1);
                        contactSaveRes.body.contacts[0].contact.addresses[0].addressType.should.equal('Billing');
                        contactSaveRes.body.contacts[1].contact.addresses[0].addressType.should.equal('Shipping');
                        contactSaveRes.body.contacts[2].contact.addresses[0].addressType.should.equal('Billing');
                        contactSaveRes.body.contacts[2].contact.addresses[1].addressType.should.equal('Shipping');
                        done();
                    });
            });

    });



    it('should be able to update Contact instance if signed in', function(done) {
        agent.post('/auth/signin')
            .send(credentials)
            .expect(200)
            .end(function(signinErr, signinRes) {
                // Handle signin error
                if (signinErr) done(signinErr);

                var token = signinRes.body.token;


                // Save a new Contact
                agent.post('/contacts')
                    .send(customerContact)
                    .set('token', token)
                    .expect(200)
                    .end(function(contactSaveErr, contactSaveRes) {
                        // Handle Contact save error
                        if (contactSaveErr) done(contactSaveErr);

                        // Update Contact name
                        customerContact.firstName = 'first name';

                        // Update existing Contact
                        agent.put('/contacts/' + contactSaveRes.body._id)
                            .send(customerContact)
                            .set('token', token)
                            .expect(200)
                            .end(function(contactUpdateErr, contactUpdateRes) {
                                // Handle Contact update error
                                if (contactUpdateErr) done(contactUpdateErr);

                                // Set assertions
                                (contactUpdateRes.body._id).should.equal(contactSaveRes.body._id);
                                (contactUpdateRes.body.firstName).should.match('first name');

                                // Call the assertion callback
                                done();
                            });
                    });
            });
    });

    it('should not be able to update Contact instance without displayName',function(done){
        var displayName = customerContact.displayName;
        customerContact.displayName='';
        agent.post('/auth/signin')
            .send(credentials)
            .expect(200)
            .end(function(signinErr, signinRes) {
                // Handle signin error
                if (signinErr) done(signinErr);

                var token = signinRes.body.token;


                // Save a new Contact
                agent.post('/contacts')
                    .send(customerContact)
                    .set('token', token)
                    .expect(200)
                    .end(function (contactSaveErr, contactSaveRes) {
                        // Handle Contact save error
                        should.exist(contactSaveErr);
                        contactSaveRes.body.message.should.equal('Display name is required for create contact');
                        customerContact.displayName=displayName;
                        done();
                    });
            });
    });
    it('should not be able to update Contact instance displayName not string',function(done){
        var displayName = customerContact.displayName;
        customerContact.displayName=25;
        agent.post('/auth/signin')
            .send(credentials)
            .expect(200)
            .end(function(signinErr, signinRes) {
                // Handle signin error
                if (signinErr) done(signinErr);

                var token = signinRes.body.token;


                // Save a new Contact
                agent.post('/contacts')
                    .send(customerContact)
                    .set('token', token)
                    .expect(200)
                    .end(function (contactSaveErr, contactSaveRes) {
                        // Handle Contact save error
                        should.exist(contactSaveErr);
                        contactSaveRes.body.message.should.equal('Display name is required as string for create contact');
                        customerContact.displayName=displayName;
                        done();
                    });
            });
    });
    it('should be able to update Contact instance if signed in - update phones, emails and addresses', function(done) {
        agent.post('/auth/signin')
            .send(credentials)
            .expect(200)
            .end(function(signinErr, signinRes) {
                // Handle signin error
                if (signinErr) done(signinErr);

                var token = signinRes.body.token;


                // Save a new Contact
                agent.post('/contacts')
                    .send(customerContact)
                    .set('token', token)
                    .expect(200)
                    .end(function(contactSaveErr, contactSaveRes) {
                        // Handle Contact save error
                        if (contactSaveErr) done(contactSaveErr);

                        // Update Contact name
                        customerContact.firstName = 'first name';

                        customerContact.phones.push({phoneNumber:'08065630100', phoneType:'Work', primary:false});
                        customerContact.emails.push({email:'momo@nvipani.com', emailType:'Work', primary:false});
                        customerContact.addresses.push({
                            addressLine: '302, Ashoka Windows, 6th Cross, 5th Main, Malleshpalya',
                            city: 'Bangalore',
                            state: 'Karnataka',
                            country: 'India',
                            pinCode: '560075',
                            addressType: 'Billing',
                            primary: false
                        });

                        // Update existing Contact
                        agent.put('/contacts/' + contactSaveRes.body._id)
                            .send(customerContact)
                            .set('token', token)
                            .expect(200)
                            .end(function(contactUpdateErr, contactUpdateRes) {
                                // Handle Contact update error
                                if (contactUpdateErr) done(contactUpdateErr);

                                var resContact = contactUpdateRes.body;

                                // Set assertions
                                (resContact.user._id).should.equal(user._id);
                                (resContact.firstName).should.match('first name');
                                (resContact.lastName).should.match('Last Name');
                                (resContact.companyName).should.match('Company Name');

                                (resContact.phones[0].phoneNumber).should.match('0123456789');
                                (resContact.phones[0].phoneType).should.match('Mobile');
                                (resContact.phones[0].primary).should.match(true);

                                (resContact.emails[0].email).should.match('email@mail.com');
                                (resContact.emails[0].emailType).should.match('Work');
                                (resContact.emails[0].primary).should.match(true);


                                (resContact.addresses[0].addressLine).should.match('23, 5th Cross');
                                (resContact.addresses[0].city).should.match('City');
                                (resContact.addresses[0].state).should.match('State');
                                (resContact.addresses[0].country).should.match('India');
                                (resContact.addresses[0].pinCode).should.match('560075');
                                (resContact.addresses[0].addressType).should.match('Billing');
                                (resContact.addresses[0].primary).should.match(true);

                                /*								(resContact.phones[1].phoneNumber).should.match('01234567894');
                                (resContact.phones[1].phoneType).should.match('Home');
                                (resContact.phones[1].primary).should.match(false);

                                (resContact.emails[1].email).should.match('test1@test.com');
                                (resContact.emails[1].emailType).should.match('Work');
                                (resContact.emails[1].primary).should.match(false);

                                //{doorNumber:'1234, 4th Floor', street:'7th Cross, 5th Main', area:'Indiranagar', city:'Bangalore', state:'Karnataka', country:'India', pinCode:'560075', AddressType:'Office'
                                (resContact.addresses[1].addressLine).should.match('1234, 4th Floor, 7th Cross, 5th Main, Indiranagar');
                                (resContact.addresses[1].city).should.match('Bangalore');
                                (resContact.addresses[1].state).should.match('Karnataka');
                                (resContact.addresses[1].country).should.match('India');
                                (resContact.addresses[1].pinCode).should.match('560038');
                                (resContact.addresses[1].addressType).should.match('Office');
                                 (resContact.addresses[1].primary).should.match(false);*/

                                (resContact.phones[1].phoneNumber).should.match('08065630100');
                                (resContact.phones[1].phoneType).should.match('Work');
                                (resContact.phones[1].primary).should.match(false);

                                (resContact.emails[1].email).should.match('momo@nvipani.com');
                                (resContact.emails[1].emailType).should.match('Work');
                                (resContact.emails[1].primary).should.match(false);

                                //{doorNumber:'1234, 4th Floor', street:'7th Cross, 5th Main', area:'Indiranagar', city:'Bangalore', state:'Karnataka', country:'India', pinCode:'560075', AddressType:'Office'
                                (resContact.addresses[1].addressLine).should.match('302, Ashoka Windows, 6th Cross, 5th Main, Malleshpalya');
                                (resContact.addresses[1].city).should.match('Bangalore');
                                (resContact.addresses[1].state).should.match('Karnataka');
                                (resContact.addresses[1].country).should.match('India');
                                (resContact.addresses[1].pinCode).should.match('560075');
                                (resContact.addresses[1].addressType).should.match('Billing');
                                (resContact.addresses[1].primary).should.match(false);

                                resContact.phones[1].phoneNumber = '0877224416';
                                resContact.phones.splice(2, 1);
                                resContact.emails.splice(2, 1);
                                resContact.addresses.splice(1, 1);
                                // Update existing Contact
                                agent.put('/contacts/' + resContact._id)
                                    .send(resContact)
                                    .set('token', token)
                                    .expect(200)
                                    .end(function(contactUpdateErr, contactUpdateRes) {
                                        // Handle Contact update error
                                        if (contactUpdateErr) done(contactUpdateErr);

                                        var secresContact = contactUpdateRes.body;

                                        // Set assertions
                                        (secresContact.user._id).should.equal(user._id);
                                        (secresContact.firstName).should.match('first name');

                                        (secresContact.lastName).should.match('Last Name');
                                        (secresContact.companyName).should.match('Company Name');

                                        (secresContact.phones[0].phoneNumber).should.match('0123456789');
                                        (secresContact.phones[0].phoneType).should.match('Mobile');
                                        (secresContact.phones[0].primary).should.match(true);

                                        (secresContact.emails[0].email).should.match('email@mail.com');
                                        (secresContact.emails[0].emailType).should.match('Work');
                                        (secresContact.emails[0].primary).should.match(true);


                                        (secresContact.addresses[0].addressLine).should.match('23, 5th Cross');
                                        (secresContact.addresses[0].city).should.match('City');
                                        (secresContact.addresses[0].state).should.match('State');
                                        (secresContact.addresses[0].country).should.match('India');
                                        (secresContact.addresses[0].pinCode).should.match('560075');
                                        (secresContact.addresses[0].addressType).should.match('Billing');
                                        (secresContact.addresses[0].primary).should.match(true);

                                        (secresContact.phones[1].phoneNumber).should.match('0877224416');
                                        (secresContact.phones[1].phoneType).should.match('Work');
                                        (secresContact.phones[1].primary).should.match(false);

                                        (secresContact.emails[1].email).should.match('momo@nvipani.com');
                                        (secresContact.emails[1].emailType).should.match('Work');
                                        (secresContact.emails[1].primary).should.match(false);

                                        //{doorNumber:'1234, 4th Floor', street:'7th Cross, 5th Main', area:'Indiranagar', city:'Bangalore', state:'Karnataka', country:'India', pinCode:'560075', AddressType:'Office'
                                        /*(secresContact.addresses[1].addressLine).should.match('1234, 4th Floor, 7th Cross, 5th Main, Indiranagar');
                                        (secresContact.addresses[1].city).should.match('Bangalore');
                                        (secresContact.addresses[1].state).should.match('Karnataka');
                                        (secresContact.addresses[1].country).should.match('India');
                                        (secresContact.addresses[1].pinCode).should.match('560038');
                                        (secresContact.addresses[1].addressType).should.match('Office');
                                        (secresContact.addresses[1].primary).should.match(false);*/

                                        /*(secresContact.phones[2].phoneNumber).should.match('08065630100');
                                        (secresContact.phones[2].phoneType).should.match('Work');
                                        (secresContact.phones[2].primary).should.match(false);*/

                                        /*(secresContact.emails[2].email).should.match('momo@nvipani.com');
                                        (secresContact.emails[2].emailType).should.match('Work');
                                        (secresContact.emails[2].primary).should.match(false);*/

                                        //{doorNumber:'1234, 4th Floor', street:'7th Cross, 5th Main', area:'Indiranagar', city:'Bangalore', state:'Karnataka', country:'India', pinCode:'560075', AddressType:'Office'
                                        /*		(secresContact.addresses[1].addressLine).should.match('302, Ashoka Windows, 6th Cross, 5th Main, Malleshpalya');
                                        (secresContact.addresses[1].city).should.match('Bangalore');
                                        (secresContact.addresses[1].state).should.match('Karnataka');
                                        (secresContact.addresses[1].country).should.match('India');
                                        (secresContact.addresses[1].pinCode).should.match('560075');
                                         (secresContact.addresses[1].addressType).should.match('Billing');
                                         (secresContact.addresses[1].primary).should.match(false);*/

                                        // Call the assertion callback
                                        done();
                                    });
                            });
                    });
            });
    });

    it('should be able to get a list of Contacts if signed in', function(done) {
        agent.post('/auth/signin')
            .send(credentials)
            .expect(200)
            .end(function(signinErr, signinRes) {
                // Handle signin error
                if (signinErr) done(signinErr);

                var token = signinRes.body.token;

                // Save a new Contact
                agent.post('/contacts')
                    .send(customerContact)
                    .set('token', token)
                    .expect(200)
                    .end(function(contactSaveErr, contactSaveRes) {
                        // Handle Contact save error
                        if (contactSaveErr) done(contactSaveErr);
                        request(app).get('/contacts')
                            .set('token', token)
                            .end(function (req, res) {
                                // Set assertion
                                console.log('LENGTH IS:'+res.body.length);
                                res.body.should.be.instanceof(Array).and.have.lengthOf(1);

                                // Call the assertion callback
                                done();
                            });
                    });
            });
    });

    it('should be able to get a single Contact if signed in', function(done) {
        agent.post('/auth/signin')
            .send(credentials)
            .expect(200)
            .end(function(signinErr, signinRes) {
                // Handle signin error
                if (signinErr) done(signinErr);

                var token = signinRes.body.token;


                // Save a new Contact
                agent.post('/contacts')
                    .send(customerContact)
                    .set('token', token)
                    .expect(200)
                    .end(function(contactSaveErr, contactSaveRes) {
                        // Handle Contact save error
                        if (contactSaveErr) done(contactSaveErr);

                        request(app).get('/contacts/' + contactSaveRes.body._id)
                            .set('token', token)
                            .end(function (req, res) {
                                // Set assertion
                                res.body.firstName.should.be.equal(customerContact.firstName);

                                // Call the assertion callback
                                done();
                            });
                    });
            });
    });

    it('should not able to create contact with company user',function (done) {
        customerContact = {
            customerType:'Customer',
            displayName: 'DisplayName',
            emails: [
                {
                    email: 'test1@test.com',
                    emailType: 'Work',
                    primary: true
                }
            ]
        };
        // Save a new Contact
        agent.post('/contacts')
            .send(customerContact)
            .set('token', token)
            .expect(400)
            .end(function (contactSaveErr, contactSaveRes) {
                // Handle Contact save error
                should.not.exist(contactSaveErr);
                should.exist(contactSaveRes);
                contactSaveRes.body.message.should.equal('Not allowed to add the company user as contact');
                done();
            });
    });

    it('should not able to import contact with company user',function (done) {
        var contactsToImport = {
            contacts: [
                {
                    contact: {
                        customerType: 'Customer',
                        firstName: 'Abc1',
                        lastName: 'xyz1',
                        contactImageURL: 'modules/contacts/img/default.png',
                        companyName: 'Company Name',
                        phones: [
                            {
                                phoneNumber: '0123456789',
                                phoneType: 'Mobile',
                                primary: true
                            }
                        ],
                        emails: [
                            {
                                email: 'email1@mail1.com',
                                emailType: 'Work',
                                primary: true
                            }
                        ],
                        addresses: [
                            {
                                addressLine: '23, 5th Cross',
                                city: 'City',
                                state: 'State',
                                country: 'India',
                                pinCode: '560075',
                                addressType: 'Billing',
                                primary: true
                            }

                        ]
                    }, srcLine: 1
                },
                {
                    contact: {
                        customerType: 'Customer',
                        firstName: 'Abc2',
                        lastName: 'xyz2',
                        contactImageURL: 'modules/contacts/img/default.png',
                        companyName: 'Company Name1',
                        emails: [
                            {
                                email: 'test1@test.com',
                                emailType: 'Work',
                                primary: true
                            }
                        ],
                        addresses: [
                            {
                                addressLine: '23, 5th Cross',
                                city: 'City',
                                state: 'State',
                                country: 'India',
                                pinCode: '560075',
                                addressType: 'Shipping',
                                primary: true
                            }
                        ]
                    }, srcLine: 2
                }]
        };

        // Save a new ContactS
        agent.post('/contactsimport')
            .send(contactsToImport)
            .set('token', token)
            .expect(200)
            .end(function (contactSaveErr, contactSaveRes) {
                // Handle Contact save error

                if (contactSaveErr) done(contactSaveErr);
                contactSaveRes.body.contacts.length.should.equal(1);
                contactSaveRes.body.errors.length.should.equal(1);
                contactSaveRes.body.errors[0].message.should.equal('Not allowed to add the company user as contact');
                contactSaveRes.body.contacts[0].contact.phones.length.should.equal(1);
                contactSaveRes.body.contacts[0].contact.emails.length.should.equal(1);
                contactSaveRes.body.contacts[0].contact.addresses.length.should.equal(1);
                contactSaveRes.body.contacts[0].contact.addresses[0].addressType.should.equal('Billing');
                done();
            });
    });

    describe('list contacts and suppliers',function() {
        var contact1,contact2,contact3;
        var contactids=[];
        beforeEach(function (done) {
            //cleanup contacts
            var contactsToImport = {
                contacts: [
                    {
                        contact: {
                            customerType: 'Customer',
                            firstName: 'Abc1',
                            lastName: 'xyz1',
                            contactImageURL: 'modules/contacts/img/default.png',
                            companyName: 'Company Name',
                            phones: [
                                {
                                    phoneNumber: '0123456789',
                                    phoneType: 'Mobile',
                                    primary: true
                                }
                            ],
                            emails: [
                                {
                                    email: 'email1@mail1.com',
                                    emailType: 'Work',
                                    primary: true
                                }
                            ],
                            addresses: [
                                {
                                    addressLine: '23, 5th Cross',
                                    city: 'City',
                                    state: 'State',
                                    country: 'India',
                                    pinCode: '560075',
                                    addressType: 'Billing',
                                    primary: true
                                }

                            ]
                        }, srcLine: 1
                    },
                    {
                        contact: {
                            customerType: 'Customer',
                            firstName: 'Abc2',
                            lastName: 'xyz2',
                            contactImageURL: 'modules/contacts/img/default.png',
                            companyName: 'Company Name1',
                            phones: [
                                {
                                    phoneNumber: '0123356789',
                                    phoneType: 'Mobile',
                                    primary: true
                                }
                            ],
                            emails: [
                                {
                                    email: 'email1@mail2.com',
                                    emailType: 'Work',
                                    primary: true
                                }
                            ],
                            addresses: [
                                {
                                    addressLine: '23, 5th Cross',
                                    city: 'City',
                                    state: 'State',
                                    country: 'India',
                                    pinCode: '560075',
                                    addressType: 'Shipping',
                                    primary: true
                                }
                            ]
                        }, srcLine: 2
                    },
                    {
                        contact: {
                            firstName: 'Abc3',
                            customerType: 'Supplier',
                            lastName: 'xyz3',
                            contactImageURL: 'modules/contacts/img/default.png',
                            companyName: 'Company Name',
                            phones: [
                                {
                                    phoneNumber: '0123454789',
                                    phoneType: 'Mobile',
                                    primary: true
                                }
                            ],
                            emails: [
                                {
                                    email: 'email1@mail4.com',
                                    emailType: 'Work',
                                    primary: true
                                }
                            ],
                            addresses: [
                                {
                                    addressLine: '23, 5th Cross',
                                    city: 'City',
                                    state: 'State',
                                    country: 'India',
                                    pinCode: '560075',
                                    addressType: 'Billing',
                                    primary: true
                                },
                                {
                                    addressLine: '23, 5th Cross',
                                    city: 'City',
                                    state: 'State',
                                    country: 'India',
                                    pinCode: '560075',
                                    addressType: 'Shipping',
                                    primary: true
                                }
                            ]
                        }, srcLine: 3
                    }
                ]
            };

            // Save a new ContactS
            agent.post('/contactsimport')
                .send(contactsToImport)
                .set('token', token)
                .expect(200)
                .end(function (contactSaveErr, contactSaveRes) {
                    // Handle Contact save error

                    if (contactSaveErr) done(contactSaveErr);
                    contactSaveRes.body.contacts.length.should.equal(3);
                    contactids.push(contactSaveRes.body.contacts[0].contact._id);
                    contactids.push(contactSaveRes.body.contacts[1].contact._id);
                    contactids.push(contactSaveRes.body.contacts[2].contact._id);
                    contactSaveRes.body.contacts[0].contact.phones.length.should.equal(1);
                    contactSaveRes.body.contacts[0].contact.emails.length.should.equal(1);
                    contactSaveRes.body.contacts[0].contact.addresses.length.should.equal(1);
                    contactSaveRes.body.contacts[0].contact.addresses[0].addressType.should.equal('Billing');
                    contactSaveRes.body.contacts[1].contact.addresses[0].addressType.should.equal('Shipping');
                    contactSaveRes.body.contacts[2].contact.addresses[0].addressType.should.equal('Billing');
                    contactSaveRes.body.contacts[2].contact.addresses[1].addressType.should.equal('Shipping');
                    var data = {summaryFilters: {NewlyAdded: true}};
                    agent.get('/customercontactssummary?&page=1&limit=10')
                        .send(data)
                        .set('token', token)
                        .end(function (contactSummaryErr, contactSummary) {
                            should.not.exist(contactSummaryErr);
                            should.exist(contactSummary.body);
                            contactSummary.body.NewlyAdded.should.be.instanceof(Array).and.have.lengthOf(2);
                            agent.get('/customercontactssummary')
                                .set('token', token)
                                .end(function (contactSummaryErr, contactSummary) {
                                    should.not.exist(contactSummaryErr);
                                    should.exist(contactSummary.body);
                                    contactSummary.body.NewlyAdded.should.match(2);
                                    var data = {summaryFilters: {NewlyAdded: true}};
                                    agent.get('/suppliercontactssummary?&page=1&limit=10')
                                        .send(data)
                                        .set('token', token)
                                        .end(function (inventorySummaryErr, inventorySummary) {
                                            should.not.exist(inventorySummaryErr);
                                            should.exist(inventorySummary.body);
                                            inventorySummary.body.NewlyAdded.should.be.instanceof(Array).and.have.lengthOf(1);
                                            agent.get('/suppliercontactssummary')
                                                .set('token', token)
                                                .end(function (inventorySummaryErr, inventorySummary) {
                                                    should.not.exist(inventorySummaryErr);
                                                    should.exist(inventorySummary.body);
                                                    inventorySummary.body.NewlyAdded.should.match(1);
                                                    // Call the assertion callback
                                                    done();
                                                });
                                        });
                                });
                        });
                });
        });

        it('should be able to get a list of Customers if signed in', function (done) {

            agent.post('/auth/signin')
                .send(credentials)
                .expect(200)
                .end(function (signinErr, signinRes) {
                    // Handle signin error
                    if (signinErr) done(signinErr);

                    var token = signinRes.body.token;

                    // Save a new Contact
                    agent.get('/contacts/customers')
                        .set('token', token)
                        .expect(200)
                        .end(function (req, res) {
                            // Set assertion
                            //this is to assert that all contacts are by default created as customers
                            res.body.contact.should.be.instanceof(Array).and.have.lengthOf(2);
                            res.body.summaryData.NewlyAdded.should.match(2);

                            // Call the assertion callback
                            done();
                        });
                });
        });

        it('should be able to get a list of Suppliers if signed in', function (done) {

            agent.post('/auth/signin')
                .send(credentials)
                .expect(200)
                .end(function (signinErr, signinRes) {
                    // Handle signin error
                    if (signinErr) done(signinErr);

                    var token = signinRes.body.token;

                    // Save a new Contact
                    agent.get('/contacts/suppliers')
                        .set('token', token)
                        .expect(200)
                        .end(function (req, res) {
                            // Set assertion
                            //this is to assert that all contacts are by default created as customers
                            res.body.contact.should.be.instanceof(Array).and.have.lengthOf(1);
                            res.body.summaryData.NewlyAdded.should.match(1);

                            // Call the assertion callback
                            done();
                        });
                });
        });
        it('should be able to inactivate customers',function(done){

            agent.post('/auth/signin')
                .send(credentials)
                .expect(200)
                .end(function (signinErr, signinRes) {
                    // Handle signin error
                    if (signinErr){
                        done(signinErr);
                    }
                    else {
                        var token = signinRes.body.token;
                        var contacts = {contacts:contactids};
                        //inactivate contacts in array
                        agent.post('/contactsinactive')
                            .set('token', token)
                            .send(contacts)
                            .send({type:'Customer'})
                            .expect(200)
                            .end(function (req, res) {
                                //inactivate the contacts by id
                                //get contacts and assert
                                console.log(res.body);
                                agent.get('/contacts')
                                    .set('token', token)
                                    .expect(200).end(function (fetchErr, fetchRes) {
                                    console.log(fetchRes.body);
                                    fetchRes.body.should.be.instanceof(Array).and.have.lengthOf(3);
                                    var rxContacts = fetchRes.body;
                                    rxContacts.forEach(function(item,index){
                                        if(item.customerType==='Customer') {
                                            item.disabled.should.equal(true);
                                        }
                                    });
                                    var data = {summaryFilters: {Inactive: true}};
                                    agent.get('/customercontactssummary?&page=1&limit=10')
                                        .send(data)
                                        .set('token', token)
                                        .end(function (contactSummaryErr, contactSummary) {
                                            should.not.exist(contactSummaryErr);
                                            should.exist(contactSummary.body);
                                            contactSummary.body.Inactive.should.be.instanceof(Array).and.have.lengthOf(2);
                                            agent.get('/customercontactssummary')
                                                .set('token', token)
                                                .end(function (contactSummaryErr, contactSummary) {
                                                    should.not.exist(contactSummaryErr);
                                                    should.exist(contactSummary.body);
                                                    contactSummary.body.Inactive.should.match(2);
                                                    // Call the assertion callback
                                                    done();
                                                });
                                        });
                                });

                            });

                    }
                });
        });
        it('should be able to activate customer',function(done){

            agent.post('/auth/signin')
                .send(credentials)
                .expect(200)
                .end(function (signinErr, signinRes) {
                    // Handle signin error
                    if (signinErr) done(signinErr);

                    var token = signinRes.body.token;
                    var contacts = {contacts:contactids};
                    // Save a new Contact
                    agent.post('/contactsactive')
                        .set('token', token)
                        .send(contacts)
                        .send({type:'Customer'})
                        .expect(200)
                        .end(function (req, res) {
                            //inactivate the contacts by id
                            //get contacts and assert
                            agent.get('/contacts')
                                .set('token',token)
                                .expect(200).end(function(fetchErr,fetchRes) {
                                fetchRes.body.should.be.instanceof(Array).and.have.lengthOf(3);
                                var rxContacts = fetchRes.body;
                                rxContacts.forEach(function(item,index){
                                    item.disabled.should.equal(false);
                                });
                                agent.get('/customercontactssummary')
                                    .set('token', token)
                                    .end(function (contactSummaryErr, contactSummary) {
                                        should.not.exist(contactSummaryErr);
                                        should.exist(contactSummary.body);
                                        contactSummary.body.Inactive.should.match(0);
                                        // Call the assertion callback
                                        done();
                                    });
                            });

                        });

                });
        });
        it('should be able to remove supplier',function(done){

            agent.post('/auth/signin')
                .send(credentials)
                .expect(200)
                .end(function (signinErr, signinRes) {
                    // Handle signin error
                    if (signinErr) done(signinErr);

                    var token = signinRes.body.token;
                    var contacts = {contacts:contactids};
                    // Save a new Contact
                    agent.post('/contactsremove')
                        .set('token', token)
                        .send(contacts)
                        .send({type:'Supplier'})
                        .expect(200)
                        .end(function (req, res) {
                            //inactivate the contacts by id
                            //get contacts and assert
                            agent.get('/contacts/suppliers')
                                .set('token',token)
                                .expect(200).end(function(fetchErr,fetchRes) {
                                    console.log(fetchRes.body.total_count.should.equal(0));
                                agent.get('/contacts/customers')
                                    .set('token',token)
                                    .expect(200).end(function(fetchErr,fetchRes) {
                                    console.log(fetchRes.body.total_count.should.equal(2));
                                    done();
                                });
                            });

                        });

                });
        });
    });

    afterEach(function (done) {
        Contact.remove().exec();
        done();
    });
    after(function(done) {
         User.remove().exec();
         Company.remove().exec();
         Contact.remove().exec();
        done();
    });
});
