'use strict';

var should = require('should'),
    request = require('supertest'),
    app = require('../../server'),
    mongoose = require('mongoose'),
    User = mongoose.model('User'),
    Company = mongoose.model('Company'),
    Contact = mongoose.model('Contact'),
    PaymentTerm = mongoose.model('PaymentTerm'),
    agent = request.agent(app);

/**
 * Globals
 */
var credentials, user, paymentTerm, paymentTerm1;

/**
 * PaymentTerm routes tests
 */
describe('PaymentTerm CRUD tests', function () {
    before(function (done) {
        var regUser = {
            username: 'test@test.com',
            password: 'password',
            confirmPassword: 'password',
            issendotp: false,
            isverifyotp: false,
            acceptTerms: true
        };
        regUser.issendotp = true;
        agent.post('/user/sendpresignupotp')
            .send(regUser)
            .expect(200)
            .end(function (presignupErr, presignupRes) {
                //console.log("Presignup Response - "+JSON.stringify(presignupRes.message));

                if (presignupErr) done(presignupErr);
                /* user.issendotp=true;
                 user.isverifyotp=true;*/
                else {
                    (presignupRes.body.user.status).should.equal('Register Request');
                    regUser.otp = presignupRes.body.otp;
                    regUser.issendotp = false;
                    regUser.isverifyotp = true;
                    agent.post('/user/sendpresignupotp')
                        .send(regUser)
                        .expect(200)
                        .end(function (presignupErr, verifiedOtp) {
                            //console.log("Presignup Response - "+JSON.stringify(presignupRes.message));
                            if (presignupErr) done(presignupErr);
                            (verifiedOtp.body.user.status).should.equal('Verified');
                            if (verifiedOtp.body.user.username === verifiedOtp.body.user.email) {
                                (verifiedOtp.body.user.emailVerified).should.equal(true);
                            } else if (verifiedOtp.body.username === verifiedOtp.body.mobile) {
                                (verifiedOtp.body.user.mobileVerified).should.equal(true);

                            }
                            //regUser.password = 'password';
                            //regUser.confirmPassword = 'password';
                            regUser.mobile = '0123456789';
                            regUser.issendotp = false;
                            regUser.isverifyotp = false;
                            regUser.ispassword = true;
                            regUser.acceptTerms = true;
                            agent.post('/user/sendpresignupotp')
                                .send(regUser)
                                .expect(200)
                                .end(function (presignupErr, changePassword) {
                                    //console.log("Presignup Response - "+JSON.stringify(presignupRes.message));
                                    if (presignupErr) done(presignupErr);
                                    (changePassword.body.user.status).should.equal('Registered');

                                    User.findOne({
                                        username: regUser.username
                                    }, '-salt -password', function (err, resUser) {
                                        if (err) {
                                            done(err);
                                        }
                                        user = resUser;
                                        if (resUser) {
                                            user = resUser;


                                            done();
                                        }
                                    });
                                });
                        });
                }
            });

        credentials = {
            username: 'test@test.com',
            password: 'password'
        };
    });
    beforeEach(function (done) {
        paymentTerm = {
            name: 'Net 30',
            type: 'NetTerm',
            netTerm: 30,
            discount: 5
        };
        paymentTerm1 = {
            name: '4/8 Net 45',
            type: 'NetTermDiscount',
            netTerm: 45,
            discountTerm: 8,
            termDiscount: 4
        };
        done();
    });

    it('should be able to save PaymentTerm instance if logged in', function (done) {
        agent.post('/auth/signin')
            .send(credentials)
            .expect(200)
            .end(function (signinErr, signinRes) {
                // Handle signin error
                if (signinErr) done(signinErr);

                // Get the token
                var token = signinRes.body.token;

                // Save a new PaymentTerm
                agent.post('/paymentterms')
                    .send(paymentTerm)
                    .set('token', token)
                    .expect(200)
                    .end(function (contactSaveErr, contactSaveRes) {
                        // Handle PaymentTerm save error
                        if (contactSaveErr) done(contactSaveErr);

                        // Get a list of PaymentTerms
                        agent.get('/paymentterms')
                            .set('token', token)
                            .end(function (paymentTermsGetErr, paymentTermsGetRes) {
                                // Handle PaymentTerm save error
                                if (paymentTermsGetErr) done(paymentTermsGetErr);

                                // Get PaymentTerms list
                                var paymentTerms = paymentTermsGetRes.body;

                                // Set assertions
                                (paymentTerms[0].user._id.toString()).should.match(user._id.toString());
                                (paymentTerms[0].name).should.match('Net 30');
                                (paymentTerms[0].netTerm).should.match(30);
                                (paymentTerms[0].type).should.match('NetTerm');
                                (paymentTerms[0].discount).should.match(5);
                                (paymentTerms[0].disabled).should.match(false);
                                (paymentTerms[0].deleted).should.match(false);
                                (paymentTerms[0].lastUpdatedUser.toString()).should.match(user._id.toString());
                                (paymentTerms[0].user._id.toString()).should.match(user._id.toString());

                                // Call the assertion callback
                                done();
                            });
                    });
            });
    });

    it('should not be able to save PaymentTerm instance if not logged in', function (done) {
        agent.post('/paymentterms')
            .send(paymentTerm1)
            .expect(401)
            .end(function (paymentTermSaveErr, paymentTermSaveRes) {
                // Call the assertion callback
                done(paymentTermSaveErr);
            });
    });

    it('should not be able to save PaymentTerm instance if no name is provided', function (done) {
        // Invalidate name field
        paymentTerm.name = '';

        agent.post('/auth/signin')
            .send(credentials)
            .expect(200)
            .end(function (signinErr, signinRes) {
                // Handle signin error
                if (signinErr) done(signinErr);

                var token = signinRes.body.token;


                // Save a new PaymentTerm
                agent.post('/paymentterms')
                    .send(paymentTerm)
                    .set('token', token)
                    .expect(400)
                    .end(function (paymentTermSaveErr, paymentTermSaveRes) {
                        // Set message assertion
                        (paymentTermSaveRes.body.message).should.match('Please fill Payment Term Name\b');

                        // Handle PaymentTerm save error
                        done(paymentTermSaveErr);
                    });
            });
    });

    /* it('should not be able to save PaymentTerm instance if no symbol is provided', function (done) {
         // Invalidate name field
         paymentTerm.symbol = '';

         agent.post('/auth/signin')
             .send(credentials)
             .expect(200)
             .end(function (signinErr, signinRes) {
                 // Handle signin error
                 if (signinErr) done(signinErr);

                 var token = signinRes.body.token;


                 // Save a new PaymentTerm
                 agent.post('/paymentterms')
                     .send(paymentTerm)
                     .set('token', token)
                     .expect(400)
                     .end(function (paymentTermSaveErr, paymentTermSaveRes) {
                         // Set message assertion
                         (paymentTermSaveRes.body.message).should.match('Please fill Unit Of Measure Symbol\b');

                         // Handle PaymentTerm save error
                         done();
                     });
             });
     });
 */
    it('should be able to update PaymentTerm instance if signed in', function (done) {
        agent.post('/auth/signin')
            .send(credentials)
            .expect(200)
            .end(function (signinErr, signinRes) {
                // Handle signin error
                if (signinErr) done(signinErr);

                var token = signinRes.body.token;


                // Save a new PaymentTerm
                agent.post('/paymentterms')
                    .send(paymentTerm)
                    .set('token', token)
                    .expect(200)
                    .end(function (paymentTermSaveErr, paymentTermSaveRes) {
                        // Handle PaymentTerm save error
                        if (paymentTermSaveErr) done(paymentTermSaveErr);

                        // Update PaymentTerm name
                        paymentTerm.name = 'Net 30 New';

                        // Update existing PaymentTerm
                        agent.put('/paymentterms/' + paymentTermSaveRes.body._id)
                            .send(paymentTerm)
                            .set('token', token)
                            .expect(200)
                            .end(function (paymentTermUpdateErr, paymentTermUpdateRes) {
                                // Handle PaymentTerm update error
                                if (paymentTermUpdateErr) done(paymentTermUpdateErr);

                                // Set assertions
                                (paymentTermUpdateRes.body._id).should.equal(paymentTermUpdateRes.body._id);
                                (paymentTermUpdateRes.body.name).should.match('Net 30 New');

                                // Call the assertion callback
                                done();
                            });
                    });
            });
    });

    it('should be able to get a list of PaymentTerms if signed in', function (done) {
        agent.post('/auth/signin')
            .send(credentials)
            .expect(200)
            .end(function (signinErr, signinRes) {
                // Handle signin error
                if (signinErr) done(signinErr);

                var token = signinRes.body.token;

                // Save a new PaymentTerm
                agent.post('/paymentterms')
                    .send(paymentTerm)
                    .set('token', token)
                    .expect(200)
                    .end(function (paymentTermSaveErr, paymentTermSaveRes) {
                        // Handle PaymentTerm save error
                        if (paymentTermSaveErr) done(paymentTermSaveErr);
                        request(app).get('/paymentterms')
                            .set('token', token)
                            .end(function (req, res) {
                                // Set assertion
                                res.body.should.be.instanceof(Array).and.have.lengthOf(1);

                                // Call the assertion callback
                                done();
                            });
                    });
            });
    });


    it('should be able to get a single PaymentTerm if signed in', function (done) {
        agent.post('/auth/signin')
            .send(credentials)
            .expect(200)
            .end(function (signinErr, signinRes) {
                // Handle signin error
                if (signinErr) done(signinErr);

                var token = signinRes.body.token;


                // Save a new PaymentTerm
                agent.post('/paymentterms')
                    .send(paymentTerm)
                    .set('token', token)
                    .expect(200)
                    .end(function (paymentTermSaveErr, paymentTermSaveRes) {
                        // Handle PaymentTerm save error
                        if (paymentTermSaveErr) done(paymentTermSaveErr);

                        request(app).get('/paymentterms/' + paymentTermSaveRes.body._id)
                            .set('token', token)
                            .end(function (req, res) {
                                // Set assertion
                                res.body.should.be.an.Object.with.property('name', paymentTerm.name);

                                // Call the assertion callback
                                done();
                            });
                    });
            });
    });

    it('should be able to delete PaymentTerm instance if signed in', function (done) {
        agent.post('/auth/signin')
            .send(credentials)
            .expect(200)
            .end(function (signinErr, signinRes) {
                // Handle signin error
                if (signinErr) done(signinErr);

                var token = signinRes.body.token;


                // Save a new PaymentTerm
                agent.post('/paymentterms')
                    .send(paymentTerm)
                    .set('token', token)
                    .expect(200)
                    .end(function (paymentTermSaveErr, paymentTermSaveRes) {
                        // Handle PaymentTerm save error
                        if (paymentTermSaveErr) done(paymentTermSaveErr);

                        // Delete existing PaymentTerm
                        agent.delete('/paymentterms/' + paymentTermSaveRes.body._id)
                            .send(paymentTerm)
                            .set('token', token)
                            .expect(200)
                            .end(function (paymentTermDeleteErr, paymentTermDeleteRes) {
                                // Handle PaymentTerm error error
                                if (paymentTermDeleteErr) done(paymentTermDeleteErr);

                                // Set assertions
                                (paymentTermDeleteRes.body._id).should.equal(paymentTermSaveRes.body._id);

                                // Call the assertion callback
                                done();
                            });
                    });
            });
    });

    it('should not be able to delete PaymentTerm instance if not signed in', function (done) {
        // Set PaymentTerm user
        paymentTerm.user = user;

        // Create new PaymentTerm model instance
        var paymentTermObj = new PaymentTerm(paymentTerm);

        // Save the PaymentTerm
        paymentTermObj.save(function () {
            // Try deleting PaymentTerm
            request(app).delete('/paymentterms/' + paymentTermObj._id)
                .expect(401)
                .end(function (paymentTermDeleteErr, paymentTermDeleteRes) {
                    // Set message assertion
                    if (paymentTermDeleteErr) {
                        done(paymentTermDeleteErr);
                    } else {
                        done();
                    }

                });

        });
    });

    afterEach(function (done) {
        /*  User.remove().exec();*/
        //Company.remove().exec();
        PaymentTerm.remove().exec();
        done();
    });
    after(function (done) {

        User.remove().exec();
        Company.remove().exec();
        Contact.remove().exec();
        done();
    });
});
