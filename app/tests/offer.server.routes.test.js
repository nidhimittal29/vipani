'use strict';

var should = require('should'),
    request = require('supertest'),
    app = require('../../server'),
    mongoose = require('mongoose'),
    User = mongoose.model('User'),
    Inventory = mongoose.model('Inventory'),
    Contact = mongoose.model('Contact'),
    Company = mongoose.model('Company'),
    Category = mongoose.model('Category'),
    Product = mongoose.model('Product'),
    Notification = mongoose.model('Notification'),
    BusinessUnit = mongoose.model('BusinessUnit'),
    Offer = mongoose.model('Offer'),
    UnitOfMeasure = mongoose.model('UnitOfMeasure'),
    HsnCodes = mongoose.model('Hsncodes'),
    commonBatchUserUtil=require('./utils/common.batch.users.utils'),
    commonBatchInventoriesUtil=require('./utils/common.batch.inventories.utils'),
    commonBatchInventoryConversionUtil=require('./utils/common.batch.conversion.inventories.utils'),
    commonOfferUtil=require('./utils/common.batch.offers.utils'),
    commonOrderUtil=require('./utils/common.batch.orders.utils'),
    commonContactUtil=require('./utils/common.batch.contacts.utils'),
    commonBatchCompanyUtil=require('./utils/common.batch.company.utils'),
    commonBatchCategoriesUtil=require('./utils/common.batch.categories.utils'),
    testLogger=require('../../lib/log').getLogger('OFFERS', 'DEBUG'),
    TaxGroup = mongoose.model('TaxGroup'),
    agent = request.agent(app);

/**
 * Globals
 */
var credentials, user,currentUser,index=0,contacts, offer, productInventory, inventory, savedContact,businessUnit,currentBusinessUnit,hsnCode,productBrandsLists,products,businessUnitId,offers;

/**
 * Offer routes tests
 */
describe('Offer CRUD tests', function () {

    before(function (done) {
        var allUsers = require('./data/register.global.users');
        commonBatchUserUtil.batchCreate(allUsers, agent, function (userErr, regAllUsers) {
            if (userErr) done(userErr);
            else {
                user=commonBatchUserUtil.getAllUsers()[0];
                done();
            }
        });
    });

    beforeEach(function(done){
        if(!currentUser || (currentUser &&  user._id!==currentUser._id)||(!currentBusinessUnit || businessUnit!==currentBusinessUnit)) {
            commonBatchUserUtil.getUser(currentUser?currentUser:user,agent,function (err,user) {
                var allProducts = require('./data/inventories.global.products');
                var token=user.body.token;
                commonBatchCompanyUtil.getBusinessUnit(currentBusinessUnit?currentBusinessUnit:businessUnit,token, agent, function (defaultBusinessUnitErr, bunit) {
                    if (defaultBusinessUnitErr) {
                        done(defaultBusinessUnitErr);
                    } else {
                        businessUnit =( bunit.body?(bunit.body._id?bunit.body._id:bunit.body):bunit);
                        currentBusinessUnit=businessUnit;
                        allProducts.businessUnit = businessUnit.toString();
                        commonBatchInventoriesUtil.batchCreate(allProducts,index, token, agent, function (productError) {
                            if (productError) {
                                done(productError);
                            } else {
                                products = commonBatchInventoriesUtil.getProducts();
                                productBrandsLists = commonBatchInventoriesUtil.getProductBrands();
                                done();
                            }

                        });
                    }
                });
            });
        }else{
            done();
        }
    });
    it('should not be able to save Offer without offer type if logged in', function (done) {
        commonBatchUserUtil.getUser(user, agent, function (signinErr, signinRes) {
            // Handle signin error
            if (signinErr) done(signinErr);
            else {
                var token = signinRes.body.token;
                offer = {
                    name: 'Coffee first Offer',
                    validTill: new Date(),
                    offerType: 'Buyer',
                    isPublic: true
                };
                var currentToken=signinRes.body.token;
                // Save a new Offer
                commonOfferUtil.addOffer(offer,currentToken,400,agent,function (offerSaveErr, offerSaveRes) {
                    if (offerSaveErr) done(offerSaveErr);

                    offerSaveRes.body.message.should.be.match('Offer Type is required to create offer');
                    done();

                });


            }
        });
    });
    it('should not be able to save Buy offer without product criteria  if logged in', function (done) {
        commonBatchUserUtil.getUser(user, agent, function (signinErr, signinRes) {
            // Handle signin error
            if (signinErr) done(signinErr);
            else {
                var token = signinRes.body.token;
                offer = {
                    name: 'Coffee first Offer',
                    validTill: new Date(),
                    offerType: 'Buy',
                    isPublic: true,
                    products:[]
                };
                var currentToken=signinRes.body.token;
                // Save a new Offer
                commonOfferUtil.addOffer(offer,currentToken,400,agent,function (offerSaveErr, offerSaveRes) {
                    if (offerSaveErr) done(offerSaveErr);

                    offerSaveRes.body.message.should.be.match('At least one product is required to create offer');
                    done();

                });


            }
        });
    });
    it('should not be able to save Sell offer without product criteria  if logged in', function (done) {
        commonBatchUserUtil.getUser(user, agent, function (signinErr, signinRes) {
            // Handle signin error
            if (signinErr) done(signinErr);
            else {
                var token = signinRes.body.token;
                offer = {
                    name: 'Coffee first Offer',
                    validTill: new Date(),
                    offerType: 'Sell',
                    isPublic: true,
                    products:[{}]
                };
                var currentToken=signinRes.body.token;
                // Save a new Offer
                commonOfferUtil.addOffer(offer,currentToken,400,agent,function (offerSaveErr, offerSaveRes) {
                    if (offerSaveErr) done(offerSaveErr);

                    offerSaveRes.body.message.should.be.match('Products are required');
                    done();

                });


            }
        });
    });
    it('should be able to save  Sell offer without validity and check summary if logged in', function (done) {
        commonBatchUserUtil.getUser(user, agent, function (signinErr, signinRes) {
            // Handle signin error
            if (signinErr) done(signinErr);
            else {
                var token = signinRes.body.token;
                commonBatchInventoriesUtil.getInventories(token,businessUnit,agent,function (inventoryErr,inventories) {
                    if (inventoryErr) {
                        done(inventoryErr);
                    } else {
                        inventories = inventories.body.inventory;
                        offer = {
                            name: 'Coffee first Offer without Validity',

                            offerType: 'Sell',
                            isPublic: true,
                            products: [
                                {
                                    inventory: inventories[0]._id,
                                    MOQ: 10,
                                    availableDate: new Date(),
                                    sampleAvailable: true
                                }
                            ]
                        };
                        var currentToken = signinRes.body.token;
                        // Save a new Offer
                        commonOfferUtil.addOffer(offer, currentToken, 200, agent, function (offerSaveErr, offerSaveRes) {
                            if (offerSaveErr) done(offerSaveErr);

                            offerSaveRes.body.offerType.should.match('Sell');
                            offerSaveRes.body.offerStatus.should.match('Opened');
                            should.equal( offerSaveRes.body.validFrom,null);
                            should.equal( offerSaveRes.body.validTill,null);
                            agent.get('/offerssourcesummary?businessUnitId=' + businessUnit)
                                .set('token', token)
                                .expect(200)
                                .end(function (offerSummaryErr, offerSummary) {
                                    should.not.exist(offerSummaryErr);
                                    should.exist(offerSummary.body);
                                    offerSummary.body.New.should.equal(1);
                                    offerSummary.body.Sell.should.equal(1);
                                    offerSummary.body.Buy.should.equal(0);
                                    offerSummary.body.Expired.should.equal(0);
                                    offerSummary.body.Draft.should.equal(0);
                                    var data = {summaryFilters: {New: true}};
                                    agent.get('/offerssourcesummary?businessUnitId=' + businessUnit + '&page=1&limit=10')
                                        .send(data)
                                        .set('token', token)
                                        .end(function (offerSummaryErr, offerSummary) {
                                            should.not.exist(offerSummaryErr);
                                            should.exist(offerSummary.body);
                                            offerSummary.body.New.should.be.instanceof(Array).and.have.lengthOf(1);
                                            done();
                                        });
                                });
                        });

                    }
                });
            }
        });
    });
    it('should be able to save Offer instance with product default Business unit if logged in', function (done) {
        commonBatchUserUtil.getUser(user,agent,function (signinErr, signinRes) {
            // Handle signin error
            if (signinErr) done(signinErr);
            else {
                var token = signinRes.body.token;
                commonBatchInventoriesUtil.getInventories(token,businessUnit,agent,function (inventoryErr,inventories) {
                    if(inventoryErr){
                        done(inventoryErr);
                    }else {
                        inventories=inventories.body.inventory;
                        offer = {
                            name: 'Acid Lemon with 30% margin',
                            validTill: new Date(),
                            offerType: 'Sell',
                            isPublic: true,
                            products: [
                                {
                                    inventory: inventories[0]._id,
                                    MOQ: 10,
                                    availableDate: new Date(),
                                    sampleAvailable: true
                                }
                            ]
                        };
                        // Save a new Offer
                        commonOfferUtil.addOffer(offer,token,200,agent,function (offerSaveErr, offerSaveRes) {
                            // Handle Offer save error
                            if (offerSaveErr) done(offerSaveErr);
                            else {
                                // Get offer By id
                                commonOfferUtil.getOfferById(offerSaveRes.body._id, token, agent, function (offersGetErr, offersGetRes) {
                                    // Handle Offer save error
                                    if (offersGetErr) done(offersGetErr);

                                    // Get Offers list
                                    offers = offersGetRes.body;
                                    // Set assertions
                                    (offers.owner.user).should.equal(user._id.toString());
                                    (offers.name).should.match('Acid Lemon with 30% margin');
                                    (offers.role).should.match('Seller');
                                    (offers.offerType).should.match('Sell');
                                    (offers.offerStatus).should.match('Opened');
                                    (offers.toContacts.contacts).should.be.instanceof(Array).and.have.lengthOf(0);
                                    (offers.owner.businessUnit).should.equal(businessUnit);
                                    (offers.products).should.be.instanceof(Array).and.have.lengthOf(1);
                                    (offers.products[0].inventory._id).should.match(inventories[0]._id);
                                    (offers.products[0].unitIndicativePrice).should.match(0);
                                    (offers.products[0].MOQ).should.match(10);
                                    (offers.products[0].sampleAvailable).should.match(true);
                                    (offers.isPublic).should.match(true);
                                    currentUser=user;
                                    currentBusinessUnit={
                                        isAdd:true,
                                        name: 'Bangalore Godown',
                                        type: 'Stockpoint',
                                        emails: [{email: 'blrgdn@test.com', isPrimary: true, emailType: 'Work'}],
                                        addresses: [{
                                            addressLine: 'address line1',
                                            city: 'bangalore',
                                            state: 'Karnataka',
                                            country: 'India',
                                            pinCode: 560075,
                                            primary: true
                                        }],

                                        disabled: false,
                                        deleted: false,
                                        company: user.company,
                                        user: user._id
                                    };
                                    commonOfferUtil.getOffers(token, businessUnit,agent, function (offersGetErr, offersGetRes) {
                                        if(offersGetErr){
                                            done(offersGetErr);
                                        }else{
                                            should.not.exist(offersGetRes.body.message);
                                            offersGetRes.body.offer.length.should.be.equal(2);
                                            offersGetRes.body.offer[0].name.should.match('Acid Lemon with 30% margin');
                                            index=1;
                                            done();
                                        }
                                    });

                                });

                            }
                        });

                    }

                });
            }
        });
    });
    it('should be able to save Offer instance with out Business unit by using different Business unit products if logged in', function (done) {
        commonBatchUserUtil.getUser(user,agent,function (signinErr, signinRes) {
            // Handle signin error
            if (signinErr) done(signinErr);
            else {
                var token = signinRes.body.token;
                commonBatchInventoriesUtil.getInventories(token,businessUnit,agent,function (inventoryErr,inventories) {
                    if(inventoryErr){
                        done(inventoryErr);
                    }else {
                        inventories=inventories.body.inventory;
                        offer = {
                            name: 'Coffee Business unit 2 first Offer',
                            validTill: new Date(),
                            offerType: 'Sell',
                            isPublic: true,
                            products: [
                                {
                                    inventory: inventories[0]._id,
                                    MOQ: 10,
                                    availableDate: new Date()
                                }
                            ]
                        };
                        // Save a new Offer
                        commonOfferUtil.addOffer(offer,token,400,agent,function (offerSaveErr, offerSaveRes) {
                            // Handle Offer save error
                            if (offerSaveErr) done(offerSaveErr);

                            offerSaveRes.body.message.should.be.match('Invalid Products with Offer Business Unit');

                            offer.businessUnit=businessUnit;
                            // Save a new Offer
                            commonOfferUtil.addOffer(offer,token,200,agent,function (offerSaveErr, offerSaveRes) {
                                // Handle Offer save error
                                if (offerSaveErr) done(offerSaveErr);

                                // Get offer By id
                                commonOfferUtil.getOfferById(offerSaveRes.body._id, token, agent, function (offersGetErr, offersGetRes) {
                                    // Handle Offer save error
                                    if (offersGetErr) done(offersGetErr);

                                    // Get Offers list
                                    offers = offersGetRes.body;
                                    // Set assertions
                                    (offers.owner.user).should.equal(user._id.toString());
                                    (offers.name).should.match('Coffee Business unit 2 first Offer');
                                    (offers.role).should.match('Seller');
                                    (offers.offerType).should.match('Sell');
                                    (offers.offerStatus).should.match('Opened');
                                    (offers.toContacts.contacts).should.be.instanceof(Array).and.have.lengthOf(0);

                                    (offers.owner.businessUnit).should.be.equal(businessUnit);
                                    (offers.products).should.be.instanceof(Array).and.have.lengthOf(1);
                                    (offers.products[0].inventory._id).should.match(inventories[0]._id);
                                    (offers.products[0].unitIndicativePrice).should.match(0);
                                    (offers.products[0].MOQ).should.match(10);
                                    (offers.products[0].sampleAvailable).should.match(false);
                                    (offers.isPublic).should.not.equal(false);
                                    commonOfferUtil.getOffers(token, businessUnit,agent, function (offersGetErr, offersGetRes) {
                                        if(offersGetErr){
                                            done(offersGetErr);
                                        }else{
                                            should.not.exist(offersGetRes.body.message);
                                            offersGetRes.body.offer.length.should.be.equal(1);
                                            offersGetRes.body.offer[0].name.should.match('Coffee Business unit 2 first Offer');
                                            commonOfferUtil.getOffers(token, businessUnit,agent, function (offersGetErr, offersGetRes) {
                                                if(offersGetErr){
                                                    done(offersGetErr);
                                                }else {
                                                    should.not.exist(offersGetRes.body.message);
                                                    offersGetRes.body.offer.length.should.be.equal(1);
                                                    offersGetRes.body.offer[0].name.should.match('Coffee Business unit 2 first Offer');
                                                    done();
                                                }
                                            });
                                        }
                                    });
                                });
                            });


                        });
                    }

                });
            }
        });
    });
    it('should be able to save Offer instance with product new Business unit if logged in', function (done) {
        commonBatchUserUtil.getUser(user,agent,function (signinErr, signinRes) {
            // Handle signin error
            if (signinErr) done(signinErr);
            else {
                var token = signinRes.body.token;
                commonBatchInventoriesUtil.getInventories(token,businessUnit,agent,function (inventoryErr,inventories) {
                    if(inventoryErr){
                        done(inventoryErr);
                    }else {
                        inventories=inventories.body.inventory;
                        offer = {
                            name: 'Coffee Business unit 2 Second Offer',
                            validTill: new Date(),
                            offerType: 'Sell',
                            businessUnit:businessUnit,
                            isPublic: true,
                            products: [
                                {
                                    inventory: inventories[0]._id,
                                    MOQ: 10,
                                    availableDate: new Date()
                                },
                                {
                                    inventory: inventories[1]._id,
                                    MOQ: 10,
                                    availableDate: new Date()
                                }
                            ]
                        };

                        // Save a new Offer
                        commonOfferUtil.addOffer(offer,token,200,agent,function (offerSaveErr, offerSaveRes) {
                            // Handle Offer save error
                            if (offerSaveErr) done(offerSaveErr);

                            // Get offer By id
                            commonOfferUtil.getOfferById(offerSaveRes.body._id, token, agent, function (offersGetErr, offersGetRes) {
                                // Handle Offer save error
                                if (offersGetErr) done(offersGetErr);

                                // Get Offers list
                                var offers = offersGetRes.body;
                                // Set assertions
                                (offers.user._id).should.equal(user._id.toString());
                                (offers.name).should.match('Coffee Business unit 2 Second Offer');
                                (offers.role).should.match('Seller');
                                (offers.offerType).should.match('Sell');
                                (offers.offerStatus).should.match('Opened');
                                (offers.toContacts.contacts).should.be.instanceof(Array).and.have.lengthOf(0);

                                (offers.owner.businessUnit).should.be.equal(businessUnit);
                                (offers.products).should.be.instanceof(Array).and.have.lengthOf(2);
                                (offers.products[0].inventory._id).should.match(inventories[0]._id);
                                (offers.products[0].unitIndicativePrice).should.match(0);
                                (offers.products[0].MOQ).should.match(10);
                                (offers.products[0].sampleAvailable).should.match(false);
                                (offers.isPublic).should.not.equal(false);
                                commonOfferUtil.getOffers(token, businessUnit,agent, function (offersGetErr, offersGetRes) {
                                    if(offersGetErr){
                                        done(offersGetErr);
                                    }else{
                                        should.not.exist(offersGetRes.body.message);
                                        offersGetRes.body.offer.length.should.be.equal(2);
                                        offersGetRes.body.offer[0].name.should.match('Coffee Business unit 2 Second Offer');
                                        commonOfferUtil.getOffers(token, businessUnit,agent, function (offersGetErr, offersGetRes) {
                                            if(offersGetErr){
                                                done(offersGetErr);
                                            }else {
                                                should.not.exist(offersGetRes.body.message);
                                                offersGetRes.body.offer.length.should.be.equal(2);
                                                offersGetRes.body.offer[0].name.should.match('Coffee Business unit 2 Second Offer');
                                                done();
                                            }
                                        });
                                    }
                                });
                            });
                        });

                    }

                });
            }
        });
    });
    it('should be able to get the  Offers from registered contacts', function (done) {
        commonBatchUserUtil.getUser(user,agent,function (signinErr, signinRes) {
            // Handle signin error
            if (signinErr) {
                done(signinErr);
            } else {
                var token = signinRes.body.token;
                contacts=require('./data/contacts.global');
                commonContactUtil.createContact(contacts[0],token,agent,function (contactErr,contactResponse) {
                    should.not.exist(contactResponse.body.message);
                    var singleContact=contactResponse.body;
                    var userName=commonContactUtil.getUserNameByContact(contacts[0]);
                    commonBatchUserUtil.createUser({username:userName,password:'password','registrationCategory': 'Retailer',
                        'selectedSegments': ['Other'],
                        'selectedCategories':['Coffee']},agent,function (contactUserErr,contactUser) {
                        if(contactUserErr){
                            done(contactUserErr);
                        }else {
                            commonBatchUserUtil.getUser({username:userName,password:'password'},agent,function (err,currentUserContact) {
                                var currentToken=currentUserContact.body.token;
                                commonContactUtil.createContact(commonContactUtil.getContactByUserName(user.username), currentToken, agent, function (contactErr, contactResponse) {
                                    commonOfferUtil.getCompanyContactOffers(currentToken, agent, function (inventoryErr, companyContactOffers) {
                                        should.not.exist(companyContactOffers.body.message);
                                        companyContactOffers.body.should.be.instanceof(Array).and.have.lengthOf(1);
                                        companyContactOffers.body[0].offers.should.be.instanceof(Array).and.have.lengthOf(4);
                                        done();

                                    });

                                });
                            });
                        }

                    });

                });

            }
        });

    });
    it('should not be able to save sell Offer instance with product criteria if logged in', function (done) {

        commonBatchUserUtil.getUser(user,agent,function (err,currentUser) {
            offer = {
                name: 'Coffee Business unit 2 with product Criteria',
                validTill: new Date(),
                offerType: 'Sell',
                isPublic: true,
                inventoryItemsClassification:{classificationType:'Criteria',classificationCriteria:{criteriaType:'All'}}
            };
            var token=currentUser.body.token;
            commonBatchCategoriesUtil.findBySubCategories(token,agent,200,function (subCategoryErr,subcategories) {
                if(subCategoryErr){
                    done(subCategoryErr);
                }else {
                    // Save a new Offer
                    commonOfferUtil.addOffer(offer, token, 200, agent, function (offerSaveErr, offerSaveRes) {
                        // Handle Offer save error
                        if (offerSaveErr){
                            done(offerSaveErr);
                        }else {
                            should.not.exist(offerSaveRes.body.message);
                            done();
                        }
                    });
                }
            });
        });

    });
    it('should not be able to save sell Offer instance with product criteria with productBrand if logged in with BB-917', function (done) {

        commonBatchUserUtil.getUser(user,agent,function (err,currentUser) {
            offer = {
                name: 'Coffee Business unit 2 with product Criteria',
                validTill: new Date(),
                offerType: 'Sell',
                isPublic: true,
                inventoryItemsClassification:{classificationType:'Criteria',classificationCriteria:{criteriaType:'Custom',criteria:[]}}
            };
            var token=currentUser.body.token;
            commonBatchInventoriesUtil.getInventories(token,businessUnit,agent,function (inventoryErr,inventories) {
                if (inventoryErr) {
                    done(inventoryErr);
                } else {
                    inventories = inventories.body.inventory;
                    if(inventories && inventories.length>0){

                        offer.inventoryItemsClassification.classificationCriteria.criteria.push({category:inventories[0].productCategory,productBrand:inventories[0].productBrand,unitOfMeasure:inventories[0].unitOfMeasure});
                        /* offer.inventoryItemsClassification.classificationCriteria.criteria.productBrand=inventories[0].productBrand;
                        offer.inventoryItemsClassification.classificationCriteria.criteria.unitOfMeasure=inventories[0].unitOfMeasure;*/
                   /* commonBatchCategoriesUtil.findBySubCategories(token, agent, 200, function (subCategoryErr, subcategories) {
                        if (subCategoryErr) {
                            done(subCategoryErr);
                        } else {*/
                            // Save a new Offer
                            offer.businessUnit=businessUnit;
                            commonOfferUtil.addOffer(offer, token, 200, agent, function (offerSaveErr, offerSaveRes) {
                                // Handle Offer save error
                                if (offerSaveErr) {
                                    done(offerSaveErr);
                                } else {
                                    should.not.exist(offerSaveRes.body.message);
                                    commonOfferUtil.getOfferById(offerSaveRes.body._id,token,agent,function (offerFetchErr,offer) {
                                      if(offerFetchErr){
                                          done(offerFetchErr);
                                      }  else{
                                          offer.body.products.should.be.instanceof(Array).and.have.lengthOf(1);
                                          done();
                                      }
                                    });
                                    done();
                                }
                            });
                       /* }
                    });*/
                }else{
                    done(new Error('No offers'));
                    }
                }
            });
        });

    });
    it('should not be able to save Buy Offer instance without product criteria if logged in', function (done) {
        var userName=commonContactUtil.getUserNameByContact(contacts[0]);
        commonBatchUserUtil.getUser({username:userName,password:'password'},agent,function (err,currentUserContact) {
            offer = {
                name: 'Coffee Business unit 2 first Offer',
                validTill: new Date(),
                offerType: 'Buy',
                isPublic: true,
                products: [
                    {
                        MOQ: 10,
                        availableDate: new Date()
                    }
                ]
            };
            var currentToken=currentUserContact.body.token;
            // Save a new Offer
            commonOfferUtil.addOffer(offer,currentToken,400,agent,function (offerSaveErr, offerSaveRes) {
                // Handle Offer save error
                if (offerSaveErr) done(offerSaveErr);

                offerSaveRes.body.message.should.be.match('Products are required');
                done();
            });


        });

    });
    it('should be able to save Buy Offer instance with product criteria if logged in', function (done) {

        commonBatchUserUtil.getUser(user,agent,function (err,currentUser) {
            offer = {
                name: 'Coffee Business unit 2 with product Criteria',
                offerType: 'Buy',
                isPublic: true,
                inventoryItemsClassification:{classificationType:'Criteria',classificationCriteria:{criteriaType:'All'}}
            };
            var token=currentUser.body.token;
            /*commonBatchCategoriesUtil.findBySubCategories(token,agent,200,function (subCategoryErr,subcategories) {*/
            /* if(subCategoryErr){
                 done(subCategoryErr);
             }else {*/
            // Save a new Offer
            commonOfferUtil.addOffer(offer, token, 200, agent, function (offerSaveErr, offerSaveRes) {
                // Handle Offer save error
                if (offerSaveErr){
                    done(offerSaveErr);
                }else {
                    should.not.exist(offerSaveRes.body.message);
                    var userName=commonContactUtil.getUserNameByContact(contacts[0]);
                    commonBatchUserUtil.getUser({username:userName,password:'password'},agent,function (err,currentUserContact) {

                        var currentToken=currentUserContact.body.token;
                        commonOfferUtil.getCompanyContactOffers(currentUserContact.body.token, agent, function (inventoryErr, companyContactOffers) {
                            should.not.exist(companyContactOffers.body.message);
                            companyContactOffers.body.should.be.instanceof(Array).and.have.lengthOf(1);
                            companyContactOffers.body[0].offers.should.be.instanceof(Array).and.have.lengthOf(7);
                            commonOfferUtil.getOfferById(companyContactOffers.body[0].offers[companyContactOffers.body[0].offers.length-1]._id, currentToken, agent, function (offersGetErr, offersGetRes) {
                                // Handle Offer save error
                                if (offersGetErr) done(offersGetErr);
                                else {
                                    commonBatchCompanyUtil.getBusinessUnit(null, token, agent, function (defaultBusinessUnitErr, bunit) {
                                        if (defaultBusinessUnitErr) {
                                            done(defaultBusinessUnitErr);
                                        } else {
                                            businessUnit = (bunit.body ? (bunit.body._id ? bunit.body._id : bunit.body) : bunit);
                                            var order = {
                                                offerId: offersGetRes.body._id,
                                                businessUnit: businessUnit,
                                                products: [{
                                                    inventory: offersGetRes.body.products[0].inventory._id,
                                                    MOQ: 10,
                                                    numberOfUnits: offersGetRes.body.products[0].inventory.numberOfUnits,
                                                    unitPrice: offersGetRes.body.products[0].inventory.MRP
                                                }]
                                            };

                                            commonOrderUtil.addOrder(order, currentUserContact.body.token, 200, agent, function (errResponse, results) {
                                                var finalOrder = results.body;
                                                should.not.exist(finalOrder.message);
                                                commonOfferUtil.getOfferById(companyContactOffers.body[0].offers[0]._id, currentToken, agent, function (offersGetErr, offersGetRes) {
                                                    // Handle Offer save error
                                                    if (offersGetErr) done(offersGetErr);
                                                    commonBatchCompanyUtil.getBusinessUnit(null, currentToken, agent, function (defaultBusinessUnitErr, bunit) {
                                                        if (defaultBusinessUnitErr) {
                                                            done(defaultBusinessUnitErr);
                                                        } else {
                                                            businessUnit = (bunit.body ? (bunit.body._id ? bunit.body._id : bunit.body) : bunit);
                                                            var order = {
                                                                offerId: offersGetRes.body._id,
                                                                businessUnit: businessUnit,
                                                                products: [{
                                                                    inventory: offersGetRes.body.products[0].inventory._id,
                                                                    MOQ: 10,
                                                                    numberOfUnits: offersGetRes.body.products[0].inventory.numberOfUnits,
                                                                    unitPrice: offersGetRes.body.products[0].inventory.MRP
                                                                }]
                                                            };

                                                            commonOrderUtil.addOrder(order, currentUserContact.body.token, 400, agent, function (errResponse, results) {
                                                               /* var finalOrder = results.body;*/
                                                                should.exist(results.body.message);
                                                                /*finalOrder.products.should.be.instanceof(Array).and.have.lengthOf(0);
                                                                testLogger.debug(finalOrder.seller);
                                                                finalOrder.buyer.contact.displayName.should.equal(offersGetRes.body.user.username);
                                                                finalOrder.seller.contact.displayName.should.equal(userName);*/
                                                                done();
                                                            });
                                                        }
                                                    });
                                                });
                                            });
                                        }
                                    });
                                }
                            });

                        });

                    });
                }
            });
            /*}*/
            /*});*/
        });

    });
    afterEach(function (done) {
        done();
    });

    after(function (done) {
        done();
    });
});
