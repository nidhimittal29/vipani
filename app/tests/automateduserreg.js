'use strict';

var should = require('should'),
    request = require('supertest'),
    app = require('../../server'),
    mongoose = require('mongoose'),
    commonUtils=require('./utils/common.utils'),
    User = mongoose.model('User'),
    Contact = mongoose.model('Contact'),
    Company = mongoose.model('Company'),
    async = require('async'),
    agent = request.agent(app),
    regUsers = require('./data/register.users');

var credentials,user,user2, contact,contactWithoutPhoneOrEmail;

/**
 * Contact routes tests
 */
describe('automated userregistration', function() {
    it('should register user', function (done) {

        var index = 0;
        //var regUser = regUsers[0];
        async.forEachSeries(regUsers, function (regUser, callback) {
            agent.post('/user/sendpresignupotp')
                .send({username: regUser.username, password: 'password', acceptTerms: true,issendotp:true})
                .expect(200)
                .end(function (presignupPasswordErr, presignupPasswordRes) {
                    if (presignupPasswordErr) done(presignupPasswordErr);
                    if (presignupPasswordRes.body.user.status === 'Register Request') {
                        (presignupPasswordRes.body.user.status).should.equal('Register Request');
                       /* console.log("OTP:"+presignupPasswordRes.body.otp);*/
                        if(presignupPasswordRes.body.user.otp) {
                            regUser.otp = presignupPasswordRes.body.user.otp;
                        }
                        if(presignupPasswordRes.body.user.emailOtp) {
                            regUser.otp = presignupPasswordRes.body.user.emailOtp;
                        }
                        regUser.isverifyotp = true;
                        regUser.issendotp=false;
                        agent.post('/user/sendpresignupotp')
                            .send(regUser)
                            .expect(200)
                            .end(function (verifiedOtpErr, verifiedOtpFirstUser) {
                                if (verifiedOtpErr){
                                    done(verifiedOtpErr);
                                }
                                (verifiedOtpFirstUser.body.user.status).should.equal('Verified');

                                regUser.registrationCategories = verifiedOtpFirstUser.body.registrationCategories;
                                regUser.segments = verifiedOtpFirstUser.body.segments;
                                regUser.categories = verifiedOtpFirstUser.body.categories;
                                var selectedSegments = [];
                                //for rice no categories.
                                /*selectedSegments.push({
                                    segment: user1.segments[0]._id,
                                    categories: []
                                });*/
                                regUser.registrationCategories.forEach(function (item, index) {
                                    if (regUser.registrationCategory === item.name) {
                                        regUser.registrationCategory = item._id;
                                    }
                                });
                                regUser.segments.forEach(function (item, index) {
                                    regUser.selectedSegments.forEach(function (selSeg, ix) {
                                        if (selSeg === item.name) {
                                            selectedSegments.push({
                                                segment: regUser.selectedSegments._id,
                                                categories: []
                                            });
                                        }
                                    });
                                });
                                agent.post('/user/sendpresignupotp')
                                    .send({
                                        username: regUser.username,
                                        registrationCategory: regUser.registrationCategory,
                                        ispassword: true,
                                        selectedSegments: selectedSegments
                                    })
                                    .expect(200)
                                    .end(function (sendBusinessSegmentPasswordErr, verifiedSendBusinessSegmentPasswordUser) {
                                        if (sendBusinessSegmentPasswordErr) done(sendBusinessSegmentPasswordErr);
                                        verifiedSendBusinessSegmentPasswordUser.body.status.should.equal(true);
                                        //We need to set proper message for the final stage.
                                        verifiedSendBusinessSegmentPasswordUser.body.message.should.equal('An OTP has been ' + verifiedSendBusinessSegmentPasswordUser.body.user.status + ' for the ' + regUser.username);
                                        user = verifiedSendBusinessSegmentPasswordUser.body.user;
                                        user.status.should.equal('Registered');

                                        index++;
                                        callback();
                                    });
                            });
                    }
                });
        },function (err) {
            if(err){
                done(err);
            }else{
                done();
            }
        });
        }, function (err) {
            should.not.exist(err);
        });

    //uncomment this if you want to remove users.
    /*after(function(done) {
        User.remove().exec();
        Company.remove().exec();
        Contact.remove().exec();
        done();
    });*/
});
