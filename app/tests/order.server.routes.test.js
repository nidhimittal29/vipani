'use strict';

var should = require('should'),
    request = require('supertest'),
    app = require('../../server'),
    mongoose = require('mongoose'),
    User = mongoose.model('User'),
    Inventory = mongoose.model('Inventory'),
    Contact = mongoose.model('Contact'),
    Company = mongoose.model('Company'),
    Category = mongoose.model('Category'),
    Product = mongoose.model('Product'),
    Notification = mongoose.model('Notification'),
    BusinessUnit = mongoose.model('BusinessUnit'),
    Offer = mongoose.model('Offer'),
    UnitOfMeasure = mongoose.model('UnitOfMeasure'),
    HsnCodes = mongoose.model('Hsncodes'),
    TaxGroup = mongoose.model('TaxGroup'),
    Order = mongoose.model('Order'),
    agent = request.agent(app),
    commonBatchUserUtil=require('./utils/common.batch.users.utils');

/**
 * Globals
 */
var credentials, users,user, order,buyOffer,offer, productInventory, inventory, savedContact,businessUnit,hsnCode,productBrand,businessUnitId;
/**
 * Order routes tests
 */
describe('Order CRUD tests', function() {
    before(function (done) {
        var allUsers = require('./data/register.global.users');
        commonBatchUserUtil.batchCreate(allUsers,agent,function (err) {
            if(err){
                done(err);
            }else{
                users=commonBatchUserUtil.getAllUsers();
                businessUnit = {
                    name: 'Bangalore Godown',
                    type: 'Stockpoint',
                    emails: [{email: 'blrgdn@test.com', isPrimary: true, emailType: 'Work'}],
                    addresses: [{
                        addressLine: 'address line1',
                        city: 'bangalore',
                        state: 'Karnataka',
                        country: 'India',
                        pinCode: 560075,
                        primary: true
                    }],

                    disabled: false,
                    deleted: false,
                    company: users[0].company,
                    user: users[0]._id
                };

                done();

            }

        });
    });

    beforeEach(function(done){
        commonBatchUserUtil.getUser(users[0],agent,function (signinErr, signinRes) {
            // Handle signin error
            if (signinErr) done(signinErr);
            else {

                var token = signinRes.body.token;

                var mainCategory = {
                    name: 'Agriculture',
                    code: 'AGR',
                    type: 'MainCategory'
                };

                // Save a new Category
                //category.token = token;
                agent.post('/createMainCategory')
                    .send(mainCategory)
                    .set('Content-Type', 'application/json')
                    .set('token', token)
                    .expect(200)
                    .end(function (categorySaveErr, categorySaveRes) {
                        // Handle Category save error
                        should.not.exist(categorySaveErr);

                        mainCategory = categorySaveRes.body;

                        var subCategory1 = {
                            name: 'Vegetables',
                            code: 'VEG',
                            type: 'SubCategory1',
                            categoryImageURL1: 'modules/categories/img/subcategory1/vegetables.png'
                        };

                        // Save a new Category
                        //category.token = token;
                        agent.post('/createMainCategory')
                            .send(subCategory1)
                            .set('Content-Type', 'application/json')
                            .set('token', token)
                            .expect(200)
                            .end(function (subCategorySaveErr, subCategorySaveRes) {
                                // Handle Category save error
                                should.not.exist(subCategorySaveErr);

                                var subCategory1 = subCategorySaveRes.body;

                                mainCategory.children.push(subCategory1);

                                // Update existing Category
                                agent.put('/createMainCategory/' + mainCategory._id)
                                    .send(mainCategory)
                                    .set('Content-Type', 'application/json')
                                    .set('token', token)
                                    .expect(200)
                                    .end(function (categoryUpdateErr, categoryUpdateRes) {
                                        // Handle Category update error
                                        should.not.exist(categoryUpdateErr);

                                        var subCategory2 = {
                                            name: 'Lemon',
                                            code: 'LEM',
                                            type: 'SubCategory2',
                                            categoryImageURL1: 'modules/categories/img/subcategory2/lemon.png'
                                        };

                                        // Save a new Category
                                        //category.token = token;
                                        agent.post('/createMainCategory')
                                            .send(subCategory2)
                                            .set('Content-Type', 'application/json')
                                            .set('token', token)
                                            .expect(200)
                                            .end(function (subCategory2SaveErr, subCategory2SaveRes) {
                                                // Handle Category save error
                                                should.not.exist(subCategory2SaveErr);

                                                subCategory2 = subCategory2SaveRes.body;

                                                subCategory1.children.push(subCategory2);

                                                // Update existing Category
                                                agent.put('/createMainCategory/' + subCategory1._id)
                                                    .send(subCategory1)
                                                    .set('Content-Type', 'application/json')
                                                    .set('token', token)
                                                    .expect(200)
                                                    .end(function (subCategoryUpdateErr, subCategoryUpdateRes) {
                                                        // Handle Category update error
                                                        if (subCategoryUpdateErr) done(subCategoryUpdateErr);
                                                        else {
                                                            var bagId;
                                                            var kgsId;
                                                            UnitOfMeasure.findOne({
                                                                name: 'Bags'
                                                            }, '_id name', function (err, uom) {
                                                                bagId = uom._id;
                                                                UnitOfMeasure.findOne({
                                                                    name: 'Kilograms'
                                                                }, '_id name', function (err, uom) {
                                                                    kgsId = uom._id;
                                                                    var unitofmeasure = new UnitOfMeasure({
                                                                        name: 'Bag of 25kgs',
                                                                        quantityType: 'Measure',
                                                                        symbol: '25kgsBag',
                                                                        type: 'Compound',
                                                                        firstUnitOfMeasure: bagId,
                                                                        conversion: 25,
                                                                        secondUnitOfMeasure: kgsId
                                                                    });
                                                                    unitofmeasure.save(function (err, model) {
                                                                        should.not.exist(err);
                                                                        hsnCode = {
                                                                            name: 'TOASTERS',
                                                                            chapterCode: 85,
                                                                            hsncode: '851672',
                                                                            disabled: false,
                                                                            deleted: false
                                                                        };
                                                                        productBrand = {
                                                                            name: 'SuryaTeja',
                                                                            productCategory: subCategory2._id,
                                                                            uom: model._id
                                                                        };
                                                                        productInventory = {
                                                                            /*product: {
                                                                                name: 'Acid Lemon',
                                                                                category: mainCategory._id,
                                                                                subCategory1: subCategory1._id,
                                                                                subCategory2: subCategory2._id,
                                                                            },
                                                                            inventory: {*/
                                                                            subcategory2: subCategory2._id,
                                                                            subcategory1: subCategory1._id,
                                                                            category: mainCategory._id,
                                                                            unitSize: 50,
                                                                            unitOfMeasure: model._id,
                                                                            MRP: 5000.23,
                                                                            numberOfUnits: 1000,
                                                                            manufacturingDate: new Date(),
                                                                            packagingDate: new Date(),
                                                                            moqAndMargin: [{
                                                                                MOQ: '25',
                                                                                margin: '.1'
                                                                            }],
                                                                            moqAndBuyMargin: [{
                                                                                MOQ: '50',
                                                                                margin: '.5'
                                                                            }],
                                                                            movAndMargin: [{
                                                                                MOV: '10000',
                                                                                margin: '.5'
                                                                            }],
                                                                            movAndBuyMargin: [{
                                                                                MOV: '25000',
                                                                                margin: '1'
                                                                            }],
                                                                            //}
                                                                        };
                                                                        agent.post('/branches')
                                                                            .send(businessUnit)
                                                                            .set('token', token)
                                                                            .expect(200)
                                                                            .end(function (err, res) {

                                                                                should.not.exist(err);
                                                                                businessUnitId = res.body._id;
                                                                                // Save a new Inventory
                                                                                HsnCodes.findOne({name: 'TOASTERS'}, function (err, hsnCode) {
                                                                                    should.not.exist(err);
                                                                                    productBrand.hsncode = hsnCode._id;
                                                                                    TaxGroup.findOne({name: 'GST 3%'}, function (err, taxGroup) {

                                                                                        productBrand.taxGroup = taxGroup._id;
                                                                                        agent.post('/productBrand')
                                                                                            .send(productBrand)
                                                                                            .set('token', token)
                                                                                            .expect(200)
                                                                                            .end(function (err, resp) {
                                                                                                should.not.exist(err);
                                                                                                agent.post('/inventories/createInventoryMaster')
                                                                                                    .send({
                                                                                                        inventory: productInventory,
                                                                                                        businessUnit: businessUnitId,
                                                                                                        productBrand: resp.body._id
                                                                                                    })
                                                                                                    .set('Content-Type', 'application/json')
                                                                                                    .set('token', token)
                                                                                                    .expect(200)
                                                                                                    .end(function (inventorySaveErr, inventorySaveRes) {
                                                                                                        // Handle Inventory save error
                                                                                                        should.not.exist(err);

                                                                                                        // Get a list of Inventories
                                                                                                        agent.get('/busUnitInventories/' + businessUnitId)
                                                                                                            .set('Content-Type', 'application/json')
                                                                                                            .set('token', token)
                                                                                                            .end(function (inventoriesGetErr, inventoriesGetRes) {
                                                                                                                // Handle Inventory save error
                                                                                                                if (inventoriesGetErr) done(inventoriesGetErr);

                                                                                                                // Get Inventories list
                                                                                                                var inventories = inventoriesGetRes.body;

                                                                                                                // Set assertions
                                                                                                                //(inventories.inventory[0].user._id).should.equal(user.id);
                                                                                                                (inventories.inventories[0].unitOfMeasure.toString()).should.equal(productInventory.unitOfMeasure.toString());
                                                                                                                inventories.inventories[0].numberOfUnits.should.equal(productInventory.numberOfUnits);
                                                                                                                (inventories.inventories[0].MRP).should.equal(productInventory.MRP);
                                                                                                                should.exist(inventories.inventories[0].productBrand);
                                                                                                                should.exist(inventories.inventories[0].stockMasters);
                                                                                                                inventories.inventories[0].stockMasters.length.should.equal(1);
                                                                                                                inventories.inventories[0].taxGroupName.should.equal('GST 3%');
                                                                                                                inventories.inventories[0].user.should.equal(users[0]._id);
                                                                                                                inventory = inventories.inventories[0];
                                                                                                                BusinessUnit.findOne({_id: inventories.businessUnit}, function (err, bu) {
                                                                                                                    should.not.exist(err);
                                                                                                                    bu.inventories.length.should.equal(1);
                                                                                                                    bu.inventories[0].inventory.toString().should.equal(inventories.inventories[0]._id.toString());

                                                                                                                    var savContact = {
                                                                                                                        firstName: 'First Name',
                                                                                                                        lastName: 'Last Name',
                                                                                                                        contactImageURL: 'modules/contacts/img/default.png',
                                                                                                                        companyName: 'Company Name',
                                                                                                                        phones: [
                                                                                                                            {
                                                                                                                                phoneNumber: '0123456789',
                                                                                                                                phoneType: 'Mobile',
                                                                                                                                primary: true
                                                                                                                            }
                                                                                                                        ],
                                                                                                                        emails: [
                                                                                                                            {
                                                                                                                                email: 'email@mail.com',
                                                                                                                                emailType: 'Work',
                                                                                                                                primary: true
                                                                                                                            }
                                                                                                                        ],
                                                                                                                        addresses: [
                                                                                                                            {
                                                                                                                                addressLine: '23, 5th Cross',
                                                                                                                                city: 'City',
                                                                                                                                state: 'State',
                                                                                                                                country: 'India',
                                                                                                                                pinCode: '560075',
                                                                                                                                addressType: 'Billing',
                                                                                                                                primary: true
                                                                                                                            }
                                                                                                                        ]
                                                                                                                    };
                                                                                                                    // Save a new Contact
                                                                                                                    agent.post('/contacts')
                                                                                                                        .send(savContact)
                                                                                                                        .set('token', token)
                                                                                                                        .expect(200)
                                                                                                                        .end(function (contactSaveErr, contactSaveRes) {
                                                                                                                            // Handle Contact save error
                                                                                                                            should.not.exist(err);

                                                                                                                            savedContact = contactSaveRes.body;

                                                                                                                            var validTill = new Date();
                                                                                                                            validTill.setMonth(validTill.getMonth() + 1);

                                                                                                                            offer = new Offer({
                                                                                                                                name: 'Acid Lemon with 30% margin',
                                                                                                                                validTill: validTill,
                                                                                                                                toContacts: {
                                                                                                                                    contacts: [
                                                                                                                                        {
                                                                                                                                            contact: savedContact._id
                                                                                                                                        }
                                                                                                                                    ]
                                                                                                                                },
                                                                                                                                role: 'Seller',
                                                                                                                                offerType: 'Sell',
                                                                                                                                offerStatus: 'Placed',
                                                                                                                                isPublic: true,
                                                                                                                                businessUnit: businessUnitId,
                                                                                                                                products: [
                                                                                                                                    {
                                                                                                                                        inventory: inventory._id,
                                                                                                                                        category: mainCategory._id,
                                                                                                                                        subCategory1: subCategory1._id,
                                                                                                                                        subCategory2: subCategory2._id,
                                                                                                                                        unitIndicativePrice: 3500,
                                                                                                                                        numberOfUnitsAvailable: 500,
                                                                                                                                        MOQ: 10,
                                                                                                                                        margin: 30,
                                                                                                                                        availableDate: new Date(),
                                                                                                                                        sampleAvailable: true
                                                                                                                                    }
                                                                                                                                ],
                                                                                                                            });
                                                                                                                            agent.post('/offers')
                                                                                                                                .send(offer)
                                                                                                                                .set('token', token)
                                                                                                                                .expect(200)
                                                                                                                                .end(function (err, offerRes) {
                                                                                                                                    should.not.exist(err);

                                                                                                                                    var validTill = new Date();
                                                                                                                                    validTill.setMonth(validTill.getMonth() + 1);

                                                                                                                                     buyOffer = new Offer({
                                                                                                                                        name: 'Acid Lemon with 50% margin',
                                                                                                                                        validTill: validTill,
                                                                                                                                        toContacts: {
                                                                                                                                            contacts: [
                                                                                                                                                {
                                                                                                                                                    contact: savedContact._id
                                                                                                                                                }
                                                                                                                                            ]
                                                                                                                                        },
                                                                                                                                        offerType: 'Buy',
                                                                                                                                        offerStatus: 'Placed',
                                                                                                                                        isPublic: true,
                                                                                                                                        businessUnit: businessUnitId,
                                                                                                                                        products: [
                                                                                                                                            {
                                                                                                                                                inventory: inventory._id,
                                                                                                                                                category: mainCategory._id,
                                                                                                                                                subCategory1: subCategory1._id,
                                                                                                                                                subCategory2: subCategory2._id,
                                                                                                                                                unitIndicativePrice: 3500,
                                                                                                                                                numberOfUnitsAvailable: 500,
                                                                                                                                                MOQ: 10,
                                                                                                                                                margin: 30,
                                                                                                                                                availableDate: new Date(),
                                                                                                                                                sampleAvailable: true
                                                                                                                                            }
                                                                                                                                        ],
                                                                                                                                    });
                                                                                                                                    order = {

                                                                                                                                        type: 'Buy',
                                                                                                                                        billingAddress:
                                                                                                                                            {
                                                                                                                                                addressLine: 'asdfsda',
                                                                                                                                                city: 'asdf',
                                                                                                                                                state: '3weerwe',
                                                                                                                                                country: 'india',
                                                                                                                                                pinCode: '454543'
                                                                                                                                            },
                                                                                                                                        buyer:
                                                                                                                                            {contact: savedContact._id},
                                                                                                                                        currentStatus:
                                                                                                                                            'Drafted',
                                                                                                                                        deleted: false,
                                                                                                                                        disabled: false,
                                                                                                                                        isAddress: true,
                                                                                                                                        offer: offerRes.body._id,
                                                                                                                                        orderNumber: '15022018-3',
                                                                                                                                        orderType: 'Sale',
                                                                                                                                        products: [
                                                                                                                                            {
                                                                                                                                                inventory: {_id: inventory._id},
                                                                                                                                                MOQ: 10,
                                                                                                                                                numberOfUnits: 10,
                                                                                                                                                selected: true,
                                                                                                                                            }],

                                                                                                                                        shippingAddress:
                                                                                                                                            {
                                                                                                                                                addressLine: 'asdfsda',
                                                                                                                                                city: 'asdf',
                                                                                                                                                state: '3weerwe',
                                                                                                                                                country: 'india',
                                                                                                                                                pinCode: '454543'
                                                                                                                                            },
                                                                                                                                    };
                                                                                                                                    agent.post('/offers')
                                                                                                                                        .send(buyOffer)
                                                                                                                                        .set('token', token)
                                                                                                                                        .expect(200)
                                                                                                                                        .end(function (err, buyofferRes) {
                                                                                                                                            should.not.exist(err);
                                                                                                                                            buyOffer=buyofferRes.body;

                                                                                                                                        done();
                                                                                                                                        });
                                                                                                                                });

                                                                                                                        });
                                                                                                                });
                                                                                                            });
                                                                                                    });
                                                                                            });
                                                                                    });
                                                                                });
                                                                            });
                                                                    });
                                                                });
                                                            });

                                                        }
                                                    });
                                            });
                                    });
                            });
                    });
            }
        });
    });
    it('should be able to save Order instance if logged in', function(done) {
        commonBatchUserUtil.getUser(users[0],agent,function (signinErr, signinRes) {
            // Handle signin error
            if (signinErr) done(signinErr);
            else {
                var token = signinRes.body.token;

                offer.products[0].selected = true;
                // Save a new Order
                agent.get('/createOrder/' + offer._id)
                    .send({offer:order,businessUnit:businessUnit._id})
                    .set('Content-Type', 'application/json')
                    .set('token', token)
                    .expect(200)
                    .end(function (orderSaveErr, orderSaveRes) {

                        if (orderSaveErr) done(orderSaveErr);

                        var order = orderSaveRes.body;
                        // Set assertions
                        (order.user._id).should.equal(users[0]._id);
                        (order.offer._id.toString()).should.match(offer._id.toString());
                        (order.orderType).should.match('Sale');
                        //(order.seller.nVipaniUser).should.match(user._id);
                        (order.currentStatus).should.match('Drafted');
                        (order.products).should.be.instanceof(Array).and.have.lengthOf(1);
                        (order.products[0].inventory._id).should.match(inventory._id);
                        (order.products[0].numberOfUnits).should.match(10);
                        /*(order.products[0].unitPrice).should.match(3500);*/
                        (order.products[0].MOQ).should.match(10);
                        done();
                        // Assert Updated Inventory after confirm order.
                        /*agent.get('/inventories/' + inventory._id)
                            .set('Content-Type', 'application/json')
                            .set('token', token)
                            .expect(200)
                            .end(function (inventorySaveErr, inventoryRes) {
                                // Handle Inventory save error
                                if (inventorySaveErr) done(inventorySaveErr);

                                var updatedInventory = inventoryRes.body;

                                (offer.products[0].inventory._id).should.match(updatedInventory._id);
                                (updatedInventory.unitSize).should.match(inventory.unitSize);
                                (updatedInventory.unitMeasure).should.match(inventory.unitMeasure);
                                (updatedInventory.unitPrice).should.match(inventory.unitPrice);
                                (updatedInventory.numberOfUnits).should.match(inventory.numberOfUnits);/!*
                                (updatedInventory.offerUnits).should.match(offer.products[0].numberOfUnitsAvailable);*!/
                                updatedInventory.offerUnitsHistory.should.be.instanceof(Array).and.have.lengthOf(1);
                                (updatedInventory.offerUnitsHistory[0].offer).should.match(offer._id);
                                (updatedInventory.offerUnitsHistory[0].changeUnits).should.match(offer.products[0].numberOfUnitsAvailable);
                                (updatedInventory.offerUnitsHistory[0].changeType).should.match('OfferCreation');
                                // Call the assertion callback
                                done();
                            });*/
                    });
            }
        });
    });
    it('should be able to save Order with different users instance if logged in', function(done) {
        commonBatchUserUtil.getUser(users[1],agent,function (signinErr, signinRes) {
            // Handle signin error
            if (signinErr) done(signinErr);
            else {
                var token = signinRes.body.token;

                offer.products[0].selected = true;
                // Save a new Order
                agent.get('/createOrder/' + offer._id)
                    .send(order)
                    .set('Content-Type', 'application/json')
                    .set('token', token)
                    .expect(200)
                    .end(function (orderSaveErr, orderSaveRes) {

                        if (orderSaveErr) done(orderSaveErr);

                        var order = orderSaveRes.body;
                        // Set assertions
                        (order.user._id).should.equal(users[1]._id);
                        (order.offer._id.toString()).should.match(offer._id.toString());
                        (order.orderType).should.match('Purchase');
                        //(order.seller.nVipaniUser).should.match(user._id);
                        (order.currentStatus).should.match('Drafted');
                        (order.seller.nVipaniUser.should.equal(users[0]._id));
                        (order.buyer.nVipaniUser.should.equal(users[1]._id));
                        (order.products).should.be.instanceof(Array).and.have.lengthOf(1);
                        (order.products[0].inventory._id).should.match(inventory._id);
                        (order.products[0].numberOfUnits).should.match(10);
                        /*(order.products[0].unitPrice).should.match(3500);*/
                        (order.products[0].MOQ).should.match(10);
                        done();
                        // Assert Updated Inventory after confirm order.
                        /*agent.get('/inventories/' + inventory._id)
                            .set('Content-Type', 'application/json')
                            .set('token', token)
                            .expect(200)
                            .end(function (inventorySaveErr, inventoryRes) {
                                // Handle Inventory save error
                                if (inventorySaveErr) done(inventorySaveErr);

                                var updatedInventory = inventoryRes.body;

                                (offer.products[0].inventory._id).should.match(updatedInventory._id);
                                (updatedInventory.unitSize).should.match(inventory.unitSize);
                                (updatedInventory.unitMeasure).should.match(inventory.unitMeasure);
                                (updatedInventory.unitPrice).should.match(inventory.unitPrice);
                                (updatedInventory.numberOfUnits).should.match(inventory.numberOfUnits);/!*
                                (updatedInventory.offerUnits).should.match(offer.products[0].numberOfUnitsAvailable);*!/
                                updatedInventory.offerUnitsHistory.should.be.instanceof(Array).and.have.lengthOf(1);
                                (updatedInventory.offerUnitsHistory[0].offer).should.match(offer._id);
                                (updatedInventory.offerUnitsHistory[0].changeUnits).should.match(offer.products[0].numberOfUnitsAvailable);
                                (updatedInventory.offerUnitsHistory[0].changeType).should.match('OfferCreation');
                                // Call the assertion callback
                                done();
                            });*/
                    });
            }
        });
    });
    it('should be able to list orders instance if logged in', function(done) {
        commonBatchUserUtil.getUser(users[0],agent,function (signinErr, signinRes) {
            // Handle signin error
            if (signinErr) done(signinErr);
            else {

                var token = signinRes.body.token;

                offer.products[0].selected = true;
                // Save a new Order
                agent.put('/createOrder/' + offer._id)
                    .set('Content-Type', 'application/json')
                    .send(order)
                    .set('token', token)
                    .expect(200)
                    .end(function (orderSaveErr, orderSaveRes) {

                        if (orderSaveErr) done(orderSaveErr);

                        agent.get('/orders/?businessUnitId=' + businessUnitId)
                            .set('token', token)
                            .expect(200)

                            .end(function (err, list) {
                                should.not.exist(err);
                                list.body.length.should.equal(1);


                                done();
                            });
                    });
            }
        });
    });


    it('should not be able to save Order instance if not logged in', function(done) {
        agent.post('/orders')
            .send(order)
            .expect(401)
            .end(function(orderSaveErr, orderSaveRes) {
                // Call the assertion callback
                done(orderSaveErr);
            });
    });
    it('should not  list orders instance if a different businessUnitID is provided', function(done) {
        commonBatchUserUtil.getUser(users[0],agent,function (signinErr, signinRes) {
            // Handle signin error
            if (signinErr) done(signinErr);
            else {

                var token = signinRes.body.token;

                offer.products[0].selected = true;
                // Save a new Order
                agent.put('/createOrder/' + offer._id)
                    .set('Content-Type', 'application/json')
                    .send(order)
                    .set('token', token)
                    .expect(200)
                    .end(function (orderSaveErr, orderSaveRes) {

                        if (orderSaveErr) done(orderSaveErr);
                        businessUnit = {
                            name: 'Bangalore Godown',
                            type: 'Stockpoint',
                            emails: [{email: 'blrgdn@test.com', isPrimary: true, emailType: 'Work'}],
                            addresses: [{
                                addressLine: 'address line1',
                                city: 'bangalore',
                                state: 'Karnataka',
                                country: 'India',
                                pinCode: 560075,
                                primary: true
                            }],

                            disabled: false,
                            deleted: false,
                            company: users[0].company,
                            user: users[0]._id
                        };

                        agent.post('/branches')
                            .set('token', token)
                            .send(businessUnit)
                            .expect(200)
                            .end(function (buErr, busUnit) {
                                should.not.exist(buErr);
                                agent.get('/orders/?businessUnitId=' + busUnit.body._id)
                                    .set('token', token)
                                    .expect(200)

                                    .end(function (err, list) {
                                        should.not.exist(err);
                                        list.body.length.should.equal(1);


                                        done();
                                    });
                            });


                    });
            }
        });
    });
    it('should be able to save Order with same user of buy Offer view instance if logged in', function(done) {
        commonBatchUserUtil.getUser(users[0],agent,function (signinErr, signinRes) {
            // Handle signin error
            if (signinErr) done(signinErr);
            else {
                var token = signinRes.body.token;

                buyOffer.products[0].selected = true;
                // Save a new Order
                agent.get('/createOrder/' + buyOffer._id)
                    .send(order)
                    .set('Content-Type', 'application/json')
                    .set('token', token)
                    .expect(200)
                    .end(function (orderSaveErr, orderSaveRes) {

                        if (orderSaveErr) done(orderSaveErr);

                        var order = orderSaveRes.body;
                        // Set assertions
                        (order.user._id).should.equal(users[0]._id);
                        (order.offer._id.toString()).should.match(buyOffer._id.toString());
                        (order.orderType).should.match('Purchase');
                        //(order.seller.nVipaniUser).should.match(user._id);
                        (order.currentStatus).should.match('Drafted');
                        (order.buyer.nVipaniUser.should.equal(users[0]._id));
                       /* (order.seller.nVipaniUser.should.equal(users[1]._id));*/
                        (order.products).should.be.instanceof(Array).and.have.lengthOf(0);
                        /*(order.products[0].inventory._id).should.match(inventory._id);
                        (order.products[0].numberOfUnits).should.match(10);
                        /!*(order.products[0].unitPrice).should.match(3500);*!/
                        (order.products[0].MOQ).should.match(10);*/
                        done();

                    });
            }
        });
    });

    it('should be able to save Order with differ user of buy Offer view instance if logged in', function(done) {
        commonBatchUserUtil.getUser(users[1],agent,function (signinErr, signinRes) {
            // Handle signin error
            if (signinErr) done(signinErr);
            else {
                var token = signinRes.body.token;

                buyOffer.products[0].selected = true;
                // Save a new Order
                agent.get('/createOrder/' + buyOffer._id)
                    .send(order)
                    .set('Content-Type', 'application/json')
                    .set('token', token)
                    .expect(200)
                    .end(function (orderSaveErr, orderSaveRes) {

                        if (orderSaveErr) done(orderSaveErr);

                        var order = orderSaveRes.body;
                        // Set assertions
                        (order.user._id).should.equal(users[1]._id);
                        (order.offer._id.toString()).should.match(buyOffer._id.toString());
                        (order.orderType).should.match('Sale');
                        //(order.seller.nVipaniUser).should.match(user._id);
                        (order.currentStatus).should.match('Drafted');
                        (order.buyer.nVipaniUser.should.equal(users[0]._id));
                        (order.seller.nVipaniUser.should.equal(users[1]._id));
                        (order.products).should.be.instanceof(Array).and.have.lengthOf(0);

                        done();

                    });
            }
        });
    });

    afterEach(function (done) {
        Category.remove().exec();
        Product.remove().exec();
        Inventory.remove().exec();
        Notification.remove().exec();
        Offer.remove().exec();
        Order.remove().exec();
        done();
    });

    after(function (done) {
        User.remove().exec();
        Company.remove().exec();
        BusinessUnit.remove().exec();
        Contact.remove().exec();
        Category.remove().exec();
        Product.remove().exec();
        Inventory.remove().exec();
        Notification.remove().exec();
        Offer.remove().exec();
        done();
    });
});
