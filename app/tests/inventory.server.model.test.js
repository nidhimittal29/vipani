'use strict';

/**
 * Module dependencies.
 */
var should = require('should'),
    mongoose = require('mongoose'),
    app = require('../../server'),
    User = mongoose.model('User'),
    Inventory = mongoose.model('Inventory'),
    UnitOfMeasure = mongoose.model('UnitOfMeasure');

/**
 * Globals
 */
var user, inventory,uom;

/**
 * Unit tests
 */
describe('Inventory Model Unit Tests:', function () {
    beforeEach(function (done) {
        user = new User({
            firstName: 'Full',
            lastName: 'Name',
            displayName: 'Full Name',
            email: 'test@test.com',
            username: 'username',
            password: 'password'
        });

        user.save(function () {
            var bagId;
            var kgsId;
            UnitOfMeasure.findOne({
                name:'Bags'
            },'_id name',function(err,uom){
                bagId = uom._id;
                UnitOfMeasure.findOne({
                    name:'Kilograms'
                },'_id name',function(err,uom){
                    kgsId = uom._id;
                    var unitofmeasure = new UnitOfMeasure({name:'Bag of 25kgs',quantityType:'Measure',symbol:'25kgsBag',type:'Compound',firstUnitOfMeasure:bagId,conversion:25,secondUnitOfMeasure:kgsId});
                    unitofmeasure.save(function(err,model){
                        console.log(err);
                        should.not.exist(err);
                        inventory = new Inventory({
                            name: 'Inventory Name',
                            user: user,
                            unitOfMeasure:model._id
                        });
                        done();
                    });
                });
            });
        });
    });


    //TODO save inventory
    //save inventory with invalid hsncode
    //save inventory with invalid brand


    describe('Method Save', function () {
        it('should be able to save without problems', function (done) {
            return inventory.save(function (err) {
                should.not.exist(err);
                done();
            });
        });

        it('should be able to show an error when try to save without uom', function (done) {
            inventory.unitOfMeasure = '';

            return inventory.save(function (err) {
                should.exist(err);
                done();
            });
        });
        it('should be able to show an error when try to save with invalid hsncode', function (done) {
            inventory.unitOfMeasure = '';
            return inventory.save(function (err) {
                should.exist(err);
                done();
            });
        });
     });

    afterEach(function (done) {
        Inventory.remove().exec();
        User.remove().exec();

        done();
    });
});
