'use strict';

/**
 * Module dependencies.
 */
var should = require('should'),
    mongoose = require('mongoose'),
    User = mongoose.model('User'),
    Report = mongoose.model('Report');

/**
 * Globals
 */
var credentials ,user, report;

/**
 * Unit tests
 *//*
describe('Report Model Unit Tests:', function () {
  /!*  beforeEach(function (done) {
        user = new User({
            firstName: 'Full',
            lastName: 'Name',
            displayName: 'Full Name',
            email: 'test@test.com',
            username: 'username',
            password: 'password'
        });

        user.save(function () {
            report = {
                reports: [{name:'Report Name'}],
                user: user
            };

            done();
        });
    });*!/

    before(function (done) {
        var regUser = {
            username: 'test@test.com',
            password: 'password',
            ConfirmPassword: 'password',
            firstName: 'First Name',
            lastName: 'Last Name',
            companyName: 'nVipani',
            businessType: 'Trader',
            categorySeller: true,
            categoryBuyer: true,
            categoryMediator: true,
            mobile: '0123456789',
            acceptTerms: true
        };
        agent.post('/user/presignup')
            .send(regUser)
            .expect(200)
            .end(function (presignupErr, presignupRes) {
                //console.log("Presignup Response - "+JSON.stringify(presignupRes.message));
                if (presignupErr) done(presignupErr);

                User.findOne({
                    username: credentials.username
                }, '-salt -password', function (err, resUser) {
                    if (err) {
                        done(err);
                    }
                    user = resUser;
                    if (resUser) {
                        agent.get('/user/register/' + resUser.statusToken)
                            .expect(200)
                            .end(function (validationGetErr, validationGetRes) {
                                //console.log("Presignup Response - "+JSON.stringify(presignupRes.message));
                                if (validationGetErr) done(validationGetErr);
                                report = {
                                    reports: [{name:'Report Name'}],
                                    user: user
                                };

                                done();
                            });
                    }
                });
            });

        credentials = {
            username: 'test@test.com',
            password: 'password'
        };
    });

    describe('Method Save', function () {
        it('should be able to save without problems', function (done) {
            report.user=user._id;
            return report.save(function (err) {
                should.not.exist(err);
                done();
            });
        });

        it('should be able to show an error when try to save without name', function (done) {
            report.name = '';

            return report.save(function (err) {
                should.exist(err);
                done();
            });
        });
    });

    afterEach(function (done) {
        Report.remove().exec();
        User.remove().exec();

        done();
    });
});*/
