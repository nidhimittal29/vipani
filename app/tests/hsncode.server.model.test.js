'use strict';

/**
 * Module dependencies.
 */
var should = require('should'),
    mongoose = require('mongoose'),
    app = require('../../server'),
    User = mongoose.model('User'),
    Hsncode =mongoose.model('Hsncodes');

/**
 * Globals
 */
var user, company,hsncode;

/**
 * Unit tests
 */
describe('Hsncode Model Unit Tests:', function () {
    beforeEach(function (done) {
        user = new User({
            firstName: 'Full',
            lastName: 'Name',
            displayName: 'Full Name',
            email: 'test@test.com',
            username: 'username',
            password: 'password'
        });

        user.save(function () {
            hsncode = new Hsncode({
                name: 'Company Name',
                hsncode:'2323232221',
                chapterCode:'29',
                headingCode:'32',
                disabled:false,
                deleted:false
            });

            done();
        });
    });

    describe('Method Save', function () {
        it('should be able to save without problems', function (done) {
            return hsncode.save(function (err) {
                should.not.exist(err);
                done();
            });
        });

        it('should show an error when try to save without hsncode', function (done) {
            hsncode.hsncode = '';

            return hsncode.save(function (err) {
                should.exist(err);
                done();
            });
        });
        it('should show an error when try to save without chaptercode', function (done) {
            hsncode.chapterCode = '';

            return hsncode.save(function (err) {
                should.exist(err);
                done();
            });
        });
        it('should show an error when try to save without headingcode', function (done) {
            hsncode.headingCode = '';

            return hsncode.save(function (err) {
                should.exist(err);
                done();
            });
        });
    });

    afterEach(function (done) {
        Hsncode.remove().exec();
        done();
    });
});
