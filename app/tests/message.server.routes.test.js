'use strict';

var should = require('should'),
    request = require('supertest'),
    app = require('../../server'),
    mongoose = require('mongoose'),
    User = mongoose.model('User'),
    Contact = mongoose.model('Contact'),
    Company = mongoose.model('Company'),
    Message = mongoose.model('Message'),
    agent = request.agent(app);

/**
 * Globals
 */
var credentials, user, message;

/**
 * Message routes tests
 */
describe('Message CRUD tests', function () {
    beforeEach(function (done) {
        var regUser = {
            username: 'test@test.com',
            password: 'password',
            ConfirmPassword: 'password',
            firstName: 'First Name',
            lastName: 'Last Name',
            companyName: 'nVipani',
            businessType: 'Trader',
            categorySeller: true,
            categoryBuyer: true,
            categoryMediator: true,
            mobile: '0123456789',
            acceptTerms: true
        };
        agent.post('/user/presignup')
            .send(regUser)
            .expect(200)
            .end(function (presignupErr, presignupRes) {
                //console.log("Presignup Response - "+JSON.stringify(presignupRes.message));
                if (presignupErr) done(presignupErr);

                User.findOne({
                    username: credentials.username
                }, '-salt -password', function (err, resUser) {
                    if (err) {
                        done(err);
                    }
                    user = resUser;
                    if (resUser) {
                        agent.get('/user/register/' + resUser.statusToken)
                            .expect(200)
                            .end(function (validationGetErr, validationGetRes) {
                                //console.log("Presignup Response - "+JSON.stringify(presignupRes.message));
                                if (validationGetErr) done(validationGetErr);

                                message = {
                                    body: 'This is the message body'
                                };

                                done();
                            });
                    }
                });
            });

        credentials = {
            username: 'test@test.com',
            password: 'password'
        };
    });

    it('should be able to save Message instance if logged in', function (done) {
        agent.post('/auth/signin')
            .send(credentials)
            .expect(200)
            .end(function (signinErr, signinRes) {
                // Handle signin error
                if (signinErr) done(signinErr);

                var token = signinRes.body.token;

                // Save a new Message
                agent.post('/messages')
                    .send(message)
                    .set('Content-Type', 'application/json')
                    .set('token', token)
                    .expect(200)
                    .end(function (messageSaveErr, messageSaveRes) {
                        // Handle Message save error
                        if (messageSaveErr) done(messageSaveErr);

                        // Get a list of Messages
                        agent.get('/messages')
                            .set('Content-Type', 'application/json')
                            .set('token', token)
                            .end(function (messagesGetErr, messagesGetRes) {
                                // Handle Message save error
                                if (messagesGetErr) done(messagesGetErr);

                                // Get Messages list
                                var messages = messagesGetRes.body;

                                // Set assertions
                                (messages[0].user._id).should.equal(user.id);
                                (messages[0].body).should.match('This is the message body');

                                // Call the assertion callback
                                done();
                            });
                    });
            });
    });

    it('should not be able to save Message instance if not logged in', function (done) {
        agent.post('/messages')
            .send(message)
            .expect(401)
            .end(function (messageSaveErr, messageSaveRes) {
                // Call the assertion callback
                done(messageSaveErr);
            });
    });

    it('should not be able to save Message instance if no body is provided', function (done) {
        // Invalidate name field
        message.body = '';

        agent.post('/auth/signin')
            .send(credentials)
            .expect(200)
            .end(function (signinErr, signinRes) {
                // Handle signin error
                if (signinErr) done(signinErr);

                var token = signinRes.body.token;


                // Save a new Message
                agent.post('/messages')
                    .send(message)
                    .set('Content-Type', 'application/json')
                    .set('token', token)
                    .expect(400)
                    .end(function (messageSaveErr, messageSaveRes) {
                        // Set message assertion
                        (messageSaveRes.body.message).should.match('Please fill Message Body\b');

                        // Handle Message save error
                        done(messageSaveErr);
                    });
            });
    });

    it('should be able to update Message instance if signed in', function (done) {
        agent.post('/auth/signin')
            .send(credentials)
            .expect(200)
            .end(function (signinErr, signinRes) {
                // Handle signin error
                if (signinErr) done(signinErr);

                var token = signinRes.body.token;

                // Save a new Message
                agent.post('/messages')
                    .send(message)
                    .set('Content-Type', 'application/json')
                    .set('token', token)
                    .expect(200)
                    .end(function (messageSaveErr, messageSaveRes) {
                        // Handle Message save error
                        if (messageSaveErr) done(messageSaveErr);

                        // Update Message name
                        message.body = 'WHY YOU GOTTA BE SO MEAN?';

                        // Update existing Message
                        agent.put('/messages/' + messageSaveRes.body._id)
                            .send(message)
                            .set('Content-Type', 'application/json')
                            .set('token', token)
                            .expect(200)
                            .end(function (messageUpdateErr, messageUpdateRes) {
                                // Handle Message update error
                                if (messageUpdateErr) done(messageUpdateErr);

                                // Set assertions
                                (messageUpdateRes.body._id).should.equal(messageSaveRes.body._id);
                                (messageUpdateRes.body.body).should.match('WHY YOU GOTTA BE SO MEAN?');

                                // Call the assertion callback
                                done();
                            });
                    });
            });
    });

    it('should not be able to get a list of Messages if not signed in', function (done) {
        // Create new Message model instance
        var messageObj = new Message(message);

        // Save the Message
        messageObj.save(function () {
            // Request Messages
            request(app).get('/messages')
                .end(function (req, res) {
                    // Set assertion
                    (res.body.message).should.match('User is not logged in');

                    // Call the assertion callback
                    done();
                });

        });
    });


    it('should not be able to get a single Message if not signed in', function (done) {
        // Create new Message model instance
        var messageObj = new Message(message);

        // Save the Message
        messageObj.save(function () {
            request(app).get('/messages/' + messageObj._id)
                .end(function (req, res) {
                    // Set assertion
                    (res.body.message).should.match('User is not logged in');

                    // Call the assertion callback
                    done();
                });
        });
    });

    it('should be able to delete Message instance if signed in', function (done) {
        agent.post('/auth/signin')
            .send(credentials)
            .expect(200)
            .end(function (signinErr, signinRes) {
                // Handle signin error
                if (signinErr) done(signinErr);

                // Get the userId
                var token = signinRes.body.token;


                // Save a new Message
                agent.post('/messages')
                    .send(message)
                    .set('Content-Type', 'application/json')
                    .set('token', token)
                    .expect(200)
                    .end(function (messageSaveErr, messageSaveRes) {
                        // Handle Message save error
                        if (messageSaveErr) done(messageSaveErr);

                        // Delete existing Message
                        agent.delete('/messages/' + messageSaveRes.body._id)
                            .send(message)
                            .set('Content-Type', 'application/json')
                            .set('token', token)
                            .expect(200)
                            .end(function (messageDeleteErr, messageDeleteRes) {
                                // Handle Message error error
                                if (messageDeleteErr) done(messageDeleteErr);

                                // Set assertions
                                (messageDeleteRes.body._id).should.equal(messageSaveRes.body._id);

                                // Call the assertion callback
                                done();
                            });
                    });
            });
    });

    it('should not be able to delete Message instance if not signed in', function (done) {
        // Set Message user
        message.user = user;

        // Create new Message model instance
        var messageObj = new Message(message);

        // Save the Message
        messageObj.save(function () {
            // Try deleting Message
            request(app).delete('/messages/' + messageObj._id)
                .expect(401)
                .end(function (messageDeleteErr, messageDeleteRes) {
                    // Set message assertion
                    (messageDeleteRes.body.message).should.match('User is not logged in');

                    // Handle Message error error
                    done(messageDeleteErr);
                });

        });
    });

    afterEach(function (done) {
        User.remove().exec();
        Company.remove().exec();
        Contact.remove().exec();
        Message.remove().exec();
        done();
    });
});
