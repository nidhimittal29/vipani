'use strict';

var should = require('should'),
    request = require('supertest'),
    app = require('../../server'),
    mongoose = require('mongoose'),
    User = mongoose.model('User'),
    Company = mongoose.model('Company'),
    agent = request.agent(app);

/**
 * Globals
 */
var credentials, user, company;

/**
 * Company routes tests
 */
describe('Company CRUD tests', function () {
    beforeEach(function (done) {
        var regUser = {
            username: 'test@test.com',
            password: 'password',
            ConfirmPassword: 'password',
            firstName: 'First Name',
            lastName: 'Last Name',
            companyName: 'nVipani',
            businessType: 'Trader',
            categorySeller: true,
            categoryBuyer: true,
            categoryMediator: true,
            mobile: '0123456789',
            acceptTerms: true
        };
        agent.post('/user/presignup')
            .send(regUser)
            .expect(200)
            .end(function (presignupErr, presignupRes) {
                //console.log("Presignup Response - "+JSON.stringify(presignupRes.message));
                if (presignupErr) done(presignupErr);

                User.findOne({
                    username: credentials.username
                }, '-salt -password', function (err, resUser) {
                    if (err) {
                        done(err);
                    }
                    user = resUser;
                    if (resUser) {
                        agent.get('/user/register/' + resUser.statusToken)
                            .expect(200)
                            .end(function (validationGetErr, validationGetRes) {
                                //console.log("Presignup Response - "+JSON.stringify(presignupRes.message));
                                if (validationGetErr) done(validationGetErr);
                                done();
                            });
                    }
                });
            });

        credentials = {
            username: 'test@test.com',
            password: 'password'
        };
    });

    /*    it('should be able to save Company instance if logged in', function (done) {
        agent.post('/auth/signin')
            .send(credentials)
            .expect(200)
            .end(function (signinErr, signinRes) {
                // Handle signin error
                if (signinErr) done(signinErr);

                // Get the token
                var token = signinRes.body.token;

                // Save a new Company
                agent.post('/companies')
                    .send(company)
                    .set('Content-Type', 'application/json')
                    .set('token', token)
                    .expect(200)
                    .end(function (companySaveErr, companySaveRes) {
                        // Handle Company save error
                        if (companySaveErr) done(companySaveErr);

                        // Get a list of Companies
                        agent.get('/companies')
                            .end(function (companiesGetErr, companiesGetRes) {
                                // Handle Company save error
                                if (companiesGetErr) done(companiesGetErr);

                                // Get Companies list
                                var companies = companiesGetRes.body;

                                // Set assertions
                                (companies[0].user._id).should.equal(userId);
                                (companies[0].name).should.match('Company Name');

                                // Call the assertion callback
                                done();
                            });
                    });
            });
    });

    it('should not be able to save Company instance if not logged in', function (done) {
        agent.post('/companies')
            .send(company)
            .expect(401)
            .end(function (companySaveErr, companySaveRes) {
                // Call the assertion callback
                done(companySaveErr);
            });
    });

    it('should not be able to save Company instance if no name is provided', function (done) {
        // Invalidate name field
        company.name = '';

        agent.post('/auth/signin')
            .send(credentials)
            .expect(200)
            .end(function (signinErr, signinRes) {
                // Handle signin error
                if (signinErr) done(signinErr);

                // Get the userId
                var userId = user.id;

                // Save a new Company
                agent.post('/companies')
                    .send(company)
                    .expect(400)
                    .end(function (companySaveErr, companySaveRes) {
                        // Set message assertion
                        (companySaveRes.body.message).should.match('Please fill Company name');

                        // Handle Company save error
                        done(companySaveErr);
                    });
            });
     });*/

    it('should be able to update Company instance if signed in', function (done) {
        agent.post('/auth/signin')
            .send(credentials)
            .expect(200)
            .end(function (signinErr, signinRes) {
                // Handle signin error
                if (signinErr) done(signinErr);

                // Get the token
                var token = signinRes.body.token;

                agent.post('/users/me')
                    .set('Content-Type', 'application/json')
                    .set('token', token)
                    .expect(200)
                    .end(function (userErr, userRes) {
                        if (userErr) done(userErr);
                        var companyId;
                        if (userRes.body) {
                            var user = userRes.body;
                            companyId = user.company;
                        }
                        // Save a new Company
                        agent.get('/companies/' + companyId)
                            .expect(200)
                            .end(function (companySaveErr, companySaveRes) {
                                // Handle Company save error
                                if (companySaveErr) done(companySaveErr);

                                if (companySaveErr) done(companySaveErr);

                                if (companySaveRes.body) {
                                    company = companySaveRes.body;
                                }
                                company.addresses.push({
                                    addressLine: '123, 2nd Floor, 7th Cross, 10th Main, Halli',
                                    city: 'Bangalore',
                                    state: 'Karnataka',
                                    country: 'India',
                                    pinCode: '560075',
                                    addressType: 'Billing',
                                    primary: true,
                                    isPublic: true
                                });

                                company.bankAccountDetails = {
                                    bankAccountNumber: '12345678',
                                    accountType: 'Current',
                                    bankName: 'XYZ Bank',
                                    bankBranchName: 'ABC Brancg',
                                    ifscCode: 'SFJK021385'
                                };

                                // Update existing Company
                                agent.put('/companies/' + company._id)
                                    .send(company)
                                    .set('Content-Type', 'application/json')
                                    .set('token', token)
                                    .expect(200)
                                    .end(function (companyUpdateErr, companyUpdateRes) {
                                        // Handle Company update error
                                        if (companyUpdateErr) done(companyUpdateErr);

                                        // Set assertions
                                        (companyUpdateRes.body._id).should.equal(companySaveRes.body._id);
                                        (companyUpdateRes.body.name).should.match('nVipani');
                                        (companyUpdateRes.body.profileUrl).should.match('nvipani');
                                        (companyUpdateRes.body.profileImageURL).should.match('modules/companies/img/profile/default.png');
                                        (companyUpdateRes.body.addresses[0].addressLine).should.match('123, 2nd Floor, 7th Cross, 10th Main, Halli');
                                        (companyUpdateRes.body.addresses[0].city).should.match('Bangalore');
                                        (companyUpdateRes.body.addresses[0].state).should.match('Karnataka');
                                        (companyUpdateRes.body.addresses[0].country).should.match('India');
                                        (companyUpdateRes.body.addresses[0].pinCode).should.match('560075');
                                        (companyUpdateRes.body.addresses[0].addressType).should.match('Billing');
                                        (companyUpdateRes.body.addresses[0].primary).should.match(true);
                                        (companyUpdateRes.body.addresses[0].isPublic).should.match(true);

                                        (companyUpdateRes.body.bankAccountDetails).should.match({
                                            bankAccountNumber: '12345678',
                                            accountType: 'Current',
                                            bankName: 'XYZ Bank',
                                            bankBranchName: 'ABC Brancg',
                                            ifscCode: 'SFJK021385'
                                        });
                                        // Call the assertion callback
                                        done();
                                    });
                            });
                    });
            });
    });

    it('should be able to get Company instance with company profile url', function (done) {

        // Save a new Company
        agent.get('/company/' + 'nvipani')
            .expect(200)
            .end(function (companyUpdateErr, companyUpdateRes) {
                // Handle Company update error
                if (companyUpdateErr) done(companyUpdateErr);

                // Set assertions
                //(companyUpdateRes.body._id).should.equal(companySaveRes.body._id);
                (companyUpdateRes.body.company.name).should.match('nVipani');
                (companyUpdateRes.body.company.profileUrl).should.match('nvipani');
                (companyUpdateRes.body.company.profileImageURL).should.match('modules/companies/img/profile/default.png');
                /*                (companyUpdateRes.body.addresses[0].addressLine).should.match('123, 2nd Floor, 7th Cross, 10th Main, Halli');
                 (companyUpdateRes.body.addresses[0].city).should.match('Bangalore');
                 (companyUpdateRes.body.addresses[0].state).should.match('Karnataka');
                 (companyUpdateRes.body.addresses[0].country).should.match('India');
                 (companyUpdateRes.body.addresses[0].pinCode).should.match('560075');
                 (companyUpdateRes.body.addresses[0].addressType).should.match('Billing');
                 (companyUpdateRes.body.addresses[0].primary).should.match(true);
                 (companyUpdateRes.body.addresses[0].public).should.match(true);

                 (companyUpdateRes.body.bankAccountDetails).should.match({
                 bankAccountNumber: '12345678',
                 accountType: 'Current',
                 bankName: 'XYZ Bank',
                 bankBranchName: 'ABC Brancg',
                 ifscCode: 'SFJK021385'
                 });*/
                // Call the assertion callback
                done();
            });
    });
    /*
    it('should be able to get a list of Companies if not signed in', function (done) {
        // Create new Company model instance
        var companyObj = new Company(company);

        // Save the Company
        companyObj.save(function () {
            // Request Companies
            request(app).get('/companies')
                .end(function (req, res) {
                    // Set assertion
                    res.body.should.be.instanceof(Array).and.have.lengthOf(1);

                    // Call the assertion callback
                    done();
                });

        });
    });


    it('should be able to get a single Company if not signed in', function (done) {
        // Create new Company model instance
        var companyObj = new Company(company);

        // Save the Company
        companyObj.save(function () {
            request(app).get('/companies/' + companyObj._id)
                .end(function (req, res) {
                    // Set assertion
                    res.body.should.be.an.Object.with.property('name', company.name);

                    // Call the assertion callback
                    done();
                });
        });
    });

    it('should be able to delete Company instance if signed in', function (done) {
        agent.post('/auth/signin')
            .send(credentials)
            .expect(200)
            .end(function (signinErr, signinRes) {
                // Handle signin error
                if (signinErr) done(signinErr);

                // Get the userId
                var userId = user.id;

                // Save a new Company
                agent.post('/companies')
                    .send(company)
                    .expect(200)
                    .end(function (companySaveErr, companySaveRes) {
                        // Handle Company save error
                        if (companySaveErr) done(companySaveErr);

                        // Delete existing Company
                        agent.delete('/companies/' + companySaveRes.body._id)
                            .send(company)
                            .expect(200)
                            .end(function (companyDeleteErr, companyDeleteRes) {
                                // Handle Company error error
                                if (companyDeleteErr) done(companyDeleteErr);

                                // Set assertions
                                (companyDeleteRes.body._id).should.equal(companySaveRes.body._id);

                                // Call the assertion callback
                                done();
                            });
                    });
            });
    });

    it('should not be able to delete Company instance if not signed in', function (done) {
        // Set Company user
        company.user = user;

        // Create new Company model instance
        var companyObj = new Company(company);

        // Save the Company
        companyObj.save(function () {
            // Try deleting Company
            request(app).delete('/companies/' + companyObj._id)
                .expect(401)
                .end(function (companyDeleteErr, companyDeleteRes) {
                    // Set message assertion
                    (companyDeleteRes.body.message).should.match('User is not logged in');

                    // Handle Company error error
                    done(companyDeleteErr);
                });

        });
     });*/

    afterEach(function (done) {
        User.remove().exec();
        Company.remove().exec();
        done();
    });
});
