/**
 *author Manjula
 * Includes test cases for business user routes
 *
 */
'use strict';

var should = require('should'),
    request = require('supertest'),
    app = require('../../server'),
    mongoose = require('mongoose'),
    commonUtils=require('./utils/common.utils'),
    User = mongoose.model('User'),
    Company = mongoose.model('Company'),
    BusinessUnit=mongoose.model('BusinessUnit'),
    commonUserUtil=require('./utils/common.batch.users.utils'),
    commonBusinessUserUtil=require('./utils/common.batch.businessuser.utils'),
    async = require('async'),
    agent = request.agent(app);

/**
 * Globals
 */
var credentials,user,businessUnit,token,businessUser;
describe('Business users CRUD tests', function () {

    before(function (done) {
        var regUser = {
            username: 'test@test.com',
            companyName: 'nVipani',
            businessType: 'Trader',
            categorySeller: true,
            categoryBuyer: true,
            categoryMediator: true,
            mobile: '0123456789',
            acceptTerms: true
        };
        commonUserUtil.createEachStepUser({
            username: regUser.username,
            password: 'password',
            issendotp: true
        }, 200, agent, function (presendotpErr, presendotpRes) {
            should.not.exist(presendotpErr);
            presendotpRes.body.status.should.equal(true);
            var otp = presendotpRes.body.otp;
            presendotpRes.body.message.should.equal('An OTP has been sent to Email :' + regUser.username + '. ' + otp + ' is your One Time Password (OTP)');
            commonUserUtil.createEachStepUser({
                username: regUser.username,
                password: 'password',
                isverifyotp: true,
                otp: otp
            }, 200, agent, function (verifyotpErr, verifyotpRes) {
                should.not.exist(verifyotpErr);
                verifyotpRes.body.status.should.equal(true);
                verifyotpRes.body.message.should.equal('User is verified :' + regUser.username);
                var registrationCategory = verifyotpRes.body.registrationCategories[0]._id;
                var segments = [{
                    segment: verifyotpRes.body.segments[0]._id,
                    categories: [{category: verifyotpRes.body.categories[0]._id}]
                }];
                commonUserUtil.createEachStepUser({
                    username: regUser.username,
                    password: 'password',
                    ispassword: true,
                    registrationCategory: registrationCategory,
                    selectedSegments: segments,
                    companyName: regUser.companyName
                }, 200, agent, function (verifiedErr, verifiedRes) {
                    should.not.exist(verifiedErr);
                    verifiedRes.body.status.should.equal(true);
                    verifiedRes.body.message.should.equal('User is successfully registered');
                    user = verifiedRes.body.user;
                    credentials = {
                        username: 'test@test.com',
                        password: 'password'
                    };

                    done();
                });
            });
        });
    });
    beforeEach(function (done) {
        agent.post('/auth/signin')
            .send(credentials)
            .expect(200)
            .end(function (err, res) {
                // Handle signin error
                should.not.exist(err);
                var userres = res.body;
                token = userres.token;
                done();
            });
    });

    function createNewBusinessUser(user, done) {
        agent.post('/auth/adduser')
            .set('token', token)
            .send(user)
            .end(function (buser2Err, buser2Res) {
                should.not.exist(buser2Err);
                buser2Res.body.businessUser.should.be.instanceof(Array);
                var newuser = buser2Res.body.businessUser[buser2Res.body.businessUser.length - 1];
                newuser.businessUnits.length.should.equal(1);
                newuser.status.should.equal('Request');
                done(buser2Err, newuser);
            });
    }

    /**
     * case 1:Check registered user added as default business unit
     * case 2:Check registered user added as default business user
     */
    it('Registered user should be default business user and business unit', function (done) {
        //Check default business user and business unit in company
        var defaultBusinessUser, defaultBusinessUnit;
        agent.get('/companies/' + user.company)
            .set('token', token)
            .end(function (companyErr, companyRes) {
                var companyEmployees = companyRes.body.employees;
                defaultBusinessUser = companyEmployees.filter(function (eachEmployee) {
                    return eachEmployee.user.toString() === user._id.toString();
                });
                should.exist(defaultBusinessUser);
                defaultBusinessUser.length.should.equal(1);
                should.exist(defaultBusinessUser[0].userGroup);
                defaultBusinessUnit = companyRes.body.businessUnits.filter(function (eachUnit) {
                    return eachUnit.defaultBusinessUnit === true;
                });
                should.exist(defaultBusinessUnit);
                defaultBusinessUnit.length.should.equal(1);
                defaultBusinessUser[0].businessUnits.should.be.instanceof(Array);
                defaultBusinessUser[0].businessUnits.length.should.equal(1);
                should.exist(defaultBusinessUser[0].businessUnits[0].businessUnit);
                var defaultUnitOfDefaultUser = defaultBusinessUser[0].businessUnits.filter(function (eachUnit) {
                    return eachUnit.businessUnit.toString() === defaultBusinessUnit[0].businessUnit.toString();
                });
                should.exist(defaultUnitOfDefaultUser);
                defaultUnitOfDefaultUser.length.should.equal(1);
                agent.get('/branches/' + defaultBusinessUnit[0].businessUnit.toString())
                    .set('token', token)
                    .end(function (businessUnitErr, businessUnitRes) {
                        var businessUnitEmps = businessUnitRes.body.employees;
                        var businessUnitEmp = businessUnitEmps.filter(function (eachUnitEmp) {
                            return eachUnitEmp.user._id.toString() === defaultBusinessUser[0].user.toString();
                        });
                        should.exist(businessUnitEmp);
                        businessUnitEmp.length.should.equal(1);
                        should.exist(businessUnitEmp[0].userGroup);
                        done();
                    });
            });
    });

    /**
     *  case 1: Create multiple business users to default business unit +ve test case
     *  case 2: Check list of business users at company level +ve test case
     *  case 3: Disable business users at company level +ve test case
     *  case 4: Disable business users at company level with out company id -ve test case
     *  case 5: Disable business users at company level with out business user id's -ve test case
     *  case 6: Enable business users at company level +ve test case
     *  case 7: Enable business users at company level with out company id -ve test case
     *  case 8: Enable business users at company level with out business user id's -ve test case
     *  case 9: Remove business users at company level +ve test case
     *  case 10: Remove business users at company level with out company id -ve test case
     *  case 11: Remove business users at company level with out business user id's -ve test case
     */
    it('Should be able to do mass operations at company level', function (done) {
        var buser1, buser2, buser3, buser4;
        //create multiple business users
        buser1 = {
            user: user._id,
            userName: 'userformassoperation1@nvipani.com',
            company: user.company,
            userGroup: user.companies[0].userGroup
        };
        createNewBusinessUser(buser1, function (user1Err, user1) {
            should.not.exist(user1Err);
            should.exist(user1);
            buser2 = {
                user: user._id,
                userName: 'userformassoperation2@nvipani.com',
                company: user.company,
                userGroup: user.companies[0].userGroup
            };
            createNewBusinessUser(buser2, function (user2Err, user2) {
                should.not.exist(user2Err);
                should.exist(user2);
                buser3 = {
                    user: user._id,
                    userName: 'userformassoperation3@nvipani.com',
                    company: user.company,
                    userGroup: user.companies[0].userGroup
                };
                createNewBusinessUser(buser3, function (user3Err, user3) {
                    should.not.exist(user3Err);
                    should.exist(user3);

                    buser4 = {
                        user: user._id,
                        userName: 'userformassoperation4@nvipani.com',
                        company: user.company,
                        userGroup: user.companies[0].userGroup
                    };
                    createNewBusinessUser(buser4, function (user4Err, user4) {
                        should.not.exist(user4Err);
                        should.exist(user4);

                        //Check list of business users
                        agent.get('/companyBusinessUsers/' + user.company + '?page=1&limit=10')
                            .set('token', token)
                            .end(function (buserListErr, buserListRes) {
                                should.not.exist(buserListErr);
                                var usersList = buserListRes.body.employees;
                                should.exist(usersList);
                                usersList[usersList.length - 1].user.username.should.equal(buser4.userName);
                                usersList[usersList.length - 2].user.username.should.equal(buser3.userName);
                                usersList[usersList.length - 3].user.username.should.equal(buser2.userName);
                                usersList[usersList.length - 4].user.username.should.equal(buser1.userName);

                                //Disable business users +ve
                                agent.post('/auth/companyUserMassActions')
                                    .set('token', token)
                                    .send({
                                        companyEmployees: [usersList[usersList.length - 4].user._id, usersList[usersList.length - 3].user._id],
                                        isRemove: false,
                                        isEnable: false,
                                        isDisable: true,
                                        user: user._id
                                    })
                                    .end(function (disableUserErr, disableUserRes) {
                                        var data = disableUserRes.body;
                                        should.exist(data.failure);
                                        data.failure.should.be.instanceof(Array);
                                        data.failure.length.should.equal(1);
                                        data.failure[0].message.should.equal(usersList[usersList.length - 4].user.username + ' user needs to be registered');
                                        //Disable business users -ve with out company id
                                        agent.post('/auth/companyUserMassActions')
                                            .set('token', token)
                                            .send({
                                                companyEmployees: [usersList[usersList.length - 4].user._id, usersList[usersList.length - 3].user._id],
                                                isRemove: false,
                                                isEnable: false,
                                                isDisable: true,
                                                token: token
                                            })
                                            .end(function (disableUserErr1, disableUserRes1) {
                                                var data = disableUserRes1.body;
                                                should.exist(data.failure);
                                                data.failure.should.be.instanceof(Array);
                                                data.failure.length.should.equal(1);
                                                data.failure[0].message.should.equal(usersList[usersList.length - 4].user.username + ' user needs to be registered');

                                                //Disable business users -ve without employee list
                                                agent.post('/auth/companyUserMassActions')
                                                    .set('token', token)
                                                    .send({
                                                        companyEmployees: [],
                                                        isRemove: false,
                                                        isEnable: false,
                                                        isDisable: true,
                                                        token: token
                                                    })
                                                    .end(function (disableUserErr2, disableUserRes2) {
                                                        var data = disableUserRes2.body;
                                                        should.exist(data.message);
                                                        data.message.should.equal('Company employees are not selected');

                                                        //Enable business users +ve
                                                        agent.post('/auth/companyUserMassActions')
                                                            .set('token', token)
                                                            .send({
                                                                companyEmployees: [usersList[usersList.length - 4].user._id, usersList[usersList.length - 3].user._id],
                                                                isRemove: false,
                                                                isEnable: true,
                                                                isDisable: false,
                                                                token: token
                                                            })
                                                            .end(function (enableUserErr, enableUserRes) {
                                                                var data = enableUserRes.body;
                                                                should.exist(data.failure);
                                                                data.failure.should.be.instanceof(Array);
                                                                data.failure.length.should.equal(1);
                                                                data.failure[0].message.should.equal(usersList[usersList.length - 4].user.username + ' user needs to be registered');

                                                                //Enable business users -ve with out company id
                                                                agent.post('/auth/companyUserMassActions')
                                                                    .set('token', token)
                                                                    .send({
                                                                        companyEmployees: [usersList[usersList.length - 4].user._id, usersList[usersList.length - 3].user._id],
                                                                        isRemove: false,
                                                                        isEnable: true,
                                                                        isDisable: false,
                                                                        token: token
                                                                    })
                                                                    .end(function (enableUserErr1, enableUserRes1) {
                                                                        var data = enableUserRes1.body;
                                                                        should.exist(data.failure);
                                                                        data.failure.should.be.instanceof(Array);
                                                                        data.failure.length.should.equal(1);
                                                                        data.failure[0].message.should.equal(usersList[usersList.length - 4].user.username + ' user needs to be registered');

                                                                        //Enable business users -ve with out company employee list
                                                                        agent.post('/auth/companyUserMassActions')
                                                                            .set('token', token)
                                                                            .send({
                                                                                companyEmployees: [],
                                                                                isRemove: false,
                                                                                isEnable: true,
                                                                                isDisable: false,
                                                                                token: token
                                                                            })
                                                                            .end(function (enableUserErr2, enableUserRes2) {
                                                                                var data = enableUserRes2.body;
                                                                                should.exist(data.message);
                                                                                data.message.should.equal('Company employees are not selected');

                                                                                //Remove business users +ve
                                                                                agent.post('/auth/companyUserMassActions')
                                                                                    .set('token', token)
                                                                                    .send({
                                                                                        companyEmployees: [usersList[usersList.length - 4].user._id, usersList[usersList.length - 3].user._id],
                                                                                        isRemove: true,
                                                                                        isEnable: false,
                                                                                        isDisable: false,
                                                                                        token: token
                                                                                    })
                                                                                    .end(function (removeUserErr, removeUserRes) {
                                                                                        var data = removeUserRes.body;
                                                                                        should.exist(data.success);
                                                                                        data.success.should.be.instanceof(Array);
                                                                                        data.success.length.should.equal(2);

                                                                                        //Remove business users -ve with out company id
                                                                                        agent.post('/auth/companyUserMassActions')
                                                                                            .set('token', token)
                                                                                            .send({
                                                                                                companyEmployees: [usersList[usersList.length - 2].user._id],
                                                                                                isRemove: true,
                                                                                                isEnable: false,
                                                                                                isDisable: false
                                                                                            })
                                                                                            .end(function (removeUserErr1, removeUserRes1) {
                                                                                                var data = removeUserRes1.body;
                                                                                                should.exist(data.success);
                                                                                                data.success.should.be.instanceof(Array);
                                                                                                data.success.length.should.equal(1);

                                                                                                //Remove business users -ve with out employees list
                                                                                                agent.post('/auth/companyUserMassActions')
                                                                                                    .set('token', token)
                                                                                                    .send({
                                                                                                        companyEmployees: [],
                                                                                                        isRemove: true,
                                                                                                        isEnable: false,
                                                                                                        isDisable: false,
                                                                                                        token: token
                                                                                                    })
                                                                                                    .end(function (removeUserErr2, removeUserRes2) {
                                                                                                        var data = removeUserRes2.body;
                                                                                                        should.exist(data.message);
                                                                                                        data.message.should.equal('Company employees are not selected');
                                                                                                        agent.post('/auth/companyUserMassActions')
                                                                                                            .set('token', token)
                                                                                                            .send({
                                                                                                                companyEmployees: [usersList[usersList.length - 1].user._id, usersList[usersList.length - 3].user._id],
                                                                                                                isRemove: true,
                                                                                                                isEnable: false,
                                                                                                                isDisable: false,
                                                                                                                token: token
                                                                                                            })
                                                                                                            .end(function (removeUserErr3, removeUserRes3) {
                                                                                                                var data = removeUserRes3.body;
                                                                                                                should.exist(data.success);
                                                                                                                should.exist(data.failure);
                                                                                                                data.success.should.be.instanceof(Array);
                                                                                                                data.success.length.should.equal(1);
                                                                                                                data.failure.should.be.instanceof(Array);
                                                                                                                data.failure.length.should.equal(1);
                                                                                                                data.failure[0].message.should.equal('Employee user is not found in users');
                                                                                                                createNewBusinessUser({
                                                                                                                    user: user._id,
                                                                                                                    userName: 'userformassoperation5@nvipani.com',
                                                                                                                    company: user.company,
                                                                                                                    userGroup: user.companies[0].userGroup
                                                                                                                }, function (err, employee) {
                                                                                                                    agent.post('/auth/companyUserMassActions')
                                                                                                                        .set('token', token)
                                                                                                                        .send({
                                                                                                                            companyEmployees: [employee.user._id, usersList[usersList.length - 3].user._id, usersList[usersList.length - 2].user._id],
                                                                                                                            isRemove: true,
                                                                                                                            isEnable: false,
                                                                                                                            isDisable: false,
                                                                                                                            token: token
                                                                                                                        })
                                                                                                                        .end(function (removeUserErr4, removeUserRes4) {
                                                                                                                            var data = removeUserRes4.body;
                                                                                                                            should.exist(data.success);
                                                                                                                            should.exist(data.failure);
                                                                                                                            data.success.should.be.instanceof(Array);
                                                                                                                            data.success.length.should.equal(1);
                                                                                                                            data.failure.should.be.instanceof(Array);
                                                                                                                            data.failure.length.should.equal(1);
                                                                                                                            data.failure[0].message.should.equal('Employee user is not found in users');
                                                                                                                            done();
                                                                                                                        });
                                                                                                                });

                                                                                                            });
                                                                                                    });
                                                                                            });
                                                                                    });
                                                                            });
                                                                    });
                                                            });
                                                    });
                                            });
                                    });

                            });
                    });
                });
            });
        });
    });

    /**
     *  case 1: Create multiple business users to one business unit +ve test case
     *  case 2: Check list of business users at business unit level +ve test case
     *  case 3: Enable business users at business unit level +ve test case
     *  case 4: Enable business users at business unit level with out business unit id -ve test case
     *  case 5: Enable business users at business unit level with out business user id's -ve test case
     *  case 6: Disable business users at business unit level +ve test case
     *  case 7: Disable business users at business unit level with out business unit id -ve test case
     *  case 8: Disable business users at business unit level with out business user id's -ve test case
     *  case 9: Remove business users at business unit level +ve test case
     *  case 10: Remove business users at business unit level with out business unit id -ve test case
     *  case 11: Remove business users at business unit level with out business user id's -ve test case
     */
    it('Should be able to do mass operations at business unit level', function (done) {
        var buser1, buser2, buser3;
        //create business unit
        businessUnit = {
            name: 'Bangalore Godown',
            type: 'Stockpoint',
            emails: [{email: 'blrgdn@test.com', isPrimary: true, emailType: 'Work'}],
            addresses: [{
                addressLine: 'address line1',
                city: 'bangalore',
                state: 'Karnataka',
                country: 'India',
                pinCode: 560075,
                primary: true
            }],

            disabled: false,
            deleted: false,
            company: user.company,
            user: user._id
        };
        agent.post('/branches')
            .set('token', token)
            .send(businessUnit)
            .end(function (unitErr, unitRes) {
                should.not.exist(unitErr);
                businessUnit = unitRes.body;
                businessUnit.name.should.equal('Bangalore Godown');
                //create multiple business users
                buser1 = {
                    user: user._id,
                    userName: 'unitUserMassOperations1@nvipani.com',
                    company: user.company,
                    userGroup: user.companies[0].userGroup,
                    bunits: [businessUnit._id]
                };
                createNewBusinessUser(buser1, function (user1Err, user1) {
                    should.not.exist(user1Err);
                    should.exist(user1);

                    buser2 = {
                        user: user._id,
                        userName: 'unitUserMassOperations2@nvipani.com',
                        company: user.company,
                        userGroup: user.companies[0].userGroup,
                        bunits: [businessUnit._id]
                    };
                    createNewBusinessUser(buser2, function (user2Err, user2) {
                        should.not.exist(user2Err);
                        should.exist(user2);

                        buser3 = {
                            user: user._id,
                            userName: 'unitUserMassOperations3@nvipani.com',
                            company: user.company,
                            userGroup: user.companies[0].userGroup,
                            bunits: [businessUnit._id]
                        };
                        createNewBusinessUser(buser3, function (user3Err, user3) {
                            should.not.exist(user3Err);
                            should.exist(user3);
                            //Check list of business users
                            agent.get('/companyBusinessUsers/' + user.company + '?page=1&limit=10')
                                .set('token', token)
                                .end(function (userListErr, userListRes) {
                                    should.not.exist(userListErr);
                                    var usersList = userListRes.body.employees;
                                    should.exist(usersList);
                                    usersList[usersList.length - 1].user.username.should.equal(buser3.userName);
                                    usersList[usersList.length - 2].user.username.should.equal(buser2.userName);
                                    usersList[usersList.length - 3].user.username.should.equal(buser1.userName);

                                    //Remove business users +ve
                                    agent.post('/auth/businessUnitMassActions')
                                        .set('token', token)
                                        .send({
                                            businessUnitEmployees: [usersList[usersList.length - 2].user._id, usersList[usersList.length - 3].user._id],
                                            isRemove: true,
                                            isEnable: false,
                                            isDisable: false,
                                            businessUnit: businessUnit._id
                                        })
                                        .end(function (removeUserErr, removeUserRes) {
                                            var data = removeUserRes.body;
                                            should.exist(data.success);
                                            data.success.should.be.instanceof(Array);
                                            data.success.length.should.equal(2);

                                            //Remove business users -ve wit out business unit id
                                            agent.post('/auth/businessUnitMassActions')
                                                .set('token', token)
                                                .send({
                                                    businessUnitEmployees: [usersList[usersList.length - 1].user._id],
                                                    isRemove: true,
                                                    isEnable: false,
                                                    isDisable: false
                                                })
                                                .end(function (removeUserErr1, removeUserRes1) {
                                                    var data = removeUserRes1.body;
                                                    should.exist(data.message);
                                                    data.message.should.equal('Business unit id is require');

                                                    //Remove business users -ve with out employees list
                                                    agent.post('/auth/businessUnitMassActions')
                                                        .set('token', token)
                                                        .send({
                                                            businessUnitEmployees: [],
                                                            isRemove: true,
                                                            isEnable: false,
                                                            isDisable: false,
                                                            businessUnit: businessUnit._id
                                                        })
                                                        .end(function (removeUserErr2, removeUserRes2) {
                                                            var data = removeUserRes2.body;
                                                            should.exist(data.message);
                                                            data.message.should.equal('At least one employee of business unit is required');
                                                            done();
                                                        });
                                                });
                                        });
                                });
                        });
                    });
                });
            });
    });


    /**
     * case 1: Create business user to default business unit and activate it +ve
     * case 2:Check business user is enabled or not after activation
     * case 3: Disable business user at company level +ve test case
     * case 4: Enable business user at company level +ve test case
     * case 5: Delete business user at company level +ve test case
     */
    it('Activate business user of default business unit and check enable and disable', function (done) {
        commonBusinessUserUtil.createActivateBusinessUser(user,'ActiveUser@test.com','Admin',[],'nvipani123',token,agent,function (err, regUser) {
            should.not.exist(err);
            regUser.status.should.equal('Registered');
            //Check user is in active or not
            agent.get('/companyBusinessUsers/' + user.company + '?page=1&limit=10')
                .set('token', token)
                .end(function (buserListErr, buserListRes) {
                    should.not.exist(buserListErr);
                    var usersList = buserListRes.body.employees;
                    should.exist(usersList);
                    usersList[usersList.length - 1].user.username.should.equal('ActiveUser@test.com');
                    var employee = usersList[usersList.length - 1];
                    employee.status.should.equal('Active');
                    //Disable business users +ve
                    agent.post('/auth/companyUserMassActions')
                        .set('token', token)
                        .send({
                            companyEmployees: [regUser._id],
                            isRemove: false,
                            isEnable: false,
                            isDisable: true,
                            user: user._id
                        })
                        .end(function (disableUserErr, disableUserRes) {
                            var data = disableUserRes.body;
                            should.exist(data.success);
                            data.success.should.be.instanceof(Array);
                            data.success.length.should.equal(1);
                            //Enable business user +ve
                            agent.post('/auth/companyUserMassActions')
                                .set('token', token)
                                .send({
                                    companyEmployees: [regUser._id],
                                    isRemove: false,
                                    isEnable: true,
                                    isDisable: false,
                                    user: user._id
                                })
                                .end(function (disableUserErr, disableUserRes) {
                                    var data = disableUserRes.body;
                                    should.exist(data.success);
                                    data.success.should.be.instanceof(Array);
                                    data.success.length.should.equal(1);
                                    agent.post('/auth/companyUserMassActions')
                                        .set('token', token)
                                        .send({
                                            companyEmployees: [regUser._id],
                                            isRemove: true,
                                            isEnable: false,
                                            isDisable: false,
                                            user: user._id
                                        })
                                        .end(function (disableUserErr, disableUserRes) {
                                            var data = disableUserRes.body;
                                            should.exist(data.success);
                                            data.success.should.be.instanceof(Array);
                                            done();
                                        });
                                });
                        });
                });
        });
    });

    /**
     * case 1: Create business unit and business user to that business unit and activate it +ve
     * case 2: Check business user is activate or not in business unit
     * case 2: Delete business user at business unit level +ve test case
     */
    it('Activate business user of business unit and delete user', function (done) {

        businessUnit = {
            name: 'Bangalore Godown',
            type: 'Stockpoint',
            emails: [{email: 'blrgdn@test.com', isPrimary: true, emailType: 'Work'}],
            addresses: [{
                addressLine: 'address line1',
                city: 'bangalore',
                state: 'Karnataka',
                country: 'India',
                pinCode: 560075,
                primary: true
            }],
            disabled: false,
            deleted: false,
            company: user.company,
            user: user._id
        };
        agent.post('/branches')
            .set('token', token)
            .send(businessUnit)
            .expect(200)
            .end(function (businessUnitErr, businessUnitRes) {
                //contact should have been created
                should.not.exist(businessUnitErr);
                businessUnit = businessUnitRes.body;
                businessUnit.name.should.equal('Bangalore Godown');
                businessUnit.addresses.length.should.equal(1);
                businessUnit.emails.length.should.equal(1);
                businessUnit.company._id.should.equal(user.company);
                businessUnit.employees.length.should.equal(1);
                var emp = businessUnit.employees[0];
                emp.incharge.should.equal(true);
                emp.user._id.should.equal(user._id);

                createNewBusinessUser({
                    user: user._id,
                    userName: 'ActiveUnitUser@test.com',
                    company: user.company,
                    userGroup: user.companies[0].userGroup,
                    bunits: [businessUnit._id]
                }, function (err, employee) {
                    should.not.exist(err);
                    agent.get('/auth/activeUser?token=' + employee.statusToken)
                        .expect(200)
                        .end(function (err, res) {
                            agent.post('/auth/activateUser/' + employee.statusToken)
                                .send({
                                    username: 'ActiveUnitUser@test.com',
                                    password: 'nvipani123',
                                    company: user.company,
                                    userType: 'Employee',
                                    status: 'Register Request',
                                    statusToken: employee.statusToken
                                })
                                .end(function (err, res) {
                                    should.not.exist(err);
                                    var regUser = res.body;
                                    should.exist(regUser);
                                    regUser.status.should.equal('Registered');
                                    //Check business user active or not
                                    agent.get('/companyBusinessUsers/' + user.company + '?page=1&limit=10')
                                        .set('token', token)
                                        .end(function (buserListErr, buserListRes) {
                                            should.not.exist(buserListErr);
                                            var usersList = buserListRes.body.employees;
                                            should.exist(usersList);
                                            usersList[usersList.length - 1].user.username.should.equal('ActiveUnitUser@test.com');
                                            var employee = usersList[usersList.length - 1];
                                            employee.status.should.equal('Active');
                                            //Disable business users +ve
                                            agent.post('/auth/businessUnitMassActions')
                                                .set('token', token)
                                                .send({
                                                    businessUnitEmployees: [regUser._id],
                                                    isRemove: true,
                                                    businessUnit: businessUnit._id
                                                })
                                                .end(function (disableUserErr, disableUserRes) {
                                                    var data = disableUserRes.body;
                                                    should.exist(data.success);
                                                    data.success.should.be.instanceof(Array);
                                                    data.success.length.should.equal(1);
                                                    done();
                                                });
                                        });
                                });
                        });
                });
            });
    });

    /**
     * case 1: Create business user +ve test case
     * case 2: Check business user and default business unit is in company +ve test case
     * case 3: Check business user is in business unit +ve test case
     */
    it('Create business user and check created properly', function (done) {
        businessUser = {
            user: user._id,
            userName: 'firstBUser@test.com',
            company: user.company,
            userGroup: user.companies[0].userGroup
        };

        //Create business user
        createNewBusinessUser(businessUser, function (userErr, user) {
            should.not.exist(userErr);
            should.exist(user);
            var newuser = user;
            //Check business user and default business unit in company
            agent.get('/companies/' + newuser.user.company)
                .set('token', token)
                .end(function (companyErr, companyRes) {
                    var companyEmployees = companyRes.body.employees;
                    async.forEachSeries(companyEmployees, function (eachEmployee, callback) {
                        if (eachEmployee.user.toString() === newuser.user.toString()) {
                            eachEmployee.user.should.equal(newuser.user);
                            var businessUnit = companyRes.body.businessUnits.filter(function (eachUnit) {
                                return eachUnit.defaultBusinessUnit === true;
                            });
                            businessUnit[0].businessUnit.toString().should.equal(newuser.businessUnits[0].businessUnit);
                        }
                        callback();
                    }, function (err) {
                        //Check business user in business unit
                        agent.get('/branches/' + newuser.businessUnits[0].businessUnit)
                            .set('token', token)
                            .end(function (businessUnitErr, businessUnitRes) {
                                var businessUnitEmps = businessUnitRes.body.employees;
                                var businessUnitEmp = businessUnitEmps.filter(function (eachUnitEmp) {
                                    return eachUnitEmp.user.toString() === newuser.user.toString();
                                });
                                should.exist(businessUnitEmp);
                                done();
                            });
                    });
                });

        });
    });

    /**
     *  case 1: Create businessUnit with all required Fields +ve test case
     *  case 2: Create businessUser to single businessUnit with all fields and user name as email id +ve test case
     *  case 3: Create businessUser to business Unit with out company field -ve test case
     */
    it('should be able to add businessUser with email id as user name to businessUnit', function (done) {
        //create business units and try adding them to business unit
        businessUnit = {
            name: 'Bangalore Godown',
            type: 'Stockpoint',
            emails: [{email: 'blrgdn@test.com', isPrimary: true, emailType: 'Work'}],
            addresses: [{
                addressLine: 'address line1',
                city: 'bangalore',
                state: 'Karnataka',
                country: 'India',
                pinCode: 560075,
                primary: true
            }],

            disabled: false,
            deleted: false,
            company: user.company,
            user: user._id
        };
        agent.post('/branches')
            .set('token', token)
            .send(businessUnit)
            .expect(200)
            .end(function (businessUnitErr, businessUnitRes) {
                //contact should have been created
                should.not.exist(businessUnitErr);
                businessUnit = businessUnitRes.body;
                businessUnit.name.should.equal('Bangalore Godown');
                businessUnit.addresses.length.should.equal(1);
                businessUnit.emails.length.should.equal(1);
                businessUnit.company._id.should.equal(user.company);
                businessUnit.employees.length.should.equal(1);
                var emp = businessUnit.employees[0];
                emp.incharge.should.equal(true);
                emp.user._id.should.equal(user._id);
                businessUser = {
                    user: user._id,
                    userName: 'BUser1@test.com',
                    company: user.company,
                    userGroup: user.companies[0].userGroup,
                    bunits: [businessUnit._id]
                };
                createNewBusinessUser(businessUser, function (userErr, newuser) {
                    should.not.exist(userErr);
                    should.exist(newuser);
                    businessUser = {
                        user: user._id,
                        userName: 'BUser7@test.com',
                        userGroup: user.companies[0].userGroup,
                        bunits: [businessUnit._id]
                    };
                    createNewBusinessUser(businessUser, function (user2Err, user2) {
                        should.not.exist(user2Err);
                        should.exist(user2);
                        done();
                    });
                });
            });

    });

    /**
     *  case 1: Create businessUnit with all required Fields +ve test case
     *  case 2: Create businessUser to single businessUnit with all fields and user name as mobile number -ve test case
     */
    it('should be able to add businessUser with mobile number as user name to businessUnit', function (done) {
        //create business units and try adding them to business unit
        businessUnit = {
            name: 'Bangalore Godown',
            type: 'Stockpoint',
            emails: [{email: 'blrgdn@test.com', isPrimary: true, emailType: 'Work'}],
            addresses: [{
                addressLine: 'address line1',
                city: 'bangalore',
                state: 'Karnataka',
                country: 'India',
                pinCode: 560075,
                primary: true
            }],

            disabled: false,
            deleted: false,
            company: user.company,
            user: user._id
        };
        agent.post('/branches')
            .set('token', token)
            .send(businessUnit)
            .expect(200)
            .end(function (businessUnitErr, businessUnitRes) {
                //contact should have been created
                should.not.exist(businessUnitErr);
                businessUnit = businessUnitRes.body;
                businessUnit.name.should.equal('Bangalore Godown');
                businessUnit.addresses.length.should.equal(1);
                businessUnit.emails.length.should.equal(1);
                businessUnit.company._id.should.equal(user.company);
                businessUnit.employees.length.should.equal(1);
                var emp = businessUnit.employees[0];
                emp.incharge.should.equal(true);
                emp.user._id.should.equal(user._id);
                businessUser = {
                    user: user._id,
                    userName: '1245785462',
                    company: user.company,
                    userGroup: user.companies[0].userGroup,
                    bunits: [businessUnit._id]
                };
                agent.post('/auth/adduser')
                    .set('token', token)
                    .send(businessUser)
                    .end(function (businessUserErr, businessUserRes) {
                        should.not.exist(businessUserErr);
                        should.exist(businessUserRes);
                        businessUserRes.body.message.should.equal('Should give proper user name i.e email id');
                        done();
                    });
            });

    });

    /**
     *  case 1: Create businessUser with multiple businessUnit and user names as email ids +ve test case
     *  case 2: Create single business users to multiple business unit +ve test case
     *  case 3: Add company level business users to business unit +ve test case
     *  case 3: Create new business users to  single business unit +ve test case
     *
     */
    it('should be able to add multiple businessUser with multiple businessUnit and email id as user name', function (done) {
        //create business units and try adding them to business unit
        businessUnit = {
            name: 'Bangalore Godown',
            type: 'Stockpoint',
            emails: [{email: 'blrgdn@test.com', isPrimary: true, emailType: 'Work'}],
            addresses: [{
                addressLine: 'address line1',
                city: 'bangalore',
                state: 'Karnataka',
                country: 'India',
                pinCode: 560075,
                primary: true
            }],

            disabled: false,
            deleted: false,
            company: user.company,
            user: user._id
        };

        var businessUnit1 = {
            name: 'Hyderabad Godown',
            type: 'Stockpoint',
            emails: [{email: 'hydbd@test.com', isPrimary: true, emailType: 'Work'}],
            addresses: [{
                addressLine: 'address line1',
                city: 'Hyderabad',
                state: 'Telangana',
                country: 'India',
                pinCode: 560075,
                primary: true
            }],

            disabled: false,
            deleted: false,
            company: user.company,
            user: user._id
        };
        //Create business units
        agent.post('/branches')
            .set('token', token)
            .send(businessUnit)
            .expect(200)
            .end(function (businessUnitErr, businessUnitRes) {
                //contact should have been created
                should.not.exist(businessUnitErr);
                businessUnit = businessUnitRes.body;
                businessUnit.name.should.equal('Bangalore Godown');
                businessUnit.addresses.length.should.equal(1);
                businessUnit.emails.length.should.equal(1);
                businessUnit.company._id.should.equal(user.company);
                businessUnit.employees.length.should.equal(1);
                var emp = businessUnit.employees[0];
                emp.incharge.should.equal(true);
                emp.user._id.should.equal(user._id);
                agent.post('/branches')
                    .set('token', token)
                    .send(businessUnit1)
                    .expect(200)
                    .end(function (businessUnit1Err, businessUnit1Res) {
                        //contact should have been created
                        should.not.exist(businessUnit1Err);
                        businessUnit1 = businessUnit1Res.body;
                        businessUnit1.name.should.equal('Hyderabad Godown');
                        businessUnit1.addresses.length.should.equal(1);
                        businessUnit1.emails.length.should.equal(1);
                        businessUnit1.company._id.should.equal(user.company);
                        businessUnit1.employees.length.should.equal(1);
                        var emp = businessUnit1.employees[0];
                        emp.incharge.should.equal(true);
                        emp.user._id.should.equal(user._id);

                        businessUser = {
                            user: user._id,
                            userName: 'Buser1@nvipani.com',
                            company: user.company,
                            userGroup: user.companies[0].userGroup,
                            bunits: [businessUnit._id, businessUnit1._id]
                        };
                        //Create business user to multiple units
                        agent.post('/auth/adduser')
                            .set('token', token)
                            .send(businessUser)
                            .expect(200)
                            .end(function (businessUserErr, businessUserRes) {
                                should.not.exist(businessUserErr);
                                should.exist(businessUserRes.body);
                                var newuser = businessUserRes.body.businessUser[businessUserRes.body.businessUser.length - 1];

                                newuser.businessUnits.length.should.equal(2);
                                newuser.businessUnits[0].businessUnit.should.equal(businessUnit._id);
                                newuser.businessUnits[1].businessUnit.should.equal(businessUnit1._id);
                                newuser.status.should.equal('Request');
                                //Add company business users to business unit
                                var companyUser = {
                                    user: user._id,
                                    userName: 'companyUser1@test.com',
                                    company: user.company,
                                    userGroup: user.companies[0].userGroup
                                };
                                createNewBusinessUser(companyUser, function (userErr, newCompanyUser) {
                                    should.not.exist(userErr);
                                    should.exist(newCompanyUser);
                                    var companyUser1 = {
                                        user: user._id,
                                        userName: 'companyUser2@test.com',
                                        company: user.company,
                                        userGroup: user.companies[0].userGroup
                                    };
                                    createNewBusinessUser(companyUser1, function (userErr, newCompanyUser1) {
                                        should.not.exist(userErr);
                                        should.exist(newCompanyUser1);
                                        agent.post('/auth/addMassUsersToUnit')
                                            .set('token', token)
                                            .send({
                                                unitId: businessUnit1._id,
                                                employees: [{
                                                    user: newCompanyUser.user._id,
                                                    userGroup: newCompanyUser.userGroup._id
                                                },
                                                    {
                                                        user: newCompanyUser1.user._id,
                                                        userGroup: newCompanyUser1.userGroup._id
                                                    }]
                                            })
                                            .expect(200)
                                            .end(function (updateUnitErr, updateUnit) {
                                                should.not.exist(updateUnitErr);
                                                should.exist(updateUnit);
                                                var unit = updateUnit.body;
                                                should.exist(unit.employees);
                                                var employeeList = unit.employees;
                                                employeeList[employeeList.length - 1].user.username.should.equal(companyUser1.userName);
                                                employeeList[employeeList.length - 2].user.username.should.equal(companyUser.userName);
                                                businessUser = {
                                                    user: user._id,
                                                    userName: 'singleUser@nvipani.com',
                                                    company: user.company,
                                                    userGroup: user.companies[0].userGroup,
                                                    bunits: [businessUnit._id]
                                                };
                                                //Create business user to multiple units
                                                agent.post('/auth/adduser')
                                                    .set('token', token)
                                                    .send(businessUser)
                                                    .expect(200)
                                                    .end(function (businessUserErr, businessUserRes) {
                                                        should.not.exist(businessUserErr);
                                                        should.exist(businessUserRes.body);
                                                        var newuser = businessUserRes.body.businessUser[businessUserRes.body.businessUser.length - 1];
                                                        should.exist(newuser.user);
                                                        newuser.businessUnits.length.should.equal(1);
                                                        newuser.businessUnits[0].businessUnit.should.equal(businessUnit._id);
                                                        newuser.status.should.equal('Request');
                                                        done();
                                                    });
                                            });
                                    });
                                });

                            });
                    });
            });

    });

    /*/**
     *  case 1: Create businessUser with multiple businessUnit and user names as mobile number +ve test case
     *!/
    it('should be able to add businessUser  with multiple businessUnit and mobile number as user name', function (done) {
        //create business units and try adding them to business unit
        businessUnit = {
            name: 'Bangalore Godown',
            type: 'Stockpoint',
            emails: [{email: 'blrgdn@test.com', isPrimary: true, emailType: 'Work'}],
            addresses: [{
                addressLine: 'address line1',
                city: 'bangalore',
                state: 'Karnataka',
                country: 'India',
                pinCode: 560075,
                primary: true
            }],

            disabled: false,
            deleted: false,
            company: user.company,
            user: user._id
        };

        var businessUnit1 = {
            name: 'Hyderabad Godown',
            type: 'Stockpoint',
            emails: [{email: 'hydbd@test.com', isPrimary: true, emailType: 'Work'}],
            addresses: [{
                addressLine: 'address line1',
                city: 'Hyderabad',
                state: 'Telangana',
                country: 'India',
                pinCode: 560075,
                primary: true
            }],

            disabled: false,
            deleted: false,
            company: user.company,
            user: user._id
        };
        agent.post('/branches')
            .set('token', token)
            .send(businessUnit)
            .expect(200)
            .end(function (err, res) {
                //contact should have been created
                should.not.exist(err);
                businessUnit = res.body;
                businessUnit.name.should.equal('Bangalore Godown');
                businessUnit.addresses.length.should.equal(1);
                businessUnit.emails.length.should.equal(1);
                businessUnit.company.should.equal(user.company);
                businessUnit.employees.length.should.equal(1);
                var emp = businessUnit.employees[0];
                emp.incharge.should.equal(true);
                emp.user._id.should.equal(user._id);
                agent.post('/branches')
                    .set('token', token)
                    .send(businessUnit1)
                    .expect(200)
                    .end(function (err, res) {
                        //contact should have been created
                        should.not.exist(err);
                        businessUnit1 = res.body;
                        businessUnit1.name.should.equal('Hyderabad Godown');
                        businessUnit1.addresses.length.should.equal(1);
                        businessUnit1.emails.length.should.equal(1);
                        businessUnit1.company.should.equal(user.company);
                        businessUnit1.employees.length.should.equal(1);
                        var emp = businessUnit1.employees[0];
                        emp.incharge.should.equal(true);
                        emp.user._id.should.equal(user._id);

                        businessUser = {
                            user: user._id,
                            userName: '8247340199',
                            company: user.company,
                            userGroup: user.companies[0].userGroup,
                            bunits: [businessUnit._id,businessUnit1._id]
                        };
                        agent.post('/auth/adduser')
                            .set('token', token)
                            .send(businessUser)
                            .expect(200)
                            .end(function (err, res) {
                                should.not.exist(err);
                                var newuser = res.body;
                                should.exist(newuser.user);
                                newuser.businessUnits.length.should.equal(2);
                                newuser.businessUnits[0].businessUnit.should.equal(businessUnit._id);
                                newuser.businessUnits[1].businessUnit.should.equal(businessUnit1._id);
                                newuser.status.should.equal('Request');
                                done();
                            });
                    });
            });

    });*/

    /**
     *  case 1: Create businessUser with out UserGroup field -ve test case
     *  case 2: Create businessUser with wrong UserGroup field -ve test case
     *  case 3: Create businessUser with out UserName field -ve test case
     *  case 4: Create businessUser with out userGroup and user name fields -ve test case
     *  case 5: Create businessUser with out businessUnit field -Ve test case
     *  case 6: Create businessUser with out Company and business units fields -Ve test case
     */
    it('should be able to add businessUser to default businessUnit', function (done) {
        //create business units and try adding them to business unit
        businessUser = {
            user: user._id,
            userName: 'BUser1@test.com',
            company: user.company
        };
        agent.post('/auth/adduser')
            .set('token', token)
            .send(businessUser)
            .expect(200)
            .end(function (err, res) {
                var newuser = res.body;
                should.not.exist(newuser.user);
                should.exist(newuser.message);
                newuser.message.should.equal('Should give one user group for' + businessUser.userName);

                businessUser = {
                    user: user._id,
                    userName: 'BUser2@test.com',
                    company: user.company,
                    userGroup: 'nvipani'
                };
                agent.post('/auth/adduser')
                    .set('token', token)
                    .send(businessUser)
                    .expect(200)
                    .end(function (err, res) {
                        var newuser = res.body;
                        should.not.exist(newuser.user);
                        should.exist(newuser.message);
                        newuser.message.should.equal('Cast to ObjectId failed for value "' + businessUser.userGroup + '" at path "userGroup"');

                        businessUser = {
                            user: user._id,
                            company: user.company,
                            userGroup: user.companies[0].userGroup
                        };
                        agent.post('/auth/adduser')
                            .set('token', token)
                            .send(businessUser)
                            .expect(200)
                            .end(function (err, res) {
                                var newuser = res.body;
                                should.not.exist(newuser.user);
                                should.exist(newuser.message);
                                newuser.message.should.equal('Should give user name');

                                businessUser = {
                                    user: user._id,
                                    company: user.company
                                };
                                agent.post('/auth/adduser')
                                    .set('token', token)
                                    .send(businessUser)
                                    .expect(200)
                                    .end(function (err, res) {
                                        var newuser = res.body;
                                        should.not.exist(newuser.user);
                                        should.exist(newuser.message);
                                        newuser.message.should.equal('Should give required fields i.e user group and business user');

                                        businessUser = {
                                            user: user._id,
                                            userName: 'BUser3@test.com',
                                            company: user.company,
                                            userGroup: user.companies[0].userGroup
                                        };
                                        agent.post('/auth/adduser')
                                            .set('token', token)
                                            .send(businessUser)
                                            .expect(200)
                                            .end(function (err, res) {
                                                should.not.exist(err);
                                                var newuser = res.body.businessUser[res.body.businessUser.length - 1];
                                                should.exist(newuser.user);
                                                newuser.businessUnits.length.should.equal(1);
                                                newuser.status.should.equal('Request');

                                                businessUser = {
                                                    user: user._id,
                                                    userName: 'BUser6@test.com',
                                                    userGroup: user.companies[0].userGroup
                                                };
                                                agent.post('/auth/adduser')
                                                    .set('token', token)
                                                    .send(businessUser)
                                                    .expect(200)
                                                    .end(function (err, res) {
                                                        should.not.exist(err);
                                                        var newuser = res.body.businessUser[res.body.businessUser.length - 1];
                                                        should.exist(newuser.user);
                                                        newuser.businessUnits.length.should.equal(1);
                                                        newuser.status.should.equal('Request');
                                                        done();
                                                    });
                                            });
                                    });

                            });
                    });
            });
    });

    /**
     *  case 1: Create businessUser that user is already registered -Ve test case
     */
    it('should not be able to create business user if user is already registered', function (done) {
        var regUser = {
            username: 'userregistered@test.com',
            companyName: 'nVipani1',
            businessType: 'Trader',
            categorySeller: true,
            categoryBuyer: true,
            categoryMediator: true,
            acceptTerms: true
        };
        commonUserUtil.createEachStepUser({
            username: regUser.username,
            password: 'password',
            issendotp: true
        }, 200, agent, function (presendotpErr, presendotpRes) {
            should.not.exist(presendotpErr);
            presendotpRes.body.status.should.equal(true);
            var otp = presendotpRes.body.otp;
            presendotpRes.body.message.should.equal('An OTP has been sent to Email :' + regUser.username + '. ' + otp + ' is your One Time Password (OTP)');
            commonUserUtil.createEachStepUser({
                username: regUser.username,
                password: 'password',
                isverifyotp: true,
                otp: otp
            }, 200, agent, function (verifyotpErr, verifyotpRes) {
                should.not.exist(verifyotpErr);
                verifyotpRes.body.status.should.equal(true);
                verifyotpRes.body.message.should.equal('User is verified :' + regUser.username);
                var registrationCategory = verifyotpRes.body.registrationCategories[0]._id;
                var segments = [{
                    segment: verifyotpRes.body.segments[0]._id,
                    categories: [{category: verifyotpRes.body.categories[0]._id}]
                }];
                commonUserUtil.createEachStepUser({
                    username: regUser.username,
                    password: 'password',
                    ispassword: true,
                    registrationCategory: registrationCategory,
                    selectedSegments: segments,
                    companyName: regUser.companyName
                }, 200, agent, function (verifiedErr, verifiedRes) {
                    should.not.exist(verifiedErr);
                    verifiedRes.body.status.should.equal(true);
                    verifiedRes.body.message.should.equal('User is successfully registered');
                    var user1 = verifiedRes.body.user;

                    businessUser = {
                        user: user1._id,
                        userName: 'userregistered@test.com',
                        company: user1.company,
                        userGroup: user1.companies[0].userGroup,
                        bunits: [businessUnit._id]
                    };
                    agent.post('/auth/adduser')
                        .set('token', token)
                        .send(businessUser)
                        .expect(200)
                        .end(function (err, res) {
                            var newuser = res.body;
                            should.not.exist(newuser.user);
                            newuser.message.should.equal('User is already used for some other company');
                            done();
                        });
                });
            });
        });
    });

    /**
     * For all cases email id as user name
     *  case 1: Activate user with out statusToken -Ve Test case
     *  case 2: Activate user with statusToken +ve Test case
     *  case 3: Activate user with already registered user -ve Test case
     */
    it('should not be able to activate business user if user is not given token', function (done) {

        businessUser = {
            user: user._id,
            userName: 'BUser4@test.com',
            userGroup: user.companies[0].userGroup,
            company: user.company
        };
        createNewBusinessUser(businessUser, function (userErr, newUser) {
            should.not.exist(userErr);
            should.exist(newUser);
            agent.get('/auth/activeUser')
                .expect(200)
                .end(function (err, res) {
                    should.not.exist(res.body.user);
                    res.body.message.should.equal('should give status token for activate the user');
                    agent.get('/auth/activeUser?token=' + newUser.statusToken)
                        .expect(200)
                        .end(function (err, res) {
                            agent.post('/auth/activateUser/' + newUser.statusToken)
                                .send({
                                    username: businessUser.userName,
                                    password: 'nvipani123',
                                    statusToken: newUser.statusToken
                                })
                                .end(function (err, res) {
                                    should.not.exist(err);
                                    var regUser = res.body;
                                    regUser.status.should.equal('Registered');
                                    agent.get('/auth/activeUser?token=' + regUser.statusToken)
                                        .end(function (err, res) {
                                            res.body.message.should.equal('User is already employee in the nVipani');
                                            done();
                                        });
                                });
                        });
                });
        });

    });

    /**
     * case 1:Create business user to default business unit +ve test case
     * case 2:Check activate business user with out status token -ve test case
     * case 3:Check activate business user with wrong status token -ve test case
     * case 4:Get activated business user details for displaying at UI level with out status token -ve test case
     * case 5:Get activated business user details for displaying at UI level +ve test case
     * case 6:Activate business user to default business unit +ve test case
     * case 7:Login with activated business user
     * case 8:Try to registered with activated business user name -ve test case
     * case 9:Delete business user at company and try to registered with that business user name -ve test case
     */
    it('Should create business user and activated than redirected to login', function (done) {
        businessUser = {
            user: user._id,
            userName: 'userActivateLogin@nvipani.com',
            company: user.company,
            userGroup: user.companies[0].userGroup
        };
        createNewBusinessUser(businessUser, function (err, newUser) {
            should.exist(newUser);
            agent.get('/auth/activeUser?token=')
                .expect(400)
                .end(function (tokenErr, tokenRes) {
                    should.exist(tokenRes);
                    tokenRes.body.message.should.equal('should give status token for activate the user');
                    agent.get('/auth/activeUser?token=4448546sd4d45')
                        .expect(400)
                        .end(function (token1Err, token1Res) {
                            should.exist(token1Res);
                            token1Res.body.message.should.equal('No user with status token - 4448546sd4d45');
                            agent.get('/auth/activeUser?token=' + newUser.statusToken)
                                .expect(200)
                                .end(function (activeErr, activeRes) {
                                    agent.get('/auth/employeeDetailsForActivate/123456789')
                                        .expect(400)
                                        .end(function (userDeatil1Err, userDetail1Res) {
                                            should.exist(userDetail1Res);
                                            userDetail1Res.body.message.should.equal('No user with status token - 123456789');
                                            agent.get('/auth/employeeDetailsForActivate/' + newUser.statusToken)
                                                .expect(200)
                                                .end(function (userErr, userRes) {
                                                    should.not.exist(userErr);
                                                    var regUser = userRes.body;
                                                    regUser.username.should.equal(businessUser.userName);
                                                    regUser.company.name.should.equal('nVipani');
                                                    should.exist(regUser.statusToken);
                                                    agent.post('/auth/activateUser/' + regUser.statusToken)
                                                        .send({
                                                            username: businessUser.userName,
                                                            password: 'nvipani123'
                                                        })
                                                        .expect(200)
                                                        .end(function (err, res) {
                                                            should.not.exist(err);
                                                            var newUser = res.body;
                                                            newUser.status.should.equal('Registered');
                                                            agent.post('/auth/signin')
                                                                .send(credentials)
                                                                .expect(200)
                                                                .end(function (err, res) {
                                                                    // Handle signin error
                                                                    should.not.exist(err);
                                                                    var userres = res.body;
                                                                    commonUserUtil.createEachStepUser({
                                                                        username: businessUser.userName,
                                                                        password: 'password',
                                                                        issendotp: true
                                                                    }, 400, agent, function (presendotpErr, presendotpRes) {
                                                                        should.not.exist(presendotpErr);
                                                                        should.exist(presendotpRes);
                                                                        presendotpRes.body.message.should.equal('User is already Registered');
                                                                        //Remove business users -ve with out company id
                                                                        agent.post('/auth/companyUserMassActions')
                                                                            .set('token', token)
                                                                            .send({
                                                                                companyEmployees: [newUser._id],
                                                                                isRemove: true,
                                                                                isEnable: false,
                                                                                isDisable: false
                                                                            })
                                                                            .end(function (removeUserErr1, removeUserRes1) {
                                                                                var data = removeUserRes1.body;
                                                                                should.exist(data.success);
                                                                                data.success.should.be.instanceof(Array);
                                                                                data.success.length.should.equal(1);

                                                                                commonUserUtil.createEachStepUser({
                                                                                    username: businessUser.userName,
                                                                                    password: 'password',
                                                                                    issendotp: true
                                                                                }, 400, agent, function (presendotpErr, presendotpRes) {
                                                                                    should.not.exist(presendotpErr);
                                                                                    should.exist(presendotpRes);
                                                                                    presendotpRes.body.message.should.equal('Not allow to register. Please contact customer care.');
                                                                                    done();
                                                                                });
                                                                            });
                                                                    });
                                                                });
                                                        });
                                                });
                                        });
                                });
                        });
                });
        });
    });

    /**
     * case 1:Create business user at company +ve test case
     * case 4:Try to registered with created business user -ve test case
     * case 2:Delete business user at company +ve test case
     * case 3:Try to registered with deleted business user -ve test case
     */
    it('Should not be able to registered with employee user name', function (done) {
        businessUser = {
            user: user._id,
            userName: 'registeredWithemployee@nvipani.com',
            company: user.company,
            userGroup: user.companies[0].userGroup
        };
        createNewBusinessUser(businessUser, function (err, newUser) {
            should.exist(newUser);
            commonUserUtil.createEachStepUser({
                username: businessUser.userName,
                password: 'password',
                issendotp: true
            }, 400, agent, function (presendotpErr, presendotpRes) {
                should.not.exist(presendotpErr);
                should.exist(presendotpRes);
                presendotpRes.body.message.should.equal('User is already used for some other company as employee');
                //delete business user +ve
                agent.post('/auth/companyUserMassActions')
                    .set('token', token)
                    .send({
                        companyEmployees: [newUser.user._id],
                        isRemove: true,
                        isEnable: false,
                        isDisable: false
                    })
                    .end(function (deleteUserErr, deleteUserRes) {
                        should.exist(deleteUserRes);
                        var data = deleteUserRes.body;
                        should.exist(data.success);
                        data.success.should.be.instanceof(Array);
                        data.success.length.should.equal(1);
                        commonUserUtil.createEachStepUser({
                            username: businessUser.userName,
                            password: 'password',
                            issendotp: true
                        }, 400, agent, function (presendotpErr, presendotpRes) {
                            should.not.exist(presendotpErr);
                            should.exist(presendotpRes);
                            presendotpRes.body.message.should.equal('Not allow to register. Please contact customer care.');
                            done();
                        });
                    });
            });
        });
    });
    /**
     * case 1:Create business user to default business unit +ve test case
     * case 2:Get business user and check created business user their or not +ve test case
     * case 3:Update business user with out employee id -ve test case
     * case 4:Update business user with wrong employee id -ve test case
     * case 5:Update business user with out company id -ve test case
     * case 6:Update business user with wrong company id -ve test case
     * case 7:Update business user with out userGroup id -ve test case
     * case 8:Check update of business user +ve test case
     */
    it('Should create business user and update business user', function (done) {
        businessUser = {
            user: user._id,
            userName: 'updateBusinessUser1@nvipani.com',
            company: user.company,
            userGroup: user.companies[0].userGroup
        };
        createNewBusinessUser(businessUser, function (err, newUser) {
            should.exist(newUser);
            var employee = newUser;
            agent.get('/companyBusinessUsers/' + user.company + '?page=1&limit=10')
                .set('token', token)
                .end(function (buserListErr, buserListRes) {
                    should.not.exist(buserListErr);
                    var usersList = buserListRes.body.employees;
                    should.exist(usersList);
                    agent.put('/auth/updateUser')
                        .set('token', token)
                        .send(
                            {
                                company: user.company,
                                group: user.companies[0].userGroup,
                                isGroup: true,
                                isActive: false,
                                isDisable: false,
                                isRemove: false
                            })
                        .end(function (updateNoUserErr, updateNoUserRes) {
                            should.exist(updateNoUserRes);
                            updateNoUserRes.body.message.should.equal('Company employee id should required to update employee');
                            agent.put('/auth/updateUser')
                                .set('token', token)
                                .send(
                                    {
                                        user: '5454864w445',
                                        company: user.company,
                                        group: user.companies[0].userGroup,
                                        isGroup: true,
                                        isActive: false,
                                        isDisable: false,
                                        isRemove: false
                                    })
                                .end(function (updateWrongUserErr, updateWrongUserRes) {
                                    should.exist(updateWrongUserRes);
                                    updateWrongUserRes.body.message.should.equal('Cast to ObjectId failed for value "5454864w445" at path "_id"');
                                    agent.put('/auth/updateUser')
                                        .set('token', token)
                                        .send(
                                            {
                                                user: newUser.user._id,
                                                group: user.companies[0].userGroup,
                                                isGroup: true,
                                                isActive: false,
                                                isDisable: false,
                                                isRemove: false
                                            })
                                        .end(function (updateNoCompanyErr, updateNoCompanuRes) {
                                            should.exist(updateNoCompanuRes);
                                            updateNoCompanuRes.body.message.should.equal('Company id should required to update employee');
                                            agent.put('/auth/updateUser')
                                                .set('token', token)
                                                .send(
                                                    {
                                                        user: newUser.user._id,
                                                        company: '5564564655456',
                                                        group: user.companies[0].userGroup,
                                                        isGroup: true,
                                                        isActive: false,
                                                        isDisable: false,
                                                        isRemove: false
                                                    })
                                                .end(function (updateWrongCompanyErr, updateWrongCompanyRes) {
                                                    should.exist(updateWrongCompanyRes);
                                                    updateWrongCompanyRes.body.message.should.equal('Cast to ObjectId failed for value "5564564655456" at path "_id"');
                                                    agent.put('/auth/updateUser')
                                                        .set('token', token)
                                                        .send(
                                                            {
                                                                user: newUser.user._id,
                                                                company: user.company,
                                                                isGroup: true,
                                                                isActive: false,
                                                                isDisable: false,
                                                                isRemove: false
                                                            })
                                                        .end(function (updateNoUserGroupErr, updateNoUserGroupRes) {
                                                            should.exist(updateNoUserGroupRes);
                                                            updateNoUserGroupRes.body.message.should.equal('Employee group id should required to update employee');
                                                            agent.put('/auth/updateUser')
                                                                .set('token', token)
                                                                .send(
                                                                    {
                                                                        user: newUser.user._id,
                                                                        company: user.company,
                                                                        group: user.companies[0].userGroup,
                                                                        isGroup: true,
                                                                        isActive: false,
                                                                        isDisable: false,
                                                                        isRemove: false
                                                                    })
                                                                .end(function (updateUSerErr, updateUserRes) {
                                                                    should.exist(updateUserRes);
                                                                    var updateUser = updateUserRes.body;
                                                                    should.exist(updateUser);
                                                                    done();
                                                                });
                                                        });
                                                });
                                        });
                                });
                        });

                });
        });
    });

    afterEach(function (done) {
        BusinessUnit.remove();
        done();
    });
    after(function (done) {
        User.remove().exec();
        Company.remove().exec();

        done();
    });
});
