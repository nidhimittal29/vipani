'use strict';
var async=require('async'),
    should = require('should'),
    commonUtils=require('./common.utils'),
    config = require('../../../config/config'),
    _this=this,
    customers=[],
    suppliers=[],
    contacts=[];
exports.setCustomer=function (contact) {
    customers.push(contact);
};
exports.setSupplier=function (contact) {
    suppliers.push(contact);
};

exports.getUserSupplierByUser=function (username) {
    return suppliers.filter(function (eachSupplier) {
        return eachSupplier.user.username===username;
    });

};
exports.getUserCustomersByUser=function (username) {
    return customers.filter(function (eachCustomer) {
        return eachCustomer.user.username===username;
    });

};
exports.getContactByName=function(contactName,user) {
    return contacts.filter(function (eachContact) {
        return eachContact.user.username===user.username &&
            (eachContact.phone.filter(function(eachContact){return eachContact.phoneNumber===contactName; }).length>0 || eachContact.phone.filter(function(eachContact){return eachContact.email===contactName;}).length>0);
    });
};
function createContact(eachBatchContact,token,agent,done) {
    agent.post('/contacts')
        .send(eachBatchContact)
        .set('token',token)
        .expect(200)
        .end(function (eachBatchContactError, eachBatchContactRes) {
            if (eachBatchContactError) {
                done(eachBatchContactError,null);
            } else {
                contacts.push(eachBatchContactRes.body);
                if (eachBatchContact.customerType === 'Customer') {
                    customers.push(eachBatchContactRes.body);
                } else if (eachBatchContact.customerType === 'Supplier') {
                    suppliers.push(eachBatchContactRes.body);
                }
                done(null,eachBatchContactRes);
            }
        });

}
exports.batchCreate=function(contacts,agent,token,done) {
    async.forEachSeries(contacts, function (eachBatchContact, callback) {
        createContact(eachBatchContact,token, agent, function (err,contact) {
            if (err) {
                callback(err);
            } else {
                callback();
            }

        });
    }, function (err) {
        done(err);
    });


};
exports.createContact=function (contact,token,agent,done) {
    createContact(contact, token, agent, function (err,eachBatchContactRes) {

        done(err,eachBatchContactRes);

    });

};
exports.updateContact=function (contact,token,agent,done) {
    agent.put('/contacts')
        .send(contact)
        .set('token',token)
        .expect(200)
        .end(function (eachBatchContactError, eachBatchContactRes) {
            done(eachBatchContactError,eachBatchContactRes);

        });

};
exports.getContactById=function (contactId,token,agent,done) {
    agent.get('/contacts/'+contactId)
        .set('token',token)
        .expect(200)
        .end(function (fetchContactError, fetchContactRes) {
            done(fetchContactError,fetchContactRes);

        });

};
exports.getCustomers=function (contactId,token,agent,done) {
    agent.get('/contacts/customers')
        .set('token',token)
        .expect(200)
        .end(function (fetchContactError, fetchContactRes) {
            done(fetchContactError,fetchContactRes);

        });

};

exports.getSuppliers=function (token,agent,done) {
    agent.get('/contacts/suppliers')
        .set('token',token)
        .expect(200)
        .end(function (fetchContactError, fetchContactRes) {
            done(fetchContactError,fetchContactRes);

        });

};
exports.getUserNameByContact=function (contact) {
    if(contact.emails && contact.emails.length>0){
        return contact.emails[0].email;
    }else if(contact.phones && contact.phones.length>0){
        return contact.phones[0].phoneNumber;
    }else{
        return '';
    }

};
exports.getContactByUserName=function (userName) {
    var contact={};
    var expMail = new RegExp(config.emailRegEx);
    var expPhone = new RegExp(config.phoneEx);
    if(expMail.test(userName)){

        contact.displayName=userName;
        contact.emails=[{email:userName}];
    }else if( expPhone.test(userName)){

        contact.displayName=userName;
        contact.phones=[{phoneNumber:userName}];
    }
    return contact;
};
