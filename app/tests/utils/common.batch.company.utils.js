'use strict';
var async=require('async'),
    should = require('should'),
    commonUtil=require('./common.utils'),
    _this=this,
    manufacturers=[],
    retailers=[],
    users=[],
    distributors=[];
exports.getUserCompanyDefaultBusinessUnit=function (token,agent,done) {
    agent.get('/companyDefaultBusinessUnit')
        .set('Content-Type', 'application/json')
        .set('token', token)
        .expect(200)
        .end(function (defaultBusinessUnitErr, defaultBusinessUnit){
            done(defaultBusinessUnitErr, defaultBusinessUnit);
        });
};
exports.getBusinessUnit=function (businessUnit,token,agent,done) {
    if(businessUnit instanceof Object && businessUnit.isAdd) {
        agent.post('/branches')
            .set('Content-Type', 'application/json')
            .set('token', token)
            .send(businessUnit)
            .expect(200)
            .end(function (defaultBusinessUnitErr, getBusinessUnit){
                done(defaultBusinessUnitErr, getBusinessUnit);
            });

    }else if(businessUnit) {
        done(null, businessUnit);
    }else{
        _this.getUserCompanyDefaultBusinessUnit(token,agent,function (defaultBusinessUnitErr,defaultBusinessUnit) {
            done(defaultBusinessUnitErr, defaultBusinessUnit);
        });
    }

};

