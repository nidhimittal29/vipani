'use strict';
var async=require('async'),
    should = require('should'),
    commonUtil=require('./common.utils'),
    mongoose = require('mongoose'),
    _this=this,
    productCategories;
exports.findByProductCategories=function (token,agent,response,done) {
    agent.get('/querySubCategories2')
        .set('Content-Type', 'application/json')
        .set('token', token)
        .expect(response)
        .end(function (productCategoriesErr, productCategories){
            done(productCategoriesErr,productCategories);
        });
};
exports.findBySubCategories=function (token,agent,response,done) {
    agent.get('/querySubCategories1')
        .set('Content-Type', 'application/json')
        .set('token', token)
        .expect(response)
        .end(function (subCategoriesErr, subCategories){
            done(subCategoriesErr,subCategories);
        });

};
exports.findByMainCategories=function (token,agent,response,done) {
    agent.get('/queryMainCategories')
        .set('Content-Type', 'application/json')
        .set('token', token)
        .expect(response)
        .end(function (mainCategoriesErr, mainCategories){
            done(mainCategoriesErr,mainCategories);
        });
};
