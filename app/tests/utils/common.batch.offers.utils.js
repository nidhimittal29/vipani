'use strict';
var async=require('async'),
    should = require('should'),
    commonUtils=require('./common.utils'),
    _this=this,
    manufacturers=[],
    retailers=[],
    offers=[];
function createOffer(offer,token,response,agent,done){
    agent.post('/offers')
        .send(offer)
        .set('token',token)
        .expect(response)
        .end(function (createErr, eachOffer) {
            if(createErr){
                done(createErr,null);
            }else {
                offers.push(eachOffer.body);
                done(null,eachOffer);
            }
        });

}
exports.getOrderByOrderNumber=function(offerNumber){
    return offers.filter(function (eachOffer) {
        return eachOffer.offerNumber===offerNumber;
    });

};
exports.batchCreate=function (offers,token,agent,done) {
    async.forEachSeries(offers,function (eachOffer,callback) {
        createOffer(eachOffer,token,200,agent,function (eachOfferErr,eachOffer) {
            if(eachOfferErr){
                callback(eachOfferErr);
            }else {
                offers.push(eachOffer.body);
                callback();
            }
        });
    },function (err) {
        done(err);
    });

};
exports.addOffer=function (offer,token,response,agent,done) {
    createOffer(offer,token,response,agent,function (eachOfferErr,createOffer) {
        done(eachOfferErr,createOffer);
    });

};

exports.updateOffer=function(offer,token,agent,done){
    agent.put('/offers')
        .send(offer)
        .set('token',token)
        .expect(200)
        .end(function (createErr, eachOffer) {
            done(createErr,eachOffer);

        });

};
exports.copyOffer=function(offer,token,agent,done){
    agent.put('/cloneOffer/'+offer._id)
        .send(offer)
        .set('token',token)
        .expect(200)
        .end(function (createErr, eachOffer) {
            done(createErr,eachOffer);
        });

};
exports.getOfferById=function (offerId,token,agent,done) {
    agent.get('/offers/'+offerId)
        .set('token',token)
        .expect(200)
        .end(function (fetchErr, eachOffer) {
            done(fetchErr,eachOffer);
        });

};

exports.getOffers=function (token,businessUnit,agent,done) {
    if(businessUnit){
        businessUnit='?businessUnitId='+businessUnit;
    }else{
        businessUnit='';
    }
    agent.get('/offers'+businessUnit)
        .set('token',token)
        .expect(200)
        .end(function (fetchErr, allOffers) {
            done(fetchErr,allOffers);

        });

};

exports.getReceivedOffers=function (token,agent,done) {
    agent.get('/receivedOffers')
        .set('token',token)
        .expect(200)
        .end(function (fetchErr, allReceivedOffers) {

                done(fetchErr,allReceivedOffers);

        });

};

exports.getPublicOffers=function (token,agent,done) {
    agent.get('/publicOffers')
        .set('token',token)
        .expect(200)
        .end(function (fetchErr, allPublicOffers) {

                done(fetchErr,allPublicOffers);

        });

};
exports.getCompanyContactOffers=function (token,agent,done) {
    agent.get('/offerusercontacts')
        .set('token',token)
        .expect(200)
        .end(function (fetchErr, userContactsPublicOffers) {

            done(fetchErr,userContactsPublicOffers);

        });

};
