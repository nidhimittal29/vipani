'use strict';
var async=require('async'),
    should = require('should'),
    commonUtil=require('./common.utils'),
    mongoose = require('mongoose'),
    inventories=[],
    productBrandsLists=[],allProducts=[],products=[],conversionProducts=[],businessUnit,
    productBrands=[];
exports.getUserInventories=function (user) {
    return inventories.filter(function (eachInventory) {
        return eachInventory.user.username.toString()=== user;
    });
};
function setProductBrand(brand) {
    productBrands.push(brand);
}
exports.getProductBrands=function () {
    return productBrands;
};
exports.getProducts=function () {
    return products;
};
exports.getConversionProducts=function () {
    return conversionProducts;
};
function findOrCreateProductBrand(productCategory1,eachProduct,eachBrand,index,agent,token,done) {
     var matchedProductBrand=products[products.indexOf(eachProduct)].productBrands[eachProduct.productBrands.indexOf(eachBrand)];
    if(matchedProductBrand.productBrand && matchedProductBrand.name===eachBrand.name+' '+index) {
        done(null,eachBrand);
    }else{
        eachBrand.productBrand=null;
        eachBrand.name=(eachBrand.name+' '+index).trim();
        var brandUnitMeasures = commonUtil.getMatchedUOMS(productCategory1.unitOfMeasures, eachBrand.unitOfMeasureNames);
        products[products.indexOf(eachProduct)].productBrands[eachProduct.productBrands.indexOf(eachBrand)].unitOfMeasures = commonUtil.getMatchedUOMSObject(productCategory1.unitOfMeasures, eachBrand.unitOfMeasureNames);
        var brand = {
            'name': eachBrand.name,
            'productCategory': productCategory1._id,
            'hsncode': commonUtil.getMatchedHsncodeName(productCategory1.hsnCodes, eachBrand.hsncode)[0]._id,
            'unitOfMeasures': brandUnitMeasures,
            'taxGroup': commonUtil.getMatchedTaxGroup(productCategory1.taxGroups, eachBrand.taxGroup)[0]._id
        };
        agent.post('/productBrand')
            .set('Content-Type', 'application/json')
            .set('token', token)
            .send(brand)
            .expect(200)
            .end(function (productBrand1Err, productBrand1) {
                if(productBrand1Err) done(productBrand1Err,null);
                else {
                    productBrand1.body.name.should.be.equal(eachBrand.name);
                    products[products.indexOf(eachProduct)].productBrands[eachProduct.productBrands.indexOf(eachBrand)].productBrand = productBrand1.body._id;
                    setProductBrand(productBrand1.body);
                    done(null,productBrand1.body);
                }
            });
    }

}
function createProduct(eachProduct,subCategories,index,token,agent,done) {
    var subCategory1=commonUtil.getCategoryByName(subCategories,eachProduct.categoryName)[0];
    subCategory1.type.should.equal('SubCategory1');
    subCategory1.productCategory.should.equal(false);
    //subCategory1.children.should.be.length.toBeGreaterThan(0);
    var productCategory=commonUtil.getCategoryByName(subCategory1.children,eachProduct.name);
    productCategory.should.be.instanceof(Array).and.have.lengthOf(1);
    productCategory=productCategory[0];
    productCategory.productCategory.should.equal(true);
    productCategory.type.should.equal('SubCategory2');
    agent.get('/categories/'+productCategory._id)
        .set('Content-Type', 'application/json')
        .set('token', token)
        .expect(200)
        .end(function (subCategories2Err, subCategories2){
            var productCategory1=subCategories2.body;
            /*productCategory1.children.should.be.undefined;*/
            productCategory1.productCategory.should.equal(true);
            productCategory1.type.should.equal('SubCategory2');
            var productGradeDefinition=productCategory1.productAttributes.grade.definition,
                qualityDefinition=productCategory1.productAttributes.quality.definition;
            async.forEachSeries(eachProduct.productBrands,function (eachBrand,brandCallBack) {
                findOrCreateProductBrand(productCategory1,eachProduct,eachBrand,index,agent,token,function (productBrandErr,foundBrand) {
                    if(productBrandErr){
                        brandCallBack(productBrandErr);
                    }else {
                        var eachProductInventory = commonUtil.updateInventoryUOM(products[products.indexOf(eachProduct)].productBrands[eachProduct.productBrands.indexOf(eachBrand)]);
                        if (eachProductInventory.inventory && !eachProductInventory.inventory._id) {
                            eachProductInventory.inventory[0].businessUnit=businessUnit;
                            if(eachProductInventory.inventory[0].hsncode && eachProductInventory.inventory[0].hsncode._id){
                                eachProductInventory.inventory[0].hsncode=eachProductInventory.inventory[0].hsncode._id;
                            }
                            if(eachProductInventory.inventory[0].taxGroup && eachProductInventory.inventory[0].taxGroup._id){
                                eachProductInventory.inventory[0].taxGroup=eachProductInventory.inventory[0].taxGroup._id;
                            }
                            if(eachProductInventory.inventory[0].productBrand && eachProductInventory.inventory[0].productBrand._id){
                                eachProductInventory.inventory[0].productBrand=eachProductInventory.inventory[0].productBrand._id;
                            }
                            eachProductInventory.inventory[0]._id=mongoose.Types.ObjectId();
                            eachProductInventory.inventory[0].stockMasters=[];
                            eachProductInventory.businessUnit=businessUnit;
                            agent.post('/inventories/createInventoryMaster')
                                .send(eachProductInventory)
                                .set('Content-Type', 'application/json')
                                .set('token', token)
                                .expect(200)
                                .end(function (inventorySaveErr, inventorySaveRes) {
                                    // Handle Inventory save error
                                    if (inventorySaveErr){
                                        brandCallBack(inventorySaveErr);
                                    }
                                    else {
                                        if (products[products.indexOf(eachProduct)].productBrands[eachProduct.productBrands.indexOf(eachBrand)].inventory) {
                                            products[products.indexOf(eachProduct)].productBrands[eachProduct.productBrands.indexOf(eachBrand)].inventory = inventorySaveRes.body;
                                            //commonUtil.conversionSourceInventories(conversionProducts, eachProduct, products[products.indexOf(eachProduct)].productBrands[eachProduct.productBrands.indexOf(eachBrand)]);
                                        }
                                        brandCallBack();
                                    }
                                });
                        } else {
                            brandCallBack();
                        }
                    }

                });
            },function (err) {
                done(err);
            });

        });
}

exports.batchCreate=function(inventories,index,token,agent,done) {
    products=inventories.inventories;
    conversionProducts=inventories.convertInventories;
    businessUnit=inventories.businessUnit;
    agent.get('/querySubCategories1')
        .set('Content-Type', 'application/json')
        .set('token', token)
        .expect(200)
        .end(function (subCategories1Err, subCategories1){
            if(subCategories1Err){
                done(subCategories1Err);
            }
            else {
                subCategories1.body.should.be.instanceof(Array).and.have.lengthOf(6);
                var subCategories = subCategories1.body;
                async.forEachSeries(products, function (eachProduct, productCallback) {
                    createProduct(eachProduct, subCategories, index, token, agent, function (err) {
                        if (err) {
                            productCallback(err);
                        } else {
                            productCallback();
                        }
                    });

                }, function (err) {
                    done(err);
                });

            }

        });

};
exports.addInventory=function (product,index,agent,token,done) {
    agent.get('/querySubCategories1')
        .set('Content-Type', 'application/json')
        .set('token', token)
        .expect(200)
        .end(function (subCategories1Err, subCategories1) {
            subCategories1.body.should.be.instanceof(Array).and.have.lengthOf(5);
            var subCategories = subCategories1.body;
            createProduct(product, subCategories,index, token,agent, function (err) {
                done(err);
            });
        });

};
exports.getInventoryById=function (inventoryById,token,agent,done) {
    agent.get('/inventories/'+inventoryById)
        .set('Content-Type', 'application/json')
        .set('token', token)
        .expect(200)
        .end(function (inventoryFetchErr, inventory) {
                done(inventoryFetchErr,inventory);

        });
};

exports.updateInventoryById=function (inventory,agent,token,done) {
    agent.get('/inventories'+inventory._id)
        .send(inventory)
        .set('Content-Type', 'application/json')
        .set('token', token)
        .expect(200)
        .end(function (inventoryFetchErr, inventory) {
            done(inventoryFetchErr,inventory);
        });
};

exports.getInventories=function(token,businessUnit,agent,done) {
    if(token) {
        agent.get('/busUnitInventories/'+businessUnit)
            .set('token', token)
            .expect(200)
            .end(function (inventoryFetchErr, inventoriesRes) {
                done(inventoryFetchErr, inventoriesRes);

            });
    }else{
        agent.get('/inventories')
            .expect(200)
            .end(function (inventoryFetchErr, inventoriesRes) {
                done(inventoryFetchErr, inventoriesRes);

            });
    }
};
// This is for filter by brand and categories, UOM
exports.fetchInventoriesByFilter=function (query,businessUnit,token,agent,done) {
    agent.post('/inventories/matchedinventories')
        .send({inventories: query,businessUnit:businessUnit})
        .set('Content-Type', 'application/json')
        .set('token', token)
        .expect(200)
        .end(function (inventoryFetchErr, matchedInventoryResults) {
           done(inventoryFetchErr,matchedInventoryResults);
        });
};
