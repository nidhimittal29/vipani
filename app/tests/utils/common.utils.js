'use strict';
var async=require('async'),
    _this=this;

exports.getSegmentCategories=function(categories) {
    var selectedCategoriesArray=[];
    categories.filter(function (childCategory){
        selectedCategoriesArray.push({category:childCategory.category._id,enabled:childCategory.enabled});
    });
    return selectedCategoriesArray;
};


exports.getUOMByName=function (unitMeasureNames,unitMeasureName) {
    if(unitMeasureName && unitMeasureName._id){
        return [unitMeasureName];
    } else if(unitMeasureName) {
        return unitMeasureNames.filter(function (unitOfMeasure) {
            return (unitMeasureName && unitOfMeasure && unitOfMeasure.name && unitOfMeasure.name.toLowerCase() === unitMeasureName.toLowerCase());
        });
    }else {
        return [];
    }

};
exports.getMatchedUOMS=function(unitOfMeasures,unitMeasureNames) {
    var listOfUnitOfMeasure=[];
    async.forEachSeries(unitMeasureNames,function (unitMeasureName,callback) {
        var unitMeasure=_this.getUOMByName(unitOfMeasures,unitMeasureName);
        if(unitMeasure.length>0){
            listOfUnitOfMeasure.push(unitMeasure[0]._id);
        }
        callback();

    });
    return listOfUnitOfMeasure;

};
exports.getMatchedUOMSObject=function(unitOfMeasures,unitMeasureNames) {
    var listOfUnitOfMeasure=[];
    async.forEachSeries(unitMeasureNames,function (unitMeasureName,callback) {
        var unitMeasure=_this.getUOMByName(unitOfMeasures,unitMeasureName);
        if(unitMeasure.length>0){
            listOfUnitOfMeasure.push(unitMeasure[0]);
        }
        callback();

    });
    return listOfUnitOfMeasure;

};
exports.updateInventoryUOM=function (productBrand) {
    if(productBrand.inventory) {
        async.forEachSeries(productBrand.inventory, function (eachInventory, callback) {
            var unitMeasure = _this.getUOMByName(productBrand.unitOfMeasures, eachInventory.unitOfMeasure);
            if (unitMeasure.length > 0) {
                productBrand.inventory[productBrand.inventory.indexOf(eachInventory)].uom = unitMeasure[0]._id;
                productBrand.inventory[productBrand.inventory.indexOf(eachInventory)].unitOfMeasure = unitMeasure[0]._id;
            }
            callback();

        });
    }
    return productBrand;

};
exports.getCategoryByName=function (subCategories,categoryName) {
    return subCategories.filter(function (eachSubCategory) {
        return eachSubCategory.name===categoryName;
    });
};
exports.getMatchedHsncodeName=function (hsncodes,hsncodeName) {
    return hsncodes.filter(function (eachHsncode) {
        return eachHsncode.hsncode===hsncodeName;
    });
};
exports.getBrandByName=function (productBrands,brandName) {
    return productBrands.filter(function (productBrand) {
        return productBrand.name.toLowerCase()===brandName.toLowerCase();
    });
};
exports.getMatchedTaxGroup=function (taxGroups,taxGroupName,done) {
    return taxGroups.filter(function (taxGroup) {
        return taxGroup.name.toLowerCase()===taxGroupName.toLowerCase();
    });
};
exports.getMatchedSourceProductBrandInventory=function (conversionProductsInventory,eachInventory) {
    return conversionProductsInventory.filter(function (eachConversionProductInventory) {
        return eachConversionProductInventory.categoryName.toLowerCase()===eachInventory.productSubCategoryName.toLowerCase() && eachConversionProductInventory.productBrandName.toLowerCase()===eachInventory.brandName.toLowerCase() &&
            eachConversionProductInventory.unitOfMeasureName.toLowerCase()===eachInventory.unitOfMeasureName.toLowerCase() ;
    });

};
exports.getMatchedStockMaster=function (conversionProductStockMaster,eachInventory) {
    // We need to add inventory check as part of stockmaster unique.
    return eachInventory.stockMasters.filter(function (inventoryStockMaster) {
        return conversionProductStockMaster.batchNumber===inventoryStockMaster.batchNumber.toString() ;
    });
};
function matchedConvertedProductBrand(conversion,eachBrand) {
    return conversion.productBrandName.toString()=== eachBrand.name.toString();
}
exports.conversionSourceInventories=function (conversionProducts,eachProduct,eachBrand) {

    async.forEachSeries(eachBrand.inventory,function (eachInventory,callback) {

        async.forEachSeries(conversionProducts,function (eachConversion,callConversionback) {
            var conversionProductIndex=conversionProducts.indexOf(eachConversion);
            if(matchedConvertedProductBrand(eachConversion,eachBrand)){
                conversionProducts[conversionProductIndex].productBrand=eachBrand.productBrand;
            }
            var matchedInventory=_this.getMatchedSourceProductBrandInventory(eachConversion.inventories,eachInventory);
            var conversionInventoryIndex=conversionProducts[conversionProductIndex].inventories.indexOf(matchedInventory[0]);
            if(matchedInventory && matchedInventory.length>0){
                async.forEachSeries(matchedInventory[0].convertStockMasters,function (eachStockMaster,stockCallBack) {
                    var conversionInventoryStockIndex=conversionProducts[conversionProductIndex].inventories[conversionInventoryIndex].convertStockMasters.indexOf(eachStockMaster);
                    var matchedInventoryStockMaster =_this.getMatchedStockMaster(eachStockMaster,eachInventory);
                    if(matchedInventoryStockMaster && matchedInventoryStockMaster.length>0) {
                        conversionProducts[conversionProductIndex].inventories[conversionInventoryIndex]._id = eachInventory._id;
                        conversionProducts[conversionProductIndex].inventories[conversionInventoryIndex].convertStockMasters[conversionInventoryStockIndex]._id = matchedInventoryStockMaster[0]._id;
                    }
                    stockCallBack();
                });
            }
            callConversionback();

        });
        callback();
    });
    return conversionProducts;


};
