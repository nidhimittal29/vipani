'use strict';
var async=require('async'),
    should = require('should'),
    commonUtils=require('./common.utils'),
    _this=this,
    manufacturers=[],
    retailers=[],
    orders=[];
function createOrder(order,token,response,agent,done){
    agent.put('/createOrder'+(order.offerId?'/'+order.offerId:''))
        .send(order)
        .set('token',token)
        .expect(response)
        .end(function (createErr, eachOrder) {
            if(createErr){
                done(createErr,null);
            }else {
                orders.push(eachOrder.body);
                done(null,eachOrder);
            }
        });

}
exports.getOrderByOrderNumber=function(orderNumber){
    return orders.filter(function (eachOrder) {
        return eachOrder.orderNumber===orderNumber;
    });

};
exports.batchCreate=function (orders,token,agent,done) {

    async.forEachSeries(orders,function (eachOrder,callback) {
        createOrder(eachOrder,token,200,agent,function (eachOrderErr,order) {
            if(eachOrderErr){
                callback(eachOrderErr);
            }else {
                callback();
            }
        });
    },function (err) {
        done(err);
    });

};
exports.addOrder=function (order,token,response,agent,done) {

    createOrder(order,token,response,agent,function (eachOrderErr,order) {
            done(eachOrderErr,order);

    });

};
exports.getOrderById=function (orderId,token,agent,done) {
    agent.get('/orders/'+orderId)
        .set('token',token)
        .expect(200)
        .end(function (fetchErr, eachOrder) {
            done(fetchErr,eachOrder);
        });

};

exports.getOrders=function (orderId,token,agent,done) {
    agent.get('/orders')
        .set('token',token)
        .expect(200)
        .end(function (fetchErr, allOrders) {

                done(fetchErr,allOrders);

        });

};

exports.getReceivedOrders=function (orderId,token,agent,done) {
    agent.get('/orders')
        .set('token',token)
        .expect(200)
        .end(function (fetchErr, allOrders) {

                done(fetchErr,allOrders);

        });

};
