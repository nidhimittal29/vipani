'use strict';
var async=require('async'),
    should = require('should'),
    commonUtil=require('./common.utils'),
    inventories=[],
    productBrandsLists=[],allProducts=[],products=[],conversionProducts=[];

exports.getTracker=function(trackerFields,token,agent,done) {
    agent.get('/trackPaths?stockMasterTrackId=' + trackerFields.stockMasterId + '&inventoryTrackId=' + trackerFields.inventoryId)
        .set('Content-Type', 'application/json')
        .set('token', token)
        .expect(200)
        .end(function (forwardTrackErr,forwaredTrackerResponse) {
            done(forwardTrackErr,forwaredTrackerResponse);
        });

};
exports.getForwardTracker=function(inventoryTrackerFields,token,agent,done) {
    agent.get('/trackForwardPaths?stockMasterTrackId=' + inventoryTrackerFields.stockMasterId+ '&inventoryTrackId=' + inventoryTrackerFields.inventoryId)
        .set('Content-Type', 'application/json')
        .set('token', token)
        .expect(200)
        .end(function (forwardTrackErr,forwaredTrackerResponse) {
            done(forwardTrackErr,forwaredTrackerResponse);
        });

};
