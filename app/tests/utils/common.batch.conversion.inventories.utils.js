'use strict';
var async=require('async'),
    should = require('should'),
    commonUtil=require('./common.utils'),
    inventories=[],
    productBrandsLists=[],allProducts=[],products=[],conversionProducts=[];

exports.conversionCreate=function(conversionInventoryFields,token,agent,done) {
    agent.post('/inventories/createInventoryConversion')
        .send(conversionInventoryFields)
        .set('Content-Type', 'application/json')
        .set('token', token)
        .expect(200)
        .end(function (inventorySaveErr, convertInventoryResult) {
            done(inventorySaveErr,convertInventoryResult);
        });

};
