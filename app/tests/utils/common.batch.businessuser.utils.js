'use strict';
var async=require('async'),
    should = require('should'),
    commonUtil=require('./common.utils');

function createBusinessUser(employee,token,agent,done) {

    agent.post('/auth/adduser')
        .set('token', token)
        .send(employee)
        .end(function (userErr, userRes) {
            if(userErr){
                done(userErr,null);
            }else{
                var employee=userRes.body.businessUser[userRes.body.businessUser.length-1];
                done(null,employee);
            }
        });
}
exports.createActivateBusinessUser=function (regUser,userName,userGroup,bUnits,password,token,agent,done) {
    var userGroupId;
    if(userGroup === 'Admin')
        userGroupId=regUser.companies[0].userGroup;
    else if(userGroup === 'Manager')
        userGroupId=regUser.companies[1].userGroup;
    else
        userGroupId=regUser.companies[2].userGroup;
    var employee;
    if(bUnits.length>0) {
         employee= {
            user: regUser._id,
            userName: userName,
            company: regUser.company,
            userGroup: userGroupId,
             businessUnits:bUnits
        };
    }else{
        employee= {
            user: regUser._id,
            userName: userName,
            company: regUser.company,
            userGroup: userGroupId
        };
    }
    createBusinessUser(employee,token,agent,function (err,employees) {
        if(err){
            done(err,null);
        }else{
            agent.get('/auth/activeUser?token=' + employees.statusToken)
                .end(function (validateErr, validateRes) {
                    if(validateErr){
                        done(validateErr,null);
                    }else {
                        agent.post('/auth/activateUser/' + employees.statusToken)
                            .send({
                                username: userName,
                                password: password,
                                statusToken: employees.statusToken
                            })
                            .end(function (err, res) {
                                if (err) {
                                    done(err,null);
                                }else{
                                    done(null,res.body);
                                }
                            });
                    }
                });
        }
    });

};
