'use strict';

var should = require('should'),
    request = require('supertest'),
    app = require('../../server'),
    mongoose = require('mongoose'),
    User = mongoose.model('User'),
    Inventory = mongoose.model('Inventory'),
    Contact = mongoose.model('Contact'),
    Company = mongoose.model('Company'),
    Offers = mongoose.model('Offer'),
    Order=mongoose.model('Order'),
    Category = mongoose.model('Category'),
    agent = request.agent(app),
    commonUtil=require('./utils/common.utils'),
    commonBatchUserUtil=require('./utils/common.batch.users.utils'),
    commonBatchInventoriesUtil=require('./utils/common.batch.inventories.utils'),
    commonBatchInventoryTrackerUtil=require('./utils/common.batch.tracker.inventories.utils'),
    commonBatchCompanyUtil=require('./utils/common.batch.company.utils'),
    commonBatchInventoryConversionUtil=require('./utils/common.batch.conversion.inventories.utils'),
    async = require('async');


/**
 * Globals
 */
var credentials, user,regGlobalUsers=[], businessUnit,inventory, productCategory1,UOMS1,UOMS2,productBrandsLists=[],allProducts,products,conversionProducts,allInventories=[];

/**
 * Inventory routes tests
 */
describe('Inventory Track CRUD tests', function () {

    before(function (done) {
        var allUsers = require('./data/register.global.users');
        commonBatchUserUtil.batchCreate(allUsers,agent,function (userErr,regAllUsers) {
            if(userErr) done(userErr);
            else {
                commonBatchUserUtil.getUser(commonBatchUserUtil.getSelectedUser('nvipanimanufacturer@gmail.com')[0],agent,function (signinErr, signinRes) {
                    // Handle signin error
                    if (signinErr) done(signinErr);
                    else {
                        var token = signinRes.body.token;
                        var allProducts = require('./data/inventories.global.products');
                        commonBatchCompanyUtil.getUserCompanyDefaultBusinessUnit(token, agent, function (defaultBusinessUnitErr, bunit) {
                            if (defaultBusinessUnitErr) {
                                done(defaultBusinessUnitErr);
                            } else {
                                businessUnit = bunit.body.toString();
                                allProducts.businessUnit = bunit.body.toString();
                                var index=0;
                                commonBatchInventoriesUtil.batchCreate(allProducts, index,token, agent, function (productError) {
                                    if (productError) {
                                        done(productError);
                                    } else {
                                        products = commonBatchInventoriesUtil.getProducts();
                                        productBrandsLists = commonBatchInventoriesUtil.getProductBrands();
                                        done();
                                    }

                                });
                            }
                        });
                    }
                });
            }

        });

    });
    beforeEach(function (done) {
        commonBatchUserUtil.getUser(commonBatchUserUtil.getSelectedUser('nvipanimanufacturer@gmail.com')[0],agent,function (signinErr, signinRes) {
            // Handle signin error
            if (signinErr) done(signinErr);
            else {
                var token = signinRes.body.token;
                // get Inventory lists from user
                commonBatchInventoriesUtil.getInventories(token,businessUnit, agent, function (inventorySaveErr, inventories) {

                    allInventories = inventories.body.inventory;
                    allInventories.should.be.instanceof(Array);
                    done();
                });
            }
        });

    });
    it('should be able to track with the owner of inventory if logged in', function (done) {
        commonBatchUserUtil.getUser(commonBatchUserUtil.getSelectedUser('nvipanimanufacturer@gmail.com')[0],agent,function (signinErr,signinRes) {
            if (signinErr) {
                done(signinErr);
            }else {
                var token = signinRes.body.token;
                commonBatchInventoryTrackerUtil.getTracker({inventoryId:commonBatchInventoriesUtil.getProducts()[0].productBrands[0].inventory[0]._id,stockMasterId:commonBatchInventoriesUtil.getProducts()[0].productBrands[0].inventory[0].stockMasters[0]._id},token,agent, function (inventorySaveErr, trackInventoryResults) {
                    trackInventoryResults.body.destinations.length.should.be.equal(0);
                    trackInventoryResults.body.stockMasters[0].stockMaster.stockIn.other.length.should.be.equal(1);
                    trackInventoryResults.body.stockMasters[0].stockMaster.stockIn.inventories.length.should.be.equal(0);
                    trackInventoryResults.body.stockMasters[0].stockMaster.stockIn.orders.length.should.be.equal(0);
                    trackInventoryResults.body.inventory._id.toString().should.be.equal(commonBatchInventoriesUtil.getProducts()[0].productBrands[0].inventory[0]._id);
                    trackInventoryResults.body.stockMasters.length.should.be.equal(1);
                    trackInventoryResults.body.stockMasters[0].stockMaster._id.toString().should.be.equal(commonBatchInventoriesUtil.getProducts()[0].productBrands[0].inventory[0].stockMasters[0]._id);
                    done();
                });
            }

        });
    });
    it('should be able to track the Conversion inventory with same user in tracker if logged in', function (done) {
        commonBatchUserUtil.getUser(commonBatchUserUtil.getSelectedUser('nvipanimanufacturer@gmail.com')[0],agent,function (signinErr,signinRes) {
            if (signinErr) {
                done(signinErr);
            } else {
                var token = signinRes.body.token;
                var fetch = [{_id: allInventories[0]._id}];
                commonBatchInventoriesUtil.fetchInventoriesByFilter(fetch,businessUnit,token,agent,function (inventorySaveErr, sourceInventoryResult) {
                    var sourceInventory = sourceInventoryResult.body.inventories;
                    fetch = [{productBrand: allInventories[1].productBrand}];
                    commonBatchInventoriesUtil.fetchInventoriesByFilter(fetch,businessUnit, token, agent, function (inventorySaveErr, convertInventoryResult) {
                        var oldConvertInventory = convertInventoryResult.body.inventories[0];
                        var conversionInventoryFields = {
                            'productBrand': oldConvertInventory.productBrand,
                            'inventory': {
                                'businessUnit':businessUnit,
                                _id: oldConvertInventory._id,
                                'convertStockMasters': [{
                                    _id: oldConvertInventory.stockMasters[0]._id,
                                    'currentBalance': '4'
                                }],
                                'MRP': '20'
                            },
                            'inventories': [{
                                'inventory': {
                                    '_id': sourceInventory[0]._id,
                                    'convertStockMasters': [{
                                        '_id': sourceInventory[0].stockMasters[0]._id,
                                        'currentBalance': 2
                                    }]
                                }
                            }]
                        };
                        commonBatchInventoryConversionUtil.conversionCreate(conversionInventoryFields, token, agent, function (inventorySaveErr, convertInventoryResultValue) {
                            commonBatchInventoryTrackerUtil.getTracker({inventoryId:convertInventoryResultValue.body._id,stockMasterId:convertInventoryResultValue.body.stockMasters[0]._id},token,agent, function (inventorySaveErr, trackInventoryResults) {
                                done();
                            });


                        });
                    });
                });

            }
        });
    });
    it('should be able to track  the multiple at single conversion inventory in tracker if logged in', function (done) {
        agent.post('/auth/signin')
            .send(commonBatchUserUtil.getSelectedUser('nvipanimanufacturer@gmail.com')[0])
            .expect(200)
            .end(function (signinErr, signinRes) {
                // Handle signin error
                if (signinErr) done(signinErr);

                var token = signinRes.body.token;
                done();

            });
    });
    it('should be able to track the multiple level conversion inventory in tracker if logged in', function (done) {
        agent.post('/auth/signin')
            .send(commonBatchUserUtil.getSelectedUser('nvipanimanufacturer@gmail.com')[0])
            .expect(200)
            .end(function (signinErr, signinRes) {
                // Handle signin error
                if (signinErr) done(signinErr);

                var token = signinRes.body.token;
                done();

            });
    });
    it('should be able to track the inventory at offer inventory in tracker if logged in', function (done) {
        agent.post('/auth/signin')
            .send(commonBatchUserUtil.getSelectedUser('nvipanimanufacturer@gmail.com')[0])
            .expect(200)
            .end(function (signinErr, signinRes) {
                // Handle signin error
                if (signinErr) done(signinErr);

                var token = signinRes.body.token;
                done();

            });
    });
    it('should be able to track the inventory at purchase order inventory in tracker if logged in', function (done) {
        agent.post('/auth/signin')
            .send(commonBatchUserUtil.getSelectedUser('nvipanimanufacturer@gmail.com')[0])
            .expect(200)
            .end(function (signinErr, signinRes) {
                // Handle signin error
                if (signinErr) done(signinErr);

                var token = signinRes.body.token;
                done();

            });
    });

    it('should be able to track with out conversion inventory at buyer order inventory after delivery of order in tracker if logged in', function (done) {
        agent.post('/auth/signin')
            .send(commonBatchUserUtil.getSelectedUser('nvipanimanufacturer@gmail.com')[0])
            .expect(200)
            .end(function (signinErr, signinRes) {
                // Handle signin error
                if (signinErr) done(signinErr);

                var token = signinRes.body.token;
                done();

            });
    });
    it('should be able to track with conversion inventory at buyer order inventory after delivery of order in tracker if logged in', function (done) {
        agent.post('/auth/signin')
            .send(commonBatchUserUtil.getSelectedUser('nvipanimanufacturer@gmail.com')[0])
            .expect(200)
            .end(function (signinErr, signinRes) {
                // Handle signin error
                if (signinErr) done(signinErr);

                var token = signinRes.body.token;
                done();

            });
    });
    it('should be able to track the inventory at buyer with delivery inventory if logged in', function (done) {
        agent.post('/auth/signin')
            .send(commonBatchUserUtil.getSelectedUser('nvipanimanufacturer@gmail.com')[0])
            .expect(200)
            .end(function (signinErr, signinRes) {
                // Handle signin error
                if (signinErr) done(signinErr);

                var token = signinRes.body.token;
                done();

            });
    });
    it('should be able to track the conversion inventory at buyer with delivery inventory if logged in', function (done) {
        agent.post('/auth/signin')
            .send(commonBatchUserUtil.getSelectedUser('nvipanimanufacturer@gmail.com')[0])
            .expect(200)
            .end(function (signinErr, signinRes) {
                // Handle signin error
                if (signinErr) done(signinErr);

                var token = signinRes.body.token;
                done();

            });
    });

    after(function (done) {
        done();
    });
});
