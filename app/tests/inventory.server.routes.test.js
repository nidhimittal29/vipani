'use strict';

var should = require('should'),
    request = require('supertest'),
    async=require('async'),
    app = require('../../server'),

    mongoose = require('mongoose'),
    User = mongoose.model('User'),
    Inventory = mongoose.model('Inventory'),
    Contact = mongoose.model('Contact'),
    Company = mongoose.model('Company'),
    Category = mongoose.model('Category'),
    Product = mongoose.model('Product'),
    ProductBrand = mongoose.model('ProductBrand'),
    BusinessUnit = mongoose.model('BusinessUnit'),
    ItemMaster = mongoose.model('ItemMaster'),
    StockMaster = mongoose.model('StockMaster'),
    UnitOfMeasure = mongoose.model('UnitOfMeasure'),
    HsnCodes = mongoose.model('Hsncodes'),
    TaxGroup = mongoose.model('TaxGroup'),
    commonutil=require('../tests/utils/common.batch.users.utils'),
    commonBatchUserUtil=require('./utils/common.batch.users.utils'),
    commonBatchInventoriesUtil=require('./utils/common.batch.inventories.utils'),
    commonBatchCompanyUtil=require('./utils/common.batch.company.utils'),
    agent = request.agent(app);

/**
 * Globals
 */
var credentials, regUser,user, inventory, token,productInventory,productBrand,allInventories,businessUnitId,hsnCode,allProducts,products,productBrandsLists;

/**
 * Inventory routes tests
 */
describe('Inventory CRUD tests', function () {

    before(function (done) {
        regUser = {
            username: 'test@test.com',
            password: 'password',
            ConfirmPassword: 'password',
            firstName: 'First Name',
            lastName: 'Last Name',
            companyName: 'nVipani',
            businessType: 'Trader',
            registrationCategory: 'Manufacturer',
            selectedSegments: ['Coffee','Rice'],
            mobile: '0123456789',
            acceptTerms: true
        };
        commonutil.createUser(regUser, agent, function(err,res) {
            should.not.exist(err);
            user = res;
            credentials = {
                username: 'test@test.com',
                password: 'password'
            };
            done();
        });

    });
//inventories are created here.
    beforeEach(function (done) {
        allProducts = require('./data/products');
        products = allProducts.inventories;
        agent.post('/auth/signin')
            .send(credentials)
            .expect(200)
            .end(function (signinErr, signinRes) {
                // Handle signin error
                if (signinErr) done(signinErr);

                token = signinRes.body.token;
                commonBatchUserUtil.getUser(commonBatchUserUtil.getSelectedUser('test@test.com')[0], agent, function (signinErr, signinRes) {
                    // Handle signin error
                    should.not.exist(signinErr);
                    token = signinRes.body.token;

                    commonBatchCompanyUtil.getUserCompanyDefaultBusinessUnit(token, agent, function (defaultBusinessUnitErr, bunit) {
                        should.not.exist(defaultBusinessUnitErr);
                        businessUnitId = bunit.body.toString();
                        allProducts.businessUnit = bunit.body.toString();
                        products[0].productBrands[0].inventory = [{
                            MRP: 5000.23,
                            numberOfUnits: 1000,
                            manufacturingDate: new Date(),
                            packagingDate: new Date(),
                            moqAndMargin: [{
                                MOQ: '25',
                                margin: '.1'
                            }],
                            moqAndBuyMargin: [{
                                MOQ: '50',
                                margin: '.5'
                            }],
                            movAndMargin: [{
                                MOV: '10000',
                                margin: '.5'
                            }],
                            movAndBuyMargin: [{
                                MOV: '25000',
                                margin: '1'
                            }],
                            unitOfMeasure: 'Bag of 10Kgs',
                            batchNumber: 'Batch 10kg Rice  1'
                        }];
                        done();
                    });
                });
            });
    });

    it('should be able to save Inventory instance with new product if logged in', function (done) {
        commonBatchInventoriesUtil.batchCreate(allProducts, 0,token,agent, function (productError) {
            if (productError) {
                done(productError);
            } else {
                commonBatchInventoriesUtil.getInventories(token,businessUnitId,agent,function(inventoryErr,inventoriesRes){
                var inventories = inventoriesRes.body.inventory;
                    // Get a list of Inventories
                        // Set assertions
                        //(inventories.inventory[0].user._id).should.equal(user.id);
                var invToMatch = products[0].productBrands[0].inventory[0];
                        should.exist(inventories[1].unitOfMeasure.toString());
                        (inventories[1].MRP).should.equal(invToMatch.MRP);
                        should.exist(inventories[1].productBrand);
                        should.exist(inventories[1].stockMasters);
                        inventories[1].stockMasters.length.should.equal(1);
                        inventories[1].taxGroupName.should.equal(products[0].productBrands[0].taxGroup);
                        inventories[1].user._id.toString().should.equal(user._id);
                        BusinessUnit.findOne({_id:inventories[0].businessUnit},function(err,bu){
                            should.not.exist(err);
                            bu.inventories.length.should.equal(2);
                            done();
                        });
                    });
            }
        });
    });

    //it('should be able to display no inventories if business unit has no inventories',function(done){done();});

    it('should be able to update Inventory instance with existing product if logged in', function (done) {
                products = commonBatchInventoriesUtil.getProducts();
                productBrandsLists = commonBatchInventoriesUtil.getProductBrands();
                commonBatchInventoriesUtil.getInventories(token, businessUnitId, agent, function (inventoryErr, inventoriesRes) {
                    allInventories = inventoriesRes.body.inventory;

                    var newInventory = allInventories[0];
                    newInventory.MRP = 2500;
                    newInventory.numberOfUnits = 2000;
                    newInventory.moqAndMargin = [{
                        MOQ: 10,
                        margin: 20
                    }];

                    // Update Inventory
                    agent.put('/inventories/' + newInventory._id)
                        .send(newInventory)
                        .set('Content-Type', 'application/json')
                        .set('token', token)
                        .expect(200)
                        .end(function (inventorySaveErr, inventorySaveRes) {
                            // Handle Inventory save error
                            should.not.exist(inventorySaveErr);

                            // Get a list of Inventories
                            agent.get('/busUnitInventories/' + allInventories[0].businessUnit)
                                .set('Content-Type', 'application/json')
                                .set('token', token)
                                .end(function (inventoriesGetErr, inventoriesGetRes) {
                                    // Handle Inventory save error
                                    should.not.exist(inventoriesGetErr);

                                    // Get Inventories list
                                    var newInventories = inventoriesGetRes.body;

                                    newInventories.inventory.should.be.instanceof(Array).and.have.lengthOf(2);

                                    // Set assertions
                                    (newInventories.inventory[0].user._id).should.equal(user._id);
                                    (newInventories.inventory[0].unitOfMeasure).should.equal(newInventory.unitOfMeasure);
                                    (newInventories.inventory[0].MRP).should.equal(newInventory.MRP);
                                    should.exist(newInventories.inventory[0].productNum);

                                    // Call the assertion callback
                                    done();
                                });
                        });
                });
        });

    it('should not be able to save Inventory instance if not logged in', function (done) {
        commonBatchInventoriesUtil.batchCreate(allProducts, 0,'', agent, function (productError,res) {
            should.exist(productError);
            done();
        });
    });
    it('should be able to add batches to existing inventories',function(done){
                products = commonBatchInventoriesUtil.getProducts();
                productBrandsLists = commonBatchInventoriesUtil.getProductBrands();
                commonBatchInventoriesUtil.getInventories(token, businessUnitId, agent, function (inventoryErr, inventoriesRes) {
                    allInventories = inventoriesRes.body.inventory;

                    var inventory = allInventories[0];
                    agent.get('/inventories/' + inventory._id)
                        .set('Content-Type', 'application/json')
                        .set('token', token)
                        .end(function (getinvErr, getinvres) {
                            // Handle Inventory save error
                            should.not.exist(getinvErr);
                            var newInventory = getinvres.body;
                            var stockMaster = {
                                inventory: newInventory._id,
                                batchNumber: 'CERS-1020220118',
                                openingBalance: 2000,
                                currentBalance: 2500,
                                manufacturingDate: Date.now(),
                                businessUnit:businessUnitId
                            };

                            // Update Inventory
                            agent.post('/stockMaster/')
                                .send(stockMaster)
                                .set('Content-Type', 'application/json')
                                .set('token', token)
                                .expect(200)
                                .end(function (smErr, stockMasterRes) {
                                    // Handle Inventory save error
                                    should.not.exist(smErr);
                                    var updatedStockMaster = stockMasterRes.body;
                                    should.exist(updatedStockMaster._id);
                                    // Get a list of Inventories
                                    agent.get('/inventories/' + updatedStockMaster.inventory)
                                        .set('Content-Type', 'application/json')
                                        .set('token', token)
                                        .end(function (inventoriesGetErr, inventoriesGetRes) {
                                            // Handle Inventory save error
                                            if (inventoriesGetErr) done(inventoriesGetErr);

                                            // Get Inventories list
                                            var inventory = inventoriesGetRes.body;

                                            // Set assertions
                                            (inventory.user._id).should.equal(user._id);
                                            inventory.stockMasters.should.be.instanceof(Array).and.have.lengthOf(2);
                                            inventory.stockMasters[1].currentBalance.should.equal(stockMaster.currentBalance);
                                            inventory.stockMasters[1].openingBalance.should.equal(stockMaster.openingBalance);
                                            // Call the assertion callback
                                            done();
                                        });
                                });
                        });
                });
    });

    it('should be able to delete batches from inventory',function(done){
                products = commonBatchInventoriesUtil.getProducts();
                productBrandsLists = commonBatchInventoriesUtil.getProductBrands();
                commonBatchInventoriesUtil.getInventories(token, businessUnitId, agent, function (inventoryErr, inventoriesRes) {
                    allInventories = inventoriesRes.body.inventory;

                    var inventory = allInventories[0];
                    agent.get('/inventories/' + inventory._id)
                        .set('Content-Type', 'application/json')
                        .set('token', token)
                        .end(function (getinvErr, getinvres) {
                            // Handle Inventory save error
                            should.not.exist(getinvErr);
                            var newInventory = getinvres.body;
                            var stockMaster = {
                                inventory: newInventory._id,
                                batchNumber: 'CERS-1020220118',
                                openingBalance: 2000,
                                currentBalance: 2500,
                                manufacturingDate: Date.now(),
                                businessUnit:businessUnitId
                            };

                            // Update Inventory
                            agent.post('/stockMaster/')
                                .send(stockMaster)
                                .set('Content-Type', 'application/json')
                                .set('token', token)
                                .expect(200)
                                .end(function (smErr, stockMasterRes) {
                                    // Handle Inventory save error
                                    should.not.exist(smErr);
                                    var updatedStockMaster = stockMasterRes.body;
                                    should.exist(updatedStockMaster._id);
                                    // Get a list of Inventories
                                    agent.get('/inventories/' + updatedStockMaster.inventory)
                                        .set('Content-Type', 'application/json')
                                        .set('token', token)
                                        .end(function (inventoriesGetErr, inventoriesGetRes) {
                                            // Handle Inventory save error
                                            should.not.exist(inventoriesGetErr);

                                            // Get Inventories list
                                            var inventory = inventoriesGetRes.body;

                                            // Set assertions
                                            (inventory.user._id).should.equal(user._id);
                                            inventory.stockMasters.should.be.instanceof(Array).and.have.lengthOf(3);
                                            inventory.stockMasters[1].currentBalance.should.equal(stockMaster.currentBalance);
                                            inventory.stockMasters[1].openingBalance.should.equal(stockMaster.openingBalance);
                                            inventory.stockMasters.splice(1, 1);
                                            agent.put('/inventories/' + inventory._id)
                                                .send(inventory)
                                                .set('token', token)
                                                .end(function (delSMErr, delSMRes) {
                                                    should.not.exist(delSMErr);
                                                    agent.get('/inventories/' + inventory._id.toString())
                                                        .set('Content-Type', 'application/json')
                                                        .set('token', token)
                                                        .end(function (refErr, refInvRes) {
                                                            // Handle Inventory save error
                                                            should.not.exist(refErr);

                                                            // Get Inventories list
                                                            var refInventory = refInvRes.body;
                                                            refInventory.stockMasters.should.be.instanceof(Array).and.have.lengthOf(2);
                                                            done();
                                                        });
                                                });
                                            // Call the assertion callback
                                        });
                                });
                        });
                });
    });

    it('should be able to update discounts in inventory',function(done){
                products = commonBatchInventoriesUtil.getProducts();
                productBrandsLists = commonBatchInventoriesUtil.getProductBrands();
                commonBatchInventoriesUtil.getInventories(token, businessUnitId, agent, function (inventoryErr, inventoriesRes) {
                    allInventories = inventoriesRes.body.inventory;

                    var inventory = allInventories[0];
                    agent.get('/inventories/' + inventory._id)
                        .set('Content-Type', 'application/json')
                        .set('token', token)
                        .end(function (getinvErr, getinvres) {
                            // Handle Inventory save error
                            should.not.exist(getinvErr);
                            var newInventory = getinvres.body;
                            var moqVsMargin = {'MOQ': 12, 'margin': 1};
                            inventory.moqAndMargin.push(moqVsMargin);

                            // Update Inventory
                            agent.put('/inventories/' + newInventory._id)
                                .send(inventory)
                                .set('Content-Type', 'application/json')
                                .set('token', token)
                                .expect(200)
                                .end(function (smErr, invUpdate1Res) {
                                    // Handle Inventory save error
                                    should.not.exist(smErr);
                                    // Get a list of Inventories
                                    agent.get('/inventories/' + inventory._id)
                                        .set('Content-Type', 'application/json')
                                        .set('token', token)
                                        .end(function (inventoriesGetErr, inventoriesGetRes) {
                                            // Handle Inventory save error
                                            should.not.exist(inventoriesGetErr);

                                            // Get Inventories list
                                            var inventory = inventoriesGetRes.body;

                                            // Set assertions
                                            (inventory.user._id).should.equal(user._id);
                                            inventory.moqAndMargin.should.be.instanceof(Array).and.have.lengthOf(2);
                                            inventory.moqAndMargin[1].MOQ.should.equal(moqVsMargin.MOQ);
                                            inventory.moqAndMargin[1].margin.should.equal(moqVsMargin.margin);
                                            inventory.moqAndMargin.splice(0, 1);
                                            agent.put('/inventories/' + inventory._id)
                                                .send(inventory)
                                                .set('token', token)
                                                .end(function (delSMErr, delSMRes) {
                                                    should.not.exist(delSMErr);
                                                    agent.get('/inventories/' + inventory._id.toString())
                                                        .set('Content-Type', 'application/json')
                                                        .set('token', token)
                                                        .end(function (refErr, refInvRes) {
                                                            // Handle Inventory save error
                                                            should.not.exist(refErr);

                                                            // Get Inventories list
                                                            var refInventory = refInvRes.body;
                                                            refInventory.moqAndMargin.should.be.instanceof(Array).and.have.lengthOf(1);
                                                            done();
                                                        });
                                                });
                                            // Call the assertion callback
                                        });
                                });
                        });
                });
    });



    it('should be able to delete Inventory instance if signed in', function (done) {
                products = commonBatchInventoriesUtil.getProducts();
                productBrandsLists = commonBatchInventoriesUtil.getProductBrands();
                commonBatchInventoriesUtil.getInventories(token, businessUnitId, agent, function (inventoryErr, inventoriesRes) {
                    allInventories = inventoriesRes.body.inventory;
                    // Delete existing Inventory
                    agent.post('/inventoriesremove/')
                        .set('token', token)
                        .send({inventories: [{_id: allInventories[0]._id}]})
                        .expect(200)
                        .end(function (inventoryDeleteErr, inventoryDeleteRes) {
                            // Handle Inventory error error
                            should.not.exist(inventoryDeleteErr);

                            agent.get('/busUnitInventories/' + allInventories[0].businessUnit)
                                .set('Content-Type', 'application/json')
                                .set('token', token)
                                .end(function (inventoriesGetErr, inventoriesGetRes) {
                                    // Handle Inventory save error
                                    should.not.exist(inventoriesGetErr);
                                    var newInventories = inventoriesGetRes.body;

                                    newInventories.inventory.should.be.instanceof(Array).and.have.lengthOf(1);
                                    done();
                                });
                        });
                });
    });
    it('should be able to disable Inventory instance if signed in', function (done) {
                products = commonBatchInventoriesUtil.getProducts();
                productBrandsLists = commonBatchInventoriesUtil.getProductBrands();
                commonBatchInventoriesUtil.getInventories(token, businessUnitId, agent, function (inventoryErr, inventoriesRes) {
                    allInventories = inventoriesRes.body.inventory;
                    // Delete existing Inventory
                    agent.post('/inventoriesinactive/')
                        .set('token', token)
                        .send({inventories: [{_id: allInventories[0]._id}]})
                        .expect(200)
                        .end(function (inventoryDeleteErr, inventoryDeleteRes) {
                            // Handle Inventory error error
                            should.not.exist(inventoryDeleteErr);

                            agent.get('/busUnitInventories/' + allInventories[0].businessUnit)
                                .set('Content-Type', 'application/json')
                                .set('token', token)
                                .end(function (inventoriesGetErr, inventoriesGetRes) {
                                    // Handle Inventory save error
                                    should.not.exist(inventoriesGetErr);
                                    var newInventories = inventoriesGetRes.body;

                                    newInventories.inventory.should.be.instanceof(Array).and.have.lengthOf(1);
                                    for (var i = 0; i < newInventories.inventory.length; i++) {
                                        if (newInventories.inventory[i]._id === allInventories[0]._id.toString()) {
                                            newInventories.inventory[i].disabled.should.equal(true);
                                        }
                                    }
                                    done();
                                });
                        });
                });
    });

    it('should be able to enable Inventory instance if signed in', function (done) {
                products = commonBatchInventoriesUtil.getProducts();
                productBrandsLists = commonBatchInventoriesUtil.getProductBrands();
                commonBatchInventoriesUtil.getInventories(token, businessUnitId, agent, function (inventoryErr, inventoriesRes) {
                    allInventories = inventoriesRes.body.inventory;
                    // Delete existing Inventory
                    agent.post('/inventoriesinactive')
                        .set('token', token)
                        .send({inventories: [{_id: allInventories[0]._id}]})
                        .expect(200)
                        .end(function (inventoryDeleteErr, inventoryDeleteRes) {
                            // Handle Inventory error error
                            should.not.exist(inventoryDeleteErr);

                            agent.get('/busUnitInventories/' + allInventories[0].businessUnit)
                                .set('Content-Type', 'application/json')
                                .set('token', token)
                                .end(function (inventoriesGetErr, inventoriesGetRes) {
                                    // Handle Inventory save error
                                    should.not.exist(inventoriesGetErr);
                                    var newInventories = inventoriesGetRes.body;

                                    newInventories.inventory.should.be.instanceof(Array).and.have.lengthOf(1);
                                    for (var i = 0; i < newInventories.inventory.length; i++) {
                                        if (newInventories.inventory[i]._id === allInventories[0]._id.toString()) {
                                            newInventories.inventory[i].disabled.should.equal(true);
                                        }
                                    }
                                    agent.post('/inventoriesactive')
                                        .set('token', token)
                                        .send({inventories: [{_id: allInventories[0]._id}]})
                                        .expect(200)
                                        .end(function (invEnableErr, invEnableRes) {
                                            // Handle Inventory error error
                                            should.not.exist(invEnableErr);

                                            agent.get('/busUnitInventories/' + allInventories[0].businessUnit)
                                                .set('Content-Type', 'application/json')
                                                .set('token', token)
                                                .end(function (inventoriesGetErr, inventoriesGetRes) {
                                                    // Handle Inventory save error
                                                    should.not.exist(inventoriesGetErr);
                                                    var newInventories = inventoriesGetRes.body;

                                                    newInventories.inventory.should.be.instanceof(Array).and.have.lengthOf(1);
                                                    for (var i = 0; i < newInventories.inventory.length; i++) {
                                                        if (newInventories.inventory[i]._id === allInventories[0]._id.toString()) {
                                                            newInventories.inventory[i].disabled.should.equal(false);
                                                        }
                                                    }
                                                    done();
                                                });
                                        });

                                });
                        });
                });
    });

    it('should not be able to delete Inventory instance if not signed in', function (done) {
                products = commonBatchInventoriesUtil.getProducts();
                productBrandsLists = commonBatchInventoriesUtil.getProductBrands();
                commonBatchInventoriesUtil.getInventories(token, businessUnitId, agent, function (inventoryErr, inventoriesRes) {
                    allInventories = inventoriesRes.body.inventory;
                    // Create new Inventory model instance
                    agent.post('/inventoriesremove/')
                        .send({inventories: [{_id: allInventories[0]._id}]})
                        .expect(200)
                        .end(function (inventoryDeleteErr, inventoryDeleteRes) {
                            // Handle Inventory error error
                            should.exist(inventoryDeleteErr);
                            done();

                        });
        });
     });

    /**
     * case 1:Check Summary after inventory creation
     * case 2: Check summary after inventory creation with empty stock
     * case 3: Check Summary after batch added
     * case 4: Check summary after inactivate inventory
     * case 5: Check summary after activate inventory
     * case 6: Check summary after deletion
     */
    it('Check summay after performing actions',function (done) {
        products = commonBatchInventoriesUtil.getProducts();
        productBrandsLists = commonBatchInventoriesUtil.getProductBrands();
        //Check Newly added stock summary
        commonBatchInventoriesUtil.getInventories(token, businessUnitId, agent, function (inventoryErr, inventoriesRes) {
            allInventories = inventoriesRes.body.inventory;

            var newInventory = allInventories[0];
            newInventory.MRP = 2500;
            newInventory.numberOfUnits = 2000;
            newInventory.moqAndMargin = [{
                MOQ: 10,
                margin: 20
            }];

            agent.put('/inventories/' + newInventory._id)
                .send(newInventory)
                .set('Content-Type', 'application/json')
                .set('token', token)
                .expect(200)
                .end(function (inventorySaveErr, inventorySaveRes) {
                    // Handle Inventory save error
                    should.not.exist(inventorySaveErr);

                    agent.get('/inventorysummary?businessUnitId='+businessUnitId)
                        .set('token',token)
                        .expect(200)
                        .end(function (inventorySummaryErr, inventorySummary) {
                            should.not.exist(inventorySummaryErr);
                            should.exist(inventorySummary.body);
                            inventorySummary.body.NewlyAdded.should.equal(1);

                            //Check Out of Stock summary list
                            newInventory.numberOfUnits=0;
                            agent.put('/inventories/' + newInventory._id)
                                .send(newInventory)
                                .set('Content-Type', 'application/json')
                                .set('token', token)
                                .expect(200)
                                .end(function (inventorySaveErr, inventorySaveRes) {
                                    // Handle Inventory save error
                                    should.not.exist(inventorySaveErr);

                                    agent.get('/inventorysummary?businessUnitId=' + businessUnitId)
                                        .set('token', token)
                                        .expect(200)
                                        .end(function (inventorySummaryErr, inventorySummary) {
                                            should.not.exist(inventorySummaryErr);
                                            should.exist(inventorySummary.body);
                                            inventorySummary.body.OutOfStock.should.equal(1);
                                            inventorySummary.body.NewlyAdded.should.equal(1);

                                            //Added Batch
                                            agent.get('/inventories/' + newInventory._id)
                                                .set('Content-Type', 'application/json')
                                                .set('token', token)
                                                .end(function (getinvErr, getinvres) {
                                                    var newInventory = getinvres.body;
                                                    var stockMaster = {
                                                        inventory: newInventory._id,
                                                        batchNumber: 'CERS-1020220118',
                                                        openingBalance: 2000,
                                                        currentBalance: 2500,
                                                        manufacturingDate: Date.now(),
                                                        businessUnit:businessUnitId
                                                    };

                                                    // Update Inventory
                                                    agent.post('/stockMaster/')
                                                        .send(stockMaster)
                                                        .set('Content-Type', 'application/json')
                                                        .set('token', token)
                                                        .expect(200)
                                                        .end(function (smErr, stockMasterRes) {
                                                            // Handle Inventory save error
                                                            should.not.exist(smErr);
                                                            var updatedStockMaster = stockMasterRes.body;
                                                            should.exist(updatedStockMaster._id);

                                                            agent.get('/inventorysummary?businessUnitId=' + businessUnitId)
                                                                .set('token', token)
                                                                .expect(200)
                                                                .end(function (inventorySummaryErr, inventorySummary) {
                                                                    should.not.exist(inventorySummaryErr);
                                                                    should.exist(inventorySummary.body);
                                                                    inventorySummary.body.OutOfStock.should.equal(0);
                                                                    inventorySummary.body.NewlyAdded.should.equal(1);

                                                                    //Check summary after Inactive
                                                                    agent.post('/inventoriesinactive/')
                                                                        .set('token', token)
                                                                        .send({inventories: [{_id: newInventory._id}]})
                                                                        .expect(200)
                                                                        .end(function (inventoryDeleteErr, inventoryDeleteRes) {
                                                                            // Handle Inventory error error
                                                                            should.not.exist(inventoryDeleteErr);
                                                                            agent.get('/inventorysummary?businessUnitId=' + businessUnitId)
                                                                                .set('token', token)
                                                                                .expect(200)
                                                                                .end(function (inventorySummaryErr, inventorySummary) {
                                                                                    should.not.exist(inventorySummaryErr);
                                                                                    should.exist(inventorySummary.body);
                                                                                    inventorySummary.body.OutOfStock.should.equal(0);
                                                                                    inventorySummary.body.NewlyAdded.should.equal(1);
                                                                                    inventorySummary.body.Inactive.should.equal(1);

                                                                                    //Check summary after Activation
                                                                                    agent.post('/inventoriesactive/')
                                                                                        .set('token', token)
                                                                                        .send({inventories: [{_id: newInventory._id}]})
                                                                                        .expect(200)
                                                                                        .end(function (inventoryDeleteErr, inventoryDeleteRes) {
                                                                                            // Handle Inventory error error
                                                                                            should.not.exist(inventoryDeleteErr);
                                                                                            agent.get('/inventorysummary?businessUnitId=' + businessUnitId)
                                                                                                .set('token', token)
                                                                                                .expect(200)
                                                                                                .end(function (inventorySummaryErr, inventorySummary) {
                                                                                                    should.not.exist(inventorySummaryErr);
                                                                                                    should.exist(inventorySummary.body);
                                                                                                    inventorySummary.body.OutOfStock.should.equal(0);
                                                                                                    inventorySummary.body.NewlyAdded.should.equal(1);
                                                                                                    inventorySummary.body.Inactive.should.equal(0);

                                                                                                    //Check summary after deletion
                                                                                                    // Delete existing Inventory
                                                                                                    agent.post('/inventoriesremove/')
                                                                                                        .set('token', token)
                                                                                                        .send({inventories: [{_id: allInventories[0]._id}]})
                                                                                                        .expect(200)
                                                                                                        .end(function (inventoryDeleteErr, inventoryDeleteRes) {
                                                                                                            // Handle Inventory error error
                                                                                                            should.not.exist(inventoryDeleteErr);
                                                                                                            agent.get('/inventorysummary?businessUnitId=' + businessUnitId)
                                                                                                                .set('token', token)
                                                                                                                .expect(200)
                                                                                                                .end(function (inventorySummaryErr, inventorySummary) {
                                                                                                                    should.not.exist(inventorySummaryErr);
                                                                                                                    should.exist(inventorySummary.body);
                                                                                                                    inventorySummary.body.OutOfStock.should.equal(0);
                                                                                                                    inventorySummary.body.NewlyAdded.should.equal(0);
                                                                                                                    inventorySummary.body.Inactive.should.equal(0);
                                                                                                                    done();
                                                                                                                });
                                                                                                        });
                                                                                                });
                                                                                        });
                                                                                });
                                                                        });
                                                                });
                                                        });
                                                });
                                        });
                                });
                        });
                });
        });

    });

    /**
     * case 1:Check summary with filters after inventory creation
     * @param done
     */
    it('Check summary with filters',function (done) {
        commonBatchInventoriesUtil.batchCreate(allProducts, 0,token,agent, function (productError) {
            if (productError) {
                done(productError);
            } else {
                commonBatchInventoriesUtil.getInventories(token,businessUnitId,agent,function(inventoryErr,inventoriesRes) {
                    var inventories = inventoriesRes.body.inventory;
                    var data = {summaryFilters: {NewlyAdded: true}};
                    agent.get('/inventorysummary?businessUnitId=' + businessUnitId + '&page=1&limit=10')
                        .send(data)
                        .set('token', token)
                        .end(function (inventorySummaryErr, inventorySummary) {
                            should.not.exist(inventorySummaryErr);
                            should.exist(inventorySummary.body);
                            inventorySummary.body.NewlyAdded.should.be.instanceof(Array).and.have.lengthOf(2);
                            //Check summary after Inactive
                            agent.post('/inventoriesinactive/')
                                .set('token', token)
                                .send({inventories: [{_id: inventories[0]._id}]})
                                .expect(200)
                                .end(function (inventoryInactiveErr, inventoryInactiveRes) {
                                    // Handle Inventory error error
                                    should.not.exist(inventoryInactiveErr);
                                    data = {summaryFilters: {Inactive: true}};
                                    agent.get('/inventorysummary?businessUnitId=' + businessUnitId+'&page=1&limit=10')
                                        .send(data)
                                        .set('token', token)
                                        .end(function (inventorySummaryErr, inventorySummary) {
                                            should.not.exist(inventorySummaryErr);
                                            should.exist(inventorySummary.body);
                                            inventorySummary.body.Inactive.should.be.instanceof(Array).and.have.lengthOf(1);

                                            var newInventory = inventories[0];
                                            newInventory.MRP = 2500;
                                            newInventory.numberOfUnits = 0;
                                            newInventory.moqAndMargin = [{
                                                MOQ: 10,
                                                margin: 20
                                            }];

                                            agent.put('/inventories/' + newInventory._id)
                                                .send(newInventory)
                                                .set('Content-Type', 'application/json')
                                                .set('token', token)
                                                .expect(200)
                                                .end(function (inventorySaveErr, inventorySaveRes) {
                                                    // Handle Inventory save error
                                                    should.not.exist(inventorySaveErr);
                                                    data = {summaryFilters: {OutOfStock: true}};
                                                    agent.get('/inventorysummary?businessUnitId=' + businessUnitId + '&page=1&limit=10')
                                                        .send(data)
                                                        .set('token', token)
                                                        .expect(200)
                                                        .end(function (inventorySummaryErr, inventorySummary) {
                                                            should.not.exist(inventorySummaryErr);
                                                            should.exist(inventorySummary.body);
                                                            inventorySummary.body.OutOfStock.should.be.length(1);
                                                            newInventory.numberOfUnits = 1;
                                                            agent.put('/inventories/' + newInventory._id)
                                                                .send(newInventory)
                                                                .set('Content-Type', 'application/json')
                                                                .set('token', token)
                                                                .expect(200)
                                                                .end(function (inventorySaveErr, inventorySaveRes) {
                                                                    // Handle Inventory save error
                                                                    should.not.exist(inventorySaveErr);
                                                                    data = {summaryFilters: {LowOnStock: true}};
                                                                    agent.get('/inventorysummary?businessUnitId=' + businessUnitId + '&page=1&limit=10')
                                                                        .send(data)
                                                                        .set('token', token)
                                                                        .expect(200)
                                                                        .end(function (inventorySummaryErr, inventorySummary) {
                                                                            should.not.exist(inventorySummaryErr);
                                                                            should.exist(inventorySummary.body);
                                                                            inventorySummary.body.LowOnStock.should.be.instanceof(Array).and.have.lengthOf(1);
                                                                            done();
                                                                        });
                                                                });
                                                        });
                                                });
                                        });
                                });

                        });
                });
            }
        });
    });

    function removeInventoriesFromBu(done){
        commonBatchCompanyUtil.getUserCompanyDefaultBusinessUnit(token, agent, function (defaultBusinessUnitErr, bunit) {
            should.not.exist(defaultBusinessUnitErr);
            var id=bunit.body;
            BusinessUnit.findById(id,function(err,bu){
                should.not.exist(err);
                bu.inventories = [];
                bu.save(function (err) {
                    done(err);
                });
            });

        });
    }

    function removeProductCategories(done){
        Category.find({'name':{$in:['Sona Masoori Rice','Coffee Berries']}},function(err,categories){
            should.not.exist(err);
            categories.forEach(function(category){
                category.productBrands=[];
            });
            async.forEach(categories,function(cat){
                cat.save(function(err){
                    should.not.exist(err);
                });
            });
        });
    }
    afterEach(function (done) {
        done();
    });

    after(function (done) {
        Inventory.remove().exec();

        ItemMaster.remove().exec();
        ProductBrand.remove().exec();
        removeInventoriesFromBu(function (err){
            should.not.exist(err);
        });
        removeProductCategories(function(err){
            should.not.exist(err);
        });

        User.remove().exec();
        BusinessUnit.remove().exec();
        Company.remove().exec();
        Contact.remove().exec();
        done();
    });
});
