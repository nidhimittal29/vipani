'use strict';

var should = require('should'),
    request = require('supertest'),
    app = require('../../server'),
    mongoose = require('mongoose'),
    User = mongoose.model('User'),
    Company = mongoose.model('Company'),
    Contact = mongoose.model('Contact'),
    Subscription = mongoose.model('Subscription'),
    agent = request.agent(app);

/**
 * Globals
 */
var credentials, user, subscription, subscription1;

/**
 * Subscription routes tests
 */
describe('Subscription CRUD tests', function () {
    before(function (done) {
        var regUser = {
            username: 'test@test.com',
            password: 'password',
            confirmPassword: 'password',
            issendotp: false,
            isverifyotp: false,
            acceptTerms: true
        };
        regUser.issendotp = true;
        agent.post('/user/sendpresignupotp')
            .send(regUser)
            .expect(200)
            .end(function (presignupErr, presignupRes) {
                //console.log("Presignup Response - "+JSON.stringify(presignupRes.message));

                if (presignupErr) done(presignupErr);
                /* user.issendotp=true;
                 user.isverifyotp=true;*/
                else {
                    (presignupRes.body.user.status).should.equal('Register Request');
                    regUser.otp = presignupRes.body.otp;
                    regUser.issendotp = false;
                    regUser.isverifyotp = true;
                    agent.post('/user/sendpresignupotp')
                        .send(regUser)
                        .expect(200)
                        .end(function (presignupErr, verifiedOtp) {
                            //console.log("Presignup Response - "+JSON.stringify(presignupRes.message));
                            if (presignupErr) done(presignupErr);
                            (verifiedOtp.body.user.status).should.equal('Verified');
                            if (verifiedOtp.body.user.username === verifiedOtp.body.user.email) {
                                (verifiedOtp.body.user.emailVerified).should.equal(true);
                            } else if (verifiedOtp.body.username === verifiedOtp.body.mobile) {
                                (verifiedOtp.body.user.mobileVerified).should.equal(true);

                            }

                            regUser.mobile = '0123456789';
                            regUser.issendotp = false;
                            regUser.isverifyotp = false;
                            regUser.ispassword = true;
                            regUser.acceptTerms = true;
                            agent.post('/user/sendpresignupotp')
                                .send(regUser)
                                .expect(200)
                                .end(function (presignupErr, changePassword) {
                                    //console.log("Presignup Response - "+JSON.stringify(presignupRes.message));
                                    if (presignupErr) done(presignupErr);
                                    (changePassword.body.user.status).should.equal('Registered');

                                    User.findOne({
                                        username: regUser.username
                                    }, '-salt -password', function (err, resUser) {
                                        if (err) {
                                            done(err);
                                        }
                                        user = resUser;
                                        if (resUser) {
                                            user = resUser;


                                            done();
                                        }
                                    });
                                });
                        });
                }
            });

        credentials = {
            username: 'test@test.com',
            password: 'password'
        };
    });
    beforeEach(function (done) {
        subscription = {
            name: 'Free',
            type: 'Free',
            scope: 'User',
            termType: 'Quarterly',
            billingTermType: 'Yearly',
            price: 0,
            netPrice: 0,
            freeTerm: 0
        };
        subscription1 = {
            name: 'Platinum',
            type: 'Paid',
            scope: 'User',
            termType: 'Quarterly',
            billingTermType: 'Yearly',
            price: 500,
            netPrice: 0,
            freeTerm: 0
        };
        done();
    });

    it('should be able to save Subscription instance if logged in', function (done) {
        agent.post('/auth/signin')
            .send(credentials)
            .expect(200)
            .end(function (signinErr, signinRes) {
                // Handle signin error
                if (signinErr) done(signinErr);

                // Get the token
                var token = signinRes.body.token;

                // Save a new Subscription
                agent.post('/subscriptions')
                    .send(subscription)
                    .set('token', token)
                    .expect(200)
                    .end(function (contactSaveErr, contactSaveRes) {
                        // Handle Subscription save error
                        if (contactSaveErr) done(contactSaveErr);

                        // Get a list of Subscriptions
                        agent.get('/subscriptions')
                            .set('token', token)
                            .end(function (subscriptionsGetErr, subscriptionsGetRes) {
                                // Handle Subscription save error
                                if (subscriptionsGetErr) done(subscriptionsGetErr);

                                // Get Subscriptions list
                                var subscriptions = subscriptionsGetRes.body;

                                // Set assertions
                                (subscriptions[0].user._id.toString()).should.match(user._id.toString());

                                (subscriptions[0].name).should.match('Free');
                                (subscriptions[0].price).should.match(0);
                                (subscriptions[0].type).should.match('Free');
                                (subscriptions[0].scope).should.match('User');
                                (subscriptions[0].termType).should.match('Quarterly');
                                (subscriptions[0].billingTermType).should.match('Yearly');
                                (subscriptions[0].netPrice).should.match(0);
                                (subscriptions[0].freeTerm).should.match(0);
                                (subscriptions[0].deleted).should.match(false);
                                (subscriptions[0].lastUpdatedUser.toString()).should.match(user._id.toString());
                                (subscriptions[0].user._id.toString()).should.match(user._id.toString());

                                // Call the assertion callback
                                done();
                            });
                    });
            });
    });

    it('should not be able to save Subscription instance if not logged in', function (done) {
        agent.post('/subscriptions')
            .send(subscription1)
            .expect(401)
            .end(function (subscriptionSaveErr, subscriptionSaveRes) {
                // Call the assertion callback
                done(subscriptionSaveErr);
            });
    });

    it('should not be able to save Subscription instance if no name is provided', function (done) {
        // Invalidate name field
        subscription.name = '';

        agent.post('/auth/signin')
            .send(credentials)
            .expect(200)
            .end(function (signinErr, signinRes) {
                // Handle signin error
                if (signinErr) done(signinErr);

                var token = signinRes.body.token;


                // Save a new Subscription
                agent.post('/subscriptions')
                    .send(subscription)
                    .set('token', token)
                    .expect(400)
                    .end(function (subscriptionSaveErr, subscriptionSaveRes) {
                        // Set message assertion
                        (subscriptionSaveRes.body.message).should.match('Please fill Payment Term Name\b');

                        // Handle Subscription save error
                        done(subscriptionSaveErr);
                    });
            });
    });

    /* it('should not be able to save Subscription instance if no symbol is provided', function (done) {
         // Invalidate name field
         subscription.symbol = '';

         agent.post('/auth/signin')
             .send(credentials)
             .expect(200)
             .end(function (signinErr, signinRes) {
                 // Handle signin error
                 if (signinErr) done(signinErr);

                 var token = signinRes.body.token;


                 // Save a new Subscription
                 agent.post('/subscriptions')
                     .send(subscription)
                     .set('token', token)
                     .expect(400)
                     .end(function (subscriptionSaveErr, subscriptionSaveRes) {
                         // Set message assertion
                         (subscriptionSaveRes.body.message).should.match('Please fill Unit Of Measure Symbol\b');

                         // Handle Subscription save error
                         done();
                     });
             });
     });
 */
    it('should be able to update Subscription instance if signed in', function (done) {
        agent.post('/auth/signin')
            .send(credentials)
            .expect(200)
            .end(function (signinErr, signinRes) {
                // Handle signin error
                if (signinErr) done(signinErr);

                var token = signinRes.body.token;


                // Save a new Subscription
                agent.post('/subscriptions')
                    .send(subscription)
                    .set('token', token)
                    .expect(200)
                    .end(function (subscriptionSaveErr, subscriptionSaveRes) {
                        // Handle Subscription save error
                        if (subscriptionSaveErr) done(subscriptionSaveErr);

                        // Update Subscription name
                        subscription.name = 'Net 30 New';

                        // Update existing Subscription
                        agent.put('/subscriptions/' + subscriptionSaveRes.body._id)
                            .send(subscription)
                            .set('token', token)
                            .expect(200)
                            .end(function (subscriptionUpdateErr, subscriptionUpdateRes) {
                                // Handle Subscription update error
                                if (subscriptionUpdateErr) done(subscriptionUpdateErr);

                                // Set assertions
                                (subscriptionUpdateRes.body._id).should.equal(subscriptionUpdateRes.body._id);
                                (subscriptionUpdateRes.body.name).should.match('Net 30 New');

                                // Call the assertion callback
                                done();
                            });
                    });
            });
    });

    it('should be able to get a list of Subscriptions if signed in', function (done) {
        agent.post('/auth/signin')
            .send(credentials)
            .expect(200)
            .end(function (signinErr, signinRes) {
                // Handle signin error
                if (signinErr) done(signinErr);

                var token = signinRes.body.token;

                // Save a new Subscription
                agent.post('/subscriptions')
                    .send(subscription)
                    .set('token', token)
                    .expect(200)
                    .end(function (subscriptionSaveErr, subscriptionSaveRes) {
                        // Handle Subscription save error
                        if (subscriptionSaveErr) done(subscriptionSaveErr);
                        request(app).get('/subscriptions')
                            .set('token', token)
                            .end(function (req, res) {
                                // Set assertion
                                res.body.should.be.instanceof(Array).and.have.lengthOf(1);

                                // Call the assertion callback
                                done();
                            });
                    });
            });
    });


    it('should be able to get a single Subscription if signed in', function (done) {
        agent.post('/auth/signin')
            .send(credentials)
            .expect(200)
            .end(function (signinErr, signinRes) {
                // Handle signin error
                if (signinErr) done(signinErr);

                var token = signinRes.body.token;


                // Save a new Subscription
                agent.post('/subscriptions')
                    .send(subscription)
                    .set('token', token)
                    .expect(200)
                    .end(function (subscriptionSaveErr, subscriptionSaveRes) {
                        // Handle Subscription save error
                        if (subscriptionSaveErr) done(subscriptionSaveErr);

                        request(app).get('/subscriptions/' + subscriptionSaveRes.body._id)
                            .set('token', token)
                            .end(function (req, res) {
                                // Set assertion
                                res.body.should.be.an.Object.with.property('name', subscription.name);

                                // Call the assertion callback
                                done();
                            });
                    });
            });
    });

    it('should be able to delete Subscription instance if signed in', function (done) {
        agent.post('/auth/signin')
            .send(credentials)
            .expect(200)
            .end(function (signinErr, signinRes) {
                // Handle signin error
                if (signinErr) done(signinErr);

                var token = signinRes.body.token;


                // Save a new Subscription
                agent.post('/subscriptions')
                    .send(subscription)
                    .set('token', token)
                    .expect(200)
                    .end(function (subscriptionSaveErr, subscriptionSaveRes) {
                        // Handle Subscription save error
                        if (subscriptionSaveErr) done(subscriptionSaveErr);

                        // Delete existing Subscription
                        agent.delete('/subscriptions/' + subscriptionSaveRes.body._id)
                            .send(subscription)
                            .set('token', token)
                            .expect(200)
                            .end(function (subscriptionDeleteErr, subscriptionDeleteRes) {
                                // Handle Subscription error error
                                if (subscriptionDeleteErr) done(subscriptionDeleteErr);

                                // Set assertions
                                (subscriptionDeleteRes.body._id).should.equal(subscriptionSaveRes.body._id);

                                // Call the assertion callback
                                done();
                            });
                    });
            });
    });

    it('should not be able to delete Subscription instance if not signed in', function (done) {
        // Set Subscription user
        subscription.user = user;

        // Create new Subscription model instance
        var subscriptionObj = new Subscription(subscription);

        // Save the Subscription
        subscriptionObj.save(function () {
            // Try deleting Subscription
            request(app).delete('/subscriptions/' + subscriptionObj._id)
                .expect(401)
                .end(function (subscriptionDeleteErr, subscriptionDeleteRes) {
                    // Set message assertion
                    if (subscriptionDeleteErr) {
                        done(subscriptionDeleteErr);
                    } else {
                        done();
                    }

                });

        });
    });

    afterEach(function (done) {
        /*  User.remove().exec();*/
        //Company.remove().exec();
        Subscription.remove().exec();
        done();
    });
    after(function (done) {

        User.remove().exec();
        Company.remove().exec();
        Contact.remove().exec();
        done();
    });
});
