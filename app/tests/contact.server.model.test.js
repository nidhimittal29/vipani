'use strict';

/**
 * Module dependencies.
 */
var should = require('should'),
    mongoose = require('mongoose'),
    app = require('../../server'),
    User = mongoose.model('User'),
    Contact = mongoose.model('Contact'),
    PaymentTerm = mongoose.model('PaymentTerm');
/**
 * Globals
 */
var user, contact;
var contactWithAddress,userWithAddress;
var contactWithWrongAddressType,userWithWrongAddressType;
var contactWithInvalidPinCode,userWithInvalidPinCode;
var contactWithPaymentTerms,userWithPaymentTerms;
var contactWithWrongAccountType,userWithWrongAccountType;

/**
 * Unit tests
 */
describe('Contact Model Unit Tests:', function() {
    beforeEach(function(done) {
        user = new User({
            firstName: 'Full',
            lastName: 'Name',
            displayName: 'Full Name',
            email: 'test@test.com',
            username: 'test@test.com',
            password: 'password',
            status: 'Registered',
            provider: 'local'
        });
        user.save(function() {
            contact = new Contact({
                firstName: 'Contact Name',
                user: user
            });
            contactWithAddress = new Contact({
                firstName: 'With Address',
                user: user,
                addresses:[{
                    addressLine:'address line1',
                    city:'bangalore',
                    state:'Karnataka',
                    country:'India',
                    pinCode:560075,
                    primary:true
                },{
                    addressLine:'address line2',
                    city:'hyderabad',
                    state:'Andhra Pradesh',
                    country:'India',
                    pinCode:500043,
                    primary:true,
                    addressType:'Shipping'
                }]
            });
            contactWithInvalidPinCode = new Contact({
                firstName:'InvalidPinCode',
                user:userWithInvalidPinCode,
                addresses:[{
                    addressLine:'address line1',
                    city:'bangalore',
                    state:'Karnataka',
                    country:'India',
                    pinCode:56075,
                    primary:true
                },{
                    addressLine:'address line2',
                    city:'hyderabad',
                    state:'Andhra Pradesh',
                    country:'India',
                    pinCode:50043,
                    primary:true,
                    addressType:'Shipping'
                }]
            });
            contactWithWrongAccountType = new Contact({
                firstName:'WithWrongAccountType',
                user:userWithWrongAccountType,
                bankAccountDetails:{
                    bankAccountNumber:'1231239876',
                    accountType:'Savings',
                    bankName:'IDBI Bank Ltd',
                    bankBranchName:'Indiranagar',
                    ifscCode:'ICIC023212'
                }
            });
            contactWithPaymentTerms = new Contact({
                firstName:'WithPaymentTerms',
                user:userWithPaymentTerms

            });
            contactWithWrongAddressType = new Contact({
                firstName:'WithWrongAddressType',
                user:userWithWrongAddressType,
                addresses:[{
                    addressLine:'address line1',
                    city:'bangalore',
                    state:'Karnataka',
                    country:'India',
                    pinCode:56075,
                    primary:true
                },{
                    addressLine:'address line2',
                    city:'hyderabad',
                    state:'Andhra Pradesh',
                    country:'India',
                    pinCode:50043,
                    primary:true,
                    addressType:'Shiping'
                }]
            });
            done();
        });

    });

    describe('Method Save', function() {
        it('should be able to save without problems', function(done) {
            return contact.save(function(err) {
                should.not.exist(err);
                done();
            });
        });
        //no longer relevant as namefields are not required.
        /*it('should be able to show an error when try to save without name', function(done) {
            contact.firstName = '';

            return contact.save(function(err) {
                should.exist(err);
                done();
            });
        });*/
        it('should save with address',function(done){
            return contactWithAddress.save(function (err) {
                should.not.exist(err);
                done();
            });
        });
/*
TBD
        it('should save with paymentTerms',function(done){
            PaymentTerm.findAll(function (err,paymentTerms) {
                if(err){
                    should.not.exist(err);
                    done();
                }
                else {
                    contactWithPaymentTerms.paymentTerms = [paymentTerms[0]._id, paymentTerms[1]._id];
                    return contactWithPaymentTerms.save(function (err) {
                        should.not.exist(err);
                        done();
                    });
                }
            });

        });
*/
        it('should not save with wrong addressType',function (done) {
            return contactWithWrongAddressType.save(function (err) {
                should.exist(err);
                done();
            });
        });
        it('should not save with wrong accountType',function (done) {
            return contactWithWrongAccountType.save(function (err) {
                should.exist(err);
                done();
            });
        });
    });

    describe('Method Delete',function(){
        it('should delete without error',function (done) {
            contactWithAddress.save(function (err) {
                if(!err){
                    return Contact.remove({firstName:'With Address'},function(err){
                        should.not.exist(err);
                        done();
                    });
                }
                else {
                    should.not.exist(err);
                }
            });


        });
    });

    afterEach(function(done) {

        Contact.remove().exec();
        User.remove().exec();

        done();
    });
});
