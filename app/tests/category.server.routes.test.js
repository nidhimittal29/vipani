'use strict';

var should = require('should'),
    request = require('supertest'),
    app = require('../../server'),
    mongoose = require('mongoose'),
    User = mongoose.model('User'),
    Category = mongoose.model('Category'),
    Company = mongoose.model('Company'),
    Contact = mongoose.model('Contact'),
    agent = request.agent(app);

/**
 * Globals
 */
var credentials, user, category,subcategory1,subcategory2;

/**
 * Category routes tests
 */
describe('Category CRUD tests', function () {
    beforeEach(function (done) {
        // Create user credentials
        //{"username":"test@test.com","password":"password","ConfirmPassword":"password","firstName":"First Name","lastName":"Last Name","companyName":"nVipani","businessType":"Trader","categorySeller":true,"categoryBuyer":true,"categoryMediator":true,"mobile":"0123456789","acceptTerms":true}
        var regUser = {
            username: 'test@test.com',
            password: 'password',
            ConfirmPassword: 'password',
            firstName: 'First Name',
            lastName: 'Last Name',
            companyName: 'nVipani',
            businessType: 'Trader',
            categorySeller: true,
            categoryBuyer: true,
            categoryMediator: true,
            mobile: '0123456789',
            acceptTerms: true
        };
        agent.post('/user/sendpresignupotp')
            .send({username: regUser.username, password: 'password', acceptTerms: true,issendotp:true})
            .expect(200)
            .end(function (presignupPasswordErr, presignupPasswordRes) {
                if (presignupPasswordErr) done(presignupPasswordErr);
                if (presignupPasswordRes.body.user.status === 'Register Request') {
                    (presignupPasswordRes.body.user.status).should.equal('Register Request');
                    regUser.otp = presignupPasswordRes.body.otp;
                    regUser.isverifyotp = true;
                    regUser.sendotp=false;
                    agent.post('/user/sendpresignupotp')
                        .send(regUser)
                        .expect(200)
                        .end(function (verifiedOtpErr, verifiedOtpFirstUser) {
                            if (verifiedOtpErr) done(verifiedOtpErr);
                            (verifiedOtpFirstUser.body.user.status).should.equal('Verified');

                            regUser.registrationCategories = verifiedOtpFirstUser.body.registrationCategories;
                            regUser.segments = verifiedOtpFirstUser.body.segments;
                            regUser.categories = verifiedOtpFirstUser.body.categories;
                            var selectedSegments = [];
                            //for rice no categories.
                            selectedSegments.push({
                                segment: regUser.segments[0]._id,
                                categories: []
                            });
                            agent.post('/user/sendpresignupotp')
                                .send({
                                    username: regUser.username,
                                    registrationCategory: regUser.registrationCategories[0]._id,
                                    ispassword: true,
                                    selectedSegments: selectedSegments
                                })
                                .expect(200)
                                .end(function (sendBusinessSegmentPasswordErr, verifiedSendBusinessSegmentPasswordUser) {
                                    if (sendBusinessSegmentPasswordErr) done(sendBusinessSegmentPasswordErr);
                                    verifiedSendBusinessSegmentPasswordUser.body.status.should.equal(true);
                                    //We need to set proper message for the final stage.
                                    verifiedSendBusinessSegmentPasswordUser.body.message.should.equal('An OTP has been ' + verifiedSendBusinessSegmentPasswordUser.body.user.status + ' for the ' + regUser.username);
                                        user=verifiedSendBusinessSegmentPasswordUser.body.user;
                                    credentials = {
                                        username: 'test@test.com',
                                        password: 'password'
                                    };
                                    category={
                                        name:'Agriculture',
                                        code:'AGR',
                                        type:'MainCategory'
                                    };
                                    subcategory1={
                                        name:'Cereals',
                                        code:'CRLS',
                                        type:'SubCategory1'
                                    };
                                    subcategory2={
                                        name:'Rice',
                                        code:'RIC',
                                        type:'SubCategory2'
                                    };
                                    done();
                                });
                        });
                }
            });
    });


        // Create a new user
        /*  user = new User({
         firstName: 'First Name',
         lastName: 'Last Name',
            displayName: 'Full Name',
            email: 'test@test.com',
            username: credentials.username,
            password: credentials.password,
            provider: 'local'
        });

        // Save a user to the test db and create new Category
        user.save(function () {
            category = {
         name: 'Agriculture',
         code: 'AGR',
            };

            done();
         });*/
    it('should be able to save Category instance if logged in', function (done) {
        agent.post('/auth/signin')
            .send(credentials)
            .expect(200)
            .end(function (signinErr, signinRes) {
                // Handle signin error
                if (signinErr) {
                    console.log(signinRes);
                    done(signinErr);
                }

                // Get the token
                var token = signinRes.body.token;

                // Save a new Category
                //category.token = token;
                agent.post('/createMainCategory')
                    .send(category)
                    .set('Content-Type', 'application/json')
                    .set('token', token)
                    .expect(200)
                    .end(function (categorySaveErr, categorySaveRes) {
                        // Handle Category save error
                        if (categorySaveErr){
                            console.log('response:'+categorySaveRes.body.message);
                            done(categorySaveErr);
                        }

                        // Get a list of Categories
                        agent.get('/categories')
                            .set('Content-Type', 'application/json')
                            .set('token', token)
                            .end(function (categoriesGetErr, categoriesGetRes) {
                                // Handle Category save error
                                if (categoriesGetErr) done(categoriesGetErr);

                                // Get Categories list
                                var categories = categoriesGetRes.body;
                                console.log(categories);
                                // Set assertions
                                (categories[0].user._id).should.equal(user._id);
                                (categories[0].name).should.match('Agriculture');
                                (categories[0].code).should.match('AGR');

                                // Call the assertion callback
                                done();
                            });
                    });
            });
    });

    it('should not be able to save Category instance if not logged in', function (done) {
        agent.post('/categories')
            .send(category)
            .expect(401)
            .end(function (categorySaveErr, categorySaveRes) {
                // Call the assertion callback
                done(categorySaveErr);
            });
    });

    it('should not be able to save Category instance if no name is provided', function (done) {
        // Invalidate name field
        category.name = '';

        agent.post('/auth/signin')
            .send(credentials)
            .expect(200)
            .end(function (signinErr, signinRes) {
                // Handle signin error
                if (signinErr) done(signinErr);

                // Get the token
                var token = signinRes.body.token;

                // Save a new Category
                agent.post('/categories')
                    .send(category)
                    .set('Content-Type', 'application/json')
                    .set('token', token)
                    .expect(400)
                    .end(function (categorySaveErr, categorySaveRes) {
                        // Set message assertion
                        (categorySaveRes.body.message).should.match('Please fill Category name\b');

                        // Handle Category save error
                        done(categorySaveErr);
                    });
            });
    });
/**This test is removed as we now create a new code from the name of the category*/
    /*it('should not be able to save Category instance if no code is provided', function (done) {
        // Invalidate name field
        category.code = '';

        agent.post('/auth/signin')
            .send(credentials)
            .expect(200)
            .end(function (signinErr, signinRes) {
                // Handle signin error
                if (signinErr) done(signinErr);

                // Get the token
                var token = signinRes.body.token;

                // Save a new Category
                agent.post('/createMainCategory')
                    .send(category)
                    .set('Content-Type', 'application/json')
                    .set('token', token)
                    .expect(400)
                    .end(function (categorySaveErr, categorySaveRes) {
                        // Set message assertion
                        (categorySaveRes.body.message).should.match('Please fill Category Code\b');

                        // Handle Category save error
                        done(categorySaveErr);
                    });
            });
    });*/

    it('should be able to update Category instance if signed in', function (done) {
        agent.post('/auth/signin')
            .send(credentials)
            .expect(200)
            .end(function (signinErr, signinRes) {
                // Handle signin error
                if (signinErr) done(signinErr);

                // Get the token
                var token = signinRes.body.token;

                // Save a new Category
                agent.post('/createMainCategory')
                    .send(category)
                    .set('Content-Type', 'application/json')
                    .set('token', token)
                    .expect(200)
                    .end(function (categorySaveErr, categorySaveRes) {
                        // Handle Category save error
                        if (categorySaveErr) done(categorySaveErr);

                        // Update Category name
                        category.name = 'WHY YOU GOTTA BE SO MEAN?';

                        // Update existing Category
                        agent.put('/categories/' + categorySaveRes.body._id)
                            .send(category)
                            .set('Content-Type', 'application/json')
                            .set('token', token)
                            .expect(200)
                            .end(function (categoryUpdateErr, categoryUpdateRes) {
                                // Handle Category update error
                                if (categoryUpdateErr) done(categoryUpdateErr);

                                // Set assertions
                                (categoryUpdateRes.body._id).should.equal(categorySaveRes.body._id);
                                (categoryUpdateRes.body.name).should.match('WHY YOU GOTTA BE SO MEAN?');

                                // Call the assertion callback
                                done();
                            });
                    });
            });
    });

    it('should be able to get a list of Categories if not signed in', function (done) {
        // Create new Category model instance
        var categoryObj = new Category(category);

        // Save the Category
        categoryObj.save(function () {
            // Request Categories
            request(app).get('/categories')
                .end(function (req, res) {
                    // Set assertion
                    res.body.should.be.instanceof(Array).and.have.lengthOf(1);

                    // Call the assertion callback
                    done();
                });

        });
    });


    it('should be able to get a single Category if not signed in', function (done) {
        // Create new Category model instance
        var categoryObj = new Category(category);

        // Save the Category
        categoryObj.save(function () {
            request(app).get('/categories/' + categoryObj._id)
                .end(function (req, res) {
                    // Set assertion
                    res.body.should.be.an.Object.with.property('name', category.name);

                    // Call the assertion callback
                    done();
                });
        });
    });
    it('should be able to search for a main category if not signed in', function (done) {
        // Create new Category model instance
        var categoryObj = new Category(category);

        // Save the Category
        categoryObj.save(function () {
            request(app).get('/queryMainCategoriesSearch/')
                .end(function (req, res) {
                    // Set assertion
                    res.body.length.should.equal(1);

                    // Call the assertion callback
                    done();
                });
        });
    });
    it('should be able to create subcategory2 and subcategory1 if signed in', function (done) {
        // Create new Category model instance


        // Save a new Category
        //category.token = token;
        agent.post('/auth/signin')
            .send(credentials)
            .expect(200)
            .end(function (signinErr, signinRes) {
                // Handle signin error
                if (signinErr) done(signinErr);

                var token = signinRes.body.token;
                agent.post('/createMainCategory')
                    .send(category)
                    .set('Content-Type', 'application/json')
                    .set('token', token)
                    .expect(200)
                    .end(function (categorySaveErr, categorySaveRes) {
                        // Handle Category save error
                        if (categorySaveErr) done(categorySaveErr);

                        var mainCategory = categorySaveRes.body;


                        // Save a new Category
                        //category.token = token;
                        agent.post('/createMainCategory')
                            .send(subcategory1)
                            .set('Content-Type', 'application/json')
                            .set('token', token)
                            .expect(200)
                            .end(function (subCategorySaveErr, subCategorySaveRes) {
                                // Handle Category save error
                                if (subCategorySaveErr) done(subCategorySaveErr);

                                var subCategory1 = subCategorySaveRes.body;

                                mainCategory.children.push(subCategory1);

                                // Update existing Category
                                agent.put('/createMainCategory/' + mainCategory._id)
                                    .send(mainCategory)
                                    .set('Content-Type', 'application/json')
                                    .set('token', token)
                                    .expect(200)
                                    .end(function (categoryUpdateErr, categoryUpdateRes) {
                                        // Handle Category update error
                                        if (categoryUpdateErr) done(categoryUpdateErr);

                                        // Save a new Category
                                        //category.token = token;
                                        agent.post('/createMainCategory')
                                            .send(subcategory2)
                                            .set('Content-Type', 'application/json')
                                            .set('token', token)
                                            .expect(200)
                                            .end(function (subCategory2SaveErr, subCategory2SaveRes) {
                                                // Handle Category save error
                                                if (subCategory2SaveErr) done(subCategory2SaveErr);

                                                var subCategory2 = subCategory2SaveRes.body;

                                                subCategory1.children.push(subCategory2);

                                                // Update existing Category
                                                agent.put('/createMainCategory/' + subCategory1._id)
                                                    .send(subCategory1)
                                                    .set('Content-Type', 'application/json')
                                                    .set('token', token)
                                                    .expect(200)
                                                    .end(function (subCategoryUpdateErr, subCategoryUpdateRes) {
                                                        should.not.exist(subCategoryUpdateErr);
                                                        subCategoryUpdateRes.body.name.should.equal('Cereals');
                                                        subCategoryUpdateRes.body.children.length.should.equal(1);
                                                        subCategoryUpdateRes.body.children[0].name.should.equal('Rice');
                                                        subCategoryUpdateRes.body.children[0].type.should.equal('SubCategory2');
                                                        done();

                                                    });
                                            });
                                    });
                            });
                    });
            });
    });

    it('should be able to search for a sub category1 if signed in', function (done) {
        // Create new Category model instance


        // Save a new Category
        //category.token = token;
        agent.post('/auth/signin')
            .send(credentials)
            .expect(200)
            .end(function (signinErr, signinRes) {
                // Handle signin error
                if (signinErr) done(signinErr);

                var token = signinRes.body.token;
                agent.post('/createMainCategory')
                    .send(category)
                    .set('Content-Type', 'application/json')
                    .set('token', token)
                    .expect(200)
                    .end(function (categorySaveErr, categorySaveRes) {
                        // Handle Category save error
                        if (categorySaveErr) done(categorySaveErr);

                        var mainCategory = categorySaveRes.body;


                        // Save a new Category
                        //category.token = token;
                        agent.post('/createMainCategory')
                            .send(subcategory1)
                            .set('Content-Type', 'application/json')
                            .set('token', token)
                            .expect(200)
                            .end(function (subCategorySaveErr, subCategorySaveRes) {
                                // Handle Category save error
                                if (subCategorySaveErr) done(subCategorySaveErr);

                                var subCategory1 = subCategorySaveRes.body;

                                mainCategory.children.push(subCategory1);

                                // Update existing Category
                                agent.put('/createMainCategory/' + mainCategory._id)
                                    .send(mainCategory)
                                    .set('Content-Type', 'application/json')
                                    .set('token', token)
                                    .expect(200)
                                    .end(function (categoryUpdateErr, categoryUpdateRes) {
                                        // Handle Category update error
                                        if (categoryUpdateErr) done(categoryUpdateErr);

                                        // Save a new Category
                                        //category.token = token;
                                        agent.post('/createMainCategory')
                                            .send(subcategory2)
                                            .set('Content-Type', 'application/json')
                                            .set('token', token)
                                            .expect(200)
                                            .end(function (subCategory2SaveErr, subCategory2SaveRes) {
                                                // Handle Category save error
                                                if (subCategory2SaveErr) done(subCategory2SaveErr);

                                                var subCategory2 = subCategory2SaveRes.body;

                                                subCategory1.children.push(subCategory2);

                                                // Update existing Category
                                                agent.put('/createMainCategory/' + subCategory1._id)
                                                    .send(subCategory1)
                                                    .set('Content-Type', 'application/json')
                                                    .set('token', token)
                                                    .expect(200)
                                                    .end(function (subCategoryUpdateErr, subCategoryUpdateRes) {
                                                        agent.get('/querySubCategories')
                                                            .set('token',token)
                                                            .expect(200)
                                                            .end(function(err,res){
                                                                should.not.exist(err);
                                                                var subcategories = res.body;
                                                                subcategories.length.should.equal(1);
                                                                subcategories[0].type.should.equal('SubCategory1');
                                                                subcategories[0].children.length.should.equal(1);
                                                                subcategories[0].children[0].name.should.equal('Rice');
                                                                done();

                                                            });

                                                    });
                                            });
                                    });
                            });
                    });
            });
    });

    it('should be able to search for a sub category2 if signed in', function (done) {
        // Save a new Category
        //category.token = token;
        agent.post('/auth/signin')
            .send(credentials)
            .expect(200)
            .end(function (signinErr, signinRes) {
                // Handle signin error
                if (signinErr) done(signinErr);

                var token = signinRes.body.token;

                agent.post('/createMainCategory')
                    .send(category)
                    .set('Content-Type', 'application/json')
                    .set('token', token)
                    .expect(200)
                    .end(function (categorySaveErr, categorySaveRes) {
                        // Handle Category save error
                        if (categorySaveErr) done(categorySaveErr);

                        var mainCategory = categorySaveRes.body;

                        // Save a new Category
                        //category.token = token;
                        agent.post('/createMainCategory')
                            .send(subcategory1)
                            .set('Content-Type', 'application/json')
                            .set('token', token)
                            .expect(200)
                            .end(function (subCategorySaveErr, subCategorySaveRes) {
                                // Handle Category save error
                                if (subCategorySaveErr) done(subCategorySaveErr);

                                var subCategory1 = subCategorySaveRes.body;

                                mainCategory.children.push(subCategory1);

                                // Update existing Category
                                agent.put('/createMainCategory/' + mainCategory._id)
                                    .send(mainCategory)
                                    .set('Content-Type', 'application/json')
                                    .set('token', token)
                                    .expect(200)
                                    .end(function (categoryUpdateErr, categoryUpdateRes) {
                                        // Handle Category update error
                                        if (categoryUpdateErr) done(categoryUpdateErr);

                                        // Save a new Category
                                        //category.token = token;
                                        agent.post('/createMainCategory')
                                            .send(subcategory2)
                                            .set('Content-Type', 'application/json')
                                            .set('token', token)
                                            .expect(200)
                                            .end(function (subCategory2SaveErr, subCategory2SaveRes) {
                                                // Handle Category save error
                                                if (subCategory2SaveErr) done(subCategory2SaveErr);

                                                var subCategory2 = subCategory2SaveRes.body;

                                                subCategory1.children.push(subCategory2);

                                                // Update existing Category
                                                agent.put('/createMainCategory/' + subCategory1._id)
                                                    .send(subCategory1)
                                                    .set('Content-Type', 'application/json')
                                                    .set('token', token)
                                                    .expect(200)
                                                    .end(function (subCategoryUpdateErr, subCategoryUpdateRes) {
                                                        agent.get('/querySubCategories2')
                                                            .set('token',token)
                                                            .expect(200)
                                                            .end(function(err,res){
                                                                should.not.exist(err);

                                                                var subcategories2 = subCategoryUpdateRes.body;
                                                                subcategories2.length.should.equal(1);
                                                                subcategories2[0].type.should.equal('SubCategory1');
                                                                subcategories2[0].children.length.should.equal(1);
                                                                subcategories2[0].children[0].name.should.equal('Rice');
                                                                done();

                                                            });

                                                    });
                                            });
                                    });
                            });
                    });
            });

    });
    /*removed as this is no longer supported*/
    /*it('should be able to delete Category instance if signed in', function (done) {
        agent.post('/auth/signin')
            .send(credentials)
            .expect(200)
            .end(function (signinErr, signinRes) {
                // Handle signin error
                if (signinErr) done(signinErr);

                // Get the token
                var token = signinRes.body.token;

                // Save a new Category
                agent.post('/createMainCategory')
                    .send(category)

                    .set('Content-Type', 'application/json')
                    .set('token', token)
                    .expect(200)
                    .end(function (categorySaveErr, categorySaveRes) {
                        // Handle Category save error
                        if (categorySaveErr) done(categorySaveErr);

                        // Delete existing Category
                        agent.delete('/categories/' + categorySaveRes.body._id)
                            .send(category)
                            .set('Content-Type', 'application/json')
                            .set('token', token)
                            .expect(200)
                            .end(function (categoryDeleteErr, categoryDeleteRes) {
                                // Handle Category error error
                                if (categoryDeleteErr) done(categoryDeleteErr);

                                // Set assertions
                                (categoryDeleteRes.body._id).should.equal(categorySaveRes.body._id);

                                // Call the assertion callback
                                done();
                            });
                    });
            });
    });

    it('should not be able to delete Category instance if not signed in', function (done) {
        // Set Category user
        category.user = user;

        // Create new Category model instance
        var categoryObj = new Category(category);

        // Save the Category
        categoryObj.save(function () {
            // Try deleting Category
            request(app).delete('/categories/' + categoryObj._id)
                .expect(401)
                .end(function (categoryDeleteErr, categoryDeleteRes) {
                    // Set message assertion
                    (categoryDeleteRes.body.message).should.match('User is not logged in');

                    // Handle Category error error
                    done(categoryDeleteErr);
                });

        });
    });*/

    afterEach(function (done) {
        User.remove().exec();
        Company.remove().exec();
        Contact.remove().exec();
        Category.remove().exec();
        done();
    });
});
