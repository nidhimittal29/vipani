'use strict';

var should = require('should'),
    request = require('supertest'),
    app = require('../../server'),
    mongoose = require('mongoose'),
    User = mongoose.model('User'),
    Inventory = mongoose.model('Inventory'),
    Contact = mongoose.model('Contact'),
    Company = mongoose.model('Company'),
    Category = mongoose.model('Category'),
    Product = mongoose.model('Product'),
    agent = request.agent(app);

/**
 * Globals
 */
var credentials, user, inventory, productInventory;

/**
 * Inventory routes tests
 */
/*describe('Hsncode CRUD tests', function () {

    before(function (done) {
        var regUser = {
            username: 'test@test.com',
            password: 'password',
            ConfirmPassword: 'password',
            firstName: 'First Name',
            lastName: 'Last Name',
            companyName: 'nVipani',
            businessType: 'Trader',
            categorySeller: true,
            categoryBuyer: true,
            categoryMediator: true,
            mobile: '0123456789',
            acceptTerms: true
        };
        agent.post('/user/presignup')
            .send(regUser)
            .expect(200)
            .end(function (presignupErr, presignupRes) {
                //console.log("Presignup Response - "+JSON.stringify(presignupRes.message));
                if (presignupErr) done(presignupErr);

                User.findOne({
                    username: credentials.username
                }, '-salt -password', function (err, resUser) {
                    if (err) {
                        done(err);
                    }
                    user = resUser;
                    if (resUser) {
                        agent.get('/user/register/' + resUser.statusToken)
                            .expect(200)
                            .end(function (validationGetErr, validationGetRes) {
                                //console.log("Presignup Response - "+JSON.stringify(presignupRes.message));
                                if (validationGetErr) done(validationGetErr);
                                done();
                            });
                    }
                });
            });

        credentials = {
            username: 'test@test.com',
            password: 'password'
        };
    });
    beforeEach(function (done) {
        agent.post('/auth/signin')
            .send(credentials)
            .expect(200)
            .end(function (signinErr, signinRes) {
                // Handle signin error
                if (signinErr) done(signinErr);

                var token = signinRes.body.token;

                var mainCategory = {
                    name: 'Agriculture',
                    code: 'AGR',
                    type: 'MainCategory'
                };

                // Save a new Category
                //category.token = token;
                agent.post('/categories')
                    .send(mainCategory)
                    .set('Content-Type', 'application/json')
                    .set('token', token)
                    .expect(200)
                    .end(function (categorySaveErr, categorySaveRes) {
                        // Handle Category save error
                        if (categorySaveErr) done(categorySaveErr);

                        mainCategory = categorySaveRes.body;

                        var subCategory1 = {
                            name: 'Vegetables',
                            code: 'VEG',
                            type: 'SubCategory1',
                            categoryImageURL1: 'modules/categories/img/subcategory1/vegetables.png'
                        };

                        // Save a new Category
                        //category.token = token;
                        agent.post('/categories')
                            .send(subCategory1)
                            .set('Content-Type', 'application/json')
                            .set('token', token)
                            .expect(200)
                            .end(function (subCategorySaveErr, subCategorySaveRes) {
                                // Handle Category save error
                                if (subCategorySaveErr) done(subCategorySaveErr);

                                var subCategory1 = subCategorySaveRes.body;

                                mainCategory.children.push(subCategory1);

                                // Update existing Category
                                agent.put('/categories/' + mainCategory._id)
                                    .send(mainCategory)
                                    .set('Content-Type', 'application/json')
                                    .set('token', token)
                                    .expect(200)
                                    .end(function (categoryUpdateErr, categoryUpdateRes) {
                                        // Handle Category update error
                                        if (categoryUpdateErr) done(categoryUpdateErr);

                                        var subCategory2 = {
                                            name: 'Lemon',
                                            code: 'LEM',
                                            type: 'SubCategory2',
                                            categoryImageURL1: 'modules/categories/img/subcategory2/lemon.png'
                                        };

                                        // Save a new Category
                                        //category.token = token;
                                        agent.post('/categories')
                                            .send(subCategory2)
                                            .set('Content-Type', 'application/json')
                                            .set('token', token)
                                            .expect(200)
                                            .end(function (subCategory2SaveErr, subCategory2SaveRes) {
                                                // Handle Category save error
                                                if (subCategory2SaveErr) done(subCategory2SaveErr);

                                                subCategory2 = subCategory2SaveRes.body;

                                                subCategory1.children.push(subCategory2);

                                                // Update existing Category
                                                agent.put('/categories/' + subCategory1._id)
                                                    .send(subCategory1)
                                                    .set('Content-Type', 'application/json')
                                                    .set('token', token)
                                                    .expect(200)
                                                    .end(function (subCategoryUpdateErr, subCategoryUpdateRes) {
                                                        // Handle Category update error
                                                        if (subCategoryUpdateErr) done(subCategoryUpdateErr);
                                                            else {
                                                            productInventory = {
                                                                product: {
                                                                    name: 'Acid Lemon',
                                                                    category: mainCategory._id,
                                                                    subCategory1: subCategory1._id,
                                                                    subCategory2: subCategory2._id,
                                                                    keywords: [{
                                                                        attributeKey: 'FarmingType',
                                                                        attributeValue: 'Natural Farming'
                                                                    }],
                                                                },
                                                                inventory: {
                                                                    unitSize: 50,
                                                                    unitMeasure: 'Kg',
                                                                    unitPrice: 5000.23,
                                                                    numberOfUnits: 1000,
                                                                    moqAndSalePrice: [{
                                                                        MOQ: 5,
                                                                        margin: 30,
                                                                        price: 5000.23 *(1- 30/100).toFixed('2')
                                                                    }]
                                                                }
                                                            };
                                                         done();
                                                        }

                                                    });
                                            });
                                    });
                            });
                    });
            });
    });

    it('should be able to save Inventory instance with new product if logged in', function (done) {
        agent.post('/auth/signin')
            .send(credentials)
            .expect(200)
            .end(function (signinErr, signinRes) {
                // Handle signin error
                if (signinErr) done(signinErr);

                var token = signinRes.body.token;

                // Save a new Inventory
                agent.post('/inventories/createProduct')
                    .send(productInventory)
                    .set('Content-Type', 'application/json')
                    .set('token', token)
                    .expect(200)
                    .end(function (inventorySaveErr, inventorySaveRes) {
                        // Handle Inventory save error
                        if (inventorySaveErr) done(inventorySaveErr);

                        // Get a list of Inventories
                        agent.get('/inventories')
                            .set('Content-Type', 'application/json')
                            .set('token', token)
                            .end(function (inventoriesGetErr, inventoriesGetRes) {
                                // Handle Inventory save error
                                if (inventoriesGetErr) done(inventoriesGetErr);

                                // Get Inventories list
                                var inventories = inventoriesGetRes.body;

                                // Set assertions
                                (inventories[0].user._id).should.equal(user.id);
                                (inventories[0].unitSize).should.equal(productInventory.inventory.unitSize);
                                (inventories[0].unitMeasure).should.equal(productInventory.inventory.unitMeasure);
                                (inventories[0].unitPrice).should.equal(productInventory.inventory.unitPrice);
                                should.exist(inventories[0].productNum);
                                (inventories[0].product.name).should.equal(productInventory.product.name);
                                (inventories[0].moqAndSalePrice[0].price).should.equal((productInventory.inventory.unitPrice*(1-(productInventory.inventory.moqAndSalePrice[0].margin/100).toFixed('2'))));
                                should.exist(inventories[0].product.productUID);
                                (inventories[0].product.user).should.equal(user.id);
                                (inventories[0].product.category._id).should.equal(productInventory.product.category);
                                (inventories[0].product.subCategory1._id).should.equal(productInventory.product.subCategory1);
                                (inventories[0].product.subCategory2._id).should.equal(productInventory.product.subCategory2);
                                done();
                            });
                    });
            });
    });


    it('should be able to save Inventory instance with existing product if logged in', function (done) {
        agent.post('/auth/signin')
            .send(credentials)
            .expect(200)
            .end(function (signinErr, signinRes) {
                // Handle signin error
                if (signinErr) done(signinErr);

                var token = signinRes.body.token;

                // Save a new Inventory
                agent.post('/inventories/createProduct')
                    .send(productInventory)
                    .set('Content-Type', 'application/json')
                    .set('token', token)
                    .expect(200)
                    .end(function (inventorySaveErr, inventorySaveRes) {
                        // Handle Inventory save error
                        if (inventorySaveErr) done(inventorySaveErr);

                        // Get a list of Inventories
                        agent.get('/inventories')
                            .set('Content-Type', 'application/json')
                            .set('token', token)
                            .end(function (inventoriesGetErr, inventoriesGetRes) {
                                // Handle Inventory save error
                                if (inventoriesGetErr) done(inventoriesGetErr);

                                // Get Inventories list
                                var inventories = inventoriesGetRes.body;

                                // Set assertions
                                (inventories[0].user._id).should.equal(user.id);
                                (inventories[0].unitSize).should.equal(productInventory.inventory.unitSize);
                                (inventories[0].unitMeasure).should.equal(productInventory.inventory.unitMeasure);
                                (inventories[0].unitPrice).should.equal(productInventory.inventory.unitPrice);
                                should.exist(inventories[0].productNum);
                                (inventories[0].product.user).should.equal(user.id);
                                (inventories[0].product.name).should.equal(productInventory.product.name);
                                should.exist(inventories[0].product.productUID);
                                (inventories[0].product.category._id).should.equal(productInventory.product.category);
                                (inventories[0].product.subCategory1._id).should.equal(productInventory.product.subCategory1);
                                (inventories[0].product.subCategory2._id).should.equal(productInventory.product.subCategory2);
                                //(inventories[0].name).should.match('Inventory Name');

                                var newInventory = {};
                                newInventory.product = inventories[0].product._id;
                                newInventory.unitSize = 25;
                                newInventory.unitMeasure = 'Kg';
                                newInventory.unitPrice = 2550.23;
                                newInventory.numberOfUnits = 2000;
                                newInventory.moqAndPrice = [{
                                    MOQ: 10,
                                    margin: 20,
                                    price: 2550.23 *(1-20/100).toFixed('2')
                                }];

                                // Save a new Inventory
                                agent.post('/inventories')
                                    .send(newInventory)
                                    .set('Content-Type', 'application/json')
                                    .set('token', token)
                                    .expect(200)
                                    .end(function (inventorySaveErr, inventorySaveRes) {
                                        // Handle Inventory save error
                                        if (inventorySaveErr) done(inventorySaveErr);

                                        // Get a list of Inventories
                                        agent.get('/inventories')
                                            .set('Content-Type', 'application/json')
                                            .set('token', token)
                                            .end(function (inventoriesGetErr, inventoriesGetRes) {
                                                // Handle Inventory save error
                                                if (inventoriesGetErr) done(inventoriesGetErr);

                                                // Get Inventories list
                                                var newInventories = inventoriesGetRes.body;

                                                // Set assertions
                                                (newInventories[1].user._id).should.equal(user.id);
                                                (newInventories[1].unitSize).should.equal(productInventory.inventory.unitSize);
                                                (newInventories[1].unitMeasure).should.equal(productInventory.inventory.unitMeasure);
                                                (newInventories[1].unitPrice).should.equal(productInventory.inventory.unitPrice);
                                                should.exist(newInventories[1].productNum);
                                                (newInventories[1].product.user).should.equal(user.id);
                                                (newInventories[1].product.name).should.equal(productInventory.product.name);
                                                should.exist(newInventories[1].product.productUID);
                                                (newInventories[1].product.category._id).should.equal(productInventory.product.category);
                                                (newInventories[1].product.subCategory1._id).should.equal(productInventory.product.subCategory1);
                                                (newInventories[1].product.subCategory2._id).should.equal(productInventory.product.subCategory2);


                                                (newInventories[0].user._id).should.equal(user.id);
                                                (newInventories[0].unitSize).should.equal(newInventory.unitSize);
                                                (newInventories[0].unitMeasure).should.equal(newInventory.unitMeasure);
                                                (newInventories[0].unitPrice).should.equal(newInventory.unitPrice);
                                                should.exist(newInventories[0].productNum);
                                                (newInventories[0].product.user).should.equal(user.id);
                                                (newInventories[0].productNum).should.not.equal(newInventories[1].productNum);
                                                (newInventories[0].product.name).should.equal(productInventory.product.name);
                                                should.exist(newInventories[0].product.productUID);
                                                (newInventories[0].product.productUID).should.equal(newInventories[1].product.productUID);
                                                (newInventories[0].product.category._id).should.equal(productInventory.product.category);
                                                (newInventories[0].product.subCategory1._id).should.equal(productInventory.product.subCategory1);
                                                (newInventories[0].product.subCategory2._id).should.equal(productInventory.product.subCategory2);
                                                // Call the assertion callback
                                                done();
                                            });
                                    });
                            });
                    });
            });
    });


        it('should be able to update Inventory instance with existing product if logged in', function (done) {
            agent.post('/auth/signin')
                .send(credentials)
                .expect(200)
                .end(function (signinErr, signinRes) {
                    // Handle signin error
                    if (signinErr) done(signinErr);

                    var token = signinRes.body.token;

                    // Save a new Inventory
                    agent.post('/inventories/createProduct')
                        .send(productInventory)
                        .set('Content-Type', 'application/json')
                        .set('token', token)
                        .expect(200)
                        .end(function (inventorySaveErr, inventorySaveRes) {
                            // Handle Inventory save error
                            if (inventorySaveErr) done(inventorySaveErr);

                            // Get a list of Inventories
                            agent.get('/inventories')
                                .set('Content-Type', 'application/json')
                                .set('token', token)
                                .end(function (inventoriesGetErr, inventoriesGetRes) {
                                    // Handle Inventory save error
                                    if (inventoriesGetErr) done(inventoriesGetErr);

                                    // Get Inventories list
                                    var inventories = inventoriesGetRes.body;

                                    // Set assertions
                                    (inventories[0].user._id).should.equal(user.id);
                                    (inventories[0].unitSize).should.equal(productInventory.inventory.unitSize);
                                    (inventories[0].unitMeasure).should.equal(productInventory.inventory.unitMeasure);
                                    (inventories[0].unitPrice).should.equal(productInventory.inventory.unitPrice);
                                    should.exist(inventories[0].productNum);
                                    (inventories[0].product.user).should.equal(user.id);
                                    (inventories[0].product.name).should.equal(productInventory.product.name);
                                    should.exist(inventories[0].product.productUID);
                                    (inventories[0].product.category._id).should.equal(productInventory.product.category);
                                    (inventories[0].product.subCategory1._id).should.equal(productInventory.product.subCategory1);
                                    (inventories[0].product.subCategory2._id).should.equal(productInventory.product.subCategory2);
                                    //(inventories[0].name).should.match('Inventory Name');

                                    var newInventory = inventories[0];
                                    newInventory.unitSize = 25;
                                    newInventory.unitMeasure = 'Kg';
                                    newInventory.unitPrice = 2500;
                                    newInventory.numberOfUnits = 2000;
                                    newInventory.moqAndPrice = [{
                                        MOQ: 10,
                                        margin: 20,
                                        price:  2550 *(1-20/100).toFixed('2')
                                    }];

                                    // Update Inventory
                                    agent.put('/inventories/' + newInventory._id)
                                        .send(newInventory)
                                        .set('Content-Type', 'application/json')
                                        .set('token', token)
                                        .expect(200)
                                        .end(function (inventorySaveErr, inventorySaveRes) {
                                            // Handle Inventory save error
                                            if (inventorySaveErr) done(inventorySaveErr);

                                            // Get a list of Inventories
                                            agent.get('/inventories')
                                                .set('Content-Type', 'application/json')
                                                .set('token', token)
                                                .end(function (inventoriesGetErr, inventoriesGetRes) {
                                                    // Handle Inventory save error
                                                    if (inventoriesGetErr) done(inventoriesGetErr);

                                                    // Get Inventories list
                                                    var newInventories = inventoriesGetRes.body;

                                                    newInventories.should.be.instanceof(Array).and.have.lengthOf(1);
                                                    // Set assertions
                                                    (newInventories[0].user._id).should.equal(user.id);
                                                    (newInventories[0].unitSize).should.equal(newInventory.unitSize);
                                                    (newInventories[0].unitMeasure).should.equal(newInventory.unitMeasure);
                                                    (newInventories[0].unitPrice).should.equal(newInventory.unitPrice);
                                                    should.exist(newInventories[0].productNum);
                                                    (newInventories[0].product.user).should.equal(user.id);
                                                    (newInventories[0].productNum).should.equal(newInventory.productNum);
                                                    (newInventories[0].product.name).should.equal(productInventory.product.name);
                                                    should.exist(newInventories[0].product.productUID);
                                                    (newInventories[0].product.productUID).should.equal(newInventory.product.productUID);
                                                    (newInventories[0].product.category._id).should.equal(productInventory.product.category);
                                                    (newInventories[0].product.subCategory1._id).should.equal(productInventory.product.subCategory1);
                                                    (newInventories[0].product.subCategory2._id).should.equal(productInventory.product.subCategory2);

                                                    // Call the assertion callback
                                                    done();
                                                });
                                        });
                                });
                        });
                });
        });

      /!*  it('should be able to update product instance with existing inventory if logged in', function (done) {
            agent.post('/auth/signin')
                .send(credentials)
                .expect(200)
                .end(function (signinErr, signinRes) {
                    // Handle signin error
                    if (signinErr) done(signinErr);

                    var token = signinRes.body.token;

                    // Save a new Inventory
                    agent.post('/inventories/createProduct')
                        .send(productInventory)
                        .set('Content-Type', 'application/json')
                        .set('token', token)
                        .expect(200)
                        .end(function (inventorySaveErr, inventorySaveRes) {
                            // Handle Inventory save error
                            if (inventorySaveErr) done(inventorySaveErr);

                            // Get a list of Inventories
                            agent.get('/inventories')
                                .set('Content-Type', 'application/json')
                                .set('token', token)
                                .end(function (inventoriesGetErr, inventoriesGetRes) {
                                    // Handle Inventory save error
                                    if (inventoriesGetErr) done(inventoriesGetErr);

                                    // Get Inventories list
                                    var inventories = inventoriesGetRes.body;

                                    // Set assertions
                                    (inventories[0].user._id).should.equal(user.id);
                                    (inventories[0].unitSize).should.equal(productInventory.inventory.unitSize);
                                    (inventories[0].unitMeasure).should.equal(productInventory.inventory.unitMeasure);
                                    (inventories[0].unitPrice).should.equal(productInventory.inventory.unitPrice);
                                    should.exist(inventories[0].productNum);
                                    (inventories[0].product.user).should.equal(user.id);
                                    (inventories[0].product.name).should.equal(productInventory.product.name);
                                    should.exist(inventories[0].product.productUID);
                                    (inventories[0].product.category._id).should.equal(productInventory.product.category);
                                    (inventories[0].product.subCategory1._id).should.equal(productInventory.product.subCategory1);
                                    (inventories[0].product.subCategory2._id).should.equal(productInventory.product.subCategory2);
                                    //(inventories[0].name).should.match('Inventory Name');

                                    var newProduct = inventories[0].product;

                                    newProduct.keywords.push({
                                        attributeKey: 'LocalName',
                                        attributeValue: 'Nimma'
                                    });

                                    newProduct.name = 'Acid Lemon - Gudur';
                                    newProduct.category = newProduct.category._id;
                                    newProduct.subCategory1 = newProduct.subCategory1._id;
                                    newProduct.subCategory2 = newProduct.subCategory2._id;
                                    // Update Inventory
                                    agent.put('/products/' + newProduct._id)
                                        .send(newProduct)
                                        .set('Content-Type', 'application/json')
                                        .set('token', token)
                                        .expect(200)
                                        .end(function (inventorySaveErr, inventorySaveRes) {
                                            // Handle Inventory save error
                                            if (inventorySaveErr) done(inventorySaveErr);

                                            // Get a list of Inventories
                                            agent.get('/inventories')
                                                .set('Content-Type', 'application/json')
                                                .set('token', token)
                                                .end(function (inventoriesGetErr, inventoriesGetRes) {
                                                    // Handle Inventory save error
                                                    if (inventoriesGetErr) done(inventoriesGetErr);

                                                    // Get Inventories list
                                                    var newInventories = inventoriesGetRes.body;

                                                    newInventories.should.be.instanceof(Array).and.have.lengthOf(1);
                                                    // Set assertions
                                                    (newInventories[0].user._id).should.equal(user.id);
                                                    (newInventories[0].unitSize).should.equal(inventories[0].unitSize);
                                                    (newInventories[0].unitMeasure).should.equal(inventories[0].unitMeasure);
                                                    (newInventories[0].unitPrice).should.equal(inventories[0].unitPrice);
                                                    should.exist(newInventories[0].productNum);
                                                    (newInventories[0].product.user).should.equal(user.id);
                                                    (newInventories[0].productNum).should.equal(inventories[0].productNum);
                                                    (newInventories[0].product.name).should.equal('Acid Lemon - Gudur');
                                                    (newInventories[0].product.keywords).should.be.instanceof(Array).and.have.lengthOf(2);
                                                    (newInventories[0].product.keywords[1].attributeKey).should.equal('LocalName');
                                                    (newInventories[0].product.keywords[1].attributeValue).should.equal('Nimma');
                                                    should.exist(newInventories[0].product.productUID);
                                                    (newInventories[0].product.productUID).should.equal(inventories[0].product.productUID);
                                                    (newInventories[0].product.category._id).should.equal(productInventory.product.category);
                                                    (newInventories[0].product.subCategory1._id).should.equal(productInventory.product.subCategory1);
                                                    (newInventories[0].product.subCategory2._id).should.equal(productInventory.product.subCategory2);

                                                    // Call the assertion callback
                                                    done();
                                                });
                                        });
                                });
                        });
                });
        });*!/
    /!*it('should not be able to save Inventory instance if not logged in', function (done) {
        agent.post('/inventories')
            .send(inventory)
            .expect(401)
            .end(function (inventorySaveErr, inventorySaveRes) {
                // Call the assertion callback
                done(inventorySaveErr);
            });
    });

    it('should not be able to save Inventory instance if no name is provided', function (done) {
        // Invalidate name field
        inventory.name = '';

        agent.post('/auth/signin')
            .send(credentials)
            .expect(200)
            .end(function (signinErr, signinRes) {
                // Handle signin error
                if (signinErr) done(signinErr);

                // Get the userId
                var userId = user.id;

                // Save a new Inventory
                agent.post('/inventories')
                    .send(inventory)
                    .expect(400)
                    .end(function (inventorySaveErr, inventorySaveRes) {
                        // Set message assertion
                        (inventorySaveRes.body.message).should.match('Please fill Inventory name');

                        // Handle Inventory save error
                        done(inventorySaveErr);
                    });
            });
    });

    it('should be able to update Inventory instance if signed in', function (done) {
        agent.post('/auth/signin')
            .send(credentials)
            .expect(200)
            .end(function (signinErr, signinRes) {
                // Handle signin error
                if (signinErr) done(signinErr);

                // Get the userId
                var userId = user.id;

                // Save a new Inventory
                agent.post('/inventories')
                    .send(inventory)
                    .expect(200)
                    .end(function (inventorySaveErr, inventorySaveRes) {
                        // Handle Inventory save error
                        if (inventorySaveErr) done(inventorySaveErr);

                        // Update Inventory name
                        inventory.name = 'WHY YOU GOTTA BE SO MEAN?';

                        // Update existing Inventory
                        agent.put('/inventories/' + inventorySaveRes.body._id)
                            .send(inventory)
                            .expect(200)
                            .end(function (inventoryUpdateErr, inventoryUpdateRes) {
                                // Handle Inventory update error
                                if (inventoryUpdateErr) done(inventoryUpdateErr);

                                // Set assertions
                                (inventoryUpdateRes.body._id).should.equal(inventorySaveRes.body._id);
                                (inventoryUpdateRes.body.name).should.match('WHY YOU GOTTA BE SO MEAN?');

                                // Call the assertion callback
                                done();
                            });
                    });
            });
    });

    it('should be able to get a list of Inventories if not signed in', function (done) {
        // Create new Inventory model instance
        var inventoryObj = new Inventory(inventory);

        // Save the Inventory
        inventoryObj.save(function () {
            // Request Inventories
            request(app).get('/inventories')
                .end(function (req, res) {
                    // Set assertion
                    res.body.should.be.instanceof(Array).and.have.lengthOf(1);

                    // Call the assertion callback
                    done();
                });

        });
    });


    it('should be able to get a single Inventory if not signed in', function (done) {
        // Create new Inventory model instance
        var inventoryObj = new Inventory(inventory);

        // Save the Inventory
        inventoryObj.save(function () {
            request(app).get('/inventories/' + inventoryObj._id)
                .end(function (req, res) {
                    // Set assertion
                    res.body.should.be.an.Object.with.property('name', inventory.name);

                    // Call the assertion callback
                    done();
                });
        });
    });

    it('should be able to delete Inventory instance if signed in', function (done) {
        agent.post('/auth/signin')
            .send(credentials)
            .expect(200)
            .end(function (signinErr, signinRes) {
                // Handle signin error
                if (signinErr) done(signinErr);

                // Get the userId
                var userId = user.id;

                // Save a new Inventory
                agent.post('/inventories')
                    .send(inventory)
                    .expect(200)
                    .end(function (inventorySaveErr, inventorySaveRes) {
                        // Handle Inventory save error
                        if (inventorySaveErr) done(inventorySaveErr);

                        // Delete existing Inventory
                        agent.delete('/inventories/' + inventorySaveRes.body._id)
                            .send(inventory)
                            .expect(200)
                            .end(function (inventoryDeleteErr, inventoryDeleteRes) {
                                // Handle Inventory error error
                                if (inventoryDeleteErr) done(inventoryDeleteErr);

                                // Set assertions
                                (inventoryDeleteRes.body._id).should.equal(inventorySaveRes.body._id);

                                // Call the assertion callback
                                done();
                            });
                    });
            });
    });

    it('should not be able to delete Inventory instance if not signed in', function (done) {
        // Set Inventory user 
        inventory.user = user;

        // Create new Inventory model instance
        var inventoryObj = new Inventory(inventory);

        // Save the Inventory
        inventoryObj.save(function () {
            // Try deleting Inventory
            request(app).delete('/inventories/' + inventoryObj._id)
                .expect(401)
                .end(function (inventoryDeleteErr, inventoryDeleteRes) {
                    // Set message assertion
                    (inventoryDeleteRes.body.message).should.match('User is not logged in');

                    // Handle Inventory error error
                    done(inventoryDeleteErr);
                });

        });
     });*!/

    afterEach(function (done) {
        Category.remove().exec();
        Product.remove().exec();
        Inventory.remove().exec();
        done();
    });

    after(function (done) {
        User.remove().exec();
        Company.remove().exec();
        Contact.remove().exec();
        done();
    });
});*/
