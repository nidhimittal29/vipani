'use strict';

var should = require('should'),
    request = require('supertest'),
    app = require('../../server'),
    mongoose = require('mongoose'),
    User = mongoose.model('User'),
    Company = mongoose.model('Company'),
    Contact = mongoose.model('Contact'),
    UnitOfMeasure = mongoose.model('UnitOfMeasure'),
    agent = request.agent(app);

/**
 * Globals
 */
var credentials, user, unitOfMeasure, unitOfMeasure1;

/**
 * UnitOfMeasure routes tests
 */
describe('UnitOfMeasure CRUD tests', function () {
    before(function (done) {
        var regUser = {
            username: 'test@test.com',
            password: 'password',
            confirmPassword: 'password',
            issendotp: false,
            isverifyotp: false,
            acceptTerms: true
        };
        regUser.issendotp = true;
        agent.post('/user/sendpresignupotp')
            .send(regUser)
            .expect(200)
            .end(function (presignupErr, presignupRes) {
                //console.log("Presignup Response - "+JSON.stringify(presignupRes.message));

                if (presignupErr) done(presignupErr);
                /* user.issendotp=true;
                 user.isverifyotp=true;*/
                else {
                    (presignupRes.body.user.status).should.equal('Register Request');
                    regUser.otp = presignupRes.body.otp;
                    regUser.issendotp = false;
                    regUser.isverifyotp = true;
                    agent.post('/user/sendpresignupotp')
                        .send(regUser)
                        .expect(200)
                        .end(function (presignupErr, verifiedOtp) {
                            //console.log("Presignup Response - "+JSON.stringify(presignupRes.message));
                            if (presignupErr) done(presignupErr);
                            (verifiedOtp.body.user.status).should.equal('Verified');
                            if (verifiedOtp.body.user.username === verifiedOtp.body.user.email) {
                                (verifiedOtp.body.user.emailVerified).should.equal(true);
                            } else if (verifiedOtp.body.username === verifiedOtp.body.mobile) {
                                (verifiedOtp.body.user.mobileVerified).should.equal(true);

                            }
                            //regUser.password = 'password';
                            //regUser.confirmPassword = 'password';
                            regUser.registrationCategories = verifiedOtp.body.registrationCategories;
                            regUser.segments = verifiedOtp.body.segments;
                            regUser.categories = verifiedOtp.body.categories;
                            var selectedSegments = [];
                            //for rice no categories.
                            selectedSegments.push({
                                segment: regUser.segments[0]._id,
                                categories: []
                            });

                            regUser.mobile = '0123456789';
                            regUser.issendotp = false;
                            regUser.isverifyotp = false;
                            regUser.ispassword = true;
                            regUser.acceptTerms = true;
                            regUser.registrationCategory= regUser.registrationCategories[0]._id;
                            regUser.selectedSegments=selectedSegments;
                            agent.post('/user/sendpresignupotp')
                                .send(regUser)
                                .expect(200)
                                .end(function (presignupErr, changePassword) {
                                    //console.log("Presignup Response - "+JSON.stringify(presignupRes.message));
                                    if (presignupErr) done(presignupErr);
                                    (changePassword.body.user.status).should.equal('Registered');

                                    User.findOne({
                                        username: regUser.username
                                    }, '-salt -password', function (err, resUser) {
                                        if (err) {
                                            done(err);
                                        }
                                        user = resUser;
                                        if (resUser) {
                                            user = resUser;


                                            done();
                                        }
                                    });
                                });
                        });
                }
            });

        credentials = {
            username: 'test@test.com',
            password: 'password'
        };
    });
    beforeEach(function (done) {
        unitOfMeasure = {
            name: 'Kg',
            symbol: 'Kg',
            type: 'Simple',
            quantityType: 'Measure',
            disabled: false,
            deleted: false
        };
        unitOfMeasure1 = {
            name: 'Litre',
            symbol: 'Ltr',
            type: 'Simple',
            quantityType: 'Measure',
            numberOfDecimalPlaces: 2,
            disabled: false,
            deleted: false
        };
        done();
    });

    it('should be able to save UnitOfMeasure instance if logged in', function (done) {
        agent.post('/auth/signin')
            .send(credentials)
            .expect(200)
            .end(function (signinErr, signinRes) {
                // Handle signin error
                if (signinErr) done(signinErr);

                // Get the token
                var token = signinRes.body.token;

                // Save a new UnitOfMeasure
                agent.post('/unitofmeasures')
                    .send(unitOfMeasure)
                    .set('token', token)
                    .expect(200)
                    .end(function (contactSaveErr, contactSaveRes) {
                        // Handle UnitOfMeasure save error
                        if (contactSaveErr) done(contactSaveErr);

                        // Get a list of UnitOfMeasures
                        agent.get('/unitofmeasures')
                            .set('token', token)
                            .end(function (unitOfMeasuresGetErr, unitOfMeasuresGetRes) {
                                // Handle UnitOfMeasure save error
                                if (unitOfMeasuresGetErr) done(unitOfMeasuresGetErr);

                                // Get UnitOfMeasures list
                                var unitOfMeasures = unitOfMeasuresGetRes.body;

                                // Set assertions
                                (unitOfMeasures[0].user._id.toString()).should.match(user._id.toString());
                                (unitOfMeasures[0].name).should.match('Kg');
                                (unitOfMeasures[0].symbol).should.match('Kg');
                                (unitOfMeasures[0].type).should.match('Simple');
                                (unitOfMeasures[0].numberOfDecimalPlaces).should.match(0);
                                (unitOfMeasures[0].disabled).should.match(false);
                                (unitOfMeasures[0].deleted).should.match(false);
                                (unitOfMeasures[0].lastUpdatedUser.toString()).should.match(user._id.toString());

                                // Call the assertion callback
                                done();
                            });
                    });
            });
    });

    it('should not be able to save UnitOfMeasure instance if not logged in', function (done) {
        agent.post('/unitofmeasures')
            .send(unitOfMeasure1)
            .expect(401)
            .end(function (unitOfMeasureSaveErr, unitOfMeasureSaveRes) {
                // Call the assertion callback
                done(unitOfMeasureSaveErr);
            });
    });

    it('should not be able to save UnitOfMeasure instance if no name is provided', function (done) {
        // Invalidate name field
        unitOfMeasure.name = '';

        agent.post('/auth/signin')
            .send(credentials)
            .expect(200)
            .end(function (signinErr, signinRes) {
                // Handle signin error
                if (signinErr) done(signinErr);

                var token = signinRes.body.token;


                // Save a new UnitOfMeasure
                agent.post('/unitofmeasures')
                    .send(unitOfMeasure)
                    .set('token', token)
                    .expect(400)
                    .end(function (unitOfMeasureSaveErr, unitOfMeasureSaveRes) {
                        // Set message assertion
                        (unitOfMeasureSaveRes.body.message).should.match('Please fill Unit Of Measure Name\b');

                        // Handle UnitOfMeasure save error
                        done(unitOfMeasureSaveErr);
                    });
            });
    });

    it('should not be able to save UnitOfMeasure instance if no symbol is provided', function (done) {
        // Invalidate name field
        unitOfMeasure.symbol = '';

        agent.post('/auth/signin')
            .send(credentials)
            .expect(200)
            .end(function (signinErr, signinRes) {
                // Handle signin error
                if (signinErr) done(signinErr);

                var token = signinRes.body.token;


                // Save a new UnitOfMeasure
                agent.post('/unitofmeasures')
                    .send(unitOfMeasure)
                    .set('token', token)
                    .expect(400)
                    .end(function (unitOfMeasureSaveErr, unitOfMeasureSaveRes) {
                        // Set message assertion
                        (unitOfMeasureSaveRes.body.message).should.match('Please fill Unit Of Measure Symbol\b');

                        // Handle UnitOfMeasure save error
                        done();
                    });
            });
    });

    it('should be able to update UnitOfMeasure instance if signed in', function (done) {
        agent.post('/auth/signin')
            .send(credentials)
            .expect(200)
            .end(function (signinErr, signinRes) {
                // Handle signin error
                if (signinErr) done(signinErr);

                var token = signinRes.body.token;


                // Save a new UnitOfMeasure
                agent.post('/unitofmeasures')
                    .send(unitOfMeasure)
                    .set('token', token)
                    .expect(200)
                    .end(function (unitOfMeasureSaveErr, unitOfMeasureSaveRes) {
                        // Handle UnitOfMeasure save error
                        if (unitOfMeasureSaveErr) done(unitOfMeasureSaveErr);

                        // Update UnitOfMeasure name
                        unitOfMeasure.name = 'Kgs';

                        // Update existing UnitOfMeasure
                        agent.put('/unitofmeasures/' + unitOfMeasureSaveRes.body._id)
                            .send(unitOfMeasure)
                            .set('token', token)
                            .expect(200)
                            .end(function (unitOfMeasureUpdateErr, unitOfMeasureUpdateRes) {
                                // Handle UnitOfMeasure update error
                                if (unitOfMeasureUpdateErr) done(unitOfMeasureUpdateErr);

                                // Set assertions
                                (unitOfMeasureUpdateRes.body._id).should.equal(unitOfMeasureUpdateRes.body._id);
                                (unitOfMeasureUpdateRes.body.name).should.match('Kgs');

                                // Call the assertion callback
                                done();
                            });
                    });
            });
    });

    it('should be able to get a list of UnitOfMeasures if signed in', function (done) {
        agent.post('/auth/signin')
            .send(credentials)
            .expect(200)
            .end(function (signinErr, signinRes) {
                // Handle signin error
                if (signinErr) done(signinErr);

                var token = signinRes.body.token;

                // Save a new UnitOfMeasure
                agent.post('/unitofmeasures')
                    .send(unitOfMeasure)
                    .set('token', token)
                    .expect(200)
                    .end(function (unitOfMeasureSaveErr, unitOfMeasureSaveRes) {
                        // Handle UnitOfMeasure save error
                        if (unitOfMeasureSaveErr) done(unitOfMeasureSaveErr);
                        request(app).get('/unitofmeasures')
                            .set('token', token)
                            .end(function (req, res) {
                                // Set assertion
                                res.body.should.be.instanceof(Array).and.have.lengthOf(1);

                                // Call the assertion callback
                                done();
                            });
                    });
            });
    });


    it('should be able to get a single UnitOfMeasure if signed in', function (done) {
        agent.post('/auth/signin')
            .send(credentials)
            .expect(200)
            .end(function (signinErr, signinRes) {
                // Handle signin error
                if (signinErr) done(signinErr);

                var token = signinRes.body.token;


                // Save a new UnitOfMeasure
                agent.post('/unitofmeasures')
                    .send(unitOfMeasure)
                    .set('token', token)
                    .expect(200)
                    .end(function (unitOfMeasureSaveErr, unitOfMeasureSaveRes) {
                        // Handle UnitOfMeasure save error
                        if (unitOfMeasureSaveErr) done(unitOfMeasureSaveErr);

                        request(app).get('/unitofmeasures/' + unitOfMeasureSaveRes.body._id)
                            .set('token', token)
                            .end(function (req, res) {
                                // Set assertion
                                res.body.should.be.an.Object.with.property('name', unitOfMeasure.name);

                                // Call the assertion callback
                                done();
                            });
                    });
            });
    });

    /*it('should be able to delete UnitOfMeasure instance if signed in', function (done) {
        agent.post('/auth/signin')
            .send(credentials)
            .expect(200)
            .end(function (signinErr, signinRes) {
                // Handle signin error
                if (signinErr) done(signinErr);

                var token = signinRes.body.token;


                // Save a new UnitOfMeasure
                agent.post('/unitofmeasures')
                    .send(unitOfMeasure)
                    .set('token', token)
                    .expect(200)
                    .end(function (unitOfMeasureSaveErr, unitOfMeasureSaveRes) {
                        // Handle UnitOfMeasure save error
                        if (unitOfMeasureSaveErr) done(unitOfMeasureSaveErr);

                        // Delete existing UnitOfMeasure
                        agent.delete('/unitofmeasures/' + unitOfMeasureSaveRes.body._id)
                            .send(unitOfMeasure)
                            .set('token', token)
                            .expect(200)
                            .end(function (unitOfMeasureDeleteErr, unitOfMeasureDeleteRes) {
                                // Handle UnitOfMeasure error error
                                if (unitOfMeasureDeleteErr) done(unitOfMeasureDeleteErr);

                                // Set assertions
                                (unitOfMeasureDeleteRes.body._id).should.equal(unitOfMeasureSaveRes.body._id);

                                // Call the assertion callback
                                done();
                            });
                    });
            });
    });

    it('should not be able to delete UnitOfMeasure instance if not signed in', function (done) {
        // Set UnitOfMeasure user
        unitOfMeasure.user = user;

        // Create new UnitOfMeasure model instance
        var unitOfMeasureObj = new UnitOfMeasure(unitOfMeasure);

        // Save the UnitOfMeasure
        unitOfMeasureObj.save(function () {
            // Try deleting UnitOfMeasure
            request(app).delete('/unitofmeasures/' + unitOfMeasureObj._id)
                .expect(401)
                .end(function (unitOfMeasureDeleteErr, unitOfMeasureDeleteRes) {
                    // Set message assertion
                    if (unitOfMeasureDeleteErr) {
                        done(unitOfMeasureDeleteErr);
                    } else {
                        done();
                    }

                });

        });
    });*/

    afterEach(function (done) {
      /*  User.remove().exec();*/
        //Company.remove().exec();
        UnitOfMeasure.remove().exec();
        done();
    });
    after(function (done) {
        User.remove().exec();
        Company.remove().exec();
        Contact.remove().exec();
        done();
    });
});
