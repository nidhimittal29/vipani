'use strict';

var should = require('should'),
    request = require('supertest'),
    app = require('../../server'),

    mongoose = require('mongoose'),
    User = mongoose.model('User'),
    Inventory = mongoose.model('Inventory'),
    Contact = mongoose.model('Contact'),
    Company = mongoose.model('Company'),
    Category = mongoose.model('Category'),
    Product = mongoose.model('Product'),
    BusinessUnit = mongoose.model('BusinessUnit'),
    ItemMaster = mongoose.model('ItemMaster'),
    StockMaster = mongoose.model('StockMaster'),
    UnitOfMeasure = mongoose.model('UnitOfMeasure'),
    HsnCodes = mongoose.model('Hsncodes'),
    TaxGroup = mongoose.model('TaxGroup'),
    commonutil=require('../tests/utils/common.batch.users.utils'),
    commonBatchUserUtil=require('./utils/common.batch.users.utils'),
    commonBatchInventoriesUtil=require('./utils/common.batch.inventories.utils'),
    commonBatchCompanyUtil=require('./utils/common.batch.company.utils'),
    agent = request.agent(app);

var user,credentials,token,businessUnitId;

describe('Inventory CRUD tests', function () {

    before(function (done) {
        var regUser = {
            username: 'test@test.com',
            password: 'password',
            ConfirmPassword: 'password',
            firstName: 'First Name',
            lastName: 'Last Name',
            companyName: 'nVipani',
            businessType: 'Trader',
            registrationCategory: 'Manufacturer',
            selectedSegments: ['Coffee', 'Rice'],
            mobile: '0123456789',
            acceptTerms: true
        };
        commonutil.createUser(regUser, agent, function (err, res) {
            should.not.exist(err);
            user = res;
            credentials = {
                username: 'test@test.com',
                password: 'password'
            };

            done();
        });
    });
    beforeEach(function(done){
        agent.post('/auth/signin')
            .send(credentials)
            .expect(200)
            .end(function (signinErr, signinRes) {
                // Handle signin error
                should.not.exist(signinErr);
                token = signinRes.body.token;
                commonBatchCompanyUtil.getUserCompanyDefaultBusinessUnit(token, agent, function (defaultBusinessUnitErr, bunit) {
                    should.not.exist(defaultBusinessUnitErr);
                    businessUnitId = bunit.body.toString();
                    done();
                });
            });
    });

    it('should fetch subcategories for valid subcategories passed in',function(done){
        var inputdata = [{mainCategoryName:'Agriculture',subCategoryName:'Cereals1'},
            {mainCategoryName:'Agriculture',subCategoryName:'Coffee'},
            {mainCategoryName:'Agriculture',subCategoryName:'Cereals'}];
        agent.post('/fetchSubCategoryByName')
            .send(inputdata)
            .set('token',token)
            .expect(200)
            .end(function(fetchErr,fetchedData){
               should.not.exist(fetchErr);
               var results = fetchedData.res.body;
                results.should.be.instanceof(Array).and.have.lengthOf(3);
                should.exist(results[0].error);
                should.exist(results[1]._id);
                should.exist(results[2]._id);
                done();
            });
    });

    it('should import selected subcategories step2 of import process',function(done){
        var inputdata = [{mainCategoryName:'Agriculture',subCategoryName:'Cereals'}];
        agent.post('/fetchSubCategoryByName')
            .send(inputdata)
            .set('token',token)
            .expect(200)
            .end(function(fetchErr,fetchedData) {
                should.not.exist(fetchErr);
                var results = fetchedData.res.body;
                var subCategory1Id = results[0]._id;
                var mainCategoryId = results[0].parent._id;
                var step2InputData = require('./data/product-import-file1.json');
                step2InputData.inputdata[0].subCategoryId = subCategory1Id;
                step2InputData.inputdata[0].mainCategoryId = mainCategoryId;
                agent.post('/fetchProductsName')
                    .send(step2InputData.inputdata)
                    .set('token', token)
                    .expect(200)
                    .end(function (fetchErr, fetchedData) {
                        should.not.exist(fetchErr);
                        var results = fetchedData.res.body;
                        results.should.be.instanceof(Array).and.have.lengthOf(1);
                        results[0].value.should.be.instanceof(Array).and.have.lengthOf(8);
                        results[0].header.should.be.instanceof(Array).and.have.lengthOf(77);
                        should.exist(results[0].error[0].data);
                        results[0].error[0].data.lineData.should.be.instanceof(Array).and.have.lengthOf(77);
                        results[0].error[0].data.srcLine.should.equal(12);
                        results[0].error[0].errorMessage.should.equal('No unit of measure with symbol -Baga, type - Simple');
                        done();
                    });
            });
    });
    it('should import all selected products to inventory',function(done){
        var inputdata = [{mainCategoryName:'Agriculture',subCategoryName:'Cereals'}];
        agent.post('/fetchSubCategoryByName')
            .send(inputdata)
            .set('token',token)
            .expect(200)
            .end(function(fetchErr,fetchedData) {
                should.not.exist(fetchErr);
                var results = fetchedData.res.body;
                var subCategory1Id = results[0]._id;
                var mainCategoryId = results[0].parent._id;
                var step2InputData = require('./data/product-import-file1.json');
                step2InputData.inputdata[0].subCategoryId = subCategory1Id;
                step2InputData.inputdata[0].mainCategoryId = mainCategoryId;
                agent.post('/fetchProductsName')
                    .send(step2InputData.inputdata)
                    .set('token', token)
                    .expect(200)
                    .end(function (fetchErr, fetchedData) {
                        should.not.exist(fetchErr);
                        var results = fetchedData.res.body;
                        results.should.be.instanceof(Array).and.have.lengthOf(1);
                        results[0].value.should.be.instanceof(Array).and.have.lengthOf(8);
                        results[0].header.should.be.instanceof(Array).and.have.lengthOf(77);
                        should.exist(results[0].error[0].data);
                        results[0].error[0].data.lineData.should.be.instanceof(Array).and.have.lengthOf(77);
                        results[0].error[0].data.srcLine.should.equal(12);
                        results[0].error[0].errorMessage.should.equal('No unit of measure with symbol -Baga, type - Simple');
                        results[0].value.forEach(function (result) {
                            result.selected = true;
                        });
                        var ui=[];
                        results[0].value.forEach(function(inventoryValue){
                            var userInventories = {};
                            userInventories.productBrand = inventoryValue.productBrandId;
                            userInventories.inventory=[];
                            userInventories.inventory.push({srcLine:inventoryValue.srcLine,itemMasterId:inventoryValue.product,unitOfMeasure:inventoryValue.unitOfMeasure});
                            ui.push(userInventories);
                        });

                        agent.post('/inventories/createUserItemMaster')
                            .send({userinventories:ui,businessUnit:businessUnitId})
                            .set('token', token)
                            .expect(200)
                            .end(function (useritemerr, useritemdata) {
                                should.not.exist(useritemerr);
                                var userItemMasterRes = useritemdata.res.body;
                                results[0].value.should.be.instanceof(Array).and.have.lengthOf(8);
                                results[0].value[0].srcLine.should.equal(6);
                                results[0].value[1].srcLine.should.equal(8);
                                results[0].value[2].srcLine.should.equal(10);
                                results[0].value[3].srcLine.should.equal(14);
                                results[0].value[4].srcLine.should.equal(16);
                                results[0].value[5].srcLine.should.equal(18);
                                results[0].value[6].srcLine.should.equal(20);
                                results[0].value[7].srcLine.should.equal(22);
                                commonBatchCompanyUtil.getUserCompanyDefaultBusinessUnit(token,agent,function(err,bures){
                                    should.not.exist(err);
                                    commonBatchInventoriesUtil.getInventories(token,bures.body,agent,function(invErr,invRes){
                                        should.not.exist(invErr);
                                        invRes.body.inventory.length.should.equal(8);
                                        done();
                                    });
                                });


                            });
                    });
            });
    });
    it('should not import if subcategory name is not specified',function(done){
        var inputdata = [{mainCategoryName:'Agriculture',subCategoryName:'Cereals'}];
        agent.post('/fetchSubCategoryByName')
            .send(inputdata)
            .set('token',token)
            .expect(200)
            .end(function(fetchErr,fetchedData) {
                should.not.exist(fetchErr);
                var results = fetchedData.res.body;
                var subCategory1Id = results[0]._id;
                var mainCategoryId = results[0].parent._id;
                var step2InputData = require('./data/product-import-file2.json');
                step2InputData.inputdata[0].subCategoryId = subCategory1Id;
                step2InputData.inputdata[0].mainCategoryId = mainCategoryId;
                agent.post('/fetchProductsName')
                    .send(step2InputData.inputdata)
                    .set('token', token)
                    .expect(200)
                    .end(function (fetchErr, fetchedData) {
                        should.not.exist(fetchErr);
                        var results = fetchedData.res.body;
                        results.should.be.instanceof(Array).and.have.lengthOf(1);
                        results[0].error.should.be.instanceof(Array).and.have.lengthOf(1);
                        results[0].error[0].errorMessage.should.match('Please fill Brand name\b');
                        done();
                    });
            });
    });
    it('should  import batches if available',function(done){
        var inputdata = [{mainCategoryName:'Agriculture',subCategoryName:'Cereals'}];
        agent.post('/fetchSubCategoryByName')
            .send(inputdata)
            .set('token',token)
            .expect(200)
            .end(function(fetchErr,fetchedData) {
                should.not.exist(fetchErr);
                var results = fetchedData.res.body;
                var subCategory1Id = results[0]._id;
                var mainCategoryId = results[0].parent._id;
                var step2InputData = require('./data/product-import-file3.json');
                step2InputData.inputdata[0].subCategoryId = subCategory1Id;
                step2InputData.inputdata[0].mainCategoryId = mainCategoryId;
                agent.post('/fetchProductsName')
                    .send(step2InputData.inputdata)
                    .set('token', token)
                    .expect(200)
                    .end(function (fetchErr, fetchedData) {
                        should.not.exist(fetchErr);
                        var results = fetchedData.res.body;
                        results.should.be.instanceof(Array).and.have.lengthOf(1);
                        results[0].error.should.be.instanceof(Array).and.have.lengthOf(0);
                        var ui=[];
                        results[0].value.forEach(function(inventoryValue){
                            var userInventories = {};
                            userInventories.productBrand = inventoryValue.productBrandId;
                            userInventories.inventory=[];

                            userInventories.inventory.push(
                                {
                                    srcLine:inventoryValue.srcLine,itemMasterId:inventoryValue.product,unitOfMeasure:inventoryValue.unitOfMeasure,
                                    stockMaster:
                                        [{batchNumber:step2InputData.inputdata[0].value[0].lineData[77],
                                            openingBalance:step2InputData.inputdata[0].value[0].lineData[78],
                                            currentBalance:step2InputData.inputdata[0].value[0].lineData[79],
                                            MRP:step2InputData.inputdata[0].value[0].lineData[80],
                                            manufacturingDate:new Date(step2InputData.inputdata[0].value[0].lineData[81]),
                                            packagingDate:new Date(step2InputData.inputdata[0].value[0].lineData[82])
                                        }]
                                }
                                );
                            ui.push(userInventories);
                        });

                        agent.post('/inventories/createUserItemMaster')
                            .send({userinventories:ui,businessUnit:businessUnitId})
                            .set('token', token)
                            .expect(200)
                            .end(function (useritemerr, useritemdata) {
                                should.not.exist(useritemerr);
                                var userItemMasterRes = useritemdata.res.body;
                                results[0].value.should.be.instanceof(Array).and.have.lengthOf(1);
                                results[0].value[0].srcLine.should.equal(6);

                                commonBatchCompanyUtil.getUserCompanyDefaultBusinessUnit(token,agent,function(err,bures){
                                    should.not.exist(err);
                                    commonBatchInventoriesUtil.getInventories(token,bures.body,agent,function(invErr,invRes){
                                        should.not.exist(invErr);
                                        invRes.body.inventory.length.should.equal(1);
                                        commonBatchInventoriesUtil.getInventoryById(invRes.body.inventory[0]._id,token,agent,function(invDetErr,invDetRes){
                                            should.not.exist(invDetErr);
                                            var sm = invDetRes.body.stockMasters;
                                            sm.should.be.instanceof(Array).and.have.lengthOf(1);
                                            sm[0].batchNumber.should.equal('BB-SONMR-20171223');
                                            sm[0].openingBalance.should.equal(2000);
                                            sm[0].currentBalance.should.equal(2500);
                                            done();
                                        });

                                    });
                                });


                            });
                    });
            });
    });

    it('should not import batches if currentBalance is not set',function(done) {
        var inputdata = [{mainCategoryName:'Agriculture',subCategoryName:'Cereals'}];
        agent.post('/fetchSubCategoryByName')
            .send(inputdata)
            .set('token',token)
            .expect(200)
            .end(function(fetchErr,fetchedData) {
                should.not.exist(fetchErr);
                var results = fetchedData.res.body;
                var subCategory1Id = results[0]._id;
                var mainCategoryId = results[0].parent._id;
                var step2InputData = require('./data/product-import-file4.json');
                step2InputData.inputdata[0].subCategoryId = subCategory1Id;
                step2InputData.inputdata[0].mainCategoryId = mainCategoryId;
                agent.post('/fetchProductsName')
                    .send(step2InputData.inputdata)
                    .set('token', token)
                    .expect(200)
                    .end(function (fetchErr, fetchedData) {
                        should.not.exist(fetchErr);
                        var results = fetchedData.res.body;
                        results.should.be.instanceof(Array).and.have.lengthOf(1);
                        results[0].error.should.be.instanceof(Array).and.have.lengthOf(0);
                        var ui=[];
                        results[0].value.forEach(function(inventoryValue){
                            var userInventories = {};
                            userInventories.productBrand = inventoryValue.productBrandId;
                            userInventories.inventory=[];

                            userInventories.inventory.push(
                                {
                                    srcLine:inventoryValue.srcLine,itemMasterId:inventoryValue.product,unitOfMeasure:inventoryValue.unitOfMeasure,
                                    stockMaster:
                                        [{batchNumber:step2InputData.inputdata[0].value[0].lineData[77],
                                            openingBalance:step2InputData.inputdata[0].value[0].lineData[78],
                                            currentBalance:step2InputData.inputdata[0].value[0].lineData[79],
                                            MRP:step2InputData.inputdata[0].value[0].lineData[80],
                                            manufacturingDate:new Date(step2InputData.inputdata[0].value[0].lineData[81]),
                                            packagingDate:new Date(step2InputData.inputdata[0].value[0].lineData[82])
                                        }]
                                }
                            );
                            ui.push(userInventories);
                        });

                        agent.post('/inventories/createUserItemMaster')
                            .send({userinventories:ui,businessUnit:businessUnitId})
                            .set('token', token)
                            .expect(200)
                            .end(function (useritemerr, useritemdata) {
                                should.exist(useritemerr);
                                var userItemMasterRes = useritemdata.res.body;
                                userItemMasterRes.message.should.equal('No Proper Stock');
                                done();
                            });
                    });
            });
    });
    afterEach(function (done) {
        Inventory.remove().exec();
        ItemMaster.remove().exec();
        StockMaster.remove().exec();
        done();
    });

    after(function (done) {
        User.remove().exec();
        BusinessUnit.remove().exec();
        Company.remove().exec();
        Contact.remove().exec();
        done();
    });

});
