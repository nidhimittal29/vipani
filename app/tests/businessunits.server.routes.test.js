/**
 *author ayesha
 * Includes test cases for business units routes
 *
 */
'use strict';

var should = require('should'),
    request = require('supertest'),
    app = require('../../server'),
    mongoose = require('mongoose'),
    commonUtils=require('./utils/common.utils'),
    User = mongoose.model('User'),
    Contact = mongoose.model('Contact'),
    Company = mongoose.model('Company'),
    BusinessUnit=mongoose.model('BusinessUnit'),
    commonUserUtil=require('./utils/common.batch.users.utils'),
    async = require('async'),
    agent = request.agent(app);

/**
 * Globals
 */
var credentials,user,businessUnit,token,productInventory,unitIdsForRemove={businessUnits:[]};
describe('Business units CRUD tests', function () {

    var registerUser = function(done){
        var regUser = {
            username: 'test@test.com',
            companyName: 'nVipani',
            businessType: 'Trader',
            categorySeller: true,
            categoryBuyer: true,
            categoryMediator: true,
            mobile: '0123456789',
            acceptTerms: true
        };
        commonUserUtil.createEachStepUser({username:regUser.username,password:'password',issendotp:true},200,agent,function (presendotpErr,presendotpRes) {
            should.not.exist(presendotpErr);
            presendotpRes.body.status.should.equal(true);
            var otp = presendotpRes.body.otp;
            presendotpRes.body.message.should.equal('An OTP has been sent to Email :'+regUser.username+'. ' + otp + ' is your One Time Password (OTP)');
            commonUserUtil.createEachStepUser({
                username: regUser.username,
                password: 'password',
                isverifyotp: true,
                otp: otp
            }, 200, agent, function (verifyotpErr, verifyotpRes) {
                should.not.exist(verifyotpErr);
                verifyotpRes.body.status.should.equal(true);
                verifyotpRes.body.message.should.equal('User is verified :'+regUser.username);
                var registrationCategory = verifyotpRes.body.registrationCategories[0]._id;
                var segments = [{
                    segment: verifyotpRes.body.segments[0]._id,
                    categories: [{category: verifyotpRes.body.categories[0]._id}]
                }];
                commonUserUtil.createEachStepUser({
                    username: regUser.username,
                    password: 'password',
                    ispassword: true,
                    registrationCategory: registrationCategory,
                    selectedSegments: segments,
                    companyName: regUser.companyName
                }, 200, agent, function (verifiedErr, verifiedRes) {
                    should.not.exist(verifiedErr);
                    verifiedRes.body.status.should.equal(true);
                    verifiedRes.body.message.should.equal('User is successfully registered');
                    user = verifiedRes.body.user;
                    credentials = {
                        username: 'test@test.com',
                        password: 'password'
                    };

                    done();
                });
            });
        });
    };
    before(function (done) {
        registerUser(function(err) {
            if (err instanceof Error) {
                should.not.exist(err);
            }
            done();
        });
    });
    beforeEach(function (done) {
        agent.post('/auth/signin')
            .send(credentials)
            .expect(200)
            .end(function (err, res) {
                // Handle signin error
                should.not.exist(err);
                var userres = res.body;
                token = res.body.token;

                businessUnit={
                    name:'Bangalore Godown',
                    type:'Stockpoint',
                    emails:[{email:'blrgdn@test.com',isPrimary:true,emailType:'Work'}],
                    addresses:[{
                        addressLine:'address line1',
                        city:'bangalore',
                        state:'Karnataka',
                        country:'India',
                        pinCode:560075,
                        primary:true
                    }],

                    disabled:false,
                    deleted:false,
                    company:user.company,
                    user:user._id
                };

                done();
            });

    });

    function createNewBusinessUnit(unit,done) {
        agent.post('/branches')
            .set('token',token)
            .send(unit)
            .expect(200)
            .end(function(err,res) {
                //contact should have been created
                should.not.exist(err);
                var newUnit = res.body;
                newUnit.name.should.equal(unit.name);
                done(newUnit);
            });
    }
    function createNewBusinessUser(user, done) {
        agent.post('/auth/adduser')
            .set('token', token)
            .send(user)
            .end(function (buser2Err, buser2Res) {
                should.not.exist(buser2Err);
                buser2Res.body.businessUser.should.be.instanceof(Array);
                var newuser = buser2Res.body.businessUser[buser2Res.body.businessUser.length - 1];
                newuser.businessUnits.length.should.equal(1);
                newuser.status.should.equal('Request');
                done(newuser);
            });
    }
    function getAdminUserGroup(done){
        mongoose.model('UserGroup').findOne({name:'Admin'}).exec(function(err,userGroup){
            should.not.exist(err);
            done(userGroup);
        });
    }
    /**
     * case 1:Create business unit if user has signed in +ve test case
     * case 2:Check business unit status at company level +ve test case
     */
    it('should be able to add business unit and check status at company level',function (done) {

        createNewBusinessUnit(businessUnit,function (unit) {
            var businessUnit=unit;
            businessUnit.addresses.length.should.equal(1);
            businessUnit.emails.length.should.equal(1);
            businessUnit.company._id.should.equal(user.company);
            businessUnit.employees.length.should.equal(1);
            businessUnit.disabled.should.be.equal(false);
            var emp = businessUnit.employees[0];
            emp.incharge.should.equal(true);
            emp.user._id.should.equal(user._id);
            agent.get('/companies/'+user.company)
                    .set('token',token)
                    .expect(200)
                    .end(function (companyErr, companyRes) {
                        should.exist(companyRes);
                        var company=companyRes.body;
                        var companyBUnit=company.businessUnits.filter(function (unit) {
                            return unit.businessUnit===businessUnit._id;
                        });
                        should.exist(companyBUnit);
                        companyBUnit.length.should.equal(1);
                        companyBUnit[0].status.should.equal('Active');
                        var companyEmployee=company.employees.filter(function (eachEmployee) {
                            return eachEmployee.user === user._id;
                        });
                        var companyEmployeeBUnit=companyEmployee[0].businessUnits.filter(function (eachUnit) {
                            return eachUnit.businessUnit===businessUnit._id;
                        });
                        companyEmployee[0].businessUnits[companyEmployee[0].businessUnits.indexOf(companyEmployeeBUnit[0])].status.should.equal('Active');
                        unitIdsForRemove.businessUnits.push({_id:businessUnit._id});
                        done();
                    });
            });
    });

    /**
     * case 1:Check business unit is created or not if no name is provided -ve test case
     * case 2:Check business unit is created or not if company is not set -ve test case
     * case 3:Check business unit is created or not if company id is not set -ve test case
     */
    it('should not be able to create a businessunit if company or name or company id is not set',function(done){
        businessUnit.name='';
        agent.post('/branches')
            .set('token',token)
            .send(businessUnit)
            .expect(200)
            .end(function(err,res){
                should.exist(err);
                should.exist(res.body.message);
                res.body.message.should.equal('Please fill Business Unit Name\b');
                businessUnit.company='';
                agent.post('/branches')
                    .set('token',token)
                    .send(businessUnit)
                    .expect(200)
                    .end(function(err,res){
                        should.exist(err);
                        should.exist(res.body.message);
                        res.body.message.should.equal('The company must be set while creating a business unit');
                        businessUnit.company='';
                        agent.post('/branches')
                            .set('token',token)
                            .send(businessUnit)
                            .expect(200)
                            .end(function(err,res){
                                should.exist(err);
                                should.exist(res.body.message);
                                res.body.message.should.equal('The company must be set while creating a business unit');
                                done();
                            });
                    });
            });

    });

    /**
     * case 1:Create business user and add it as incharge to business unit +ve test case
     */
    it('should be able to add a user and then add it as incharge to businessunit',function(done){
        //create business units and try adding them to business unit
        var newUser = {user:user._id,
            userName:'abc123@test123.com',
            company:user.company,
            userGroup:user.companies[0].userGroup
        };
        createNewBusinessUser(newUser,function (newUser1) {
            newUser=newUser1;
            businessUnit.employees=[];
            businessUnit.employees.push({user:newUser.user._id,incharge:true,userGroup:newUser.userGroup._id});
            createNewBusinessUnit(businessUnit,function (newUnit) {
                businessUnit=newUnit;
                businessUnit.addresses.length.should.equal(1);
                businessUnit.emails.length.should.equal(1);
                businessUnit.company._id.should.equal(user.company);
                businessUnit.employees.length.should.equal(1);
                var emp=businessUnit.employees[0];
                emp.incharge.should.equal(true);
                emp.user._id.should.equal(newUser.user._id);
                unitIdsForRemove.businessUnits.push({_id:businessUnit._id});
                done();
            });
        });
    });
    /**
     * case 1:Create list of business units and check list getting properly or not
     * case 2:Check status of business unit at company level
     * case 3:Disable business unit at company level +ve test case
     * case 4:Enable business unit at company level +ve test case
     * case 5:Delete business unit at company level +ve test case
     */
    it('should be able to get list of business units',function(done){
        var listUnit1={name:'Bangalore Godown',
            type:'Stockpoint',
            emails:[{email:'blrgdn@test.com',isPrimary:true,emailType:'Work'}],
            company:user.company,
            user:user._id},
            listUnit2={name:'Hyderabad Godown',
            type:'Stockpoint',
            emails:[{email:'hyd@test.com',isPrimary:true,emailType:'Work'}],
            company:user.company,
            user:user._id};
        createNewBusinessUnit(listUnit1,function (unit) {
            listUnit1=unit;
            createNewBusinessUnit(listUnit2,function (newUnit) {
                listUnit2=newUnit;
                agent.get('/branches')
                    .set('token',token)
                    .expect(200)
                    .end(function(listErr,listRes) {
                        should.not.exist(listErr);
                        should.exist(listRes);
                        var list = listRes.body;
                        list.should.be.instanceof(Array);
                        list[list.length-1].name.should.equal(listUnit2.name);
                        list[list.length-2].name.should.equal(listUnit1.name);
                        //Check list of business users
                        agent.get('/companies/'+user.company)
                            .set('token',token)
                            .end(function (buserListErr, buserListRes) {
                                should.not.exist(buserListErr);
                                var unitsList = buserListRes.body.businessUnits;
                                should.exist(unitsList);
                                unitsList.should.be.instanceof(Array);
                                unitsList.length.should.equal(3);
                                unitsList[unitsList.length - 1].status.should.equal('Active');
                                unitsList[unitsList.length - 2].status.should.equal('Active');
                                //Disable the business unit
                                agent.post('/businessUnitsDisable')
                                    .set('token',token)
                                    .send({businessUnits:[{_id:listUnit1._id}]})
                                    .expect(200).end(function(buErr,busRes){
                                    should.not.exist(buErr);
                                    var businessUnitList = busRes.body.businessUnits;
                                    //check response
                                    var retBu = businessUnitList.filter(function(bu){
                                        return bu.businessUnit._id.toString()===listUnit1._id.toString();
                                    });
                                    should.exist(retBu);
                                    retBu.length.should.equal(1);
                                    retBu[0].businessUnit.disabled.should.equal(true);
                                    businessUnitList.length.should.equal(3);
                                    //fetch it and check
                                    BusinessUnit.findOne({_id:listUnit1._id}).exec(function(err,bu){
                                        should.not.exist(err);
                                        bu.disabled.should.equal(true);
                                        //Enable business unit
                                        agent.post('/businessUnitsEnable')
                                            .set('token',token)
                                            .send({businessUnits:[{_id:listUnit1._id}]})
                                            .expect(200).end(function(buErr,busRes){
                                            should.not.exist(buErr);
                                            var businessUnitList = busRes.body.businessUnits;
                                            //check response
                                            var retBu = businessUnitList.filter(function(bu){
                                                //console.log('bumatch'+bu.businessUnit._id);
                                                return bu.businessUnit._id.toString()===listUnit1._id.toString();
                                            });
                                            should.exist(retBu);
                                            retBu.length.should.equal(1);
                                            retBu[0].businessUnit.disabled.should.equal(false);
                                            businessUnitList.length.should.equal(3);
                                            //fetch it and check
                                            BusinessUnit.findOne({_id:listUnit1._id}).exec(function(err,bu){
                                                should.not.exist(err);
                                                bu.disabled.should.equal(false);
                                                agent.post('/businessUnitsRemove')
                                                    .set('token',token)
                                                    .send({businessUnits:[{_id:listUnit1._id}]})
                                                    .expect(200).end(function(buErr,busRes){
                                                    should.not.exist(buErr);
                                                    var businessUnitList = busRes.body.businessUnits;
                                                    var retBu = businessUnitList.filter(function(bu){
                                                        //console.log('bumatch'+bu.businessUnit._id);
                                                        return bu.businessUnit._id.toString()===listUnit1._id.toString();
                                                    });
                                                    should.exist(retBu);
                                                    retBu.length.should.equal(0);
                                                    //retBu[0].businessUnit.deleted.should.equal(true);
                                                    businessUnitList.length.should.equal(2);
                                                    //fetch it and check
                                                    BusinessUnit.findOne({_id:listUnit1._id}).exec(function(err,bu){
                                                        should.not.exist(err);
                                                        bu.deleted.should.equal(true);
                                                        done();

                                                    });
                                                });
                                            });
                                        });
                                    });

                                });
                            });
                    });
            });
        });
    });

    /**
     * case 1:Update business unit with basic information +ve test case
     * case 2:Update business unit with emails,phones and address +ve test case
     * case 3:Update business unit with bank details +ve test case
     */
    it('should be able to edit business unit',function(done){
         var listUnit1={name:'Hyderabad Godown',
            type:'Stockpoint',
            emails:[{email:'hyd@test.com',isPrimary:true,emailType:'Work'}],
            company:user.company,
            user:user._id};
        createNewBusinessUnit(listUnit1,function (unit) {
            listUnit1 = unit;
            agent.get('/branches')
                .set('token', token)
                .expect(200)
                .end(function (listErr, listRes) {
                    should.not.exist(listErr);
                    should.exist(listRes);
                    var list = listRes.body;
                    list.should.be.instanceof(Array);
                    list[list.length - 1].name.should.equal(listUnit1.name);
                    var updateUnit={name:'Chennai Godown',
                        type:'Stockpoint',
                        emails:[{email:'hyd@test.com',isPrimary:true,emailType:'Work'}],
                        company:user.company,
                        user:user._id};
                    agent.put('/branches/'+listUnit1._id)
                        .set('token', token)
                        .send({
                            businessUnit:updateUnit,
                            isUpdate:true
                        })
                        .expect(200)
                        .end(function (listErr, listRes) {
                            should.not.exist(listErr);
                            should.exist(listRes);
                            var updatedUnit = listRes.body;
                            updatedUnit.name.should.equal(updateUnit.name);
                            updatedUnit.company.should.equal(user.company);
                            updateUnit={name:'Chennai Godown',
                                type:'Stockpoint',
                                emails:[{email:'hyd@test.com',primary:true,emailType:'Work'},{email:'Chennai@test.com',primary:false,emailType:'Personal'}],
                                phones:[{phoneNumber:'4785123694',primary:true,phoneType:'Mobile'},{phoneNumber:'5236987458',primary:false,phoneType:'Work'}],
                                addresses:[{addressLine:'5-27',city:'Chennai',state:'Tamilnadu',country:'India',pinCode:'452178'},
                                    {addressLine:'1/28H27',city:'Chennai',state:'Tamilnadu',country:'India',pinCode:'452178'}],
                                company:user.company,
                                user:user._id};
                            agent.put('/branches/'+listUnit1._id)
                                .set('token', token)
                                .send({
                                    businessUnit:updateUnit,
                                    isUpdate:true
                                })
                                .expect(200)
                                .end(function (listErr, listRes) {
                                    should.not.exist(listErr);
                                    should.exist(listRes);
                                    var updatedUnit = listRes.body;
                                    updatedUnit.name.should.equal(updateUnit.name);
                                    should.exist(updatedUnit.emails);
                                    updatedUnit.emails.length.should.equal(2);
                                    updatedUnit.emails[0].emailType.should.equal(updateUnit.emails[0].emailType);
                                    updatedUnit.emails[0].email.should.equal(updateUnit.emails[0].email);
                                    updatedUnit.emails[0].primary.should.equal(updateUnit.emails[0].primary);
                                    updatedUnit.emails[1].emailType.should.equal(updateUnit.emails[1].emailType);
                                    updatedUnit.emails[1].email.should.equal(updateUnit.emails[1].email);
                                    updatedUnit.emails[1].primary.should.equal(updateUnit.emails[1].primary);
                                    should.exist(updatedUnit.phones);
                                    updatedUnit.phones.length.should.equal(2);
                                    updatedUnit.phones[0].phoneType.should.equal(updateUnit.phones[0].phoneType);
                                    updatedUnit.phones[0].phoneNumber.should.equal(updateUnit.phones[0].phoneNumber);
                                    updatedUnit.phones[0].primary.should.equal(updateUnit.phones[0].primary);
                                    updatedUnit.phones[1].phoneType.should.equal(updateUnit.phones[1].phoneType);
                                    updatedUnit.phones[1].phoneNumber.should.equal(updateUnit.phones[1].phoneNumber);
                                    updatedUnit.phones[1].primary.should.equal(updateUnit.phones[1].primary);
                                    should.exist(updatedUnit.addresses);
                                    updatedUnit.addresses.length.should.equal(2);
                                    updatedUnit.addresses[0].addressLine.should.equal(updateUnit.addresses[0].addressLine);
                                    updatedUnit.addresses[0].city.should.equal(updateUnit.addresses[0].city);
                                    updatedUnit.addresses[0].state.should.equal(updateUnit.addresses[0].state);
                                    updatedUnit.addresses[0].country.should.equal(updateUnit.addresses[0].country);
                                    updatedUnit.addresses[0].pinCode.should.equal(updateUnit.addresses[0].pinCode);
                                    updatedUnit.addresses[1].addressLine.should.equal(updateUnit.addresses[1].addressLine);
                                    updatedUnit.addresses[1].city.should.equal(updateUnit.addresses[1].city);
                                    updatedUnit.addresses[1].state.should.equal(updateUnit.addresses[1].state);
                                    updatedUnit.addresses[1].country.should.equal(updateUnit.addresses[1].country);
                                    updatedUnit.addresses[1].pinCode.should.equal(updateUnit.addresses[1].pinCode);

                                    updateUnit={name:'Tirupathi Godown',
                                        type:'Stockpoint',
                                        emails:[{email:'tpt@test.com',primary:false,emailType:'Work'}],
                                        phones:[{phoneNumber:'4785123694',primary:true,phoneType:'Mobile'}],
                                        addresses:[{addressLine:'5-27',city:'Tirupsti',state:'Ap',country:'India',pinCode:'452178'}],
                                        bankAccountDetails:{bankAccountNumber:'334242432424234',accountType:'Saving',bankName:'State bank of India',ifscCode:'qwer0123456'},
                                        company:user.company,
                                        user:user._id};
                                    agent.put('/branches/'+listUnit1._id)
                                        .set('token', token)
                                        .send({
                                            businessUnit:updateUnit,
                                            isUpdate:true
                                        })
                                        .expect(200)
                                        .end(function (listErr, listRes) {
                                            should.not.exist(listErr);
                                            should.exist(listRes);
                                            var updatedUnit = listRes.body;
                                            updatedUnit.name.should.equal(updateUnit.name);
                                            should.exist(updatedUnit.bankAccountDetails);
                                            updatedUnit.bankAccountDetails.bankAccountNumber.should.equal(updateUnit.bankAccountDetails.bankAccountNumber);
                                            updatedUnit.bankAccountDetails.accountType.should.equal(updateUnit.bankAccountDetails.accountType);
                                            updatedUnit.bankAccountDetails.bankName.should.equal(updateUnit.bankAccountDetails.bankName);
                                            updatedUnit.bankAccountDetails.ifscCode.should.equal(updateUnit.bankAccountDetails.ifscCode);
                                            done();
                                        });
                                });
                        });
                });
        });
    });

    /**
     * case 1:Add new business user to business unit and check in company and business unit +ve test case
     * case 2:Add one or more company level business users to business unit +ve test case
     * case 3:Check list of business users in business unit +ve test case
     * case 3:Delete business users at business unit level and check in company and business unit +ve test case
     */
    it('should be able to add employees to businessunit',function(done) {
        //create business units and try adding them to business unit
        businessUnit = {
            name: 'Anantapur Godown',
            type: 'Stockpoint',
            emails: [{email: 'antp@test.com', isPrimary: true, emailType: 'Work'}],
            company: user.company,
            user: user._id
        };
        createNewBusinessUnit(businessUnit, function (unit) {
            businessUnit = unit;
            var newUser = {
                user: user._id,
                userName: 'AtpUser@test.com',
                company: user.company,
                userGroup: user.companies[0].userGroup,
                bunits: [businessUnit._id]
            }, companyUser1, companyUser2;
            createNewBusinessUser(newUser, function (newUser1) {
                newUser = newUser1;
                newUser.businessUnits[0].businessUnit.should.equal(businessUnit._id);
                newUser.status.should.equal('Request');
                //Check user in business unit
                agent.get('/branches/' + businessUnit._id)
                    .set('token', token)
                    .expect(200)
                    .end(function (unitErr, unitRes) {
                        should.not.exist(unitErr);
                        should.exist(unitRes);
                        var unit = unitRes.body;
                        unit.name.should.equal(businessUnit.name);
                        unit.employees[unit.employees.length - 1].user._id.should.equal(newUser.user._id);
                        //Check user in company level
                        agent.get('/companyBusinessUsers/' + user.company + '?page=1&limit=10')
                            .set('token', token)
                            .end(function (buserListErr, buserListRes) {
                                should.not.exist(buserListErr);
                                var usersList = buserListRes.body.employees;
                                should.exist(usersList);
                                usersList[usersList.length - 1].user.username.should.equal(newUser.user.username);
                                companyUser1 = {
                                    user: user._id,
                                    userName: 'AtpCmpUser1@test.com',
                                    company: user.company,
                                    userGroup: user.companies[0].userGroup
                                };
                                createNewBusinessUser(companyUser1, function (userRes) {
                                    companyUser1 = userRes;
                                    companyUser1.status.should.equal('Request');
                                    companyUser2 = {
                                        user: user._id,
                                        userName: 'AtpCmpUser2@test.com',
                                        company: user.company,
                                        userGroup: user.companies[0].userGroup
                                    };
                                    createNewBusinessUser(companyUser2, function (userRes) {
                                        companyUser2 = userRes;
                                        companyUser2.status.should.equal('Request');
                                        //add company employees to business unit
                                        agent.post('/auth/addMassUsersToUnit')
                                            .send({
                                                unitId: businessUnit._id,
                                                employees: [{
                                                    user: companyUser1.user._id,
                                                    userGroup: companyUser1.userGroup._id
                                                },
                                                    {
                                                        user: companyUser2.user._id,
                                                        userGroup: companyUser2.userGroup._id
                                                    }]
                                            })
                                            .set('token', token)
                                            .end(function (err, unitRes) {
                                                should.not.exist(err);
                                                should.exist(unit);
                                                var updateUnit = unitRes.body;
                                                updateUnit.employees.length.should.equal(4);
                                                //Check all users in business unit
                                                agent.get('/branches/' + businessUnit._id)
                                                             .set('token', token)
                                                    .expect(200)
                                                    .end(function (unitErr, unitRes) {
                                                        should.not.exist(unitErr);
                                                        should.exist(unitRes);
                                                        var unit = unitRes.body;
                                                        unit.name.should.equal(businessUnit.name);
                                                        var unitEmployees = unit.employees;
                                                        unitEmployees.length.should.equal(4);
                                                        unitEmployees[0].user.username.should.equal(user.username);
                                                        unitEmployees[1].user.username.should.equal(newUser.user.username);
                                                        unitEmployees[2].user.username.should.equal(companyUser1.user.username);
                                                        unitEmployees[3].user.username.should.equal(companyUser2.user.username);
                                                        //Remove business users +ve in unit
                                                        agent.post('/auth/businessUnitMassActions')
                                                            .set('token', token)
                                                            .send({
                                                                businessUnitEmployees: [companyUser2.user._id],
                                                                isRemove: true,
                                                                isEnable: false,
                                                                isDisable: false,
                                                                businessUnit: businessUnit._id
                                                            })
                                                            .end(function (removeUserErr, removeUserRes) {
                                                                var data = removeUserRes.body;
                                                                should.exist(data.success);
                                                                data.success.should.be.instanceof(Array);
                                                                data.success.length.should.equal(1);
                                                                //Should present deleted user in company level
                                                                agent.get('/companyBusinessUsers/' + user.company + '?page=1&limit=10')
                                                                    .set('token', token)
                                                                    .end(function (buserListErr, buserListRes) {
                                                                        should.not.exist(buserListErr);
                                                                        var usersList = buserListRes.body.employees;
                                                                        should.exist(usersList);
                                                                        usersList[usersList.length - 1].user.username.should.equal(companyUser2.user.username);
                                                                        //Check deleted users in business unit
                                                                        agent.get('/branches/' + businessUnit._id)
                                                                            .set('token', token)
                                                                            .expect(200)
                                                                            .end(function (unitErr, unitRes) {
                                                                                should.not.exist(unitErr);
                                                                                should.exist(unitRes);
                                                                                var unit = unitRes.body;
                                                                                unit.name.should.equal(businessUnit.name);
                                                                                var unitEmployees = unit.employees;
                                                                                unitEmployees.length.should.equal(3);
                                                                                unitEmployees[0].user.username.should.equal(user.username);
                                                                                unitEmployees[1].user.username.should.equal(newUser.user.username);
                                                                                unitEmployees[2].user.username.should.equal(companyUser1.user.username);
                                                                                done();
                                                                            });
                                                                    });
                                                            });
                                                    });
                                            });
                                    });
                                });
                            });
                    });
            });
        });
    });

    afterEach(function (done) {
        //cleanup: delete the bu that was created.
        agent.post('/businessUnitsRemove')
            .set('token',token)
            .send(unitIdsForRemove)
            .expect(200).end(function(buErr,busRes) {
                unitIdsForRemove={businessUnits:[]};
            done();
        });
    });
    after(function (done) {
        BusinessUnit.remove().exec();
        Contact.remove().exec();
        User.remove().exec();
        Company.remove().exec();
        done();
    });

});
