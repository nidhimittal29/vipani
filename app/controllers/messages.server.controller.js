'use strict';

/**
 * Module dependencies.
 */
var mongoose = require('mongoose'),
    errorHandler = require('./errors.server.controller'),
    usersJWTUtil = require('./utils/users.jwtutil'),
    offerUtil = require('./utils/common.offer.util'),
    notificationUtil = require('./utils/notification.util'),
    logger = require('../../lib/log').getLogger('MESSAGES', 'DEBUG'),
    Message = mongoose.model('Message'),
    Contact = mongoose.model('Contact'),
    Order = mongoose.model('Order'),
    Offer = mongoose.model('Offer'),
    Notification = mongoose.model('Notification'),
    async = require('async'),
    _ = require('lodash');

/**
 * Create a Message
 */
exports.create = function (req, res) {
    var message = new Message(req.body);
    var token = req.body.token || req.headers.token;
    usersJWTUtil.findUserByToken(token, function (err, user) {
        if (err) {
            return res.status(400).send({
                message: errorHandler.getErrorMessage(err)
            });
        }
        message.user = user;

        message.save(function (err) {
            if (err) {
                return res.status(400).send({
                    message: errorHandler.getErrorMessage(err)
                });
            } else {
                if(message.order){
                    //Order.findById(message.order).populate('buyer.contact seller.contact mediator.contact buyer.nVipaniUser seller.nVipaniUser mediator.nVipaniUser', 'firstName lastName middleName').exec(function (err, order) {
                    Order.findById(message.order).exec(function (err, order) {
                        if (err) {
                            return res.status(400).send({
                                message: errorHandler.getErrorMessage(err)
                            });
                        } else {
                            if (order.seller && order.seller.nVipaniUser && order.seller.nVipaniUser.toString() === user._id.toString()) {
                                var notification = new Notification();
                                notification.user = user;
                                notification.target.contact = order.buyer.contact;
                                notification.target.nVipaniUser = order.buyer.nVipaniUser;
                                notification.source.order = order;
                                notification.source.message = message;
                                notification.type = 'OrderComment';

                                notification.channel = ['Email', 'SMS', 'Push'];

                                notification.emailMessage = user.displayName + ' has posted the comment for the order number  ' + order.orderNumber + '. ' + message.body;
                                notification.shortMessage = user.displayName + ' has posted the comment for the order number  ' + order.orderNumber + '. From nVipani Team';

                                notification.title = user.displayName + ' has posted the comment for the order number  ' + order.orderNumber;
                                notification.save(function (notErr) {
                                    //Error
                                    if (notErr) {
                                        logger.error(errorHandler.getErrorMessage(notErr));
                                        logger.error('Error while creating the notification with details - ' + JSON.stringify(notification));
                                    } else if (order.mediator && order.mediator.contact) {
                                        notificationUtil.processNotification(res, notification, function (processNotificationError) {
                                            if (processNotificationError) {
                                                logger.error('Error while processing notification for comment posted for the order number ' + order.orderNumber, processNotificationError);
                                                return res.status(400).send({
                                                    status: false,
                                                    message: errorHandler.getErrorMessage(processNotificationError)
                                                });
                                            } else {
                                                var mediatorNotification = new Notification();
                                                mediatorNotification.user = user;
                                                mediatorNotification.target.contact = order.mediator.contact;
                                                mediatorNotification.target.nVipaniUser = order.mediator.nVipaniUser;
                                                mediatorNotification.source.order = order;
                                                mediatorNotification.source.message = message;
                                                mediatorNotification.type = 'OrderComment';

                                                mediatorNotification.channel = ['Email', 'SMS', 'Push'];

                                                mediatorNotification.emailMessage = user.displayName + ' has posted the comment for the order number  ' + order.orderNumber + '. ' + message.body;
                                                mediatorNotification.shortMessage = user.displayName + ' has posted the comment for the order number  ' + order.orderNumber + '. From nVipani Team';

                                                mediatorNotification.title = user.displayName + ' has posted the comment for the order number  ' + order.orderNumber;
                                                mediatorNotification.save(function (mediatorNotErr) {
                                                    //Error
                                                    if (mediatorNotErr) {
                                                        logger.error(errorHandler.getErrorMessage(mediatorNotErr));
                                                        logger.error('Error while creating the notification with details - ' + JSON.stringify(mediatorNotification));
                                                    } else {
                                                        notificationUtil.processNotification(res, mediatorNotification, function (processMediatorNotificationError) {
                                                            if (processMediatorNotificationError) {
                                                                logger.error('Error while processing notification for comment posted for the order number ' + order.orderNumber, processMediatorNotificationError);
                                                                return res.status(400).send({
                                                                    status: false,
                                                                    message: errorHandler.getErrorMessage(processMediatorNotificationError)
                                                                });
                                                            } else {
                                                                res.jsonp(message);
                                                            }
                                                        });
                                                    }
                                                });
                                            }
                                        });
                                    } else {
                                        notificationUtil.processNotification(res, notification, function (processNotificationError) {
                                            if (processNotificationError) {
                                                logger.error('Error while processing notification for comment posted for the order number ' + order.orderNumber, processNotificationError);
                                                return res.status(400).send({
                                                    status: false,
                                                    message: errorHandler.getErrorMessage(processNotificationError)
                                                });
                                            } else {
                                                res.jsonp(message);
                                            }
                                        });
                                    }
                                });
                            } else if (order.buyer && order.buyer.nVipaniUser && order.buyer.nVipaniUser.toString() === user._id.toString()) {
                                var sellNotification = new Notification();
                                sellNotification.user = user;
                                sellNotification.target.contact = order.seller.contact;
                                sellNotification.target.nVipaniUser = order.seller.nVipaniUser;
                                sellNotification.source.order = order;
                                sellNotification.source.message = message;
                                sellNotification.type = 'OrderComment';

                                sellNotification.channel = ['Email', 'SMS', 'Push'];

                                sellNotification.emailMessage = user.displayName + ' has posted the comment for the order number  ' + order.orderNumber + '. ' + message.body;
                                sellNotification.shortMessage = user.displayName + ' has posted the comment for the order number  ' + order.orderNumber + '. From nVipani Team';

                                sellNotification.title = user.displayName + ' has posted the comment for the order number  ' + order.orderNumber;
                                sellNotification.save(function (notErr) {
                                    //Error
                                    if (notErr) {
                                        logger.error(errorHandler.getErrorMessage(notErr));
                                        logger.error('Error while creating the notification with details - ' + JSON.stringify(sellNotification));
                                    } else if (order.mediator && order.mediator.contact) {
                                        notificationUtil.processNotification(res, sellNotification, function (processNotificationError) {
                                            if (processNotificationError) {
                                                logger.error('Error while processing notification for comment posted for the order number ' + order.orderNumber, processNotificationError);
                                                return res.status(400).send({
                                                    status: false,
                                                    message: errorHandler.getErrorMessage(processNotificationError)
                                                });
                                            } else {
                                                var mediatorNotification = new Notification();
                                                mediatorNotification.user = user;
                                                mediatorNotification.target.contact = order.mediator.contact;
                                                mediatorNotification.target.nVipaniUser = order.mediator.nVipaniUser;
                                                mediatorNotification.source.order = order;
                                                mediatorNotification.source.message = message;
                                                mediatorNotification.type = 'OrderComment';

                                                mediatorNotification.channel = ['Email', 'SMS', 'Push'];

                                                mediatorNotification.emailMessage = user.displayName + ' has posted the comment for the order number  ' + order.orderNumber + '. ' + message.body;
                                                mediatorNotification.shortMessage = user.displayName + ' has posted the comment for the order number  ' + order.orderNumber + '. From nVipani Team';

                                                mediatorNotification.title = user.displayName + ' has posted the comment for the order number  ' + order.orderNumber;
                                                mediatorNotification.save(function (mediatorNotErr) {
                                                    //Error
                                                    if (mediatorNotErr) {
                                                        logger.error(errorHandler.getErrorMessage(mediatorNotErr));
                                                        logger.error('Error while creating the notification with details - ' + JSON.stringify(mediatorNotification));
                                                    } else {
                                                        notificationUtil.processNotification(res, mediatorNotification, function (processMediatorNotificationError) {
                                                            if (processMediatorNotificationError) {
                                                                logger.error('Error while processing notification for comment posted for the order number ' + order.orderNumber, processMediatorNotificationError);
                                                                return res.status(400).send({
                                                                    status: false,
                                                                    message: errorHandler.getErrorMessage(processMediatorNotificationError)
                                                                });
                                                            } else {
                                                                res.jsonp(message);
                                                            }
                                                        });
                                                    }
                                                });
                                            }
                                        });
                                    } else {
                                        notificationUtil.processNotification(res, sellNotification, function (processNotificationError) {
                                            if (processNotificationError) {
                                                logger.error('Error while processing notification for comment posted for the order number ' + order.orderNumber, processNotificationError);
                                                return res.status(400).send({
                                                    status: false,
                                                    message: errorHandler.getErrorMessage(processNotificationError)
                                                });
                                            } else {
                                                res.jsonp(message);
                                            }
                                        });
                                    }
                                });
                            } else if (order.mediator && order.mediator.nVipaniUser && order.mediator.nVipaniUser.toString() === user._id.toString()) {
                                var sellerNotification = new Notification();
                                sellerNotification.user = user;
                                sellerNotification.target.contact = order.seller.contact;
                                sellerNotification.target.nVipaniUser = order.seller.nVipaniUser;
                                sellerNotification.source.order = order;
                                sellerNotification.source.message = message;
                                sellerNotification.type = 'OrderComment';

                                sellerNotification.channel = ['Email', 'SMS', 'Push'];

                                sellerNotification.emailMessage = user.displayName + ' has posted the comment for the order number  ' + order.orderNumber + '. ' + message.body;
                                sellerNotification.shortMessage = user.displayName + ' has posted the comment for the order number  ' + order.orderNumber + '. From nVipani Team';

                                sellerNotification.title = user.displayName + ' has posted the comment for the order number  ' + order.orderNumber;
                                sellerNotification.save(function (notErr) {
                                    //Error
                                    if (notErr) {
                                        logger.error(errorHandler.getErrorMessage(notErr));
                                        logger.error('Error while creating the notification with details - ' + JSON.stringify(sellerNotification));
                                    } else if (order.buyer && order.buyer.contact) {
                                        notificationUtil.processNotification(res, sellNotification, function (processNotificationError) {
                                            if (processNotificationError) {
                                                logger.error('Error while processing notification for comment posted for the order number ' + order.orderNumber, processNotificationError);
                                                return res.status(400).send({
                                                    status: false,
                                                    message: errorHandler.getErrorMessage(processNotificationError)
                                                });
                                            } else {
                                                var mediatorNotification = new Notification();
                                                mediatorNotification.user = user;
                                                mediatorNotification.target.contact = order.buyer.contact;
                                                mediatorNotification.target.nVipaniUser = order.buyer.nVipaniUser;
                                                mediatorNotification.source.order = order;
                                                mediatorNotification.source.message = message;
                                                mediatorNotification.type = 'OrderComment';

                                                mediatorNotification.channel = ['Email', 'SMS', 'Push'];

                                                mediatorNotification.emailMessage = user.displayName + ' has posted the comment for the order number  ' + order.orderNumber + '. ' + message.body;
                                                mediatorNotification.shortMessage = user.displayName + ' has posted the comment for the order number  ' + order.orderNumber + '. From nVipani Team';

                                                mediatorNotification.title = user.displayName + ' has posted the comment for the order number  ' + order.orderNumber;

                                                mediatorNotification.save(function (mediatorNotErr) {
                                                    //Error
                                                    if (mediatorNotErr) {
                                                        logger.error(errorHandler.getErrorMessage(mediatorNotErr));
                                                        logger.error('Error while creating the notification with details - ' + JSON.stringify(mediatorNotification));
                                                    } else {
                                                        notificationUtil.processNotification(res, mediatorNotification, function (processMediatorNotificationError) {
                                                            if (processMediatorNotificationError) {
                                                                logger.error('Error while processing notification for comment posted for the order number ' + order.orderNumber, processMediatorNotificationError);
                                                                return res.status(400).send({
                                                                    status: false,
                                                                    message: errorHandler.getErrorMessage(processMediatorNotificationError)
                                                                });
                                                            } else {
                                                                res.jsonp(message);
                                                            }
                                                        });
                                                    }
                                                });
                                            }
                                        });
                                    } else {
                                        notificationUtil.processNotification(res, sellNotification, function (processNotificationError) {
                                            if (processNotificationError) {
                                                logger.error('Error while processing notification for comment posted for the order number ' + order.orderNumber, processNotificationError);
                                                return res.status(400).send({
                                                    status: false,
                                                    message: errorHandler.getErrorMessage(processNotificationError)
                                                });
                                            } else {
                                                res.jsonp(message);
                                            }
                                        });
                                    }
                                });
                            }
                        }
                    });
                } else if (message.offer) {
                    offerUtil.fetchOfferById(message.offer,-1,function (errResult,offer) {
                        if (errResult) {
                            return res.status(400).send({
                                message: errorHandler.getErrorMessage(errResult)
                            });
                        } else if (offer.notificationsContacts && offer.notificationsContacts.length > 0) {
                            // If the comment is created by some notification contact then offer created user should get notification for this comment

                            if (offer.user.toString() !== user._id.toString()) {
                                Contact.findOne({
                                    user: offer.user,
                                    nVipaniUser: offer.user
                                }, function (contactErr, contact) {
                                    if (contact) {
                                        var notification = new Notification();
                                        notification.user = offer.user;
                                        notification.target.contact = contact.contact;
                                        notification.target.nVipaniUser = offer.user;
                                        notification.source.offer = offer;
                                        notification.source.message = message;
                                        notification.type = 'OfferComment';

                                        notification.channel = ['Email', 'SMS', 'Push'];

                                        notification.emailMessage = user.displayName + ' has posted the comment for the offer number  ' + offer.offerNumber + '. ' + message.body;
                                        notification.shortMessage = user.displayName + ' has posted the comment for the offer number  ' + offer.offerNumber + '. From nVipani Team';

                                        notification.title = user.displayName + ' has posted the comment for the offer number  ' + offer.offerNumber;

                                        notification.save(function (notErr) {
                                            if (notErr) {
                                                logger.error(errorHandler.getErrorMessage(notErr));
                                                logger.error('Error while creating the notification with details - ' + JSON.stringify(notification));
                                            } else {
                                                notificationUtil.processNotification(res, notification, function (processNotificationError) {
                                                    if (processNotificationError) {
                                                        logger.error('Error while processing notification for comment posted for the offer number  ' + offer.offerNumber, processNotificationError);
                                                        return res.status(400).send({
                                                            status: false,
                                                            message: errorHandler.getErrorMessage(processNotificationError)
                                                        });
                                                    } else {
                                                        async.each(offer.notificationsContacts, function (notContact, callback) {
                                                            if ((!notContact.nVipaniUser) || (notContact.nVipaniUser && notContact.nVipaniUser.toString() !== user._id.toString())) {
                                                                var notification = new Notification();
                                                                notification.user = user;
                                                                notification.target.contact = notContact.contact;
                                                                notification.target.nVipaniUser = notContact.nVipaniUser;
                                                                notification.source.offer = offer;
                                                                notification.source.message = message;
                                                                notification.type = 'OfferComment';

                                                                notification.channel = ['Email', 'SMS', 'Push'];

                                                                notification.emailMessage = user.displayName + ' has posted the comment for the offer number  ' + offer.offerNumber + '. ' + message.body;
                                                                notification.shortMessage = user.displayName + ' has posted the comment for the offer number  ' + offer.offerNumber + '. From nVipani Team';

                                                                notification.title = user.displayName + ' has posted the comment for the offer number  ' + offer.offerNumber;

                                                                notification.save(function (notErr) {
                                                                    if (notErr) {
                                                                        logger.error(errorHandler.getErrorMessage(notErr));
                                                                        logger.error('Error while creating the notification with details - ' + JSON.stringify(notification));
                                                                        callback(notErr);
                                                                    } else {
                                                                        notificationUtil.processNotification(res, notification, function (processNotificationError) {
                                                                            if (processNotificationError) {
                                                                                logger.error('Error while processing notification for comment posted for the offer number  ' + offer.offerNumber, processNotificationError);
                                                                                callback(processNotificationError);
                                                                            } else {
                                                                                callback();
                                                                            }
                                                                        });
                                                                    }
                                                                });
                                                            } else {
                                                                callback();
                                                            }
                                                        }, function (loopErr) {
                                                            if (loopErr) {
                                                                return res.status(400).send({
                                                                    message: errorHandler.getErrorMessage(loopErr)
                                                                });
                                                            } else {
                                                                res.jsonp(message);
                                                            }
                                                        });
                                                    }
                                                });
                                            }
                                        });
                                    } else {
                                        res.jsonp(message);
                                    }
                                });
                            } else {
                                async.each(offer.notificationsContacts, function (notContact, callback) {
                                    if ((!notContact.nVipaniUser) || (notContact.nVipaniUser && notContact.nVipaniUser.toString() !== user._id.toString())) {
                                        var notification = new Notification();
                                        notification.user = user;
                                        notification.target.contact = notContact.contact;
                                        notification.target.nVipaniUser = notContact.nVipaniUser;
                                        notification.source.offer = offer;
                                        notification.source.message = message;
                                        notification.type = 'OfferComment';

                                        notification.channel = ['Email', 'SMS', 'Push'];

                                        notification.emailMessage = user.displayName + ' has posted the comment for the offer number  ' + offer.offerNumber + '. ' + message.body;
                                        notification.shortMessage = user.displayName + ' has posted the comment for the offer number  ' + offer.offerNumber + '. From nVipani Team';

                                        notification.title = user.displayName + ' has posted the comment for the offer number  ' + offer.offerNumber;

                                        notification.save(function (notErr) {
                                            if (notErr) {
                                                logger.error(errorHandler.getErrorMessage(notErr));
                                                logger.error('Error while creating the notification with details - ' + JSON.stringify(notification));
                                            }else{
                                                notificationUtil.processNotification(res, notification, function (processNotificationError) {
                                                    if (processNotificationError) {
                                                        logger.error('Error while processing notification for comment posted for the offer number  ' + offer.offerNumber, processNotificationError);
                                                        callback(processNotificationError);
                                                    } else {
                                                        callback(notErr);
                                                    }
                                                });
                                            }
                                        });
                                    } else {
                                        callback();
                                    }
                                }, function (loopErr) {
                                    if (loopErr) {
                                        return res.status(400).send({
                                            message: errorHandler.getErrorMessage(loopErr)
                                        });
                                    } else {
                                        res.jsonp(message);
                                    }
                                });
                            }
                        } else {
                            res.jsonp(message);
                        }
                    });
                } else {
                    res.jsonp(message);
                }
            }
        });
    });
};

/**
 * Show the current Message
 */
exports.read = function (req, res) {
    res.jsonp(req.message);
};

/**
 * Update a Message
 */
exports.update = function (req, res) {
    var message = req.message;

    message = _.extend(message, req.body);

    message.save(function (err) {
        if (err) {
            return res.status(400).send({
                message: errorHandler.getErrorMessage(err)
            });
        } else {
            res.jsonp(message);
        }
    });
};

/**
 * Delete an Message
 */
exports.delete = function (req, res) {
    var message = req.message;

    message.remove(function (err) {
        if (err) {
            return res.status(400).send({
                message: errorHandler.getErrorMessage(err)
            });
        } else {
            res.jsonp(message);
        }
    });
};

/**
 * List of Messages
 */
exports.list = function (req, res) {
    var token = req.body.token || req.headers.token;
    var lastSyncTime = req.headers.lastsynctime;
    usersJWTUtil.getUserByToken(token, function(err, loginuser) {
        if (!err) {
            var query;
            if(lastSyncTime){
                query = Message.find({'target.nVipaniUser': loginuser.id, lastUpdated: {$gt: lastSyncTime}});
            }else{

                query=Message.find({user: loginuser.id});
            }
            query.sort('-created').populate('user', 'displayName').exec(function (err, messages) {
                if (err) {
                    return res.status(400).send({
                        message: errorHandler.getErrorMessage(err)
                    });
                } else {
                    res.jsonp(messages);
                }
            });
        }
    });
};

/**
 * List of Messages
 */
exports.listOrderMessages = function (req, res) {
    var orderId = req.params.orderId;
    var token = req.body.token || req.headers.token;
    usersJWTUtil.getUserByToken(token, function (err, loginuser) {
        Message.find({order: orderId}).sort('-created').populate('user', 'displayName profileImageURL').exec(function (err, messages) {
            if (err) {
                return res.status(400).send({
                    message: errorHandler.getErrorMessage(err)
                });
            } else {
                res.jsonp(messages);
            }
        });
    });

};



exports.listOfferMessages = function (req, res) {
    var offerId = req.params.offerId;
    var token = req.body.token || req.headers.token;
    usersJWTUtil.getUserByToken(token, function (err, loginuser) {
        Message.find({offer: offerId}).sort('-created').populate('user', 'displayName profileImageURL').exec(function (err, messages) {
            if (err) {
                return res.status(400).send({
                    message: errorHandler.getErrorMessage(err)
                });
            } else {
                res.jsonp(messages);
            }
        });
    });

};
/**
 * Message middleware
 */
exports.messageByID = function (req, res, next, id) {
    Message.findById(id).populate('user', 'displayName').exec(function (err, message) {
        if (err) return next(err);
        if (!message) return next(new Error('Failed to load Message ' + id));
        req.message = message;
        next();
    });
};

/**
 * Message authorization middleware
 */
exports.hasAuthorization = function (req, res, next) {
    var token = req.body.token || req.headers.token;
    usersJWTUtil.findUserByToken(token, function (err, user) {
        if (err) {
            return res.status(400).send({
                message: errorHandler.getErrorMessage(err)
            });
        } else {
            if (req.message.user && req.message.user.id !== user.id) {
                return res.status(403).send('User is not authorized');
            } else {
                next();
            }
        }
    });

};
