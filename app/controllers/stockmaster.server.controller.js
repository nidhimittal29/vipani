'use strict';

/**
 * Module dependencies.
 */
var mongoose = require('mongoose'),
    errorHandler = require('./errors.server.controller'),
    StockMaster = mongoose.model('StockMaster'),
    Inventory = mongoose.model('Inventory'),
    logger = require('../../lib/log').getLogger('STOCKMASTER', 'DEBUG'),
    inventoriesUtil = require('./utils/common.inventories.util'),
    businessUnitUtil = require('./utils/common.businessunit.util'),
    globalUtil = require('./utils/common.global.util'),
    async = require('async'),
    usersJWTUtil = require('./utils/users.jwtutil'),
    _ = require('lodash');

/**
 * Create a Stock Master
 */
exports.create = function (req, res) {
    logger.debug('Creating stock data -' + JSON.stringify(req.body));
    var stockMaster = new StockMaster(req.body);
    var token = req.body.token || req.headers.token;
    var businessUnit=req.body.businessUnit;
    usersJWTUtil.findUserByToken(token, function (err, user) {
        if (err) {
            return res.status(400).send({
                message: errorHandler.getErrorMessage(err)
            });
        }
        if(businessUnit) {
            businessUnitUtil.findOneBusinessUnit([{_id:businessUnit}],0,function (businessUnitErr,businessUnit) {
            logger.debug('Prepare stock -' + JSON.stringify(stockMaster));
            stockMaster.user = user;
            stockMaster.stockOwner = globalUtil.prepareOwnerByBusinessUnit(businessUnit);

            stockMaster.currentOwner=globalUtil.prepareOwnerByBusinessUnit(businessUnit);
            stockMaster.stockIn.other = {
                count: stockMaster.currentBalance,
                comment: 'Created default stock Master as part of Inventory',
                updatedDate: new Date(),
                updatedUser: user._id
            };
            stockMaster.save(function (err) {
                if (err) {
                    return res.status(400).send({
                        message: errorHandler.getErrorMessage(err)
                    });
                } else {
                    /* res.jsonp(stockMaster);*/
                    logger.debug('Insert stock data in item master -' + JSON.stringify(stockMaster.inventory));
                    res.jsonp(stockMaster);

                }
            });
            });
        }else{
            return res.status(400).send({
                message: 'Business Unit is required to create Stock Master'
            });
        }


    });
};
function updateUser (user,docs,done) {

    async.each(docs, function (doc, docCallBack) {
        doc.user = user._id;

        var date = Date.now();
        doc.created= date;
        doc.lastUpdated= date;
        docCallBack();

    }, function (docErr) {
        if (!docErr) done(null,docs);
        else done(docErr);
    });

}
function getStocksGroup (inventory,docs) {
    if(!inventory.stockMasters || inventory.stockMasters===null){
        inventory.stockMasters=[];
    }
    async.each(docs, function (doc, docCallBack) {
        inventory.stockMasters.push((doc._id));
        docCallBack();

    });
    return inventory;

}
/*
 Create Mass Insert
 */
exports.createMassInsert = function (req, res) {
    var token = req.body.token || req.headers.token;
    var inventory=req.body.inventory;

    usersJWTUtil.findUserByToken(token, function (err, user) {
        if (err) {
            return res.status(400).send({
                message: errorHandler.getErrorMessage(err)
            });
        }
        updateUser(user, req.body.doc, function (error, docs) {
            StockMaster.collection.insert(docs, function (errDoc, listDocs) {
                if (errDoc) {
                    return res.status(400).send({
                        message: errorHandler.getErrorMessage(errDoc)
                    });
                } else {
                    inventoriesUtil.findInventoryById(inventory,-1,function (populateInventory) {
                        if (populateInventory instanceof Error) {
                            return res.status(400).send({
                                message: errorHandler.getErrorMessage(populateInventory)
                            });
                        }else {
                            res.jsonp(inventory);
                        }
                    });
                    /* });*/
                }
            });
        });
    });
};
/**
 * Show the current Stock Master
 */
exports.read = function (req, res) {
    res.jsonp(req.productBrand);
};

/**
 * Update a Stock Master
 */
exports.update = function (req, res) {
    var stockMaster = req.stockMaster;
    var versionKey = stockMaster.stockMasterVersionKey;
    var oldNumberOfUnits=req.stockMaster.currentBalance;
    var oldPriceValue=req.stockMaster.MRP;
    stockMaster = _.extend(stockMaster, req.body);
    stockMaster.stockMasterVersionKey = versionKey;
    stockMaster.set('lastUpdated', Date.now());
    if (stockMaster.inventory) {
        stockMaster.save(function (err) {
            if (err) {
                return res.status(400).send({
                    message: errorHandler.getErrorMessage(err)
                });
            } else {
                if(stockMaster.currentBalance===oldNumberOfUnits){
                    res.jsonp(stockMaster);
                }else {
                    /* we need to add testcase for the following code.
                     we need to update the stock master change in the inventory after updating the number of unit.
                     */
                    var updateValue;
                    if(stockMaster.currentBalance<oldNumberOfUnits){
                        updateValue=-(oldNumberOfUnits-stockMaster.currentBalance);
                    }else{
                        updateValue=stockMaster.currentBalance-oldNumberOfUnits;
                    }
                    Inventory.update({_id: stockMaster.inventory}, {$inc: {numberOfUnits: updateValue}}).exec(function (inventoryStockUpdateErr, inventoryStockUpdate) {
                        if (inventoryStockUpdateErr) {
                            return res.status(400).send({
                                message: errorHandler.getErrorMessage(inventoryStockUpdateErr)
                            });
                        } else if (!inventoryStockUpdate) {
                            return res.status(400).send({
                                message: 'No proper stock update in the inventory'
                            });
                        } else {
                            res.jsonp(stockMaster);
                        }
                    });
                }
            }
        });
    }else return res.status(400).send({
        message: 'No able to find the inventory with stock id' +stockMaster._id
    });
};

/**
 * Delete an Stock Master
 */
exports.delete = function (req, res) {
    var stockMaster = req.stockMaster;

    stockMaster.remove(function (err) {
        if (err) {
            return res.status(400).send({
                message: errorHandler.getErrorMessage(err)
            });
        } else {
            res.jsonp(stockMaster);
        }
    });
};


/**
 * List of Stock Master Table
 */
exports.list = function (req, res) {
    StockMaster.find().sort('-created').populate('user', 'displayName').populate('inventory').exec(function (err, categories) {
        if (err) {
            return res.status(400).send({
                message: errorHandler.getErrorMessage(err)
            });
        } else {

            res.jsonp(categories);
        }
    });
};


/**
 * Stock Master By Id middleware
 */
exports.stockMasterById = function (req, res, next, id) {
    StockMaster.findById(id).populate('inventory').exec(function (err, stockMaster) {
        if (err) return next(err);
        if (!stockMaster) return next(new Error('Failed to load Stock Master ' + id));
        req.stockMaster = stockMaster;
        next();
    });
};

/**
 * product Brand authorization middleware
 */
exports.hasAuthorization = function (req, res, next) {
    var token = req.body.token || req.headers.token;
    usersJWTUtil.findUserByToken(token, function (err, user) {
        if (err) {
            return res.status(400).send({
                message: errorHandler.getErrorMessage(err)
            });
        } else {
            if (req.stockMaster.user && req.stockMaster.user._id.toString() !== user._id) {
                return res.status(403).send('User is not authorized');
            } else {
                next();
            }
        }
    });

};
