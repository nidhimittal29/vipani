'use strict';

/**
 * Module dependencies.
 */
var mongoose = require('mongoose'),
    errorHandler = require('./errors.server.controller'),
    usersJWTUtil = require('./utils/users.jwtutil'),
    dbUtil = require('./utils/common.db.util'),
    logger = require('../../lib/log').getLogger('PAYMENTTERMS', 'DEBUG'),
    User = mongoose.model('User'),
    PaymentTerm = mongoose.model('PaymentTerm'),
    companyJWTUtil = require('./utils/common.company.util'),
    async = require('async'),
    fs = require('fs'),
    _ = require('lodash');

/**
 * Create a PaymentTerm
 */

exports.create = function (req, res) {
    var token = req.body.token || req.headers.token;
    var paymentTerm = new PaymentTerm(req.body);

    logger.debug('Creating PaymentTerm -' + JSON.stringify(paymentTerm));

    var date = Date.now();
    paymentTerm.set('created', date);
    paymentTerm.set('lastUpdated', date);

    usersJWTUtil.findUserByToken(token, function (err, user) {
        if (err) {
            return res.status(400).send({
                message: errorHandler.getErrorMessage(err)
            });
        }else {
            paymentTerm.user = user;
            paymentTerm.lastUpdatedUser = user;
            paymentTerm.company = user.company;
            paymentTerm.save(function (savePaymentTermErr) {
                if (savePaymentTermErr) {
                    logger.error('Error while creating PaymentTerm.', savePaymentTermErr);
                    return res.status(400).send({
                        message: errorHandler.getErrorMessage(savePaymentTermErr)
                    });
                } else {
                    companyJWTUtil.findByCompanyId(logger,paymentTerm.company,null,0,function (companyErr,companyRes) {
                       if(companyErr){
                           logger.error('Error while update PaymentTerm in the company.', companyErr);
                           return res.status(400).send({
                               message: errorHandler.getErrorMessage(companyErr)
                           });
                       } else if(!companyRes){
                           logger.error('Error while update PaymentTerm in the company because company object is null');
                           return res.status(400).send({
                               message: 'Company object is null'
                           });
                       }else {
                           var company=companyRes;
                           company.settings.paymentTerms.push({paymentTerm:paymentTerm._id});
                           company.save(function (saveCompanyErr) {
                               if(saveCompanyErr){
                                   logger.error('Error while update PaymentTerm in the company', savePaymentTermErr);
                                   return res.status(400).send({
                                       message: errorHandler.getErrorMessage(savePaymentTermErr)
                                   });
                               }else {
                                   res.jsonp(paymentTerm);
                               }
                           });
                       }
                    });
                }
            });
        }
    });
};

/**
 * Show the current PaymentTerm
 */
exports.read = function (req, res) {
    res.jsonp(req.paymentTerm);
};

/**
 * Update a PaymentTerm
 */
exports.update = function (req, res) {
    var paymentTerm = req.paymentTerm;
    var token = req.body.token || req.headers.token;

    var versionKey = paymentTerm.paymentTermVersionKey;

    paymentTerm = _.extend(paymentTerm, req.body);

    paymentTerm.paymentTermVersionKey = versionKey;

    paymentTerm.set('lastUpdated', Date.now());

    usersJWTUtil.findUserByToken(token, function (err, user) {
        if (err) {
            //logger.debug('err 1 -'+err);
            //logger.debug('err 1 -'+JSON.stringify(err));
            return res.status(400).send({
                message: errorHandler.getErrorMessage(err)
            });
        }
        paymentTerm.lastUpdatedUser = user;

        paymentTerm.save(function (savePaymentTermErr) {
            if (savePaymentTermErr) {
                logger.error('Error while creating Contact.', savePaymentTermErr);
                return res.status(400).send({
                    message: errorHandler.getErrorMessage(savePaymentTermErr)
                });
            } else {
                res.jsonp(paymentTerm);
            }
        });
    });
};


/**
 * Delete an PaymentTerm
 */

exports.delete = function (req, res) {
    var paymentTerm = req.paymentTerm;

    var token = req.body.token || req.headers.token;

    var versionKey = paymentTerm.paymentTermVersionKey;

    paymentTerm = _.extend(paymentTerm, req.body);

    paymentTerm.paymentTermVersionKey = versionKey;

    paymentTerm.deleted = true;
    paymentTerm.set('lastUpdated', Date.now());

    usersJWTUtil.findUserByToken(token, function (err, user) {
        if (err) {
            //logger.debug('err 1 -'+err);
            //logger.debug('err 1 -'+JSON.stringify(err));
            return res.status(400).send({
                message: errorHandler.getErrorMessage(err)
            });
        }
        paymentTerm.lastUpdatedUser = user;

        paymentTerm.save(function (savePaymentTermErr) {
            if (savePaymentTermErr) {
                logger.error('Error while creating Contact.', savePaymentTermErr);
                return res.status(400).send({
                    message: errorHandler.getErrorMessage(savePaymentTermErr)
                });
            } else {
                res.jsonp(paymentTerm);
            }
        });
    });
};

/**
 * List of PaymentTerms
 */
exports.list = function (req, res) {
    var token = req.body.token || req.headers.token;
    if (token) {
        usersJWTUtil.getUserByToken(token, function (err, loginuser) {
            if (loginuser) {
                var query;
                query = PaymentTerm.find({$and:[{deleted:false},{disabled:false},{$or: [{user: loginuser.id}, {$and: [{$or:[{user:{$exists:false}},{user:{$exists:true}}]},{user:null}]}]}]});
                query.sort('-created').populate('user', 'displayName').exec(function (err, paymentTerms) {
                    if (err) {
                        return res.status(400).send({
                            message: errorHandler.getErrorMessage(err)
                        });
                    } else {
                        res.jsonp(paymentTerms);
                    }
                });
            }
        });
    } else {
        var query;
        query = PaymentTerm.find({$and: [{deleted: false},{disabled:false},  {$and: [{$or:[{user:{$exists:false}},{user:{$exists:true}}]},{user:null}]}]});
        query.sort('-created').populate('user', 'displayName').exec(function (err, paymentTerms) {
            if (err) {
                return res.status(400).send({
                    message: errorHandler.getErrorMessage(err)
                });
            } else {
                res.jsonp(paymentTerms);
            }
        });
    }

};

/**
 * Contact middleware
 */
exports.paymentTermByID = function (req, res, next, id) {
    PaymentTerm.findById(id).populate('user', 'displayName').exec(function (err, paymentTerm) {
        if (err) return next(err);
        if (!paymentTerm) return next(new Error('Failed to load PaymentTerm ' + id));
        req.paymentTerm = paymentTerm;
        next();
    });
};

/**
 * PaymentTerm authorization middleware
 */
exports.hasAuthorization = function (req, res, next) {
    var token = req.body.token || req.headers.token;
    usersJWTUtil.getUserByToken(token, function (err, user) {
        if (err) {
            if (err) {
                return res.status(400).send({
                    message: errorHandler.getErrorMessage(err)
                });
            }
        } else {
            if ((req.paymentTerm.user !== null) && (req.paymentTerm.user.id.toString() !== user.id)) {
                return res.status(403).send('User is not authorized');
            } else {
                next();
            }
        }
    });
};

exports.getPaymentTerms=function (req,res) {
    var companyId=req.query.paymentCompanyId;
    dbUtil.getOfferPaymentTerms(companyId,null,null,[],true,function (err,paymentTerms) {
        if(err){
            return res.status(400).send({
                message: errorHandler.getErrorMessage(err)
            });
        }else{
            res.jsonp(paymentTerms);
        }

    });
};
exports.getPaymentModes=function (req,res) {
    var companyId=req.query.paymentCompanyId;
    dbUtil.getOfferPaymentModes(companyId,null,null,[],true,function (err, paymentModes) {

        if(err){
            return res.status(400).send({
                message: errorHandler.getErrorMessage(err)
            });
        }else{
            res.jsonp(paymentModes);
        }
    });

};
