'use strict';

/**
 * Module dependencies.
 */
var mongoose = require('mongoose'),
    errorHandler = require('./errors.server.controller'),
    BusinessUnit = mongoose.model('BusinessUnit'),
    Contact = mongoose.model('Contact'),
    Company = mongoose.model('Company'),
    UserGroup = mongoose.model('UserGroup'),
    User = mongoose.model('User'),
    companyUtil = require('./utils/common.company.util'),
    usersJWTUtil   = require('./utils/users.jwtutil'),
    businessUnitJWTUtils = require('./utils/common.businessunit.util'),
    logger  = require('../../lib/log').getLogger('COMPANY', 'DEBUG'),
    async = require('async'),
    _ = require('lodash');

/*function validateBusinessUnit(businessUnit,done) {
    if(businessUnit && !businessUnit.name){

    }else{

    }
}*/
/**
 * Create a Business unit
 * Should have an incharge and a company contact must be created.
 * if incharge is not specified created user becomes the incharge.
 */
exports.create = function (req, res) {
    var businessUnit = req.body;
    var token = req.body.token || req.headers.token;
    usersJWTUtil.findUserByToken(token, function (err, user) {
        if (err) {
            return res.status(400).send({
                message: errorHandler.getErrorMessage(err)
            });
        }
        businessUnit.user = user._id;
        businessUnit.updateHistory = [];
        businessUnit.updateHistory.push({modifiedOn: Date.now(), modifiedBy: user});
        if (!businessUnit.company) {
            return res.status(400).send({
                message: 'The company must be set while creating a business unit'
            });
        }
        if (!businessUnit.employees || businessUnit.employees.length === 0) {
            //no incharge specified, create a business unit incharge.
            businessUnit.employees = [];
            //for now, lets make it an admin user.
            var userGroup;
            if (user.companies.length > 0) {
                var currentCompany = user.companies.filter(function (company) {
                    return company.company.toString() === businessUnit.company;
                });
                if (currentCompany && currentCompany.length === 1) {
                    userGroup = currentCompany[0].userGroup.toString();
                }
            }
            if (!userGroup) {
                return res.status(400).send({
                    message: 'An error occurred, the company user didnt have a userGroup. Please contact nVipani for support on this issue'
                });
            }
            businessUnit.employees.push({user: user._id, incharge: true, userGroup: userGroup});
        }
        var busUnit = new BusinessUnit(businessUnit);
        busUnit.save(function (err) {
            if (err) {
                return res.status(400).send({
                    message: errorHandler.getErrorMessage(err)
                });
            } else {
                /* //create a company contact for the business unit
                 var incharge = businessUnit.employees.filter(function (employee) {
                     return employee.incharge === true;
                 });
                 var inchargeUser = User.findById(incharge.user, function (err, userObj) {
                     var contact = {displayName: busUnit.name, addresses: [], emails: [], phones: []};
                     contact.firstName = inchargeUser.firstName;
                     contact.lastName = inchargeUser.lastName;
                     contact.middleName = inchargeUser.middleName;
                     contact.user = user._id;
                     contact.nVipaniCompany = user.company;
                     contact.nVipaniRegContact = true;
                     contact.nVipaniUser = user;
                     contact.businessUnit = busUnit._id;
                     busUnit.addresses.forEach(function (address) {
                         contact.addresses.push({
                             addressType: address.addressType,
                             addressLine: address.addressLine,
                             city: address.city,
                             state: address.state,
                             country: address.country,
                             pinCode: address.pinCode
                         });
                     });
                     busUnit.phones.forEach(function (phone) {
                         contact.phones.push({
                             phoneNumber: phone.phoneNumber,
                             phoneType: phone.phoneType,
                             primary: phone.primary
                         });
                     });
                     busUnit.emails.forEach(function (email) {
                         contact.emails.push({
                             email: email.email,
                             emailType: email.emailType,
                             primary: email.primary
                         });
                     });
                     var contakt = new Contact(contact);
                     contakt.save(function (retContactErr) {
                         if (retContactErr) {
                             logger.error('Business unit was created but contact was not created for the business unit' + err.message);
                         }*/
                //even if contact was not created, business unit can be returned as successful.
                var query=[{_id:busUnit._id}];
                businessUnitJWTUtils.findOneBusinessUnit(query,1,function(businessErr,businessUnits){
                    if(businessErr){
                        return res.status(400).send({
                            message: errorHandler.getErrorMessage(businessErr)
                        });
                    } else {
                        res.jsonp(businessUnits);
                    }
                });
            }
        });
    });
};

/**
 * Show the current Business Unit
 */
exports.read = function (req, res) {
    res.jsonp(req.businessUnit);
};

var batchBusinessUnitUpdate=function(query,update,done){
    BusinessUnit.update(
        query,
        {$set:update},
        {multi: true},
        function(err,updateContacts){
            done(err,updateContacts);
        }
    );
};

/**
 * Update a Business unit
 */
exports.update = function (req, res) {

    var businessUnit = req.businessUnit;
     var data=req.body;

    var versionKey = businessUnit.businessUnitVersionKey;

    businessUnit = _.extend(businessUnit, data.businessUnit);

    businessUnit.businessUnitVersionKey = versionKey;
    var token = req.body.token || req.headers.token;
    usersJWTUtil.findUserByToken(token, function (err, user) {
        if (err) {
            return res.status(400).send({
                message: errorHandler.getErrorMessage(err)
            });
        }
        if(data.isUpdate) {
            businessUnit.updateHistory.push({modifiedOn: Date.now(), modifiedBy:user});
            businessUnit.save(function (saveCompanyErr) {
                if (saveCompanyErr) {
                    logger.error('Error while saving the business unit with name ' + businessUnit.name, saveCompanyErr);
                    return res.status(400).send({
                        message: errorHandler.getErrorMessage(saveCompanyErr)
                    });
                } else {

                    res.jsonp(businessUnit);
                }
            });
        }else if(data.isActive || data.isDisable){
            businessUnit.disabled=data.isActive?false:true;
            businessUnit.updateHistory.push({modifiedOn: Date.now(), modifiedBy:user});
            businessUnit.save(function (saveCompanyErr) {
                if (saveCompanyErr) {
                    logger.error('Error while saving the business unit with name ' + businessUnit.name, saveCompanyErr);
                    return res.status(400).send({
                        message: errorHandler.getErrorMessage(saveCompanyErr)
                    });
                } else {
                    Company.findById(businessUnit.company, function (err, company) {
                        if (err) {
                            logger.error('Error while getting company at create business unit' + businessUnit.company);
                            return res.status(400).send({
                                message: err.message
                            });
                        } else if (!company) {
                            logger.error('Company object is null at create business unit' + businessUnit.company);
                            return res.status(400).send({
                                message: 'Company is not found'
                            });
                        } else {
                            var companyBunit=company.businessUnits.filter(function (eachUnit) {
                                return eachUnit.businessUnit.toString()===businessUnit._id.toString();
                            });
                            if(!companyBunit){
                                logger.error('Business unit in the company is not found' + company+'business unit'+businessUnit);
                                return res.status(400).send({
                                    message: 'Business unit is not found in company'
                                });
                            }else if(companyBunit.length>1){
                                logger.error('More than one business unit is found in the company with same id' + businessUnit);
                                return res.status(400).send({
                                    message: 'More than one business unit is found in the company'
                                });
                            }else {
                                company.businessUnits[company.businessUnits.indexOf(companyBunit[0])].status = data.isActive ? 'Active' : 'Inactive';
                                if(businessUnit.employees && businessUnit.employees.length>0) {
                                    async.forEachSeries(businessUnit.employees, function (eachUnitEmployee, callback) {
                                        var companyEmployee = company.employees.filter(function (eachEmployee) {
                                            return eachEmployee.user.toString() === eachUnitEmployee.user._id.toString();
                                        });
                                        if (companyEmployee && companyEmployee.length === 1) {
                                            var companyEmployeeUnit = companyEmployee[0].businessUnits.filter(function (eachUnit) {
                                                return eachUnit.businessUnit.toString() === businessUnit._id.toString();
                                            });
                                            if (companyEmployeeUnit && companyEmployeeUnit.length === 1) {
                                                company.employees[company.employees.indexOf(companyEmployee[0])]
                                                    .businessUnits[company.employees[company.employees.indexOf(companyEmployee[0])].businessUnits.indexOf(companyEmployeeUnit[0])].status = data.isActive ? 'Active' : 'Inactive';
                                                callback();
                                            } else {
                                                logger.error('Error while update business unit status in company employees' + company);
                                                callback(new Error('Error while update business unit status in company employees'));
                                            }
                                        } else {
                                            logger.error('Error while update business unit status in company employees' + company);
                                            callback(new Error('Error while update business unit status in company employees'));
                                        }
                                    }, function (err) {
                                        if (err) {
                                            logger.error('Error while update status in the company business units' + company);
                                            return res.status(400).send({
                                                message: errorHandler.getErrorMessage(err)
                                            });
                                        } else {
                                            company.save(function (err) {
                                                if (err) {
                                                    return res.status(400).send({
                                                        message: errorHandler.getErrorMessage(err)
                                                    });
                                                } else {
                                                    BusinessUnit.populate(businessUnit, {path: 'employees.user employees.userGroup'}, function (err, populateUnit) {
                                                        if (err) {
                                                            return res.status(400).send({
                                                                message: errorHandler.getErrorMessage(err)
                                                            });
                                                        } else {
                                                            return res.jsonp(populateUnit);
                                                        }
                                                    });
                                                }
                                            });
                                        }
                                    });
                                }else {
                                    logger.error('Error while update status in the company business units' + company);
                                    return res.status(400).send({
                                        message: 'Business unit employee ids are required to update business unit status of employee '
                                    });
                                }
                            }
                        }
                    });
                }
            });
        }else if(data.isRemove){
            Company.findById(businessUnit.company.toString(), function (err, company) {
                if (err) {
                    logger.error('Error while getting company at create business unit' + businessUnit.company.toString());
                    return res.status(400).send({
                        message: err.message
                    });
                } else if (!company) {
                    logger.error('Company object is null at create business unit' + businessUnit.company.toString());
                    return res.status(400).send({
                        message: 'Company is not found'
                    });
                } else {
                    var companyBunit=company.businessUnits.filter(function (eachUnit) {
                        return eachUnit.businessUnit.toString()===businessUnit._id.toString();
                    });
                    if(!companyBunit){
                        logger.error('Business unit in the company is not found' + company+'business unit'+businessUnit);
                        return res.status(400).send({
                            message: 'Business unit is not found in company'
                        });
                    }else if(companyBunit.length>1){
                        logger.error('More than one business unit is found in the company with same id' + businessUnit);
                        return res.status(400).send({
                            message: 'More than one business unit is found in the company'
                        });
                    }else {
                        company.businessUnits.splice(company.businessUnits.indexOf(companyBunit[0]),1);
                        async.forEachSeries(businessUnit.employees, function (eachUnitEmployee, callback) {
                            var companyEmployee = company.employees.filter(function (eachEmployee) {
                                return eachEmployee.user.toString() === eachUnitEmployee.user._id.toString();
                            });
                            if (companyEmployee && companyEmployee.length === 1) {
                                var companyEmployeeUnit = companyEmployee[0].businessUnits.filter(function (eachUnit) {
                                    return eachUnit.businessUnit.toString() === businessUnit._id.toString();
                                });
                                if (companyEmployeeUnit && companyEmployeeUnit.length === 1) {
                                    company.employees[company.employees.indexOf(companyEmployee[0])]
                                        .businessUnits.splice([company.employees[company.employees.indexOf(companyEmployee[0])].businessUnits.indexOf(companyEmployeeUnit[0])],1);
                                    callback();
                                } else {
                                    logger.error('Error while update business unit status in company employees' + company);
                                    callback(new Error('Error while update business unit status in company employees'));
                                }
                            } else {
                                logger.error('Error while update business unit status in company employees' + company);
                                callback(new Error('Error while update business unit status in company employees'));
                            }
                        },function (err) {
                            company.save(function (err) {
                                if(err){
                                    return res.status(400).send({
                                        message: errorHandler.getErrorMessage(err)
                                    });
                                }else {
                                    batchBusinessUnitUpdate([businessUnit._id.toString()],{'deleted':true}, function (updateErr, results) {
                                        if(updateErr){
                                            return res.status(400).send({
                                                message: errorHandler.getErrorMessage(updateErr)
                                            });
                                        }else  if(!results) {
                                            return res.status(400).send({
                                                message: 'Not Deleted',
                                                status: false
                                            });
                                        }
                                        else{
                                            companyUtil.findBusinessUnitsByCompany(user,company._id, function (buErr, companies) {
                                                if (buErr) {
                                                    return res.status(400).send({
                                                        message: errorHandler.getErrorMessage(buErr),
                                                    });
                                                } else {
                                                    res.jsonp(companies);
                                                }
                                            });
                                        }
                                    });
                                }
                            });
                        });
                    }
                }
            });
        }else {
            return res.status(400).send({
                message: 'No operation for business unit',
                status: false
            });
        }
    });
};
var findBusinessUnits = function (user, done) {
    BusinessUnit.find({company: user.company}).populate('user', 'displayName').exec(function (err, companies) {
        if (err) {
            done(err,null);
        } else {
            done(null,companies);
        }
    });
};
/**
 * List of Business units
 */
exports.list = function (req, res) {
    var token = req.body.token || req.headers.token;
    usersJWTUtil.findUserByToken(token, function (err, user) {
        if (err) {
            return res.status(400).send({
                message: errorHandler.getErrorMessage(err)
            });
        }
        else{
            findBusinessUnits(user,function(err,companies){
                if (err) {
                    return res.status(400).send({
                        message: errorHandler.getErrorMessage(err)
                    });
                } else {
                    res.jsonp(companies);
                }
            });
        }
    });
};

exports.getBusinessUsersByBusinessId=function (req, res, next, id) {
    var token = req.body.token || req.headers.token;
    usersJWTUtil.findUserByToken(token, function (err, user) {
        if (err) {
            return res.status(400).send({
                message: errorHandler.getErrorMessage(err)
            });
        }else {
            var query=[{_id:id}];
            businessUnitJWTUtils.findOneBusinessUnit(query,2,function(businessErr,businessUnits){
                if(businessErr){
                    return res.status(400).send({
                        message: errorHandler.getErrorMessage(businessErr)
                    });
                } else {
                    res.jsonp(businessUnits);
                }
            });
        }
    });

};
/**
 * Business unit middleware
 */
exports.businessUnitByID = function (req, res, next, id) {
    var query=[{_id:id}];
    businessUnitJWTUtils.findOneBusinessUnit(query,2,function(businessErr,businessUnits){
        if(businessErr){
            return next(businessErr);
        } else {
            req.businessUnit = businessUnits;
            next();
        }
    });
};


exports.massDelete = function(req,res){
    var token = req.body.token || req.headers.token;
    var businessUnits=req.body.businessUnits;
    usersJWTUtil.findUserByToken(token, function (err, user) {
        if(err){
            return res.status(400).send({
                message: errorHandler.getErrorMessage(err)
            });
        }else {
            if(businessUnits && businessUnits.length>0){
                var query={'company':user.company};
                if(businessUnits && businessUnits.length>0){
                    query.$or=businessUnits;
                }
                var buToDelete = [];
                for(var i=0;i<businessUnits.length;i++) {
                    buToDelete.push(businessUnits[i]._id);
                }
                Company.update({_id:user.company},{$pull:{businessUnits:{businessUnit:{$in:buToDelete}}}}).exec(function(buErr,buUpdate) {
                    if (buErr) {
                        return res.status(400).send({
                            message: errorHandler.getErrorMessage(buErr)
                        });
                    }
                    else {
                        batchBusinessUnitUpdate(query,{'deleted':true}, function (updateErr, results) {
                            if(updateErr){
                                return res.status(400).send({
                                    message: errorHandler.getErrorMessage(updateErr)
                                });
                            }else  if(!results) {
                                return res.status(400).send({
                                    message: 'Not Deleted',
                                    status: false
                                });
                            }
                            else{

                                companyUtil.findBusinessUnitsByCompany(user,user.company, function (buErr, companies) {
                                    if (buErr) {
                                        return res.status(400).send({
                                            message: errorHandler.getErrorMessage(buErr)
                                        });
                                    } else {
                                        res.jsonp(companies);
                                    }

                                });
                            }
                        });
                    }

                });
            }else {
                return res.status(400).send({
                    message: 'No Selected BusinessUnits'
                });
            }
        }
    });
};


exports.massEnable = function(req,res){
    var token = req.body.token || req.headers.token;
    var businessUnits=req.body.businessUnits;
    usersJWTUtil.findUserByToken(token, function (err, user) {
        if(err){
            return res.status(400).send({
                message: errorHandler.getErrorMessage(err)
            });
        }else {
            if(businessUnits && businessUnits.length>0){
                var query={'company':user.company};
                if(businessUnits && businessUnits.length>0){
                    query.$or=businessUnits;
                }
                batchBusinessUnitUpdate(query,{'disabled':false}, function (updateErr, results) {
                    if(updateErr){
                        return res.status(400).send({
                            message: errorHandler.getErrorMessage(updateErr)
                        });
                    }else  if(!results){
                        return res.status(400).send({
                            message: 'Not Deleted',
                            status:false
                        });
                    }else {
                        companyUtil.findBusinessUnitsByCompany(user,user.company,function (buErr,companies) {
                            if(buErr){
                                return res.status(400).send({
                                    message: errorHandler.getErrorMessage(buErr)
                                });
                            }else{
                                res.jsonp(companies);
                            }

                        });
                    }

                });
            }else {
                return res.status(400).send({
                    message: 'No Selected BusinessUnits'
                });
            }
        }
    });
};

exports.massDisable=function(req,res){
    var token = req.body.token || req.headers.token;
    var businessUnits=req.body.businessUnits;
    usersJWTUtil.findUserByToken(token, function (err, user) {
        if(err){
            return res.status(400).send({
                message: errorHandler.getErrorMessage(err)
            });
        }else {
            if(businessUnits && businessUnits.length>0){
                var query={'company':user.company};
                if(businessUnits && businessUnits.length>0){
                    query.$or=businessUnits;
                }
                batchBusinessUnitUpdate(query,{'disabled':true}, function (updateErr, results) {
                    if(updateErr){
                        return res.status(400).send({
                            message: errorHandler.getErrorMessage(updateErr)
                        });
                    }else  if(!results){
                        return res.status(400).send({
                            message: 'Not Deleted',
                            status:false
                        });
                    }else {
                        companyUtil.findBusinessUnitsByCompany(user,user.company,function (buErr,companies) {
                            if(buErr){
                                return res.status(400).send({
                                    message: errorHandler.getErrorMessage(buErr)
                                });
                            }else{
                                res.jsonp(companies);
                            }

                        });
                    }

                });
            }else {
                return res.status(400).send({
                    message: 'No Selected BusinessUnits'
                });
            }
        }
    });
};

/**
 * Business unit authorization middleware
 */
exports.hasAuthorization = function (req, res, next) {
    var token = req.body.token || req.headers.token;
    usersJWTUtil.findUserByToken(token, function (err, user) {
        if (err) {
            return res.status(400).send({
                message: errorHandler.getErrorMessage(err)
            });
        } else {
            if (req.businessUnit.company && req.businessUnit.company._id && (req.businessUnit.company._id.toString() !== user.company.toString())) {
                return res.status(403).send('User is not authorized');
            } else {
                next();
            }
        }
    });

};
