'use strict';

/**
 * Module dependencies.
 */
var mongoose = require('mongoose'),
    errorHandler = require('./errors.server.controller'),
    ItemMaster = mongoose.model('ItemMaster'),
    usersJWTUtil = require('./utils/users.jwtutil'),
    _ = require('lodash');

/**
 * Create a Item Master
 */
exports.create = function (req, res) {
    var itemMaster = new ItemMaster(req.body);
    var token = req.body.token || req.headers.token;
    usersJWTUtil.findUserByToken(token, function (err, user) {
        if (err) {
            return res.status(400).send({
                message: errorHandler.getErrorMessage(err)
            });
        }
        itemMaster.user = user;
        itemMaster.save(function (err) {
                if (err) {
                    return res.status(400).send({
                        message: errorHandler.getErrorMessage(err)
                    });
                } else {
                    res.jsonp(itemMaster);
                }
            });


    });
};
/**
 * Show the current Item Master
 */
exports.read = function (req, res) {
    res.jsonp(req.itemMaster);
};

/**
 * Update a Item Master
 */
exports.update = function (req, res) {
    var itemMaster = req.itemMaster;
    var versionKey = itemMaster.itemMasterVersionKey;

    itemMaster = _.extend(itemMaster, req.body);
    itemMaster.itemMasterVersionKey = versionKey;
    itemMaster.set('lastUpdated', Date.now());

    itemMaster.save(function (err) {
        if (err) {
            return res.status(400).send({
                message: errorHandler.getErrorMessage(err)
            });
        } else {
            res.jsonp(itemMaster);
        }
    });
};

/**
 * Delete an Item Master
 */
exports.delete = function (req, res) {
    var itemMaster = req.itemMaster;

    itemMaster.remove(function (err) {
        if (err) {
            return res.status(400).send({
                message: errorHandler.getErrorMessage(err)
            });
        } else {
            res.jsonp(itemMaster);
        }
    });
};


/**
 * List of Item Master
 */
exports.list = function (req, res) {
    ItemMaster.find().sort('-created').populate('user', 'displayName').populate('productBrand unitOfMeasures').exec(function (err, categories) {
        if (err) {
            return res.status(400).send({
                message: errorHandler.getErrorMessage(err)
            });
        } else {

            res.jsonp(categories);
        }
    });
};


/**
 *Item Master middleware
 */
exports.itemById = function (req, res, next, id) {
    ItemMaster.findById(id).populate('user', 'displayName').populate('productBrand unitOfMeasures').exec(function (err, itemMaster) {
        if (err) return next(err);
        if (!itemMaster) return next(new Error('Failed to load Item Master ' + id));
        req.itemMaster = itemMaster;
        next();
    });
};

/**
 * Item Master authorization middleware
 */
exports.hasAuthorization = function (req, res, next) {
    var token = req.body.token || req.headers.token;
    usersJWTUtil.findUserByToken(token, function (err, user) {
        if (err) {
            return res.status(400).send({
                message: errorHandler.getErrorMessage(err)
            });
        } else {
            if (req.itemMaster.user && req.itemMaster.user._id.toString() !== user._id) {
                return res.status(403).send('User is not authorized');
            } else {
                next();
            }
        }
    });

};
