'use strict';

/**
 * Module dependencies.
 */
var _ = require('lodash'),
	errorHandler = require('../errors.server.controller.js'),
	mongoose = require('mongoose'),
	fs = require('fs'),
	logger  = require('../../../lib/log').getLogger('USERS', 'DEBUG'),
	passport = require('passport'),
	usersJWTUtil   = require('../utils/users.jwtutil'),
    dbUtil = require('../utils/common.db.util'),
	Notification = mongoose.model('Notification'),
    UserGroup = mongoose.model('UserGroup'),
    Company = mongoose.model('Company'),
	Category = mongoose.model('Category'),
	Order = mongoose.model('Order'),
	Todo = mongoose.model('Todo'),
	User = mongoose.model('User'),
async = require('async');
/**
 * Update user details
 */
exports.update = function(req, res) {
	// Init Variables

	var message = null;

	// For security measurement we remove the roles from the req.body object
	delete req.body.roles;
	delete req.body.password;
	delete req.body.salt;
	var token = req.body.token || req.headers.token;

	usersJWTUtil.findUserByToken(token, function(err, user) {
		if (err) {
			return res.status(400).send({
				status: false,
				message: errorHandler.getErrorMessage(err)
			});
		}

		User.findOne({
			username: user.username
		}, '-salt -password', function(err, dbuser) {
			if (dbuser) {
                var versionKey = dbuser.userVersionKey;
				dbuser = _.extend(dbuser, req.body);
				dbuser.userVersionKey=versionKey;
				dbuser.updated = Date.now();
				if(dbuser.firstName || dbuser.lastName || dbuser.middleName) {
                    dbuser.displayName = dbuser.firstName + (dbuser.middleName ? ' ' + dbuser.middleName : '') + (dbuser.lastName ? ' ' + dbuser.lastName : '');
                }

				dbuser.save(function (err) {
					if (err) {
						return res.status(400).send({
							status: false,
							message: errorHandler.getErrorMessage(err)
						});
					} else {
						res.json({
							status: true,
							token: usersJWTUtil.genToken(dbuser.username, dbuser.id),
							user: dbuser
						});
					}
				});
			} else {
				res.status(400).send({
					status: false,
					message: 'User is not signed in'
				});
			}
		});
	});
};

/**
 * Update profile picture
 */
exports.changeProfilePicture = function (req, res) {
	var token = req.body.token || req.headers.token;
	if (token) {
		logger.debug('Profile Picture [name:' + req.files.file.name + ', fieldname:' + req.files.file.fieldname + ', originalname:' + req.files.file.originalname + ']');
		usersJWTUtil.findUserByToken(token, function (err, user) {
			if (user) {
				fs.writeFile('./public/modules/users/img/profile/uploads/' + req.files.file.name, req.files.file.buffer, function (uploadError) {
					if (uploadError) {
						return res.status(400).send({
							status: false,
							message: 'Error occurred while uploading profile picture'
						});
					} else {
						User.findOne({
							username: user.username
						}, '-salt -password', function (err, dbuser) {
							if (dbuser) {
								dbuser.profileImageURL = 'modules/users/img/profile/uploads/' + req.files.file.name;

								dbuser.save(function (saveError) {
									if (saveError) {
										return res.status(400).send({
											status: false,
											message: errorHandler.getErrorMessage(saveError)
										});
									} else {
										res.json({
											status: true,
											token: usersJWTUtil.genToken(dbuser.username, dbuser.id),
											user: dbuser
										});
									}
								});
							}
						});
					}
				});
			} else {
				res.status(400).send({
					status: false,
					message: 'User is not signed in'
				});
			}
		});
	} else {
		res.status(400).send({
			status: false,
			message: 'User is not signed in'
		});
	}
};

/**
 * Send User
 */
exports.me = function(req, res) {
	/*logger.debug('req.token-'+req.token);
	logger.debug('req.body-'+JSON.stringify(req.body));
	logger.debug('req.body.token-'+req.body.token);*/
	var token = req.body.token || req.headers.token;
	if (token) {
		usersJWTUtil.findUserByToken(token, function (err, user) {
			if (err) {
				return res.status(400).send({
					status: false,
					message: errorHandler.getErrorMessage(err)
				});
			}
			res.json(user || null);


		});
	} else {
		res.status(400).send({
			status: false,
			message: 'User is not signed in'
		});
	}
};

/**
 * Send User
 */
exports.myhome = function (req, res) {
	/*logger.debug('req.token-'+req.token);
	 logger.debug('req.body-'+JSON.stringify(req.body));
	 logger.debug('req.body.token-'+req.body.token);*/
	var token = req.body.token || req.headers.token;
    var version =req.query.version;
	if (token) {
		usersJWTUtil.findUserByToken(token, function (err, user) {
			if (err) {
				return res.status(400).send({
					status: false,
					message: errorHandler.getErrorMessage(err)
				});
			}

			// Notifications
			// Todo List
			// Order Summary
			// Sales Summary
            Company.findById(user.company).exec(function (err, company) {
			Notification.find({'target.nVipaniCompany': user.company.toString(),'disabled':false,viewed:false}).sort('-created').populate('user', 'displayName').sort('-created').exec(function (err, notifications) {
				if (err) {
					return res.status(400).send({
						status: false,
						message: errorHandler.getErrorMessage(err)
					});
				} else {
					Order.find({$and: [{currentStatus: {$in: ['Placed', 'Confirmed', 'InProgress', 'Shipped', 'Delivered', 'Disputed']}}, {$or: [{'seller.nVipaniCompany': user.company.toString()}, {'buyer.nVipaniCompany': user.company.toString()}, {'mediator.nVipaniCompany': user.company.toString()}]},{'disabled':false}]}).exec(function (err, orders) {
						/*					Order.aggregate([{
						$match: {
							$and: [{currentStatus: {$ne: 'Completed'}}, {currentStatus: {$ne: 'Drafted'}},
								{$or: [{'seller.nVipaniUser': user._id}, {'buyer.nVipaniUser': user._id}, {'mediator.nVipaniUser': user._id}]}]
						}
					},
						{
							$group: {
								_id: '$currentStatus',
								orderDetails: {
									$push: {
										id: '$_id',
										orderNumber: '$orderNumber',
										created: '$created',
										totalAmount: '$totalAmount'
									}
								}
							}
						 }], function (err, orders) {*/
						if (err) {
							return res.status(400).send({
								status: false,
								message: errorHandler.getErrorMessage(err)
							});
						} else {
							var newOrders = []; // 'Placed' and Confirmed
							var disputedOrders = []; // 'Disputed'
							var deliveryPendingOrders = []; // Shipped Orders
							var paymentPendingOrders = []; // Payment OverDue orders
							var deliveredOrders = []; // In Progress and Delivered Orders
							var ordersSummary = [];
							/*async.each(orders, function (order, callback) {
								if ((order.paymentOptionType.payNow.selection && order.paymentOptionType.payNow.selection === true && order.paymentOptionType.payNow.status === 'Overdue') ||
									(order.paymentOptionType.payOnDelivery.selection && order.paymentOptionType.payOnDelivery.selection === true && order.paymentOptionType.payOnDelivery.status === 'Overdue') ||
									(order.paymentOptionType.lineOfCredit.selection && order.paymentOptionType.lineOfCredit.selection === true && order.paymentOptionType.lineOfCredit.status === 'Overdue')) {
									paymentPendingOrders.push({
										id: order._id,
										orderNumber: order.orderNumber,
										created: order.created,
										paidAmount: order.paidAmount,
										currentStatus: order.currentStatus,
										totalAmount: order.totalAmount
									});
								} else if (order.paymentOptionType.installments.selection && order.paymentOptionType.installments.selection === true) {
									for (var i = 0; i < order.paymentOptionType.installments.options.length; i++) {
										if (order.paymentOptionType.installments.options[i].status === 'Overdue') {
											paymentPendingOrders.push({
												id: order._id,
												orderNumber: order.orderNumber,
												created: order.created,
												paidAmount: order.paidAmount,
												currentStatus: order.currentStatus,
												totalAmount: order.totalAmount
											});
											break;
										}
									}
								}

								if (order.currentStatus === 'Placed' || order.currentStatus === 'Confirmed') {
									newOrders.push({
										id: order._id,
										orderNumber: order.orderNumber,
										created: order.created,
										paidAmount: order.paidAmount,
										currentStatus: order.currentStatus,
										totalAmount: order.totalAmount
									});
								} else if (order.currentStatus === 'Disputed') {
									disputedOrders.push({
										id: order._id,
										orderNumber: order.orderNumber,
										created: order.created,
										paidAmount: order.paidAmount,
										currentStatus: order.currentStatus,
										totalAmount: order.totalAmount
									});
								} else if (order.currentStatus === 'Shipped' || order.currentStatus === 'InProgress') {
									deliveryPendingOrders.push({
										id: order._id,
										orderNumber: order.orderNumber,
										created: order.created,
										paidAmount: order.paidAmount,
										currentStatus: order.currentStatus,
										totalAmount: order.totalAmount
									});
								} else if (order.currentStatus === 'Delivered') {
									deliveredOrders.push({
										id: order._id,
										orderNumber: order.orderNumber,
										created: order.created,
										paidAmount: order.paidAmount,
										currentStatus: order.currentStatus,
										totalAmount: order.totalAmount
									});
								}
								callback();
							}, function (eachErr) {
								if (eachErr) {
									logger.error('Error while aggregating the orders for orders summary.', eachErr);
									return res.status(400).send({
										status: false,
										message: errorHandler.getErrorMessage(eachErr)
									});
								} else {
									ordersSummary.push({
										summaryCategory: 'New',
										orderDetails: newOrders
									});
									ordersSummary.push({
										summaryCategory: 'Disputed',
										orderDetails: disputedOrders
									});
									ordersSummary.push({
										summaryCategory: 'DeliveryPending',
										orderDetails: deliveryPendingOrders
									});
									ordersSummary.push({
										summaryCategory: 'Delivered',
										orderDetails: deliveredOrders
									});
									ordersSummary.push({
										summaryCategory: 'PaymentPending',
										orderDetails: paymentPendingOrders
									});
								}
							});*/
                            dbUtil.findQueryByCategories([{type: 'SubCategory1'}],1,function (categories) {
								if (categories instanceof  Error) {
									return res.status(400).send({
										status: false,
										message: errorHandler.getErrorMessage(categories)
									});
								} else {
									Todo.find({
										target: user.id,
										completed: false
									}).sort('-created').populate('user', 'displayName').exec(function (err, todos) {
										if (err) {
											return res.status(400).send({
												status: false,
												message: errorHandler.getErrorMessage(err)
											});
										} else {
											res.jsonp({
												status: true,
												'notifications': notifications,
												'orders': ordersSummary,
												'offers': categories,
												'todos': todos,
												'companySegments':company.segments,
												'isforceupdate':version?false:true
											});
										}
									});
								}
							});
						}
					});
				}
			});
		});
        });
	} else {
		res.status(400).send({
			status: false,
			message: 'User is not signed in'
		});
	}
};
/**
 * List of User Groups
 */
exports.listUserGroups = function (req, res) {
    var token = req.body.token || req.headers.token;
    usersJWTUtil.findUserByToken(token, function (err, user) {
        if (err) {
            return res.status(400).send({
                message: errorHandler.getErrorMessage(err)
            });
        }
        UserGroup.find({$and:[{deleted:false},{disabled:false},{$or: [{user: user.id}, {$and: [{$or:[{user:{$exists:false}},{user:{$exists:true}}]},{user:null}]}]}]}).exec(function (err, userGroups) {
            if (err) {
                return res.status(400).send({
                    message: errorHandler.getErrorMessage(err)
                });
            } else {
                res.jsonp(userGroups);
            }
        });
    });
};

