'use strict';

/**
 * Module dependencies.
 */
var mongoose = require('mongoose'),
    errorHandler = require('./errors.server.controller'),
    usersJWTUtil = require('./utils/users.jwtutil'),
    inventoryUtil = require('./utils/common.inventories.util'),
    inventoryTrackUtil = require('./utils/common.inventorytracker.util'),
    util = require('./utils/common.util'),
    Inventory = mongoose.model('Inventory'),
    Product = mongoose.model('Product'),
    ProductBrand = mongoose.model('ProductBrand'),
    Order=mongoose.model('Order'),
    StockMaster = mongoose.model('StockMaster'),
    ItemMaster=mongoose.model('ItemMaster'),
    Company = mongoose.model('Company'),
    BusinessUnit = mongoose.model('BusinessUnit'),
    HsnCode=mongoose.model('Hsncodes'),
    async = require('async'),
    config = require('../../config/config'),
    logger = require('../../lib/log').getLogger('INVENTORY', 'DEBUG'),
    _ = require('lodash');

/**
 *
 */

function fetchInventoryById(inventoryId, done) {
    Inventory.findById(inventoryId).populate('user', 'displayName').populate('stockMasters itemMaster productCategory parent grandParent').exec(function (err, inventory){
        if (err) {
            logger.error('Error while loading the Order details with order id- ' + inventoryId);
            done(err);
        } else {
            ProductBrand.populate(inventory, {
                path: 'itemMaster.productBrand',
                model: 'ProductBrand',
                select: 'name productCategory taxGroup hsncode variety'
            }, function (nestedErr, inventoryProductBrand) {
                if (nestedErr) {
                    done(nestedErr);
                } else {
                    HsnCode.populate(inventoryProductBrand, {
                        path: 'itemMaster.productBrand.productCategory',
                        model: 'Category',
                        select: 'name description code'
                    }, function (nestedProductErr, inventoryProduct) {
                        if (nestedProductErr) {
                            done(nestedProductErr);
                        } else {
                            done(inventoryProduct);
                        }
                    });
                }
            });
        }

    });
}
function fetchInventoryTrackById(inventoryId,pathNode, done) {
    /*logger.error('Error while loading the Order details with order id- ' + pathCounter);*/
    Inventory.findById(inventoryId,'-offerUnits -saleUnitPrice -buyUnitPrice -updateHistory -numberOfUnitsHistory -offerUnitsHistory -buyOfferUnitsHistory -moqAndPrice -croppedInventoryImageURL2 -croppedInventoryImageURL3 -croppedInventoryImageURL4 -inventoryImageURL2 -inventoryImageURL3 -inventoryImageURL4').populate('product','-productImageURL2 -productImageURL3 -productImageURL4 -croppedProductImageURL2 -croppedProductImageURL3 -croppedProductImageURL4 -gradeDefinition -qualityDefinition -updateHistory').populate('source.orders.order','seller').populate('user','company').exec(function (err, inventory) {
        if (err) {
            logger.error('Error while loading the Order details with order id- ' + inventoryId);
            done(err);
        }else if(!inventory){
            logger.error('No Inventory with the inventory id :' + inventoryId);
            done(new Error('No Inventory with the Id'+inventoryId));
        }else{
            Inventory.populate(inventory, {
                path: 'user.company',
                model: 'Company',
                select: 'name'
            }, function (nestedErr, inventoryProduct) {
                if (nestedErr) {
                    done(nestedErr);
                } else {
                    var destinations = [];
                    pathNode.inventory=inventoryProduct;
                    pathNode.company = inventoryProduct.user.company;
                    pathNode.destinations = destinations;
                    logger.error('Found while loading the Order details with first id- ' + inventory._id);

                    async.each(inventoryProduct.source.orders, function (singleOrder, orderCallBack) {
                        if (singleOrder.order.seller.nVipaniUser) {
                            Inventory.findOne({
                                productNum: inventory.productNum,
                                user: singleOrder.order.seller.nVipaniUser.toString()
                            },'-offerUnits -saleUnitPrice -buyUnitPrice -updateHistory -numberOfUnitsHistory -offerUnitsHistory -buyOfferUnitsHistory -moqAndPrice -croppedInventoryImageURL2 -croppedInventoryImageURL3 -croppedInventoryImageURL4 -inventoryImageURL2 -inventoryImageURL3 -inventoryImageURL4').populate('product','-productImageURL2 -productImageURL3 -productImageURL4 -croppedProductImageURL2 -croppedProductImageURL3 -croppedProductImageURL4 -gradeDefinition -qualityDefinition -updateHistory').populate('source.orders.order','seller').populate('user','company').exec(function (err, nextSetInventory) {
                                if (!err) {
                                    Inventory.populate(nextSetInventory, {
                                        path: 'user.company',
                                        model: 'Company',
                                        select: 'name'
                                    }, function (nestOrderErr, nextSetInventoryProduct) {
                                        if (nestOrderErr) {
                                            orderCallBack(nestOrderErr);
                                        } else {
                                            logger.error('Error while loading the Order details with next Inventory id- ' + nextSetInventory._id);
                                            var childPathNode = {count:singleOrder.count,inventory: nextSetInventoryProduct,company:nextSetInventoryProduct.user.company};
                                            fetchInventoryTrackById(nextSetInventory._id.toString(), childPathNode, function (error, respChildNode) {
                                                pathNode.destinations.push(respChildNode);
                                                orderCallBack();
                                            });
                                        }
                                    });
                                }else {
                                    orderCallBack(err);
                                }
                            });
                        } else {
                            orderCallBack();
                        }

                    }, function (orderErr) {
                        if(!orderErr) done(null, pathNode);
                        else done(orderErr);
                    });
                }
            });
        }
    });

}


function fetchInventoryByIdAtSingle(inventoryId, done) {
    Inventory.findById(inventoryId).populate('user', 'displayName').populate('product').exec(function (err, inventory){
        if (err) {
            logger.error('Error while loading the Order details with order id- ' + inventoryId);
            done(err);
        } else {
            Inventory.populate(inventory, {
                path: 'product.category product.subCategory1 product.subCategory2',
                model: 'Category',
                select: 'name categoryImageURL1 categoryImageURL2 categoryImageURL3 hsncodes'
            }, function (nestedErr, inventoryProduct) {
                if (nestedErr) {
                    done(nestedErr);
                } else {
                    done(inventoryProduct);

                    /* HsnCode.populate(inventoryProduct, {
                         path: 'product.subCategory2.hsncodes.hsncode',
                         model: 'Hsncodes',
                         select: 'name hsncode description CGST SGST IGST'
                     }, function (nestedhsnErr, hsncodeproduct) {
                         if (nestedhsnErr) {
                             done(nestedhsnErr);
                         } else {
                             done(hsncodeproduct);
                         }
                     });*/
                }
            });
        }
    });
}
function inventoryCreate(inventory,productObject,user,done){
    var buId = inventory.businessUnit;
    if (inventory.product) {
        logger.debug('Started inventory product using product Name- ' + productObject.name + ', Product Number:' + productObject.productUID);
        if (inventory.productNum) {
            inventory.set('lastUpdated', Date.now());
            inventory.updateHistory.push({modifiedOn: Date.now(), modifiedBy: user});
        } else {
            inventory.user = user;
            inventory.updateHistory = [];
            inventory.updateHistory.push({modifiedOn: Date.now(), modifiedBy: user});
        }

        inventory.save(function (saveInventory) {
            if (saveInventory) {
                done(saveInventory);
            } else {
                Company.findById(user.company).exec(function (compErr, company) {
                    if (compErr) {
                        logger.error('Failed to fetch company name as part of  Inventory creation');
                        done(compErr);
                    }

                    var versionKey = company.companiesVersionKey;
                    logger.debug('Setting the Inventory to company - ' + company.name);
                    company.inventories.push({inventory: inventory});
                    company.updateHistory.push({modifiedOn: Date.now(), modifiedBy: user});
                    if(!buId){
                        //use default business unit if business id is not passed in.
                        buId = company.businessUnits[0]._id;
                    }
                    BusinessUnit.findById(buId,function(err,businessUnit) {
                        if(!businessUnit.inventories) {
                            businessUnit.inventories = [];
                        }
                        businessUnit.inventories.push({inventory: inventory});
                        businessUnit.save(function (savBuErr) {
                            if (savBuErr) {
                                logger.error('Error updating inventories to Business unit:' + businessUnit.name);
                            }
                            company.companiesVersionKey = versionKey;
                            company.set('lastUpdated', Date.now());
                            company.save(function (savCompErr) {
                                if (savCompErr) {
                                    logger.error('Error while setting the Inventory to company - ' + company.name);
                                    done(savCompErr);
                                } else {
                                    logger.debug('Successfully updated the Inventory to company - ' + company.name + ' for the product name' + productObject.name);
                                    done(inventory);
                                }
                            });
                        });
                    });
                });
            }
        });

    } else {
        logger.debug('Started creating master product with product Name- ' + productObject.name);
        if(productObject.productUID){
            productObject.user = user._id;
        }else{
            productObject.updateHistory = [];
            productObject.user = user;
        }
        productObject.set('lastUpdated', Date.now());
        productObject.updateHistory.push({modifiedOn: Date.now(), modifiedBy: user._id});
        productObject = _.extend(productObject, '');
        productObject.save(function (err) {
            if (err) {
                logger.error('Failed to create master product with product Name- ' + productObject.name + ': ' + errorHandler.getErrorMessage(err));
                done(err);
                /* return res.status(400).send({
                 message: errorHandler.getErrorMessage(err)
                 });*/
            } else {
                logger.debug('Successfully created master product with product Name- ' + productObject.name);
                logger.debug('Started creating inventory product with newly created master product Name- ' + productObject.name + ', Product Number:' + productObject.productUID);
                //inventory.productNum=productObject.productUID;
                if (productObject) {
                    inventory.product = productObject._id.toString();
                }
                inventory.user = user;
                inventory.updateHistory = [];
                inventory.set('lastUpdated', Date.now());
                inventory.updateHistory.push({modifiedOn: Date.now(), modifiedBy: user});
                inventory = _.extend(inventory, '');
                inventory.save(function (saveInventory) {
                    if (saveInventory) {
                        done(saveInventory);
                    } else {
                        Company.findById(user.company).exec(function (compErr, company) {
                            if (compErr) {
                                logger.error('Failed to fetch company name as part of  Inventory creation');
                                done(compErr);
                            }

                            var versionKey = company.companiesVersionKey;
                            logger.debug('Setting the Inventory to company - ' + company.name);
                            if(!buId){
                                //use default business unit if business id is not passed in.
                                buId = company.businessUnits[0].businessUnit;
                                logger.debug('Default business unit id:'+buId);
                            }
                            BusinessUnit.findById(buId,function(err,businessUnit) {
                                if(!businessUnit.inventories) {
                                    businessUnit.inventories = [];
                                }
                                businessUnit.inventories.push({inventory: inventory});
                                businessUnit.save(function (savBuErr) {
                                    if (savBuErr) {
                                        logger.error('Error updating inventories to Business unit:' + businessUnit.name);
                                    }
                                    //company.inventories.push({inventory: inventory});
                                    else {
                                        company.updateHistory.push({modifiedOn: Date.now(), modifiedBy: user});

                                        company.companiesVersionKey = versionKey;

                                        company.save(function (savCompErr) {
                                            if (savCompErr) {
                                                logger.error('Error while setting the Inventory to company - ' + company.name);
                                                done(savCompErr);
                                            } else {
                                                logger.debug('Successfully updated the Inventory to company - ' + company.name + ' for the product name' + productObject.name);
                                                done(inventory);
                                            }
                                        });
                                    }
                                });
                            });
                        });
                    }
                });
            }
        });
    }
}
function createMasterProduct(inventory, productObject, user, done) {
    if(inventory instanceof  Inventory){
        inventoryCreate(inventory, productObject, user, function (inventoryResponse) {
            done(inventoryResponse);
        });
    }else if (inventory instanceof Array){
        var inventories=[];
        async.each(inventory, function (eachInventory, callback) {
            eachInventory=new Inventory(eachInventory);
            if(inventories.length>0){
                inventory.product=inventories[0].product;
            }
            inventoryCreate(eachInventory, productObject, user, function (inventoryResponse) {
                if(inventoryResponse instanceof  Error){
                    logger.debug('Error while creating the inventory'+inventoryResponse);
                    callback(inventoryResponse);
                }else {
                    /*fetchInventoryByIdAtSingle(inventoryResponse._id, function (inventoryR) {*/
                    inventoryUtil.findInventoryById(inventoryResponse._id,-1, function (inventoryR) {
                        if (inventoryR instanceof Error) {
                            logger.error('Error while fetching  inventory after creation- ' + productObject.name + ' Error Details:' + errorHandler.getErrorMessage(inventoryResponse));
                            callback(inventoryR);
                        } else {
                            inventories.push(inventoryR);
                            callback();
                        }
                    });

                }
            });
        }, function (err) {
            if (err) {
                done(err);
            } else {
                done(inventories);
            }
        });

    }

}



/**
 * Create a Inventory
 */
exports.create = function (req, res) {
    var inventory = new Inventory(req.body);
    var token = req.body.token || req.headers.token;
    logger.debug('Inventory create -' + JSON.stringify(req.body));
    usersJWTUtil.findUserByToken(token, function (err, user) {
        if (err) {
            return res.status(400).send({
                message: errorHandler.getErrorMessage(err)
            });
        }
        inventory.user = user;
        inventory.updateHistory = [];
        inventory.set('lastUpdated', Date.now());
        inventory.updateHistory.push({modifiedOn: Date.now(), modifiedBy: user});

        inventory.save(function (saveErr) {
            if (saveErr) {
                logger.debug('Error while creating the inventory', saveErr);
                return res.status(400).send({
                    message: errorHandler.getErrorMessage(saveErr)
                });
            } else {
                Company.findById(user.company).exec(function (compErr, company) {
                    if (compErr) {
                        return res.status(400).send({
                            message: errorHandler.getErrorMessage(compErr)
                        });
                    }

                    var versionKey = company.companiesVersionKey;
                    logger.debug('Setting the Inventory to company - ' + company.name);
                    company.inventories.push({inventory: inventory});
                    company.set('lastUpdated', Date.now());
                    company.updateHistory.push({modifiedOn: Date.now(), modifiedBy: user});

                    company.companiesVersionKey = versionKey;

                    company.save(function (savCompErr) {
                        if (savCompErr) {
                            logger.debug('Error while setting the Inventory to company - ' + company.name);
                            return res.status(400).send({
                                message: errorHandler.getErrorMessage(savCompErr)
                            });
                        } else {
                            res.jsonp(inventory);
                        }
                    });

                });

            }
        });
    });
};
/**
 * Create inventory with master product
 */
exports.createProduct = function (req, res) {
    var inventory =null;
    if(req.body.inventory instanceof Array){
        inventory=req.body.inventory;
    }else{
        inventory = new Inventory(req.body.inventory);
    }
    var productObject = new Product(req.body.product);
    var token = req.body.token || req.headers.token;
    logger.debug('Inventory create -' + JSON.stringify(req.body));
    usersJWTUtil.findUserByToken(token, function (err, user) {
        if (err) {
            return res.status(400).send({
                message: errorHandler.getErrorMessage(err)
            });
        }

        createMasterProduct(inventory, productObject, user, function (inventoryResponse) {
            if (inventoryResponse instanceof Error) {
                logger.error('Error while creating product in inventory - ' + productObject.name + ' Error Details:' + errorHandler.getErrorMessage(inventoryResponse));
                return res.status(400).send({
                    message: errorHandler.getErrorMessage(inventoryResponse)
                });
            } else if(inventoryResponse instanceof Array) {
                res.jsonp(inventoryResponse);
            }else {
                logger.debug('Successfully created inventory products with product Name- ' + productObject.name);
                if (inventoryResponse instanceof Inventory) {
                    inventoryUtil.findInventoryById(inventoryResponse._id,-1, function (inventoryResponse) {
                        if (inventoryResponse instanceof Error) {
                            logger.error('Error while fetching  inventory after creation- ' + productObject.name + ' Error Details:' + errorHandler.getErrorMessage(inventoryResponse));
                            return res.status(400).send({
                                message: errorHandler.getErrorMessage(inventoryResponse)
                            });
                        } else {
                            res.jsonp(inventoryResponse);
                        }
                    });
                }
            }
        });


    });
};
/**
 * Create Item master and Inventory and stock master
 *
 */
exports.createInventoryMaster = function (req, res) {
    var inventory =req.body.inventory;
    var productBrand=req.body.productBrand;
    var businessUnit = req.body.businessUnit;

    /* var uom=req.body.uom;*/
    var mobileAppKey=req.headers.apikey;
    /* if(req.body.inventory instanceof Array){
         inventory=req.body.inventory;
     }else {
         inventory = new Inventory(req.body.inventory);

     }*/
    /*var productObject = new Product(req.body.product);*/
    var token = req.body.token || req.headers.token;
    logger.debug('Inventory create -' + JSON.stringify(req.body));
    usersJWTUtil.findUserByToken(token, function (err, user) {
        if (err) {
            return res.status(400).send({
                message: errorHandler.getErrorMessage(err)
            });
        }
        else{
            var owner={businessUnit:businessUnit,user:user};
            inventoryUtil.createMassImport([{inventory:inventory,productBrand:productBrand,businessUnit:businessUnit}],owner,'other',function (err,response) {
                if(err){
                    return res.status(400).send({
                        message: errorHandler.getErrorMessage(err)
                    });
                }else{
                    if(mobileAppKey===config.bbapikey && response.length===1) {
                        res.jsonp(response[0]);
                    }else{
                        res.jsonp(response);
                    }
                }

            });
        }


    });
};
/**
 * Create mass import inventory
 */

exports.createUserInventoryImport = function (req, res) {
    var inventories =req.body.userinventories;
    var token = req.body.token || req.headers.token;
    var businessUnit = req.body.businessUnit;

    logger.debug('Inventory create -' + JSON.stringify(req.body));
    usersJWTUtil.findUserByToken(token, function (err, user) {
        if (err) {
            return res.status(400).send({
                message: errorHandler.getErrorMessage(err)
            });
        }  else{
            if(inventories ) {
                var owner={businessUnit:businessUnit,user:user};
                inventoryUtil.createMassImport(inventories,owner,'other', function (err, response) {
                    if (err) {
                        return res.status(400).send({
                            message: errorHandler.getErrorMessage(err)
                        });
                    } else {
                        res.jsonp(response);
                    }

                });
            }else{
                return res.status(400).send({
                    message: 'No User Inventories'
                });
            }
        }

    });
};

/**
 * Convert inventory with multiple inventories
 */
exports.convertInventory=function (req, res) {
    var inventories =req.body.inventories;
    var inventory=req.body.inventory;
    var productBrand=req.body.productBrand;
    var businessUnit=req.body.inventory?req.body.inventory.businessUnit:null;
    var convertType=req.body.convertType;
    var token = req.body.token || req.headers.token;
    logger.debug('Inventory Conversion create -' + JSON.stringify(req.body));
    usersJWTUtil.findUserByToken(token, function (err, user) {
        if (err) {
            return res.status(400).send({
                message: errorHandler.getErrorMessage(err)
            });
        } else {
            inventoryUtil.getValidateConversionData(req.body,logger,function (errDataFormat,data) {
                if (errDataFormat) {
                    return res.status(400).send({
                        message: errorHandler.getErrorMessage(errDataFormat),
                        status:true
                    });
                } else {
                    var owner={businessUnit:businessUnit,user:user};
                    inventoryUtil.conversionInventory(data.inventories, data.inventory, data.productBrand,owner,convertType, function (conversionErr, conversionInventory) {
                        if (conversionErr) {
                            return res.status(400).send({
                                status: true,
                                message: errorHandler.getErrorMessage(conversionErr)
                            });
                        } else {
                            if(conversionInventory._id){
                                inventoryUtil.findInventoryById(conversionInventory._id,1, function (inventoryResponse) {
                                    if (inventoryResponse instanceof Error) {
                                        /* logger.error('Error while fetching  inventory after creation- ' + productObject.name + ' Error Details:' + errorHandler.getErrorMessage(inventoryResponse));
                                         */    return res.status(400).send({
                                            message: errorHandler.getErrorMessage(inventoryResponse)
                                        });
                                    } else {
                                        res.jsonp(inventoryResponse);
                                    }
                                });
                            }else{
                                logger.error('Failed to create inventory with the data:'+req.body);
                                return res.status(400).send({
                                    status: true,
                                    message: 'No Conversion Inventory'
                                });
                            }
                        }
                    });
                }


            });
        }
    });

};

/**
 *  Search with UOM,Brand , category and Subcategory
 * @param req
 * @param res
 */
exports.searchInventory=function (req,res) {
    logger.debug('Inventory create -' + JSON.stringify(req.body));
    var token=req.body.token || req.headers.token;
    var inventories=req.body.inventories;
    var businessUnit=req.body.businessUnit;
    var issource=req.body.issource;
    if(token) {
        usersJWTUtil.findUserByToken(token, function (err, user) {
            if (err) {
                return res.status(400).send({
                    message: errorHandler.getErrorMessage(err)
                });
            } else {
                var isItemMaster = false;
                if(inventories && inventories.length>0) {
                    inventoryUtil.searchItemMaster(inventories,businessUnit, user, isItemMaster, issource, function (err, results, isItemMaster) {
                        if (err) {
                            logger.error('No User ItemMaster with the specified data' + JSON.stringify(req.body));
                            return res.status(400).send({
                                message: 'No User ItemMaster with the specified data'
                            });
                        }else{
                            res.jsonp({inventories: results, isItemMaster: isItemMaster});
                        }
                    });
                }else{
                    return res.status(400).send({
                        message: 'No inventories for search'
                    });
                }
            }
        });
    }else{
        logger.error('No authenticate user for the inventory search'+JSON.stringify(req.body));
        return res.status(400).send({
            message: 'No authenticate user for the inventory search'
        });
    }
};
function getFilterArray(field) {
    var queryObject= {OutOfStock:{'numberOfUnits': 0},LowOnStock:{'numberOfUnits': 1},Inactive:{'disabled': true},
        NewlyAdded:{$or:[{'packagingDate':{$lt: new Date(Date.now())}},{'created':{$lt: new Date(Date.now())}}]},Expired:{'expiryDate': {$lt: new Date(Date.now())}}};
    if(queryObject.hasOwnProperty(field)){
        return queryObject[field];
    }else {
        return null;
    }
}

function getQuery(summaryFilters,queryArray,done) {
    if(summaryFilters) {
        Object.keys(summaryFilters).forEach(function (key) {
            var value=getFilterArray(key);
            if(value){
                queryArray.push(value);
            }

        });
    }
    done(queryArray);

}
function inventoryQuery(req,done) {
    var token = req.body.token || req.headers.token;
    var lastSyncTime = req.headers.lastsynctime;
    var businessUnitId = req.query.businessUnitId?req.query.businessUnitId:req.params.busUnitId;
    if(!businessUnitId){
        done(new Error('The business unit id is not set'),null);
    }else {
        logger.info('Fetching user inventories');
        usersJWTUtil.findUserByToken(token, function (err, loginuser) {
            if (err) {
                logger.error('Error while fetching the user with the given token for fetching user inventories.');
                done(err, null);
            } else {
                var pageOptions = {};
                if (req.query.page && !isNaN(req.query.page)) {

                    pageOptions.page = parseInt(req.query.page);
                }
                if (req.query.limit) {
                    pageOptions.limit = parseInt(req.query.limit);
                }
                var query={$and:[{'businessUnit': businessUnitId}]};
                if (lastSyncTime) {
                    query.lastUpdated = {$gt: lastSyncTime};
                }
                var queryArray=[];
                if(req.query.searchText){
                    queryArray.push({businessUnit:businessUnitId});
                    var regExp={'$regex': req.query.searchText};
                    for (var property in Inventory.schema.paths) {
                        if (Inventory.schema.paths.hasOwnProperty(property)  && Inventory.schema.paths[property].instance === 'String') {
                            var eachproduct={};
                            eachproduct[property] = regExp;
                            queryArray.push(eachproduct);
                        }

                    }
                }
                getQuery(req.body.summaryFilters, queryArray, function (queryArray) {
                    var finalQuery = null;
                    if (queryArray.length > 0)
                        finalQuery = {$and: [query, {$or: queryArray}]};
                    else
                        finalQuery = query;
                    done(null, finalQuery, pageOptions);
                });
            }
        });
    }
}
/**
 * Show the current Inventory
 */
exports.read = function (req, res) {
    res.jsonp(req.inventory);
};

/**
 * Update a Inventory
 */
exports.update = function (req, res) {
    var inventory = req.inventory;
    var versionKey = inventory.inventoriesVersionKey;
    var oldNumberOfUnits = inventory.numberOfUnits;

    inventory = _.extend(inventory, req.body);
    var token = req.body.token || req.headers.token;
    logger.debug('Inventory update -' + JSON.stringify(inventory));
    usersJWTUtil.findUserByToken(token, function (err, user) {
        if (err) {
            return res.status(400).send({
                message: errorHandler.getErrorMessage(err)
            });
        } else {
            inventory.updateHistory.push({modifiedOn: Date.now(), modifiedBy: user});
            if (oldNumberOfUnits !== inventory.numberOfUnits) {
                var changeNumberOfUnits = inventory.numberOfUnits - oldNumberOfUnits;
                inventory.numberOfUnitsHistory.push({changeType: 'InventoryUpdate', changeUnits: changeNumberOfUnits});
            }
            inventory.inventoriesVersionKey = versionKey;
            inventory.set('lastUpdated', Date.now());
            inventory.save(function (saveErr) {
                if (saveErr) {
                    logger.debug('Error while updating the inventory.', saveErr);
                    return res.status(400).send({
                        message: errorHandler.getErrorMessage(saveErr)
                    });
                } else {
                    inventoryUtil.findInventoryById(inventory._id,1, function (inventoryResponse) {
                        if (inventoryResponse instanceof Error) {
                            /* logger.error('Error while fetching  inventory after creation- ' + productObject.name + ' Error Details:' + errorHandler.getErrorMessage(inventoryResponse));
                             */    return res.status(400).send({
                                message: errorHandler.getErrorMessage(inventoryResponse)
                            });
                        } else {
                            res.jsonp(inventoryResponse);
                        }
                    });
                }
            });
        }
    });
};

/**
 * Delete an Inventory
 */
exports.delete = function (req, res) {
    var inventory = req.inventory;

    inventory.remove(function (err) {
        if (err) {
            return res.status(400).send({
                message: errorHandler.getErrorMessage(err)
            });
        } else {
            res.jsonp(inventory);
        }
    });
};

/**
 * List of Inventories
 */
exports.productList = function (req, res) {
    var token = req.body.token || req.headers.token;
    var mobileAppKey=req.headers.apikey;
    var lastSyncTime = req.headers.lastsynctime;
    usersJWTUtil.findUserByToken(token, function (err, loginuser) {
        if(!loginuser || !loginuser.company){
            return res.status(400).send({
                message: 'No Data'
            });
        }else {
            var query={user: mongoose.Types.ObjectId(loginuser.id)};
            if (lastSyncTime) {
                query.lastUpdated = {$gt: lastSyncTime};
            }
            if(req.param('category')){

                query.productCategory= mongoose.Types.ObjectId(req.param('category'));
            }
            var queryArray=[];
            var finalQuery=null;
            if(queryArray.length>0)
                finalQuery={$and:[query]};
            else finalQuery=query;

            inventoryUtil.getProductInventories(query,function (err,products) {
                if(err){
                    return res.status(400).send({
                        message: errorHandler.getErrorMessage(err)
                    });
                }else {
                    res.jsonp(products);
                }

            });
        }
    });
};
/**
 * List of Inventories
 */
exports.list = function (req, res) {
    var token = req.body.token || req.headers.token;
    var businessUnitId = req.query.busUnitId;
    var mobileAppKey=req.headers.apikey;
    var lastSyncTime = req.headers.lastsynctime;
    usersJWTUtil.findUserByToken(token, function (err, loginuser) {
        if(!loginuser || !loginuser.company){
            return res.status(400).send({
                message: 'No Data'
            });
        }else {


            var pageOptions = {};
            if(req.param('page') && !isNaN(req.param('page'))){

                pageOptions.page= req.param('page');
                /*pageOptions.limit=10;*/
            }
            if(req.param('limit')){
                pageOptions.limit = parseInt(req.param('limit'));
            }

            var query={user: loginuser.id};
            if (lastSyncTime) {
                query.lastUpdated = {$gt: lastSyncTime};
            }
            var queryArray=[];
            queryArray.push({businessUnit:businessUnitId});
            if(req.param('searchText')){
                var regExp={'$regex': req.param('searchText')};
                for (var property in Inventory.schema.paths) {
                    if (Inventory.schema.paths.hasOwnProperty(property)  && Inventory.schema.paths[property].instance === 'String') {
                        var eachproduct={};
                        eachproduct[property] = regExp;
                        queryArray.push(eachproduct);
                    }

                }
            }
            var finalQuery=null;
            if(queryArray.length>0)
                finalQuery={$and:[query,{ $or : queryArray}]};
            else finalQuery=query;
            //get the businessunit id and set it here .

            /*
                        BusinessUnit.findById(buId,function(err,busUnit){
                            busUnit.inventories.populate();
                        });
            */
            inventoryUtil.findQueryByInventories([finalQuery],0,false,pageOptions,function (inventories) {
                if (inventories instanceof Error) {
                    return res.status(400).send({
                        message: errorHandler.getErrorMessage(inventories)
                    });
                } else {
                    res.jsonp(inventories);

                }
            });
        }
    });
};

/**
 * Inventory middleware
 */
exports.inventoryByID = function (req, res, next, id) {
    inventoryUtil.findInventoryById(id,1, function (inventoryResponse) {
        if (inventoryResponse instanceof Error) {
            logger.error('Error while loading the order details with Order number ' + id );
            return next(inventoryResponse);
        } else {
            req.inventory = inventoryResponse ;
            next();
        }
    });
};


exports.getTrackPaths=function (req, res) {
    //var trackPaths={company:'',destinations:[],inventory:''};
    var rootPathNode = {inventory:req.query.inventoryTrackId};

    var pathCounter=1;
    fetchInventoryTrackById(req.query.inventoryTrackId,rootPathNode, function (inventoryErr,responseRootPathNode) {
        res.jsonp(responseRootPathNode);

    });
};

exports.getForwardTrackerPaths=function (req, res) {
    var token = req.body.token || req.headers.token;
    var rootPathNode = {
        inventory: req.query.inventoryTrackId,
        stockMasters: [],
        businessUnit: req.query.businessUnit,
        company: req.query.company
    };
    var eachNode = {inventory: req.query.inventoryTrackId, stockMasters: [{stockMaster: req.query.stockMasterTrackId}]};
    if(token) {
        usersJWTUtil.getUserByToken(token, function (err, user) {
            if (err) {
                return res.status(400).send({
                    message: errorHandler.getErrorMessage(err)
                });
            } else {
                if (eachNode.inventory && eachNode.inventory !== 'undefined' && eachNode.stockMasters[0].stockMaster && eachNode.stockMasters[0].stockMaster !== 'undefined') {
                    inventoryTrackUtil.trackPaths(eachNode, rootPathNode, true, function (stockErr, stockId, pathNode) {
                        if (stockErr) {
                            return res.status(400).send({
                                status: false,
                                message: errorHandler.getErrorMessage(stockErr)
                            });
                        } else {
                            logger.debug('Final track Path ' + JSON.stringify(pathNode));
                            res.jsonp(pathNode);
                        }

                    });
                } else {
                    return res.status(400).send({
                        status: false,
                        message: 'No Proper input to trace the paths'
                    });
                }
            }
        });
    }else{
        return res.status(400).send({
            message: 'No Rights to access the paths'
        });
    }

};
exports.redirectPublicPaths = function(req, res){
    var bathNumber=req.query.batchNumber;
    var businessUnit=req.query.businessUnit;
    return res.redirect('/#!/tracker/publicPath?batchNumber='+bathNumber+'&businessUnit='+businessUnit);
};
exports.getPublicPaths=function (req,res) {
    var bathNumber=req.query.batchNumber;
    var businessUnit=req.query.businessUnit;
    if(businessUnit  && bathNumber) {
        StockMaster.findOne({
            _id: bathNumber,
            'currentOwner.businessUnit': businessUnit
        }).exec(function (batchNumberErr, currentBatchNumber) {
            if (currentBatchNumber) {
                var rootPathNode = {
                    inventory: currentBatchNumber.inventory,
                    stockMasters: [],
                    businessUnit: currentBatchNumber.currentOwner.businessUnit,
                    company: currentBatchNumber.currentOwner.company
                };
                var eachNode = {
                    inventory: currentBatchNumber.inventory,
                    stockMasters: [{stockMaster: currentBatchNumber._id}]
                };
                inventoryTrackUtil.trackPaths(eachNode, rootPathNode, false, function (stockErr, stockId, pathNode) {
                    if (stockErr) {
                        return res.status(400).send({
                            status: false,
                            message: errorHandler.getErrorMessage(stockErr)
                        });
                    } else {
                        logger.debug('Final track Path ' + JSON.stringify(pathNode));
                        res.jsonp(pathNode);
                    }

                });
            } else {
                return res.status(400).send({
                    status: false,
                    message: 'No Proper input to trace the paths'
                });

            }

        });
    }else{
        return res.status(400).send({
            message: 'Failed to get the paths with data '+req.query
        });
    }
};
exports.scanProduct=function (req,res) {
    var bathNumber=req.query.batchNumber;
    var businessUnit=req.query.businessUnit;
    if(businessUnit  && bathNumber) {
        StockMaster.findOne({
            _id: bathNumber,
            'currentOwner.businessUnit': businessUnit,
            'deleted':false
        }).populate('inventory','productName productSubCategoryName productMainCategoryName productBrand brandName taxGroupName unitOfMeasureName inventoryImageURL1 inventoryImageURL2 inventoryTestCertificate inventoryImageURL3').exec(function (batchNumberErr, currentBatchNumber) {
            if (currentBatchNumber) {
                StockMaster.populate(currentBatchNumber, [{
                    path: 'inventory.productBrand',
                    model: 'ProductBrand',
                    select:'gradeDefinition qualityDefinition name description'
                },{'path':'currentOwner.company',model:'Company',select:'name addresses'},{'path':'currentOwner.businessUnit',model:'BusinessUnit',select:'name addresses'}], function (nestedErr, populatedStockMasterInventory) {
                    if(nestedErr){
                        return res.status(400).send({
                            status: false,
                            message: 'No Proper scan input to trace the paths'
                        });
                    }else {
                        res.jsonp({
                            inventory: populatedStockMasterInventory.inventory,
                            stockMasters: populatedStockMasterInventory,
                            businessUnit: populatedStockMasterInventory.currentOwner.businessUnit,
                            company: populatedStockMasterInventory.currentOwner.company
                        });
                    }
                });
            }else{
                return res.status(400).send({
                    status: false,
                    message: 'No Proper scan input to trace the paths'
                });
            }
        });

    } else {
        return res.status(400).send({
            status: false,
            message: 'No product  with this details'
        });

    }

};
exports.getTrackerPaths=function (req, res) {

    var token = req.body.token || req.headers.token;
    var rootPathNode = {
        inventory: req.query.inventoryTrackId,
        stockMasters: [],
        businessUnit: req.query.businessUnit,
        company: req.query.company
    };
    var eachNode = {inventory: req.query.inventoryTrackId, stockMasters: [{stockMaster: req.query.stockMasterTrackId}]};
    if(token){
        usersJWTUtil.getUserByToken(token, function (err, user) {
            if (err) {
                return res.status(400).send({
                    message: errorHandler.getErrorMessage(err)
                });
            } else {
                if (eachNode.inventory && eachNode.inventory !== 'undefined' && eachNode.stockMasters[0].stockMaster && eachNode.stockMasters[0].stockMaster !== 'undefined') {
                    inventoryTrackUtil.trackPaths(eachNode, rootPathNode, false, function (stockErr, stockId, pathNode) {
                        if (stockErr) {
                            return res.status(400).send({
                                status: false,
                                message: errorHandler.getErrorMessage(stockErr)
                            });
                        } else {
                            logger.debug('Final track Path ' + JSON.stringify(pathNode));
                            res.jsonp(pathNode);
                        }

                    });
                } else {
                    return res.status(400).send({
                        status: false,
                        message: 'No Proper input to trace the paths'
                    });
                }
            }
        });
    }else
    {
        return res.status(400).send({
            message: 'No Rights to access the paths'
        });
    }

};
/**
 * Inventory authorization middleware
 */
exports.hasAuthorization = function (req, res, next) {
    var token = req.body.token || req.headers.token;
    usersJWTUtil.findUserByToken(token, function (err, user) {
        if (err) {
            return res.status(400).send({
                message: errorHandler.getErrorMessage(err)
            });
        } else {
            if ((req.inventory && req.inventory.owner && req.inventory.owner.company && req.inventory.owner.company.toString() !== user.company.toString())) {
                return res.status(403).send('User is not authorized');
            } else {
                next();
            }
        }
    });
};

/**
 * This api is used to update images for all the inventory items
 * previously imported from csv or xls.
 * requires valid session
 * @param req
 * @param res
 */
exports.massUpdateImages = function(req,res){
    var token = req.body.token || req.headers.token;
    var inventories=req.body.inventoriesToUpload;
    var type=req.body.type;
    var errors=[];

    usersJWTUtil.findUserByToken(token, function (err, user) {
        if (err) {
            return res.status(400).send({
                message: errorHandler.getErrorMessage(err)
            });
        } else {
            if (inventories && inventories.length > 0) {
                async.forEachSeries(inventories,function(inventory,done){
                    var srcLine = inventory.srcLine;
                    inventoryUtil.findInventoryById(inventory.inventoryId, -1,function(retInventory){
                        if(retInventory instanceof Error){
                            var message = 'Mass Upload Images: Failed to find inventory by id:'+inventory._id+ 'SRCLINE:'+srcLine;
                            logger.error(message);
                            errors.push({srcLine:inventory.srcLine,message:message});
                        }
                        else {
                            if(inventory.inventoryImageURL1 && util.isFileAccessible(inventory.inventoryImageURL1)) {
                                retInventory.inventoryImageURL1 = util.mapImageURL(inventory.inventoryImageURL1);
                            }
                            if(inventory.inventoryImageURL2 && util.isFileAccessible(inventory.inventoryImageURL2)) {
                                retInventory.inventoryImageURL2 = util.mapImageURL(inventory.inventoryImageURL2);
                            }
                            if(inventory.inventoryImageURL3 && util.isFileAccessible(inventory.inventoryImageURL3)) {
                                retInventory.inventoryImageURL3 = util.mapImageURL(inventory.inventoryImageURL3);
                            }
                            if(inventory.inventoryImageURL4 && util.isFileAccessible(inventory.inventoryImageURL4)) {
                                retInventory.inventoryImageURL4 = util.mapImageURL(inventory.inventoryImageURL4);
                            }

                            retInventory.save(function (invSaveErr, res) {
                                if (invSaveErr) {
                                    var message = 'Mass Upload Images: Failed to save inventory for id:' + inventory._id + 'SRCLINE:' + srcLine;
                                    logger.error(message);
                                    errors.push({srcLine: srcLine, message: message});
                                }
                                else {
                                    var brandId = retInventory.productBrand;
                                    ProductBrand.findById(brandId).exec(function (brandFindErr, productBrand) {
                                        if (brandFindErr) {
                                            var message = 'Mass Upload Images: Failed to find brand for id:' + retInventory.productBrand + 'SRCLINE:' + srcLine;
                                            logger.error(message);
                                            errors.push({srcLine: inventory.srcLine, message: message});
                                        }
                                        else {
                                            if(inventory.productBrandImageURL1 && util.isFileAccessible(inventory.productBrandImageURL1) ) {
                                                productBrand.productBrandImageURL1 = util.mapImageURL(inventory.productBrandImageURL1);
                                            }
                                            if(inventory.productBrandImageURL2 && util.isFileAccessible(inventory.productBrandImageURL2)) {
                                                productBrand.productBrandImageURL2 = util.mapImageURL(inventory.productBrandImageURL2);
                                            }
                                            if(inventory.productBrandImageURL3 && util.isFileAccessible(inventory.productBrandImageURL3)) {
                                                productBrand.productBrandImageURL3 = util.mapImageURL(inventory.productBrandImageURL3);
                                            }
                                            if(inventory.productBrandImageURL4 && util.isFileAccessible(inventory.productBrandImageURL4)) {
                                                productBrand.productBrandImageURL4 = util.mapImageURL(inventory.productBrandImageURL4);
                                            }
                                            productBrand.save(function (brandSaveErr) {
                                                if (brandSaveErr) {
                                                    var message = 'Mass Upload Images: Failed to save brand for id:' + retInventory.productBrand + 'SRCLINE:' + srcLine;
                                                    logger.error(message);
                                                    errors.push({srcLine: inventory.srcLine, message: message});
                                                }
                                                //save failed but we can try to update itemmaster image.
                                                ItemMaster.findById(retInventory.itemMaster).exec(function (itemMasterFindErr, itemMaster) {
                                                    if (itemMasterFindErr) {
                                                        var message = 'Mass Upload Images: Failed to find itemMaster for id:' + retInventory.productBrand + 'SRCLINE:' + srcLine;
                                                        logger.error(message);
                                                        errors.push({srcLine: inventory.srcLine, message: message});
                                                    }
                                                    else {

                                                        if(inventory.itemMasterImageURL1 && util.isFileAccessible(inventory.itemMasterImageURL1) && !util.isDefaultImage(itemMaster.itemMasterImageURL1) && itemMaster.user===user._id) {itemMaster.itemMasterImageURL1 = util.mapImageURL(inventory.itemMasterImageURL1);
                                                        }
                                                        if(inventory.itemMasterImageURL2 && util.isFileAccessible(inventory.itemMasterImageURL2) && !util.isDefaultImage(itemMaster.itemMasterImageURL2) && itemMaster.user===user._id) {
                                                            itemMaster.itemMasterImageURL2 = util.mapImageURL(inventory.itemMasterImageURL2);
                                                        }
                                                        if(inventory.itemMasterImageURL3 && util.isFileAccessible(inventory.itemMasterImageURL3) && !util.isDefaultImage(itemMaster.itemMasterImageURL3) && itemMaster.user===user._id) {
                                                            itemMaster.itemMasterImageURL3 = util.mapImageURL(inventory.itemMasterImageURL3);
                                                        }
                                                        if(inventory.itemMasterImageURL4 && util.isFileAccessible(inventory.itemMasterImageURL4) && !util.isDefaultImage(itemMaster.itemMasterImageURL4) && itemMaster.user===user._id) {
                                                            itemMaster.itemMasterImageURL4 = util.mapImageURL(inventory.itemMasterImageURL4);
                                                        }
                                                        itemMaster.save(function (itemMasterSaveErr) {
                                                            if (itemMasterSaveErr) {
                                                                var message = 'Mass Upload Images: Failed to find itemMaster for id:' + retInventory.itemMaster + 'SRCLINE:' + srcLine;
                                                                logger.error(message);
                                                                errors.push({
                                                                    srcLine: inventory.srcLine,
                                                                    message: message
                                                                });
                                                            }
                                                            else {
                                                                logger.info('Updated images for inventory:' + inventory._id + ',srcLine:' + inventory.srcLine);
                                                                done();
                                                            }
                                                        });
                                                    }
                                                });
                                            });
                                        }
                                    });
                                }
                            });

                        }
                    });
                },function(err){
                    if(err){
                        logger.error(err);
                        errors.push(err);
                    }
                    else{
                        return res.status(200).send({status:true,message:'Images updated successfully',errors:errors});
                    }
                });
            }
            else{
                return res.status(400).send({status:true,message:'No images were uploaded',errors:errors});
            }

        }
    });
};


exports.massDelete=function (req,res) {
    var token = req.body.token || req.headers.token;
    var inventories=req.body.inventories;
    var type=req.body.type;
    usersJWTUtil.findUserByToken(token, function (err, user) {
        if(err){
            return res.status(400).send({
                message: errorHandler.getErrorMessage(err)
            });
        }else {
            if(inventories && inventories.length>0) {
                var query={'owner.company':user.company};
                if(inventories && inventories.length>0){
                    query.$or=inventories;
                }
                //get business unit from any one of the inventory
                Inventory.findById(inventories[0]._id,function(invErr,invObj){
                    if(invErr){
                        return res.status(400).send({
                            message: errorHandler.getErrorMessage(invErr)
                        });
                    }
                    else{
                        var businessUnit = invObj.businessUnit;
                        var invToDelete = [];
                        for(var i=0;i<inventories.length;i++) {
                            invToDelete.push(inventories[i]._id);
                        }

                        //remove the inventories from the business unit and save it
                        BusinessUnit.update({_id:businessUnit},{$pull:{inventories:{inventory:{$in:invToDelete}}}}).exec(function(buErr,buUpdate){
                            if(buErr){
                                return res.status(400).send({
                                    message: errorHandler.getErrorMessage(buErr)
                                });
                            }
                            inventoryUtil.batchInventoryUpdate(query, {'deleted': true}, function (inventoryErr, results) {
                                if (inventoryErr) {
                                    return res.status(400).send({
                                        message: errorHandler.getErrorMessage(inventoryErr)
                                    });
                                } else if (!results) {
                                    return res.status(400).send({
                                        message: 'Not Deleted',
                                        status: false
                                    });
                                } else {
                                    inventoryUtil.findQueryByInventories([{businessUnit:businessUnit}], 0, false, {
                                        page: 0,
                                        limit: 10
                                    }, function (inventories) {
                                        if (inventories instanceof Error) {
                                            return res.status(400).send({
                                                message: errorHandler.getErrorMessage(inventories)
                                            });
                                        } else {
                                            res.jsonp(inventories);
                                        }

                                    });
                                }

                            });
                        });
                    }
                });

            }else{
                return res.status(400).send({
                    message: 'No Selected Inventories'
                });
            }
        }
    });
};

exports.massDisable=function (req,res) {
    var token = req.body.token || req.headers.token;
    var inventories=req.body.inventories;
    var type=req.body.type;
    usersJWTUtil.findUserByToken(token, function (err, user) {
        if(err){
            return res.status(400).send({
                message: errorHandler.getErrorMessage(err)
            });
        }else {
            if(inventories && inventories.length>0) {
                var query = {'owner.company': user.company};
                if (inventories && inventories.length > 0) {
                    query.$or = inventories;
                }
                inventoryUtil.batchInventoryUpdate(query, {'disabled': true}, function (inventoryErr, results) {
                    if (inventoryErr) {
                        return res.status(400).send({
                            message: errorHandler.getErrorMessage(inventoryErr)
                        });
                    } else if (!results) {
                        return res.status(400).send({
                            message: 'Not Deleted',
                            status: false
                        });
                    } else {
                        inventoryUtil.findQueryByInventories([query], -1, false, {
                            page: 0,
                            limit: 10
                        }, function (err, inventories) {
                            if (err) {
                                return res.status(400).send({
                                    message: errorHandler.getErrorMessage(err)
                                });
                            } else {
                                res.jsonp(inventories);
                            }

                        });
                    }

                });
            }else{
                return res.status(400).send({
                    message: 'No Selected Inventories'
                });
            }
        }
    });
};
exports.massEnable=function (req,res) {
    var token = req.body.token || req.headers.token;
    var inventories=req.body.inventories;
    var type=req.body.type;
    usersJWTUtil.findUserByToken(token, function (err, user) {
        if(err){
            return res.status(400).send({
                message: errorHandler.getErrorMessage(err)
            });
        }else {
            if(inventories && inventories.length>0) {
                var query = {'owner.company': user.company};
                if (inventories && inventories.length > 0) {
                    query.$or = inventories;
                }
                inventoryUtil.batchInventoryUpdate(query, {'disabled': false}, function (inventoryErr, results) {
                    if (inventoryErr) {
                        return res.status(400).send({
                            message: errorHandler.getErrorMessage(inventoryErr)
                        });
                    } else if (!results) {
                        return res.status(400).send({
                            message: 'Not Deleted',
                            status: false
                        });
                    } else {
                        inventoryUtil.findQueryByInventories([query], -1, false, {
                            page: 0,
                            limit: 10
                        }, function (err, inventories) {
                            if (err) {
                                return res.status(400).send({
                                    message: errorHandler.getErrorMessage(err)
                                });
                            } else {
                                res.jsonp(inventories);
                            }

                        });
                    }

                });
            }else{
                return res.status(400).send({
                    message: 'No Selected Inventories'
                });
            }
        }
    });

};
exports.inventoryByBusinessUnit = function(req,res){
    inventoryQuery(req,function(err,query,pageOptions){
        if(err) {
            return res.status(400).send({
                status: false,
                message: errorHandler.getErrorMessage(err)
            });
        }else{
            inventoryUtil.findQueryByInventories([query], 0,true, pageOptions, function ( inventories) {
                if (!inventories) {
                    logger.error('Error while fetching user inventory ');
                    return res.status(400).send({
                        status: false,
                        message: errorHandler.getErrorMessage(err)
                    });
                } else {
                    res.jsonp(inventories);
                }
            });

        }
    });
};



exports.getInventorySummaries = function (req, res) {
    inventoryQuery(req,function(err,query,pageOptions){
        if(err) {
            return res.status(400).send({
                status: false,
                message: errorHandler.getErrorMessage(err)
            });
        }else{
            inventoryUtil.findQueryByInventories([query], 0,false, pageOptions, function ( inventories) {
                if (!inventories) {
                    logger.error('Error while fetching user inventory ');
                    return res.status(400).send({
                        status: false,
                        message: errorHandler.getErrorMessage(err)
                    });
                } else {
                    inventoryUtil.getInventorySummary(inventories.inventory,req.body.summaryFilters, function (summaryData) {
                        res.jsonp(summaryData);
                    });
                }
            });

        }
    });
};
