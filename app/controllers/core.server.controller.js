'use strict';

var errorHandler = require('./errors.server.controller'),
	usersJWTUtil   = require('./utils/users.jwtutil');
/**
 * Module dependencies.
 */
exports.index = function(req, res) {
	var token = req.body.token || req.headers.token;
	if(token) {
		usersJWTUtil.findUserByToken(token, function (err, user) {
			if (err) {
				res.status(400).send({
					message: errorHandler.getErrorMessage(err)
				});
			} else {
				res.render('index', {
					user: user || null,
					request: req
				});
			}
		});
	} else {
		res.render('index', {
			user: null,
			request: req
		});
	}
};
