'use strict';

/**
 * Module dependencies.
 */
var mongoose = require('mongoose'),
	errorHandler = require('./errors.server.controller'),
	usersJWTUtil   = require('./utils/users.jwtutil'),
	Lead = mongoose.model('Leads'),
	logger = require('../../lib/log').getLogger('LEADS', 'DEBUG'),
	_ = require('lodash');

/**
 * Create a Product
 */
exports.create = function(req, res) {
	var lead = new Lead(req.body);
	var token = req.body.token || req.headers.token;
	logger.debug('req.lead create -'+req.body);
		lead.save(function (err) {
			if (err) {
				return res.status(400).send({
					message: errorHandler.getErrorMessage(err)
				});
			} else {
				res.jsonp(lead);
			}
		});
};

/**
 * Show the current Product
 */
exports.read = function(req, res) {
	res.jsonp(req.lead);
};

/**
 * Update a Product
 */
exports.update = function(req, res) {
	var lead = req.lead;

	logger.debug('req.lead received  -'+req.body);
	logger.debug('req.lead before update -'+req.lead);
	var versionKey = lead.leadsVersionKey;
	lead = _.extend(lead , req.body);
	lead.leadsVersionKey = versionKey;
	logger.debug('req.lead -'+req.lead);
	lead.save(function(err) {
		if (err) {
			return res.status(400).send({
				message: errorHandler.getErrorMessage(err)
			});
		} else {
			res.jsonp(lead);
		}
	});
};

/**
 * Delete an Product
 */
exports.delete = function(req, res) {
	var lead = req.lead ;

	lead.remove(function(err) {
		if (err) {
			return res.status(400).send({
				message: errorHandler.getErrorMessage(err)
			});
		} else {
			res.jsonp(lead);
		}
	});
};

/**
 * List of Products
 */
exports.list = function (req, res) {
	Lead.find().sort('-created').exec(function (err, leads) {
		if (err) {
			return res.status(400).send({
				message: errorHandler.getErrorMessage(err)
			});
		} else {
			res.jsonp(leads);
		}
	});
};

/**
 * Product middleware
 */
exports.leadByID = function(req, res, next, id) {
	Lead.findById(id).exec(function(err, lead) {
		if (err) return next(err);
		if (! lead) return next(new Error('Failed to load lead ' + id));
		req.lead = lead ;
		next();
	});
};

/**
 * Product authorization middleware
 */
exports.hasAuthorization = function(req, res, next) {
	var token = req.body.token || req.headers.token;
/*	usersJWTUtil.getUserByToken(token, function(err, user) {
		if (err) {
			return res.status(400).send({
				message: errorHandler.getErrorMessage(err)
			});
		} else {*/
			next();
/*
		}
	});
*/

};
