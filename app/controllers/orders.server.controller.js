'use strict';

/**
 * Module dependencies.
 */
var mongoose = require('mongoose'),
    errorHandler = require('./errors.server.controller'),
    usersJWTUtil   = require('./utils/users.jwtutil'),
    notificationUtil = require('./utils/notification.util'),
    inventoriesUtil = require('./utils/common.inventories.util'),
    contactUtil = require('./utils/common.contacts.util'),
    bUtil=require('./utils/common.businessunit.util'),
    reportsUtil = require('./utils/reports.util'),
    logger = require('../../lib/log').getLogger('ORDERS', 'DEBUG'),
    async = require('async'),
    config = require('../../config/config'),
    commonUtil= require('./utils/common.util'),
    dbUtil = require('./utils/common.db.util'),
    globalUtil=require('./utils/common.global.util'),
    offerUtil= require('./utils/common.offer.util'),
    orderUtil= require('./utils/common.orders.util'),
    path = require('path'),
    Order = mongoose.model('Order'),
    Offer = mongoose.model('Offer'),
    User = mongoose.model('User'),
    Contact = mongoose.model('Contact'),
    Product = mongoose.model('Product'),
    Inventory = mongoose.model('Inventory'),
    jsreport=require('jsreport'),
    Companies=mongoose.model('Company'),
    businessUnitUtil= require('./utils/common.businessunit.util'),
    Notification = mongoose.model('Notification'),
    StockMaster = mongoose.model('StockMaster'),
    Todo = mongoose.model('Todo'),
    fs = require('fs'),
    _ = require('lodash');
/**
 * Order inter state
 */
function getGSTInvoice(sellerCompany,buyerCompany) {
    return sellerCompany.gstinNumber && sellerCompany.gstinNumber.length>2 && buyerCompany.gstinNumber && buyerCompany.gstinNumber.length>2 && sellerCompany.gstinNumber.substring(0,2)!==buyerCompany.gstinNumber.substring(0,2) ;
}
/*
Get MoqVs Price from inventory

 */
function  getMoqPrice(product,isBuyer){
    var moq;
    if(isBuyer){
        if(product.inventory.moqAndBuyMargin) {
            moq = product.inventory.moqAndBuyMargin;
        }
    }else{
        if(product.inventory.moqAndMargin) {
            moq = product.inventory.moqAndMargin;
        }
    }

    return moq;
}

/**
 *
 * @param buyerInventory
 * @param sellerStockMaster
 * @param done
 */
function getBuyerMatchedStockMasters(buyerInventory,eachOrderStockMaster,done) {
    logger.debug('user Buyer inventory Stock masters to get the matched Stocks for the buyer inventory as part of order delivery');
    if(!buyerInventory){
        done(null,null);
    }else if(buyerInventory && buyerInventory._id){
        var filterBuyerStock=buyerInventory.stockMasters.filter(function (eachBuyerStock) { return eachBuyerStock.batchNumber&&eachOrderStockMaster.batchNumber && eachBuyerStock.batchNumber=== eachOrderStockMaster.batchNumber; });
        if(filterBuyerStock.length>0){
            done(null,filterBuyerStock[0]);
        }else{
            done(null,null);
        }
    }else{
        done(null,null);
    }
}

/**
 * Deprecated API
 * @param stockMasters
 * @param matchedStockMaster
 * @param done
 */
function getMatchedStockMasterById(stockMasters,matchedStockMaster,done){
    var matchedRecords= stockMasters.filter(function (eachStock) {
        return eachStock._id.toString()===matchedStockMaster.stockMaster.toString();
    });
    if(matchedRecords.length===0){
        logger.error('No matched stock masters at order stockMaster'+matchedStockMaster);
        done(null);
    }else{
        done(matchedRecords[0]);
    }
}

/**
 *
 * @param orderId
 * @param sellerInventory
 * @param buyerInventory
 * @param user
 * @param orderProduct
 * @param eachBuyerUserItemMaster
 * @param done
 */
function getOrderMatchedSellerStocksBuyProduct(orderId,user,orderProduct,eachBuyerUserItemMaster,done){

    // user seller inventory stock master to get the matched stocks for the buyer inventory creation.
    logger.debug('Update Stock master at the buyer inventory as part of order delivery for the Order Id :'+orderId);

    if(orderProduct.stockMasters && orderProduct.stockMasters.length>0) {
        async.forEachSeries(orderProduct.stockMasters, function (eachOrderStockMaster, callbackStockOut) {
            /*  getMatchedStockMasterById(sellerInventory.stockMasters,eachOrderStockMaster,function (eachStockMaster) {*/
            var isOld = false;
            if (eachOrderStockMaster) {
                // Get buyer matched stock master with seller batch number.
                getBuyerMatchedStockMasters(eachBuyerUserItemMaster.buyerInventory, eachOrderStockMaster, function (buyerStockMasterErr,buyerMatchedStockMaster) {

                    // if there is any existing seller matched stock master at buyer level
                    if (buyerMatchedStockMaster) {
                        buyerMatchedStockMaster.stockIn.orders.push({
                            order: orderId,
                            count: eachOrderStockMaster.numberOfUnits,
                            updatedUser: user,
                            updatedDate: new Date()
                        });
                        buyerMatchedStockMaster.currentBalance += eachOrderStockMaster.numberOfUnits;
                        eachBuyerUserItemMaster.buyerInventory.numberOfUnits+=eachOrderStockMaster.numberOfUnits;
                        eachBuyerUserItemMaster.stockMasters.push(buyerMatchedStockMaster);
                    } else {
                        buyerMatchedStockMaster = new StockMaster(eachOrderStockMaster);
                        buyerMatchedStockMaster._id = mongoose.Types.ObjectId();
                        buyerMatchedStockMaster.isNew = true;
                        buyerMatchedStockMaster.batchNumber = eachOrderStockMaster.batchNumber;
                        var stockArray={other:[],inventories:[],orders:[]};
                        buyerMatchedStockMaster.stockIn = stockArray;
                        buyerMatchedStockMaster.stockOut = stockArray;
                        /*if (eachOrderStockMaster.stockOwner) {
                            buyerMatchedStockMaster.stockOwner = eachOrderStockMaster.stockOwner;
                        } else {
                            buyerMatchedStockMaster.stockOwner = eachOrderStockMaster.user;
                        }*/
                        buyerMatchedStockMaster.currentBalance = eachOrderStockMaster.numberOfUnits;
                        buyerMatchedStockMaster.MRP = eachOrderStockMaster.unitPrice;
                        buyerMatchedStockMaster.stockIn.orders = [{
                            order: orderId,
                            count: eachOrderStockMaster.numberOfUnits,
                            updatedUser: user,
                            updatedDate: new Date()
                        }];


                    }
                    eachBuyerUserItemMaster.stockMasters.push(buyerMatchedStockMaster);
                    callbackStockOut();


                });
            } else {
                // There is no stock out order at seller inventory .
                logger.error('There is no matched stock masters at seller stock with Id :' + eachOrderStockMaster._id);
                done(new Error('There is no stock out orders at seller inventory'));

            }

            /*});*/
        }, function (err) {
            if (err) {
                done(err, null);
            } else {
                done(null, eachBuyerUserItemMaster);
            }
        });
    }else{
        logger.Error('No proper stockMaster for the product at order'+JSON.stringify(orderProduct));
        done(new Error('No proper stockMaster for the product at order'), eachBuyerUserItemMaster);
    }

}
function getMatchedBusinessUnits(businessArray,playerId) {
    var matchedRecords;
    if (businessArray && businessArray instanceof Array && businessArray.length === 2) {
        matchedRecords = businessArray.filter(function (eachUnit) {
            return eachUnit._id.toString() === playerId.toString();
        });
        if(matchedRecords.length===1){
            return matchedRecords[0];
        }else{
            return matchedRecords;
        }
    }  else{
        return matchedRecords;
    }
}
function createInventoryAndStockMaster(stockMasterCreation,order,done) {
    if(stockMasterCreation && stockMasterCreation instanceof Array && stockMasterCreation.length>0) {
        if (order.buyer.businessUnit && order.buyer.businessUnit._id && order.seller.businessUnit && order.seller.businessUnit._id) {
            businessUnitUtil.findQuery([{$or: [{_id: order.buyer.businessUnit._id}, {_id: order.seller.businessUnit._id}]}],null, null, function (businessStockOwnerUnitError, fetchStockOwnersBusinessUnits) {
                if (businessStockOwnerUnitError) {
                    done(businessStockOwnerUnitError);
                } else {
                    var sellerBusinessUnit = getMatchedBusinessUnits(fetchStockOwnersBusinessUnits, order.seller.businessUnit._id);
                    var buyerBusinessUnit = getMatchedBusinessUnits(fetchStockOwnersBusinessUnits, order.buyer.businessUnit._id);

                    async.eachSeries(stockMasterCreation, function (eachItemMasterCreation, inventorySaveCallBack) {
                        var isNew = eachItemMasterCreation.inventory.isNew;

                        var isNewStockMaster;
                        if (eachItemMasterCreation.inventory) {
                            // create Inventory - New Buyer inventory
                            if (isNew) {
                                if (!eachItemMasterCreation.inventory.owner) {
                                    eachItemMasterCreation.inventory.owner = {};
                                    eachItemMasterCreation.inventory.owner = globalUtil.prepareOwnerByBusinessUnit(sellerBusinessUnit);
                                }
                                /* if (sellerBusinessUnit)
                                     eachItemMasterCreation.inventory.owner = globalUtil.prepareOwnerByBusinessUnit(sellerBusinessUnit);*/
                                if(buyerBusinessUnit){
                                    eachItemMasterCreation.inventory.currentOwner=globalUtil.prepareOwnerByBusinessUnit(buyerBusinessUnit);
                                }
                            }
                            eachItemMasterCreation.inventory.save(function (inventorySaveErr) {
                                if (inventorySaveErr) {
                                    logger.error('Error while updating the inventory with product number ' + eachItemMasterCreation.inventory._id, inventorySaveErr);
                                    done(inventorySaveErr);
                                } else {
                                    async.eachSeries(eachItemMasterCreation.stockMasters, function (eachStockMaster, stockSaveCallback) {
                                        // isNew=eachStockMaster.isNew;
                                        // create Stock Master -New Stock Master Record
                                        //eachStockMaster=new St
                                        isNewStockMaster = eachStockMaster.isNew;

                                        /*var eachValidStock=new StockMaster(eachStockMaster);*/
                                        if (isNewStockMaster) {
                                            if (!eachStockMaster.currentOwner) {
                                                eachStockMaster.currentOwner = {};
                                            }
                                            // eachStockMaster.currentOwner=eachItemMasterCreation.inventory.owner;
                                            if (buyerBusinessUnit)
                                                eachStockMaster.currentOwner = globalUtil.prepareOwnerByBusinessUnit(buyerBusinessUnit);
                                            /* if (sellerBusinessUnit)*/
                                            eachStockMaster.stockOwner = eachItemMasterCreation.inventory.owner;

                                        }
                                        eachStockMaster.user = order.buyer.nVipaniUser;
                                        eachStockMaster.inventory = eachItemMasterCreation.inventory._id;
                                        eachStockMaster.save(function (stockSaveErr) {
                                            if (stockSaveErr) {
                                                logger.error('Failed to save the stock master at order delivery ' + errorHandler.getErrorMessage(stockSaveErr) + JSON.stringify(eachStockMaster));
                                                done(stockSaveErr);
                                            } else {
                                                stockSaveCallback();

                                            }
                                        });

                                    }, function (err) {
                                        if (err) {
                                            done(err);
                                        } else {
                                            // update inventory - Update the inventory with new stock record
                                            inventorySaveCallBack();
                                        }
                                    });

                                }

                            });

                        } else {
                            logger.error('No Product object at order with orderId:' + order.orderNumber + ' at Buyer stock master ');
                            done(new Error('No Inventory object at order with order Number' + order.orderNumber), null);
                        }
                    }, function (inventoryErr) {
                        done(inventoryErr);
                    });
                }
                /*else{
                                done(new Error('No Business unit to create inventory at order with Order Number '+order.orderNumber));
                            }*/
            });
        } else {
            done(null);
        }
    }else{
        done(null);
    }
}
/**
 * Create Buyer inventory with stock master from seller order products if the buyer is Registered user
 * @param order
 * @param loginuser
 * @param rollback
 * @param done
 */
function createInventoryProductWithStockMaster(order,owner,rollback, done) {
    if(order && order.buyer && order.buyer.nVipaniUser) {
        var stockMasterCreation=[];

        if (order.products && order.products.length > 0) {

            async.eachSeries(order.products, function (orderProduct, callback) {
                var orderInventoryId = (orderProduct.inventory._id ? orderProduct.inventory._id.toString() : orderProduct.inventory.toString());

                inventoriesUtil.findInventoryById(orderInventoryId, 1, function (inventory) {
                    if (inventory instanceof Error) {
                        logger.error('Error while loading the inventory with product number - ' + inventory.inventory, inventory);
                        callback(inventory);
                    } else {
                        /* inventory.user = order.buyer.nVipaniUser;*/
                        logger.debug('Fetch buyer inventory with Item Master :' + inventory.itemMaster + ' with user:' + order.buyer.nVipaniUser + ' with unit of measure :' + inventory.unitOfMeasure.name);
                        // find out if there is user item master  with item master
                        Inventory.findOne({
                            deleted: false,
                            businessUnit: order.buyer.businessUnit,
                            itemMaster: inventory.itemMaster,
                            unitOfMeasure: inventory.unitOfMeasure._id
                        }).populate('stockMasters hsncode taxGroup unitOfMeasure').exec(function (errBuyerInventory, buyerInventory) {
                            if (errBuyerInventory) {
                                logger.error('Error while loading the buyer inventory :' + inventory.itemMaster + ' with user:' + order.buyer.businessUnit + ' with unit of measure :' + inventory.unitOfMeasureName);
                                callback(errBuyerInventory);
                            } else {
                                var eachUserItemMasterInventory = {
                                    buyerInventory: buyerInventory,
                                    inventory: inventory,
                                    stockMasters: [],
                                    numberOfUnits: 0
                                };
                                getOrderMatchedSellerStocksBuyProduct(order._id, owner.user, orderProduct, eachUserItemMasterInventory, function (errBuyerMatchInventory, eachBuyerMatchedRecord) {
                                    if (errBuyerMatchInventory) {
                                        callback(errBuyerMatchInventory);
                                    } else if (eachBuyerMatchedRecord.stockMasters && eachBuyerMatchedRecord.stockMasters.length === 0) {
                                        logger.error('Not able to create any stock master at buyer for the Seller User Item master id :' + inventory._id + ' at order Id:' + order._id);
                                        callback(new Error('Not able to create any stock master at buyer for the User Item master'));
                                    } else {
                                        if (buyerInventory) {
                                            eachBuyerMatchedRecord.inventory = eachBuyerMatchedRecord.buyerInventory;
                                        } else {
                                            eachBuyerMatchedRecord.inventory = new Inventory(inventory);
                                            eachBuyerMatchedRecord.inventory.user = order.buyer.nVipaniUser;
                                            eachBuyerMatchedRecord.inventory._id = mongoose.Types.ObjectId();
                                            eachBuyerMatchedRecord.inventory.numberOfUnits = 0;
                                            eachBuyerMatchedRecord.inventory.isNew = true;
                                            eachBuyerMatchedRecord.inventory.owner=inventory.owner;
                                            eachBuyerMatchedRecord.inventory.businessUnit = order.buyer.businessUnit;
                                            eachBuyerMatchedRecord.inventory.stockMasters = [];
                                        }
                                        logger.debug('Got the stock master at buyer for the Seller User Item master id :' + inventory._id + ' at order Id:' + order._id);
                                        stockMasterCreation.push(eachBuyerMatchedRecord);
                                        callback();
                                    }

                                });
                            }
                        });


                    }
                });
            }, function (err) {
                if (err) {
                    done(err, null);
                } else {
                    if (stockMasterCreation.length > 0) {
                        createInventoryAndStockMaster(stockMasterCreation, order, function (createError) {
                            if (createError) {
                                done(createError, null);
                            } else {
                                done(null, order);
                            }
                        });
                    } else {
                        done(new Error('Failed to create stocks at delivery with order orderNumber :' + order.orderNumber));
                    }
                }
            });
        } else {

            logger.error('No Proper Products at order :' + order.orderNumber);
            done(new Error('No Proper Products at order :'), null);
        }


    }
    else{
        logger.error('No Buyer at order Id:'+order._id);
        done(null);
    }
}
/**
 *
 */
function createInventoryProduct(order,loginuser,rollback, done) {
    async.each(order.products, function (orderProduct, callback) {
        inventoriesUtil.findInventoryById(orderProduct.inventory,-1,function (inventory) {
            if (inventory instanceof Error) {
                logger.error('Error while loading the inventory with product number - ' + inventory.inventory, inventory);
                callback(inventory);
            } else {
                if(order.buyer.nVipaniUser) {
                    inventory.user = order.buyer.nVipaniUser;

                    Inventory.findOne({user:order.buyer.nVipaniUser,product:inventory.product,unitMeasure : inventory.unitMeasure,unitSize:inventory.unitSize}).exec( function (errCompany, product) {
                        if(product) {
                            if(rollback) {
                                product.numberOfUnits = product.numberOfUnits - orderProduct.numberOfUnits;
                                product.buyOfferUnits=product.buyOfferUnits+orderProduct.numberOfUnits;
                            }else{
                                product.numberOfUnits = product.numberOfUnits + orderProduct.numberOfUnits;
                                if(order.offer && order.offer.offerType==='Buy') {
                                    product.buyOfferUnits = product.buyOfferUnits - orderProduct.numberOfUnits;
                                }
                            }
                            //*if(inventory.unitPrice < product.unitPrice)
                            product.MRP = product.MRP;
                            product.numberOfUnitsHistory.push({changeType: rollback?'OrderReturn':'OrderDelivered', changeUnits: orderProduct.numberOfUnits});
                            if(product.source.orders)
                                product.source.orders.push({order:order._id,count:orderProduct.numberOfUnits});
                            else {
                                product.source.orders = [];
                                product.source.orders.push({order: order._id, count: orderProduct.numberOfUnits});
                            }
                            product.updateHistory.push({modifiedOn: Date.now(), modifiedBy: loginuser});


                            if(!order.offer || order.offer && order.offer.offerType==='Sell' ){

                                product.moqAndBuyPrice.push({
                                    MOQ: orderProduct.numberOfUnits,
                                    margin:orderProduct.margin,
                                    price: orderProduct.unitMrpPrice*(1-(orderProduct.margin)/100)
                                });
                            }
                            else if (order.offer && order.offer.offerType==='Buy'){
                                product.moqAndSalePrice .push({
                                    MOQ: orderProduct.numberOfUnits,
                                    margin:orderProduct.margin,
                                    price: orderProduct.unitMrpPrice*(1-(orderProduct.margin)/100)
                                });
                                //inventory.moqAndBuyPrice=[];
                            }
                            product.set('lastUpdated', Date.now());
                            product.save(function (inventorySaveErr) {
                                if (inventorySaveErr) {
                                    logger.error('Error while updating the inventory with product number ' + product.productNum, inventorySaveErr);
                                    callback(inventorySaveErr);
                                } else {
                                    /*logger.debug('Successfully updated the inventory offer product count for the inventory with product number ' + product.productNum + ', count ' + product.numberOfUnitsAvailable + ' for the  ' + changeType + ' of the offer with offer ' + offer.name);
                                     */
                                    callback();
                                }
                            });
                        }else {
                            if(!rollback){
                                inventory._id = mongoose.Types.ObjectId();
                                inventory.isNew = true;
                                inventory.productNum = orderProduct.inventory.productNum;
                                /*	inventory.product=inventory.product;*/
                                inventory.numberOfUnits = orderProduct.numberOfUnits;
                                inventory.updateHistory = [];
                                inventory.updateHistory.push({modifiedOn: Date.now(), modifiedBy: loginuser});
                                inventory.offerUnits = 0;
                                inventory.buyOfferUnits = 0;
                                inventory.maxUnits=0;
                                /*inventory.moqAndBuyPrice=orderProduct.moqAndSalePrice;
                                inventory.moqAndSalePrice=orderProduct.moqAndSalePrice;*/
                                inventory.source.orders=[];

                                inventory.source.orders.push({order: order._id, count: orderProduct.numberOfUnits});

                                if(!order.offer || order.offer && order.offer.offerType==='Sell' ){
                                    inventory.moqAndBuyPrice = [{
                                        MOQ: orderProduct.numberOfUnits,
                                        margin:orderProduct.margin,
                                        price: orderProduct.unitMrpPrice*(1-(orderProduct.margin)/100)
                                    }];
                                    inventory.moqAndSalePrice=[];
                                }
                                else if (order.offer && order.offer.offerType==='Buy'){
                                    inventory.moqAndSalePrice = [{
                                        MOQ: orderProduct.numberOfUnits,
                                        margin:orderProduct.margin,
                                        price: orderProduct.unitMrpPrice*(1-(orderProduct.margin)/100)
                                    }];
                                    inventory.moqAndBuyPrice=[];
                                }
                                inventory.set('lastUpdated', Date.now());
                                inventory.MRP = inventory.MRP;
                                inventory.save(function (inventorySaveErr) {
                                    if (inventorySaveErr) {
                                        logger.error('Error while updating the inventory with product number ' + inventory.productNum, inventorySaveErr);
                                        callback(inventorySaveErr);
                                    } else {
                                        User.findOne({'_id': order.buyer.nVipaniUser}, '-salt -password', function (err, user) {
                                            if (err) {
                                                callback(err);
                                            }
                                            if (user) {
                                                Companies.findOne({'_id': user.company}, function (err, company) {
                                                    company.inventories.push({inventory: inventory._id});
                                                    company.save(function (companyError) {
                                                        if (companyError) {
                                                            logger.error('Error while updating the inventory with product number ' + product.productNum, companyError);
                                                            callback(companyError);
                                                        } else {
                                                            /*logger.debug('Successfully updated the inventory offer product count for the inventory with product number ' + product.productNum + ', count ' + product.numberOfUnitsAvailable + ' for the  ' + changeType + ' of the offer with offer ' + offer.name);
                                                             */
                                                            callback();
                                                        }
                                                    });
                                                });
                                            }
                                        });
                                    }
                                });
                            }else {
                                callback();
                            }
                        }
                    });

                }else{
                    inventory.productNum=orderProduct.inventory.productNum;
                    inventory._id = mongoose.Types.ObjectId();
                    inventory.isNew = true;
                    inventory.numberOfUnits = orderProduct.numberOfUnits;

                    inventory.updateHistory = [];
                    inventory.updateHistory.push({modifiedOn: Date.now(), modifiedBy: loginuser});
                    inventory.offerUnits=0;
                    inventory.buyOfferUnits=0;
                    inventory.maxUnits=0;
                    if(!order.offer || order.offer && order.offer.offerType==='Sell' ){
                        inventory.moqAndBuyPrice = [{
                            MOQ: orderProduct.numberOfUnits,
                            price: commonUtil.getQuantityPrice(orderProduct.inventory.moqAndSalePrice, orderProduct.numberOfUnits, orderProduct.inventory.unitPrice)
                        }];
                        inventory.moqAndSalePrice=[];
                    }
                    else if (order.offer && order.offer.offerType==='Buy'){
                        inventory.moqAndSalePrice = [{
                            MOQ: orderProduct.numberOfUnits,
                            price: commonUtil.getQuantityPrice(orderProduct.inventory.moqAndSalePrice, orderProduct.numberOfUnits, orderProduct.inventory.unitPrice)
                        }];
                        inventory.moqAndBuyPrice=[];
                    }
                    inventory.MRP = orderProduct.unitPrice;
                    inventory.save(function (inventorySaveErr) {
                        if (inventorySaveErr) {
                            logger.error('Error while updating the inventory with product number ' + inventory.productNum, inventorySaveErr);
                            callback(inventorySaveErr);
                        } else {
                            logger.debug('Successfully updated the inventory offer product count for the inventory with product number ' + inventory.productNum + ', count ' + orderProduct.numberOfUnits + ' for the order deliver with order number ' + order.orderNumber);
                            //callback();
                            User.findOne({'_id': order.buyer.nVipaniUser}, '-salt -password', function (err, user) {
                                if (err) {
                                    callback(err);
                                }
                                if(user){
                                    Companies.findOne({'_id': user.company._id}, function (err, company) {
                                        company.inventories.push({inventory:inventory._id});
                                        company.save(function (companyError) {
                                            if (companyError) {
                                                logger.error('Error while updating the inventory with company number ' + user.company._id, companyError);
                                                callback(companyError);
                                            } else {
                                                /*logger.debug('Successfully updated the inventory offer product count for the inventory with product number ' + product.productNum + ', count ' + product.numberOfUnitsAvailable + ' for the  ' + changeType + ' of the offer with offer ' + offer.name);
                                                 */
                                                callback();
                                            }
                                        });
                                    });
                                }
                            });
                        }
                    });
                }
            }
        });


    }, function (err) {
        if (err) {
            done(err);
        } else {
            done();
        }
    });
}

/**
 *
 */

function fetchSellerProductInventory(offerProduct, offer,loginuser,order,done){
    if((offer && offer.user.toString() === loginuser._id.toString() && order.seller && order.seller.nVipaniUser  && offer.offerType==='Buy') || (loginuser.nVipaniUser)) {

        var query={
            user: order.seller.nVipaniUser,
            product: offerProduct.inventory.product,
            unitMeasure: offerProduct.inventory.unitMeasure,
            unitSize: offerProduct.inventory.unitSize,
            numberOfUnits: {$gt: offerProduct.MOQ}
        };
        inventoriesUtil.findQueryByInventories([query],0,false,null,function (product) {
            /*if(product instanceof Error){

            }else{*/
            done(product);
            /*  }*/

        });

    }else {
        done(offerProduct);
    }

}
/**
 *
 */
function updateOrderProducts(order,loginuser,done){
    if(order.offer && order.offer._id) {
        offerUtil.fetchOfferById(order.offer._id,1,function (offer) {
            if (offer instanceof Error) {
                done(offer);
            } else {
                async.each(offer.products, function (eachOfferProduct, callback) {

                    fetchSellerProductInventory(eachOfferProduct, offer, loginuser, order, function (offerProduct) {

                        if (offerProduct instanceof Error) {
                            callback(offerProduct);
                        } else {
                            if ((offer.offerType === 'Sell' && offerProduct) || (offer.offerType === 'Buy' && offerProduct.length > 0)) {
                                /*     order.products.push({
                                         inventory: offer.offerType === 'Buy' ? offerProduct[0]._id : offerProduct.inventory._id,
                                         numberOfUnits:eachOfferProduct.MOQ,
                                         numberOfUnitsAvailable:eachOfferProduct.numberOfUnitsAvailable,
                                         unitPrice: offer.offerType === 'Buy' ? offerProduct[0].unitPrice : offerProduct.unitIndicativePrice,
                                         MOQ: eachOfferProduct.MOQ
                                     });*/


                                order.products.push({
                                    inventory: offer.offerType === 'Buy' ? offerProduct[0]._id : offerProduct.inventory._id,
                                    numberOfUnits: eachOfferProduct.MOQ,
                                    numberOfUnitsAvailable: eachOfferProduct.inventory.numberOfUnits,
                                    unitPrice: commonUtil.getQuantityPrice(getMoqPrice(eachOfferProduct,(offer && offer.offerType === 'Buy')),eachOfferProduct.MOQ, offerProduct.inventory.unitPrice),
                                    MOQ: eachOfferProduct.inventory.maxUnits
                                });


                                order.totalAmount += ((eachOfferProduct.MOQ) * commonUtil.getQuantityPrice(getMoqPrice(eachOfferProduct,(offer && offer.offerType === 'Buy')),eachOfferProduct.MOQ, offerProduct.inventory.unitPrice));
                                callback();
                            } else {
                                logger.error('Creating order notification for status change to ' + order.currentStatus + ' for order with Order number ' + order.orderNumber);
                                callback();
                            }
                        }

                    });
                }, function (err) {
                    if (err) {
                        done(err);
                    } else {
                        done(order);
                    }
                });
            }
        });
    }else{
        done(order);
        /* async.each(order.products, function (eachOfferProduct, callback) {

             fetchSellerProductInventory(eachOfferProduct, order.offer, loginuser, order,function (offerProduct) {

                 if (offerProduct instanceof  Error) {
                     callback(offerProduct);
                 } else {
                     if ((order.orderType ==='Purchase')) {
                         order.products.push({
                             inventory:  offerProduct._id,
                             numberOfUnits:eachOfferProduct.numberOfUnits,
                             numberOfUnitsAvailable: eachOfferProduct.numberOfUnits,
                             unitPrice: (order.offer && order.offer.offerType === 'Buy') ?  commonUtil.getQuantityPrice(offerProduct.inventory.moqAndBuyPrice,eachOfferProduct.numberOfUnits,eachOfferProduct.unitPrice) : commonUtil.getQuantityPrice(offerProduct.inventory.moqAndSalePrice,eachOfferProduct.numberOfUnits,offerProduct.inventory.unitPrice),
                             MOQ: eachOfferProduct.inventory.maxUnits
                     });


                         order.totalAmount += ((eachOfferProduct.MOQ) * (order.offer && order.offer.offerType === 'Buy') ?  commonUtil.getQuantityPrice(eachOfferProduct.inventory.moqAndBuyPrice,eachOfferProduct.numberOfUnits,eachOfferProduct.unitPrice) : commonUtil.getQuantityPrice(offerProduct.inventory.moqAndSalePrice,eachOfferProduct.numberOfUnits,offerProduct.inventory.unitPrice));
                         callback();
                     }else{
                         logger.error('Creating order notification for status change to ' + order.currentStatus + ' for order with Order number ' + order.orderNumber);
                         callback();
                     }
                 }

             });
         }, function (err) {
             if (err) {
                 done(err);
             } else {
                 done(order);
             }
         });*/
    }
}
function displayName(user){
    return user.displayName ?user.displayName:user.username;
}
function notificationTargetFields(notificationTarget,player) {
    if(player.contact) {
        notificationTarget.contact = player.contact;
        if (player.nVipaniUser)
            notificationTarget.nVipaniUser = player.nVipaniUser;
        if (player.nVipaniCompany)
            notificationTarget.nVipaniCompany = player.nVipaniCompany;
        if (player.businessUnit)
            notificationTarget.businessUnit = player.businessUnit;
    }
    return notificationTarget;
}
/*

 */
function orderNotifications(res, order, oldStatus, loginuser, done) {

    logger.info('Creating order notification for status change to ' + order.currentStatus + ' for order with Order number ' + order.orderNumber);

    var todo;
    var todoUpdate;

    var notification = new Notification();
    notification.user = loginuser;

    notification.source.order = order;

    notification.type = 'Order';

    notification.channel = ['Email', 'SMS', 'Push'];

    if (order.currentStatus === 'Placed') {
        if (order.orderType === 'Purchase') {
            notification.target=notificationTargetFields(notification.target,order.seller);
            notification.title = 'Buyer ' + loginuser.displayName + ' ' + order.currentStatus.toLowerCase() + ' the order with order number ' + order.orderNumber;
            notification.shortMessage = 'Buyer ' + loginuser.displayName + ' ' + order.currentStatus.toLowerCase() + ' the order with order number ' + order.orderNumber + '. From nVipani Team';
        } else if (order.orderType === 'Sale') {
            notification.target=notificationTargetFields(notification.target,order.buyer);
            notification.title = 'Seller ' + displayName(loginuser)+ ' has Placed order with order number ' + order.orderNumber + ' on behalf you (Buyer)';
            notification.shortMessage = 'Seller ' + displayName(loginuser)+ ' has Placed order with order number ' + order.orderNumber + ' on behalf you (Buyer). From nVipani Team';
        }

        todo = new Todo();
        todo.source.order = order;
        todo.title = 'Order with order number ' + order.orderNumber + ' needs to be confirmed';
        todo.type = 'OrderConfirmation';
        todo.target = order.seller.nVipaniUser;
    } else if (order.currentStatus === 'InProgress') {
        notification.target=notificationTargetFields(notification.target,order.buyer);
        //notification.title = 'The order with order number ' + order.orderNumber + ' is being prepared for shipping by seller - ' + loginuser.displayName;

        notification.title = 'Seller ' + displayName(loginuser)+ ' is preparing the order with order number ' + order.orderNumber + ' for shipping';
        notification.shortMessage = 'Seller ' + displayName(loginuser)+ ' is preparing the order with order number ' + order.orderNumber + ' for shipping. From nVipani Team';
    } else if (order.currentStatus === 'Shipped') {
        notification.target=notificationTargetFields(notification.target,order.buyer);
        //notification.title = 'The order with order number ' + order.orderNumber + ' is shipped by seller - ' + loginuser.displayName;

        notification.title = 'Seller ' + displayName(loginuser)+ ' shipped the order with order number ' + order.orderNumber;
        notification.shortMessage = 'Seller ' + displayName(loginuser)+ ' shipped the order with order number ' + order.orderNumber + '. From nVipani Team';

        todoUpdate = new Todo();
        todoUpdate.source.order = order;
        todoUpdate.type = 'OrderShipment';
        todoUpdate.target = order.seller.nVipaniUser;
    } else if (order.currentStatus === 'Delivered') {
        if (order.buyer.nVipaniUser && notification.user.toString() === order.buyer.nVipaniUser.toString()) {
            notification.target=notificationTargetFields(notification.target,order.seller);
        } else {
            notification.target=notificationTargetFields(notification.target,order.buyer);
        }
        notification.title = 'The order with order number ' + order.orderNumber + ' is delivered to shipping address';
        notification.shortMessage = 'The order with order number ' + order.orderNumber + ' is delivered to shipping address. From nVipani Team';
    } else if (oldStatus === 'Placed' && (order.currentStatus === 'Cancelled' || order.currentStatus === 'Rejected') && order.seller.nVipaniCompany&& order.seller.nVipaniCompany._id && order.seller.nVipaniCompany._id.toString() === loginuser.company.toString()) {
        notification.target=notificationTargetFields(notification.target,order.buyer);

        notification.title = 'The order with order number ' + order.orderNumber + ' is ' + order.currentStatus + ' by Seller';
        notification.shortMessage = 'The order with order number ' + order.orderNumber + ' is ' + order.currentStatus + ' by Seller. From nVipani Team';

        todoUpdate = new Todo();
        todoUpdate.source.order = order;
        todoUpdate.type = 'OrderConfirmation';
        todoUpdate.target = order.seller.nVipaniUser;

    } else if (oldStatus === 'Placed' && (order.currentStatus === 'Cancelled' || order.currentStatus === 'Rejected') && order.buyer.nVipaniCompany && order.buyer.nVipaniCompany._id&& order.buyer.nVipaniCompany._id.toString() === loginuser.company.toString()) {
        notification.target=notificationTargetFields(notification.target,order.seller);
        notification.title = 'The order with order number ' + order.orderNumber + ' is ' + order.currentStatus + ' by Buyer';
        notification.shortMessage = 'The order with order number ' + order.orderNumber + ' is ' + order.currentStatus + ' by Buyer. From nVipani Team';

        todoUpdate = new Todo();
        todoUpdate.source.order = order;
        todoUpdate.type = 'OrderConfirmation';
        /*  if(order.currentStatus==='Disputed'){
         todoUpdate.type = 'OrderDisputed';
         notification.title = 'The order with order number ' + order.orderNumber + ' is ' + order.currentStatus + ' by Buyer';
         notification.shortMessage = 'The order with order number ' + order.orderNumber + ' is ' + order.currentStatus + ' by Buyer. From nVipani Team';
         }*/
        todoUpdate.target = order.seller.nVipaniUser;
    } else if (oldStatus === 'Delivered' && order.currentStatus === 'Disputed' && order.buyer.nVipaniCompany&& order.buyer.nVipaniCompany._id && order.buyer.nVipaniCompany._id.toString() === loginuser.company.toString()) {
        notification.target=notificationTargetFields(notification.target,order.seller);
        todo = new Todo();
        todo.source.order = order;
        todo.type = 'OrderDisputed';
        todo.title = 'Order with order number ' + order.orderNumber + ' needs to be resolved or returned';
        todo.target = order.buyer.nVipaniUser;
        notification.title = 'The order with order number ' + order.orderNumber + ' is ' + order.currentStatus + ' by Buyer';
        notification.shortMessage = 'The order with order number ' + order.orderNumber + ' is ' + order.currentStatus + ' by Buyer. From nVipani Team';
    } else if (oldStatus === 'Disputed' && (order.currentStatus === 'Resolved' || order.currentStatus === 'Returned') && order.buyer.nVipaniCompany && order.buyer.nVipaniCompany&& order.buyer.nVipaniCompany._id.toString() === loginuser.company.toString()) {
        notification.target=notificationTargetFields(notification.target,order.seller);
        todo = new Todo();
        todo.source.order = order;
        todo.type = 'OrderDisputeResolution';
        todo.title = 'Order with order number ' + order.orderNumber + ' needs to be completed';
        todo.target = order.seller.nVipaniUser;

        todoUpdate = new Todo();
        todoUpdate.source.order = order;
        todoUpdate.type = 'OrderDisputed';
        todoUpdate.target = order.buyer.nVipaniUser;
        notification.title = 'The order with order number ' + order.orderNumber + ' is ' + order.currentStatus + ' by Buyer';
        notification.shortMessage = 'The order with order number ' + order.orderNumber + ' is ' + order.currentStatus + ' by Buyer. From nVipani Team';
    } else if ((oldStatus === 'Resolved' || oldStatus === 'Returned') && order.currentStatus === 'Completed' && order.seller.nVipaniCompany&&order.seller.nVipaniCompany._id && order.seller.nVipaniCompany._id.toString() === loginuser.company.toString()) {
        notification.target=notificationTargetFields(notification.target,order.buyer);

        todoUpdate = new Todo();
        todoUpdate.source.order = order;
        todoUpdate.type = 'OrderDisputeResolution';
        todoUpdate.target = order.seller.nVipaniUser;
        notification.title = 'The order with order number ' + order.orderNumber + ' is ' + order.currentStatus + ' by Seller';
        notification.shortMessage = 'The order with order number ' + order.orderNumber + ' is ' + order.currentStatus + ' by Seller. From nVipani Team';
    }
    if (notification.title.length !== 0) {
        notification.save(function (err) {
            if (err) {
                logger.error('Error while creating order notification for status change to ' + order.currentStatus + ' for order with Order number ' + order.orderNumber);
                /*return res.status(400).send({
                 status: false,
                 message: errorHandler.getErrorMessage(err)
                 });*/
                done(err);
            } else if (order.mediator && order.mediator.contact) {
                notificationUtil.processNotification(res, notification, function (processNotificationError) {
                    if (processNotificationError) {
                        logger.error('Error while processing  order notification for status change to ' + order.currentStatus + ' for order with Order number ' + order.orderNumber, processNotificationError);
                        done(processNotificationError);
                    } else {
                        var mediatorNotification = new Notification();
                        mediatorNotification.user = loginuser;
                        mediatorNotification.target=notificationTargetFields(mediatorNotification.target,order.mediator);
                        /*mediatorNotification.target.contact = order.mediator.contact;
                        if (order.mediator.contact && order.mediator.nVipaniUser) {
                            mediatorNotification.target.nVipaniUser = order.mediator.nVipaniUser;
                            mediatorNotification.target.nVipaniCompany = order.mediator.nVipaniCompany;
                        }*/
                        mediatorNotification.source.order = order;
                        mediatorNotification.type = 'Order';

                        mediatorNotification.channel = ['Email', 'SMS', 'Push'];

                        mediatorNotification.title = notification.title;
                        mediatorNotification.shortMessage = notification.shortMessage;
                        mediatorNotification.save(function (err) {
                            if (err) {
                                logger.error('Error while creating order notification for mediator for status change to ' + order.currentStatus + ' for order with Order number ' + order.orderNumber);
                                done(err);
                            } else {
                                notificationUtil.processNotification(res, mediatorNotification, function (processNotificationError) {
                                    if (processNotificationError) {
                                        logger.error('Error while processing  order notification for mediator for status change to ' + order.currentStatus + ' for order with Order number ' + order.orderNumber, processNotificationError);
                                        done(processNotificationError);
                                    } else {
                                        if (todo) {
                                            todo.save(function (err) {
                                                if (err) {
                                                    logger.error('Error while creating order todo for status change to ' + order.currentStatus + ' for order with Order number ' + order.orderNumber);
                                                    /*return res.status(400).send({
                                                     status: false,
                                                     message: errorHandler.getErrorMessage(err)
                                                     });*/
                                                    done(err);
                                                } else {
                                                    if (todoUpdate) {
                                                        Todo.update({
                                                            type: todoUpdate.type,
                                                            'source.order': todoUpdate.source.order,
                                                            target: todoUpdate.target
                                                        }, {completed: true}, function (err, numberAffected, rawResponse) {
                                                            if (err) {
                                                                logger.error('Error while creating order todo for status change to ' + order.currentStatus + ' for order with Order number ' + order.orderNumber);
                                                                done(err);
                                                            } else {
                                                                logger.info('Loading the order details with Order number ' + order.orderNumber + ' after creation');
                                                                done();
                                                            }
                                                        });
                                                    } else {
                                                        logger.info('Loading the order details with Order number ' + order.orderNumber + ' after creation');
                                                        done();
                                                    }
                                                }
                                            });
                                        } else {
                                            if (todoUpdate) {
                                                Todo.update({
                                                    type: todoUpdate.type,
                                                    'source.order': todoUpdate.source.order,
                                                    target: todoUpdate.target
                                                }, {completed: true}, function (err, numberAffected, rawResponse) {
                                                    if (err) {
                                                        logger.error('Error while creating order todo for status change to ' + order.currentStatus + ' for order with Order number ' + order.orderNumber);
                                                        done(err);
                                                    } else {
                                                        logger.info('Loading the order details with Order number ' + order.orderNumber + ' after creation');
                                                        done();
                                                    }
                                                });
                                            } else {
                                                logger.info('Loading the order details with Order number ' + order.orderNumber + ' after creation');
                                                done();
                                            }
                                        }
                                    }
                                });
                            }
                        });
                    }
                });
            } else {
                notificationUtil.processNotification(res, notification, function (processNotificationError) {
                    if (processNotificationError) {
                        logger.error('Error while processing  order notification for status change to ' + order.currentStatus + ' for order with Order number ' + order.orderNumber, processNotificationError);
                        done(processNotificationError);
                    } else {
                        if (todo) {
                            todo.save(function (err) {
                                if (err) {
                                    done(err);
                                } else {
                                    if (todoUpdate) {
                                        Todo.update({
                                            type: todoUpdate.type,
                                            'source.order': todoUpdate.source.order,
                                            target: todoUpdate.target
                                        }, {completed: true}, function (err, numberAffected, rawResponse) {
                                            if (err) {
                                                logger.error('Error while creating order todo for status change to ' + order.currentStatus + ' for order with Order number ' + order.orderNumber);
                                                done(err);
                                            } else {
                                                logger.info('Loading the order details with Order number ' + order.orderNumber + ' after creation');
                                                done();
                                            }
                                        });
                                    } else {
                                        logger.info('Loading the order details with Order number ' + order.orderNumber + ' after creation');
                                        done();
                                    }
                                }
                            });
                        } else {
                            if (todoUpdate) {
                                Todo.update({
                                    type: todoUpdate.type,
                                    'source.order': todoUpdate.source.order,
                                    target: todoUpdate.target
                                }, {completed: true}, function (err, numberAffected, rawResponse) {
                                    if (err) {
                                        logger.error('Error while creating order todo for status change to ' + order.currentStatus + ' for order with Order number ' + order.orderNumber);
                                        done(err);
                                    } else {
                                        logger.info('Loading the order details with Order number ' + order.orderNumber + ' after creation');
                                        done();
                                    }
                                });
                            } else {
                                logger.info('Loading the order details with Order number ' + order.orderNumber + ' after creation');
                                done();
                            }
                        }
                    }
                });
            }
        });
    } else {
        logger.info('Loading the order details with Order number without notification ' + order.orderNumber + ' after update');
        done();
    }
}

/**
 *
 * @param inventory
 * @param order
 * @param sufficientStock
 * @param stockMasterMatchedRecords
 * @param user
 * @param done
 */
function getMatchedBatchRecords(inventory,orderId,orderProduct,units,stockMasterMatchedRecords,user,done){
    var stockMasters=inventory.stockMasters.sort(function(obj1, obj2){
        return new Date(obj1.created).getTime() - new Date(obj2.created).getTime() >0 && (obj2.currentBalance && obj1.currentBalance);
    });

    async.forEachSeries(stockMasters,function (eachStock,callback) {
        if(eachStock.currentBalance >0) {
            if (units - eachStock.currentBalance <= 0) {
                var currentBalance = eachStock.currentBalance;
                eachStock.stockOut.orders.push({
                    order: orderId,
                    count: units,
                    updatedUser: user,
                    updatedDate: new Date()
                });
                eachStock.currentBalance = eachStock.currentBalance - units;
                stockMasterMatchedRecords.push(eachStock);

                if (orderProduct) {
                    orderProduct.stockMasters.push({
                        stockMaster: eachStock._id,
                        businessUnit:eachStock.currentOwner.businessUnit,
                        unitPrice: orderProduct.unitPrice,
                        batchNumber: eachStock.batchNumber,
                        unitMrpPrice: orderProduct.inventory.MRP,
                        numberOfUnits: units
                    });
                }
                units = units - currentBalance;
                done(null, stockMasterMatchedRecords, orderProduct, units);
            } else {
                eachStock.stockOut.orders.push({
                    order: orderId,
                    count: eachStock.currentBalance,
                    updatedUser: user,
                    updatedDate: new Date()
                });
                if (orderProduct) {
                    orderProduct.stockMasters.push({
                        stockMaster: eachStock._id,
                        unitPrice: orderProduct.unitPrice,
                        batchNumber: eachStock.batchNumber,
                        businessUnit:eachStock.currentOwner.businessUnit,
                        unitMrpPrice: orderProduct.inventory.MRP,
                        numberOfUnits: eachStock.currentBalance
                    });
                }

                units = units - eachStock.currentBalance;
                eachStock.currentBalance = 0;
                stockMasterMatchedRecords.push(eachStock);
                callback();
            }
        }else{
            callback();
        }
    },function (err) {
        if(err){
            done(err,null,orderId);
        }else{
            done(null,stockMasterMatchedRecords,orderProduct,units);
        }
    });
}

/***
 *
 * @param stockMaster
 * @param done
 */
function saveStockMasters(stockMaster,done){

}

/**
 *
 * @param inventory
 * @param done
 */
function saveUserItemMaster(inventory,done){

}

/***
 *  Update stock master in case of order confirmation.
 * @param order
 * @param loginuser
 * @param done
 */
function updateStockMasterAndInventory(order, loginuser, isStockMaster,done) {
    if(order && order.currentStatus==='Confirmed') {
        var inventoryIdVsNumUnits=[];
        async.forEachSeries(order.products, function (orderProduct, callback) {
            inventoriesUtil.findInventoryById(orderProduct.inventory._id,isStockMaster?1:-1,function (inventory) {
                if (inventory instanceof Error) {
                    logger.error('Error while loading the inventory with product number - ' + orderProduct.inventory._id, inventory);
                    callback(inventory);
                } else {
                    if(inventory.numberOfUnits >= orderProduct.numberOfUnits) {
                        if (isStockMaster) {
                            var stockMasterMatchedRecords = [];
                            var orderProductIndex=order.products.indexOf(orderProduct);
                            getMatchedBatchRecords(inventory, order._id, orderProduct, orderProduct.numberOfUnits,stockMasterMatchedRecords, loginuser, function (stockErr, stockData, updateProduct,units) {
                                if(stockErr){
                                    logger.error('Error while fetching  matched stock records for the productName'+ inventory.productCategory.name +' Error :'+errorHandler.getErrorMessage(stockErr));
                                    callback(stockErr);
                                }else {
                                    if (units <= 0) {
                                        /*inventory.numberOfUnits = inventory.numberOfUnits - orderProduct.numberOfUnits;*/
                                        inventoryIdVsNumUnits.push({
                                            inventory: {inventory: inventory, stockMasters: stockData},
                                            numUnits: orderProduct.numberOfUnits
                                        });
                                        order.products[orderProductIndex]=updateProduct;
                                        callback();
                                    } else {
                                        callback(new Error('No Sufficient stock for product :' + inventory.productName +'. Required Stock :'+orderProduct.numberOfUnits +' but current Stock is :'+inventory.numberOfUnits));
                                    }
                                }
                            });
                        } else {
                            inventory.numberOfUnits = inventory.numberOfUnits - orderProduct.numberOfUnits;
                            inventoryIdVsNumUnits.push({
                                inventory: {inventory: inventory},
                                numUnits: orderProduct.numberOfUnits
                            });
                            callback();
                        }
                    }else{
                        callback(new Error('No Sufficient stock for product :"'+inventory.productName+'". Required Stock :'+orderProduct.numberOfUnits +' but current Stock is :'+inventory.numberOfUnits));
                    }
                }

            });
        }, function (err) {
            if(err){
                done(err,null);
            }else{
                async.each(inventoryIdVsNumUnits, function (idVsUnits, inventoryCallBack) {
                    var eachInventory=idVsUnits.inventory;
                    /*var inventory= new Inventory(idVsUnits.inventory);*/
                    /* inventory.unitOfMeasure=idVsUnits.inventory.inventory._doc.unitOfMeasure.toString();*/
                    inventoriesUtil.findInventoryById(eachInventory.inventory._id,-1,function (inventory) {
                        inventory.numberOfUnits=eachInventory.inventory.numberOfUnits-idVsUnits.numUnits;
                        inventory.save(function (inventorySaveErr) {
                            if (inventorySaveErr) {
                                logger.error('Error while updating the inventory with product name - ' + eachInventory.productNum, inventorySaveErr);
                                inventoryCallBack(inventorySaveErr);
                            } else {
                                logger.debug('Successfully updated the inventory count for the inventory with product name -' + inventory.productName + ', count -' + idVsUnits.numUnits + ' for the order confirmation of order with order number -' + order.orderNumber);
                                /* inventoryCallBack();*/
                                if(eachInventory.stockMasters && eachInventory.stockMasters.length>0) {
                                    async.forEachSeries(eachInventory.stockMasters, function (stock, stockCallback) {
                                        stock.save(function (eachStockError) {
                                            if (eachStockError) {
                                                logger.error('Error while updating the inventory with product name - ' + eachInventory.productName, inventorySaveErr);
                                                stockCallback(eachStockError);
                                            } else {
                                                logger.debug('Successfully updated the inventory count for the inventory with product name -' + inventory.productName + ', count -' + idVsUnits.numUnits + ' for the order confirmation of order with order number -' + order.orderNumber);
                                                stockCallback();
                                            }

                                        });
                                    }, function (stockErr) {
                                        if (stockErr) {
                                            inventoryCallBack(stockErr);
                                        } else {
                                            inventoryCallBack();
                                        }

                                    });
                                }else{
                                    inventoryCallBack();
                                }
                            }
                        });
                    });
                }, function (inventoryUpdatErr) {
                    if (inventoryUpdatErr) {
                        done(inventoryUpdatErr,null);
                    } else {
                        done(null,order);
                    }
                });
            }
        });
    }else{
        done(new Error('Order is not confirmed'));
    }
}
/**
 *Update the product and inventory and offer count after order confirmation.
 */
function updateOfferAndInventoryProductCount(order, loginuser, done) {
    logger.info('Updating offer product count after order confirmation of Order number - ' + order.orderNumber);

    if (false) {
        offerUtil.fetchOfferById(order.offer._id,1,function (offer) {
            if (offer instanceof Error) {
                logger.error('Error while loading the offer with offer number - ' + offer.offerNumber, offer);
                done(offer);
            } else {
                if (offer.offerStatus && offer.offerStatus === 'Placed') {
                    var stockAvailable = false;
                    var index = 0;
                    var  inventoryIdVsNumUnits = [];
                    var productIndex=0;
                    async.each(order.products, function (orderProduct, callback) {
                        var offerProduct = offer.products.filter(function (product) {

                            return ((offer.offerType === 'Sell' && orderProduct.inventory._id && product.inventory._id.toString() === orderProduct.inventory._id.toString()) || (offer.offerType === 'Buy' && orderProduct.inventory.product._id === orderProduct.inventory.product._id));

                        });
                        if(offerProduct.length >0 && order.products[productIndex].inventory._id.toString()===offerProduct[0].inventory._id.toString()){
                            order.products[productIndex].unitPrice=offerProduct[0].inventory.unitPrice;
                        }
                        if (offerProduct && offerProduct.length > 0) {
                            if (offerProduct[0].numberOfUnitsAvailable >= orderProduct.numberOfUnits) {
                                logger.debug('Updating the offer available count for the inventory number :' + orderProduct.inventory.productNum + ' for confirming the order with order number :' + order.orderNumber + '. Offer product available units (' + offerProduct[0].numberOfUnitsAvailable + '), order product count units (' + orderProduct.numberOfUnits + ')');
                                offerProduct[0].numberOfOfferUnitsHistory.push({
                                    order: order._id,
                                    changeType: 'OrderConfirmation',
                                    changeUnits: orderProduct.numberOfUnits
                                });
                                //offerProduct[0].numberOfUnitsAvailable = offerProduct[0].numberOfUnitsAvailable - orderProduct.numberOfUnits;
                                inventoryIdVsNumUnits.push({
                                    inventoryid: offerProduct[0].inventory._id,
                                    numUnits: orderProduct.numberOfUnits
                                });
                                // offer.products[index] = offerProduct[0];
                                index++;
                                if (!stockAvailable && offerProduct[0].numberOfUnitsAvailable !== 0) {
                                    stockAvailable = true;
                                }
                                callback();
                            } else {
                                logger.error('Not enough number of offer products are available for the offer product with inventory number :' + orderProduct.inventory.productNum + ', available offer products units:' + offerProduct[0].numberOfUnitsAvailable + ' and required order products units:' + orderProduct.numberOfUnits);
                                callback(new Error('Not enough number of offer products are available for the offer product with inventory number - ' + orderProduct.inventory.productNum + ', available offer products units-' + offerProduct[0].numberOfUnitsAvailable + ' and required order products units:' + orderProduct.numberOfUnits));
                            }
                        } else {
                            logger.error('No matching offer product to update the offer product units of the offer product with inventory number :' + orderProduct.inventory.productNum);
                            callback(new Error('No matching offer product to update the offer product units of the offer product with inventory number :' + orderProduct.inventory.productNum));
                        }
                        productIndex++;
                    }, function (err) {
                        if (err) {
                            done(err);
                        } else {
                            offer.updateHistory.push({modifiedOn: Date.now(), modifiedBy: loginuser._id});
                            if (!stockAvailable) {
                                offer.offerStatus = 'Completed';
                            }
                            offer.save(function (offerSaveErr) {
                                if (offerSaveErr) {
                                    logger.error('Error while updating the offer with offer number - ' + offer.offerNumber, offerSaveErr);
                                    done(offerSaveErr);
                                } else {
                                    async.each(inventoryIdVsNumUnits, function (idVsUnits, inventoryCallBack) {
                                        inventoriesUtil.findInventoryById(idVsUnits.inventoryid,-1,function (inventory) {
                                            if (inventory instanceof Error) {
                                                logger.error('Error while loading the inventory with product number - ' + idVsUnits.inventoryid, inventory);
                                                inventoryCallBack(inventory);
                                            } else {
                                                inventory.numberOfUnitsHistory.push({
                                                    order: order._id,
                                                    changeType: 'OrderConfirmation',
                                                    changeUnits: idVsUnits.numUnits
                                                });
                                                inventory.numberOfUnits = inventory.numberOfUnits - idVsUnits.numUnits;
                                                if (offer.offerType === 'Sell') {
                                                    inventory.offerUnitsHistory.push({
                                                        offer: offer._id,
                                                        changeType: 'OfferUpdation',
                                                        changeUnits: -idVsUnits.numUnits
                                                    });
                                                    inventory.offerUnits = inventory.offerUnits - idVsUnits.numUnits;
                                                    // Needs to update the offer units also, so that numberOfUnits - offerUnits will be used for next offer creation
                                                }
                                                if (offer.offerType === 'Buy') {
                                                    inventory.buyOfferUnitsHistory.push({
                                                        offer: offer._id,
                                                        changeType: 'OfferUpdation',
                                                        changeUnits: -idVsUnits.numUnits
                                                    });
                                                    inventory.buyOfferUnits = inventory.buyOfferUnits - idVsUnits.numUnits;
                                                    // Needs to update the offer units also, so that numberOfUnits - offerUnits will be used for next offer creation
                                                }
                                                inventory.updateHistory.push({
                                                    modifiedOn: Date.now(),
                                                    modifiedBy: loginuser._id
                                                });
                                                inventory.save(function (inventorySaveErr) {
                                                    if (inventorySaveErr) {
                                                        logger.error('Error while updating the inventory with product number - ' + inventory.productNum, inventorySaveErr);
                                                        inventoryCallBack(inventorySaveErr);
                                                    } else {
                                                        logger.debug('Successfully updated the inventory count for the inventory with product number -' + inventory.productNum + ', count -' + idVsUnits.numUnits + ' for the order confirmation of order with order number -' + order.orderNumber);
                                                        inventoryCallBack();
                                                    }
                                                });
                                            }
                                        });
                                    }, function (inventoryUpdatErr) {
                                        if (inventoryUpdatErr) {
                                            done(inventoryUpdatErr);
                                        } else {
                                            done();
                                        }
                                    });
                                }
                            });
                        }
                    });
                } else {
                    done(new Error('Offer already in - ' + offer.offerStatus + ' state.'));
                }
            }
        });
    } else {
// Just deduct from the inventory
        if(order.currentStatus==='Confirmed') {
            async.each(order.products, function (orderProduct, callback) {
                /*  Inventory.findById(orderProduct.inventory._id).exec(function (inventoryError, inventory) {*/
                inventoriesUtil.findInventoryById(orderProduct.inventory._id,-1,function (inventory) {
                    if (inventory instanceof Error) {
                        logger.error('Error while loading the inventory with product number - ' + orderProduct.inventory.productNum, inventory);
                        callback(inventory);
                    } else {
                        if(inventory.disabled){
                            logger.debug('Skipped disabled inventory count for the inventory with product number -' + inventory.productNum + ', count -' + orderProduct.numberOfUnits + ' for the order confirmation of order with order number -' + order.orderNumber);
                            callback();
                        }else {
                            if (inventory.numberOfUnits - inventory.offerUnits < orderProduct.numberOfUnits) {
                                logger.error('Not enough quantity available for the inventory product with product number - ' + inventory.productNum);
                                callback(new Error('Not enough quantity available for the inventory product with product number - ' + inventory.productNum));
                            } else {
                                inventory.numberOfUnitsHistory.push({
                                    order: order._id,
                                    changeType: 'OrderConfirmation',
                                    changeUnits: orderProduct.numberOfUnits
                                });
                                inventory.numberOfUnits = inventory.numberOfUnits - orderProduct.numberOfUnits;

                                inventory.updateHistory.push({
                                    modifiedOn: Date.now(),
                                    modifiedBy: loginuser._id
                                });
                                inventory.save(function (inventorySaveErr) {
                                    if (inventorySaveErr) {
                                        logger.error('Error while updating the inventory with product number - ' + inventory.productNum, inventorySaveErr);
                                        callback(inventorySaveErr);
                                    } else {
                                        logger.debug('Successfully updated the inventory count for the inventory with product number -' + inventory.productNum + ', count -' + orderProduct.numberOfUnits + ' for the order confirmation of order with order number -' + order.orderNumber);
                                        callback();
                                    }
                                });
                            }
                        }
                    }
                });
            }, function (err) {
                done(err);
            });
        }else{
            done();
        }
    }

}
/*
 updateOrderRollbackInventoryOfferCount
 */
function updateOrderRollbackInventoryOfferCount(order, loginuser, done) {
    logger.info('Updating offer product count after order confirmation of Order number - ' + order.orderNumber);

    if (false) {
        offerUtil.fetchOfferById(order.offer._id,1,function (offer) {
            if (offer instanceof Error) {
                logger.error('Error while loading the offer with offer number - ' + order.offer._id, offer);
                done(offer);
            } else {
                if (offer.offerStatus && offer.offerStatus === 'Placed') {
                    var stockAvailable = false;
                    var index = 0;
                    var inventoryIdVsNumUnits = [];

                    async.each(order.products, function (orderProduct, callback) {
                        var offerProduct = offer.products.filter(function (product) {
                            if (offer.offerType === 'Sell')
                                return ((orderProduct.inventory._id && product.inventory.toString() === orderProduct.inventory._id.toString()) || (product.inventory.toString() === orderProduct.inventory.toString()));
                            if (offer.offerType === 'Buy') {
                                //TODO: Please verify this always returns true
                                return ((orderProduct.inventory.product._id === orderProduct.inventory.product._id));
                            }

                        });
                        if (offerProduct && offerProduct.length > 0) {
                            /*  if (offerProduct[0].numberOfUnitsAvailable >= orderProduct.numberOfUnits) {*/
                            logger.debug('Updating the offer available count for the inventory number :' + orderProduct.inventory.productNum + ' for confirming the order with order number :' + order.orderNumber + '. Offer product available units (' + offerProduct[0].numberOfUnitsAvailable + '), order product count units (' + orderProduct.numberOfUnits + ')');
                            offerProduct[0].numberOfOfferUnitsHistory.push({
                                order: order._id,
                                changeType: 'OrderReturn',
                                changeUnits: orderProduct.numberOfUnits
                            });
                            offerProduct[0].numberOfUnitsAvailable = offerProduct[0].numberOfUnitsAvailable +orderProduct.numberOfUnits;
                            inventoryIdVsNumUnits.push({
                                inventoryid: offerProduct[0].inventory._id,
                                numUnits: orderProduct.numberOfUnits
                            });
                            offer.products[index] = offerProduct[0];
                            index++;
                            if (!stockAvailable && offerProduct[0].numberOfUnitsAvailable !== 0) {
                                stockAvailable = true;
                            }
                            callback();
                            /*} else {
                             logger.error('Not enough number of offer products are available for the offer product with inventory number :' + orderProduct.inventory.productNum + ', available offer products units:' + offerProduct[0].numberOfUnitsAvailable + ' and required order products units:' + orderProduct.numberOfUnits);
                             callback(new Error('Not enough number of offer products are available for the offer product with inventory number - ' + orderProduct.inventory.productNum + ', available offer products units-' + offerProduct[0].numberOfUnitsAvailable + ' and required order products units:' + orderProduct.numberOfUnits));
                             }*/
                        } else {
                            logger.error('No matching offer product to update the offer product units of the offer product with inventory number :' + orderProduct.inventory.productNum);
                            callback(new Error('No matching offer product to update the offer product units of the offer product with inventory number :' + orderProduct.inventory.productNum));
                        }
                    }, function (err) {
                        if (err) {
                            done(err);
                        } else {
                            offer.updateHistory.push({modifiedOn: Date.now(), modifiedBy: loginuser._id});
                            if (!stockAvailable) {
                                offer.offerStatus = 'Completed';
                            }
                            offer.save(function (offerSaveErr) {
                                if (offerSaveErr) {
                                    logger.error('Error while updating the offer with offer number - ' + offer.offerNumber, offerSaveErr);
                                    done(offerSaveErr);
                                } else {
                                    async.each(inventoryIdVsNumUnits, function (idVsUnits, inventoryCallBack) {
                                        /* Inventory.findById(idVsUnits.inventoryid).exec(function (inventoryError, inventory) {*/
                                        inventoriesUtil.findInventoryById(idVsUnits.inventoryid,-1,function (inventory) {
                                            if (inventory instanceof Error) {
                                                logger.error('Error while loading the inventory with product number - ' + idVsUnits.inventoryid, inventory);
                                                inventoryCallBack(inventory);
                                            } else {
                                                inventory.numberOfUnitsHistory.push({
                                                    order: order._id,
                                                    changeType: 'OrderReturn',
                                                    changeUnits: idVsUnits.numUnits
                                                });
                                                inventory.numberOfUnits = inventory.numberOfUnits + idVsUnits.numUnits;
                                                if (offer.offerType === 'Sell') {
                                                    inventory.offerUnitsHistory.push({
                                                        offer: offer._id,
                                                        changeType: 'OfferUpdation',
                                                        changeUnits: idVsUnits.numUnits
                                                    });
                                                    inventory.offerUnits = inventory.offerUnits + idVsUnits.numUnits;
                                                    // Needs to update the offer units also, so that numberOfUnits - offerUnits will be used for next offer creation
                                                }
                                                if (offer.offerType === 'Buy') {
                                                    inventory.buyOfferUnitsHistory.push({
                                                        offer: offer._id,
                                                        changeType: 'OfferUpdation',
                                                        changeUnits: idVsUnits.numUnits
                                                    });
                                                    inventory.buyOfferUnits = inventory.buyOfferUnits + idVsUnits.numUnits;
                                                    // Needs to update the offer units also, so that numberOfUnits - offerUnits will be used for next offer creation
                                                }
                                                inventory.updateHistory.push({
                                                    modifiedOn: Date.now(),
                                                    modifiedBy: loginuser._id
                                                });
                                                inventory.save(function (inventorySaveErr) {
                                                    if (inventorySaveErr) {
                                                        logger.error('Error while updating the inventory with product number - ' + inventory.productNum, inventorySaveErr);
                                                        inventoryCallBack(inventorySaveErr);
                                                    } else {
                                                        logger.debug('Successfully updated the inventory count for the inventory with product number -' + inventory.productNum + ', count -' + idVsUnits.numUnits + ' for the order confirmation of order with order number -' + order.orderNumber);
                                                        inventoryCallBack();
                                                    }
                                                });
                                            }
                                        });
                                    }, function (inventoryUpdatErr) {
                                        if (inventoryUpdatErr) {
                                            done(inventoryUpdatErr);
                                        } else {
                                            done();
                                        }
                                    });
                                }
                            });
                        }
                    });
                } else {
                    logger.debug('Offer already in - ' + offer.offerStatus + ' state.');
                    //done(new Error('Offer already in - ' + offer.offerStatus + ' state.'));

                    async.each(order.products, function (orderProduct, callback) {

                        inventoriesUtil.findInventoryById(orderProduct.inventory._id,-1,function (inventory) {
                            if (inventory instanceof Error) {
                                logger.error('Error while loading the inventory with product number - ' + orderProduct.inventory.productNum, inventory);
                                callback(inventory);
                            } else {
                                inventory.numberOfUnitsHistory.push({
                                    order: order._id,
                                    changeType: 'OrderReturn',
                                    changeUnits: orderProduct.numberOfUnits
                                });
                                inventory.numberOfUnits = inventory.numberOfUnits + orderProduct.numberOfUnits;
                                inventory.updateHistory.push({
                                    modifiedOn: Date.now(),
                                    modifiedBy: loginuser._id
                                });
                                inventory.save(function (inventorySaveErr) {
                                    if (inventorySaveErr) {
                                        logger.error('Error while updating the inventory with product number - ' + inventory.productNum, inventorySaveErr);
                                        callback(inventorySaveErr);
                                    } else {
                                        logger.debug('Successfully updated the inventory count for the inventory with product number -' + inventory.productNum + ', count -' + orderProduct.numberOfUnits + ' for the order confirmation of order with order number -' + order.orderNumber);
                                        callback();
                                    }
                                });
                            }
                        });

                    }, function (err) {
                        if (err) {
                            done(err);
                        } else {
                            done();
                        }
                    });
                }
            }
        });
    } else {
// Just deduct from the inventory
        async.each(order.products, function (orderProduct, callback) {
            inventoriesUtil.findInventoryById(orderProduct.inventory._id,-1,function (inventory) {
                if (inventory instanceof Error) {
                    logger.error('Error while loading the inventory with product number - ' + orderProduct.inventory.productNum, inventory);
                    callback(inventory);
                } else {
                    if (inventory.numberOfUnits - inventory.offerUnits < orderProduct.numberOfUnits) {
                        logger.error('Not enough quantity available for the inventory product with product number - ' + inventory.productNum);
                        callback(new Error('Not enough quantity available for the inventory product with product number - ' + inventory.productNum));
                    } else {
                        inventory.numberOfUnitsHistory.push({
                            order: order._id,
                            changeType: 'OrderReturn',
                            changeUnits: orderProduct.numberOfUnits
                        });
                        inventory.numberOfUnits = inventory.numberOfUnits + orderProduct.numberOfUnits;

                        inventory.updateHistory.push({
                            modifiedOn: Date.now(),
                            modifiedBy: loginuser._id
                        });
                        inventory.save(function (inventorySaveErr) {
                            if (inventorySaveErr) {
                                logger.error('Error while updating the inventory with product number - ' + inventory.productNum, inventorySaveErr);
                                callback(inventorySaveErr);
                            } else {
                                logger.debug('Successfully updated the inventory count for the inventory with product number -' + inventory.productNum + ', count -' + orderProduct.numberOfUnits + ' for the order confirmation of order with order number -' + order.orderNumber);
                                callback();
                            }
                        });
                    }
                }
            });
        }, function (err) {
            if (err) {
                done(err);
            } else {
                done();
            }
        });
    }
}

/**
 * Create a Order
 * This api is deprecated and never used now.
 */
exports.create = function(req, res) {
    var order = new Order(req.body);
    logger.info(order.offer.toString());
    var token = req.body.token || req.headers.token;
    logger.info('Create Order - Order number ' + order.orderNumber);
    usersJWTUtil.findUserByToken(token, function(err, user) {
        if (err) {
            logger.error('Error while fetching the user with the given token for creating order with Order number ' + order.orderNumber);
            return res.status(400).send({
                status: false,
                message: errorHandler.getErrorMessage(err)
            });
        }

        order.user = user;

        order.updateHistory = [];
        order.updateHistory.push({modifiedOn: Date.now(), modifiedBy: user});

        logger.info('Create Order - Order with order number ' + order.orderNumber + ' is placed by ' + user.displayName);

        offerUtil.fetchOfferById(order.offer,-1,function (err,oldOffer) {
            if (err instanceof Error) {
                logger.error('Error while creating order with Order number ' + order.orderNumber + ' and placed by ' + user.displayName);
                return res.status(400).send({
                    status: false,
                    message: errorHandler.getErrorMessage(err)
                });
            }else {
                if (oldOffer.user.id.toString() !== order.user.id.toString()) {
                    Contact.find({
                        'nVipaniRegContact': true,
                        nVipaniUser: oldOffer.user
                    }).exec(function (currenterr, oldContacts) {
                        if (oldContacts.length > 0) {
                            if (!order.seller.contact) {
                                order.seller.contact = oldContacts[0]._id;
                                order.seller.nVipaniUser = oldOffer.user;
                                order.seller.businessUnit=oldContacts[0].businessUnit;
                            }
                        }

                    });
                }
                orderUtil.saveOrder(order,logger,function (orderResponseErr,orderResponse) {
                    if (orderResponseErr) {
                        logger.error('Error while creating order with Order number ' + order.orderNumber + ' and placed by ' + user.displayName);
                        return res.status(400).send({
                            status: false,
                            message: errorHandler.getErrorMessage(orderResponseErr)
                        });
                    } else {
                        res.json(orderResponse);
                    }


                });
            }
        });

    });
};


/**
 * Show the current Order
 */
exports.read = function(req, res) {
    res.jsonp(req.order);
};


/**
 * Update a Order
 */
function invoiceReportPath(order,regenerate,done) {
    /* regenerate=true;*/
    orderUtil.filePath(order,function (reportPath) {
        if(reportPath && !regenerate){
            done(null,reportPath);
        }else {
            orderUtil.invoiceReportGeneration(order, regenerate,logger, function (reportGenErr, invoiceGeneratedOrder) {
                if (reportGenErr) {
                    logger.error('Error while generating the invoice for the order with Order number ' + order.orderNumber, reportGenErr);
                    done(reportGenErr,null);
                } else {
                    orderUtil.filePath(invoiceGeneratedOrder,function (reportPath) {
                        if(reportPath){
                            done(null,reportPath);
                        }else{
                            done(new Error('No Proper report path'),null);
                        }
                    });

                }


            });
        }
    });


}

exports.invoiceReport =function(req,res){
    var order=req.order;

    var mobileAppKey= req.query.apikey;
    var regenerate=req.query.generate;

    order=_.extend(order, '');

    var invoiceReportFile;

    invoiceReportPath(order,regenerate,function (reportPathErr,reportPath) {

        if (reportPathErr) {
            return res.status(400).send({
                status: false,
                message: errorHandler.getErrorMessage(reportPathErr)
            });
        } else {
            fs.readFile(reportPath, function (reportReadErr, data) {
                if (reportReadErr) {
                    logger.error('Error while reading the invoice for the order with Order number ' + order.orderNumber, reportReadErr);
                    return res.status(400).send({
                        status: false,
                        message: errorHandler.getErrorMessage(reportReadErr)
                    });
                } else {
                    if (mobileAppKey === config.bbapikey) {
                        res.contentType('application/octet-stream');
                    } else {
                        res.contentType('application/pdf');
                    }

                    res.send(data);

                }
            });
        }
    });
};

function saveOrderSendNotification(order,dataFields,loginuser,res,done) {
    orderUtil.saveOrder(order,logger,function(orderResponseErr,orderResponse){

        if (orderResponseErr) {
            logger.error('Error while updating the order with Order number ' + order.orderNumber);
            done(orderResponseErr,null);
        }
        else {
            logger.info('Loading the order details with Order number ' + order.orderNumber + ' after creation');
            // Case - 1 Purchase Order - Placed (Payment Option needs to be selected) --> Confirmed (Seller) --> In Progress or Shipping (Seller) --> Delivered (Seller/Buyer) --> Completed (Seller)
            // Case - 2 Sale Order - Confirmed (Payment Option needs to be selected) --> In Progress or Shipping (Seller) --> Delivered (Seller/Buyer) --> Completed (Seller)

            if(dataFields.currentStatus !== order.currentStatus){
                orderNotifications(res, orderResponse, dataFields.currentStatus,loginuser, function (err) {
                    if (err instanceof Error) {
                        logger.error('Error while loading the order details with Order number ' + order.orderNumber + ' after creation');
                        done(err,null);
                    } else {
                        if (orderResponse.currentStatus === 'Delivered' &&((order.isIntraStock &&  order.buyer.businessUnit)|| (order.buyer.nVipaniUser))) {
                            var owner={user:loginuser,company:loginuser.company};
                            createInventoryProductWithStockMaster(orderResponse, owner,false, function (err) {
                                if (err && err instanceof Error) {
                                    done(err,null);
                                } else {
                                    done(null,orderResponse);
                                }
                            });
                        } else {
                            done(null,orderResponse);
                        }
                    }
                });
            }else {
                done(null,orderResponse);
            }
        }
    });

}
function updateIntraStockOrder(order,done) {
    if(order.isIntraStock && order.buyer.businessUnit ){
        order.orderType = 'Sale';
        bUtil.findDefaultUserBusinessUnit(order.buyer.businessUnit && order.buyer.businessUnit._id?order.buyer.businessUnit._id:order.buyer.businessUnit,function (err,user) {
            if(err){
                done(err,null);
            }else {
                contactUtil.getUserContact(user, function (contactErr, unitInchargeContact) {
                    if (contactErr) {
                        done(contactErr, null);
                    } else {
                        order.buyer.nVipaniCompany = unitInchargeContact.nVipaniCompany;
                        order.buyer.contact = unitInchargeContact._id;
                        order.buyer.nVipaniUser = unitInchargeContact.nVipaniUser;
                        done(null,order);
                    }
                });
            }
        });

    } else{
        done(null,order);
    }
}
function draftOrderUpdate(order,loginuser,dataFields,res,done) {
    if(order.currentStatus==='Drafted'  && dataFields.currentStatus==='Drafted'){
        if(dataFields.playersChange) {
            //if order was not created from an offer
            Contact.findById(((!order.offer || (order.offer && order.offer.offerType === 'Sell')) && order.buyer && order.buyer.contact) ? (order.buyer.contact._id ? order.buyer.contact._id.toString() : order.buyer.contact.toString()) : (order.seller.contact._id ? order.seller.contact._id.toString() : order.seller.contact.toString())).exec(function (errorResp, result2) {
                if (errorResp) {
                    logger.error('Error while updating the order with Order number ' + order.orderNumber);
                    done(errorResp,null);
                } else {
                    orderUtil.saveOrder(order, logger, function (orderResponseErr, orderResponse) {
                        if (orderResponseErr) {
                            logger.error('Error while loading the order details with Order number ' + order.orderNumber + ' after creation');
                            done(orderResponseErr,null);
                        } else {
                            done(null,orderResponse);
                        }
                    });

                }
            });
        }else if(dataFields.addressUpdate){
            order.ordersVersionKey = dataFields.versionKey;
            if (dataFields.isNewShippingAddress || dataFields.isNewBillingAddress) {
                var ShippingAddress = [];
                var billingAddress = [];


                Contact.findById(((!order.offer ||(order.offer && order.offer.offerType==='Sell'))&& order.buyer && order.buyer.contact) ? (order.buyer.contact._id?order.buyer.contact._id.toString():order.buyer.contact.toString()): (order.seller.contact._id?order.seller.contact._id.toString():order.seller.contact.toString())).exec(function (errorResp, result2) {
                    if (errorResp) {
                        logger.error('Error while updating the order with Order number ' + order.orderNumber);
                        done(errorResp,null);
                    }
                    else {
                        if (result2) {
                            /* shippingContact.$promise.then(function (result2) {*/

                            if (dataFields.isNewShippingAddress) {
                                ShippingAddress = order.shippingAddress.toObject();
                                if (result2.addresses) {
                                    ShippingAddress.addressType = 'Shipping';
                                    result2.addresses.push(ShippingAddress);
                                }
                            }
                            if (dataFields.isNewBillingAddress) {
                                billingAddress = order.billingAddress.toObject();
                                billingAddress.addressType = 'Billing';
                                if (result2.addresses) {
                                    result2.addresses.push(billingAddress);
                                }
                            }

                            result2.save(function (contactError, resultContact) {
                                if (contactError) {
                                    logger.error('Error while updating the order with Order number ' + order.orderNumber);
                                    done(contactError,null);
                                }
                                else {
                                    updateIntraStockOrder(order, function (err, finalOrder) {
                                        if (err) {
                                            done(err, null);
                                        } else {
                                            orderUtil.saveOrder(order, logger, function (orderResponseErr, orderResponse) {

                                                if (orderResponseErr) {
                                                    logger.error('Error while updating the order with Order number ' + order.orderNumber);

                                                }
                                                else {
                                                    logger.info('Loading the order details with Order number ' + order.orderNumber + ' after creation');


                                                }
                                                done(orderResponseErr, orderResponse)
                                                ;
                                            });
                                        }
                                    });
                                }
                            });
                        } else {
                            updateIntraStockOrder(order, function (err, finalOrder) {
                                if (err) {
                                    done(err, null);
                                } else {
                                    orderUtil.saveOrder(order, logger, function (orderResponseErr, orderResponse) {

                                        if (orderResponseErr) {
                                            logger.error('Error while updating the order with Order number ' + order.orderNumber);

                                        }
                                        else {
                                            logger.info('Loading the order details with Order number ' + order.orderNumber + ' after creation');
                                        }
                                        done(orderResponseErr, orderResponse);
                                    });
                                }
                            });
                        }
                    }
                });
            }else{
                order.ordersVersionKey = dataFields.versionKey;
                updateIntraStockOrder(order, function (err, finalOrder) {
                    if (err) {
                        done(err, null);
                    } else {
                        orderUtil.saveOrder(order, logger, function (orderResponseErr, orderResponse) {

                            if (orderResponseErr) {
                                logger.error('Error while updating the order with Order number ' + order.orderNumber);

                            }
                            else {
                                logger.info('Loading the order details with Order number ' + order.orderNumber + ' after creation');


                            }
                            done(orderResponseErr, orderResponse);
                        });
                    }
                });
            }

        }else{
            updateIntraStockOrder(order,function (err,finalOrder) {
                if(err){
                    done(err,null);
                }else{
                    orderUtil.saveOrder(finalOrder,logger,function(orderResponseErr,orderResponse) {
                        done(orderResponseErr,orderResponse);
                    });
                }

            });



        }
    }else if(order.currentStatus==='Placed'  && dataFields.currentStatus==='Drafted' && order.applicablePaymentTerm.type) {
        if (order.applicablePaymentTerm.type === 'Installments') {
            if(order.applicablePaymentTerm.installmentPaymentDetails &&  order.applicablePaymentTerm.installmentPaymentDetails.length>0) {
                order.applicablePaymentTerm.installmentPaymentDetails[0].paidToUser = {
                    user: order.seller.nVipaniUser,
                    contact: order.seller.contact,
                    businessUnit: order.seller.businessUnit,
                    company: order.seller.nVipaniCompany._id
                };
                order.applicablePaymentTerm.installmentPaymentDetails[0].paidByUser = {
                    user: loginuser,
                    contact: order.buyer.contact,
                    businessUnit: order.buyer.businessUnit,
                    company: order.buyer.nVipaniCompany._id
                };
                if (order.applicablePaymentTerm.installmentPaymentDetails[0].paymentMode) {
                    order.applicablePaymentTerm.installmentPaymentDetails[0].status = 'Paid';
                    order.applicablePaymentTerm.installmentPaymentDetails[0].amout = order.totalAmount * order.applicablePaymentTerm.installmentPaymentDetails[0].percentage / 100;
                    order.paidAmount = order.totalAmount * order.applicablePaymentTerm.installmentPaymentDetails[0].percentage / 100;
                }
            }
        } else if (order.applicablePaymentTerm.type === 'PayNow') {
            order.applicablePaymentTerm.paymentDetails.paidToUser = {
                user: order.seller.nVipaniUser,
                contact: order.seller.contact,
                businessUnit: order.seller.businessUnit,
                company: order.seller.nVipaniCompany
            };
            order.applicablePaymentTerm.paymentDetails.paidByUser = {
                user: loginuser,
                contact: order.buyer.contact,
                businessUnit: order.buyer.businessUnit,
                company: order.buyer.nVipaniCompany
            };
            order.applicablePaymentTerm.paymentDetails.status='Paid';
            order.applicablePaymentTerm.paymentDetails.amount=order.totalAmount;
            /*order.applicablePaymentTerm.paymentDetails.paidAmount=order.totalAmount;*/
            order.paidAmount=order.totalAmount;
        }else if (order.applicablePaymentTerm.type && order.applicablePaymentTerm.type.length>0) {
            order.applicablePaymentTerm.paymentDetails.paidToUser = {
                user: order.seller.nVipaniUser,
                contact: order.seller.contact,
                businessUnit: order.seller.businessUnit
            };
            if( order.seller.nVipaniCompany){
                order.applicablePaymentTerm.paymentDetails.paidToUser.company=  order.seller.nVipaniCompany._id;
            }
            order.applicablePaymentTerm.paymentDetails.paidByUser = {
                contact: order.buyer.contact,
                businessUnit: order.buyer.businessUnit
            };
            if( order.buyer.nVipaniCompany){
                order.applicablePaymentTerm.paymentDetails.paidByUser.company=  order.buyer.nVipaniCompany._id;
            }
            order.applicablePaymentTerm.paymentDetails.amount=order.totalAmount;
            order.applicablePaymentTerm.paymentDetails.paidAmount=0;
            order.applicablePaymentTerm.paymentDetails.status='YetToPay';
        }
        saveOrderSendNotification(order,dataFields,loginuser,res,function(resErr,data){
            done(resErr,data);
        });

    }else{
        done(null,order);
    }


}
function confirmOrderSendNotification(order,loginuser,res,done) {
    logger.info('Creating order notification for status change to ' + order.currentStatus + ' for order with Order number ' + order.orderNumber);

    var todo;
    var todoUpdate;

    var notification = new Notification();
    notification.user = loginuser;

    notification.source.order = order;

    notification.type = 'Order';

    notification.channel = ['Email', 'SMS', 'Push'];


    notification.target.contact = order.buyer.contact;
    if (((!order.offer && order.orderType === 'Sale') || (order.offer && order.offer.offerType === 'Sell')) && order.buyer.contact && order.buyer.nVipaniUser) {
        notification.target = notificationTargetFields(notification.target, order.buyer);
    }
    if (order.offer && order.offer.offerType === 'Buy' && order.seller.contact && order.seller.nVipaniUser) {
        notification.target = notificationTargetFields(notification.target, order.seller);
    }

    notification.title = 'Seller ' + displayName(loginuser) + ' ' + order.currentStatus.toLowerCase() + ' the order with order number ' + order.orderNumber;
    notification.shortMessage = 'Seller ' + displayName(loginuser) + ' ' + order.currentStatus.toLowerCase() + ' the order with order number ' + order.orderNumber + '. From nVipani Team';

    todo = new Todo();
    todo.source.order = order;
    todo.title = 'Order with order number ' + order.orderNumber + ' needs to be shipped';
    todo.type = 'OrderShipment';
    /*if (order.offer.offerType === 'Sell')*/
    if (order.seller.nVipaniUser) {
        todo.target = order.seller.nVipaniUser;
    }


    todoUpdate = new Todo();
    todoUpdate.source.order = order;
    todoUpdate.type = 'OrderConfirmation';


    notification.save(function (err) {
        if (err) {
            logger.error('Error while creating order notification for status change to ' + order.currentStatus + ' for order with Order number ' + order.orderNumber);
            done(err,null);
        } else if (order.mediator && order.mediator.contact) {
            notificationUtil.processNotification(res, notification, function (processNotificationError) {
                if (processNotificationError) {
                    logger.error('Error while processing  order notification for status change to ' + order.currentStatus + ' for order with Order number ' + order.orderNumber, processNotificationError);
                    done(processNotificationError,null);
                } else {
                    var mediatorNotification = new Notification();
                    mediatorNotification.user = loginuser;
                    mediatorNotification.target = notificationTargetFields(mediatorNotification.target, order.buyer);
                    /* mediatorNotification.target.contact = order.mediator.contact;
                     if (order.mediator.contact && order.mediator.nVipaniUser) {
                         mediatorNotification.target.nVipaniUser = order.mediator.nVipaniUser;
                         mediatorNotification.target.nVipaniCompany = order.mediator.nVipaniCompany;
                         mediatorNotification.target.businessUnit = order.mediator.businessUnit;
                     }*/
                    mediatorNotification.source.order = order;
                    mediatorNotification.type = 'Order';

                    mediatorNotification.channel = ['Email', 'SMS', 'Push'];

                    mediatorNotification.title = notification.title;
                    mediatorNotification.shortMessage = notification.shortMessage;
                    mediatorNotification.save(function (err) {
                        if (err) {
                            logger.error('Error while creating order notification for mediator for status change to ' + order.currentStatus + ' for order with Order number ' + order.orderNumber);
                            done(err,null);
                        } else {
                            notificationUtil.processNotification(res, mediatorNotification, function (processMediatorNotificationError) {
                                if (processMediatorNotificationError) {
                                    logger.error('Error while processing  order notification for mediator for status change to ' + order.currentStatus + ' for order with Order number ' + order.orderNumber, processMediatorNotificationError);
                                    done(processMediatorNotificationError,null);
                                } else {
                                    todo.save(function (err) {
                                        if (err) {
                                            logger.error('Error while creating order todo for status change to ' + order.currentStatus + ' for order with Order number ' + order.orderNumber);
                                            done(err,null);
                                        } else {
                                            if (order.seller.nVipaniUser) {
                                                todoUpdate.target = order.seller.nVipaniUser;
                                            }
                                            Todo.update({
                                                type: todoUpdate.type,
                                                'source.order': todoUpdate.source.order,
                                                target: todoUpdate.target
                                            }, {completed: true}, function (err, numberAffected, rawResponse) {
                                                if (err) {
                                                    logger.error('Error while creating order todo for status change to ' + order.currentStatus + ' for order with Order number ' + order.orderNumber);
                                                    done(err,null);
                                                } else {
                                                    logger.info('Loading the order details with Order number ' + order.orderNumber + ' after creation');
                                                    orderUtil.fetchOrderById(order._id, function (orderResponseErr, orderResponse) {
                                                        if (orderResponseErr) {
                                                            logger.error('Error while loading the order details with Order number ' + order.orderNumber + ' after creation');
                                                            done(orderResponseErr);
                                                        } else {
                                                            done(null,orderResponse);
                                                        }
                                                    });
                                                }
                                            });
                                        }
                                    });
                                }
                            });
                        }
                    });
                }
            });

        } else {
            notificationUtil.processNotification(res, notification, function (processNotificationError) {
                if (processNotificationError) {
                    logger.error('Error while processing  order notification for status change to ' + order.currentStatus + ' for order with Order number ' + order.orderNumber, processNotificationError);
                    done(processNotificationError,null);
                } else {
                    todo.save(function (err) {
                        if (err) {
                            logger.error('Error while creating order todo for status change to ' + order.currentStatus + ' for order with Order number ' + order.orderNumber);
                            done(err,null);
                        } else {
                            if (order.seller.nVipaniUser) {
                                todoUpdate.target = order.seller.nVipaniUser;
                            }
                            Todo.update({
                                type: todoUpdate.type,
                                'source.order': todoUpdate.source.order,
                                target: todoUpdate.target
                            }, {completed: true}, function (err, numberAffected, rawResponse) {
                                if (err) {
                                    logger.error('Error while creating order todo for status change to ' + order.currentStatus + ' for order with Order number ' + order.orderNumber);
                                    done(err,null);
                                } else {
                                    logger.info('Loading the order details with Order number ' + order.orderNumber + ' after creation');
                                    orderUtil.fetchOrderById(order._id, function (orderResponseErr, orderResponse) {
                                        if (orderResponseErr) {
                                            logger.error('Error while loading the order details with Order number ' + order.orderNumber + ' after creation');
                                            done(orderResponseErr,null);
                                        } else {
                                            done(null,orderResponse);
                                        }
                                    });
                                }
                            });
                        }
                    });
                }
            });
        }
    });
}
function onlyPaymentUpdate(order,loginuser,done) {
    if (order.applicablePaymentTerm.type === 'Installments') {
        if(order.applicablePaymentTerm.installmentPaymentDetails &&  order.applicablePaymentTerm.installmentPaymentDetails.length>0) {
            order.applicablePaymentTerm.installmentPaymentDetails[0].paidToUser = {
                user: order.seller.nVipaniUser,
                contact: order.seller.contact,
                businessUnit: order.seller.businessUnit,
                company: order.seller.nVipaniCompany._id
            };
            order.applicablePaymentTerm.installmentPaymentDetails[0].paidByUser = {
                user: loginuser,
                contact: order.buyer.contact,
                businessUnit: order.buyer.businessUnit,
                company: order.buyer.nVipaniCompany._id
            };
            if (order.applicablePaymentTerm.installmentPaymentDetails[0].paymentMode) {
                order.applicablePaymentTerm.installmentPaymentDetails[0].status = 'Paid';
                order.applicablePaymentTerm.installmentPaymentDetails[0].amout = order.totalAmount * order.applicablePaymentTerm.installmentPaymentDetails[0].percentage / 100;
                order.paidAmount = order.totalAmount * order.applicablePaymentTerm.installmentPaymentDetails[0].percentage / 100;
            }
        }
    }else if (order.applicablePaymentTerm.type.length>0) {
        order.applicablePaymentTerm.paymentDetails.paidToUser = {
            user: order.seller.nVipaniUser,
            contact: order.seller.contact,
            businessUnit: order.seller.businessUnit,
            company: order.seller.nVipaniCompany._id
        };
        order.applicablePaymentTerm.paymentDetails.paidByUser = {
            contact: order.buyer.contact,
            businessUnit: order.buyer.businessUnit,
            company: order.buyer.nVipaniCompany._id
        };
        order.applicablePaymentTerm.paymentDetails.amount=order.totalAmount;
        order.applicablePaymentTerm.paymentDetails.paidAmount=0;
        order.applicablePaymentTerm.paymentDetails.status='YetToPay';
    }
    orderUtil.saveOrder(order,logger,function (errResponse,response) {
        done(errResponse,response);
    });
}
exports.update = function(req, res) {
    var order = req.order;
    var oldStatus = order.currentStatus;
    var dataFields={isPaymentUpdate:req.body.applicablePaymentTerm && req.body.applicablePaymentTerm.isPaymentUpdate,oldPaymentTerm:req.body.applicablePaymentTerm,oldPaymentType:order.applicablePaymentTerm.type,currentStatus:order.currentStatus, oldSeller:order.seller,oldBuyer:order.buyer,versionKey: order.ordersVersionKey,paymentUpdate:req.body.isPayment,addressUpdate:req.body.isAddress,playersChange:req.body.isPlayers,isNewShippingAddress:(req.body.shippingAddress && req.body.shippingAddress.isNew ?req.body.shippingAddress.isNew :false),isNewBillingAddress:(req.body.billingAddress && req.body.billingAddress.isNew ?req.body.billingAddress.isNew :false)};


    order = _.extend(order, req.body);


    order.set('lastUpdated', Date.now());

    var token = req.body.token || req.headers.token;
    logger.info('Updating order with Order number ' + JSON.stringify(order));
    usersJWTUtil.findUserByToken(token, function (userErr, loginuser) {
        if (userErr) {
            logger.error('Error while fetching the user with the given token for updating order with Order number ' + order.orderNumber, userErr);
            return res.status(400).send({
                status: false,
                message: errorHandler.getErrorMessage(userErr)
            });
        }else {
            order.updateHistory.push({modifiedOn: Date.now(), modifiedBy: loginuser._id});
            order.ordersVersionKey = dataFields.versionKey;
            if (dataFields.currentStatus !== order.currentStatus) {
                order.statusHistory.push({
                    status: order.currentStatus,
                    user: loginuser._id,
                    displayName: loginuser.displayName,
                    comments: order.statusComments !== null ? order.statusComments : ''
                });
            }
            if(dataFields.currentStatus===order.currentStatus && dataFields.isPaymentUpdate){
                onlyPaymentUpdate(order,loginuser,function (errResponse,response) {
                    if(errResponse){
                        logger.error('Error while updating order  in draft order of Order number - ' + order.orderNumber, errResponse);
                        return res.status(400).send({
                            message: errorHandler.getErrorMessage(errResponse)
                        });
                    }else{
                        res.jsonp(response);
                    }
                });
            }else if((order.currentStatus==='Placed' || order.currentStatus==='Drafted') && dataFields.currentStatus==='Drafted') {
                draftOrderUpdate(order,loginuser,dataFields,res,function (draftOrderErr,draftOrder) {
                    if(draftOrderErr){
                        logger.error('Error while updating order  in draft order of Order number - ' + order.orderNumber, draftOrderErr);
                        return res.status(400).send({
                            message: errorHandler.getErrorMessage(draftOrderErr)
                        });
                    }else{
                        res.jsonp(draftOrder);
                    }

                });
            }else if (oldStatus !== order.currentStatus && order.currentStatus==='Confirmed' ) {
                updateStockMasterAndInventory(order, loginuser,true, function (err) {
                    if (err) {
                        logger.error('Error while updating offer product count after order confirmation of Order number - ' + order.orderNumber, err);
                        return res.status(400).send({
                            message: errorHandler.getErrorMessage(err)
                        });
                    } else {
                        order.save(function (err) {
                            if (err) {
                                logger.error('Error while creating order with Order number ' + order.orderNumber + ' and Confirmed by ' + loginuser.displayName);
                                return res.status(400).send({
                                    status: false,
                                    message: errorHandler.getErrorMessage(err)
                                });
                            } else {
                                orderUtil.invoiceReportGeneration(order, false, logger, function (reportGenErr, invoiceGeneratedOrder) {
                                    if (reportGenErr) {
                                        logger.error('Error while generating the invoice for the order with Order number ' + order.orderNumber, reportGenErr);
                                        return res.status(400).send({
                                            status: false,
                                            message: errorHandler.getErrorMessage(reportGenErr)
                                        });
                                    } else {

                                        // Case - 1 Purchase Order - Placed (Payment Option needs to be selected) --> Confirmed (Seller) --> In Progress or Shipping (Seller) --> Delivered (Seller/Buyer) --> Completed (Seller)
                                        // Case - 2 Sale Order - Confirmed (Payment Option needs to be selected) --> In Progress or Shipping (Seller) --> Delivered (Seller/Buyer) --> Completed (Seller)
                                        confirmOrderSendNotification(invoiceGeneratedOrder,loginuser,res,function (confirmNotificationErr,notificationOrder) {
                                            if(confirmNotificationErr){
                                                return res.status(400).send({
                                                    status: false,
                                                    message: errorHandler.getErrorMessage(confirmNotificationErr)
                                                });
                                            } else{
                                                res.jsonp(notificationOrder);
                                            }
                                        });

                                    }
                                });
                            }
                        });

                    }
                });
            }
            else if (order.orderType==='Purchase' && (!order.offer ||! (order.offer && order.offer.offerType==='Sell')) && dataFields.oldSeller && dataFields.oldSeller.nVipaniUser && dataFields.currentStatus === 'Drafted') {
                updateOrderProducts(order, loginuser, function (resultorder) {
                    if (resultorder instanceof Error) {
                        return res.status(400).send({
                            status: false,
                            message: errorHandler.getErrorMessage(resultorder)
                        });
                    } else {
                        orderUtil.saveOrder(resultorder, logger, function (orderResponseErr, orderResponse) {
                            if (orderResponseErr) {
                                logger.error('Error while updating the order with Order number ' + order.orderNumber);
                                return res.status(400).send({
                                    status: false,
                                    message: errorHandler.getErrorMessage(orderResponseErr)
                                });
                            } else {
                                res.json(orderResponse);
                            }


                        });
                    }


                });

            }else if(oldStatus !== order.currentStatus && oldStatus === 'Disputed' && order.currentStatus === 'Returned'){
                createInventoryProduct(order, loginuser,true, function (err) {
                    if (err instanceof Error) {
                        return res.status(400).send({
                            message: errorHandler.getErrorMessage(err)
                        });
                    } else {
                        orderUtil.saveOrder(order, logger, function (orderResponseErr, orderResponse) {
                            if (orderResponseErr) {
                                logger.error('Error while updating the order with Order number ' + order.orderNumber);
                                return res.status(400).send({
                                    status: false,
                                    message: errorHandler.getErrorMessage(orderResponseErr)
                                });
                            } else {
                                res.json(orderResponse);
                            }


                        });
                        /*     }
                         });*/
                    }
                });

            }else {
                saveOrderSendNotification(order,dataFields,loginuser,res,function (saveErr,saveResponse) {
                    if(saveErr){
                        logger.error('Error while updating order with Order number - ' + order.orderNumber, saveErr);
                        return res.status(400).send({
                            message: errorHandler.getErrorMessage(saveErr)
                        });
                    }else{
                        res.jsonp(saveResponse);
                    }
                });
            }
        }

    });
};


/**
 *  find contact and update contact nVipani user to order
 * @param order
 * @param contact
 * @param context
 * @param done
 */
function getOrderContact(orderNumber,contact,context,done) {
    contactUtil.findByContactById(contact,2,function (errorBuyerResp, buyerContact){
        if(errorBuyerResp){
            logger.error('Error while fetching the order with order Id for order '+context+' contact update ' ,errorBuyerResp);
            /*      return res.status(400).send({
                      status: false,
                      message: errorHandler.getErrorMessage(errorSellerResp)
                  });*/
            done(errorBuyerResp,null);
        }else if(!buyerContact){
            logger.error('Error while fetching the order with order Number :'+orderNumber+' for order '+context+' contact update ');
            /* return res.status(400).send({
                 status: false,
                 message: 'No seller contact for  order with order Number :'+order.orderNumber
             });*/
            done(new Error('No  '+context+'  contact for  order with order Number :'+orderNumber),null);

        }else{
            logger.debug('Got the '+context+' contact to update the nVipani user');
            done(null,buyerContact);
            /*if(buyerContact.nVipaniUser) {
                if(context==='Seller'){
                    order.seller.nVipaniUser = buyerContact.nVipaniUser;
                }else if(context==='Buyer') {
                    order.buyer.nVipaniUser = buyerContact.nVipaniUser;
                }
            }
            /!**
             *  update the order and find order by Id with populated fields
             *!/
            saveOrder(order,function (updateErr,updateBuyerOrder) {
                if(updateErr){
                    logger.error('Got the '+context+' contact to update the nVipani user',updateErr);
                    done(updateErr,null);
                }else{
                    logger.debug('Updated the order with the '+context+' contact nVipani user');
                    done(null,updateBuyerOrder);
                }

            });*/
        }

    });
}
function getOrderStakeHolderContact(order,isSeller,isAddressUpdate,done) {
    if(!order || !order._id){
        logger.error('Error while fetching the order with order Id for order contact update ' + order.orderNumber);
        done(new Error('No order with order Number :'+order.orderNumber),null,null);
        // order is created from inventory but not from offer.
    }else if(!order.offer){
        // find  the seller contact nVipani user for purchase order if the order is not created from offer.
        if(order.orderType==='Purchase'){
            if(order.seller.contact){
                // update the nVipani user for seller contact and then update order
                getOrderContact(order.orderNumber,order.seller.contact._id?order.seller.contact._id:order.seller.contact.toString(),'Seller',function (errorSellerResp, sellerContact){
                    done(errorSellerResp,sellerContact,true);
                });
            }else{
                logger.error('Error while fetching the order with order Id for order seller contact update ' + JSON.stringify(order));
                done( new Error('Error while fetching the order with order Id for order seller contact update'),null,null);
            }
            // find  the Buyer contact nVipani user for  Sale order if the order is not created from offer.
        }else if(order.orderType==='Sale'){
            if(order.buyer.contact){
                // update the nVipani user for buyer contact and then update order
                getOrderContact(order.orderNumber,order.buyer.contact._id?order.buyer.contact._id:order.buyer.contact.toString(),'Buyer',function (errorBuyerResp, buyerContact){
                    done(errorBuyerResp,buyerContact,false);
                });
            }else{
                logger.error('Error while fetching the order with order Id for order buyer contact update ' + JSON.stringify(order));
                done( new Error('Error while fetching the order with order Id for order buyer contact update'),null,null);
            }

        }else{
            logger.error('Error while fetching the order with order Id for order contact update ' + JSON.stringify(order));
            done( new Error('Error while fetching the order with order Id for order contact update'),null,null);
        }
        // order is created from offer.
    }else if(order.offer && order.offer.offerType==='Sell' ){
        // find out the buyer contact nVipani user for Sale order is created from offer.
        if(order.orderType==='Sale' && order.buyer && order.buyer.contact){
            //find the Buyer contact nVipani user and then update the order
            getOrderContact(order.orderNumber,order.buyer.contact._id?order.buyer.contact._id:order.buyer.contact.toString(),'Buyer',function (errorSellerResp, buyerContact){
                done(errorSellerResp,buyerContact,false);
            });

        }else if(isAddressUpdate && order.orderType==='Purchase' && order.buyer && order.buyer.contact){
            //find the Buyer contact nVipani user and then update the order
            getOrderContact(order.orderNumber,order.buyer.contact._id?order.buyer.contact._id:order.buyer.contact.toString(),'Buyer',function (errorSellerResp, buyerContact){
                done(errorSellerResp,buyerContact,false);
            });

        }else{
            logger.error('No Buyer for the order with the order Number :' + order.orderNumber);
            done(new Error('No Buyer for the Order with the Order Number :'+order.orderNumber),null,null);
        }
    }else if(order.offer && order.offer.offerType==='Buy'){
        // find out the buyer contact nVipani user for Sale order is created from offer.
        if(order.orderType==='Purchase' && order.seller && order.seller.contact){
            //find the Seller contact nVipani user and then update the order
            getOrderContact(order.orderNumber,order.seller.contact._id?order.seller.contact._id:order.seller.contact.toString(),'Seller',function (errorSellerResp, sellerContact){
                done(errorSellerResp,sellerContact,true);
            });
        }else if(isAddressUpdate && order.orderType==='Sale' && order.seller && order.seller.contact){
            //find the Buyer contact nVipani user and then update the order
            getOrderContact(order.orderNumber,order.seller.contact._id?order.seller.contact._id:order.seller.contact.toString(),'Seller',function (errorSellerResp, sellerContact){
                done(errorSellerResp,sellerContact,false);
            });

        }else{
            logger.error('No Seller for the Order with the Order Number :'+ order.orderNumber);
            done(new Error('No Seller for the Order with the Order Number :'+order.orderNumber),null,null);

        }
    }else{
        logger.error('No Seller or buyer for the Order with the Order Number :'+ JSON.stringify(order));
        done(new Error('No Seller/Buyer  for the Order with the Order'),null,null);

    }
}

function saveOrderContactAddress(order,isNewShippingAddress,isNewBillingAddress,isStakeHolder,done) {
    /*  if(isNewShippingAddress || isNewBillingAddress) {*/
    var isSeller;
    getOrderStakeHolderContact(order, isSeller, ((isNewShippingAddress || isNewBillingAddress) ?true:false) , function (contactErr, contact, isSeller) {
        if (contactErr) {
            done(contactErr, null);
        } else {
            if(isNewShippingAddress || isNewBillingAddress) {
                var ShippingAddress = [];
                var billingAddress = [];

                if (isNewShippingAddress) {
                    ShippingAddress = order.shippingAddress.toObject();
                    if (contact.addresses) {
                        ShippingAddress.addressType = 'Shipping';
                        contact.addresses.push(ShippingAddress);
                    }
                }
                if (isNewBillingAddress) {
                    billingAddress = order.billingAddress.toObject();
                    billingAddress.addressType = 'Billing';
                    if (contact.addresses) {
                        contact.addresses.push(billingAddress);
                    }
                }

                contact.save(function (contactError, resultContact) {
                    if (contactError) {
                        logger.error('Error while updating the order with Order number ' + order.orderNumber);
                        /* return res.status(400).send({
                             status: false,
                             message: errorHandler.getErrorMessage(contactError)
                         });*/
                        done(contactError, null);
                    }
                    else {
                        logger.debug('Successfully update the ' + (isSeller ? 'Seller' : 'Buyer') + ' address in order Number :' + order.orderNumber);
                        if(isStakeHolder){
                            if (isSeller) {
                                if(contact.nVipaniUser) {
                                    order.seller.nVipaniUser = contact.nVipaniUser;
                                    order.seller.businessUnit = contact.businessUnit;
                                }
                            }else {
                                if(contact.nVipaniUser) {
                                    order.buyer.nVipaniUser = contact.nVipaniUser;
                                    order.buyer.businessUnit = contact.businessUnit;
                                }

                            }

                        }
                        done(null, order);
                    }
                });
            }else{
                logger.debug('Update with the old Contact Address at order Number:'+order.orderNumber);
                done(null, order);
            }


        }
    });
    /* }else{
         logger.debug('No new address '+(isSeller?'Seller':'Buyer')+' to update  in order Number :'+order.orderNumber);
         done(null,order);

     }*/
}
/**
 *
 * @param req
 * @param res
 */
exports.updateStakeholders=function (req,res) {
    var order = req.order;
    var isNewShippingAddress=(req.body.shippingAddress && req.body.shippingAddress.isNew ?req.body.shippingAddress.isNew :false);
    var isNewBillingAddress=(req.body.billingAddress && req.body.billingAddress.isNew ?req.body.billingAddress.isNew :false);
    var token = req.body.token || req.headers.token;
    order = _.extend(order, req.body);
    order.set('lastUpdated', Date.now());
    logger.info('Updating order with Order number ' + JSON.stringify(order));
    usersJWTUtil.findUserByToken(token, function (userErr, loginuser) {
        if (userErr) {
            logger.error('Error while fetching the user with the given token for updating order with Order number for contact ' + order.orderNumber, userErr);
            return res.status(400).send({
                status: false,
                message: errorHandler.getErrorMessage(userErr)
            });
        }else {
            // update the nVipani user based on order type
            var isSeller;
            if(!order || !order._id){
                logger.error('Error while fetching the order with order Id for order contact update');
                return res.status(400).send({
                    status: false,
                    message:'No order with order Number :'+order.orderNumber
                });
            }else if (order.currentStatus==='Drafted'){
                /*getOrderStakeHolderContact(order, isSeller, false,function (err, contact, isSeller) {*/
                saveOrderContactAddress(order,isNewShippingAddress,isNewBillingAddress,false,function (orderContactErr,updateOrder) {
                    if (orderContactErr) {
                        return res.status(400).send({
                            status: false,
                            message: errorHandler.getErrorMessage(orderContactErr)
                        });
                    } else {
                        /**
                         *  update the order and find order by Id with populated fields
                         */
                        orderUtil.saveOrder(updateOrder, logger,function (updateErr, updateBuyerOrder) {
                            if (updateErr) {
                                logger.error('Got the ' + (isSeller ? 'Seller' : 'Buyer') + ' contact to update the nVipani user', updateErr);
                                return res.status(400).send({
                                    status: false,
                                    message: errorHandler.getErrorMessage(updateErr)
                                });
                            } else {
                                logger.debug('Updated the order with the ' + (isSeller ? 'Seller' : 'Buyer') + ' contact nVipani user');
                                res.jsonp(order);
                            }

                        });
                        /*} else {
                            return res.status(400).send({
                                status: false,
                                message: new Error('No Stakeholder to update the order with orderNumber :' + order.orderNumber)
                            });

                        }*/
                    }

                });
            }else{
                logger.error('Not allowed to change the order stakeholder without Draft status' + order.orderNumber +' Status :'+order.currentStatus);
                return res.status(400).send({
                    status: false,
                    message:'Not allowed to change the order stakeholder :'+order.orderNumber
                });
            }

        }

    });

};
/**
 * Order update with  only address information
 */
exports.updateAddress=function (req,res) {
    var order = req.order;
    var isNewShippingAddress=(req.body.shippingAddress && req.body.shippingAddress.isNew ?req.body.shippingAddress.isNew :false);
    var isNewBillingAddress=(req.body.billingAddress && req.body.billingAddress.isNew ?req.body.billingAddress.isNew :false);
    order = _.extend(order, req.body);
    order.set('lastUpdated', Date.now());

    var token = req.body.token || req.headers.token;
    logger.info('Updating order with Order number ' + JSON.stringify(order));
    usersJWTUtil.findUserByToken(token, function (userErr, loginuser) {
        if (userErr) {
            logger.error('Error while fetching the user with the given token for updating order with Order number for update Address ' + order.orderNumber, userErr);
            return res.status(400).send({
                status: false,
                message: errorHandler.getErrorMessage(userErr)
            });
        } else {
            if (order.currentStatus === 'Drafted') {
                var isSeller;
                //find out contact address update as part of order address.
                saveOrderContactAddress(order,isNewShippingAddress,isNewBillingAddress,false,function (orderContactErr,order) {
                    /**
                     *  update the order and find order by Id with populated fields
                     */
                    orderUtil.saveOrder(order,logger, function (updateErr, updateBuyerOrder) {
                        if (updateErr) {
                            logger.error('Got the ' + (isSeller ? 'Seller' : 'Buyer') + ' Address  update at the order', updateErr);
                            return res.status(400).send({
                                status: false,
                                message: errorHandler.getErrorMessage(updateErr)
                            });
                        } else {
                            logger.debug('Updated the order with the ' + (isSeller ? 'Seller' : 'Buyer') + ' Address update at the order');
                            res.jsonp(order);
                        }

                    });
                });
            }else {
                logger.error('Not allowed user to change the address for the order with order Number:'+order.orderNumber);
                return res.status(400).send({
                    status: false,
                    message: 'Not allowed user to change the address for the order with order Number:'+order.orderNumber
                });
            }
        }
    });




};
/**
 *
 * @param req
 * @param res
 */
exports.updateStatus=function (req,res) {
    var order = req.order;

    var oldStatus = order.currentStatus;
    order = _.extend(order, req.body);


    order.set('lastUpdated', Date.now());

    var token = req.body.token || req.headers.token;
    logger.info('Updating order with Order number ' + JSON.stringify(order));
    usersJWTUtil.findUserByToken(token, function (userErr, loginuser) {
        if (userErr) {
            logger.error('Error while fetching the user with the given token for updating order with Order number ' + order.orderNumber, userErr);
            return res.status(400).send({
                status: false,
                message: errorHandler.getErrorMessage(userErr)
            });
        } else {
            if (oldStatus !== order.currentStatus) {
                order.statusHistory.push({
                    status: order.currentStatus,
                    user: loginuser._id,
                    displayName: loginuser.displayName,
                    comments: order.statusComments !== null ? order.statusComments : ''
                });
            }
            order.updateHistory.push({modifiedOn: Date.now(), modifiedBy: loginuser._id});
            /**
             *  update the order and find order by Id with populated fields
             */

            if (oldStatus !== order.currentStatus && order.currentStatus === 'Confirmed') {
                updateStockMasterAndInventory(order, loginuser, true, function (err) {
                    if (err) {
                        logger.error('Error while updating offer product count after order confirmation of Order number - ' + order.orderNumber, err);
                        return res.status(400).send({
                            message: errorHandler.getErrorMessage(err)
                        });
                    } else {
                        orderUtil.invoiceReportGeneration(order,false, logger, function (reportErr, order) {
                            if (reportErr) {
                                logger.error('Error while updating the order with Order number ' + order.orderNumber, reportErr);
                                return res.status(400).send({
                                    status: false,
                                    message: errorHandler.getErrorMessage(reportErr)
                                });
                            } else {
                                // Case - 1 Purchase Order - Placed (Payment Option needs to be selected) --> Confirmed (Seller) --> In Progress or Shipping (Seller) --> Delivered (Seller/Buyer) --> Completed (Seller)
                                // Case - 2 Sale Order - Confirmed (Payment Option needs to be selected) --> In Progress or Shipping (Seller) --> Delivered (Seller/Buyer) --> Completed (Seller)

                                logger.info('Creating order notification for status change to ' + order.currentStatus + ' for order with Order number ' + order.orderNumber);

                                var todo;
                                var todoUpdate;

                                var notification = new Notification();
                                notification.user = loginuser;

                                notification.source.order = order;

                                notification.type = 'Order';

                                notification.channel = ['Email', 'SMS', 'Push'];


                                notification.target.contact = order.buyer.contact;
                                if (((!order.offer && order.orderType === 'Sale') || (order.offer && order.offer.offerType === 'Sell')) && order.buyer.contact && order.buyer.nVipaniUser) {
                                    notification.target=notificationTargetFields(notification.target,order.buyer);
                                }
                                if (order.offer && order.offer.offerType === 'Buy' && order.seller.contact && order.seller.nVipaniUser) {
                                    notification.target=notificationTargetFields(notification.target,order.seller);
                                }

                                notification.title = 'Seller ' + displayName(loginuser) + ' ' + order.currentStatus.toLowerCase() + ' the order with order number ' + order.orderNumber;
                                notification.shortMessage = 'Seller ' + displayName(loginuser) + ' ' + order.currentStatus.toLowerCase() + ' the order with order number ' + order.orderNumber + '. From nVipani Team';

                                todo = new Todo();
                                todo.source.order = order;
                                todo.title = 'Order with order number ' + order.orderNumber + ' needs to be shipped';
                                todo.type = 'OrderShipment';
                                /*if (order.offer.offerType === 'Sell')*/
                                if (order.seller.nVipaniUser) {
                                    todo.target = order.seller.nVipaniUser;
                                }


                                todoUpdate = new Todo();
                                todoUpdate.source.order = order;
                                todoUpdate.type = 'OrderConfirmation';


                                notification.save(function (err) {
                                    if (err) {
                                        logger.error('Error while creating order notification for status change to ' + order.currentStatus + ' for order with Order number ' + order.orderNumber);
                                        return res.status(400).send({
                                            status: false,
                                            message: errorHandler.getErrorMessage(err)
                                        });
                                    } else if (order.mediator && order.mediator.contact) {
                                        notificationUtil.processNotification(res, notification, function (processNotificationError) {
                                            if (processNotificationError) {
                                                logger.error('Error while processing  order notification for status change to ' + order.currentStatus + ' for order with Order number ' + order.orderNumber, processNotificationError);
                                                return res.status(400).send({
                                                    status: false,
                                                    message: errorHandler.getErrorMessage(processNotificationError)
                                                });
                                            } else {
                                                var mediatorNotification = new Notification();
                                                mediatorNotification.user = loginuser;
                                                mediatorNotification.target=notificationTargetFields(mediatorNotification.target,order.mediator);
                                                /* mediatorNotification.target.contact = order.mediator.contact;
                                                 if (order.mediator.contact && order.mediator.nVipaniUser) {
                                                     mediatorNotification.target.nVipaniUser = order.mediator.nVipaniUser;
                                                     notification.target.nVipaniCompany = order.mediator.nVipaniCompany;
                                                     notification.target.businessUnit = order.mediator.businessUnit;
                                                 }*/
                                                mediatorNotification.source.order = order;
                                                mediatorNotification.type = 'Order';

                                                mediatorNotification.channel = ['Email', 'SMS', 'Push'];

                                                mediatorNotification.title = notification.title;
                                                mediatorNotification.shortMessage = notification.shortMessage;
                                                mediatorNotification.save(function (err) {
                                                    if (err) {
                                                        logger.error('Error while creating order notification for mediator for status change to ' + order.currentStatus + ' for order with Order number ' + order.orderNumber);
                                                        return res.status(400).send({
                                                            status: false,
                                                            message: errorHandler.getErrorMessage(err)
                                                        });
                                                    } else {
                                                        notificationUtil.processNotification(res, mediatorNotification, function (processMediatorNotificationError) {
                                                            if (processMediatorNotificationError) {
                                                                logger.error('Error while processing  order notification for mediator for status change to ' + order.currentStatus + ' for order with Order number ' + order.orderNumber, processMediatorNotificationError);
                                                                return res.status(400).send({
                                                                    status: false,
                                                                    message: errorHandler.getErrorMessage(processMediatorNotificationError)
                                                                });
                                                            } else {
                                                                todo.save(function (err) {
                                                                    if (err) {
                                                                        logger.error('Error while creating order todo for status change to ' + order.currentStatus + ' for order with Order number ' + order.orderNumber);
                                                                        return res.status(400).send({
                                                                            status: false,
                                                                            message: errorHandler.getErrorMessage(err)
                                                                        });
                                                                    } else {
                                                                        if (order.seller.nVipaniUser) {
                                                                            todoUpdate.target = order.seller.nVipaniUser;
                                                                        }
                                                                        Todo.update({
                                                                            type: todoUpdate.type,
                                                                            'source.order': todoUpdate.source.order,
                                                                            target: todoUpdate.target
                                                                        }, {completed: true}, function (err, numberAffected, rawResponse) {
                                                                            if (err) {
                                                                                logger.error('Error while creating order todo for status change to ' + order.currentStatus + ' for order with Order number ' + order.orderNumber);
                                                                                return res.status(400).send({
                                                                                    status: false,
                                                                                    message: errorHandler.getErrorMessage(err)
                                                                                });
                                                                            } else {
                                                                                logger.info('Loading the order details with Order number ' + order.orderNumber + ' after creation');
                                                                                orderUtil.fetchOrderById(order._id, function (orderResponse) {
                                                                                    if (orderResponse instanceof Error) {
                                                                                        logger.error('Error while loading the order details with Order number ' + order.orderNumber + ' after creation');
                                                                                        return res.status(400).send({
                                                                                            message: errorHandler.getErrorMessage(orderResponse)
                                                                                        });
                                                                                    } else {
                                                                                        res.json(orderResponse);
                                                                                    }
                                                                                });
                                                                            }
                                                                        });
                                                                    }
                                                                });
                                                            }
                                                        });
                                                    }
                                                });
                                            }
                                        });

                                    } else {
                                        notificationUtil.processNotification(res, notification, function (processNotificationError) {
                                            if (processNotificationError) {
                                                logger.error('Error while processing  order notification for status change to ' + order.currentStatus + ' for order with Order number ' + order.orderNumber, processNotificationError);
                                                return res.status(400).send({
                                                    status: false,
                                                    message: errorHandler.getErrorMessage(processNotificationError)
                                                });
                                            } else {
                                                todo.save(function (err) {
                                                    if (err) {
                                                        logger.error('Error while creating order todo for status change to ' + order.currentStatus + ' for order with Order number ' + order.orderNumber);
                                                        return res.status(400).send({
                                                            status: false,
                                                            message: errorHandler.getErrorMessage(err)
                                                        });
                                                    } else {
                                                        if (order.seller.nVipaniUser) {
                                                            todoUpdate.target = order.seller.nVipaniUser;
                                                        }
                                                        Todo.update({
                                                            type: todoUpdate.type,
                                                            'source.order': todoUpdate.source.order,
                                                            target: todoUpdate.target
                                                        }, {completed: true}, function (err, numberAffected, rawResponse) {
                                                            if (err) {
                                                                logger.error('Error while creating order todo for status change to ' + order.currentStatus + ' for order with Order number ' + order.orderNumber);
                                                                return res.status(400).send({
                                                                    status: false,
                                                                    message: errorHandler.getErrorMessage(err)
                                                                });
                                                            } else {
                                                                logger.info('Loading the order details with Order number ' + order.orderNumber + ' after creation');
                                                                orderUtil.fetchOrderById(order._id, function (orderResponse) {
                                                                    if (orderResponse instanceof Error) {
                                                                        logger.error('Error while loading the order details with Order number ' + order.orderNumber + ' after creation');
                                                                        return res.status(400).send({
                                                                            message: errorHandler.getErrorMessage(orderResponse)
                                                                        });
                                                                    } else {
                                                                        res.json(orderResponse);
                                                                    }
                                                                });
                                                            }
                                                        });
                                                    }
                                                });
                                            }
                                        });
                                    }
                                });
                            }
                        });
                    }
                });
            } else {
                orderUtil.saveOrder(order, logger, function (updateErr, updateBuyerOrder) {
                    if (updateErr) {
                        logger.error('Failed to update order status', updateErr);
                        return res.status(400).send({
                            status: false,
                            message: errorHandler.getErrorMessage(updateErr)
                        });
                    } else {
                        logger.debug('Successfully updated the order status from  orderStatus:' + oldStatus + 'to  orderStatus:' + order.currentStatus);
                        res.jsonp(order);
                    }

                });

            }
        }
    });


};

/**
 * update products as part of order
 * @param req
 * @param res
 */
exports.updateProducts=function (req,res) {
    var order = req.order;
    order = _.extend(order, req.body);
    order.set('lastUpdated', Date.now());

    var token = req.body.token || req.headers.token;
    logger.info('Updating order with Order number ' + JSON.stringify(order));
    usersJWTUtil.findUserByToken(token, function (userErr, loginuser) {
        if (userErr) {
            logger.error('Error while fetching the user with the given token for updating order with Order number for contact ' + order.orderNumber, userErr);
            return res.status(400).send({
                status: false,
                message: errorHandler.getErrorMessage(userErr)
            });
        }else {
            orderUtil.saveOrder(order,logger,function (err,order) {
                if(err){
                    return res.status(400).send({
                        status: false,
                        message: errorHandler.getErrorMessage(err)
                    });
                }else{
                    res.jsonp(order);
                }

            });

        }

    });

};
/**
 *
 * @param req
 * @param res
 */
exports.updateComments=function (req,res) {

};
exports.updatePayments=function (req,res) {

};
/**
 * Delete an Order
 */
exports.delete = function(req, res) {
    var order = req.order ;

    order.remove(function(err) {
        if (err) {
            logger.error('Error while deleting order with Order number ' + order.orderNumber);
            return res.status(400).send({
                status: false,
                message: errorHandler.getErrorMessage(err)
            });
        } else {
            res.jsonp(order);
        }
    });
};

function getFilterArray(field) {
    var queryObject= {Intra:{'isIntraStock': true},New:{$or:[{'currentStatus': 'Placed'},{'currentStatus':'Confirmed'}]}, Disputed:{'currentStatus':'Disputed'},
        DeliveryPending:{$or:[{'currentStatus':'Shipped'},{'currentStatus':'InProgress'}]},Delivered:{'currentStatus':'Delivered'}};
    for (var key in queryObject) {
        if(field && key===field)
            return queryObject[key];
    }
    return null;
}

function getQuery(summaryFilters,queryArray,done) {
    if(summaryFilters) {
        for (var key in summaryFilters) {
            var value=getFilterArray(key);
            if(value) {
                queryArray.push(value);
            }
        }
    }
    done(queryArray);

}
function getOrderSpecificQuery(isSource,businessUnitId,loginuser,done) {
    var query={};
    if(isSource){
        query={'owner.company': loginuser.company,'owner.businessUnit': businessUnitId};
    }else {
        query = {'currentStatus':{$ne:'Drafted'},
            $or: [{'owner.company': loginuser.company, 'owner.businessUnit': {$ne: businessUnitId}, $or: [
                    {'seller.nVipaniCompany': loginuser.company, 'seller.businessUnit': businessUnitId},
                    {'buyer.nVipaniCompany': loginuser.company, 'buyer.businessUnit': businessUnitId},
                    {'mediator.nVipaniCompany': loginuser.company, 'mediator.businessUnit': businessUnitId}]
            },
                {'owner.company': {$ne: loginuser.company},
                    $or: [
                        {'owner.businessUnit': businessUnitId},
                        {'seller.nVipaniCompany': loginuser.company, 'seller.businessUnit': businessUnitId},
                        {'buyer.nVipaniCompany': loginuser.company, 'buyer.businessUnit': businessUnitId},
                        {'mediator.nVipaniCompany': loginuser.company, 'mediator.businessUnit': businessUnitId}]
                }]
        };

    }
    done(null,query);

}

function orderQuery(req,isSource,done) {
    var token = req.body.token || req.headers.token;
    var lastSyncTime = req.headers.lastsynctime;
    var businessUnitId = req.query.businessUnitId;
    if(!businessUnitId){
        done(new Error('The business unit id is not set'),null);
    }else {
        logger.info('Fetching user my orders');
        usersJWTUtil.findUserByToken(token, function (err, loginuser) {
            if (err) {
                logger.error('Error while fetching the user with the given token for fetching user orders.');
                done(err, null);
            } else {


                var pageOptions = {};
                if (req.query.page && !isNaN(req.query.page)) {

                    pageOptions.page = parseInt(req.query.page);
                }
                if (req.query.limit) {
                    pageOptions.limit = parseInt(req.query.limit);
                }
                getOrderSpecificQuery(isSource,businessUnitId,loginuser,function (err,query) {

                    if (lastSyncTime) {
                        query.lastUpdated = {$gt: lastSyncTime};
                    }
                    var queryArray = [];
                    if (req.query.searchText) {
                        var regExp = {'$regex': req.query.searchText};
                        for (var property in Order.schema.paths) {
                            if (Order.schema.paths.hasOwnProperty(property) && Order.schema.paths[property].instance === 'String') {
                                var eachproduct = {};
                                eachproduct[property] = regExp;
                                queryArray.push(eachproduct);
                            }

                        }
                    }
                    getQuery(req.body.summaryFilters, queryArray, function (queryArray) {
                        var finalQuery = null;
                        if (queryArray.length > 0)
                            finalQuery = {$and: [query, {$or: queryArray}]};
                        else
                            finalQuery = query;
                        done(null, finalQuery, pageOptions);
                    });
                });
                //done(null,query,pageOptions);
            }
        });
    }
}
exports.listOrdersByBusinessUnit = function(req,res){
    orderQuery(req,true,function (queryErr,query,pageOptions) {
        if(queryErr){
            return res.status(400).send({
                status:false,
                message: errorHandler.getErrorMessage(queryErr)
            });
        }else {
            orderUtil.findQueryByOrders([query], 0,true, pageOptions, function (err, orders) {
                if (err) {
                    logger.error('Error while fetching user received orders');
                    return res.status(400).send({
                        status: false,
                        message: errorHandler.getErrorMessage(err)
                    });
                } else {
                    res.jsonp(orders);

                }
            });
        }
    });
};

function checkUserBusinessUnit(bunitId,loginuser,done){
    if(!bunitId){
        done(new Error('Business unit is required to check whether user belongs to the business unit'));
    }
    else if(!loginuser){
        done(new Error('user id is required to check whether user belongs to business unit'));
    }
    else{
        User.findOne(loginuser.id).populate('company').exec(function(err,user){
            if(err){
                done(new Error('no user found with id:'+loginuser.id));
            }
            else{
                var matchingbu=user.company.businessUnits.filter(function(businessunit){
                    return bunitId.toString()===businessunit.businessUnit.toString();
                });
                if(matchingbu.length>0){
                    done(null,matchingbu);
                }
            }
        });
    }
}

function findMatchBusinessUnit(bunit,loginuser,done){
    if(!bunit){
        dbUtil.findDefaultBusinessUnit(loginuser,function(err,bunit) {
            done(err,bunit.businessUnit);
        });
    }else{
        done(null,bunit);
    }
}


function getMatechedOrders(orders,loginuser) {

    return orders.filter(function(doc){

        return  (!((doc.offer && doc.offer.offerType === 'Buy'  ||!doc.offer ) &&  doc.buyer && doc.buyer.nVipaniUser && doc.buyer.nVipaniUser.toString() && doc.buyer.nVipaniUser.toString() === loginuser.id && doc.orderType === 'Sale') || (((doc.offer && doc.offer.offerType === 'Sell' || !doc.offer) && doc.buyer && doc.buyer.nVipaniUser && doc.buyer.nVipaniUser.toString()&& doc.seller&& doc.seller.nVipaniUser && doc.seller.nVipaniUser.toString() === loginuser.id && doc.orderType === 'Purchase')));

    });
}

function matchedPlayerQuery(query){
    // Order type sale order
    //order type purchase order

}
exports.listSpecificOrder = function (req, res) {
    orderQuery(req,false,function (queryErr,query,pageOptions) {
        if(queryErr){
            return res.status(400).send({
                status:false,
                message: errorHandler.getErrorMessage(queryErr)
            });
        }else {
            orderUtil.findQueryByOrders([query], 0,true, pageOptions, function (err, orders) {
                if (err) {
                    logger.error('Error while fetching user received orders - ');
                    return res.status(400).send({
                        status: false,
                        message: errorHandler.getErrorMessage(err)
                    });
                }else {
                    res.jsonp(orders);

                }
            });
        }
    });
};
/**
 * Order middleware
 */
exports.orderByID = function (req, res, next, id) {


    orderUtil.fetchOrderById(id, function (orderResponseErr,orderResponse) {
        if (orderResponseErr) {
            logger.error('Error while loading the order details with Order number ' + id );
            return next(orderResponseErr);
        } else {
            req.order = orderResponse ;
            next();
        }
    });

};

/**
 * Order authorization middleware
 */
exports.hasAuthorization = function(req, res, next) {
    var token = req.body.token || req.headers.token;
    usersJWTUtil.findUserByToken(token, function(err, user) {
        if (err) {
            logger.error('Error while fetching the user with the given token for checking user authorization for order');
            return res.status(400).send({
                status: false,
                message: errorHandler.getErrorMessage(err)
            });
        } else {
            //TODO:
            if((req.order.owner.company!==null && user.company && req.order.owner.company.toString()===user.company.toString()) || ( req.order.seller.nVipaniCompany._id && req.order.seller.nVipaniCompany._id.toString()===user.company.toString()) ||(req.order.buyer.nVipaniCompany._id && req.order.buyer.nVipaniCompany._id.toString()===user.company.toString()) || (req.order.mediator.nVipaniCompany._id && req.order.mediator.nVipaniCompany._id.toString()===user.company.toString())){
                next();
            } else {
                logger.error('User is not authorized');
                return res.status(403).send({
                    status: false,
                    message: 'User is not authorized'
                });
            }
        }
    });

};
exports.orderEnumValues=function (req,res) {
    var orderSchema=Order.schema;
    res.jsonp({
        status: true,
        'currentStatus': {'all':orderSchema.path('currentStatus').enumValues,'default':orderSchema.path('currentStatus').defaultValue},
        'orderType':{'all':orderSchema.path('orderType').enumValues,'default':orderSchema.path('orderType').defaultValue},
        'paymentMode':{'all':orderSchema.path('paymentOptionType.payOnDelivery.paymentMode').enumValues,'default':orderSchema.path('paymentOptionType.payOnDelivery.paymentMode').defaultValue},
        'paymentstatus':{'all':orderSchema.path('paymentOptionType.payOnDelivery.status').enumValues,'default':orderSchema.path('paymentOptionType.payOnDelivery.status').defaultValue},
        'statusReason':{'all':orderSchema.path('statusReason.reason').enumValues,'default':orderSchema.path('statusReason.reason').defaultValue}
    });
};

exports.massDelete=function (req,res) {
    var token = req.body.token || req.headers.token;
    var orders=req.body.orders;
    var businessUnit=req.body.businessUnit;
    var type=req.body.type;
    usersJWTUtil.findUserByToken(token, function (err, user) {
        if(err){
            return res.status(400).send({
                status:false,
                message: errorHandler.getErrorMessage(err)
            });
        }else {
            if(orders && orders.length>0) {
                var query={'owner.company':user.company};
                if(orders && orders.length>0){
                    query.$or=orders;
                }
                orderUtil.batchOrderUpdate(query,{'deleted':true}, function (orderErr, results) {
                    if(orderErr){
                        return res.status(400).send({
                            status:false,
                            message: errorHandler.getErrorMessage(orderErr)
                        });
                    }else  if(!results){
                        return res.status(400).send({
                            message: 'Not Deleted',
                            status:false
                        });
                    }else {
                        //res.jsonp(orders);
                        orderUtil.findQueryByOrders([{'owner.businessUnit':businessUnit.toString()}],0,false,{page:0,limit:10},function (errOrders,orders) {
                            if(errOrders){
                                return res.status(400).send({
                                    status:false,
                                    message: errorHandler.getErrorMessage(errOrders)
                                });
                            }else{
                                res.jsonp(orders);
                            }

                        });
                    }

                });
            }else{
                return res.status(400).send({
                    status:false,
                    message: 'No Selected Orders'
                });
            }
        }
    });
};

exports.getOrdersByOffer=function (req,res) {
    var token = req.body.token || req.headers.token;
    var orders=req.body.orders;
    var offerId = req.params.orderOfferId;
    var type=req.body.type;
    usersJWTUtil.findUserByToken(token,function (err,user) {
        if(err){
            return res.status(400).send({
                status:false,
                message: errorHandler.getErrorMessage(err)
            });
        }else {
            offerUtil.getOrdersByOffer(offerId, user,function (ordersErr, orders) {
                if(ordersErr) {
                    return res.status(400).send({
                        status: false,
                        message: errorHandler.getErrorMessage(ordersErr)
                    });
                }else{
                    res.jsonp(orders);
                }
            });
        }
    });

};
exports.getSourceSummaries=function (req,res) {
    orderQuery(req,true,function(err,query,pageOptions){
        if(err) {
            return res.status(400).send({
                status: false,
                message: errorHandler.getErrorMessage(err)
            });
        }else{
            orderUtil.findQueryByOrders([query], 0,false, pageOptions, function (err, orders) {
                if (err) {
                    logger.error('Error while fetching user my orders ');
                    return res.status(400).send({
                        status: false,
                        message: errorHandler.getErrorMessage(err)
                    });
                } else {
                    orderUtil.getOrdersSummary(orders.order, true, req.body.summaryFilters, function ( summaryData) {
                        res.jsonp(summaryData);
                    });
                }
            });

        }
    });
};
exports.getTargetSummaries=function (req,res) {
    orderQuery(req,false,function(err,query,pageOptions){
        if(err) {
            return res.status(400).send({
                status: false,
                message: errorHandler.getErrorMessage(err)
            });
        }else{
            orderUtil.findQueryByOrders([query], 0,false, pageOptions, function (err, orders) {
                if (err) {
                    logger.error('Error while fetching user received orders ');
                    return res.status(400).send({
                        status: false,
                        message: errorHandler.getErrorMessage(err)
                    });
                } else {
                    orderUtil.getOrdersSummary(orders.order,false,req.body.summaryFilters,   function ( summaryData) {
                        res.jsonp(summaryData);
                    });
                }
            });

        }

    });

};
exports.getPaymentTerms=function (req,res) {
    var paymentOrderId=req.query.paymentOrderId;
    orderUtil.getOrderPaymentTerms(paymentOrderId,null,null,[],true,function (err,paymentTerms) {
        if(err){
            return res.status(400).send({
                message: errorHandler.getErrorMessage(err)
            });
        }else{
            res.jsonp(paymentTerms);
        }

    });
};
exports.getPaymentModes=function (req,res) {
    var paymentOrderId=req.query.paymentOrderId;
    orderUtil.getOrderPaymentModes(paymentOrderId,null,null,[],true,function (err, paymentModes) {

        if(err){
            return res.status(400).send({
                message: errorHandler.getErrorMessage(err)
            });
        }else{
            res.jsonp(paymentModes);
        }
    });

};
