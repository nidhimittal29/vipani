'use strict';

/**
 * Module dependencies.
 */
var mongoose = require('mongoose'),
    errorHandler = require('./errors.server.controller'),
    usersJWTUtil = require('./utils/users.jwtutil'),
    logger = require('../../lib/log').getLogger('PAYMENTTERMS', 'DEBUG'),
    User = mongoose.model('User'),
    UserGroup = mongoose.model('UserGroup'),
    async = require('async'),
    fs = require('fs'),
    _ = require('lodash');

/**
 * Create a UserGroup
 */

exports.create = function (req, res) {
    var token = req.body.token || req.headers.token;
    var userGroup = new UserGroup(req.body);

    logger.debug('Creating UserGroup -' + JSON.stringify(userGroup));

    var date = Date.now();
    userGroup.set('created', date);
    userGroup.set('lastUpdated', date);

    usersJWTUtil.findUserByToken(token, function (err, user) {
        if (err) {
            //logger.debug('err 1 -'+err);
            //logger.debug('err 1 -'+JSON.stringify(err));
            return res.status(400).send({
                message: errorHandler.getErrorMessage(err)
            });
        }
        userGroup.user = user;
        userGroup.lastUpdatedUser = user;

        userGroup.save(function (saveUserGroupErr) {
            if (saveUserGroupErr) {
                logger.error('Error while creating UserGroup.', saveUserGroupErr);
                return res.status(400).send({
                    message: errorHandler.getErrorMessage(saveUserGroupErr)
                });
            } else {
                res.jsonp(userGroup);
            }
        });
    });
};

/**
 * Show the current UserGroup
 */
exports.read = function (req, res) {
    res.jsonp(req.userGroup);
};

/**
 * Update a UserGroup
 */
exports.update = function (req, res) {
    var userGroup = req.userGroup;
    var token = req.body.token || req.headers.token;

    var versionKey = userGroup.userGroupVersionKey;

    userGroup = _.extend(userGroup, req.body);

    userGroup.userGroupVersionKey = versionKey;

    userGroup.set('lastUpdated', Date.now());

    usersJWTUtil.findUserByToken(token, function (err, user) {
        if (err) {
            //logger.debug('err 1 -'+err);
            //logger.debug('err 1 -'+JSON.stringify(err));
            return res.status(400).send({
                message: errorHandler.getErrorMessage(err)
            });
        }
        userGroup.lastUpdatedUser = user;

        userGroup.save(function (saveUserGroupErr) {
            if (saveUserGroupErr) {
                logger.error('Error while creating Contact.', saveUserGroupErr);
                return res.status(400).send({
                    message: errorHandler.getErrorMessage(saveUserGroupErr)
                });
            } else {
                res.jsonp(userGroup);
            }
        });
    });
};


/**
 * Delete an UserGroup
 */

exports.delete = function (req, res) {
    var userGroup = req.userGroup;

    var token = req.body.token || req.headers.token;

    var versionKey = userGroup.userGroupVersionKey;

    userGroup = _.extend(userGroup, req.body);

    userGroup.userGroupVersionKey = versionKey;

    userGroup.deleted = true;
    userGroup.set('lastUpdated', Date.now());

    usersJWTUtil.findUserByToken(token, function (err, user) {
        if (err) {
            //logger.debug('err 1 -'+err);
            //logger.debug('err 1 -'+JSON.stringify(err));
            return res.status(400).send({
                message: errorHandler.getErrorMessage(err)
            });
        }
        userGroup.lastUpdatedUser = user;

        userGroup.save(function (saveUserGroupErr) {
            if (saveUserGroupErr) {
                logger.error('Error while creating Contact.', saveUserGroupErr);
                return res.status(400).send({
                    message: errorHandler.getErrorMessage(saveUserGroupErr)
                });
            } else {
                res.jsonp(userGroup);
            }
        });
    });
};

/**
 * List of UserGroups
 */
exports.list = function (req, res) {
    var token = req.body.token || req.headers.token;
    if (token) {
        usersJWTUtil.getUserByToken(token, function (err, loginuser) {
            if (loginuser) {
                var query;
                query = UserGroup.find({$and:[{deleted:false},{disabled:false},{$or: [{user: loginuser.id}, {$and: [{$or:[{user:{$exists:false}},{user:{$exists:true}}]},{user:null}]}]}]});
                query.sort('-created').populate('user', 'displayName').exec(function (err, userGroups) {
                    if (err) {
                        return res.status(400).send({
                            message: errorHandler.getErrorMessage(err)
                        });
                    } else {
                        res.jsonp(userGroups);
                    }
                });
            }
        });
    } else {
        var query;
        query = UserGroup.find({$and: [{deleted: false},{disabled:false},  {$and: [{$or:[{user:{$exists:false}},{user:{$exists:true}}]},{user:null}]}]});
        query.sort('-created').populate('user', 'displayName').exec(function (err, userGroups) {
            if (err) {
                return res.status(400).send({
                    message: errorHandler.getErrorMessage(err)
                });
            } else {
                res.jsonp(userGroups);
            }
        });
    }

};

/**
 * Contact middleware
 */
exports.userGroupByID = function (req, res, next, id) {
    UserGroup.findById(id).populate('user', 'displayName').exec(function (err, userGroup) {
        if (err) return next(err);
        if (!userGroup) return next(new Error('Failed to load UserGroup ' + id));
        req.userGroup = userGroup;
        next();
    });
};

/**
 * UserGroup authorization middleware
 */
exports.hasAuthorization = function (req, res, next) {
    var token = req.body.token || req.headers.token;
    usersJWTUtil.getUserByToken(token, function (err, user) {
        if (err) {
            if (err) {
                return res.status(400).send({
                    message: errorHandler.getErrorMessage(err)
                });
            }
        } else {
            if ((req.userGroup.user !== null) && (req.userGroup.user.id.toString() !== user.id)) {
                return res.status(403).send('User is not authorized');
            } else {
                next();
            }
        }
    });
};

