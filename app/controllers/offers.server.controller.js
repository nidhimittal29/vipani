'use strict';

/**
 * Module dependencies.
 */
var mongoose = require('mongoose'),
    errorHandler = require('./errors.server.controller'),
    usersJWTUtil = require('./utils/users.jwtutil'),
    notificationUtil = require('./utils/notification.util'),
    offerUtil = require('./utils/common.offer.util'),
    dbUtil = require('./utils/common.db.util'),
    inventoriesUtil = require('./utils/common.inventories.util'),
    contactUtil = require('./utils/common.contacts.util'),
    companyUtil = require('./utils/common.company.util'),
    bUtil=require('./utils/common.businessunit.util'),
    Company = mongoose.model('Company'),
    logger = require('../../lib/log').getLogger('OFFERS', 'DEBUG'),
    Offer = mongoose.model('Offer'),
    Order = mongoose.model('Order'),
    User = mongoose.model('User'),
    Contacts = mongoose.model('Contact'),
    Inventory = mongoose.model('Inventory'),
    Notification = mongoose.model('Notification'),
    commonUtil = require('./utils/common.util'),
    async = require('async'),
    _ = require('lodash');

/**
 * Create offer notifications to all to contacts
 */
function createOfferNotifications(offer, notificationsContacts, user, res, done) {
    logger.info('Creating offer notifications to all to contacts for the offer - ' + offer.offerNumber);
    if (notificationsContacts && notificationsContacts) {
        async.each(notificationsContacts, function (eachnotificationContact, callback) {
            var sellerNotification = new Notification();
            sellerNotification.user = user;
            sellerNotification.target.contact = eachnotificationContact.contact;
            sellerNotification.target.nVipaniUser = eachnotificationContact.nVipaniUser;
            sellerNotification.target.nVipaniCompany = eachnotificationContact.nVipaniCompany;

            sellerNotification.source.offer = offer;
            sellerNotification.type = 'Offer';

            sellerNotification.channel = ['Email', 'SMS', 'Push'];

            sellerNotification.title = (user.displayName ? user.displayName : user.username) + ' ' + offer.offerStatus.toLowerCase() + ' the offer with offer name ' + offer.name + ' and offer number ' + offer.offerNumber;

            sellerNotification.shortMessage = (user.displayName ? user.displayName : user.username) + ' ' + offer.offerStatus.toLowerCase() + ' the offer with offer name ' + offer.name + '. From nVipani Team';

            sellerNotification.save(function (notSaveErr) {
                if (notSaveErr) {
                    logger.error('Error while saving the notification ' + sellerNotification.title, notSaveErr);
                    callback(notSaveErr);
                } else {
                    notificationUtil.processNotification(res, sellerNotification, function (processNotificationError) {
                        callback(processNotificationError);
                    });
                }
            });
        }, function (err) {
            if (err) {
                done(err);
            } else {
                done();
            }
        });
    } else {
        done();
    }

}

/**
 *  Fetch the offer Contacts and Groups
 */
function getOfferContacts(toContacts, user, notifications, done) {

    var query = [];
    contactUtil.findContactsQuery(toContacts, query, function (errQuery, query) {
        if (errQuery) {
            done(errQuery, null);
        } else {
            contactUtil.findQueryByContacts([{$or: query}], 1, false, [], function (err, contacts) {
                if (err) {
                    done(err, null);
                } else {
                    async.forEachSeries(contacts, function (eachContact, callback) {
                        var contact = {};
                        if (eachContact && !eachContact.disable) {
                            contact.contact = eachContact._id;
                            contact.nVipaniUser = eachContact.nVipaniUser;
                            contact.nVipaniCompany = eachContact.nVipaniCompany;
                            notifications.push(contact);
                            callback();
                        }
                    }, function (notificationErr) {
                        if (!notificationErr) done(null, notifications);
                        else done(notificationErr, null);
                    });
                }

            });
        }
    });
}


/**
 * save Offer and send notification if there is an alert.
 */
function offerSave(offer, notificationsContacts, user, isSendNotification, res, done) {
    if (offer.validTill && offer.validTill.getTime() <= offer.created.getTime()) {
        offer.offerStatus = 'Expired';
    }
    offer.save(function (err) {
        if (err) {
            logger.error('Error while saving offer with offer name ' + offer.name, err);
            done(err, null);
        } else {
            if (isSendNotification) {
                createOfferNotifications(offer, notificationsContacts, user, res, function (createOfferNotificationErr) {
                    if (createOfferNotificationErr) {
                        logger.error('Error while creating offer notifications for offer name ' + offer.name, createOfferNotificationErr);
                    }
                    logger.info('Loading the offer details with offer number ' + offer.offerNumber + ' after creation');
                    offerUtil.fetchOfferById(offer._id, 3, function (errResult, offerResponse) {
                        if (errResult) {
                            logger.error('Error while loading the offer details with offer number ' + offer._id + ' after creation');
                            /*return res.status(400).send({
                             message: errorHandler.getErrorMessage(offerResponse)
                             });*/
                        }
                        done(err, offerResponse);

                    });
                });
            } else {
                logger.info('Loading the offer details with offer number ' + offer.offerNumber + ' after creation');
                offerUtil.fetchOfferById(offer._id, 3, function (errResult, offerResponse) {
                    if (errResult) {
                        logger.error('Error while loading the offer details with offer number ' + offer._id + ' after creation');
                        /*return res.status(400).send({
                         message: errorHandler.getErrorMessage(offerResponse)
                         });*/
                    }
                    done(errResult, offerResponse);
                });
            }
        }
    });

}

function getOfferCriteria(offer,data,done) {
    if(data.inventoryItemsClassification && data.inventoryItemsClassification.classificationCriteria.criteria) {
        if (offerUtil.isOfferProductCriteriaSpecified(offer)) {
            var criteria = [], isAll = false;
            async.forEachSeries(data.inventoryItemsClassification.classificationCriteria.criteria, function (eachCriteria, callback) {
                var eachField = {};
                if (eachCriteria.category && eachCriteria.category === -1) {
                    isAll = true;
                } else {
                    if (eachCriteria.category && eachCriteria.category !== -1) {
                        eachField.category = eachCriteria.category;
                    }
                    if (eachCriteria.productBrand && eachCriteria.productBrand !== -1) {
                        eachField.productBrand = eachCriteria.productBrand;
                    }
                    if (eachCriteria.unitOfMeasure && eachCriteria.unitOfMeasure !== -1) {
                        eachField.unitOfMeasure = eachCriteria.unitOfMeasure;
                    }
                    if (eachField.category || eachField.productBrand || eachField.unitOfMeasure) {
                        criteria.push(eachField);
                    }
                }
                callback();
            }, function () {
                if (!isAll && criteria.length === 0) {
                    done(new Error('No Proper Criteria'), offer, true);
                } else {
                    if (isAll) {
                        offer.inventoryItemsClassification.classificationCriteria.criteriaType = 'All';
                    } else {
                        offer.inventoryItemsClassification.classificationCriteria.criteria = criteria;
                    }
                    done(null, offer, true);
                }
            });
        } else {
            offer.inventoryItemsClassification.classificationCriteria.criteria = [];
            done(null, offer, false);
        }
    }else{
        done(null,offer,false);
    }
}

function offerFieldsSave(offer,data,user,res,done) {
    dbUtil.getOfferPaymentTerms(offer.owner.company,data.paymentTerms,null,[],false,function (paymentErr,paymentTerms) {
        if(paymentErr){
            done(paymentErr,null);
        }else {
            dbUtil.getOfferPaymentModes(offer.owner.company, data.paymentModes, null, [],false, function (paymentModesErr, paymentModes) {
                if (paymentModesErr) {
                    done(paymentModesErr, null);
                } else {
                    if (paymentTerms && paymentTerms.length > 0) {
                        offer.applicablePaymentTerms = paymentTerms;
                    }
                    if (paymentModes && paymentModes.length > 0) {
                        offer.applicablePaymentModes = paymentModes;
                    }
                    getOfferCriteria(offer, data, function (offerCriteriaErr, foundOffer, isCriteria) {
                        if (offerCriteriaErr) {
                            done(offerCriteriaErr, null);
                        } else {
                            /* offer=new Offer(foundOffer);*/
                            var config = require('../../config/config');
                            offerSave(foundOffer, foundOffer.notificationsContacts, user, config.sendNotification, res, function (err, response) {
                                done(err, response);
                            });
                        }
                    });
                }
            });
        }

    });
}
/**
 * Place Offer with notification contacts
 */
function offerPlaced(offer, data,user, changeType, updateChange, level, res, done) {
    //Send Notifications only if there are contacts in offer.
    if (updateChange === 1 && offer.toContacts && ((offer.toContacts.contacts && offer.toContacts.contacts.length > 0) || (offer.toContacts.groups && offer.toContacts.groups.length > 0 || (offer.toContacts.businessUnits && offer.toContacts.businessUnits.length > 0)))) {
        var notificationContacts = [];
        getOfferContacts(offer.toContacts, user, notificationContacts, function (err, contacts) {
            if (err) {
                done(err, null);
            } else {
                offer.notificationsContacts = contacts;

                /* offer=new Offer(foundOffer);*/
                if(offer.toContacts.businessUnits && offer.toContacts.businessUnits.length>0 ){
                    offer.isIntraCompany=true;
                }else{
                    offer.isIntraCompany=false;
                }
                offerFieldsSave(offer,data,user,res,function (offerFieldsSaveErr,response) {
                    done(offerFieldsSaveErr, response);

                });
            }

        });

    } else {
        //save offer without send notification.
        offerFieldsSave(offer,data,user,res,function (offerFieldsSaveErr,response) {
            done(offerFieldsSaveErr, response);

        });
    }
}

function offerRollBack(offer, user, changeType, res, done) {
    /*updateInventoryOfferProductCount(offer, user, changeType, function (updateErr) {
        if (updateErr) {
            logger.error('Error while updating inventory offer product count while creating the offer with offer name ' + offer.name, updateErr);
            done(updateErr);
            /!*return res.status(400).send({
             message: errorHandler.getErrorMessage(updateErr)
             });*!/
        } else {*/
    offer.save(function (err) {
        if (err) {
            logger.error('Error while saving offer with offer name ' + offer.name, err);
            done(err);
            /*return res.status(400).send({
             message: errorHandler.getErrorMessage(err)
             });*/
        } else {
            if (offer.notificationsContacts && offer.notificationsContacts.length > 0) {
                createOfferNotifications(offer, offer.notificationsContacts, user, res, function (createOfferNotificationErr) {
                    if (createOfferNotificationErr) {
                        logger.error('Error while creating offer notifications for offer name ' + offer.name, createOfferNotificationErr);
                    }
                    logger.info('Loading the offer details with offer number ' + offer.offerNumber + ' after creation');
                    offerUtil.fetchOfferById(offer._id, 3, function (errResult, offerResponse) {
                        if (errResult) {
                            logger.error('Error while loading the offer details with offer number ' + offer._id + ' after creation');
                            /*return res.status(400).send({
                             message: errorHandler.getErrorMessage(offerResponse)
                             });*/
                        }
                        done(errResult, offerResponse);
                    });
                });
            } else {
                logger.info('Loading the offer details with offer number ' + offer.offerNumber + ' after creation');
                offerUtil.fetchOfferById(offer._id, 3, function (errResult, offerResponse) {
                    if (errResult) {
                        logger.error('Error while loading the offer details with offer number ' + offer._id + ' after creation');
                        /*return res.status(400).send({
                         message: errorHandler.getErrorMessage(offerResponse)
                         });*/
                    }
                    done(errResult, offerResponse);
                });
            }
        }
    });
    /*    }
    });*/

}

/**
 * Create a Offer
 */
exports.create = function (req, res) {
    var data=req.body;
    var offer = new Offer(req.body);
    logger.debug('Create Offer with the data:'+JSON.stringify(req.body));

    var token = req.body.token || req.headers.token;
    usersJWTUtil.findUserByToken(token, function (err, user) {
        if (err) {
            return res.status(400).send({
                status: false,
                message: errorHandler.getErrorMessage(err)
            });
        }else{

            logger.debug('Create Offer with the data:'+JSON.stringify(user));
            companyUtil.findDefaultCompanyBusinessUnit(user.company,offer.businessUnit, function (bunitErr, bunit) {
                if (bunitErr) {
                    return res.status(400).send({
                        status: false,
                        message: 'Default business unit could not be found'
                    });
                }
                else {
                    offer.user = user;
                    if (offer.validTill) {
                        offer.validTill.setHours(23, 59, 5, 999);
                    }
                    /*if(offer.validFrom){
                        offer.validFrom
                    }*/
                    offer.offerStatus='Opened';
                    offer.notificationsContacts = [];
                    offer.updateHistory = [];
                    offer.businessUnit = bunit;
                    offer.company=user.company;
                    offer.owner={user:user,company:user.company,businessUnit:bunit};
                    // Check the offer data validation before creating the offer.
                    logger.debug('Check the offer data validation before creating the offer :'+JSON.stringify(offer));
                    offerUtil.offerDataValidation(offer,inventoriesUtil,logger,function (errOffer) {
                        if(errOffer){
                            return res.status(400).send({
                                status: false,
                                message: errorHandler.getErrorMessage(errOffer)
                            });
                        }else {
                            // Populate the notificationContacts in the server
                            // Get all contacts of the user
                            offerPlaced(offer, data,user, 'OfferCreation', 1, 1, res, function (createOfferErr, response) {
                                if (createOfferErr) {
                                    return res.status(400).send({
                                        status: false,
                                        message: errorHandler.getErrorMessage(createOfferErr)
                                    });
                                } else {
                                    res.json(response);
                                }

                            });
                        }
                    });
                }

            });
        }
    });
};

/**
 * Show the current Offer
 */
exports.read = function (req, res) {
    res.jsonp(req.offer);
};

/**
 *find Order with populate tables
 */
function fetchOrderById(orderId, done) {
    Order.findById(orderId).populate('offer', 'offerType offerNumber').populate('buyer.contact seller.contact mediator.contact', 'firstName lastName middleName addresses displayName').populate('products.inventory', '-updateHistory -numberOfUnitsHistory -offerUnitsHistory -buyOfferUnitsHistory').populate('user', 'displayName').exec(function (err, resultOrder) {

        /*Order.findById(order._id).populate('toContacts.contacts.contact toContacts.groups.group notificationsContacts.nVipaniUser', 'firstName lastName middleName name').populate('products.inventory').populate('user', 'displayName').exec(function (err, resultOrder) {
         */
        if (err) {
            logger.error('Error while loading the Order details with order id- ' + orderId);
            done(err);
        } else {
            Order.populate(resultOrder, {
                path: 'products.inventory.product',
                model: 'Product',
                select: 'name productImageURL1'
            }, function (nestedErr, populatedResultOrder) {
                if (nestedErr) {
                    done(nestedErr);
                } else {
                    done(populatedResultOrder);
                }
            });
        }

    });
}

/**
 * Update a Offer
 */

/*/
 *find Product with populate tables
 */
function fetchSellerProductInventory(offerProduct, offer,order, loginuser, done) {
    var id=(offerProduct.inventory&& offerProduct.inventory._id) ?offerProduct.inventory._id:(offerProduct.inventory?offerProduct.inventory:offerProduct);
    if (offer && offer.offerType === 'Buy') {
        // this needs to be changes with respective to business unit or company
        if (order.seller.businessUnit && offer.businessUnit.toString() !== order.seller.businessUnit.toString() && offer.offerType === 'Buy') {
            inventoriesUtil.findInventoryById(id,-1,function (errResponse) {
                if(errResponse instanceof Error){
                    done(errResponse);
                }else {
                    var query = {
                        businessUnit: order.seller.businessUnit,
                        productCategory: errResponse.productCategory,
                        productBrand: errResponse.productBrand,
                        unitOfMeasure: errResponse.unitOfMeasure
                    };
                    if (offerProduct.MOQ) {
                        query.numberOfUnits = {$gt: offerProduct.MOQ};
                    }
                    inventoriesUtil.findQueryByInventories([query], -2, false, null, function (product) {
                        if (product instanceof Error) {
                            logger.debug('Fetch inventory from DB for the inventory ID');

                        }
                        done(product.length>0?product[0]:null);

                    });
                }
            });
        }else{
            done(offerProduct);
        }
    } else {
        //Inventory needs to be fetched from DB as there could be multiple updated on the same inventory.
        if (id) {
            logger.debug('Fetch inventory from DB for the inventory ID' + id);
            inventoriesUtil.findInventoryById(id, -1, function (inventory) {
                if (inventory instanceof Error) {
                    logger.error(inventory.message);
                } else {
                    offerProduct.inventory = inventory;

                }
                done(offerProduct);
            });
        } else {
            logger.error('No inventory with the inventory ID' + JSON.stringify(offerProduct));
            done(offerProduct);
        }
    }

}

function findCompanyByUserId(userId, done) {
    User.findById(userId,'company').populate('company').exec( function (errUser, user) {
        if (errUser) {
            done(errUser, null);
        } else if (user) {
            done(errUser, user.company);
        } else {
            done(new Error('No offer Company'), null);
        }
    });

}
function getOfferPayment(offer,done) {
    if(offer && ((offer.applicablePaymentTerms && offer.applicablePaymentTerms instanceof Array && offer.applicablePaymentTerms.length>0)||(offer.applicablePaymentModes && offer.applicablePaymentModes instanceof Array && offer.applicablePaymentModes.length>0))){
        done(null,offer.applicablePaymentTerms,offer.applicablePaymentModes);
    }else{
        done(null,null,null);
    }

}
function getSellerCompany(loginuser,done) {
    //Get the company information by offer company or user company id

    findCompanyByUserId(loginuser,function (offerCompanyErr, offerCompany) {
        if (offerCompanyErr) {
            logger.error('Failed to fetch the company while creating the order. Error:' + offerCompanyErr.message);
            /* return res.status(400).send({
                 status: false,
                 message: errorHandler.getErrorMessage(offerCompanyErr)
             });*/
            done(offerCompanyErr, null);
        } else {
            //Set the company information to the order by offer user or loginuser  id

            done(null, offerCompany);

        }
    });
}
// This case need to be seller friendly
function setContactPayment(order,done) {
    Contacts.findById(order.buyer.contact,'paymentTerm',function (contactErr,contact) {
        if(contactErr){
            done(contactErr,null);
        }else if(contact){
            done(null,contact.paymentTerm);
        }else{
            done(null,null);
        }
    });


}

function findContactOrOfferPayments(offer,order, done) {
    // Offer Payment Terms
    getOfferPayment(offer,function (paymentTerms) {
        if(paymentTerms && paymentTerms.isArray && paymentTerms.length>0){
            done(null,paymentTerms);
        }else {
            setContactPayment(order, function (contactErr, paymentContact) {
                if(contactErr){
                    done(contactErr,order);
                }else {
                    // Contact Payment terms
                    done(null,paymentContact?[paymentContact]:null);
                }

            });
        }
    });


}
function orderPaymentTerms(terms) {
    if(terms && terms instanceof Array && terms.length>0){
        var currentTerms=[];
        terms.forEach(function (eachTerm) {  if(eachTerm.selected){
            currentTerms.push({paymentTerm:eachTerm.paymentTerm,selected:eachTerm.selected});
        }});
        return currentTerms;
    }else {
        return null;
    }
}
function orderPaymentModes(modes) {
    if(modes && modes instanceof Array && modes.length>0){
        var currentModes=[];
        modes.forEach(function (eachMode) {  if(eachMode.selected){
            currentModes.push({paymentMode:eachMode.paymentMode,selected:true});
        }});
      return currentModes;
    }else {
        return null;
    }
}
function setOrderPaymentTerms(offerPayments,offerPaymentModes,offerCompany,order,done) {
    if(offerPayments){
        order.applicablePaymentModes=orderPaymentModes(offerPaymentModes);
        order.applicablePaymentTerms=orderPaymentTerms(offerPayments);
        done(null,order);
    }else if (offerCompany){
        order.applicablePaymentModes=orderPaymentModes(offerCompany.settings.paymentModes);
        order.applicablePaymentTerms=orderPaymentTerms(offerCompany.settings.paymentTerms);
        done(null,order);
    }else{
        done(new Error('No Payment Terms'),null);
    }

}

/**
 * fetch Registered contact with user
 * @param userId
 * @param done
 */
function getRegisterContactByUserId(userId, done) {
    Contacts.findOne({
        user: userId,
        nVipaniRegContact: true
    }, function (errContact, contact) {
        if (errContact) {
            done(errContact, null);
        } else if (!contact) {
            done(new Error('No default Contact with the user Id ' + userId), null);
        } else {
            done(errContact, contact);
        }
    });
}
function getMatchedObject(offerProduct,offer,done) {
    var numberOfUnits=0,unitPrice=0,amount=0;
    if(offerProduct && offerProduct instanceof Object){
        if(offerProduct.hasOwnProperty('inventory')){
            if(offer && offer.offerType && (offer.offerType === 'Buy'|| offer.offerType === 'Sell')) {
                    numberOfUnits = offerProduct.MOQ?offerProduct.MOQ:commonUtil.getMinQuantity(offerProduct.inventory, offer.offerType === 'Buy');
                    unitPrice = commonUtil.getQuantityPrice(offer && offer.offerType === 'Buy' ?offerProduct.inventory.moqAndBuyMargin:offerProduct.inventory.moqAndMargin, numberOfUnits, offerProduct.inventory.MRP);
                    if(!numberOfUnits){
                        numberOfUnits=(offerProduct.inventory.numberOfUnits>0?1:0);
                    }
                    if(!unitPrice){
                        unitPrice=offerProduct.inventory.MRP?offerProduct.inventory.MRP:0;
                    }
                done(null,{
                    inventory: offerProduct.inventory._id,
                        numberOfUnits: numberOfUnits,
                    numberOfUnitsAvailable: offerProduct.inventory.numberOfUnits,
                    unitPrice:unitPrice? unitPrice.toFixed('2'):0,
                    MOQ: numberOfUnits?numberOfUnits:(offerProduct.numberOfUnits>0?1:0),
                    unitMrpPrice: offerProduct.inventory.MRP,
                    margin: (offerProduct.inventory.MRP? ((offerProduct.inventory.MRP - unitPrice) * 100 / offerProduct.inventory.MRP):0).toFixed('2')
                },numberOfUnits * unitPrice);
            }else{

            }


        }else{
            numberOfUnits=commonUtil.getMinQuantity(offerProduct, false);
            unitPrice=commonUtil.getQuantityPrice(offerProduct.moqAndMargin, numberOfUnits, offerProduct.MRP);
            done(null,{
                inventory: offerProduct._id,
                numberOfUnits: numberOfUnits,
                numberOfUnitsAvailable: offerProduct.numberOfUnits,
                unitPrice: unitPrice ?unitPrice.toFixed('2'):offerProduct.MRP,
                MOQ: numberOfUnits?numberOfUnits:(offerProduct.numberOfUnits>0?1:0),
                unitMrpPrice: offerProduct.MRP,
                margin: (offerProduct.MRP ? ((offerProduct.MRP - unitPrice) * 100 / offerProduct.MRP ): 0).toFixed('2')
            },amount);
        }

    }else{
        done(null,null);

    }
}
function setOrderProductObject(offerProduct,offer,done) {
    getMatchedObject(offerProduct,offer,function (err,finalProduct,amount) {
        if(err){
            done(err,null,amount);
        }else{
            done(null,finalProduct,amount);
        }
    });
}
/**
 *
 * @param products
 * @param offer
 * @param order
 * @param loginuser
 * @param done
 */
function setSellerProductsToOrder(productFields, order, loginuser, done) {
    var products = productFields.products;
    var offer = productFields.offer;
    if(order) {
        if (!order.products) {
            order.products = [];
        }

        if (products && products.length !== 0) {
            async.forEachSeries(products, function (eachOfferProduct, callback) {
                var numberOfUnits = 0;
                var unitPrice = 0;
                var errMesssage;
                fetchSellerProductInventory(eachOfferProduct, offer, order, loginuser, function (offerProduct) {
                    setOrderProductObject(offerProduct,offer,function (err,eachProduct,amount) {
                        if(err){
                            callback(err);
                        }else if(eachProduct && eachProduct.hasOwnProperty('inventory')){
                            order.products.push(eachProduct);
                            order.totalAmount=+amount;
                        }else{
                            callback(new Error('No matched Product'));
                        }

                    });
                    callback(errMesssage);
                });
            }, function (err) {
                done(err, order);
            });
        } else {
            done(new Error('No Products'), order);
        }
    }else{
        done(new Error('No order'),order);
    }
}
function setContact(eachContact) {
    var finalContact = {};
    if (eachContact._id) {
        finalContact.contact = eachContact._id.toString();
    }
    if (eachContact.nVipaniUser) {
        finalContact.nVipaniUser = eachContact.nVipaniUser;
    }
    if (eachContact.businessUnit) {
        finalContact.businessUnit = eachContact.businessUnit;
    }
    if (eachContact.nVipaniCompany) {
        finalContact.nVipaniCompany = eachContact.nVipaniCompany;
    }
    return finalContact;
}

/**
 * get the seller with order type
 * @param order
 * @param playerFields
 * @param done
 */

function setOrderSeller(order, playerFields, done) {
    var offer = playerFields.offer;
    var orderValue = playerFields.dataFields;
    var businessUnit = playerFields.businessUnit;
    var loginuser = playerFields.loginuser;
    var offerContact = playerFields.contact;
    var loginRegContact = playerFields.loginRegContact;
    if (offer && offer.offerType === 'Buy' && offer.businessUnit.toString() !== businessUnit.toString()) {
        if(!businessUnit){
            done(new Error('No Business unit for seller'),null);
        }else if(businessUnit && businessUnit===offerContact.businessUnit){
            done(new Error('Same Business unit for both seller and buyer'),null);
        }else {
            loginRegContact.businessUnit = businessUnit;
            order.orderType = 'Sale';
            done(null, setContact(loginRegContact));
        }

    } else if (offer && offer.offerType === 'Sell' && offer.businessUnit.toString() === businessUnit.toString()) {
        order.orderType = 'Sale';
        loginRegContact.businessUnit=offer.businessUnit;
        done(null, setContact(loginRegContact));

    } else if (offer && offer.offerType === 'Sell' && offer.businessUnit.toString() !== businessUnit.toString()) {
        offerContact.businessUnit=offer.businessUnit;
        order.orderType = 'Purchase';

        done(null, setContact(offerContact));

    } else if (!offer && order && order.orderType === 'Sale') {
        loginRegContact.businessUnit=businessUnit;
        done(null, setContact(loginRegContact));
    } else {
        done(null, {});
    }

}


/**
 * get the buyer with order type
 * @param order
 * @param playerFields
 * @param done
 */
function setOrderBuyer(order, playerFields, done) {
    var offer = playerFields.offer;
    var orderValue = playerFields.dataFields;
    var businessUnit = playerFields.businessUnit;
    var loginuser = playerFields.loginuser;
    var offerContact = playerFields.contact;
    var loginRegContact = playerFields.loginRegContact;
     if (offer && offer.offerType === 'Sell' && offer.businessUnit.toString() !== businessUnit.toString()) {
        order.orderType = 'Purchase';
        if(businessUnit && businessUnit.toString()===offerContact.businessUnit.toString()){
            done(new Error('Same Business unit for both seller and buyer'),null);
        }else {
            loginRegContact.businessUnit = businessUnit;
            done(null, setContact(loginRegContact));
        }

    } else if (offer && offer.offerType === 'Buy' && offer.businessUnit.toString() === businessUnit.toString()) {
        offerContact.businessUnit=offer.businessUnit;
        order.orderType = 'Purchase';

        done(null, setContact(offerContact));

    } else if (offer && offer.offerType === 'Buy' && offer.businessUnit.toString() !== businessUnit.toString()) {
        order.orderType = 'Sale';
        offerContact.businessUnit=offer.businessUnit;
        done(null, setContact(offerContact));

    } else if (!offer && order && order.orderType === 'Purchase') {
        loginRegContact.businessUnit=businessUnit;
        done(null, setContact(loginRegContact));
    } else {
        done(null, {});
    }

}

/**
 *  set the order players for create Order from inventory or offer
 * @param order
 * @param playerFields
 * @param done
 */
function setOrderPlayers(order, playerFields, done) {
    getRegisterContactByUserId(playerFields.loginuser._id, function (errLoginContact, loginRegContact) {
        if (errLoginContact) {
            done(errLoginContact);
        } else {
            playerFields.loginRegContact = loginRegContact;
            setOrderSeller(order, playerFields, function (sellerErr, seller) {
                if (sellerErr) {
                    done(sellerErr, order);
                } else {
                    if (seller.contact)
                        order.seller = seller;
                    setOrderBuyer(order, playerFields, function (buyerErr, buyer) {
                        if (buyerErr) {
                            done(buyerErr, order);
                        } else {
                            if (buyer.contact)
                                order.buyer = buyer;
                            done(buyerErr, order);
                        }
                    });
                }
            });
        }
    });
}

/**
 *
 * @param req
 * @param res
 */
exports.createOrder = function (req, res) {
    var offer = req.offer;
    var products = req.body.products;
    var businessUnit = req.body.businessUnit;

    var orderValue = req.body.order;
    if (offer) {
        var versionKey = offer.offersVersionKey;
        offer = _.extend(offer, req.offer);
        offer.offersVersionKey = versionKey;
        offer.products=products;
        /*$scope.findContactVSGroups();*/
    }else{
        if(req.body.products.length>0 && req.body.products[0].businessUnit && !businessUnit)
            businessUnit=req.body.products[0].businessUnit;
    }
    var token = req.body.token || req.headers.token;

    usersJWTUtil.findUserCompanyByToken(token, function (usererr, loginuser) {
        if (usererr) {
            logger.debug('Fail to get the user with the token :' + token);
            return res.status(400).send({
                status: false,
                message: errorHandler.getErrorMessage(usererr)
            });
        } else {
            logger.debug('Found the user with the token :' + token);
            var order = new Order();
            if (orderValue) {
                order.seller = orderValue.seller;
                order.buyer = orderValue.buyer;
                order.orderType = orderValue.orderType;
            }
            order.user = loginuser._id;
            order.updateHistory = [];
            order.updateHistory.push({modifiedOn: Date.now(), modifiedBy: loginuser._id});
            if (offer && offer._id)
                order.offer = offer._id;

            order.products = [];
            var alignFillDate = new Date();
            var userId = (offer ? offer.user._id : loginuser._id);

            //Get the Contact information by offer user company or login user company

            getRegisterContactByUserId(userId, function (offerContactErr, offerContact) {
                if (offerContactErr) {
                    logger.error('Failed to get the Contact information by offer user or current login user id :' + offer + 'Error : ' + offerContactErr.message);
                    return res.status(400).send({
                        status: false,
                        message: errorHandler.getErrorMessage(offerContactErr)
                    });
                } else {
                    //Set the  Buyer and Seller contact to the order
                    var playerFields = {
                        contact: offerContact,
                        offer: offer,
                        dataFields: orderValue,
                        loginuser: loginuser,
                        businessUnit: businessUnit
                    };
                    if (offer && offer.businessUnit) {
                        playerFields.offerBusinessUnit = offer.businessUnit;
                    }
                    order.owner = {user: loginuser._id, company: loginuser.company, businessUnit: businessUnit};
                    setOrderPlayers(order, playerFields, function (orderPlayerErr, order) {
                        if (orderPlayerErr) {
                            return res.status(400).send({
                                status: false,
                                message: errorHandler.getErrorMessage(orderPlayerErr)
                            });
                        } else if (!(order.seller.contact || order.buyer.contact)) {
                            return res.status(400).send({
                                status: false,
                                message: 'No seller and buyer for the order'
                            });
                        } else {
                            //Get the Contact information by offer user company or login user company

                            getOfferPayment(offer, function (err,offerPayments,offerPaymentModes) {
                                getSellerCompany(userId, function (companyPaymentErr, company) {
                                    if (companyPaymentErr) {
                                        return res.status(400).send({
                                            status: false,
                                            message: errorHandler.getErrorMessage(companyPaymentErr)
                                        });
                                    } else {
                                        // set Company Payment terms or  offer Payment Term to order

                                        setOrderPaymentTerms(offerPayments,offerPaymentModes,company,order,function (paymentTermErr,order) {
                                            if(paymentTermErr){
                                                return res.status(400).send({
                                                    status: false,
                                                    message: errorHandler.getErrorMessage(paymentTermErr)
                                                });
                                            }else {
                                                var productFields = {products: products, offer: offer};
                                                // Set the Products to order from offer  or from inventory
                                                setSellerProductsToOrder(productFields, order, loginuser, function (errProduct, order) {
                                                    if (errProduct) {
                                                        return res.status(400).send({
                                                            status: false,
                                                            message: errorHandler.getErrorMessage(errProduct)
                                                        });
                                                    } else {
                                                        if (order instanceof Order) {
                                                            order.save(function (saveErr) {
                                                                if (saveErr) {
                                                                    return res.status(400).send({
                                                                        status: false,
                                                                        message: errorHandler.getErrorMessage(saveErr)
                                                                    });
                                                                } else {
                                                                    logger.info('Loading the order details with Order number ' + order.orderNumber + ' after creation');
                                                                    fetchOrderById(order._id, function (orderResponse) {
                                                                        if (orderResponse instanceof Error) {
                                                                            logger.error('Error while loading the order details with Order number ' + order.orderNumber + ' after creation');
                                                                            return res.status(400).send({
                                                                                status: false,
                                                                                message: errorHandler.getErrorMessage(orderResponse)
                                                                            });
                                                                        } else {
                                                                            res.json(orderResponse);
                                                                        }
                                                                    });
                                                                }
                                                            });
                                                        } else {
                                                            return res.status(400).send({
                                                                status: false,
                                                                message: 'No proper data to save the order'
                                                            });
                                                        }
                                                    }
                                                });
                                            }
                                        });
                                    }
                                });


                            });


                        }

                    });

                }

            });


        }
    });


};


/**
 * Clone Offer
 */
exports.cloneOffer = function (req, res) {

    var offer = req.offer;

    var versionKey = offer.offersVersionKey;
    offer = _.extend(offer, '');


    offer.offersVersionKey = versionKey;
    offer.set('lastUpdated', Date.now());

    offer.validTill = Date.now();
    offer.validTill.setHours(23, 59, 59, 999);

    var token = req.body.token || req.headers.token;
    usersJWTUtil.findUserCompanyByToken(token, function (usererr, loginuser) {
        if (usererr) {
            return res.status(400).send({
                status: false,
                message: errorHandler.getErrorMessage(usererr)
            });
        } else {
            offer.name = 'Copy _' + offer.name;
            offer.offerStatus = 'Drafted';

            offer.offerNumber = null;
            offer._id = mongoose.Types.ObjectId();
            offer.isNew = true;
            offer.user = loginuser._id;

            offer.updateHistory = [];
            offer.updateHistory.push({modifiedOn: Date.now(), modifiedBy: loginuser._id});
            offer.set('lastUpdated', Date.now());
            offer.set('created', Date.now());
            /*offer.toContacts=mongoose.Types.toContacts;*/
            offer.notificationsContacts = mongoose.Types.notificationsContacts;
            offer.statusHistory = mongoose.Types.statusHistory;
            offer.sampleRequests = mongoose.Types.sampleRequests;
            offer.statusComments = mongoose.Types.statusComments;
            if (offer.inventoryItemsClassification && offer.inventoryItemsClassification.classificationType === 'Criteria') {
                offer.products = [];
            }
            //This is needs to be checked if the quantity is available at the inventory but not in the offer level.
            //Copy the  products if  the no stock at offer but quantity is availabe at the inventory.
            /*  var products=offer.products;
             for (var i = 0; i < products.length; i++) {
             //&& offer.products[i].availableDate <= alignFillDate
             if (products[i].numberOfUnitsAvailable > 0) {
             offer.products.push(products[i]);
             }
             }*/
            offer.save(function (err) {
                if (err) {
                    return res.status(400).send({
                        status: false,
                        message: errorHandler.getErrorMessage(err)
                    });
                } else {
                    offerUtil.fetchOfferById(offer._id, 3, function (errResult, offerResponse) {
                        if (errResult) {
                            logger.error('Error while loading the offer details with offer number ' + offer.offerNumber + ' after creation');
                            return res.status(400).send({
                                status: false,
                                message: errorHandler.getErrorMessage(errResult)
                            });
                        } else {
                            res.json(offerResponse);
                        }
                    });

                }
            });

        }
    });
};
/**
 * Delete an Offer
 */
exports.delete = function (req, res) {
    var offer = req.offer;

    offer.remove(function (err) {
        if (err) {
            return res.status(400).send({
                status: false,
                message: errorHandler.getErrorMessage(err)
            });
        } else {
            res.jsonp(offer);
        }
    });
};


/**
 *
 */


exports.update = function (req, res) {
    var offer = req.offer;

    var versionKey = offer.offersVersionKey;
    var offerCurrentStatus = offer.offerStatus;
    offer = _.extend(offer, req.body);

    offer.set('lastUpdated', Date.now());
    offer.offersVersionKey = versionKey;
    var token = req.body.token || req.headers.token;
    if(offer.validTill) {
        offer.validTill.setHours(23, 59, 59, 999);
    }
    usersJWTUtil.findUserByToken(token, function (usererr, loginuser) {
        if (usererr) {
            logger.debug('login user token error -' + JSON.stringify(usererr));
            return res.status(400).send({
                status: false,
                message: errorHandler.getErrorMessage(usererr)
            });
        } else {
            if (offerCurrentStatus === 'Drafted' && (offer.offerStatus === 'Placed' || offer.offerStatus === 'Opened')) {
                offerPlaced(offer, req.body, loginuser, 'OfferUpdation', 1, 1, res, function (errR, response) {
                    if (errR) {
                        return res.status(400).send({
                            status: false,
                            message: errorHandler.getErrorMessage(errR)
                        });
                    } else {
                        res.json(response);
                    }
                });
            } else if ((offer.offerStatus === 'Placed' || offer.offerStatus === 'Opened') && offer.offerStatus === 'Cancelled') {

                if (offerCurrentStatus !== offer.offerStatus) {
                    offer.statusHistory.push({
                        status: offer.offerStatus,
                        user: loginuser._id,
                        displayName: loginuser.displayName,
                        comments: offer.statusComments !== null ? offer.statusComments : ''
                    });


                }
                offerRollBack(offer, loginuser, 'OfferUpdation', res, function (response) {
                    if (response instanceof Error) {
                        return res.status(400).send({
                            status: false,
                            message: errorHandler.getErrorMessage(response)
                        });
                    } else {
                        res.json(response);
                    }
                });
            } else {
                if (offerCurrentStatus !== offer.offerStatus) {
                    offer.statusHistory.push({
                        status: offer.offerStatus,
                        user: loginuser._id,
                        displayName: loginuser.displayName,
                        comments: offer.statusComments !== null ? offer.statusComments : ''
                    });


                }
                offer.updateHistory.push({modifiedOn: Date.now(), modifiedBy: loginuser._id});
                offerPlaced(offer, req.body, loginuser, 'OfferUpdation', 0, 0, res, function (errR, response) {
                    if (errR) {
                        return res.status(400).send({
                            status: false,
                            message: errorHandler.getErrorMessage(errR)
                        });
                    } else {
                        res.json(response);
                    }
                });

            }


        }
    });

};

/**
 * List public offers
 */
exports.listPublicOffers = function (req, res) {
    var mobileAppKey = req.headers.apikey;
    var lastSyncTime = req.headers.lastsynctime;
    var pageOptions = {};
    if (req.param('page') && !isNaN(req.param('page'))) {

        pageOptions.page = parseInt(req.param('page'));
    }
    if (req.param('limit')) {
        pageOptions.limit = parseInt(req.param('limit'));
    }

    var query = {
        isPublic: true, '$or': [{offerStatus: 'Placed'}, {offerStatus: 'Opened'}]
    };
    offerUtil.findQueryByOffers([query], 0,false, pageOptions,'target', function (errOffer, offers) {
        if (errOffer) {
            return res.status(400).send({
                status: false,
                message: errorHandler.getErrorMessage(errOffer)
            });
        } else {
            res.jsonp(offers);
        }
    });
};
/**
 * List subCategory1 offers
 */
exports.listSubCategory2Offers = function (req, res) {
    var subCategory2Id = req.query.subCategory2Id;
    var pageOptions = {};
    if (req.param('page') && !isNaN(req.param('page'))) {

        pageOptions.page = req.param('page');
    }
    if (req.param('limit')) {
        pageOptions.limit = req.param('limit');
    }
    var query = {
        isPublic: true, '$or': [{offferStatus: 'Placed'}, {offerStatus: 'Opened'}]
    };
    if (subCategory2Id) {
        query.products = {'subCategory2': subCategory2Id};
    }
    offerUtil.findQueryByOffers([query], 0,false, pageOptions,'target', function (errOffer, offers) {
        if (errOffer) {
            return res.status(400).send({
                status: false,
                message: errorHandler.getErrorMessage(errOffer)
            });
        } else {
            res.jsonp(offers);
        }
    });
};

function getFilterArray(field) {
    var queryObject= {New:{'offerStatus': 'Opened'},Intra:{'isIntraCompany': true}, Sell:{'offerType':'Sell'},
        Buy:{'offerType':'Buy'},Expired:{'offerStatus':'Expired'},Draft:{'offerStatus':'Drafted'}};
    for (var key in queryObject) {
        if(field && key===field)
            return queryObject[key];
    }
    return null;
}

function getQuery(summaryFilters,queryArray,done) {
    if(summaryFilters) {
        for (var key in summaryFilters) {
            var value=getFilterArray(key);
            if(value) {
                queryArray.push(value);
            }
        }
    }
    done(queryArray);

}
function offerQuery(req,isSource,done) {
    var token = req.body.token || req.headers.token;
    var lastSyncTime = req.headers.lastsynctime;
    var businessUnitId = req.query.businessUnitId;
    if(!businessUnitId){
        done(new Error('The business unit id is not set'),null);
    }else {
        logger.info('Fetching user my offers');
        usersJWTUtil.findUserByToken(token, function (err, loginuser) {
            if (err) {
                logger.error('Error while fetching the user with the given token for fetching user offers.');
                done(err, null);
            } else {
                var pageOptions = {};
                if (req.query.page&& !isNaN(req.query.page)) {

                    pageOptions.page = parseInt(req.query.page);
                }
                if (req.query.limit) {
                    pageOptions.limit = parseInt(req.query.limit);
                }
                var query = isSource?({
                    $and: [{'owner.businessUnit': businessUnitId},
                        {'owner.company': loginuser.company.toString()}]
                }):({ $and: [{offerStatus: {$ne: 'Drafted'}},{$or:[{'owner.company': loginuser.company.toString(),'toContacts.businessUnits.businessUnit': businessUnitId,isIntraCompany:true},{isIntraCompany:false,'owner.company': {$ne: loginuser.company.toString()},'notificationsContacts.nVipaniCompany': loginuser.company}]}]});
                if (lastSyncTime) {
                    query.lastUpdated = {$gt: lastSyncTime};
                }
                var queryArray=[];
                if(req.query.searchText){
                    var regExp={'$regex':req.query.searchText};
                    for (var property in Offer.schema.paths) {
                        if (Offer.schema.paths.hasOwnProperty(property)  && Offer.schema.paths[property].instance === 'String') {
                            var eachproduct={};
                            eachproduct[property] = regExp;
                            queryArray.push(eachproduct);
                        }

                    }
                }
                getQuery(req.body.summaryFilters, queryArray, function (queryArray) {
                    var finalQuery = null;
                    if (queryArray.length > 0)
                        finalQuery = {$and: [query, {$or: queryArray}]};
                    else
                        finalQuery = query;
                    done(null, finalQuery, pageOptions);
                });
                // done(null,query,pageOptions);
            }
        });
    }
}

/**
 * List received Offers
 */
exports.listReceivedOffers = function (req, res) {
    offerQuery(req,false,function(err,query,pageOptions) {
        if (err) {
            return res.status(400).send({
                status: false,
                message: errorHandler.getErrorMessage(err)
            });
        } else {
            offerUtil.findQueryByOffers([query], 0, true, pageOptions,'target', function (err, offers) {
                if (err) {
                    logger.error('Error while fetching user received offers ');
                    return res.status(400).send({
                        status: false,
                        message: errorHandler.getErrorMessage(err)
                    });
                } else {
                    res.jsonp(offers);
                }
            });
        }
    });
};

exports.listByBusinessUnit = function (req, res) {
    offerQuery(req,true,function(err,query,pageOptions) {
        if (err) {
            return res.status(400).send({
                status: false,
                message: errorHandler.getErrorMessage(err)
            });
        } else {
            offerUtil.findQueryByOffers([query], 0, true, pageOptions,'own', function (err, offers) {
                if (err) {
                    logger.error('Error while fetching user received offers ');
                    return res.status(400).send({
                        status: false,
                        message: errorHandler.getErrorMessage(err)
                    });
                } else {
                    res.jsonp(offers);
                }
            });
        }
    });
};


/**
 * Offer middleware
 */
exports.offerByID = function (req, res, next, id) {
    var token = req.body.token || req.headers.token;
    usersJWTUtil.findUserByToken(token, function (err, user) {
        if (err) {
            return res.status(400).send({
                status: false,
                message: errorHandler.getErrorMessage(err)
            });
        } else {
            offerUtil.fetchOfferById(id, 3, function (errResult, offerResponse) {
                if (errResult) {
                    logger.error('Error while loading the offer details with Offer number ' + id);
                    return next(errResult);
                } else if (!offerResponse) {
                    logger.error('Error while loading the offer details with Offer number ' + id);
                    return next(new Error('No offer id' + id));
                } else if (offerResponse && offerResponse.products && offerResponse.products.length === 0 && offerResponse.inventoryItemsClassification && offerResponse.inventoryItemsClassification.classificationType && (offerResponse.inventoryItemsClassification.classificationType === 'Criteria' || offerResponse.inventoryItemsClassification.classificationType === 'All')) {
                    inventoriesUtil.offerCriteriaInventories(offerResponse, req, user, function (errInventoryClassification, products) {
                        if (errInventoryClassification)
                            return next(errInventoryClassification);
                        else {
                            offerResponse.products = products;
                            req.offer = offerResponse;
                            next();
                        }
                    });
                } else {
                    req.offer = offerResponse;
                    next();
                }
            });
        }
    });
};

function getMatchedNotificationContact(notificationsContacts,company,businessUnit,done) {
    done(notificationsContacts.filter(function (eachNotificationContact) {
        return eachNotificationContact.nVipaniCompany &&eachNotificationContact.nVipaniCompany.toString() === company.toString();
    }));
}

/**
 * Offer authorization middleware
 */
exports.hasAuthorization = function (req, res, next) {
    var token = req.body.token || req.headers.token;
    usersJWTUtil.findUserByToken(token, function (err, user) {
        if (err) {
            return res.status(400).send({
                status: false,
                message: errorHandler.getErrorMessage(err)
            });
        } else {
            if (req.offer && ((req.offer.owner.company && user.company && req.offer.owner.company && req.offer.owner.company._id.toString() === user.company.toString()) ||
                    req.offer.isPublic)) {
                next();
            } else {
                var hasAuth = false;
                if (req.offer && req.offer.notificationsContacts && req.offer.notificationsContacts instanceof Array) {
                    getMatchedNotificationContact(req.offer.notificationsContacts,user.company,null, function (matchedContact) {
                        if (matchedContact.length > 0) {
                            next();
                        } else {
                            return res.status(403).send('User is not authorized');
                        }
                    });
                }else{
                    return res.status(403).send('User is not authorized');
                }

            }
        }
    });

};
exports.massDelete = function (req, res) {
    var token = req.body.token || req.headers.token;
    var offers = req.body.offers;
    var businessUnit=req.body.businessUnit;
    var type = req.body.type;
    usersJWTUtil.findUserByToken(token, function (err, user) {
        if (err) {
            return res.status(400).send({
                status: false,
                message: errorHandler.getErrorMessage(err)
            });
        } else {
            if (offers && offers.length > 0) {
                var query = {'owner.company':user.company};
                if (offers && offers.length > 0) {
                    query.$or = offers;
                }
                offerUtil.batchOfferUpdate(query, {'deleted': true}, function (offerErr, results) {
                    if (offerErr) {
                        return res.status(400).send({
                            status: false,
                            message: errorHandler.getErrorMessage(offerErr)
                        });
                    } else if (!results) {
                        return res.status(400).send({
                            message: 'Not Deleted',
                            status: false
                        });
                    } else {
                        offerUtil.findQueryByOffers([{businessUnit:businessUnit}], 0,false, {
                            page: 0,
                            limit: 10
                        },'own', function (errOffer, offers) {
                            if (errOffer) {
                                return res.status(400).send({
                                    status: false,
                                    message: errorHandler.getErrorMessage(errOffer)
                                });
                            } else {
                                res.jsonp(offers);
                            }

                        });
                    }

                });
            } else {
                return res.status(400).send({
                    status: false,
                    message: 'No Selected Offers'
                });
            }
        }
    });
};
exports.massDisable = function (req, res) {
    var token = req.body.token || req.headers.token;
    var offers = req.body.offers;
    var businessUnit=req.body.businessUnit;
    var type = req.body.type;
    usersJWTUtil.findUserByToken(token, function (err, user) {
        if (err) {
            return res.status(400).send({
                status: false,
                message: errorHandler.getErrorMessage(err)
            });
        } else {
            if (offers && offers.length > 0) {
                var query = {'owner.company':user.company};
                if (offers && offers.length > 0) {
                    query.$or = offers;
                }
                offerUtil.batchOfferUpdate(query, {'disabled': true}, function (offerErr, results) {
                    if (offerErr) {
                        return res.status(400).send({
                            status: false,
                            message: errorHandler.getErrorMessage(offerErr)
                        });
                    } else if (!results) {
                        return res.status(400).send({
                            message: 'Not Deleted',
                            status: false
                        });
                    } else {
                        offerUtil.findQueryByOffers([{businessUnit:businessUnit}], -1,false, {
                            page: 0,
                            limit: 10
                        },'own', function (errFetch, offers) {
                            if (errFetch) {
                                return res.status(400).send({
                                    status: false,
                                    message: errorHandler.getErrorMessage(errFetch)
                                });
                            } else {
                                res.jsonp(offers);
                            }

                        });
                    }

                });
            } else {
                return res.status(400).send({
                    status: false,
                    message: 'No Selected Offers'
                });
            }
        }
    });
};
exports.massEnable = function (req, res) {
    var token = req.body.token || req.headers.token;
    var offers = req.body.offers;
    var businessUnit=req.body.businessUnit;
    var type = req.body.type;
    usersJWTUtil.findUserByToken(token, function (err, user) {
        if (err) {
            return res.status(400).send({
                status: false,
                message: errorHandler.getErrorMessage(err)
            });
        } else {
            if (offers && offers.length > 0) {
                var query = {'owner.company':user.company};
                if (offers && offers.length > 0) {
                    query.$or = offers;
                }
                offerUtil.batchOfferUpdate(query, {'disabled': false}, function (offerErr, results) {
                    if (offerErr) {
                        return res.status(400).send({
                            status: false,
                            message: errorHandler.getErrorMessage(offerErr)
                        });
                    } else if (!results) {
                        return res.status(400).send({
                            message: 'Not Deleted',
                            status: false
                        });
                    } else {
                        offerUtil.findQueryByOffers([{businessUnit:businessUnit}], -1, false,{
                            page: 0,
                            limit: 10
                        },'own', function (errFetch, offers) {
                            if (errFetch) {
                                return res.status(400).send({
                                    status: false,
                                    message: errorHandler.getErrorMessage(errFetch)
                                });
                            } else {
                                res.jsonp(offers);
                            }

                        });
                    }

                });
            } else {
                return res.status(400).send({
                    status: false,
                    message: 'No Selected Offers'
                });
            }
        }
    });
};

exports.massSendNotification = function (req, res) {
    var token = req.body.token || req.headers.token;
    var offers = req.body.offers;
    var toContacts = req.body.toContacts;
    var resend = req.body.resend;
    usersJWTUtil.findUserByToken(token, function (err, user) {
        if (err) {
            return res.status(400).send({
                status: false,
                message: errorHandler.getErrorMessage(err)
            });
        } else {
            if (offers && offers.length > 0) {
                var query = {user: user._id};
                if (offers && offers.length > 0) {
                    query.$or = offers;
                }
                var offerSendNotification = [];
                if (resend) {
                    async.forEachSeries(offers, function (offer, callback) {
                        offerUtil.fetchOfferById(offer._id, 3, function (errResult, sendOfferContact) {

                            createOfferNotifications(sendOfferContact, sendOfferContact.notificationsContacts, user, res, function (createOfferNotificationErr) {
                                if (createOfferNotificationErr) {
                                    logger.error('Error while creating offer notifications for offer name ' + sendOfferContact.name, createOfferNotificationErr);
                                } else {
                                    logger.info('Loading the offer details with offer number ' + sendOfferContact.offerNumber + ' after creation');
                                    offerSendNotification.push(sendOfferContact);
                                    callback();
                                }
                            });
                        });
                    }, function (errFetch) {
                        if (errFetch) {
                            return res.status(400).send({
                                status: false,
                                message: errorHandler.getErrorMessage(errFetch)
                            });
                        } else {
                            res.jsonp(offerSendNotification);
                        }

                    });

                } else if (toContacts && ((toContacts.contacts && toContacts.contacts.length > 0) || (toContacts.groups && toContacts.groups.length > 0))) {
                    var notificationContacts = [];
                    getOfferContacts(toContacts, user, notificationContacts, function (err, contacts) {
                        async.forEachSeries(offers, function (offer, callback) {
                            offerUtil.fetchOfferById(offer._id, 3, function (errResult, sendOfferContact) {
                                sendOfferContact.toContacts.contacts = sendOfferContact.toContacts.contacts.concat(toContacts.contacts);
                                sendOfferContact.toContacts.groups = sendOfferContact.toContacts.groups.concat(toContacts.groups);
                                offerSave(sendOfferContact, contacts, user, true, res, function (offerErr, sendNotification) {
                                    if (offerErr) {
                                        callback(offerErr);
                                    } else if (!sendNotification) {
                                        callback(new Error('Not able to send the notifications'));
                                    } else {
                                        offerSendNotification.push(sendNotification);
                                        callback();
                                    }

                                });
                            });
                        }, function (errFetch) {
                            if (errFetch) {
                                return res.status(400).send({
                                    status: false,
                                    message: errorHandler.getErrorMessage(errFetch)
                                });
                            } else {
                                res.jsonp(offerSendNotification);
                            }

                        });
                    });

                } else {
                    logger.error('No Selected Contacts to send notification');
                    return res.status(400).send({
                        status: false,
                        message: 'No Selected Contacts to send'
                    });
                }

            } else {
                logger.error('No Selected Offers');
                return res.status(400).send({
                    status: false,
                    message: 'No Selected Offers'
                });
            }
        }
    });
};

function getMatchedSegmentById(segments,segmentId) {
    return segments.filter(function (eachSegment) {
        return eachSegment.segment &&  segmentId && eachSegment.segment.toString()===segmentId.toString();
    });
}
function getSegmentArray(segments) {
    var finalArray=[];
    segments.forEach(function (eachSegment) {
        finalArray.push({_id:eachSegment.segment._id.toString(),name:eachSegment.segment.name,offers:eachSegment.offers});
    });
    return finalArray;

}
exports.findByPublicOffers=function (req,res) {
    var token = req.body.token || req.headers.token;
    var category = req.body.category;
    var segment = req.params.segment;
    usersJWTUtil.findUserCompanyByToken(token, function (err, user) {
        if (err) {
            return res.status(400).send({
                status: false,
                message: errorHandler.getErrorMessage(err)
            });
        } else {
            if(segment){
                var matchedSegments=getMatchedSegmentById(user.company.segments,segment);
                offerUtil.segmentsCategoriesOffers(user.company.segments,matchedSegments[0],null,function (err,offers) {
                    res.jsonp({offers:offers});
                });
            }else {
                Company.findById(user.company._id, 'segments').populate('segments.segment','name').exec(function (companyErr, companyUser) {
                    var segments=companyUser.segments;
                    offerUtil.segmentsCategoriesOffers(segments,segments[0],null,function (err,offers) {
                        segments[0].offers=offers;

                        res.jsonp(getSegmentArray(segments));


                    });
                });
            }
        }
    });
};

exports.findByFromContactOffers=function (req,res) {
    var token = req.body.token || req.headers.token;
    var contactCompany = req.params.contactCompany;
    usersJWTUtil.findUserCompanyByToken(token, function (err, user) {
        if (err) {
            return res.status(400).send({
                status: false,
                message: errorHandler.getErrorMessage(err)
            });
        } else {
            if(!contactCompany){
                contactCompany=user.company._id;
            }
            offerUtil.companyContactsOffers(contactCompany,user,logger,function (offerError,offers) {
                if(offerError){
                    return res.status(400).send({
                        status: false,
                        message: errorHandler.getErrorMessage(offerError)
                    });
                }else {
                    res.jsonp(offers);
                }
            });
        }
    });
};

exports.getSourceSummaries=function (req,res) {
    offerQuery(req,true,function(err,query,pageOptions){
        if(err) {
            return res.status(400).send({
                status: false,
                message: errorHandler.getErrorMessage(err)
            });
        }else{
            offerUtil.findQueryByOffers([query], 0,false, pageOptions,'own', function (err, offers) {
                if (err) {
                    logger.error('Error while fetching user my offers ');
                    return res.status(400).send({
                        status: false,
                        message: errorHandler.getErrorMessage(err)
                    });
                } else {
                    offerUtil.getOffersSummary(offers.offer, req.body.summaryFilters,'own',  function ( summaryData) {
                        res.jsonp(summaryData);
                    });
                }
            });

        }
    });
};

exports.getTargetSummaries=function (req,res) {
    offerQuery(req,false,function(err,query,pageOptions){
        if(err) {
            return res.status(400).send({
                status: false,
                message: errorHandler.getErrorMessage(err)
            });
        }else{
            offerUtil.findQueryByOffers([query], 0,false, pageOptions,'target', function (err, offers) {
                if (err) {
                    logger.error('Error while fetching user received offers ');
                    return res.status(400).send({
                        status: false,
                        message: errorHandler.getErrorMessage(err)
                    });
                } else {
                    offerUtil.getOffersSummary(offers.offer, req.body.summaryFilters,'target',  function (summaryData) {
                        res.jsonp(summaryData);
                    });
                }
            });

        }

    });

};
