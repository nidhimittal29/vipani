'use strict';

/**
 * Module dependencies.
 */
var mongoose = require('mongoose'),
    errorHandler = require('./errors.server.controller'),
    fs = require('fs'),
    Company = mongoose.model('Company'),
    async = require('async'),
    usersJWTUtil   = require('./utils/users.jwtutil'),
    logger  = require('../../lib/log').getLogger('COMPANY', 'DEBUG'),
    Contact=mongoose.model('Contact'),
    Product=mongoose.model('Product'),
    RegistrationCategories=mongoose.model('RegistrationCategories'),
    Offer = mongoose.model('Offer'),
    OffersUtil =require('./utils/common.offer.util'),
    InventoriesUtil =require('./utils/common.inventories.util'),
    Inventory = mongoose.model('Inventory'),
    User = mongoose.model('User'),
    Category=mongoose.model('Category'),
    BusinessUnit = mongoose.model('BusinessUnit'),
    companyUtil = require('./utils/common.company.util'),
    _ = require('lodash');

/**
 *
 */
function fetchCompanyById(companyId, done) {

    Company.findById(companyId).populate('inventories.inventory','-updateHistory -numberOfUnitsHistory -offerUnitsHistory -buyOfferUnitsHistory').populate('user', 'displayName').exec(function (companyErr, resCompany) {
        if (companyErr) {
            logger.error('Error while loading the Company details with company id- ' + companyId);
            done(companyErr);
        } else {
            Company.populate(resCompany, {
                path: 'inventories.inventory.product',
                model: Product,
                select: 'name category subCategory1 subCategory2 productImageURL1 productImageURL2 productImageURL3 sampleNumber'
            }, function (nestedErr, populatedResultCompany) {
                if (nestedErr) {
                    done(nestedErr);
                } else {
                    Company.populate(populatedResultCompany, {
                        path: 'inventories.inventory.product.category inventories.inventory.product.subCategory1 inventories.inventory.product.subCategory2',
                        model: 'Category',
                        select: 'name productAttributes inventoryAttributes searchFilters'
                    },function (nestedErrs, populatedResultsCompany){
                        if (nestedErrs) {
                            done(nestedErrs);
                        } else {
                            done(populatedResultsCompany);
                        }              });  }
            });
        }

    });
}


/**
 *
 */
function fetchCompanyByIdRegistrationCategories(companyId, done) {
    Company.findById(companyId).populate('registrationCategory').populate('user', 'displayName').exec(function (companyErr, resCompany) {
        if (companyErr) {
            logger.error('Error while loading the Company details with company id- ' + companyId);
            done(companyErr);
        } else {
            if (resCompany.registrationCategory) {
                RegistrationCategories.find({name: resCompany.registrationCategory.name}).populate('user', 'displayName').exec(function (registrationErr, registrationCategories) {
                    if (registrationErr) {
                        logger.error('Error while loading the Company details with company id- ' + companyId);
                        done(companyErr);
                    } else {
                        done({company: resCompany, registrationCategories: registrationCategories});
                    }

                });
            }else{
                done(resCompany);
            }
        }
    });
}


function fetchCompanyByIdNoInvenotry(companyId, done) {
    Company.findById(companyId).populate('registrationCategory').populate('user', 'displayName').exec(function (companyErr, resCompany) {
        if (companyErr) {
            logger.error('Error while loading the Company details with company id- ' + companyId);
            done(companyErr);
        } else {

            done(resCompany);

        }

    });
}
/**
 * Create a Company
 */
exports.create = function (req, res) {
    var company = new Company(req.body);
    var token = req.body.token || req.headers.token;
    usersJWTUtil.findUserByToken(token, function (err, user) {
        if (err) {
            return res.status(400).send({
                message: errorHandler.getErrorMessage(err)
            });
        }
        company.user = user;
        company.updateHistory = [];
        company.updateHistory.push({modifiedOn: Date.now(), modifiedBy: user});
        company.save(function (err) {
            if (err) {
                return res.status(400).send({
                    message: errorHandler.getErrorMessage(err)
                });
            } else {
                /*     Company.findById(company._id).populate('inventories.inventory', 'product unitSize unitMeasure unitPrice numberOfUnits').populate('user', 'displayName').exec(function (companyErr, resCompany) {
                         if (companyErr) {
                             return res.status(400).send({
                                 message: errorHandler.getErrorMessage(companyErr)
                             });
                         }
                         if (!resCompany) {
                             return res.status(400).send({
                                 message: errorHandler.getErrorMessage('Failed to load Company ' + resCompany._id)
                             });
                         }
                         res.jsonp(resCompany);
                     });*/

                fetchCompanyById(company._id, function (resCompany) {
                    if (resCompany instanceof Error) {
                        logger.error('Error while loading the company details with Company name ' + company.name + ' after creation');
                        return res.status(400).send({
                            message: errorHandler.getErrorMessage(resCompany)
                        });
                    } else {
                        res.json(resCompany);
                    }
                });
            }
        });
    });
};

/**
 * Show the current Company
 */
exports.read = function (req, res) {
    res.jsonp(req.company);
};

/**
 * Update a Company
 */
exports.update = function (req, res) {
    var company = req.company;

    var versionKey = company.companiesVersionKey;

    company = _.extend(company, req.body);

    company.companiesVersionKey = versionKey;
    var token = req.body.token || req.headers.token;
    usersJWTUtil.findUserByToken(token, function (err, user) {
        if (err) {
            return res.status(400).send({
                message: errorHandler.getErrorMessage(err)
            });
        }
        /*company.user=company.user.toString();*/
        company.updateHistory.push({modifiedOn: Date.now(), modifiedBy: user});
        if(company.name===user.username){
            company.isPredefined=true;
        }else{
            company.isPredefined=false;
            company.url=company.name;
        }
        company.save(function (saveCompanyErr) {
            if (saveCompanyErr) {
                logger.error('Error while saving the company details with Company name ' + company.name, saveCompanyErr);
                return res.status(400).send({
                    message: errorHandler.getErrorMessage(saveCompanyErr)
                });
            } else {
                fetchCompanyByIdNoInvenotry(company._id, function (resCompany) {
                    if (resCompany instanceof Error) {
                        logger.error('Error while loading the company details with Company name ' + company.name + ' after updation', resCompany);
                        return res.status(400).send({
                            message: errorHandler.getErrorMessage(resCompany)
                        });
                    } else {
                        /* res.json(resCompany);*/
                        if (!resCompany) {
                            logger.error('Error while loading the company details with Company name ' + company.name + ' after updation');
                            return res.status(400).send({
                                message: errorHandler.getErrorMessage('Failed to load Company ' + company._id)
                            });
                        }
                        Contact.findOne({
                            nVipaniUser: resCompany.user._id,
                            nVipaniRegContact: true
                        }, function (errContact, contact) {
                            if (errContact) {
                                logger.error('Error while loading the contact for the registration user id ' + resCompany.user._id + ' after saving the company details with Company name ' + company.name, errContact);
                                return res.status(400).send({
                                    message: errorHandler.getErrorMessage(errContact)
                                });
                            }

                            if (!contact) {
                                logger.error('Error while loading the contact for the user id ' + resCompany.user._id + ' after saving the company details with Company name ' + company.name);
                                return res.status(400).send({
                                    message: errorHandler.getErrorMessage('Failed to load registration Contact for the user id ' + resCompany.user._id)
                                });
                            }
                            contact.addresses = resCompany.addresses;
                            contact.emails = resCompany.emails;
                            contact.phones = resCompany.phones;
                            contact.company=resCompany._id;
                            contact.gstinNumber=resCompany.gstinNumber;
                            contact.gstinNumberProof=resCompany.gstinNumberProof;
                            contact = _.extend(contact, {});
                            contact.save(function (contactError) {
                                if (contactError) {
                                    return res.status(400).send({
                                        message: errorHandler.getErrorMessage(contactError)
                                    });
                                } else {
                                    resCompany.user=resCompany.user._id.toString();
                                    res.jsonp(resCompany);
                                }
                            });

                        });
                    }
                });
            }
        });
    });
};

/**
 * Delete an Company
 */
exports.delete = function (req, res) {
    var company = req.company;

    company.remove(function (err) {
        if (err) {
            return res.status(400).send({
                message: errorHandler.getErrorMessage(err)
            });
        } else {
            res.jsonp(company);
        }
    });
};

/**
 * List of Companies
 */
exports.list = function (req, res) {
    var token = req.body.token || req.headers.token;
    usersJWTUtil.findUserByToken(token, function (err, user) {
        if (err) {
            return res.status(400).send({
                message: errorHandler.getErrorMessage(err)
            });
        }
        Company.find({user: user.id}).sort('-created').populate('user', 'displayName').exec(function (err, companies) {
            if (err) {
                return res.status(400).send({
                    message: errorHandler.getErrorMessage(err)
                });
            } else {
                res.jsonp(companies);
            }
        });
    });
};

/**
 * List of Companies
 */
exports.userCompanies = function (req, res) {
    var token = req.body.token || req.headers.token;
    usersJWTUtil.findUserByToken(token, function (err, user) {
        if (err) {
            return res.status(400).send({
                message: errorHandler.getErrorMessage(err)
            });
        }
        Company.find({id: {$in: user.companies}}).sort('-created').exec(function (err, companies) {
            if (err) {
                return res.status(400).send({
                    message: errorHandler.getErrorMessage(err)
                });
            } else {
                res.jsonp(companies);
            }
        });
    });
};

/**
 * Company middleware
 */
exports.companyByID = function (req, res, next, id) {
    Company.findById(id).populate('registrationCategory').exec(function (err, company) {
        if (err) return next(err);
        if (!company) return next(new Error('Failed to load Company ' + id));
        req.company = company;
        next();
    });
};
function companyEmployees(company,listEmployees,done) {
    async.forEachSeries(company.employees,function (eachEmploy,callback) {
        async.forEachSeries(eachEmploy.user.companies,function (eachCompany,companyCallBack) {
            if(eachCompany.company.toString()===company._id.toString()) {
                listEmployees.push({'userGroup':eachCompany.userGroup,'user':{'firstName':eachEmploy.user.firstName, 'lastName':eachEmploy.user.lastName, 'displayName':eachEmploy.user.lastName, 'username':eachEmploy.user.lastName, 'company':eachEmploy.user.company , 'profileImageURL':eachEmploy.user.profileImageURL, 'croppedProfileImageURL':eachEmploy.user.croppedProfileImageURL, 'status':eachEmploy.user.status},'businessUnits':eachEmploy.businessUnits,'status':eachEmploy.status});
                callback();
            }else{
                companyCallBack();
            }
        },function (errEmploy) {
            if(errEmploy){
                done(errEmploy,null);
            }else{
                callback();
            }
        });
    },function (err) {
        if(err){
            done(err,null);
        }else{
            done(null,listEmployees);
        }
    });
}
/**
 * Company middleware
 */
exports.companyBusinessUsers = function (req, res, next,id) {
    var token = req.body.token || req.headers.token;
    var lastSyncTime = req.headers.lastsynctime;
    var companyId = id;
    var pageOptions = {};
    if(req.params.page && !isNaN(req.params.page)){

        pageOptions.page= parseInt(req.params.page);

    }
    if(req.params.limit){
        pageOptions.limit = parseInt(req.params.limit);
    }

    if(!companyId){
        return res.status(400).send({
            message:'No Company id was provided',
            status:false
        });
    }
    else{
        usersJWTUtil.findUserByToken(token, function (err, loginuser) {
            if(err){
                return res.status(400).send({
                    status:false,
                    message: errorHandler.getErrorMessage(err)
                });
            }else if (!loginuser ) {
                return res.status(400).send({
                    status:false,
                    message: 'No login user'
                });
            } else {
                var query = [];
                if (lastSyncTime) {
                    query.lastUpdated = {$gt: lastSyncTime};
                }
                query._id=companyId;
                var queryArray=[];
                if(req.param('searchText')){
                    var regExp={'$regex': new RegExp(req.param('searchBusinessUserText'), 'g')};
                    for (var property in Contact.schema.paths) {
                        if (Contact.schema.paths.hasOwnProperty(property)  && Contact.schema.paths[property].instance === 'String') {
                            var eachUser={};
                            eachUser[property] = regExp;
                            queryArray.push(eachUser);
                        }

                    }
                }
                var finalQuery=null;
                if(queryArray.length>0)
                    finalQuery={$and:[query,{ $or : queryArray}]};
                else finalQuery=query;

                companyUtil.findBusinessUserByCompany(query, pageOptions, function (err, companyEmployees) {
                    if (err) {
                        return res.status(400).send({
                            message: errorHandler.getErrorMessage(err)
                        });
                    } else {
                        res.jsonp(companyEmployees);
                    }
                });
            }
        });
    }
};

exports.companyInchargeUsers = function(req,res,next,id){
    var token = req.body.token || req.headers.token;
    var lastSyncTime = req.headers.lastsynctime;
    var companyId = id;
    if(!companyId){
        return res.status(400).send({
            message:'No Company id was provided',
            status:false
        });
    }
    else{
        usersJWTUtil.findUserByToken(token, function (err, loginuser) {
            if(err){
                return res.status(400).send({
                    status:false,
                    message: errorHandler.getErrorMessage(err)
                });
            }else if (!loginuser ) {
                return res.status(400).send({
                    status:false,
                    message: 'No login user'
                });
            } else {
                var query = [];
                if (lastSyncTime) {
                    query.lastUpdated = {$gt: lastSyncTime};
                }
                query._id=companyId;
                var queryArray=[];
                if(req.param('searchText')){
                    var regExp={'$regex': new RegExp(req.param('searchBusinessUserText'), 'g')};
                    for (var property in Contact.schema.paths) {
                        if (Contact.schema.paths.hasOwnProperty(property)  && Contact.schema.paths[property].instance === 'String') {
                            var eachUser={};
                            eachUser[property] = regExp;
                            queryArray.push(eachUser);
                        }

                    }
                }
                var finalQuery=null;
                if(queryArray.length>0)
                    finalQuery={$and:[query,{ $or : queryArray}]};
                else finalQuery=query;

                companyUtil.findInchargeUsers(query, function (err, companyEmployees) {
                    if (err) {
                        return res.status(400).send({
                            message: errorHandler.getErrorMessage(err)
                        });
                    } else {
                        res.jsonp(companyEmployees);
                    }
                });
            }
        });
    }
};


/**
 * Company middleware
 */
exports.companyBusinessUnits = function (req, res, next, id) {
    var token = req.body.token || req.headers.token;
    if (token) {
        usersJWTUtil.findUserByToken(token, function (err, user) {
            if (err) {
                next(err);
            }else{
                companyUtil.findBusinessUnitsByCompany(user,id,function(err,companyBUs){
                    if(err){
                        next(err);
                    }
                    else{
                        req.company=companyBUs;
                        next();
                    }
                });
            }
        });
    }

    /*Company.findById(id,'name businessUnits registrationCategory').populate('businessUnits.businessUnit').populate('registrationCategory').exec(function (err, company) {
        if (err) return next(err);
        else if (!company) return next(new Error('Failed to load Company ' + id));
        else {
            Company.populate(company, {
                path: 'businessUnits.businessUnit.employees.user',
                model: 'User',
                select:'username firstName lastName middleName displayName'
            }, function (nestedErr, populatedEmployedBunits) {
                if (nestedErr) {
                    next(nestedErr);
                } else {
                    req.company = populatedEmployedBunits;
                    /!*var filteredBUs = populatedEmployedBunits.businessUnits.filter(function(bu){
                        return bu.deleted===false && bu.disabled===false;
                    });
                    req.company.businessUnits=filteredBUs;*!/
                    next();
                }
            });
        }
    });*/
};
/**
 * Company middleware
 */
exports.companyInfo= function (req, res, next, id) {
    Company.findById(id,'-updateHistory -segments -employees -products').populate('registrationCategory').exec(function (err, company) {
        if (err) return next(err);
        if (!company) return next(new Error('Failed to load Company ' + id));
        req.company = company;
        next();
    });
};
/**
 * Company With registration Segments middleware
 */
exports.companyRegistrationSegments = function (req, res, next, companyRegistrationId) {
    fetchCompanyByIdRegistrationCategories(companyRegistrationId, function (companyResponse) {
        /*  Company.findById(id).exec(function (err, company) {*/
        if (companyResponse instanceof Error) return next(companyResponse);
        if (!companyResponse) return next(new Error('Failed to load Company ' + companyRegistrationId));
        req.company = companyResponse;
        next();
    });
};
function getCategoriesQuery(categories,done) {
    var finalCategories=[];
    categories.forEach(function (eachCategory) {
        finalCategories.push({_id:eachCategory._id.toString()});
    });
    done(finalCategories);
}
function getBusinessUnitProducts(businessUnitId,done) {

    Inventory.aggregate([{$match:{$and:[{'currentOwner.businessUnit':businessUnitId,'deleted':false,'disabled':false}]}},
        {
            $group: {
                _id: '$productCategory'
            }
        }]).exec(function(err,bunitProducts) {
            if(bunitProducts && bunitProducts.length>0) {
                getCategoriesQuery(bunitProducts, function (result) {
                    Category.find({$or: result}).populate('inventories.inventory').exec(function (err, categories) {
                        done(err, categories);
                    });
                });
            }else{
                done(null,[]);
            }


    });
}
function getBusinessUnitOfferDetails(businessUnitId,done) {

    OffersUtil.findQueryByOffers([{'owner.businessUnit':businessUnitId,isPublic:true,'offerStatus':'Opened'}],-3, false, null,'own',function (offerErr,offers) {
       done(offerErr,offers);
    });

}

/**
 * Company middleware
 */

exports.companyByProfileUrl = function (req, res) {
    var profileUrl=req.query.companyProfileUrl;
    var bunit=req.query.bunit;
    Company.findOne({profileUrl: profileUrl},'-settings -updateHistory -companiesVersionKey -bankAccountDetailsProof -bankAccountDetails -panNumberProof -panNumber -cstNumber -cstNumberProof -vatNumber -vatNumberProof -tinNumber -tinNumberProof').populate('businessUnits.businessUnit').populate('user', 'displayName').exec(function (companyErr, resCompany) {
        if (companyErr || !resCompany) {
            logger.error('Error while loading the Company details with company url- ' + profileUrl);
            res.status(400).send({
                message: 'Error occurred while uploading company public page'
            });
        } else {
            if(!bunit){
                bunit=resCompany.businessUnits[0].businessUnit._id;
            }else{
                bunit = mongoose.Types.ObjectId(bunit);
            }
            var businessUnitDetails={offers:[],products:[]};
            getBusinessUnitProducts(bunit,function (errDetails,products) {
                if(errDetails){
                    res.status(400).send({
                        message: 'Error occurred while uploading company public page'
                    });
                } else{
                    businessUnitDetails.products=products;
                    getBusinessUnitOfferDetails(bunit,function (errOffer,offersList) {
                        if(errOffer){
                            res.status(400).send({
                                message: 'Error occurred while uploading company public page'
                            });
                        }else{
                            businessUnitDetails.offers=offersList;
                            businessUnitDetails.company=resCompany;
                            res.json(businessUnitDetails);
                        }
                    });

                }
            });
        }
    });
};

exports.companyProducts= function (req,res,next,companyProducts) {
    fetchCompanyById(companyProducts, function (companyResponse) {
        if (companyResponse instanceof Error) {
            logger.error('Failed to load Company ' + companyProducts);
            return next(companyResponse);
        } else {
            req.company = companyResponse;
            next();

        }
    });
};

/**
 * Company authorization middleware
 */
exports.hasAuthorization = function (req, res, next) {
    var token = req.body.token || req.headers.token;
    usersJWTUtil.findUserByToken(token, function (err, user) {
        if (err) {
            return res.status(400).send({
                message: errorHandler.getErrorMessage(err)
            });
        } else {
            if (req.company.user && (req.company.user.toString() !== user.id)) {
                return res.status(403).send('User is not authorized');
            } else {
                next();
            }
        }
    });

};


/**
 * Update profile picture
 */
exports.changeProfilePicture = function (req, res) {
    var token = req.body.token || req.headers.token;
    if (token) {
        logger.debug('Company Profile Picture [name:' + req.files.file.name + ', fieldname:' + req.files.file.fieldname + ', originalname:' + req.files.file.originalname + ']');
        usersJWTUtil.findUserByToken(token, function (err, user) {
            if (user) {
                fs.writeFile('./public/modules/companies/img/profile/uploads/' + req.files.file.name, req.files.file.buffer, function (uploadError) {
                    if (uploadError) {
                        return res.status(400).send({
                            message: 'Error occurred while uploading company profile picture'
                        });
                    } else {
                        Company.findById(user.company).exec(function (err, company) {
                            if (company) {
                                company.profileImageURL = 'modules/companies/img/profile/uploads/' + req.files.file.name;

                                company.save(function (saveError) {
                                    if (saveError) {
                                        return res.status(400).send({
                                            message: errorHandler.getErrorMessage(saveError)
                                        });
                                    } else {
                                        res.json(company);
                                    }
                                });
                            }
                        });
                    }
                });
            } else {
                res.status(400).send({
                    message: 'User is not signed in'
                });
            }
        });
    } else {
        res.status(400).send({
            message: 'User is not signed in'
        });
    }
};
exports.createContacts = function (req, res) {
    var token = req.body.token || req.headers.token;
    usersJWTUtil.findUserByToken(token, function (err, user) {
        if (err) {
            return res.status(400).send({
                message: errorHandler.getErrorMessage(err)
            });
        } else {
            if (req.company.user && (req.company.user.toString() !== user.id)) {
                Contact.findOne({
                    nVipaniUser: req.company.user,
                    nVipaniRegContact: true
                }, function (errContact, contact) {
                    if (errContact) {
                        logger.error('Error while loading the contact for the registration user id ' + req.company.user._id + ' after saving the company details with Company name ' + req.company.name, errContact);
                        return res.status(400).send({
                            message: errorHandler.getErrorMessage(errContact)
                        });
                    }

                    if (!contact) {
                        logger.error('Error while loading the contact for the user id ' + req.company.user._id + ' after saving the company details with Company name ' + req.company.name);
                        return res.status(400).send({
                            message: errorHandler.getErrorMessage('Failed to load registration Contact for the user id ' + req.company.user._id)
                        });
                    }
                    contact.isNew=true;
                    contact._id=null;
                    contact.user=user._id;
                    contact.nVipaniCompany=req.company._id;
                    contact.bbUser=true;
                    /*contact.addresses = req.company.addresses;
                    contact.emails = req.company.emails;
                    contact.phones = req.company.phones;*/
                    contact.nVipaniRegContact=false;
                    contact = _.extend(contact, {});
                    contact.save(function (contactError) {
                        if (contactError) {
                            return res.status(400).send({
                                message: errorHandler.getErrorMessage(contactError)
                            });
                        } else {
                            /* resCompany.user=resCompany.user._id.toString();*/
                            res.jsonp(req.company);
                        }
                    });

                });
                /* }*/
                /* });*/
            }
        }
    });

};
exports.listUserBusinessUnits = function(req,res) {
    var token = req.body.token || req.headers.token;
    var nvipaniUserId = req.param('nvipaniRegUser');
    var results = [];
    usersJWTUtil.findUserByToken(token, function (err, user) {
        if (err) {
            return res.status(400).send({
                message: errorHandler.getErrorMessage(err)
            });
        }
        /* Company.findOne({'employees.user': nvipaniUserId }, {employees: {$elemMatch: { user: nvipaniUserId }},
         }).populate('employees.businessUnits.businessUnit').exec(function(companyErr, company){
 */
        BusinessUnit.find({'employees.user': nvipaniUserId},'name addresses'
        ).exec(function(companyErr, businessUnits){
            if (companyErr) {
                return res.status(400).send({
                    status:false,
                    message: errorHandler.getErrorMessage(companyErr)
                });
            }else if(!businessUnits){
                logger.error('No Business units with the Contact user ID :'+nvipaniUserId);
                return res.status(400).send({
                    status:false,
                    message: 'No Business units with the Contact'
                });
            } else {
                if (businessUnits.length > 0) {
                    res.jsonp(businessUnits);
                }
                else {
                    return res.status(400).send({
                        status:false,
                        message: 'No employees found for the businessunit' + user.displayName
                    });
                }

            }

        });
    });
};
exports.companyDefaultBusinessUnit=function (req,res) {
    var token = req.body.token || req.headers.token;
    var businessUnit=req.body.businessUnit;
    usersJWTUtil.findUserCompanyByToken(token, function (err, user) {
        if (err) {
            return res.status(400).send({
                message: errorHandler.getErrorMessage(err)
            });
        } else {
            companyUtil.findDefaultCompanyBusinessUnit(user.company._id,businessUnit, function (err, businessUnit) {
                res.jsonp(businessUnit);
            });
        }
    });


};
