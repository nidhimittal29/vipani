'use strict';

/**
 * Module dependencies.
 */
var mongoose = require('mongoose'),
    errorHandler = require('./errors.server.controller'),
    usersJWTUtil = require('./utils/users.jwtutil'),
    logger = require('../../lib/log').getLogger('TODO', 'DEBUG'),
    Todo = mongoose.model('Todo'),
    _ = require('lodash');

/**
 * Create a Todo
 */
exports.create = function (req, res) {
    var token = req.body.token || req.headers.token;
    logger.debug('req.todo create -' + req.body);
    usersJWTUtil.findUserByToken(token, function (err, user) {
        if (err) {
            return res.status(400).send({
                message: errorHandler.getErrorMessage(err)
            });
        }
        var todo = new Todo(req.body);
        todo.user = user;

        todo.save(function (err) {
            if (err) {
                return res.status(400).send({
                    message: errorHandler.getErrorMessage(err)
                });
            } else {
                res.jsonp(todo);
            }
        });
    });
};

/**
 * Show the current Todo
 */
exports.read = function (req, res) {
    res.jsonp(req.todo);
};

/**
 * Update a Todo
 */
exports.update = function (req, res) {
    var todo = req.todo;

    todo = _.extend(todo, req.body);

    todo.save(function (err) {
        if (err) {
            return res.status(400).send({
                message: errorHandler.getErrorMessage(err)
            });
        } else {
            res.jsonp(todo);
        }
    });
};

/**
 * Delete an Todo
 */
exports.delete = function (req, res) {
    var todo = req.todo;

    todo.remove(function (err) {
        if (err) {
            return res.status(400).send({
                message: errorHandler.getErrorMessage(err)
            });
        } else {
            res.jsonp(todo);
        }
    });
};

/**
 * List of Todos
 */
exports.list = function (req, res) {
    var token = req.body.token || req.headers.token;
    usersJWTUtil.getUserByToken(token, function (err, loginuser) {
        if (err) {
            return res.status(400).send({
                message: errorHandler.getErrorMessage(err)
            });
        }
        Todo.find({target: loginuser.id,completed: false}).sort('-created').populate('user', 'displayName').exec(function (err, todos) {
            if (err) {
                return res.status(400).send({
                    message: errorHandler.getErrorMessage(err)
                });
            } else {
                res.jsonp(todos);
            }
        });
    });
};

/**
 * Todo middleware
 */
exports.todoByID = function (req, res, next, id) {
    Todo.findById(id).populate('user', 'displayName').exec(function (err, todo) {
        if (err) return next(err);
        if (!todo) return next(new Error('Failed to load Todo ' + id));
        req.todo = todo;
        next();
    });
};

/**
 * Todo authorization middleware
 */
exports.hasAuthorization = function (req, res, next) {
    var token = req.body.token || req.headers.token;
    usersJWTUtil.findUserByToken(token, function (err, user) {
        if (err) {
            return res.status(400).send({
                message: errorHandler.getErrorMessage(err)
            });
        } else {
            if (req.todo.user && req.todo.user.id !== user.id) {
                return res.status(403).send('User is not authorized');
            } else {
                next();
            }
        }
    });
};
