'use strict';

/**
 * Module dependencies.
 */
var mongoose = require('mongoose'),
	errorHandler = require('./errors.server.controller'),
	usersJWTUtil   = require('./utils/users.jwtutil'),
    dbUtil = require('./utils/common.db.util'),
	fs = require('fs'),
	Product = mongoose.model('Product'),
	Category=mongoose.model('Category'),

	logger = require('../../lib/log').getLogger('PRODUCT', 'DEBUG'),
	_ = require('lodash');

/**
 * Create a Product
 */
exports.create = function(req, res) {
	var product = new Product(req.body);
	var token = req.body.token || req.headers.token;
	logger.debug('req.product create -'+req.body);
	usersJWTUtil.findUserByToken(token, function(err, user) {
		if (err) {
			return res.status(400).send({
				message: errorHandler.getErrorMessage(err)
			});
		}
		product.user = user;
		product.user = user;
		product.updateHistory = [];
		product.updateHistory.push({modifiedOn: Date.now(), modifiedBy: user});
		product.save(function (err) {
			if (err) {
				return res.status(400).send({
					message: errorHandler.getErrorMessage(err)
				});
			} else {
				res.jsonp(product);
			}
		});
	});
};
/**
 * Create a Product
 */
exports.createProduct= function(req, res) {
    var category = new Category(req.body.product ? req.body.product : req.body);
    var token = req.body.token || req.headers.token;
    category.type='SubCategory2';
    usersJWTUtil.findUserByToken(token, function(err, user) {
        if (err) {
            return res.status(400).send({
                message: errorHandler.getErrorMessage(err)
            });
        }
        category.user = user;
        category.save(function (errCategory) {
            if (errCategory) {
                return res.status(400).send({
                    message: errorHandler.getErrorMessage(errCategory)
                });
            } else {
                Category.findOneAndUpdate({ _id: category.parent }, { $addToSet: { children: category._id } }, { new: true }, function(errSubCategory, doc) {
                	if(errSubCategory){
                        return res.status(400).send({
                            message: errorHandler.getErrorMessage(errSubCategory)
                        });
                    }else if(!doc){
                        return res.status(400).send({
                            message: 'Failed to Create Product with Name:'+category.name
                        });
					} else {
                        dbUtil.findCategoryById(doc._id,function (errFetch,populateCategory) {
                            if (errFetch) {
                                return res.status(400).send({
                                    message: errorHandler.getErrorMessage(errFetch)
                                });
                            }
                            res.jsonp(populateCategory);

                        });
                    }

                });
            }
        });
    });
};
/**
 * Show the current Product
 */
exports.read = function(req, res) {
	res.jsonp(req.product);
};

/**
 * Update a Product
 */
exports.update = function(req, res) {
	var product = req.product ;
	logger.debug('req.product before update -'+req.product.name);
	var versionKey = product.productsVersionKey;
	product = _.extend(product, req.body);

	var token = req.body.token || req.headers.token;
	usersJWTUtil.findUserByToken(token, function (err, user) {

		product.updateHistory.push({modifiedOn: Date.now(), modifiedBy: user});
		product.productsVersionKey = versionKey;
		logger.debug('req.product -' + req.product);
        product.set('lastUpdated', Date.now());
		product.save(function (err) {
			if (err) {
				return res.status(400).send({
					message: errorHandler.getErrorMessage(err)
				});
			} else {
				res.jsonp(product);
			}
		});
	});
};

/**
 * Delete an Product
 */
exports.delete = function(req, res) {
	var product = req.product ;

	product.remove(function(err) {
		if (err) {
			return res.status(400).send({
				message: errorHandler.getErrorMessage(err)
			});
		} else {
			res.jsonp(product);
		}
	});
};

/**
 * List of Products
 */
exports.list = function (req, res) {
	Product.find().sort('-created').populate('user', 'displayName').populate('category subCategory1 subCategory2', 'name code').exec(function (err, inventories) {
		if (err) {
			return res.status(400).send({
				message: errorHandler.getErrorMessage(err)
			});
		} else {
			res.jsonp(inventories);
		}
	});
};

/**
 *
 */


exports.productSearch = function (req, res) {
    var token = req.body.token || req.headers.token;
    var productKey = req.query.productKey;
    var lastSyncTime = req.headers.lastsynctime;
    var word=req.query.searchKey;
	/*usersJWTUtil.getUserByToken(token, function (err, loginuser) {
        if(err){
            logger.error('Error while fetching products with subcategory2 :'+subCategory2Id+' using token number :'+token+' Details:'+errorHandler.getErrorMessage(err));
            return res.status(400).send({
                message: errorHandler.getErrorMessage(err)
            });
        }else{*/


            var queryArray = [];
			var regExp={'$regex': productKey };
            for (var property in Product.schema.paths) {
				if (Product.schema.paths.hasOwnProperty(property)  && Product.schema.paths[property].instance === 'String') {
                    var eachproduct={};
                	eachproduct[property] = regExp;
                    queryArray.push(eachproduct);
                }

            }

            var query={ $or : queryArray};

            Product.find(query).populate('user', 'displayName').populate('category subCategory1 subCategory2', 'name code productAttributes inventoryAttributes searchFilters').exec(function (productsErr, products) {
                if (productsErr) {
                    logger.error('Error while fetching products with value: :'+productKey+' Details :'+errorHandler.getErrorMessage(productsErr));
                    return res.status(400).send({
                        message: errorHandler.getErrorMessage(productsErr)
                    });
                } else {
                    logger.debug('Successfully  fetched products with value :'+productKey );
                    res.jsonp(products);
                }
            });
    /*    }*/
   /* });*/
};
/** 
 * List of subCategory2 Products
 */
exports.subCategory2InventoryLists = function (req, res) {
	var token = req.body.token || req.headers.token;
	var subCategory2Id = req.query.subCategory2Id;
	var lastSyncTime = req.headers.lastsynctime;
	var word=req.query.searchKey;
	usersJWTUtil.getUserByToken(token, function (err, loginuser) {
		if(err){
			logger.error('Error while fetching products with subcategory2 :'+subCategory2Id+' using token number :'+token+' Details:'+errorHandler.getErrorMessage(err));
			return res.status(400).send({
				message: errorHandler.getErrorMessage(err)
			});
		}else{
			var query = {subCategory2: subCategory2Id};
			if (lastSyncTime) {
				query.lastUpdated= {$gt: lastSyncTime};
			}
			if(word){
				query.name = {'$regex' : '.*' + word + '.*','$options': 'i'};
			}
            Product.find(query).populate('user', 'displayName').populate('category subCategory1 subCategory2', 'name code productAttributes inventoryAttributes searchFilters').exec(function (productsErr, products) {
				if (productsErr) {
					logger.error('Error while fetching products with subcategory2 :'+subCategory2Id+' Details :'+errorHandler.getErrorMessage(productsErr));
					return res.status(400).send({
						message: errorHandler.getErrorMessage(productsErr)
					});
				} else {
					logger.debug('Successfully  fecthed products with subcategory2 :'+subCategory2Id );
					res.jsonp(products);
				}
			});
		}
	});
};
/**
 * Product middleware
 */
exports.productByID = function(req, res, next, id) {
	Product.findById(id).populate('user', 'displayName').exec(function(err, product) {
		if (err) return next(err);
		if (! product) return next(new Error('Failed to load Product ' + id));
		req.product = product ;
		next();
	});
};

/**
 * Product authorization middleware
 */
exports.hasAuthorization = function(req, res, next) {
	var token = req.body.token || req.headers.token;
	usersJWTUtil.getUserByToken(token, function(err, user) {
		if (err) {
			return res.status(400).send({
				message: errorHandler.getErrorMessage(err)
			});
		} else {
			if ((req.product.user !== null) && (req.product.user.id.toString() !== user.id)) {
				return res.status(403).send('User is not authorized');
			} else {
				next();
			}
		}
	});

};

/**
 * Update product picture
 */
exports.changeProductPicture = function (req, res) {
	var token = req.body.token || req.headers.token;
	if (token) {
		logger.debug('Product Picture [name:' + req.files.file.name + ', fieldname:' + req.files.file.fieldname + ', originalname:' + req.files.file.originalname + ']');
		usersJWTUtil.findUserByToken(token, function (err, user) {
			if (user) {
				fs.writeFile('./public/modules/products/img/profile/uploads/' + req.files.file.name, req.files.file.buffer, function (uploadError) {
					if (uploadError) {
						logger.error(uploadError);
						logger.error(errorHandler.getErrorMessage(uploadError));
						return res.status(400).send({
							message: 'Error occurred while uploading product picture - ' + errorHandler.getErrorMessage(uploadError)
						});
					} else {
						var productImageURL = 'modules/products/img/profile/uploads/' + req.files.file.name;
						res.json({imageURL:productImageURL});
					}
				});
			} else {
				res.status(400).send({
					message: 'User is not signed in'
				});
			}
		});
	} else {
		res.status(400).send({
			message: 'User is not signed in'
		});
	}
};

