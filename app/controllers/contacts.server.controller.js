'use strict';

/**
 * Module dependencies.
 */
var mongoose = require('mongoose'),
    errorHandler = require('./errors.server.controller'),
    usersJWTUtil = require('./utils/users.jwtutil'),
    contactUtil = require('./utils/common.contacts.util'),
    dbUtil = require('./utils/common.db.util'),
    logger = require('../../lib/log').getLogger('CONTACT', 'DEBUG'),
    User = mongoose.model('User'),
    config = require('../../config/config'),
    Contact = mongoose.model('Contact'),
    async = require('async'),
    fs = require('fs'),
    Group = mongoose.model('Group'),
    _ = require('lodash');

function isCurrentCompanyUser(contact,companyId,done) {
    contactUtil.getContactPlayersQuery(contact,function (query) {
        if(query.length>0){
            User.find({username:{$in:query},'company':companyId},function (err,companyUsersContact) {
                done(err,companyUsersContact);
            });
        }else {
            done(null,null);
        }
    });
}
/**
 *
 */
var createContact = function(contact,user,date,done){
    if((contact.phones && contact.phones.length===0) && (contact.emails && contact.emails.length===0)){
        return done(new Error('At least one phone or email is required to create contact'));
    }
    else {
        isCurrentCompanyUser(contact,user.company,function (err,currentCompanyUser) {
            if(err){
                logger.error('Error while checking the Contact in company users.');
                done(err);
            }else if(currentCompanyUser && currentCompanyUser.length>0){
                logger.error('Error while creating the Contact.');
                done(new Error('Not allowed to add the company user as contact'));
            }else{
                contactUtil.matchContact(contact, user, function (contactMatchErr, matchedContacts) {
                    if (contactMatchErr) {
                        done(contactMatchErr);
                    }
                    else if (matchedContacts && matchedContacts.length > 0) {
                        done(new Error('Contact ' + contact.displayName + ' already exists. Cannot be added.'));
                    }
                    else {
                        /*contact.user = user._id;*/
                        contact.user=user._id;
                        contact.company=user.company;
                        contact.save(function (contactSaveErr) {
                            done(contactSaveErr, contact);
                        });

                    }
                });
            }
        });
    }
};

function contactValidation(contact, done) {
    if(!contact.displayName){
        logger.error('Display name not send from client to create contact'+contact);
        done(new Error('Display name is required for create contact'));
    }else if(typeof (contact.displayName)!== 'string'){
        logger.error('Display name not send as string from client to create contact'+contact);
        done(new Error('Display name is required as string for create contact'));
    }else if(!(contact.phones || contact.emails)){
        logger.error('Phone number or email id not send from client to create contact'+contact);
        done(new Error('Phone number or email id is required for create contact'));
    } else if(!((contact.phones && contact.phones.length>=0) || (contact.emails && contact.emails.length>=0))){
        logger.error('One phone number or email id not send from client to create contact'+contact);
        done(new Error('At least one phone or email is required to create contact'));
    }else {
        done(null);
    }
}


/**
 * Create a Contact
 */

exports.create = function (req, res) {
    var token = req.body.token || req.headers.token;
    var contact=req.body;
    usersJWTUtil.findUserByToken(token, function (err, user) {
        if (err) {
            logger.error('Error while getting user for create contact with token' + token + 'Error:' + err);
            return res.status(400).send({
                message: errorHandler.getErrorMessage(err)
            });
        }else if(!user){
            logger.error('User object getting null for create contact with token' + token);
            return res.status(400).send({
                message: 'No user to create contact'
            });
        } else {
            contactValidation(contact, function (err) {
                if (err) {
                    return res.status(400).send({
                        message: errorHandler.getErrorMessage(err)
                    });
                } else {
                    contact = new Contact(req.body);
                    isCurrentCompanyUser(contact, user.company,function (err, currentCompanyUser) {
                        if(err){
                            logger.error('Error while checking the Contact in company users.');
                            return res.status(400).send({
                                message: errorHandler.getErrorMessage(err)
                            });
                        }else if(currentCompanyUser && currentCompanyUser.length>0){
                            logger.error('Error while creating the Contact.');
                            return res.status(400).send({
                                message: 'Not allowed to add the company user as contact'
                            });
                        }else{
                            contact.user = user;
                            var date = Date.now();
                            contact.set('created', date);
                            contact.set('lastUpdated', date);
                            contact.company = user.company;
                            contact.save(function (contactSaveErr) {
                                if (contactSaveErr) {
                                    logger.error('Error while creating the Contact.', contactSaveErr);
                                    return res.status(400).send({
                                        message: errorHandler.getErrorMessage(contactSaveErr)
                                    });
                                } else {
                                    logger.debug('Contact created successfully' + contact);
                                    res.jsonp(contact);
                                }
                            });
                        }
                    });

                }
            });
        }
    });

};


/**
 * Show the current Contact
 */
exports.read = function (req, res) {
    res.jsonp(req.contact);
};

/**
 * Update a Contact
 */
exports.update = function (req, res) {
    var contact = req.contact;
    var token = req.body.token || req.headers.token;
    usersJWTUtil.findUserByToken(token, function (err, user) {
        if (err) {
            logger.error('Error while getting user for create contact with token' + token + 'Error:' + err);
            return res.status(400).send({
                message: errorHandler.getErrorMessage(err)
            });
        } else if (!user) {
            logger.error('User object getting null for create contact with token' + token);
            return res.status(400).send({
                message: 'No user to create contact'
            });
        } else {
            contactValidation(contact, function (err) {
                if (err) {
                    return res.status(400).send({
                        message: errorHandler.getErrorMessage(err)
                    });
                } else {
                    var versionKey = contact.contactsVersionKey;
                    var emails=contact.emails.filter(function (email) { return email.isPublic;}).concat(req.body.emails);
                    var phones=contact.phones.filter(function (eachPhone){ return eachPhone.isPublic;}).concat(req.body.phones);
                    contact = _.extend(contact, req.body);
                    contact.phones=phones;
                    contact.emails=emails;
                    contact.contactsVersionKey = versionKey;

                    contact.set('lastUpdated', Date.now());

                    usersJWTUtil.findUserByToken(token, function (err, user) {
                        if (err) {
                            //logger.debug('err 1 -'+err);
                            //logger.debug('err 1 -'+JSON.stringify(err));
                            return res.status(400).send({
                                message: errorHandler.getErrorMessage(err)
                            });
                        } else{
                            contact.save(function (contactSaveErr) {
                                if (contactSaveErr) {
                                    logger.error('Error while updating the Contact.', contactSaveErr);
                                    return res.status(400).send({
                                        message: errorHandler.getErrorMessage(contactSaveErr)
                                    });
                                }else{
                                    logger.debug('Contact updated successfully :'+contact);
                                    res.jsonp(contact);
                                }
                            });
                        }
                    });
                }
            });
        }
    });
};

function getFilterArray(field) {
    var queryObject= {Inactive:{$and:[{'nVipaniRegContact':false},{'disabled': true}]}, NewlyAdded:{$and:[{'nVipaniRegContact':false},{'created':{$lt: new Date(Date.now())}}]}};
    for (var key in queryObject) {
        if(field && key===field)
            return queryObject[key];
    }
    return null;
}

function getQuery(summaryFilters,queryArray,done) {
    if(summaryFilters) {
        for (var key in summaryFilters) {
            var value=getFilterArray(key);
            if(value) {
                queryArray.push(value);
            }
        }
    }
    done(queryArray);

}

function contactQuery(req,isCustomer,done) {
    var token = req.body.token || req.headers.token;
    var lastSyncTime = req.headers.lastsynctime;
    logger.info('Fetching user contacts');
    usersJWTUtil.findUserByToken(token, function (err, loginuser) {
        if (err) {
            logger.error('Error while fetching the user with the given token for fetching user contacts.');
            done(err, null);
        } else {
            var pageOptions = {};
            if (req.query.page && !isNaN(req.query.page)) {

                pageOptions.page = parseInt(req.query.page);
            }
            if (req.query.limit) {
                pageOptions.limit = parseInt(req.query.limit);
            }
            var query=isCustomer?({$and: [{'company': loginuser.company.toString()},
                        {'customerType': 'Customer'}]})
                :({$and:[{'company': loginuser.company.toString()},
                        {'customerType': 'Supplier'}]
                });
            if (lastSyncTime) {
                query.lastUpdated = {$gt: lastSyncTime};
            }
            var queryArray=[];
            if(req.query.searchText){
                var regExp={'$regex': new RegExp(req.query.searchText, 'g')};
                for (var property in Contact.schema.paths) {
                    if (Contact.schema.paths.hasOwnProperty(property)  && Contact.schema.paths[property].instance === 'String') {
                        var eachproduct={};
                        eachproduct[property] = regExp;
                        queryArray.push(eachproduct);
                    }

                }
                queryArray.push({'emails.email':regExp});
                queryArray.push({'phones.phoneNumber':regExp});
            }
            getQuery(req.body.summaryFilters, queryArray, function (queryArray) {
                var finalQuery = null;
                if (queryArray.length > 0)
                    finalQuery = {$and: [query, {$or: queryArray}]};
                else
                    finalQuery = query;
                done(null, finalQuery, pageOptions);
            });
        }
    });
}

exports.getContacts = function (req, res) {
    var token = req.body.token || req.headers.token;
    var companyId=req.query.companyId;
    usersJWTUtil.findUserByToken(token, function (err, user) {
        Contact.findOne({
            nVipaniUser: user._id,
            nVipaniCompany: companyId
        }, function (errContact, contact) {
            if (errContact) {
                return res.status(400).send({
                    status: false,
                    message: 'Email or phone is required for contact creation'
                });
            }else{
                res.json({contact: contact});
            }
        });
    });
};
exports.delete = function (req, res) {
    var contact = req.contact;

    contact.remove(function (err) {
        if (err) {
            return res.status(400).send({
                message: errorHandler.getErrorMessage(err)
            });
        } else {
            res.jsonp(contact);
        }
    });
};

/**
 * List of Contacts
 */
exports.list = function (req, res) {
    var token = req.body.token || req.headers.token;

    var lastSyncTime = req.headers.lastsynctime;
    usersJWTUtil.findUserByToken(token, function (err, loginuser) {
        if (loginuser) {
            var query;
            query = {company: loginuser.company};
            if (lastSyncTime) {
                query.lastUpdated= {$gt: lastSyncTime};
            }
            contactUtil.findQueryByContacts([query],-1,null,null,function (err, contacts) {
                if (err) {
                    return res.status(400).send({
                        message: errorHandler.getErrorMessage(err)
                    });
                } else {
                    res.jsonp(contacts);
                }
            });
        }
    });
};

/**
 * List of Contacts for Customer
 */
exports.queryByCustomer = function (req, res) {
    contactQuery(req,true,function(err,query,pageOptions){
        if(err) {
            return res.status(400).send({
                status: false,
                message: errorHandler.getErrorMessage(err)
            });
        }else{
            contactUtil.findQueryByContacts([query],0,true,pageOptions,function (contactErr,contacts) {
                if (contactErr) {
                    logger.error('Error while fetching user contacta ');
                    return res.status(400).send({
                        message: errorHandler.getErrorMessage(contactErr)
                    });
                } else {
                    res.jsonp(contacts);
                }
            });
        }
    });
};


/**
 * List of Contacts for Suppliers
 */
exports.queryBySupplier = function (req, res) {
    contactQuery(req,false,function(err,query,pageOptions){
        if(err) {
            return res.status(400).send({
                status: false,
                message: errorHandler.getErrorMessage(err)
            });
        }else{
            contactUtil.findQueryByContacts([query],0,true,pageOptions,function (contactErr,contacts) {
                if (contactErr) {
                    logger.error('Error while fetching user contacta ');
                    return res.status(400).send({
                        message: errorHandler.getErrorMessage(contactErr)
                    });
                } else {
                    res.jsonp(contacts);
                }
            });
        }
    });
};

/**
 * Contact middleware
 */
exports.contactByID = function (req, res, next, id) {
    Contact.findById(id).populate('user', 'displayName').populate('paymentTerm').exec(function (err, contact) {
        if (err) return next(err);
        if (!contact) return next(new Error('Failed to load Contact ' + id));
        req.contact = contact;
        next();
    });
};

/**
 * Contact authorization middleware
 */
exports.hasAuthorization = function (req, res, next) {
    var token = req.body.token || req.headers.token;
    usersJWTUtil.findUserByToken(token, function (err, user) {
        if (err) {
            if (err) {
                return res.status(400).send({
                    message: errorHandler.getErrorMessage(err)
                });
            }
        } else {
            if ((req.contact.company !== null) && (req.contact.company.toString() !== user.company.toString())) {
                return res.status(403).send('User is not authorized');
            } else {
                next();
            }
        }
    });
};

exports.getValidContact = function (req, res) {
    var token = req.body.token || req.headers.token;
    var companyId=req.query.companyId;
    usersJWTUtil.findUserByToken(token, function (err, user) {
        Contact.findOne({
            nVipaniCompany: companyId,
            user: user._id.toString(),
        }, function (errContact, contact) {
            if (errContact) {
                logger.error('Error while loading the contact for the registration user id ' + req.company.user._id + ' after saving the company details with Company name ' + req.company.name, errContact);
                return res.status(400).send({
                    message: errorHandler.getErrorMessage(errContact)
                });
            }

            if (contact) {
                res.json({contactExists: true});
            }
        });


    });
};
exports.changeContactProfilePicture = function (req, res) {
    var token = req.body.token || req.headers.token;
    if (token) {
        logger.debug('Contact Profile Picture [name:' + req.files.file.name + ', fieldname:' + req.files.file.fieldname + ', originalname:' + req.files.file.originalname + ']');
        usersJWTUtil.findUserByToken(token, function (err, user) {
            if (user) {
                fs.writeFile('./public/modules/contacts/img/profile/' + req.files.file.name, req.files.file.buffer, function (uploadError) {
                    if (uploadError) {
                        return res.status(400).send({
                            status: false,
                            message: 'Error occurred while uploading company profile picture'
                        });
                    } else {
                        var contactImageURL = 'modules/contacts/img/profile/' + req.files.file.name;
                        res.json({imageURL: contactImageURL});
                    }
                });
            } else {
                res.status(400).send({
                    status: false,
                    message: 'User is not signed in'
                });
            }
        });
    } else {
        res.status(400).send({
            status: false,
            message: 'User is not signed in'
        });
    }
};
exports.massDelete=function (req,res) {
    var token = req.body.token || req.headers.token;
    var contacts=req.body.contacts;
    var type=req.body.type;
    usersJWTUtil.findUserByToken(token, function (err, user) {
        if(err){
            return res.status(400).send({
                message: errorHandler.getErrorMessage(err)
            });
        }else {
            if(req.body.contacts && req.body.contacts.length>0){
                var query={company:user.company,customerType:type};
                if(contacts && contacts.length>0){
                    query.$or=contacts;
                }
                contactUtil.batchContactUpdate(query,{'deleted':true}, function (contactErr, results) {
                    if(contactErr){
                        return res.status(400).send({
                            message: errorHandler.getErrorMessage(contactErr)
                        });
                    }else  if(!results){
                        return res.status(400).send({
                            message: 'Not Deleted',
                            status:false
                        });
                    }else {
                        contactUtil.findQueryByContacts([{company:user.company,customerType:type,nVipaniRegContact:false}],0,false,{page:1,limit:10},function (contactFetchErr,contacts) {
                            if(contactFetchErr){
                                return res.status(400).send({
                                    message: errorHandler.getErrorMessage(contactFetchErr)
                                });
                            }else{
                                res.jsonp(contacts);
                            }

                        });
                    }

                });
            }else {
                return res.status(400).send({
                    message: 'No Selected Contacts'
                });
            }
        }
    });
};
exports.massDisable=function (req,res) {
    var token = req.body.token || req.headers.token;
    var contacts=req.body.contacts;
    var type=req.body.type;
    usersJWTUtil.findUserByToken(token, function (err, user) {
        if(err){
            return res.status(400).send({
                message: errorHandler.getErrorMessage(err)
            });
        }else {
            if (req.body.contacts && req.body.contacts.length>0){
                var query={'$or':contacts,company:user.company,customerType:type};
                contactUtil.batchContactUpdate(query,{'disabled':true}, function (contactErr, results) {
                    if(contactErr){
                        return res.status(400).send({
                            message: errorHandler.getErrorMessage(contactErr)
                        });
                    }else if(!results){
                        return res.status(400).send({
                            message: 'Not Disabled',
                            status:false
                        });
                    }else {
                        contactUtil.findQueryByContacts([query],-1,false,{page:0,limit:10},function (contactFetchErr,contacts) {
                            if(contactFetchErr){
                                return res.status(400).send({
                                    message: errorHandler.getErrorMessage(contactFetchErr)
                                });
                            }else{
                                res.jsonp(contacts);
                            }

                        });
                    }

                });
            }else {
                return res.status(400).send({
                    message: 'No Selected Contacts'
                });
            }
        }
    });
};
exports.massEnable=function (req,res) {
    var token = req.body.token || req.headers.token;
    var contacts=req.body.contacts;
    var type=req.body.type;
    usersJWTUtil.findUserByToken(token, function (err, user) {
        if(err){
            return res.status(400).send({
                message: errorHandler.getErrorMessage(err)
            });
        }else {
            if (req.body.contacts && req.body.contacts.length>0){
                var query = {'$or': contacts, company:user.company, customerType: type};
                contactUtil.batchContactUpdate(query, {'disabled': false}, function (contactErr, results) {
                    if (contactErr) {
                        return res.status(400).send({
                            message: errorHandler.getErrorMessage(contactErr)
                        });
                    } else if (!results) {
                        return res.status(400).send({
                            message: 'Not Enabled',
                            status: false
                        });
                    } else {
                        contactUtil.findQueryByContacts([query], -1, false, {
                            page: 0,
                            limit: 10
                        }, function (contactFetchErr, contacts) {
                            if (contactFetchErr) {
                                return res.status(400).send({
                                    message: errorHandler.getErrorMessage(contactFetchErr)
                                });
                            } else {
                                res.jsonp(contacts);
                            }

                        });
                    }

                });
            }else {
                return res.status(400).send({
                    message: 'No Selected Contacts'
                });
            }
        }
    });
};

var getPhone = function(contact,phoneFields,headers,rowData,errors){
    if(!contact.phones || contact.phones.length===0){
        contact.phones=[];
    }
    var phones = contact.phones;
    for (var k = 0; k < phoneFields.length; k++) {

        var field = phoneFields[k];
        var value = rowData.lineData[headers.indexOf(field)];
        var strings = field.split('.');
        //Billing.1.AddressLine,Billing.1.City,Billing.1.State,Billing.1.Country,Billing.1.PinCode
        if(value && value.trim()!=='') {
            var phone1={};
            switch (strings[2]) {
                case 'Mobile':
                case 'Work':
                case 'Home':
                case 'Other':
                    phone1.primary=false;
                     phone1.phoneType = strings[2];
                    phone1.phoneNumber = value;
                    phones.push(phone1);
                    break;
                default:
                    errors.push(strings[2] + ' is an invalid phone type and was not added');
            }
        }

    }
    return phones;
};

var getAddress = function(addressType,addressFields,headers,rowData,errors){
    var address1={};
    for (var k = 0; k < addressFields.length; k++) {
        var field = addressFields[k];
        var value = rowData.lineData[headers.indexOf(field)];
        var strings = field.split('.');
        //Billing.1.AddressLine,Billing.1.City,Billing.1.State,Billing.1.Country,Billing.1.PinCode
        if(value && value.trim()!=='') {

            switch (strings[2]) {
                case 'AddressLine':
                    address1.addressLine = value;
                    break;
                case 'City':
                    address1.city = value;
                    break;
                case 'State':
                    address1.state = value;
                    break;
                case 'Country':
                    address1.country = value;
                    break;
                case 'PinCode':
                    address1.pinCode = value;
                    break;

            }
        }
    }
    return address1;
};
var getEmail = function(contact,emailFields,headers,rowData,errors){
    if(!contact.emails || contact.emails.length===0){
        contact.emails=[];
    }
    var emails=contact.emails;
    for (var k = 0; k < emailFields.length; k++) {

        var field = emailFields[k];
        var value = rowData.lineData[headers.indexOf(field)];
        var strings = field.split('.');
        //Billing.1.AddressLine,Billing.1.City,Billing.1.State,Billing.1.Country,Billing.1.PinCode
        if(value) {
            var email1={};
            switch (strings[2]) {
                case 'Mobile':
                case 'Work':
                case 'Home':
                case 'Other':
                    email1.primary=false;
                    email1.emailType = strings[2];
                    email1.email = value;
                    emails.push(email1);
                    break;
                default:
                    errors.push(strings[2] + ' is an invalid email type and was not added');
            }
        }
    }
    return emails;
};

var getContactDetail = function(type,contact,headers,obj,errors){
    var index = 1;
    while(true){
        var phoneFields = headers.filter(function (item) {
            return item.match('^'+type+'.' + index);
        });
        if (phoneFields && phoneFields.length > 0) {
            switch(type) {
                case 'Phone': getPhone(contact, phoneFields, headers, obj, errors);break;
                case 'Email': getEmail(contact,phoneFields,headers,obj,errors);break;
            }
        }
        else {
            break;
        }
        //added an exit condition in case of any unexpected user error.
        if(index>25){
            break;
        }
        index++;
    }
};
var getAddresses = function(addressType,contact,headers,obj){
    var index=1;
    while(true){
        var billingAddressFields = headers.filter(function (item) {
            return item.match('^'+addressType +'.'+ index);
        });
        if (billingAddressFields && billingAddressFields.length > 0) {
            var address = getAddress(addressType,billingAddressFields, headers, obj);
            if (address && address.pinCode) {
                address.addressType = addressType;
                contact.addresses.push(address);
            }
        }
        else {
            break;
        }
        //added an exit condition in case of any unexpected user error.
        if(index>25){
            break;
        }
        index++;
    }

};

/**
 * This api is used to validate the csv file.
 * @param req
 * @param res
 */
exports.preMassImportValidate = function(req,res){
    //the contacts should have data.
    var dataValues = req.body.contacts.dataValues;
    //header should have the header field
    var headers = req.body.contacts.headers;
    var retVal = {errors:[],contacts:[]};
    var indexOfDisplayName = headers.indexOf('DisplayName');
    var indexOfFirstName = headers.indexOf('FirstName');
    var indexOfLastName = headers.indexOf('LastName');
    var indexOfMiddleName = headers.indexOf('MiddleName');
    var indexOfType = headers.indexOf('Type');
    var indexOfPrimaryPhoneNumber = headers.indexOf('PrimaryPhone');
    var indexOfPrimaryPhoneType = headers.indexOf('PrimaryPhoneType');
    var indexOfPrimaryEmail = headers.indexOf('PrimaryEmail');
    var indexOfPrimaryEmailType = headers.indexOf('PrimaryEmailType');
    var indexOfPaymentTerm = headers.indexOf('PaymentTerm');
    //dataValues is expected to have data & srcline.
    async.forEachSeries(dataValues,function(dataValue,callback){
        var rowError = {};
        rowError.errors = [];
        var srcLine = dataValue.srcLine;
        var obj = dataValue;
        var values = obj.lineData;
        var contact = {
            customerType: values[indexOfType],
            displayName: values[indexOfDisplayName],
            firstName: values[indexOfFirstName],
            middleName: values[indexOfMiddleName],
            lastName: values[indexOfLastName]

        };
        contact.phones = [];
        contact.emails = [];


        if (values[indexOfPrimaryPhoneNumber]) {
            if (indexOfPrimaryPhoneNumber !== -1 && indexOfPrimaryPhoneType !== -1 &&
                values[indexOfPrimaryPhoneNumber] && values[indexOfPrimaryPhoneType]) {
                contact.phones.push(
                    {
                        primary: true,
                        phoneType: values[indexOfPrimaryPhoneType],
                        phoneNumber: values[indexOfPrimaryPhoneNumber]
                    }
                );
            }
            else {
                rowError.srcLine = srcLine;
                rowError.errors.push('Primary phone number must be specified.');
            }
        }
        if (values[indexOfPrimaryEmail]) {
            if (indexOfPrimaryEmail !== -1 && indexOfPrimaryEmailType !== -1 &&
                 values[indexOfPrimaryEmail] && values[indexOfPrimaryEmailType]) {
                contact.emails.push
                ({
                        primary: true,
                        emailType: values[indexOfPrimaryEmailType],
                        email: values[indexOfPrimaryEmail]
                    }
                );
            }

        }
        contact.addresses = [];

        //upto 5 phones and emails
        getContactDetail('Phone',contact,headers,obj,rowError.errors);
        getContactDetail('Email',contact,headers,obj,rowError.errors);
        getAddresses('Billing',contact,headers,obj);
        getAddresses('Shipping',contact,headers,obj);
        if (headers.indexOf('GSTIN') >= 0) {
            contact.gstinNumber = values[headers.indexOf('GSTIN')];
        }
        if (headers.indexOf('CompanyName') >= 0) {
            contact.companyName = values[headers.indexOf('CompanyName')];
        }
        var paymentTerm = values[indexOfPaymentTerm];
        //rowError should have srcline and message.
        if(paymentTerm) {
            contactUtil.findPaymentTerm(paymentTerm, function (ptErr, pTerm) {
                if (ptErr) {
                    logger.error(errorHandler.getErrorMessage(ptErr));
                    rowError.errors.push(errorHandler.getErrorMessage(ptErr));
                    rowError.srcLine = srcLine;
                    retVal.errors.push(rowError);
                    callback();
                }
                else {
                    contact.paymentTerm = pTerm._id;
                    contact.deleted = false;
                    contact.disabled = false;
                    retVal.contacts.push({contact:contact,srcLine:srcLine});
                    if (rowError.srcLine && rowError.errors && rowError.errors.length>0) {
                        retVal.errors.push(rowError);
                    }
                    callback();

                }
            });
        }else{
            contact.deleted = false;
            contact.disabled = false;
            retVal.contacts.push({contact:contact,srcLine:srcLine});
            if (rowError.srcLine && rowError.errors && rowError.errors.length>0) {
                retVal.errors.push(rowError);
            }
            callback();
        }
    },function(err){
        if(err){
            logger.error(errorHandler.getErrorMessage(err));
            return res.status(400).send(errorHandler.getErrorMessage(err));
        }
        else {
            res.json(retVal);
        }
    });
};



exports.massImport = function(req,res){
    var contacts =req.body.contacts;
    var retVal={errors:[], contacts:[]};
    var token = req.body.token || req.headers.token;
    logger.debug('create contact -' + JSON.stringify(req.body));
    usersJWTUtil.findUserByToken(token, function (err, user) {
        if (err) {
            logger.error(errorHandler.getErrorMessage(err));
            return res.status(400).send({
                message: errorHandler.getErrorMessage(err)
            });
        } else {
            async.forEachSeries(contacts,function(contact,callback){
                var srcLine = contact.srcLine;
                var contakt = new Contact(contact.contact);
                var date = Date.now();
                contakt.set('created', date);
                contakt.set('lastUpdated', date);

                //logger.debug('Before:'+JSON.stringify(contact));
                //logger.debug('After:'+JSON.stringify(contakt));
                createContact(contakt,user,date,function(createContactErr,savedContact){
                    if(createContactErr){
                        logger.error(errorHandler.getErrorMessage(createContactErr));
                        retVal.errors.push({message:errorHandler.getErrorMessage(createContactErr),srcLine:srcLine});
                        callback();
                    }
                    else{
                        retVal.contacts.push({contact:savedContact,srcLine:contact.srcLine});
                        callback();
                    }
                });
            },function(err){
                if(err){
                    return res.status(400).send({message:errorHandler.getErrorMessage(err)});
                }
                else{
                    res.jsonp(retVal);
                }
            });
        }
    });
};
exports.getContactOffers=function (req,res) {


    var token = req.body.token || req.headers.token;
    var orders=req.body.orders;
    var contactId=req.params.offerContactId;
    var type=req.body.type;
    usersJWTUtil.findUserByToken(token,function (err,user) {
        if(err){
            return res.status(400).send({
                status:false,
                message: errorHandler.getErrorMessage(err)
            });
        }else {
            contactUtil.getContactOffers(contactId, user,function (offerErr, offers) {
                if(offerErr) {
                    return res.status(400).send({
                        status: false,
                        message: errorHandler.getErrorMessage(offerErr)
                    });
                }else{
                    res.jsonp(offers);
                }
            });
        }
    });
};
exports.getContactOrders=function (req,res) {
    var token = req.body.token || req.headers.token;
    var orders=req.body.orders;
    var contactId=req.params.orderContactId;
    var type=req.body.type;
    usersJWTUtil.findUserByToken(token,function (err,user) {
        if (err) {
            return res.status(400).send({
                status: false,
                message: errorHandler.getErrorMessage(err)
            });
        } else {
            contactUtil.getContactOrders(contactId,user,function (orderErrors,orders) {
                if(orderErrors) {
                    return res.status(400).send({
                        status: false,
                        message: errorHandler.getErrorMessage(orderErrors)
                    });
                }else{
                    res.jsonp(orders);
                }
            });
        }
    });

};
exports.getGroupOffers=function (req,res) {
    var token = req.body.token || req.headers.token;
    var orders=req.body.orders;
    var groupId=req.query.offerGroupId;
    var type=req.body.type;
    usersJWTUtil.findUserByToken(token,function (err,user) {
        if(err){
            return res.status(400).send({
                status:false,
                message: errorHandler.getErrorMessage(err)
            });
        }else {
            contactUtil.getGroupOffers(groupId, user,function (offerErr, offers) {
                if(offerErr) {
                    return res.status(400).send({
                        status: false,
                        message: errorHandler.getErrorMessage(offerErr)
                    });
                }else{
                    res.jsonp(offers);
                }
            });
        }
    });

};




exports.getCustomerContactsSummaries=function (req,res) {
    contactQuery(req,true,function(err,query,pageOptions){
        if(err) {
            return res.status(400).send({
                status: false,
                message: errorHandler.getErrorMessage(err)
            });
        }else{
            contactUtil.findQueryByContacts([query],0,false,pageOptions,function (contactErr,contacts) {
                if(contactErr){
                    logger.error('Error while fetching user contacta ');
                    return res.status(400).send({
                        message: errorHandler.getErrorMessage(contactErr)
                    });
                }else{
                    contactUtil.getContactsSummary(contacts.contact,req.body.summaryFilters, function ( summaryData) {
                        res.jsonp(summaryData);
                    });
                }

            });
        }
    });
};

exports.getSupplierContactsSummaries=function (req,res) {
    contactQuery(req,false,function(err,query,pageOptions){
        if(err) {
            return res.status(400).send({
                status: false,
                message: errorHandler.getErrorMessage(err)
            });
        }else{
            contactUtil.findQueryByContacts([query],0,false,pageOptions,function (contactErr,contacts) {
                if(contactErr){
                    logger.error('Error while fetching user contacta ');
                    return res.status(400).send({
                        message: errorHandler.getErrorMessage(contactErr)
                    });
                }else{
                    contactUtil.getContactsSummary(contacts.contact,req.body.summaryFilters, function ( summaryData) {
                        res.jsonp(summaryData);
                    });
                }

            });

        }

    });

};

