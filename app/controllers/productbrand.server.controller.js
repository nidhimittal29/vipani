'use strict';

/**
 * Module dependencies.
 */
var mongoose = require('mongoose'),
    errorHandler = require('./errors.server.controller'),
    ProductBrand = mongoose.model('ProductBrand'),
    Category = mongoose.model('Category'),
    Hsncodes=mongoose.model('Hsncodes'),
    usersJWTUtil = require('./utils/users.jwtutil'),
    logger = require('../../lib/log').getLogger('PRODUCT', 'DEBUG'),
    dbUtil = require('./utils/common.db.util'),
    _ = require('lodash');

/**
 * Create a Product Brand with product Update
 */
exports.createProductBrand = function (req, res) {
    var productBrand = new ProductBrand(req.body);
    var token = req.body.token || req.headers.token;
    usersJWTUtil.findUserByToken(token, function (err, user) {
        if (err) {
            return res.status(400).send({
                message: errorHandler.getErrorMessage(err)
            });
        }
        productBrand.user = user;
        if(productBrand && !productBrand.name){
            logger.error('No Product Name specification at Product Brand'+JSON.stringify(productBrand));
            return res.status(400).send({
                status:false,
                message: 'No Product Brand Name'
            });

        }else if(!productBrand.productCategory){
            logger.error('No Product category specification at Product Brand'+JSON.stringify(productBrand));
            return res.status(400).send({
                status:false,
                message: 'No Product category specification at Product Brand'
            });
        }else {
            productBrand.save(function(error, doc, next){
                if (error) {
                    logger.error('Failed to save the product Brand '+errorHandler.getErrorMessage(err));
                    return res.status(400).send({
                        message: errorHandler.getErrorMessage(err)
                    });
                } else {
                    res.jsonp(productBrand);
                }
            });
        }


    });
};
/*
Create Product Brand
 */

exports.create = function (req, res) {
    var productBrand = new ProductBrand(req.body);
    var token = req.body.token || req.headers.token;
    usersJWTUtil.findUserByToken(token, function (err, user) {
        if (err) {
            return res.status(400).send({
                message: errorHandler.getErrorMessage(err)
            });
        }
        productBrand.user = user;
        productBrand.save(function (err) {
            if (err) {
                return res.status(400).send({
                    message: errorHandler.getErrorMessage(err)
                });
            } else {
                res.jsonp(productBrand);
            }
        });


    });
};
/**
 * Show the current Product Brand
 */
exports.read = function (req, res) {
    res.jsonp(req.productBrand);
};

/**
 * Update a Product Brand
 */
exports.update = function (req, res) {
    var productBrand = req.productBrand;
    var versionKey = productBrand.productBrandVersionKey;

    productBrand = _.extend(productBrand, req.body);
    productBrand.inventoriesVersionKey = versionKey;
    productBrand.set('lastUpdated', Date.now());

    productBrand.save(function (err) {
        if (err) {
            return res.status(400).send({
                message: errorHandler.getErrorMessage(err)
            });
        } else {
            res.jsonp(productBrand);
        }
    });
};

/**
 * Delete an Category
 */
exports.delete = function (req, res) {
    var productBrand = req.productBrand;

    productBrand.remove(function (err) {
        if (err) {
            return res.status(400).send({
                message: errorHandler.getErrorMessage(err)
            });
        } else {
            res.jsonp(productBrand);
        }
    });
};


/**
 * List of Brand Table
 */
exports.list = function (req, res) {
    ProductBrand.find().sort('-created').populate('user', 'displayName').populate('hsncode', 'name code  description CGST SGST IGST').exec(function (err, categories) {
        if (err) {
            return res.status(400).send({
                message: errorHandler.getErrorMessage(err)
            });
        } else {

            res.jsonp(categories);
        }
    });
};


/**
 * product Brand middleware
 */
exports.brandById = function (req, res, next, id) {
    ProductBrand.findById(id).populate('user', 'displayName').populate('hsncode productCategory taxGroup unitOfMeasures').exec(function (err, productBrand) {
        if (err) return next(err);
        if (!productBrand) return next(new Error('Failed to load product Brand ' + id));
        req.productBrand = productBrand;
        next();
    });
};

/**
 * product Brand authorization middleware
 */
exports.hasAuthorization = function (req, res, next) {
    var token = req.body.token || req.headers.token;
    usersJWTUtil.findUserByToken(token, function (err, user) {
        if (err) {
            return res.status(400).send({
                message: errorHandler.getErrorMessage(err)
            });
        } else {
            if (req.productBrand.user && req.productBrand.user._id.toString() !== user._id) {
                return res.status(403).send('User is not authorized');
            } else {
                next();
            }
        }
    });

};
