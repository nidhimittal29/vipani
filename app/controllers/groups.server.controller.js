'use strict';

/**
 * Module dependencies.
 */
var mongoose = require('mongoose'),
    errorHandler = require('./errors.server.controller'),
    usersJWTUtil   = require('./utils/users.jwtutil'),
    Group = mongoose.model('Group'),
    Contact = mongoose.model('Contact'),
    config = require('../../config/config'),
    async = require('async'),
    contactUtil = require('./utils/common.contacts.util'),
    logger = require('../../lib/log').getLogger('GROUP', 'DEBUG'),
    _ = require('lodash');

/**
 * Create a Group
 */
exports.create = function(req, res) {
    /*var reqBody = req.body;
    if(reqBody.groupClassification && reqBody.groupClassification.classificationType==='All'){
        reqBody.groupClassification.classificationType='Criteria';
    }*/
    var group = new Group(req.body);
    var token = req.body.token || req.headers.token;
    usersJWTUtil.findUserByToken(token, function(err, user) {
        if (err) {
            logger.error('Error while getting user for create contact with token' + token + 'Error:' + err);
            return res.status(400).send({
                message: errorHandler.getErrorMessage(err)
            });
        }else if(!user){
            logger.error('User object was not found for create group with token' + token);
            return res.status(400).send({
                message: 'No user to create group'
            });
        } else {
            if (!group.name) {
                logger.error('Group name not sent from client to create group' + group);
                return res.status(400).send({
                    message: 'Name is required for create group'
                });
            } else if (typeof (group.name) !== 'string') {
                logger.error('Name not sent as string from client to create group' + group);
                return res.status(400).send({
                    message: 'Name is required as string for create group'
                });
            } else if (group.groupClassification && group.groupClassification.classificationType === 'Criteria' && group.groupClassification.classificationCriteria.criteriaType ==='Custom' && (!(group.groupClassification.classificationCriteria.location) || group.groupClassification.classificationCriteria.location.length === 0)) {
                logger.error('Location must be set while creating if group is a criteria group' + group);
                return res.status(400).send({
                    message: 'Location must be set while creating if group is a criteria group'
                });
            }
            else {
                var date = Date.now();
                group.set('created', date);



                group.set('lastUpdated', date);
                group.updateHistory = [];
                group.updateHistory.push({modifiedOn: date, modifiedBy: user});
                group.user = user;
                group.company = user.company;
                group.save(function (err) {
                    if (err) {
                        logger.error(errorHandler.getErrorMessage(err));
                        return res.status(400).send({
                            message: errorHandler.getErrorMessage(err)
                        });
                    } else {
                        res.jsonp(group);
                    }
                });
            }
        }
    });
};

/**
 * Show the current Group
 */
exports.read = function(req, res) {
    res.jsonp(req.group);
};

/**
 * Update a Group
 */
exports.update = function(req, res) {
    var token = req.body.token || req.headers.token;
    var group = req.group ;
    var versionKey = group.groupsVersionKey;
    group = _.extend(group , req.body);
    group.set('lastUpdated', Date.now());
    usersJWTUtil.findUserByToken(token, function (err, user) {
        if (err) {
            logger.error('Error while getting user for create contact with token' + token + 'Error:' + err);
            return res.status(400).send({
                message: errorHandler.getErrorMessage(err)
            });
        }else if(!user){
            logger.error('User object is null for update group with token' + token);
            return res.status(400).send({
                message: 'No user to update group'
            });
        } else {
            if (!group.name) {
                logger.error('Group name not sent from client to update group' + group);
                return res.status(400).send({
                    message: 'Name is required for update group'
                });
            } else if (typeof (group.name) !== 'string') {
                logger.error('Name not sent as string from client to update group' + group);
                return res.status(400).send({
                    message: 'Name is required as string for update group'
                });
            } else if (group.groupClassification && group.groupClassification.classificationType === 'Criteria' && (!group.groupClassification.classificationCriteria.location || group.groupClassification.classificationCriteria.location.length === 0)) {
                logger.error('Location must be set while updating if group is a criteria group' + group);
                return res.status(400).send({
                    message: 'Location must be set while updating if group is a criteria group'
                });
            }
            else {
                group.updateHistory.push({modifiedOn: Date.now(), modifiedBy: user});
                group.groupsVersionKey = versionKey;
                group.save(function (err) {
                    if (err) {
                        return res.status(400).send({
                            message: errorHandler.getErrorMessage(err)
                        });
                    } else {
                        res.jsonp(group);
                    }
                });
            }
        }
    });
};

/**
 * Delete an Group
 */
exports.delete = function(req, res) {
    var group = req.group ;

    group.remove(function(err) {
        if (err) {
            return res.status(400).send({
                message: errorHandler.getErrorMessage(err)
            });
        } else {
            res.jsonp(group);
        }
    });
};

/**
 * List of Groups
 */
exports.list = function(req, res) {
    var token = req.body.token || req.headers.token;

    var lastSyncTime = req.headers.lastsynctime;
    var mobileAppKey=req.headers.apikey;
    usersJWTUtil.findUserByToken(token, function (err, loginuser) {
        if (loginuser) {
            var query={company: loginuser.company};
            if (lastSyncTime) {
                query.lastUpdated =  {$gt: lastSyncTime};
            }
            var pageOptions = {};
            if (req.query.page && !isNaN(req.query.page)) {

                pageOptions.page = parseInt(req.query.page);
            }
            if (req.query.limit) {
                pageOptions.limit = parseInt(req.query.limit);
            }
            var queryArray=[];
            if(req.query.searchText){
                var regExp={'$regex': new RegExp(req.query.searchText, 'g')};
                for (var property in Group.schema.paths) {
                    if (Group.schema.paths.hasOwnProperty(property)  && Group.schema.paths[property].instance === 'String') {
                        var eachproduct={};
                        eachproduct[property] = regExp;
                        queryArray.push(eachproduct);
                    }

                }

            }
            var finalQuery=null;
            if(queryArray.length>0)
                finalQuery={$and:[query,{ $or : queryArray}]};
            else finalQuery=query;

            contactUtil.findQueryByGroups([finalQuery],0,mobileAppKey===config.bbapikey,pageOptions,function (groupErr,groups) {
                if(groupErr){
                    return res.status(400).send({
                        message: errorHandler.getErrorMessage(groupErr)
                    });
                }else{
                    res.jsonp(groups);
                }

            });
        }
    });
};

/**
 * Get query for location field
 */
function getQuery(classificationCriteria,done){
    var queryArray=[];
    if(classificationCriteria.criteriaType==='Custom') {
        async.forEach(classificationCriteria.location, function (location, callback) {
            if(location.pinCode || location.city) {
                var eachContact = {};
                if (location.pinCode && location.pinCode.length>0) {
                    eachContact['addresses.pinCode'] =  location.pinCode;
                }
                if (location.city && location.city.length>0) {
                    eachContact['addresses.city'] = {'$regex': location.city,'$options':'i'};
                }
                queryArray.push(eachContact);
            }
            callback();
        }, function (err) {
            if (err) {
                done(err, null);
            } else {
                done(null, queryArray);
            }
        });
    }else if (classificationCriteria.criteriaType==='All') {
        done(null, queryArray);
    }else
    {
        done(null, queryArray);
    }

}

/**
 * Get Group related contact pattern.
 */
function getGroupContacts(contacts,done) {
    var queryArray=[];
    async.forEach(contacts, function (contact, callback) {

        queryArray.push({contact:contact});
        callback();
    },function (err) {
        if(err){
            done(err,null);
        }else{
            done(null,queryArray);
        }
    });

}

/**
 * Get Group Contacts
 */
function getGroupContactIds (group,isGroup,done) {
    var contactsIds=[];
    async.forEachSeries(group.contacts,function (contact,callback) {
        if(isGroup) {
            if(contact.contact instanceof Contact) {
                contactsIds.push(contact.contact._id);
            }else{
                logger.error('Error while fetching contact with value: :'+group.name);
            }
        }else{
            contactsIds.push(contact._id);
        }
        callback();
    },function (err) {
        if(err){
            logger.error('Error while fetching contact with value: :'+group.name +' .Error :'+errorHandler.getErrorMessage(err));
            done(err,null);
        }else{
            done(null,contactsIds);
        }
    });
}
/**
 * Get the contacts for Match Criteria
 */
function getContacts(group, req ,done) {

    var query={user: group.user.id,customerType:group.groupType,nVipaniRegContact:false};

    var pageOptions = {};
    if(req.param('page') && !isNaN(req.param('page'))){

        pageOptions.page= req.param('page');
        /*pageOptions.limit=10;*/
    }
    if(req.param('limit')){
        pageOptions.limit = parseInt(req.param('limit'));
    }
    var queryArray=[];

    getQuery(group.groupClassification.classificationCriteria,function (err,queryArray) {
        var finalQuery=null;
        if(queryArray.length>0)
            finalQuery={$and:[query,{ $or : queryArray}]};
        else finalQuery=query;
        contactUtil.findQueryByContacts([finalQuery],-1,false,pageOptions,function (contactErr,contacts) {
            if(contactErr){
                done(contactErr,null);
            }else{
                getGroupContacts(contacts, function (err, finalContacts) {
                    done(null, finalContacts);
                });

            }

        });

    });


}
/**
 * Group middleware
 */
exports.groupByID = function(req, res, next, id) {
    Group.findById(id).populate('user', 'displayName').populate('contacts.contact').exec(function (err, group) {
        if (err) return next(err);
        else if (! group) return next(new Error('Failed to load Group ' + id));

        else if (group.contacts.length ===0 && group.groupClassification && (group.groupClassification.classificationType && group.groupClassification.classificationType === 'Criteria' ||  group.groupClassification.classificationType && group.groupClassification.classificationType === 'All')) {
            getContacts(group, req, function (errGroupClassification, contacts) {
                if (errGroupClassification)
                    return next(new Error('Failed to load Group Classification ' + id));
                else {
                    group.contacts = contacts;
                    req.group = group;
                    next();
                }
            });
        } else {
            req.group = group;
            if(req.query.participant){
                getGroupContactIds(group, true, function (errContactIds, resultGroup) {
                    req.group = resultGroup;
                    next();
                });
            }else {
                next();
            }


        }
    });
};

/**
 * Group authorization middleware
 */
exports.hasAuthorization = function(req, res, next) {
    var token = req.body.token || req.headers.token;
    usersJWTUtil.findUserByToken(token, function(err, user) {
        if (err) {
            return res.status(400).send({
                message: errorHandler.getErrorMessage(err)
            });
        } else {
            if ((req.group.company !== null) && (req.group.company.toString() !== user.company.toString())) {
                return res.status(403).send('User is not authorized');
            } else {
                next();
            }
        }

    });

};

exports.massDelete=function (req,res) {
    var token = req.body.token || req.headers.token;
    var groups=req.body.groups;
    /* var type=req.body.type;*/
    usersJWTUtil.findUserByToken(token, function (err, user) {
        if(err){
            return res.status(400).send({
                message: errorHandler.getErrorMessage(err)
            });
        }else {
            if(req.body.groups && req.body.groups.length>0) {
                var query = {user: user._id};
                if (groups && groups.length > 0) {
                    query.$or = groups;
                }
                contactUtil.batchGroupUpdate(query, {'deleted': true}, function (groupErr, results) {
                    if (groupErr) {
                        return res.status(400).send({
                            message: errorHandler.getErrorMessage(groupErr)
                        });
                    } else if (!results) {
                        return res.status(400).send({
                            message: 'Not Deleted',
                            status: false
                        });
                    } else {
                        contactUtil.findQueryByGroups([{user: user._id}], 0, false, {
                            page: 0,
                            limit: 10
                        }, function (groupFetchErr, groups) {
                            if (groupFetchErr) {
                                return res.status(400).send({
                                    message: errorHandler.getErrorMessage(groupFetchErr)
                                });
                            } else {
                                res.jsonp(groups);
                            }

                        });
                    }

                });
            }else {
                return res.status(400).send({
                    message: 'No Selected Groups'
                });
            }
        }
    });
};
exports.massDisable=function (req,res) {
    var token = req.body.token || req.headers.token;
    var groups=req.body.groups;
    /* var type=req.body.type;*/
    usersJWTUtil.findUserByToken(token, function (err, user) {
        if(err){
            return res.status(400).send({
                message: errorHandler.getErrorMessage(err)
            });
        }else {
            if(req.body.groups && req.body.groups.length>0) {
                var query = {'$or': groups, user: user._id};
                contactUtil.batchGroupUpdate(query, {'disabled': true}, function (groupErr, results) {
                    if (groupErr) {
                        return res.status(400).send({
                            message: errorHandler.getErrorMessage(groupErr)
                        });
                    } else if (!results) {
                        return res.status(400).send({
                            message: 'Not Disabled',
                            status: false
                        });
                    } else {
                        contactUtil.findQueryByGroups([query], -1, false, {
                            page: 0,
                            limit: 10
                        }, function (groupFetchErr, groups) {
                            if (groupFetchErr) {
                                return res.status(400).send({
                                    message: errorHandler.getErrorMessage(groupFetchErr)
                                });
                            } else {
                                res.jsonp(groups);
                            }

                        });
                    }

                });
            }else {
                return res.status(400).send({
                    message: 'No Selected Groups'
                });
            }
        }
    });
};
exports.massEnable=function (req,res) {
    var token = req.body.token || req.headers.token;
    var groups=req.body.groups;
    /* var type=req.body.type;*/
    usersJWTUtil.findUserByToken(token, function (err, user) {
        if(err){
            return res.status(400).send({
                message: errorHandler.getErrorMessage(err)
            });
        }else {
            if(req.body.groups && req.body.groups.length>0) {
                var query = {'$or': groups, user: user._id};
                contactUtil.batchGroupUpdate(query, {'disabled': false}, function (groupErr, results) {
                    if (groupErr) {
                        return res.status(400).send({
                            message: errorHandler.getErrorMessage(groupErr)
                        });
                    } else if (!results) {
                        return res.status(400).send({
                            message: 'Not Disabled',
                            status: false
                        });
                    } else {
                        contactUtil.findQueryByGroups([query], -1, false, {
                            page: 0,
                            limit: 10
                        }, function (groupFetchErr, groups) {
                            if (groupFetchErr) {
                                return res.status(400).send({
                                    message: errorHandler.getErrorMessage(groupFetchErr)
                                });
                            } else {
                                res.jsonp(groups);
                            }
                        });
                    }

                });
            }else{
                return res.status(400).send({
                    message: 'No Selected Groups'
                });
            }
        }
    });
};

