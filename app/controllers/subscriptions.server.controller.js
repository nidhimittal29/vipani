'use strict';

/**
 * Module dependencies.
 */
var mongoose = require('mongoose'),
    errorHandler = require('./errors.server.controller'),
    usersJWTUtil = require('./utils/users.jwtutil'),
    logger = require('../../lib/log').getLogger('SUBSCRIPTIONS', 'DEBUG'),
    User = mongoose.model('User'),
    Subscription = mongoose.model('Subscription'),
    _ = require('lodash');

/**
 * Create a Subscription
 */

exports.create = function (req, res) {
    var token = req.body.token || req.headers.token;
    var subscription = new Subscription(req.body);

    logger.debug('Creating Subscription -' + JSON.stringify(subscription));

    var date = Date.now();
    subscription.set('created', date);
    subscription.set('lastUpdated', date);

    usersJWTUtil.findUserByToken(token, function (err, user) {
        if (err) {
            //logger.debug('err 1 -'+err);
            //logger.debug('err 1 -'+JSON.stringify(err));
            return res.status(400).send({
                message: errorHandler.getErrorMessage(err)
            });
        }
        subscription.user = user;
        subscription.lastUpdatedUser = user;

        subscription.save(function (saveSubscriptionErr) {
            if (saveSubscriptionErr) {
                logger.error('Error while creating Subscription.', saveSubscriptionErr);
                return res.status(400).send({
                    message: errorHandler.getErrorMessage(saveSubscriptionErr)
                });
            } else {
                res.jsonp(subscription);
            }
        });
    });
};

/**
 * Show the current Subscription
 */
exports.read = function (req, res) {
    res.jsonp(req.subscription);
};

/**
 * Update a Subscription
 */
exports.update = function (req, res) {
    var subscription = req.subscription;
    var token = req.body.token || req.headers.token;

    var versionKey = subscription.subscriptionVersionKey;

    subscription = _.extend(subscription, req.body);

    subscription.subscriptionVersionKey = versionKey;

    subscription.set('lastUpdated', Date.now());

    usersJWTUtil.findUserByToken(token, function (err, user) {
        if (err) {
            //logger.debug('err 1 -'+err);
            //logger.debug('err 1 -'+JSON.stringify(err));
            return res.status(400).send({
                message: errorHandler.getErrorMessage(err)
            });
        }
        subscription.lastUpdatedUser = user;

        subscription.save(function (saveSubscriptionErr) {
            if (saveSubscriptionErr) {
                logger.error('Error while creating Contact.', saveSubscriptionErr);
                return res.status(400).send({
                    message: errorHandler.getErrorMessage(saveSubscriptionErr)
                });
            } else {
                res.jsonp(subscription);
            }
        });
    });
};


/**
 * Delete an Subscription
 */

exports.delete = function (req, res) {
    var subscription = req.subscription;

    var token = req.body.token || req.headers.token;

    var versionKey = subscription.subscriptionVersionKey;

    subscription = _.extend(subscription, req.body);

    subscription.subscriptionVersionKey = versionKey;

    subscription.deleted = true;
    subscription.set('lastUpdated', Date.now());

    usersJWTUtil.findUserByToken(token, function (err, user) {
        if (err) {
            //logger.debug('err 1 -'+err);
            //logger.debug('err 1 -'+JSON.stringify(err));
            return res.status(400).send({
                message: errorHandler.getErrorMessage(err)
            });
        }
        subscription.lastUpdatedUser = user;

        subscription.save(function (saveSubscriptionErr) {
            if (saveSubscriptionErr) {
                logger.error('Error while creating Contact.', saveSubscriptionErr);
                return res.status(400).send({
                    message: errorHandler.getErrorMessage(saveSubscriptionErr)
                });
            } else {
                res.jsonp(subscription);
            }
        });
    });
};

/**
 * List of Subscriptions
 */
exports.list = function (req, res) {
    var token = req.body.token || req.headers.token;
    if (token) {
        usersJWTUtil.getUserByToken(token, function (err, loginuser) {
            if (loginuser) {
                var query;
                query = Subscription.find({$or: [{$and: [{deleted: false}, {user: loginuser.id}]}, {$and: [{deleted: false}, {user: null}]}]});
                query.sort('-created').populate('user', 'displayName').exec(function (err, subscriptions) {
                    if (err) {
                        return res.status(400).send({
                            message: errorHandler.getErrorMessage(err)
                        });
                    } else {
                        res.jsonp(subscriptions);
                    }
                });
            }
        });
    } else {
        var query;
        query = Subscription.find({$and: [{deleted: false}, {user: null}]});
        query.sort('-created').populate('user', 'displayName').exec(function (err, subscriptions) {
            if (err) {
                return res.status(400).send({
                    message: errorHandler.getErrorMessage(err)
                });
            } else {
                res.jsonp(subscriptions);
            }
        });
    }

};

/**
 * Contact middleware
 */
exports.subscriptionByID = function (req, res, next, id) {
    Subscription.findById(id).populate('user', 'displayName').exec(function (err, subscription) {
        if (err) return next(err);
        if (!subscription) return next(new Error('Failed to load Subscription ' + id));
        req.subscription = subscription;
        next();
    });
};

/**
 * Subscription authorization middleware
 */
exports.hasAuthorization = function (req, res, next) {
    var token = req.body.token || req.headers.token;
    usersJWTUtil.getUserByToken(token, function (err, user) {
        if (err) {
            if (err) {
                return res.status(400).send({
                    message: errorHandler.getErrorMessage(err)
                });
            }
        } else {
            if ((req.subscription.user !== null) && (req.subscription.user.id.toString() !== user.id)) {
                return res.status(403).send('User is not authorized');
            } else {
                next();
            }
        }
    });
};

