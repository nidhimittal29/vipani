'use strict';

/**
 * Module dependencies.
 */
var mongoose = require('mongoose'),
    errorHandler = require('./errors.server.controller'),
    usersJWTUtil = require('./utils/users.jwtutil'),
    Notification = mongoose.model('Notification'),
    _ = require('lodash');

/**
 * Create a Notification
 */
exports.create = function (req, res) {
    var notification = new Notification(req.body);
    var token = req.body.token || req.headers.token;
    usersJWTUtil.findUserByToken(token, function (err, user) {
        if (err) {
            return res.status(400).send({
                message: errorHandler.getErrorMessage(err)
            });
        }
        notification.user = user;

        notification.save(function (saveErr) {
            if (saveErr) {
                return res.status(400).send({
                    message: errorHandler.getErrorMessage(saveErr)
                });
            } else {
                res.jsonp(notification);
            }
        });
    });
};

/**
 * Show the current Notification
 */
exports.read = function (req, res) {
    res.jsonp(req.notification);
};

/**
 * Update a Notification
 */
exports.update = function (req, res) {
    var notification = req.notification;

    notification = _.extend(notification, req.body);
    notification.set('lastUpdated', Date.now());
    var token = req.body.token || req.headers.token;
    usersJWTUtil.findUserByToken(token, function (err, user) {
        if (err) {
            return res.status(400).send({
                message: errorHandler.getErrorMessage(err)
            });
        }

        notification.save(function (saveErr) {
            if (saveErr) {
                return res.status(400).send({
                    message: errorHandler.getErrorMessage(err)
                });
            } else {
                res.jsonp(notification);
            }
        });
    });
};

/**
 * Delete an Notification
 */
exports.delete = function (req, res) {
    var notification = req.notification;

    notification.remove(function (err) {
        if (err) {
            return res.status(400).send({
                message: errorHandler.getErrorMessage(err)
            });
        } else {
            res.jsonp(notification);
        }
    });
};

/**
 * List of Notifications
 */
exports.list = function (req, res) {
    var token = req.body.token || req.headers.token;
    var lastSyncTime = req.headers.lastsynctime;
    usersJWTUtil.findUserByToken(token, function(err, loginuser) {
        if (!err) {
            var query;
            if(lastSyncTime){
                query = Notification.find({'target.nViapniCompany': loginuser.company.toString(), created: {$gt: lastSyncTime},disabled:false,viewed:false});
            }else{
                query=Notification.find({'target.nViapniCompany': loginuser.company.toString(),disabled:false,viewed:false});
            }
            query.sort('-created').populate('user', 'displayName').exec(function (err, notifications) {
                if (err) {
                    return res.status(400).send({
                        message: errorHandler.getErrorMessage(err)
                    });
                } else {
                    res.jsonp(notifications);
                }
            });
        }
    });
};

/**
 *  clear Notifications
 */
exports.clearNotify = function (req, res) {
    var token = req.body.token || req.headers.token;
    var notificationType = req.query.type;
    var notificationId = req.query.notificationId;
    usersJWTUtil.getUserByToken(token, function(err, loginuser) {
        if (!err) {
          /*  var query;
            if(lastSyncTime){
                query = Notification.find({'target.nVipaniUser': loginuser.id, created: {$gt: lastSyncTime},disabled:false});
            }else{
                query=Notification.find({'target.nVipaniUser': loginuser.id});
            }
            query.sort('-created').populate('user', 'displayName').exec(function (err, notifications) {
                if (err) {
                    return res.status(400).send({
                        message: errorHandler.getErrorMessage(err)
                    });
                } else {
                    res.jsonp(notifications);
                }
            });*/
          var parms={'target.nVipaniUser':loginuser.id,viewed: false};
          if(notificationType){
              parms.type=  {$regex : '^' + notificationType};
          }
            if(notificationId){
                parms._id=  notificationId;
            }
            Notification.update(parms, {
                $set: {viewed: true }
            }, {
                multi: true
            }, function (err,result) {
               if(!err){
                   Notification.find({'target.nVipaniUser': loginuser.id,disabled:false,viewed:false}).sort('-created').populate('user', 'displayName').exec(function (err, notifications) {
                       res.jsonp(notifications);
                   });
               }

            });
        }
    });
};
/**
 * Notification middleware
 */
exports.notificationByID = function (req, res, next, id) {
    Notification.findById(id).populate('user', 'displayName').exec(function (err, notification) {
        if (err) return next(err);
        if (!notification) return next(new Error('Failed to load Notification ' + id));
        req.notification = notification;
        next();
    });
};

/**
 * Notification authorization middleware
 */
exports.hasAuthorization = function (req, res, next) {
    var token = req.body.token || req.headers.token;
    usersJWTUtil.findUserByToken(token, function (err, user) {
        if (err) {
            return res.status(400).send({
                message: errorHandler.getErrorMessage(err)
            });
        } else {
            if (req.notification.user && req.notification.user._id !== user._id) {
                return res.status(403).send('User is not authorized');
            } else {
                next();
            }
        }
    });

};
