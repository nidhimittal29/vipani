'use strict';

/**
 * Module dependencies.
 */
var mongoose = require('mongoose'),
    errorHandler = require('./errors.server.controller'),
    usersJWTUtil = require('./utils/users.jwtutil'),
    dbUtil = require('./utils/common.db.util'),
    logger = require('../../lib/log').getLogger('IMPORTFILE', 'DEBUG'),
    ImportFile = mongoose.model('ImportFile'),
    Category = mongoose.model('Category'),
    Hsncodes = mongoose.model('Hsncodes'),
    TaxGroup = mongoose.model('TaxGroup'),
    UnitOfMeasure = mongoose.model('UnitOfMeasure'),
    ProductBrand=mongoose.model('ProductBrand'),
    ItemMaster = mongoose.model('ItemMaster'),
    User = mongoose.model('User'),
    Contact = mongoose.model('Contact'),
    async = require('async'),
    fs = require('fs'),
    _ = require('lodash');

/**
 * Create a ImportFile
 */

/**
 * This function imports the item masters from the csv or excel file.
 * Step 1) Check the subCategory1 exists or not. If not error in importing the row. Otherwise go to - Step 1.1
 *      Step 1.1) Check the parent category of subCategory1 exists or not, if not error in importing the row. Otherwise go to Step 2
 * Step 2) Check for the product category exists or not by checking the product category code. If it doesn't exist create it and go to Step 3.
 * Step 3) For the product category check whether product brand exists or not. If it doesn't exist then create it and go to Step 4
 * Step 4) For the product brand check whether UOM exists or not. If it doesn't exist then create it and return. Repeat the Steps from 1 to 4 for all the rows of the data.
 *
 * @param importFile
 * @param user
 * @param done
 */


function insertImportFileData(importFile, user, done) {
    //Category	SubCategory1	ProductName	ProductCode	ProductAlias.1	ProductSuperClassification.1	ProductDescription
    // Check Category and SubCategory1
    if (importFile.header && importFile.header.length > 0 && importFile.data && importFile.data.length > 0) {
        var result = {itemmasters: [], linedata: [], errorLineData: [], status: true};
        async.forEachSeries(importFile.data, function (eachLineData, callback) {

            logger.debug('Line Src - ' + eachLineData.srcLine + ', Line Data - ' + JSON.stringify(eachLineData.lineData));
            var errorMessage = '';
            var subCategory1Name = eachLineData.lineData[importFile.header.indexOf('SubCategory1')];
            var categoryName = eachLineData.lineData[importFile.header.indexOf('Category')];
            var query = [{name: subCategory1Name, type: 'SubCategory1'}];
            dbUtil.findQueryByCategories(query, 1, function (categoriesErr, categories) {
                if (categoriesErr) {
                    logger.error(errorHandler.getErrorMessage(categoriesErr));
                    result.errorLineData.push({
                        data: eachLineData,
                        errorMessage: errorHandler.getErrorMessage(categoriesErr)
                    });
                    result.status = false;
                    callback();
                    //callback(categoriesErr);
                    /* if (errorMessage === '') {
                         errorMessage = errorHandler.getErrorMessage(categoriesErr);
                     } else {
                         errorMessage = errorMessage + '\n' + errorHandler.getErrorMessage(categoriesErr);
                     }
                     callback(new Error(errorMessage));*/
                } else if (categories.length > 0) {
                    var subCategory1 = categories[0];
                    if (subCategory1.parent && subCategory1.parent.name && subCategory1.parent.name === categoryName) {
                        dbUtil.findOrCreateProductCategory(subCategory1, importFile.header, eachLineData, user, function (productCategoryErr, productCategory) {
                            if (productCategoryErr) {
                                /*                                if (errorMessage === '') {
                                                                    errorMessage = errorHandler.getErrorMessage(productCategoryErr);
                                                                } else {
                                                                    errorMessage = errorMessage + '\n' + errorHandler.getErrorMessage(productCategoryErr);
                                                                }
                                                                callback(new Error(errorMessage));*/
                                logger.error(errorHandler.getErrorMessage(productCategoryErr));
                                result.errorLineData.push({
                                    data: eachLineData,
                                    errorMessage: errorHandler.getErrorMessage(productCategoryErr)
                                });
                                result.status = false;
                                callback();
                                //callback(productCategoryErr);
                            } else {
                                // Create Product Brand and Item Master
                                dbUtil.findOrCreateProductBrand(productCategory, importFile.header, eachLineData, user, function (productBrandErr, productBrand) {
                                    if (productBrandErr) {
                                        /* if (errorMessage === '') {
                                             errorMessage = errorHandler.getErrorMessage(productBrandErr);
                                         } else {
                                             errorMessage = errorMessage + '\n' + errorHandler.getErrorMessage(productBrandErr);
                                         }
                                         callback(new Error(errorMessage));*/
                                        logger.error(errorHandler.getErrorMessage(productBrandErr));
                                        result.errorLineData.push({
                                            data: eachLineData,
                                            errorMessage: errorHandler.getErrorMessage(productBrandErr)
                                        });
                                        result.status = false;
                                        callback();
                                        //callback(productBrandErr);
                                    } else {
                                       dbUtil.findOrCreateItemMaster(productBrand, importFile.header, eachLineData, user, function (itemMasterErr, itemMaster,warnings) {
                                            if (itemMasterErr) {
                                                /*if (errorMessage === '') {
                                                    errorMessage = errorHandler.getErrorMessage(itemMasterErr);
                                                } else {
                                                    errorMessage = errorMessage + '\n' + errorHandler.getErrorMessage(itemMasterErr);
                                                }
                                                callback(new Error(errorMessage));*/
                                                logger.error(errorHandler.getErrorMessage(itemMasterErr));
                                                result.errorLineData.push({
                                                    data: eachLineData,
                                                    errorMessage: errorHandler.getErrorMessage(itemMasterErr)
                                                });
                                                result.status = false;
                                                callback();
                                                //callback(itemMasterErr);
                                            } else {
                                                result.itemmasters.push(itemMaster);
                                                result.linedata.push(eachLineData);
                                                if(warnings && warnings.length>0) {
                                                    warnings.forEach(function(warning){
                                                        result.errorLineData.push({
                                                            data: eachLineData,
                                                            errorMessage: warning.message
                                                        });
                                                    });
                                                }
                                                callback();
                                            }

                                        });
                                    }
                                });

                            }
                        });

                    } else {
                        if (errorMessage === '') {
                            errorMessage = 'There is no category with name -' + categoryName;
                        } else {
                            errorMessage = errorMessage + '\n' + 'There is no category with name -' + categoryName;
                        }
                        logger.error(errorMessage);
                        result.errorLineData.push({data: eachLineData, errorMessage: errorMessage});
                        result.status = false;
                        callback();
                    }
                } else {
                    if (errorMessage === '') {
                        errorMessage = 'There is no subCategorya with name -' + subCategory1Name;
                    } else {
                        errorMessage = errorMessage + '\n' + 'There is no subCategorya with name -' + subCategory1Name;
                    }
                    logger.error(errorMessage);
                    result.errorLineData.push({data: eachLineData, errorMessage: errorMessage});
                    result.status = false;
                    callback();
                }
            });
        }, function (err) {
            if (err) {
                logger.error(errorHandler.getErrorMessage(err));
                done(err);
            } else {
                done(null, result);
            }
        });
    }else {
        done(new Error('No Proper Data'),null);
    }

    // BrandName	BrandVariety	BrandDescription	BrandOwner.Name	BrandOwner.Phone.1	BrandOwner.Phone.2	BrandOwner.Email	BrandOwner.AddressLine	BrandOwner.City	BrandOwner.State	BrandOwner.Country	BrandOwner.PinCode	Manufacturer.1.Company	Manufacturer.1.Name	Manufacturer.1.Phone.1	Manufacturer.1.Phone.2	Manufacturer.1.Email
    // Manufacturer.1.AddressLine	Manufacturer.1.City	Manufacturer.1.State	Manufacturer.1.Country	Manufacturer.1.PinCode
    // HSNCode	TaxGroup	UOM.FirstUOM	UOM.Conversion	UOM.SecondUOM
    // ProductBrandImage1	ProductBrandImage2	ProductBrandImage3	ProductBrandImage4
    // Grade.Colour	Grade.Kind	Grade.Style	Grade.Variety	Grade.CultivationType	Grade.Texture	Grade.Broken	Grade.Moisture	Grade.ForeignMatters	Quality.Quality	Quality.Aroma
    // LeastSalableQuantity	MRP	DealerPrice	IsBillable	IsInventoryItem	IsRegularItem	IsConsignmentItem	IsStockCheckRequired	ReOrderLebel	LastPurchasePrice	CurrentCost	MoqAndMargin.1.MOQ	MoqAndMargin.1.Margin	MoqAndMargin.2.MOQ	MoqAndMargin.2.Margin	MovAndMargin.1.MOV	MovAndMargin.1.Margin	MovAndMargin.2.MOV	MovAndMargin.2.Margin
    // SKU	Barcode	QRCode	QRImage	ItemMasterImage1	ItemMasterImage2	ItemMasterImage3	ItemMasterImage4


}

exports.create = function (req, res) {
    var importValue = {
        product: 'InventoryItems',
        invoice: 'Invoices',
        contact: 'Contacts',
        payments: 'PaymentsAndReceipts'
    };
    var token = req.body.token || req.headers.token;
    var importType = importValue[req.body.importType];

    var importFile = new ImportFile(req.body);
    importFile.importType = importType;

    logger.debug('Creating ImportFile name -' + JSON.stringify(importFile.fileName) + ', type -' + JSON.stringify(importFile.importType));

    var date = Date.now();
    importFile.set('created', date);
    importFile.set('lastUpdated', date);

    usersJWTUtil.findUserByToken(token, function (err, user) {
        if (err) {
            //logger.debug('err 1 -'+err);
            //logger.debug('err 1 -'+JSON.stringify(err));
            return res.status(400).send({
                message: errorHandler.getErrorMessage(err)
            });
        }
        importFile.user = user;
        importFile.lastUpdatedUser = user;
        insertImportFileData(importFile, user, function (importError, result) {
            if (importError) {
                return res.status(400).send({
                    message: errorHandler.getErrorMessage(importError)
                });
            } else {
                importFile.save(function (saveImportFileErr,resultImortFile) {
                    if (saveImportFileErr) {
                        logger.error('Error while creating ImportFile.', saveImportFileErr);
                        return res.status(400).send({
                            message: errorHandler.getErrorMessage(saveImportFileErr)
                        });
                    } else {
                        res.jsonp(resultImortFile);
                    }
                });
            }
        });

    });
};





/**
 * Show the current ImportFile
 */
exports.read = function (req, res) {
    res.jsonp(req.importFile);
};

/**
 * Update a ImportFile
 */
exports.update = function (req, res) {
    var importFile = req.importFile;
    var token = req.body.token || req.headers.token;

    var versionKey = importFile.importFileVersionKey;

    importFile = _.extend(importFile, req.body);

    importFile.importFileVersionKey = versionKey;

    importFile.set('lastUpdated', Date.now());

    usersJWTUtil.findUserByToken(token, function (err, user) {
        if (err) {
            //logger.debug('err 1 -'+err);
            //logger.debug('err 1 -'+JSON.stringify(err));
            return res.status(400).send({
                message: errorHandler.getErrorMessage(err)
            });
        }
        importFile.lastUpdatedUser = user;

        importFile.save(function (saveImportFileErr) {
            if (saveImportFileErr) {
                logger.error('Error while creating ImportFile.', saveImportFileErr);
                return res.status(400).send({
                    message: errorHandler.getErrorMessage(saveImportFileErr)
                });
            } else {
                res.jsonp(importFile);
            }
        });
    });
};


/**
 * Delete an ImportFile
 */

exports.delete = function (req, res) {
    var importFile = req.importFile;

    var token = req.body.token || req.headers.token;

    var versionKey = importFile.importFileVersionKey;

    importFile = _.extend(importFile, req.body);

    importFile.importFileVersionKey = versionKey;

    importFile.deleted = true;
    importFile.set('lastUpdated', Date.now());

    usersJWTUtil.findUserByToken(token, function (err, user) {
        if (err) {
            //logger.debug('err 1 -'+err);
            //logger.debug('err 1 -'+JSON.stringify(err));
            return res.status(400).send({
                message: errorHandler.getErrorMessage(err)
            });
        }
        importFile.lastUpdatedUser = user;

        importFile.save(function (saveImportFileErr) {
            if (saveImportFileErr) {
                logger.error('Error while creating ImportFile.', saveImportFileErr);
                return res.status(400).send({
                    message: errorHandler.getErrorMessage(saveImportFileErr)
                });
            } else {
                res.jsonp(importFile);
            }
        });
    });
};
/**
 * List of ImportFiles
 */
exports.list = function (req, res) {
    var token = req.body.token || req.headers.token;
    if (token) {
        usersJWTUtil.getUserByToken(token, function (err, loginuser) {
            if (loginuser) {
                var query;
                query = ImportFile.find({$or: [{$and: [{deleted: {$exists: false}}, {user: loginuser.id}]}, {$and: [{deleted: {$exists: false}}, {user: null}]}]});
                query.sort('-created').populate('user', 'displayName').exec(function (err, importFiles) {
                    if (err) {
                        return res.status(400).send({
                            message: errorHandler.getErrorMessage(err)
                        });
                    } else {
                        res.jsonp(importFiles);
                    }
                });
            }
        });
    } else {
        var query;
        query = ImportFile.find({$and: [{deleted: {$exists: false}}, {user: null}]});
        query.sort('-created').exec(function (err, importFiles) {
            if (err) {
                return res.status(400).send({
                    message: errorHandler.getErrorMessage(err)
                });
            } else {
                res.jsonp(importFiles);
            }
        });
    }

};

/**
 * Contact middleware
 */
exports.importFileByID = function (req, res, next, id) {
    ImportFile.findById(id).populate('user', 'displayName').exec(function (err, importFile) {
        if (err) return next(err);
        if (!importFile) return next(new Error('Failed to load ImportFile ' + id));
        req.importFile = importFile;
        next();
    });
};

/**
 * ImportFile authorization middleware
 */
exports.hasAuthorization = function (req, res, next) {
    var token = req.body.token || req.headers.token;
    usersJWTUtil.getUserByToken(token, function (err, user) {
        if (err) {
            if (err) {
                return res.status(400).send({
                    message: errorHandler.getErrorMessage(err)
                });
            }
        } else {
            if ((req.importFile.user !== null) && (req.importFile.user.id.toString() !== user.id)) {
                return res.status(403).send('User is not authorized');
            } else {
                next();
            }
        }
    });
};

