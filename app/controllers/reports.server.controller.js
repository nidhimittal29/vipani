'use strict';

/**
 * Module dependencies.
 */
var mongoose = require('mongoose'),
    mime = require('mime-types'),
    errorHandler = require('./errors.server.controller'),
    Report = mongoose.model('Report'),
    _ = require('lodash'),
    usersJWTUtil = require('./utils/users.jwtutil'),
    Product = mongoose.model('Product'),
    Company = mongoose.model('Company'),
    Inventory=mongoose.model('Inventory'),
    Order = mongoose.model('Order'),
    config = require('../../config/config'),
    reportsUtil = require('./utils/reports.util'),
    fs = require('fs'),
    path=require('path'),
    logger = require('../../lib/log').getLogger('REPORT', 'DEBUG');
/**
 * Create a Report
 */
exports.create = function (req, res) {
    var token = req.body.token || req.headers.token;
    var mobileAppKey= req.query.apikey;
    /* logger.debug('req.todo create -' + req.body);*/
    usersJWTUtil.findUserByToken(token, function (err, user) {
        if (err) {
            return res.status(400).send({
                message: errorHandler.getErrorMessage(err)
            });
        }
        var format=req.body.reportFormat;
        var report = new Report(req.body);
        report.reports = [];
        report.reports.push({format: format});

        //report.reportFormat = format;
        report.user = user;

        Company.findOne({_id: user.company}, '-settings -updateHistory -companiesVersionKey -bankAccountDetailsProof -bankAccountDetails -panNumberProof -panNumber -cstNumber -cstNumberProof -vatNumber -vatNumberProof -tinNumber -tinNumberProof').populate('inventories.inventory', '-updateHistory -numberOfUnitsHistory -offerUnitsHistory -buyOfferUnitsHistory').populate('user', 'displayName').exec(function (companyErr, resCompany) {
            if (companyErr) {
                logger.error('Error while loading the Company  details with company id- ' + user.company._id);
                return res.status(400).send({
                    status: false,
                    message: errorHandler.getErrorMessage(companyErr)
                });

            } else {
                report.company = resCompany;
                if (report.reportType === 'Stock') {
                    Company.populate(resCompany, {
                        path: 'inventories.inventory.product',
                        model: Product,
                        select: 'name category subCategory1 subCategory2 productImageURL1 sampleNumber',
                    }, function (nestedErr, populatedResultCompany) {
                        if (nestedErr) {
                            return res.status(400).send({
                                status: false,
                                message: errorHandler.getErrorMessage(nestedErr)
                            });
                        } else {
                            Company.populate(populatedResultCompany, {
                                path: 'inventories.inventory.product.category inventories.inventory.product.subCategory1 inventories.inventory.product.subCategory2',
                                model: 'Category',
                                select: 'name'
                            }, function (nestedErrs, populatedResultsCompany) {
                                if (nestedErrs) {
                                    return res.status(400).send({
                                        status: false,
                                        message: errorHandler.getErrorMessage(nestedErrs)
                                    });
                                } else {
                                    reportsUtil.generateStockReport(populatedResultsCompany, report, resCompany, function (reportGenErr, report) {
                                        if (reportGenErr) {
                                            logger.error('Error while generating the invoice for the order with Order number ', reportGenErr);
                                            return res.status(400).send({
                                                status: false,
                                                message: errorHandler.getErrorMessage(reportGenErr)
                                            });
                                        } else {
                                            logger.error('Error while generating the invoice for the order with Order number ');
                                            report.save(function (err) {
                                                if (err) {
                                                    return res.status(400).send({
                                                        message: errorHandler.getErrorMessage(err)
                                                    });
                                                } else {
                                                    res.jsonp(report);
                                                }
                                            });
                                        }
                                    });

                                }

                            });
                        }
                    });
                } else {

                    if (resCompany && resCompany.user) {
                        // Sales Report
                        Order.find({
                            'seller.nVipaniUser': resCompany.user
                        }).sort('-created').populate('user', 'displayName').populate('products.inventory', '-updateHistory -numberOfUnitsHistory -offerUnitsHistory -buyOfferUnitsHistory').exec(function (err, orders) {
                            if (err) {
                                return res.status(400).send({
                                    status: false,
                                    message: errorHandler.getErrorMessage(err)
                                });
                            } else {
                                /* if (orders.length > 0) {*/
                                Order.populate(orders, {
                                    path: 'products.inventory.product',
                                    model: 'Product',
                                    select: 'name category productImageURL1 unitSize unitMeasure unitPrice numberOfUnits sampleNumber fssaiLicenseNumber description user'
                                }, function (nestedErr, populatedOrders) {
                                    if (nestedErr) {
                                        return res.status(400).send({
                                            status: false,
                                            message: errorHandler.getErrorMessage(nestedErr)
                                        });
                                    } else {
                                        reportsUtil.generateStockReport(populatedOrders, report, resCompany, function (reportGenErr, report) {
                                            if (reportGenErr) {
                                                logger.error('Error while generating the invoice for the order with Order number ', reportGenErr);
                                                return res.status(400).send({
                                                    status: false,
                                                    message: errorHandler.getErrorMessage(reportGenErr)
                                                });
                                            } else {
                                                report.save(function (err) {
                                                    if (err) {
                                                        return res.status(400).send({
                                                            message: errorHandler.getErrorMessage(err)
                                                        });
                                                    } else {

                                                            res.send(report);
                                                    }
                                                });
                                            }
                                        });
                                    }
                                });
                            }

                            /*  }*/
                        });

                    }
                }

            }
        });

    });
};

function fetchReportLocation(reports, format, done) {
    var reportLocation;
    for (var i = 0; i < reports.length; i++) {
        var report = reports[i];

        for (var j = 0; j < report.reports.length; j++) {
            if (report.reports[j].format === format) {
                reportLocation = report.reports[j].location;
                break;
            }
        }

    }
    if (reportLocation) {
        done(null, reportLocation);
    } else {
        done(new Error('No report present with format -' + format));
    }
}

exports.getReportPath=function (req,res) {
    var report=req.report;
    var mobileAppKey= req.query.apikey;
    fs.readFile(report.reports[0].location, function (err, data) {
        if (mobileAppKey === config.bbapikey) {
            res.contentType('application/octet-stream');

        } else {
            res.contentType(mime.lookup(report.reports[0].location));
        }
        res.send(data);
    });
};
function generateStockReport(user, report, done) {
    Company.findOne({_id: user.company}, '-settings -updateHistory -companiesVersionKey -bankAccountDetailsProof -bankAccountDetails -panNumberProof -panNumber -cstNumber -cstNumberProof -vatNumber -vatNumberProof -tinNumber -tinNumberProof').populate('inventories.inventory', '-updateHistory -numberOfUnitsHistory -offerUnitsHistory -buyOfferUnitsHistory').populate('user', 'displayName').exec(function (companyErr, resCompany) {
        if (companyErr) {
            logger.error('Error while loading the Company details with company id- ' + user.company._id);
            done(companyErr);
        } else {
            report.company = resCompany;
            Company.populate(resCompany, {
                path: 'inventories.inventory.product',
                model: Product,
                select: 'name category subCategory1 subCategory2 productImageURL1 sampleNumber',
            }, function (nestedErr, populatedResultCompany) {
                if (nestedErr) {
                    done(nestedErr);
                } else {
                    Company.populate(populatedResultCompany, {
                        path: 'inventories.inventory.product.category inventories.inventory.product.subCategory1 inventories.inventory.product.subCategory2',
                        model: 'Category',
                        select: 'name'
                    }, function (nestedErrs, populatedResultsCompany) {
                        if (nestedErrs) {
                            done(nestedErrs);
                        } else {
                            reportsUtil.generateStockReport(populatedResultsCompany, report, resCompany, function (reportGenErr, report) {
                                if (reportGenErr) {
                                    logger.error('Error while generating the invoice for the order with Order number ', reportGenErr);
                                    done(reportGenErr);
                                } else {
                                    logger.error('Error while generating the invoice for the order with Order number ');
                                    report.save(function (err) {
                                        if (err) {
                                            done(err);
                                        } else {
                                            done(null, report);
                                        }
                                    });
                                }
                            });
                        }
                    });
                }
            });
        }
    });
}

function generateSalesAndPurchaseReport(user, report, done) {
    Company.findOne({_id: user.company}, '-settings -updateHistory -companiesVersionKey -bankAccountDetailsProof -bankAccountDetails -panNumberProof -panNumber -cstNumber -cstNumberProof -vatNumber -vatNumberProof -tinNumber -tinNumberProof').populate('inventories.inventory', '-updateHistory -numberOfUnitsHistory -offerUnitsHistory -buyOfferUnitsHistory').populate('user', 'displayName').exec(function (companyErr, resCompany) {
        if (companyErr) {
            logger.error('Error while loading the Company details with company id- ' + user.company._id);
            done(companyErr);
        } else {
            report.company = resCompany;
            if (resCompany && resCompany.user) {
                // Sales Report
                Order.find({
                    'seller.nVipaniUser': resCompany.user
                }).sort('-created').populate('user', 'displayName').populate('products.inventory', '-updateHistory -numberOfUnitsHistory -offerUnitsHistory -buyOfferUnitsHistory').exec(function (err, orders) {
                    if (err) {
                        done(err);
                    } else {
                        /* if (orders.length > 0) {*/
                        Order.populate(orders, {
                            path: 'products.inventory.product',
                            model: 'Product',
                            select: 'name category productImageURL1 unitSize unitMeasure unitPrice numberOfUnits sampleNumber fssaiLicenseNumber description user'
                        }, function (nestedErr, populatedOrders) {
                            if (nestedErr) {
                                done(nestedErr);
                            } else {
                                reportsUtil.generateStockReport(populatedOrders, report, resCompany, function (reportGenErr, report) {
                                    if (reportGenErr) {
                                        logger.error('Error while generating the invoice for the order with Order number ', reportGenErr);
                                        done(reportGenErr);
                                    } else {
                                        report.save(function (err) {
                                            if (err) {
                                                done(err);
                                            } else {
                                                done(null, report);
                                            }
                                        });
                                    }
                                });
                            }
                        });
                    }
                });
            }
        }
    });
}

function fetchGeneratedReportLocation(reportType, reportPeriod, format, startDate, endDate, user, done) {
    var query = {};
    if (user.company) {
        logger.debug('User Company -' + user.company);
        query.company = user.company;
    }
    if (startDate) {
        logger.debug('Start Date -' + startDate);
        query.startDate = startDate;
    }
    if (endDate) {
        logger.debug('End Date-' + endDate);
        query.endDate = endDate;
    }
    if (reportType) {
        logger.debug('Report Type-' + reportType);
        query.reportType = reportType;
    }

    if (reportPeriod) {
        logger.debug('Report Period-' + reportPeriod);
        query.reportPeriod = reportPeriod;
    }

    Report.find(query).populate('user', 'displayName').exec(function (reportErr, resultReports) {
        if (reportErr) {
            logger.error('Error while querying the reports with query - ' + query);
            done(reportErr);
        } else if (resultReports && resultReports.length > 0) {
            fetchReportLocation(resultReports, format, function (err, reportLocation) {
                if (reportLocation) {
                    done(null, reportLocation);
                } else {
                    var report = new Report();
                    report.company = user.company;
                    report.reportType = reportType;
                    report.reportPeriod = reportPeriod;
                    report.startDate = startDate;
                    report.endDate = endDate;
                    report.reports = [];
                    report.reports.push({format: format});

                    if (report.reportType === 'Stock') {
                        generateStockReport(user, report, function (reportGenErr, generateReport) {
                            if (reportGenErr) {
                                logger.error('No report is generated with query - ' + query, reportGenErr);
                                done(new Error('No report is generated with query - ' + query, reportGenErr));
                            } else {
                                done(null, generateReport.reports[0].location);
                            }
                        });
                    } else {
                        generateSalesAndPurchaseReport(user, report, function (reportGenErr, generateReport) {
                            if (reportGenErr) {
                                logger.error('No report is generated with query - ' + query, reportGenErr);
                                done(new Error('No report is generated with query - ' + query, reportGenErr));
                            } else {
                                done(null, generateReport.reports[0].location);
                            }
                        });
                    }
                }
            });
        } else {
            var report = new Report();
            report.company = user.company;
            report.reportType = reportType;
            report.reportPeriod = reportPeriod;
            report.startDate = startDate;
            report.endDate = endDate;
            report.reports = [];
            report.reports.push({format: format});
            if (report.reportType === 'Stock') {
                generateStockReport(user, report, function (reportGenErr, generateReport) {
                    if (reportGenErr) {
                        logger.error('No report is generated with query - ' + query, reportGenErr);
                        done(new Error('No report is generated with query - ' + query, reportGenErr));
                    } else {
                        done(null, generateReport.reports[0].location);
                    }
                });
            } else {
                generateSalesAndPurchaseReport(user, report, function (reportGenErr, generateReport) {
                    if (reportGenErr) {
                        logger.error('No report is generated with query - ' + query, reportGenErr);
                        done(new Error('No report is generated with query - ' + query, reportGenErr));
                    } else {
                        done(null, generateReport.reports[0].location);
                    }
                });
            }
        }
    });

}
/**
 * Create a Report
 */
exports.generateReport = function (req, res) {
    var token = req.body.token || req.headers.token;

    usersJWTUtil.findUserByToken(token, function (err, user) {
        if (err) {
            return res.status(400).send({
                message: errorHandler.getErrorMessage(err)
            });
        }
        var format = req.body.reportFormat;
        var startDate = req.body.reportStartDate;
        var endDate = req.body.reportEndDate;
        var reportType = req.body.reportType;
        var reportPeriod = req.body.reportPeriod;

        fetchGeneratedReportLocation(reportType, reportPeriod, format, startDate, endDate, user.company, function (reportLocationErr, reportLocation) {
            if (reportLocationErr) {
                logger.error('Error while fetching or generating the report of type - ' + reportType + ', report period - ' + reportPeriod + ', format -' + format + ', start date -' + startDate + ' and end date-' + endDate);
                return res.status(400).send({
                    status: false,
                    message: errorHandler.getErrorMessage(reportLocationErr)
                });
            } else if (reportLocation) {
                // Return the generated report.
                fs.readFile(reportLocation, function (reportReadErr, data) {
                    if (reportReadErr) {
                        logger.error('Error while reading the report file ' + reportLocation, reportReadErr);
                        return res.status(400).send({
                            status: false,
                            message: errorHandler.getErrorMessage(reportReadErr)
                        });
                    } else {
                        if (format === 'PDF') {
                            res.contentType('application/pdf');
                            res.send(data);
                        } else if (format === 'XLS' || format === 'XLSX') {
                            res.contentType('application/application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
                            res.send(data);
                        }
                    }
                });
            }
        });
    });
};

exports.getFiles=function(req, res) {
        var currentDir =  config.fileAccess;
        var query = req.query.path || '';
        if (query) currentDir = path.join(currentDir, query);
        console.log('browsing ', currentDir);
        if(fs.statSync(currentDir).isDirectory()) {
            fs.readdir(currentDir, function (err, files) {
                if (err) {
                    throw err;
                }
                var data = [];
                files
                    .forEach(function (file) {
                        try {
                            //console.log("processingile);
                            var isDirectory = fs.statSync(path.join(currentDir, file)).isDirectory();
                            if (isDirectory) {
                                data.push({Name: file, IsDirectory: true, Path: path.join(query, file)});
                            } else {
                                var ext = path.extname(file);
                                /* if(program.exclude && _.contains(program.exclude, ext)) {
                                     console.log("excluding file ", file);
                                     return;
                                 }*/
                                if (ext)
                                    data.push({
                                        name: file,
                                        Ext: ext,
                                        IsDirectory: false,
                                        type:mime.lookup(file),
                                        Path: path.join(currentDir, file)
                                    });

                            }

                        } catch (e) {
                            console.log(e);
                        }

                    });
                data = _.sortBy(data, function (f) {
                    return f.Name;
                });
                res.json(data);
            });
        }else if(fs.statSync(currentDir).isFile()) {
            var type=mime.lookup(currentDir);
            fs.readFile(currentDir, function (err, data) {
                res.contentType(type);
                res.send(data);
            });
        }
};
/**
 * Show the current Reportlo
 */
exports.read = function (req, res) {
    res.jsonp(req.report);
};

/**
 * Update a Report
 */
exports.update = function (req, res) {
    var report = req.report;

    report = _.extend(report, req.body);
    report.set('lastUpdated', Date.now());
    report.save(function (err) {
        if (err) {
            return res.status(400).send({
                message: errorHandler.getErrorMessage(err)
            });
        } else {
            res.jsonp(report);
        }
    });
};

/**
 * Delete an Report
 */
exports.delete = function (req, res) {
    var report = req.report;

    report.remove(function (err) {
        if (err) {
            return res.status(400).send({
                message: errorHandler.getErrorMessage(err)
            });
        } else {
            res.jsonp(report);
        }
    });
};

/**
 * List of Reports
 */
exports.list = function (req, res) {
    var token = req.body.token || req.headers.token;
    usersJWTUtil.getUserByToken(token, function (err, loginuser) {
        if (err) {
            return res.status(400).send({
                message: errorHandler.getErrorMessage(err)
            });
        }
        Report.find({target: loginuser.id}).sort('-created').populate('user', 'displayName').exec(function (err, reports) {
            if (err) {
                return res.status(400).send({
                    message: errorHandler.getErrorMessage(err)
                });
            } else {
                res.jsonp(reports);
            }
        });
    });
};

/**
 * Report middleware
 */
exports.reportByID = function (req, res, next, id) {
    Report.findById(id).populate('user', 'displayName').exec(function (err, report) {
        if (err) return next(err);
        if (!report) return next(new Error('Failed to load Report ' + id));
        req.report = report;
        next();
    });
};

/**
 * Report authorization middleware
 */

/**
 * Todo authorization middleware
 */
exports.hasAuthorization = function (req, res, next) {
    var token = req.body.token || req.headers.token;
    usersJWTUtil.findUserByToken(token, function (err, user) {
        if (err) {
            return res.status(400).send({
                message: errorHandler.getErrorMessage(err)
            });
        } else {
            if (req.report.user && req.report.user.id !== user.id) {
                return res.status(403).send('User is not authorized');
            } else {
                next();
            }
        }
    });
};
