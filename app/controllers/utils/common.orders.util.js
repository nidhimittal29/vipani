'use strict';

/**
 * Module dependencies.
 */
var mongoose = require('mongoose'),
    async = require('async'),
    commonUserUtil=require('./common.users.util'),
    reportsUtil=require('./reports.util'),
    _this=this,
    Companies=mongoose.model('Company'),
    Order = mongoose.model('Order');

/**
 * This method takes the array of moqVsPrice and quantity and returns the suitable price.
 * @param moqVsPrice
 * @param quantity
 * @returns {number|price}
 */
function getMinQuantityPrice(moqVsPrice,quantity) {
    var suitableMOQ;
    if(moqVsPrice!==undefined && moqVsPrice && moqVsPrice.length>=1) {
        //price = moqVsPrice[0].price;
        for (var i = 0; i < moqVsPrice.length; i++) {
            var moq = moqVsPrice[i].MOQ;
            if (moq <=quantity ) {
                suitableMOQ=  moqVsPrice[i];
            }
        }
    }
    return suitableMOQ;
}
exports.getQuantityPrice = function (moqVsPrice, quantity,orginalPrice) {
    if(moqVsPrice!==undefined && moqVsPrice && moqVsPrice.length>=1) {
        var suitableMOQ;
        var suitableMin =getMinQuantityPrice(moqVsPrice,quantity);
        var next = suitableMin;
        if(suitableMin) {
            if (suitableMin.MOQ === quantity) {
                return orginalPrice * (1 - (suitableMin.margin / 100));
            } else {
                for (var i = 0; i < moqVsPrice.length; i++) {
                    next = moqVsPrice[i];
                    if (suitableMin.MOQ <= next.MOQ && next.MOQ <= quantity) {
                        suitableMin = moqVsPrice[i];
                    }
                }
                return orginalPrice * (1 - (suitableMin.margin / 100));
            }
        }else{
            return orginalPrice;
        }
    }
    return orginalPrice;
};
exports.getMinQuantity=function(product,isBuy) {
    var moqVsPrice;
    if(isBuy)
        moqVsPrice=product.moqAndBuyPrice;
    else
        moqVsPrice=product.moqAndSalePrice;

    var quantity=moqVsPrice.length>0?moqVsPrice[0].MOQ:(Math.max(product.numberOfUnits,product.maxUnits)>0?1:0);
    for (var i = 0; i < moqVsPrice.length; i++) {
        var moq = moqVsPrice[i].MOQ;
        if (moqVsPrice[i].MOQ <=quantity ) {
            quantity=  moqVsPrice[i].MOQ;
        }
    }
    return quantity;
};
exports.findQueryByOrders=function(query,populateLevels,isFilterSummary,pageOptions, done) {
    query.push({deleted:{$exists: true}});
    query.push({deleted:false});
    // query.push({deleted:{$exists: false}});
    if(populateLevels===-2) {
        Order.find(query).exec(function (err, orders) {
            done(err,orders);
        });
    }else if(populateLevels===-1){
        Order.find({$and: query}, '-updateHistory -products -shippingAddress -billingAddress -statusHistory -invoiceAddress -transport -invoice -statusReason').populate('buyer.nVipaniUser buyer.contact buyer.businessUnit seller.contact seller.nVipaniUser seller.businessUnit mediator.nVipaniUser mediator.contact mediator.businessUnit', 'displayName firstName lastName middleName addresses nVipaniUser nVipaniCompany gstinNumber companyName name').populate('offer', 'name offerType offerNumber user').sort('-created').exec(function (err, orders) {
            if (err) {
                done(err,null);
            } else {
                done(null,orders);
            }
        });

    }else if(populateLevels===1) {

        Order.find({$and: query}, '--updateHistory -products -shippingAddress -billingAddress -statusHistory -invoiceAddress -transport -invoice -statusReason').sort('-created').populate('buyer.nVipaniUser buyer.contact buyer.businessUnit seller.contact seller.nVipaniUser seller.businessUnit mediator.nVipaniUser mediator.contact mediator.businessUnit', 'displayName firstName lastName middleName addresses nVipaniUser nVipaniCompany gstinNumber companyName name ').populate('offer', 'name offerType offerNumber user owner').exec(function (err, orders) {
            if (err) {
                done(err, null);
            } else {
                done(null,orders);


            }
        });
    }else if(populateLevels===0){
        Order.count({$and:query},function(err,count) {
            if(err){
                done(err, null);
            }else {
                Order.find({$and: query}, '--updateHistory -products -shippingAddress -billingAddress -statusHistory -invoiceAddress -transport -invoice -statusReason').sort('-created').populate('buyer.nVipaniUser buyer.nVipaniCompany buyer.contact buyer.businessUnit seller.contact seller.nVipaniCompany seller.nVipaniUser seller.businessUnit mediator.nVipaniUser mediator.nVipaniCompany mediator.contact mediator.businessUnit', 'name displayName firstName lastName middleName addresses nVipaniUser nVipaniCompany gstinNumber companyName name').populate('offer', 'name offerType offerNumber user owner').skip(pageOptions.page > 0 ? ((pageOptions.page - 1) * pageOptions.limit) : 0).limit(pageOptions.limit).exec(function (err, orders) {
                    if (err) {
                        done(err, null);
                    } else {
                        if(isFilterSummary && (!pageOptions.page || pageOptions.page === 1)){
                            _this.findQueryByOrders(query,1,null,null,function (err,summaryOrders) {
                                if(summaryOrders) {
                                    summaries(summaryOrders,null, function (summaryData) {
                                        done(null,{order: orders, total_count: count,summaryData:summaryData});
                                    });
                                }else {
                                    done(err,null);
                                }
                            });
                        }else {
                            done(null,{order: orders, total_count: count});
                        }

                    }
                });
            }
        });
    }
};

exports.batchOrderUpdate=function(query,update,done){
    Order.update(
        query,
        {$set:update},
        {multi: true},
        function(err,updateOffers){
            done(err,updateOffers);
        }
    );
};

function summaries(orders,summaryFilters,done) {
    var newOrders = []; // 'Placed' and Confirmed
    var disputedOrders = []; // 'Disputed'
    var deliveryPendingOrders = []; // Shipped Orders
    var paymentPendingOrders = []; // Payment OverDue orders
    var deliveredOrders = []; // In Progress and Delivered Orders
    var intraOrders=[];
    var ordersSummary = {};
    async.each(orders, function (order, callback) {
        /*if ((order.paymentOptionType.payNow.selection && order.paymentOptionType.payNow.selection === true && order.paymentOptionType.payNow.status === 'Overdue') ||
            (order.paymentOptionType.payOnDelivery.selection && order.paymentOptionType.payOnDelivery.selection === true && order.paymentOptionType.payOnDelivery.status === 'Overdue') ||
            (order.paymentOptionType.lineOfCredit.selection && order.paymentOptionType.lineOfCredit.selection === true && order.paymentOptionType.lineOfCredit.status === 'Overdue')) {
            paymentPendingOrders.push(order);
        } else if (order.paymentOptionType.installments.selection && order.paymentOptionType.installments.selection === true) {
            for (var i = 0; i < order.paymentOptionType.installments.options.length; i++) {
                if (order.paymentOptionType.installments.options[i].status === 'Overdue') {
                    paymentPendingOrders.push(order);
                    break;
                }
            }
        }*/

        if (order.currentStatus === 'Placed' || order.currentStatus === 'Confirmed') {
            newOrders.push(order);
        } else if (order.currentStatus === 'Disputed') {
            disputedOrders.push(order);
        } else if (order.currentStatus === 'Shipped' || order.currentStatus === 'InProgress') {
            deliveryPendingOrders.push(order);
        } else if (order.currentStatus === 'Delivered') {
            deliveredOrders.push(order);
        }
        if(order.isIntraStock){
            intraOrders.push(order);
        }
        callback();
    }, function (eachErr) {
        if(!eachErr){
            ordersSummary.New=(summaryFilters && summaryFilters.New)?newOrders:newOrders.length;

            ordersSummary.Disputed=(summaryFilters && summaryFilters.Disputed)?disputedOrders:disputedOrders.length;

            ordersSummary.DeliveryPending=(summaryFilters && summaryFilters.DeliveryPending)?deliveryPendingOrders:deliveryPendingOrders.length;

            ordersSummary.Delivered=(summaryFilters && summaryFilters.Delivered)?deliveredOrders:deliveredOrders.length;

            ordersSummary.PaymentPending=(summaryFilters && summaryFilters.PaymentPending)?paymentPendingOrders:paymentPendingOrders.length;
            ordersSummary.Intra=(summaryFilters && summaryFilters.Intra)?intraOrders:intraOrders.length;
            ordersSummary.Count=orders.length;
        }
    });
    done(ordersSummary);
}

exports.getOrdersSummary=function (orders,source,summaryFilters,done) {
    if(source){
        summaries(orders,summaryFilters,function (summaries) {
            done(summaries);
        });
    }else{
        summaries(orders,summaryFilters,function (summaries) {
            done(summaries);
        });
    }
};
function getMatchedPaymentTerms(paymentTerms,value) {
    return paymentTerms.filter(function (eachPaymentTerm) {
       return ((eachPaymentTerm.paymentTerm.netTerm && eachPaymentTerm.paymentTerm.netTerm>=value) ||
           (eachPaymentTerm.paymentTerm.discountTerm && eachPaymentTerm.paymentTerm.discountTerm>=value)) &&
           eachPaymentTerm.paymentTerm.type !== 'PayOnDelivery' && eachPaymentTerm.paymentTerm.type !== 'PayNow' &&
           eachPaymentTerm.paymentTerm.type !== 'Installments';
    });
}
function getFilteredPaymentTerms(order,currentMatachedPaymentTerm) {
    if(order.currentStatus === 'Delivered'){
        if(order.applicablePaymentTerm.type === 'PayOnDelivery'){
            return order.applicablePaymentTerms.filter(function (eachPaymentTerms) {
                return eachPaymentTerms.paymentTerm.type === 'PayOnDelivery';
            });
        }else{
            return getMatchedPaymentTerms(order.applicablePaymentTerms,Math.ceil((Date.now()-(order.deliveryDate ? order.deliveryDate.getTime():''))/(1000*60*60*24)));
        }
    }else if(order.currentStatus === 'Drafted'){
        return order.applicablePaymentTerms;
    }else {
        return order.applicablePaymentTerms.filter(function (eachPaymentTerms) {
            return eachPaymentTerms.paymentTerm.type !== 'PayNow' &&  eachPaymentTerms.paymentTerm.type !== 'Installments';
        });
    }
}
exports.getOrderPaymentTerms=function(orderId,paymentTerms,companyPaymentTerms,applicablePaymentTerms,ispopulate,done){
    if(paymentTerms && paymentTerms.length>0){
        paymentTerms.forEach(function (eachPaymentTerm) {
            if (ispopulate) {
                applicablePaymentTerms.push(eachPaymentTerm.paymentTerm);
            } else {
                if (eachPaymentTerm.selected || !eachPaymentTerm.paymentTerm) {
                    applicablePaymentTerms.push({
                        paymentTerm: eachPaymentTerm.paymentTerm ? eachPaymentTerm.paymentTerm : eachPaymentTerm,
                        selected: (eachPaymentTerm.enabled || eachPaymentTerm.selected || !eachPaymentTerm.paymentTerm)
                    });
                }
            }

        });
        done(null,applicablePaymentTerms);
    }else if(!companyPaymentTerms){
        Order.findById(orderId,'applicablePaymentTerms applicablePaymentTerm currentStatus deliveryDate').exec(function (companyError,orderData) {
            if(companyError){
                done(companyError,null);
            }else {
                if (ispopulate) {
                    Order.populate(orderData, {
                        path: 'applicablePaymentTerms.paymentTerm', model: 'PaymentTerm'
                    }, function (err,dataOrder) {
                        companyPaymentTerms = [];
                        paymentTerms = getFilteredPaymentTerms(orderData);
                        _this.getOrderPaymentTerms(orderId, paymentTerms, companyPaymentTerms, applicablePaymentTerms,ispopulate, function (err, paymentTerms) {
                            done(err, paymentTerms);
                        });
                    });
                } else {
                    companyPaymentTerms = [];
                    paymentTerms = orderData.applicablePaymentTerms;
                    _this.getOrderPaymentTerms(orderId, paymentTerms, companyPaymentTerms, applicablePaymentTerms,ispopulate, function (err, paymentTerms) {
                        done(err, paymentTerms);
                    });

                }
            }
        });
    }else{
        done(null,[]);
    }
};
exports.getOrderPaymentModes=function(orderId,paymentModes,companyPaymentModes,applicablePaymentMode,ispopulate,done){
    if(paymentModes && paymentModes.length>0){
        paymentModes.forEach(function (eachPaymentMode) {

            if(eachPaymentMode.selected || !eachPaymentMode.paymentMode) {
                if (ispopulate) {
                    applicablePaymentMode.push(eachPaymentMode.paymentMode);
                } else {
                    applicablePaymentMode.push({
                        paymentMode: eachPaymentMode.paymentMode ? eachPaymentMode.paymentMode : eachPaymentMode,
                        selected: (eachPaymentMode.selected || eachPaymentMode.enabled)
                    });
                }
            }

        });
        done(null,applicablePaymentMode);
    }else if(!companyPaymentModes){
        Order.findById(orderId,'applicablePaymentModes').exec(function (orderError,orderData) {
            if(orderError){
                done(orderError,null);
            }else {
                if(ispopulate) {
                    Order.populate(orderData, {
                        path: 'applicablePaymentModes.paymentMode',model:'PaymentMode',select:'name'}, function (errData,dataOrder) {
                        if(errData){
                            done(errData,null);
                        }else {
                            paymentModes = dataOrder.applicablePaymentModes;
                            _this.getOrderPaymentModes(orderId, paymentModes, companyPaymentModes, applicablePaymentMode, ispopulate, function (err, paymentsMode) {
                                done(null, paymentsMode);
                            });
                        }
                    });
                }else {
                    companyPaymentModes = [];
                    paymentModes = orderData.applicablePaymentModes;
                    _this.getOrderPaymentModes(orderId, paymentModes, companyPaymentModes, applicablePaymentMode,ispopulate, function (err, paymentsMode) {
                        done(null, paymentsMode);
                    });
                }

            }
        });
    }else{
        done(null,[]);
    }
};
/**
 *find Order with populate tables
 */
exports.fetchOrderById=function (orderId,done) {
    Order.findById(orderId).populate('offer', 'name offerType offerNumber user owner').populate('statusHistory.user','displayName profileImageURL').populate('buyer.nVipaniUser buyer.nVipaniCompany buyer.contact buyer.businessUnit seller.contact seller.nVipaniCompany seller.nVipaniUser seller.businessUnit mediator.nVipaniUser mediator.nVipaniCompany mediator.contact mediator.businessUnit', 'name displayName firstName lastName middleName addresses nVipaniUser nVipaniCompany gstinNumber companyName addresses').populate('products.inventory applicablePaymentTerm.paymentDetails.paymentMode applicablePaymentTerm.installmentPaymentDetails.paymentMode applicablePaymentTerm.paymentTerm').populate('user user', 'displayName').exec(function (err, resultOrder) {

        /*Order.findById(order._id).populate('toContacts.contacts.contact toContacts.groups.group notificationsContacts.nVipaniUser', 'firstName lastName middleName name').populate('products.inventory').populate('user', 'displayName').exec(function (err, resultOrder) {
         */	if (err) {
           /* logger.error('Error while loading the Order details with order id- ' + orderId);*/
            done(err,null);
        }else if(!resultOrder){
            done(new Error('No Order with the orderId:'+orderId),null);
        } else {
            done(null,resultOrder);
        }

    });
};
exports.fetchOrderByIdWithTax=function (orderId, done) {
    Order.findById(orderId).populate('offer','name offerType offerNumber user owner').populate('buyer.nVipaniUser buyer.nVipaniCompany buyer.contact buyer.businessUnit seller.contact seller.nVipaniCompany seller.nVipaniUser seller.businessUnit mediator.nVipaniUser mediator.nVipaniCompany mediator.contact mediator.businessUnit', 'name displayName firstName lastName middleName addresses nVipaniUser nVipaniCompany gstinNumber companyName').populate('products.inventory').populate('user', 'displayName').exec(function (err, resultOrder) {

        /*Order.findById(order._id).populate('toContacts.contacts.contact toContacts.groups.group notificationsContacts.nVipaniUser', 'firstName lastName middleName name').populate('products.inventory').populate('user', 'displayName').exec(function (err, resultOrder) {
         */	if (err) {
            /* logger.error('Error while loading the Order details with order id- ' + orderId);*/
            done(err,null);
        }else if(!resultOrder){
            done(new Error('No Order with the orderId:'+orderId),null);
        } else {
            Order.populate(resultOrder,{path:'products.inventory.taxGroup',model:'TaxGroup'},function (resultsErr,populateResult) {
                done(resultsErr,populateResult);
            });

        }

    });
};
exports.saveOrder=function (order,logger,done) {
    if(order instanceof Order){
    order.save(function (orderErr) {
        if (orderErr) {
            logger.error('Error while updating the order with Order number ' + order.orderNumber);
            done(orderErr, null);
        } else {
            _this.fetchOrderById(order._id, function (orderResponseErr, orderResponse) {
                if (orderResponseErr) {
                    logger.error('Error while loading the order details with Order number ' + orderResponse.orderNumber + ' after creation');
                    done(orderResponseErr, null);
                } else if (!orderResponse) {
                    done(new Error('No order after save'), null);
                } else {
                    done(null, orderResponse);
                }
            });
        }
    });
    }else{
        done(new Error('No proper order data'),null);
    }
};
exports.filePath=function(order,done) {
    var fs = require('fs');
    var reportPath=null;
    if(order && order.applicablePaymentTerm){
        if(order.applicablePaymentTerm.type==='Installments'&& order.applicablePaymentTerm.installmentPaymentDetails.length>0 && order.applicablePaymentTerm.installmentPaymentDetails[0].invoiceFile && order.applicablePaymentTerm.installmentPaymentDetails[0].invoiceFile.length>0) {
            reportPath = order.applicablePaymentTerm.installmentPaymentDetails[0].invoiceFile;
        }else if(order.applicablePaymentTerm.paymentDetails && order.applicablePaymentTerm.paymentDetails.invoiceFile && order.applicablePaymentTerm.paymentDetails.invoiceFile.length>0){
            reportPath=order.applicablePaymentTerm.paymentDetails.invoiceFile;
        }
    }
    if (reportPath&& fs.existsSync(reportPath)) {
        done(reportPath);
    }else{
        done(null);
    }

};
exports.invoiceReportGeneration=function(order,regenerate,logger,done) {
    if(order.seller.nVipaniUser) {
        commonUserUtil.getQueryByUser({_id: order.seller.nVipaniUser},4,function (userErr, sellerCompanyUser) {
            if (userErr) {
                logger.error('Error while loading the Company details with company id- ' + order.seller.nVipaniUser);
                done(userErr, null);
            } else {
                Companies.findById({_id: order.buyer.nVipaniCompany}, '-settings -updateHistory -companiesVersionKey -bankAccountDetailsProof -panNumberProof -panNumber -cstNumber -cstNumberProof -vatNumber -vatNumberProof -tinNumber -tinNumberProof').exec(function (companyErr, buyerCompany) {

                    if (companyErr || buyerCompany === null) {
                        var cname = order.buyer.contact ?(order.buyer.contact.firstName + ' ' + order.buyer.contact.middleName + ' ' + order.buyer.contact.lastName):'';
                        buyerCompany={'name':cname};
                        if (order.buyer.contact && order.buyer.contact.companyName) {
                            buyerCompany.name = order.buyer.contact.companyName;
                        }
                        if(order.buyer.contact && order.buyer.contact.gstinNumber)
                        buyerCompany.gstinNumber= order.buyer.contact.gstinNumber;
                        logger.debug('Buyer is not registered :' + order.buyer);
                    }
                    if (buyerCompany && buyerCompany.gstinNumber === '' &&  order.buyer.contact && order.buyer.contact.gstinNumber && order.buyer.contact.gstinNumber !== '') {
                        buyerCompany.gstinNumber = order.buyer.contact.gstinNumber;
                    }
                    _this.fetchOrderByIdWithTax(order._id, function (orderResponseErr,orderResponse) {
                        if (orderResponseErr) {
                            logger.error('Error while loading the order details with Order number ' + order.orderNumber + ' after creation');
                            /*return res.status(400).send({
                                message: errorHandler.getErrorMessage(orderResponse)
                            });*/
                            done(orderResponse, null);
                        } else {
                            reportsUtil.generateInvoiceReport(orderResponse,regenerate, sellerCompanyUser.company, buyerCompany, function (reportGenErr, invoiceGeneratedOrder) {
                                if (reportGenErr) {
                                    logger.error('Error while generating the invoice for the order with Order number ' + order.orderNumber, reportGenErr);

                                    done(reportGenErr, null);
                                } else {
                                    order=invoiceGeneratedOrder;
                                    order.save(function (saveOrderErr) {
                                        if (saveOrderErr) {
                                            logger.error('Error while updating the order with Order number ' + order.orderNumber, saveOrderErr);

                                            done(saveOrderErr,null);
                                        } else {
                                            done(null,order);

                                        }
                                    });
                                }
                            });
                        }
                    });
                });
            }
        });
    }else{
        done('Report is generated only from the seller',null);
    }
};
