'use strict';

/**
 * Module dependencies.
 */
var mongoose = require('mongoose'),
    _ = require('lodash'),
    async = require('async'),
    UserGroup = mongoose.model('UserGroup'),
    User = mongoose.model('User'),
    Company = mongoose.model('Company');

/**
 *
 * @param dbuser
 * @param dcompany
 * @param userGroup
 * @param isGroup
 * @param isActive
 * @param isDisabled
 * @param isRemove
 * @param done
 */
function findByUserCompany(dbuser,dcompany,userGroup,isGroup,isActive,isDisabled,isRemove,done){
    // we need to think if the user is registered some other companyName afterwards
    if(isRemove && dbuser.company.toString()===dcompany._id.toString()){
        dbuser.deleted=true;
        done(null, dbuser);
    }else{
        async.forEachSeries(dbuser.companies, function (employee, callback) {
            if (employee.company.toString() === dcompany._id.toString()) {
                if (isGroup) {
                    dbuser.companies[dbuser.companies.indexOf(employee)].userGroup = userGroup;
                }else if (isActive) {
                    dbuser.disabled = false;
                }else if (isDisabled) {
                    dbuser.disabled = true;
                }else if(isRemove){
                    dbuser.companies.splice(dbuser.companies.indexOf(employee),1);
                }
                done(null, dbuser);
            }else {
                callback();
            }
        }, function (err) {
            if (err) {
                done(err, dbuser);
            } else {
                if (dbuser.companies && dbuser.companies.length === 0 && isGroup) {
                    dbuser.companies.push({'company': dcompany._id, 'userGroup': userGroup});
                }
                done(null, dbuser);
            }
        });
    }
}
/**
 * Save company with business units
 * Save user with company id
 * @param dcompany
 * @param user
 * @param done
 */
function saveCompany(dcompany,user,userGroup,isGroup,isUnits,isActive,isDisabled,isRemove,done) {
    dcompany.save(function (companyErr) {
        if (companyErr) {
            done(companyErr,null);
        } else {
            /* if(!user.company) {*/
            var isUser=user.username? ({'username':user.username,'deleted':false}):{_id:user};
            User.findOne(isUser, '-salt -password', function (errFetch, dbuser) {
                if(errFetch){
                    done(errFetch, dcompany);
                }else if(!dbuser){
                    done(new Error('No user with user id :'+user), dcompany);
                }else{
                    dbuser = _.extend(dbuser, {});

                    findByUserCompany(dbuser,dcompany,userGroup,isGroup,isActive,isDisabled,isRemove,function (companyUserErr,companyDbUser) {
                        if(companyUserErr){
                            done(companyUserErr,null);
                        }else {
                            if(isUnits){
                                dbuser.company = dcompany._id;
                            }
                            companyDbUser.save(function (userErr) {
                                /* if (userErr) {
                                   done(userErr,null);
                                 } else {
                                     done()
                                 }*/
                                done(userErr, dcompany);
                            });

                        }
                    });


                }
            });
            /* }else{
                 done(null,user.company);
             }*/

        }
    });
}
/**
 *
 * @param units
 */
function getCompanyEmployeeBusinessUnits(units,done) {
    var businessUnitArray=[];
    async.forEach(units,function (eachUnit,callback) {
        businessUnitArray.push({'businessUnit':eachUnit});
        callback();
    },function(err){
        done(err, businessUnitArray);
    });
}
/**
 *
 * @param dbCompany
 * @param user
 * @param units
 * @param userGroup
 * @param done
 */
function addCompanyBusinessUser(dbCompany,user,units,userGroup,statusToken,isGroupChange,isUnits,isActive,isDisabled,isRemove,done){
    var found;

    if(user.company) {
        async.forEachSeries(dbCompany.employees, function (employee, callback) {


            if (employee.user && employee.user.toString() === user._id.toString() && (!userGroup || (isUnits && userGroup && employee.userGroup.toString() === userGroup) || isGroupChange || isActive ||isDisabled || isRemove )) {
                if(isUnits) {
                    dbCompany.employees[dbCompany.employees.indexOf(employee)].statusToken.add(statusToken);
                    dbCompany.employees[dbCompany.employees.indexOf(employee)].businessUnits.concat(getCompanyEmployeeBusinessUnits(units));
                }
                if(isGroupChange){
                    dbCompany.employees[dbCompany.employees.indexOf(employee)].userGroup= userGroup;

                }
                if(isActive){
                    dbCompany.employees[dbCompany.employees.indexOf(employee)].status='Active';
                }
                if(isDisabled){
                    dbCompany.employees[dbCompany.employees.indexOf(employee)].status='Inactive';
                }
                if(isRemove){
                    if(user.userType==='Employee') {
                        dbCompany.employees.splice(dbCompany.employees.indexOf(employee), 1);
                    }else {
                        callback(new Error('Default business user should not deleted'));
                    }
                }
                if(isGroupChange ||isActive || isDisabled )
                    found = dbCompany.employees[dbCompany.employees.indexOf(employee)];

            }
            if(found){
                done(null,dbCompany,found);
            }else {
                callback();
            }
        }, function (err) {
            if (err) {
                done(err,dbCompany,found);
            } else {
                done(err,dbCompany,found);
            }
        });
    }else {

        done(null,dbCompany,found);

    }
}
/**
 *  find Employee at company with specific user group in business unit
 *  Add company employee
 * @param user
 * @param company
 * @param units
 * @param userGroup
 * @param done
 */
exports.findOrCreateCUser=function(user,company,units,userGroup,statusToken,isGroup,isUnits,isActive,isDisabled,isRemove,done) {
    Company.findOne({
        _id: company,
        'deleted':false
    }, function (errCompany, dbCompany) {
        if (errCompany) {
            done(errCompany,null,null);
        }else{
            if(dbCompany){
                addCompanyBusinessUser(dbCompany,user,units,userGroup,statusToken,isGroup,isUnits,isActive,isDisabled,isRemove,function (err,company,found) {
                    var matchedRecord;
                    if(err){
                        done(err, null, null);
                    }else {
                        if (!found && isUnits) {
                            getCompanyEmployeeBusinessUnits(units, function (err, businessunits) {
                                matchedRecord = {
                                    user: user._id,
                                    businessUnits: businessunits,
                                    statusToken: statusToken,
                                    userGroup: userGroup,
                                    status: 'Request',
                                    company: company._id,
                                    fromDate: Date.now()
                                };
                                company.employees.push(matchedRecord);
                                saveCompany(company, user, userGroup, isGroup, isUnits, isActive, isDisabled, isRemove, function (err, finalCompany) {
                                    done(err, finalCompany, matchedRecord);
                                });
                            });

                        } else {
                            matchedRecord = dbCompany.employees.indexOf(found);
                            saveCompany(company, user, userGroup, isGroup, isUnits, isActive, isDisabled, isRemove, function (err, finalCompany) {
                                done(err, finalCompany, matchedRecord);
                            });
                        }
                    }


                });



            }else{
                done(new Error('No Company with the id :'+company),null,null);
            }

        }
    });
};
/**
 * find user group by user group name
 * @param groupName
 * @param done
 */
exports.findUserGroupByName=function (groupName,done) {
    UserGroup.findOne({name:groupName}).populate('user', 'displayName').exec(function (err, userGroup) {
        if(err){
            done(err,null);
        }else if(!userGroup){
            done(new Error('No user group with the name'+groupName),null);
        }else{
            done(null,userGroup);
        }
    });
};
/**
 * find user group by user group name
 * @param groupName
 * @param done
 */
exports.findUserGroupById=function (groupId,done) {
    UserGroup.findOne({_id:groupId}).populate('user', 'displayName').exec(function (err, userGroup) {
        if(err){
            done(err,null);
        }else if(!userGroup){
            done(new Error('No user group with group Id'+groupId),null);
        }else{
            done(null,userGroup);
        }
    });
};
/**
 * remove the user from the company employee lists
 * @param user
 * @param company
 * @param done
 */
exports.removeUserFromCompany=function (user,company,done) {

};
/**
 *
 * @param logger
 * @param company
 * @param user
 * @param populateLevel
 * @param done
 */

exports.findByCompanyId=function (logger,company,user,populateLevel,done) {
    if(company) {
        logger.debug('Found the company object id :'+company);
        Company.findOne({
            _id: company,
            'deleted': false
        }, function (errCompany, dbCompany) {
            if (errCompany) {
                done(errCompany, null);
            } else if (!dbCompany) {
                done(new Error('No Company with id' + company), null, null);
            } else {
                if(populateLevel===0) {
                    done(null, dbCompany);
                }else{
                    done(null, dbCompany);
                }
                    /*if(populateLevel>0) {

                    }else if(populateLevel>1){

                    }else if(populateLevel>2){

                    }else if(populateLevel>3){

                    }

                }*/
            }
        });

    }else{
        logger.error('No company id ');
        done(new Error('No company id'),null);
    }
};

exports.findBusinessUserByCompany=function(query,pageOptions, done){
    var totalEmployees=[],skip,pageNo,limit;
    pageNo=parseInt(pageOptions.page);
    limit=parseInt(pageOptions.limit);
    skip=pageNo > 0 ? ((pageNo-1) * limit) : 0;
    Company.findOne({
        _id: query._id,
        deleted: false
    }).sort('-created').populate('employees.user employees.userGroup').exec(function (err, companyEmployees) {
        if (err) {
            done(err, null);
        } else if (companyEmployees === null) {
            done(new Error('No company found'), null);
        } else {
            var retVal = {};
            retVal.employees = companyEmployees.employees;
            done(null,retVal);
            /*Company.populate(companyEmployees,{path: 'employees.user employees.userGroup'},function (err, company) {
                var retVal = {};
                retVal.employees = company.employees;
                //retVal.total_count = employeesCount;
                done(err, retVal);
            });*/
        }

    });


};

exports.findInchargeUsers = function(query,done){
    Company.findOne({
        _id: query._id,
        deleted: false
    }).exec(function (err, companyEmployees) {
        if (err) {
            done(err, null);
        } else if (companyEmployees === null) {
            done(new Error('No company found'), null);
        } else {
            Company.populate(companyEmployees,[{path: 'employees.user'},{path: 'employees.userGroup',match:{name:{$in:['Admin','Manager']}}}],function (searchErr, company) {
                if(searchErr){
                    done(searchErr,null);
                }else if(company) {
                    company.employees= company.employees.filter(function (eachEmployee) {
                            return eachEmployee.userGroup!==null;
                        });
                        done(err,company);

                }else{
                    done(new Error('No In charge user from company'),null);
                }
            });
        }

    });
};
function matchedEmployee(employies,user) {
    return employies.filter(function (eachEmployee) {
        var employeeuserId= (eachEmployee.user && eachEmployee.user._id?eachEmployee.user._id:eachEmployee.user);
        return  employeeuserId && employeeuserId.toString()===user._id.toString();
    });

}

exports.findBusinessUnitsByCompany = function(user,companyId,done) {
    Company.findById(companyId, 'name businessUnits registrationCategory employees profileImageURL').sort('-created').populate('businessUnits.businessUnit employees.user employees.userGroup').populate('registrationCategory').exec(function (err, company) {
        if (err) return done(err);
        else if (!company) return done(new Error('Failed to load Company ' + companyId));
        else {
            Company.populate(company, {
                path: 'businessUnits.businessUnit.employees.user',
                model: 'User',
                select: 'username firstName lastName middleName displayName'
            }, function (nestedErr, populatedEmployedBunits) {
                if (nestedErr) {
                    done(nestedErr);
                } else {
                    if(user.userType === 'Employee') {
                        var employee = matchedEmployee(populatedEmployedBunits.employees, user), businessUnits=[];
                        async.forEachSeries(employee[0].businessUnits, function (eachEmployeeUnit, callback) {
                            var eachBusinessUnit = populatedEmployedBunits.businessUnits.filter(function (eachFilterValue) {
                                return eachEmployeeUnit.businessUnit.toString() === eachFilterValue.businessUnit._id.toString();
                            });
                            if (eachBusinessUnit.length > 0) {
                                businessUnits.push(eachBusinessUnit[0]);
                            }
                            callback();
                        }, function (err) {
                            populatedEmployedBunits.businessUnits = businessUnits;
                            done(err, populatedEmployedBunits);
                        });
                    }else{
                        done(null,populatedEmployedBunits);
                    }
                }
            });
        }
    });
};
exports.findDefaultCompanyBusinessUnit=function(companyId,businessUnit,done){
    if(businessUnit){
        done(null, businessUnit);
    }else {
        if (companyId) {
            Company.findOne(companyId, {
                businessUnits: {$elemMatch: {defaultBusinessUnit: true}}
            }).populate('employees.businessUnits.businessUnit').exec(function (companyErr, company) {
                if (companyErr) {
                    done(companyErr, null);
                } else {
                    if (company && company.businessUnits.length > 0) {
                        done(null, company.businessUnits[0].businessUnit);
                    } else {
                        done(new Error('No default Business Unit with the company'), null);
                    }

                }
            });
        } else {
            done(new Error('No company is specified to get the Businessunit'));
        }
    }
};
function employeeBusinessUnits(companyBusinessUnits,employeeBusinessUnits,logger,done) {
    var businessUnits=[];
    async.forEachSeries(employeeBusinessUnits,function (eachEmployeeUnit,callback) {
        var eachBusinessUnit = companyBusinessUnits.filter(function (eachFilterValue) {
            return eachEmployeeUnit.businessUnit.toString() === eachFilterValue.businessUnit._id.toString();
        });
        if (eachBusinessUnit.length > 0){
            businessUnits.push({
                defaultBusinessUnit: eachBusinessUnit[0].defaultBusinessUnit,
                businessUnit: {_id: eachBusinessUnit[0].businessUnit._id,addresses:eachBusinessUnit[0].businessUnit.addresses, name: eachBusinessUnit[0].businessUnit.name}
            });
            /* callback();*/
        }else{
            logger.error('No proper BusinessUnit '+JSON.stringify(eachEmployeeUnit));
            /* callback();*/
            /*callback(new Error('No proper BusinessUnit '+eachEmployeeUnit.name));*/
        }
        callback();
    },function (err) {
        done(err,businessUnits);
    });
}
exports.findCompanyEmployeeBusinessUnits=function(user,logger,done){
    if (user._id) {
        if(user.userType === 'Employee') {
            Company.findOne({'employees.user': user._id.toString()}).populate('businessUnits.businessUnit', 'name addresses disabled deleted').populate('employees.userGroup', 'name').exec(function (companyErr, company) {
                if (companyErr) {
                    done(companyErr, null);
                } else {
                    var employee = matchedEmployee(company.employees,user);
                    if(employee[0].status === 'Inactive'){
                        done(new Error('User is in Inactive mode'),null);
                    }else {
                        if (employee.length > 0) {
                            var activeUnits = company.businessUnits.filter(function (eachUnit) {
                                return eachUnit.status === 'Active';
                            });
                            employeeBusinessUnits(activeUnits, employee[0].businessUnits, logger, function (err, businessUnits) {
                                done(err, {
                                    company: {id: company._id, name: company.name},
                                    userGroup: employee[0].userGroup,
                                    businessUnits: businessUnits
                                });
                            });

                        } else {
                            done(new Error('Find default Business Unit with the company'), null);
                        }
                    }

                }
            });
        }else{
            Company.findOne({_id: user.company.toString()}).populate('businessUnits.businessUnit', 'name disabled deleted').populate('employees.userGroup', 'name').exec(function (companyErr, company) {
                if (companyErr) {
                    done(companyErr, null);
                } else {
                    var employee = matchedEmployee(company.employees, user);
                    if (employee.length > 0) {
                        var businessUnits=[];
                        var activeUnits=company.businessUnits.filter(function (eachUnit) {
                            return eachUnit.status === 'Active';
                        });
                        async.forEachSeries(activeUnits,function (eachUnit,callback) {
                            businessUnits.push({
                                defaultBusinessUnit: eachUnit.defaultBusinessUnit,
                                businessUnit: {_id: eachUnit.businessUnit._id, name: eachUnit.businessUnit.name}
                            });
                            callback();
                        },function (err) {
                            done(err, {
                                company: {id: company._id, name: company.name},
                                userGroup: employee[0].userGroup,
                                businessUnits: businessUnits
                            });
                        });
                    } else {
                        done(new Error('Find default Business Unit with the company'), null);
                    }

                }
            });
        }
    } else {
        done(new Error('No company is specified to get the Businessunit'));
    }
};

