'use strict';
var mongoose = require('mongoose'),
    Inventory = mongoose.model('Inventory'),
    ItemMaster = mongoose.model('ItemMaster'),
    StockMaster = mongoose.model('StockMaster'),
    ProductBrand = mongoose.model('ProductBrand'),
    errorHandler = require('../errors.server.controller'),
    Company = mongoose.model('Company'),
    Order = mongoose.model('Order'),
    dbUtil = require('./common.db.util'),
    BusinessUnit = mongoose.model('BusinessUnit'),
    UnitOfMeasure = mongoose.model('UnitOfMeasure'),
    logger = require('../../../lib/log').getLogger('TRACKER', 'DEBUG'),
    _ = require('lodash'),
    _this=this,
    async = require('async');

/**
 * find the conversion inventories in track paths.
 * @param eachStockMaster
 * @param eachNode
 * @param pathNode
 * @param done
 */

function findConversionInventories(eachStockMaster,eachNode,pathNode,isForward,done) {

    async.forEachSeries(isForward?eachStockMaster.stockOut.inventories:eachStockMaster.stockIn.inventories, function (inventory, callback) {
        if (!inventory.inventory) {
            logger.error('No inventory at the '+(isForward?'Stock Out':'Stock In') + JSON.stringify(inventory));
            done(new Error('No inventory at the '+(isForward?'Stock Out':'Stock In')), eachNode, pathNode);
        } else if (!inventory.stockMasters || (inventory.stockMasters && inventory.stockMasters.length === 0)) {
            logger.error('No stockMasters at the '+(isForward?'Stock Out':'Stock In') + JSON.stringify(inventory));
            done(new Error('No stockMasters at the'+(isForward?'Stock Out':'Stock In')), eachNode, pathNode);
        } else {
            var childNode = {
                inventory: inventory.inventory.toString(),
                stockMasters: inventory.stockMasters
            };
            var childEachNode = {
                inventory: inventory.inventory.toString(),
                stockMasters: inventory.stockMasters
            };
            logger.debug('Find the children for the node ' + JSON.stringify(inventory));
            findByTrackerEachNode(childEachNode, childNode,isForward, function (err, eachNode, resultsNode) {
                if (err) {
                    logger.error('Error While fetching children with the node' + eachNode);
                    callback(err, resultsNode);
                } else {
                    logger.debug('Update the parent node ' + eachStockMaster.BatchNumber + ' with the children :' + JSON.stringify(resultsNode));
                    pathNode.destinations.push(resultsNode);
                    callback();
                }
            });
        }
    }, function (err) {
        done(err, eachNode, pathNode);
    });

}

/**
 * Fetch order matched stock Master with batch number
 * @param eachStockMaster
 * @param orderProducts
 * @param matchStockMaster
 * @param done
 */
function findOrderMatchedProducts(eachStockMaster,orderProducts,matchStockMaster,done) {
    async.forEachSeries(orderProducts,function (eachProduct,productCallback) {
        async.forEachSeries(eachProduct.stockMasters,function (eachOrderStockMaster,stockCallBack) {
            if(eachOrderStockMaster.batchNumber.toString()===eachStockMaster.batchNumber.toString()){
                matchStockMaster.inventory=eachProduct.inventory.toString();
                matchStockMaster.stockMasters={count:eachOrderStockMaster.numberOfUnits,stockMaster:eachOrderStockMaster.stockMaster.toString()};
                done(null,matchStockMaster);
            }else{
                stockCallBack();
            }
        },function (err) {
            productCallback(err,null);
        });
    },function (err) {
        done(err,matchStockMaster);
    });

}

/**
 *  fetch orders inventories for track paths.
 * @param eachStockMaster
 * @param eachNode
 * @param pathNode
 * @param done
 */
function findOrdersInventories(eachStockMaster,eachNode,pathNode,isForward,done) {
    async.forEachSeries(isForward?eachStockMaster.stockOut.orders:eachStockMaster.stockIn.orders, function (eachOrder, callback) {
        if(!eachOrder.order) {
            logger.error('No order at the stockIn'+JSON.stringify(eachOrder));
            done(new Error('No order at the stockIn'),null);
        }else{
            Order.findById(eachOrder.order.toString()).exec(function (err, order) {
                if(err){
                    done(err,eachNode,pathNode);
                }else {
                    var matchStocker={};
                    findOrderMatchedProducts(eachStockMaster,order.products,matchStocker,function (err,foundStockMaster) {
                        if(err){
                            done(err,eachNode,pathNode);
                        }else {
                            var childNode = {
                                inventory: foundStockMaster.inventory.toString(),
                                stockMasters: [foundStockMaster.stockMasters],
                                orderNumber: order.orderNumber

                            };
                            var childEachNode = {
                                inventory: foundStockMaster.inventory.toString(),
                                stockMasters: [foundStockMaster.stockMasters]
                            };
                            logger.debug('Find the children for the node ' + JSON.stringify(eachStockMaster.inventory.toString()));
                            findByTrackerEachNode(childEachNode, childNode,isForward, function (err, eachNode, resultsNode) {
                                if (err) {
                                    done(err,eachNode,pathNode);
                                } else {
                                    pathNode.destinations.push(resultsNode);
                                    callback();
                                }

                            });
                        }
                    });
                }
            });
        }
    },function (err) {
        done(err,eachNode,pathNode);
    });

}

/**
 * find inventory by id with some more fields at
 * @param id
 * @param done
 */
function fetchInventoryCompanyAndRegistrationCategory(id,done) {
    // Still need to find out the BusinessUnit for the user and some fields should be removed from the inventory population.
    Inventory.findById(id).populate('user','company').exec(function (err, inventory){
        if(err){
            done(err,null);
        }else if(!inventory){
            done(new Error('No Inventory with the inventory id :'+id),null);
        }else {
            Inventory.populate(inventory, {
                path: 'user.company',
                model: 'Company',
                select: 'name profileImageURL croppedProfileImageURL addresses registrationCategory'
            }, function (nestedCompanyErr, inventoryCompany) {
                if (nestedCompanyErr) {
                    done(nestedCompanyErr,null);
                } else {
                    Inventory.populate(inventory, {
                        path: 'user.company.registrationCategory',
                        model: 'RegistrationCategories',
                        select: 'name description'
                    }, function (nestedRegistrationCategoryErr, inventoryCompanyRegistrationCategory) {
                        if (nestedRegistrationCategoryErr) {
                            done(nestedRegistrationCategoryErr, null);
                        } else {
                            done(null,inventoryCompanyRegistrationCategory);
                        }
                    });
                }
            });
        }
    });
}
/**
 *
 * @param eachNode
 * @param rootNode
 * @param done
 */
function findByTrackerEachNode(eachNode,pathNode,isForward,done) {

    fetchInventoryCompanyAndRegistrationCategory(eachNode.inventory.toString(),function (populateErr,inventory) {
        if (populateErr) {
            done(populateErr);
        } else {
            pathNode.inventory = inventory;
            pathNode.stockMasters = [];
            pathNode.destinations = [];
            async.forEachSeries(eachNode.stockMasters, function (stockMaster, stockCallBack) {
                if (stockMaster.stockMaster) {
                    StockMaster.findOne({
                        _id: stockMaster.stockMaster.toString()
                    }, '-offerUnits -saleUnitPrice -buyUnitPrice -updateHistory -numberOfUnitsHistory -offerUnitsHistory -buyOfferUnitsHistory -moqAndPrice -croppedInventoryImageURL2 -croppedInventoryImageURL3 -croppedInventoryImageURL4 -inventoryImageURL2 -inventoryImageURL3 -inventoryImageURL4').populate('user', 'company').exec(function (stockFindErr, eachStockMaster) {
                        if (stockFindErr) {
                            logger.error('Failed to get the stock with stockId:' + stockMaster.stockMaster.toString() + ' Error :' + JSON.stringify(eachStockMaster));
                            done(stockFindErr, eachNode, pathNode);
                        } else if (!eachStockMaster) {
                            logger.error('No Stock with Id' + stockMaster.stockMaster.toString());
                            done(new Error('No Stock with Id' + stockMaster.stockMaster.toString()), pathNode);
                        } else if (!eachStockMaster.user) {
                            logger.error('No Stock User with Id' + stockMaster.stockMaster.toString());
                            done(new Error('No Stock User with Id' + stockMaster.stockMaster.toString()), eachNode, pathNode);
                        }/* else if (!eachStockMaster.stockOwner) {
                        logger.error('No Stock Owner with Id' + stockMaster.stockMaster.toString() + 'for the batch Number ' + eachStockMaster.batchNumber);
                        done(new Error('No Stock Owner with Id' + stockMaster.stockMaster.toString()),eachNode, pathNode);
                    }*/ else {
                            logger.debug('Found while loading the stock master details at Inventory- ' + eachStockMaster._id);
                            if(isForward) {
                                if (eachStockMaster.stockOut) {
                                  if (eachStockMaster.stockOut.orders && eachStockMaster.stockOut.orders.length > 0 && eachStockMaster.stockOut.inventories.length > 0) {
                                        logger.error('Different type of sources are present in the same stock master ' + JSON.stringify(eachStockMaster));
                                        done(new Error('Different type of sources are present in the same stock master: '), eachNode, pathNode);

                                    }else if(eachStockMaster.stockOut.orders && eachStockMaster.stockOut.orders.length > 0) {
                                        // fetch order stock master
                                        logger.debug('fetch order stock master by stock By Id' + stockMaster.stockMaster.toString());
                                        findOrdersInventories(eachStockMaster, eachNode, pathNode,isForward, function (err, eachNode, pathNode) {
                                            done(err, eachNode, pathNode);
                                        });

                                    } else if (eachStockMaster.stockOut.inventories && eachStockMaster.stockOut.inventories.length > 0) {
                                        pathNode.stockMasters.push({
                                            count: stockMaster.count,
                                            stockMaster: eachStockMaster
                                        });
                                        logger.debug('Found stock Master :' + eachStockMaster._id + ' Batch number:' + eachStockMaster.batchNumber);
                                        // find stockIn Inventory stock Master
                                        findConversionInventories(eachStockMaster, eachNode, pathNode,isForward, function (conversionErr, eachNode, pathNode) {
                                            done(conversionErr, eachNode, pathNode);
                                        });


                                    } else if (eachStockMaster.stockOut.inventories.length === 0 && eachStockMaster.stockOut.orders.length === 0 && eachStockMaster.stockOut.other.length > 0) {
                                        pathNode.stockMasters.push({
                                            count: stockMaster.count,
                                            stockMaster: eachStockMaster
                                        });
                                        stockCallBack();
                                    } else {
                                        stockCallBack();
                                    }
                                } else {
                                    logger.error('No stockIn with stockId Object:' + JSON.stringify(eachStockMaster));
                                    done(new Error('No stockIn with stockId :' + eachStockMaster), eachNode, pathNode);
                                }
                            }else{
                                if (eachStockMaster.stockIn) {
                                    if (eachStockMaster.stockIn.orders && eachStockMaster.stockIn.orders.length > 0 && eachStockMaster.stockIn.inventories.length > 0) {
                                        logger.error('Different type of sources are present in the same stock master ' + JSON.stringify(eachStockMaster));
                                        done(new Error('Different type of sources are present in the same stock master: '), eachNode, pathNode);

                                    } else if (eachStockMaster.stockIn.orders && eachStockMaster.stockIn.orders.length > 0) {
                                        // fetch order stock master
                                        logger.debug('fetch order stock master by stock By Id' + stockMaster.stockMaster.toString());
                                        findOrdersInventories(eachStockMaster, eachNode, pathNode,isForward, function (err, eachNode, pathNode) {
                                            done(err, eachNode, pathNode);
                                        });

                                    } else if (eachStockMaster.stockIn.inventories && eachStockMaster.stockIn.inventories.length > 0) {
                                        pathNode.stockMasters.push({
                                            count: stockMaster.count,
                                            stockMaster: eachStockMaster
                                        });
                                        logger.debug('Found stock Master :' + eachStockMaster._id + ' Batch number:' + eachStockMaster.batchNumber);
                                        // find stockIn Inventory stock Master
                                        findConversionInventories(eachStockMaster, eachNode, pathNode,isForward, function (conversionErr, eachNode, pathNode) {
                                            done(conversionErr, eachNode, pathNode);
                                        });


                                    } else if (eachStockMaster.stockIn.inventories.length === 0 && eachStockMaster.stockIn.orders.length === 0 && eachStockMaster.stockIn.other.length > 0) {
                                        pathNode.stockMasters.push({
                                            count: stockMaster.count,
                                            stockMaster: eachStockMaster
                                        });
                                        stockCallBack();
                                    } else {
                                        stockCallBack();
                                    }
                                } else {
                                    logger.error('No stockIn with stockId Object:' + JSON.stringify(eachStockMaster));
                                    done(new Error('No stockIn with stockId :' + eachStockMaster), eachNode, pathNode);
                                }
                            }
                        }
                    });
                } else {
                    logger.error('No StockMaster stockId:' + JSON.stringify(stockMaster));
                    stockCallBack(new Error('No Stock Master Id'));
                }

            }, function (inventoryErr) {
                if (!inventoryErr) done(null, eachNode, pathNode);
                else done(inventoryErr, eachNode, pathNode);
            });
        }


    });

}

/**
 *  tracker paths
 * @param eachNode
 * @param pathNode
 * @param isForward : forward/backward tracker paths with flag
 * @param done
 */
exports.trackPaths=function(eachNode,pathNode,isForward,done){
    findByTrackerEachNode(eachNode,pathNode,isForward,function(err,eachNode,resultsNode){
        done(err,eachNode,resultsNode);
    });
};

