'use strict';

/**
 * Module dependencies.
 */
var https = require('https'),
    logger = require('../../../lib/log').getLogger('SMS', 'DEBUG'),
    config = require('../../../config/config');

/**
 * This util method sends the SMS to given mobileNumber with given message. The mobile number is 10 digit number.
 * The country code is fixed to india.
 * @param mobileNumber
 * @param message
 * @param done
 */
exports.sendSMS = function (mobileNumber, message, done) {
    logger.debug('SMS is Successfully sent to mobile-' + mobileNumber + '. Message-' + message);
    if (process.env.NODE_ENV === 'development' || (!config.production)) {
        done(null, 'SMS is Successfully sent. Message-' + message);
    } else {
        var data = JSON.stringify({
            api_key: config.sms.apiKey,
            api_secret: config.sms.apiSecret,
            to: '91' + mobileNumber,
            from: config.sms.fromMobileNumber,
            text: message
        });

        var options = {
            host: 'rest.nexmo.com',
            path: '/sms/json',
            port: 443,
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
                'Content-Length': Buffer.byteLength(data)
            }
        };

        var req = https.request(options);

        req.write(data);
        req.end();

        var responseData = '';
        req.on('response', function (res) {
            res.on('data', function (chunk) {
                responseData += chunk;
            });
            res.on('end', function () {
                done(null, responseData);
            });
        });
    }

};

/**
 * This function is used for sending the OTP to the mobile number.
 * @param mobileNumber
 * @param otp
 * @param done
 */

exports.sendOTP = function (mobileNumber, otp, done) {
    var otpMessage = 'Hello, ' + otp + ' is your one time password for activating nVipani account.';
    this.sendSMS(mobileNumber, otpMessage, function (err, response) {
        done(err, response);
    });
};

/**
 * This function is used for sending the URL to the mobile number.
 * @param mobileNumber
 * @param url
 * @param done
 */
exports.sendURL = function (mobileNumber, url, done) {
    var otpMessage = 'Hello, ' + url + ' is your activation link for activating nVipani account.';
    this.sendSMS(mobileNumber, otpMessage, function (err, response) {
        done(err, response);
    });
};

