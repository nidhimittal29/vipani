'use strict';

/**
 * Module dependencies.
 */
var jsreport = require('jsreport'),
    fs = require('fs'),
    async = require('async'),
    path = require('path'),
    logger = require('../../../lib/log').getLogger('REPORTS', 'DEBUG'),
    config = require('../../../config/config'),
    mongoose = require('mongoose'),
    moment = require('moment'),
    numberToWords = require('number-to-words'),
    Counter = mongoose.model('Counter');


/**
 * Generates Invoice report for the given order.
 * @param order - Order for which the invoice needs to be generated
 * @param done - Callback with updated order
 */
function getGSTInvoice(sellerCompany,buyerCompany) {
    return  (sellerCompany.gstinNumber || sellerCompany.gstinNumber!==undefined ) && sellerCompany.gstinNumber.length>2 && (buyerCompany.gstinNumber || buyerCompany.gstinNumber!==undefined )&& buyerCompany.gstinNumber.length>2 && sellerCompany.gstinNumber.substring(0,2)!==buyerCompany.gstinNumber.substring(0,2) ;
}
function paymentInvoicePathOption(paymentTerm,done) {

    if(paymentTerm.type==='Installments') {
        var paidLists = paymentTerm.installmentPaymentDetails.filter(function (eachPayment) {
            return eachPayment.status === 'Paid';

        });
        var notPaidLists = paymentTerm.installmentPaymentDetails.filter(function (eachPayment) {
            return !eachPayment.status || eachPayment.status.length === 0 || eachPayment.status === 'YetToPay' || (eachPayment.status !== 'YetToPay' && eachPayment.invoiceFile.length === 0);

        });
        if (paidLists.length === paymentTerm.installmentPaymentDetails.length) {
            done(null, paidLists[paidLists.length - 1]);
        } else if (notPaidLists && notPaidLists.length > 0) {
            done(null, notPaidLists[0]);
        } else {
            done(new Error('No Path'), null);
        }
    }else{
        done(null,paymentTerm.paymentDetails);

    }

}
function generateInvoiceNumber (order,done) {
    var counterUtil = require('./common.counter.util');
    counterUtil.getNextInvoiceSequenceNumber(order, function (err, invoiceSeqNumber) {

        var date = new Date();
        var dd = '' + date.getDate();
        var mm = '' + (date.getMonth() + 1);
        var yyyy = '' + date.getFullYear();

        if (mm.length < 2) {
            mm = '0' + mm;
        }
        if (dd.length < 2) {
            dd = '0' + dd;
        }
        done(null, order.owner.companyCode+''+order.owner.businessUnitCode+'-'+dd + mm + yyyy + '-' + counterUtil.leftPadWithZeros(invoiceSeqNumber, 5));
    });
}
exports.generateInvoiceReport = function (order,regenerate,sellerCompany,buyerCompany, done) {
    generateInvoiceNumber(order,function (error,invoiceNumber) {
        paymentInvoicePathOption(order.applicablePaymentTerm,function (pathErr,pathObject) {
            if(pathErr){
                done(pathErr,null);
            }else {

                invoiceNumber = regenerate ? pathObject.invoiceNo : invoiceNumber;
                if (regenerate) {
                    order.invoice.number = pathObject.invoiceNo;
                    order.invoice.invoiceDate = pathObject.invoiceDate;
                } else {
                    order.invoice.number = invoiceNumber;
                    order.invoice.invoiceDate = new Date();
                }
                var x = order.totalAmount.toFixed(2).toString().split('.');
                /*var decimals = (x - Math.floor(x)).toFixed(2);*/

                var totalString = numberToWords.toWords(x[0]) + ' Rupees';
                if (x.length === 2 && x[1] !== 0) {
                    totalString += ' and ' + numberToWords.toWords(x[1]) + ' paisa';
                }
                totalString += ' Only';
                /*
                                if (err) {
                                    done(err);
                                } else {*/
                jsreport.renderDefaults.tempDirectory = config.reportLocation;
                jsreport.renderDefaults.dataDirectory = config.reportLocation;
                order.isInterState = getGSTInvoice(sellerCompany, buyerCompany);
                var content = fs.readFileSync(path.join(__dirname, (order.isInterState ? '../../views/templates/invoice.html' : '../../views/templates/invoice-1.html')), 'utf8');
                jsreport.render({
                    template: {
                        content: content,
                        phantom: {
                            customPhantomJS: true,
                            /* orientation: 'portrait',*/
                            orientation: 'portrait',
                            footer: '<div  style="text-align:center;font-size: 10px;"><div>Powered by nVipani</div></div>'

                        },
                        //helpers: 'function total(inventory){return inventory.numberOfUnits * inventory.unitPrice};function getCategories(data) {var categories = {};data.products.forEach(function(i) {categories[i.inventory] = categories[i.inventory];});data.products=categories;return _.values(data);}',
                        helpers: 'function subtotal(units,price){return (units*price).toFixed(2);};function eachmrp(mrp,unitprice){if(mrp && mrp>0){return mrp.toFixed(2);}else{return unitprice.toFixed(2);}};function specificaddress(companyAddresses,pincode,state,city){var retAddress="";if(companyAddresses && companyAddresses.length>0){companyAddresses.forEach(function (address){if(pincode){ retAddress =address.pinCode;}else if(state){retAddress =address.state;}else if(city){ retAddress =address.city;}; return true;});}return retAddress;};function totalsgstpprice(products){var sgstprice = 0;products.forEach(function (product){if(product.numberOfUnits && product.inventory.taxGroup && product.inventory.taxGroup.SGST){sgstprice+=product.inventory.taxGroup.SGST *(product.numberOfUnits * product.unitPrice/100);}});return sgstprice.toFixed(2)};function eachtotal(units,price,inventory,inter){var eachsum=0;if(inter&&inventory.taxGroup &&inventory.taxGroup.CGST && inventory.taxGroup.SGST){eachsum=units*price+(((units*price)*inventory.taxGroup.CGST)/100) + (((units*price)*inventory.taxGroup.SGST)/100);}else{if(inventory.taxGroup&&inventory.taxGroup.IGST)eachsum=units*price+(((units*price)*inventory.taxGroup.IGST)/100)} return eachsum.toFixed(2); };function totalmrpprice(products){var mrpprice = 0;products.forEach(function (product){if(product.numberOfUnits){if(product.unitMrpPrice && product.unitMrpPrice>0){mrpprice+=product.numberOfUnits*(product.unitMrpPrice);}else{mrpprice+=product.numberOfUnits*(product.inventory.unitPrice);}}});return mrpprice.toFixed(2)};function discounttotal(units,mrp,inventoryprice,price){mrp=eachmrp(mrp,inventoryprice);return (units*(mrp-price)).toFixed(2);};function discountttotal(products){var discount = 0;products.forEach(function (product){if(product.numberOfUnits){if(product.unitMrpPrice && product.unitMrpPrice>0){discount+=product.numberOfUnits*(product.unitMrpPrice-product.unitPrice);}else{discount+=product.numberOfUnits*(product.inventory.unitPrice-product.unitPrice);}}});return discount.toFixed(2);};function quantities(products){var quantity = 0;products.forEach(function (product){if(product.numberOfUnits){quantity+=product.numberOfUnits;}});return quantity};function sequence(val){return val+1; };function phones(companyPhone){var retPhone = ""; if(companyPhone && companyPhone.length>0){companyPhone.forEach(function (phone){ if(phone.phoneNumber){ retPhone = phone.phoneNumber; return true;}});} return retPhone;};function emails(companyEmail){var retEmail=""; if(companyEmail && companyEmail.length>0){companyEmail.forEach(function (email){ if(email.email){ retEmail = email.email; return true;}});}return retEmail;};function addresses(companyAddresses){var retAddress=""; if(companyAddresses && companyAddresses.length>0){companyAddresses.forEach(function (address){ if(address.pinCode){ retAddress = address.addressLine +", "+address.city+", "+address.state+"-"+address.pinCode+", "+address.country; return true;}});} return retAddress;};function total(numberOfUnits,unitPrice,inventoryPrice){return (numberOfUnits * eachmrp(unitPrice,inventoryPrice)).toFixed(2)};function grandnogsttotal(products){var gtotal = 0;products.forEach(function (product){gtotal +=product.numberOfUnits * product.unitPrice;}); return gtotal.toFixed(2)}function grandgsttotal(products,inter){var gtotal = 0;products.forEach(function (product){gtotal +=product.numberOfUnits * product.unitPrice;if(inter){if(product.inventory.taxGroup&&product.inventory.taxGroup.IGST!==0){gtotal +=product.inventory.taxGroup.IGST *(product.numberOfUnits * product.unitPrice/100);}}else {if(product.inventory.taxGroup&&product.inventory.taxGroup.SGST!==0){gtotal +=product.inventory.taxGroup.SGST *(product.numberOfUnits * product.unitPrice/100);}if(product.inventory.taxGroup &&product.inventory.taxGroup.CGST!==0){gtotal +=product.inventory.taxGroup.CGST *(product.numberOfUnits * product.unitPrice/100);}}});return gtotal.toFixed(2) };function grandsgstvsgstvigst(products,cgst,sgst,igst){var sgsttotal = 0;products.forEach(function (product){if(cgst && product.inventory.taxGroup && product.inventory.taxGroup.CGST!=0){sgsttotal +=product.inventory.taxGroup.CGST *(product.numberOfUnits * product.unitPrice/100);} if(sgst &&  product.inventory.taxGroup &&product.inventory.taxGroup.SGST!=0){sgsttotal +=product.inventory.taxGroup.SGST *(product.numberOfUnits * product.unitPrice/100);} if(igst && product.inventory.taxGroup && product.inventory.taxGroup.IGST!==0){sgsttotal +=product.inventory.taxGroup.IGST *(product.numberOfUnits * product.unitPrice/100);}});return sgsttotal.toFixed(2) };function grandsgst(products){var sgst = 0;products.forEach(function (product){if(product.inventory.taxGroup &&product.inventory.taxGroup.SGST!==0){sgst +=product.inventory.taxGroup.SGST *(product.numberOfUnits * product.unitPrice/100);}});return sgst.toFixed(2) };function twodecimals(amount){return amount.toFixed(2)};function smgstvscgst(products){var cgstvssgst = 0;products.forEach(function (product){if(product.inventory.taxGroup && product.inventory.taxGroup.CGST!==0){cgstvssgst +=product.inventory.taxGroup.CGST *(product.numberOfUnits * product.unitPrice/100);}if(product.inventory.taxGroup && product.inventory.taxGroup.SGST!==0){cgstvssgst +=product.inventory.taxGroup.SGST *(product.numberOfUnits * product.unitPrice/100);}});return cgstvssgst.toFixed(2) };function gsttotal(gst,units,price){return ((gst*units*price)/100).toFixed(2);}',
                        engine: 'handlebars',
                        recipe: 'phantom-pdf'
                    },
                    data: {
                        order: order,
                        invoiceNumber: invoiceNumber,
                        sellercompany: sellerCompany,
                        buyercompany: buyerCompany,
                        totalAmount: totalString,
                        placedFormat: moment(order.created).format('DD-MM-YYYY'),
                        invoiceFormat: moment(order.invoice.invoiceDate).format('DD-MM-YYYY')
                    }

                }).then(function (resp) {
                    var date = new Date();
                    var dd = '' + date.getDate();
                    var mm = '' + (date.getMonth() + 1);
                    var yyyy = '' + date.getFullYear();
                    var hour = date.getHours();
                    hour = (hour < 10 ? '0' : '') + hour;

                    var min = date.getMinutes();
                    min = (min < 10 ? '0' : '') + min;

                    var sec = date.getSeconds();
                    sec = (sec < 10 ? '0' : '') + sec;


                    if (mm.length < 2) {
                        mm = '0' + mm;
                    }
                    if (dd.length < 2) {
                        dd = '0' + dd;
                    }
                    var name = order.orderNumber + '-' + yyyy + '-' + mm + '-' + dd + '-' + hour + '-' + min + '-' + sec;
                    var reportFilePath = config.reportLocation + 'data/' + name + '.pdf';
                    resp.result.pipe(fs.createWriteStream(reportFilePath));

                    logger.debug('Finished Invoice Report -' + reportFilePath + ' Generation for the order with order number - ' + order.orderNumber);

                    if (order.applicablePaymentTerm.type==='Installments' && order.applicablePaymentTerm.installmentPaymentDetails && order.applicablePaymentTerm.installmentPaymentDetails.length > 0) {
                        var index=order.applicablePaymentTerm.installmentPaymentDetails.indexOf(pathObject);
                        order.applicablePaymentTerm.installmentPaymentDetails[index].invoiceNo = invoiceNumber;
                        order.applicablePaymentTerm.installmentPaymentDetails[index].invoiceDate = regenerate ? pathObject.invoiceDate : Date.now();
                        order.applicablePaymentTerm.installmentPaymentDetails[index].invoiceFile = reportFilePath;
                        done(null, order);
                    }else{
                        order.applicablePaymentTerm.paymentDetails.invoiceNo=invoiceNumber;
                        order.applicablePaymentTerm.paymentDetails.invoiceDate = regenerate ? pathObject.invoiceDate : Date.now();
                        order.applicablePaymentTerm.paymentDetails.invoiceFile = reportFilePath;
                        done(null, order);
                    }/* else {
                            done('No order fields', null);
                        }*/


                }).catch(function (e) {
                    logger.error('Error while generating invoice report for the order with order number - ' + order.orderNumber, e);
                    done('Error while generating invoice report for the order with order number - ' + order.orderNumber, e);
                });

            }
        });
    });

};



exports.generateStockReport = function (products, report,company, done) {

    /* jsreport.renderDefaults.tempDirectory = config.reportLocation;
     jsreport.renderDefaults.dataDirectory = config.reportLocation;
     var content  = fs.readFileSync(path.join(__dirname, '../../views/templates/stockreport.html'), 'utf8');
     if(report.reportType==='Sales' || report.reportType==='Purchases' || report.reportType==='SalesAndPurchase' ){
         content  = fs.readFileSync(path.join(__dirname, '../../views/templates/salesreport.html'), 'utf8');
     }*/

    async.each(report.reports, function (eachReport, callback) {
        jsreport.renderDefaults.tempDirectory = config.reportLocation;
        jsreport.renderDefaults.dataDirectory = config.reportLocation;
        var content  = fs.readFileSync(path.join(__dirname, (eachReport.format === 'PDF' ? '../../views/templates/stockreport.html':'../../views/templates/stockreport.html')), 'utf8');
        if(report.reportType==='Sales' || report.reportType==='Purchases' || report.reportType==='SalesAndPurchase' ) {
            content = fs.readFileSync(path.join(__dirname, '../../views/templates/salesreport.html'), 'utf8');
        }


        var template= {
            content: content,
            //helpers: 'function total(inventory){return inventory.numberOfUnits * inventory.unitPrice};function getCategories(data) {var categories = {};data.products.forEach(function(i) {categories[i.inventory] = categories[i.inventory];});data.products=categories;return _.values(data);}',
            helpers: 'function phones(companyPhone){var retPhone = ""; if(companyPhone && companyPhone.length>0){companyPhone.forEach(function (phone){ if(phone.phoneNumber){ retPhone = phone.phoneNumber; return true;}});} return retPhone;};function emails(companyEmail){var retEmail=""; if(companyEmail && companyEmail.length>0){companyEmail.forEach(function (email){ if(email.email){ retEmail = email.email; return true;}});}return retEmail;};function addresses(companyAddresses){var retAddress=""; if(companyAddresses && companyAddresses.length>0){companyAddresses.forEach(function (address){ if(address.pinCode){ retAddress = address.addressLine +", "+address.city+", "+address.state+"-"+address.pinCode+", "+address.country; return true;}});} return retAddress;};function vatprice(vat,numberOfUnits, unitPrice){ return (vat *numberOfUnits * unitPrice)/100 };function total(numberOfUnits, unitPrice){return numberOfUnits * unitPrice};function counters(counter){  return counter+1; };',
            engine: 'handlebars',
            recipe: (eachReport.format === 'XLS' ? 'html-to-xlsx' : 'phantom-pdf')
        };
        if(eachReport.format === 'PDF') {
            template.phantom= {
                customPhantomJS: true, /*
                     header:'<header><div  style="text-align:center"><h3 style="font-size: 12px;">Sales Report</h3></div></header>',*/
                footer: '<div  style="text-align:center;font-size: 10px;"><div>Powered by nVipani</div></div>',
                orientation: 'portrait'
            };
        }


        jsreport.render({
            template: template,
            data: {
                products: products,
                report:report,
                company:company
            }

        }).then(function (resp) {
            var date = report.startDate;
            var dd = '' + date.getDate();
            var mm = '' + (date.getMonth() + 1);
            var yyyy = '' + date.getFullYear();
            var reportFormat = eachReport.format;

            if (mm.length < 2) {
                mm = '0' + mm;
            }
            if (dd.length < 2) {
                dd = '0' + dd;
            }
            if (eachReport.format === 'XLS') {
                reportFormat = 'xlsx';
            }

            var name;
            if (report.reportType === 'Sales' || report.reportType === 'Purchases' || report.reportType === 'SalesAndPurchase') {
                name = company.name + '-' + report.reportType + '-' + yyyy + '-' + mm;
            } else {
                name = company.name + '-' + report.reportType + '-' + yyyy + '-' + mm + '-' + dd;
            }

            var reportFilePath = config.reportLocation + 'data/' + name + '.' + reportFormat;
            resp.result.pipe(fs.createWriteStream(reportFilePath));

            eachReport.name = name + '.' + reportFormat;
            eachReport.location = reportFilePath;
            callback();
        }).catch(function (e) {
            logger.error('Error while generating stock report - ', e);
            callback('Error while generating stock report - ', e);
        });
    }, function (err) {
        if (err) {
            done(err);
        } else {
            done(null, report,company);
        }
    });
};
