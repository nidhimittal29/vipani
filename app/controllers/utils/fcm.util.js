'use strict';

/**
 * Module dependencies.
 */
var request = require('request'),
    config = require('../../../config/config');

/**
 * This util method sends the Push notificaiton to given device token with given message using Firebase Cloud Messaging.
 * @param devicetoken - devicetoken to which the notification needs to be sent
 * @param title - Notification title
 * @param message - Notificaiton message
 * @param data - Notification data as key value pair
 * @param done - Callback
 */

exports.sendMessageToUser = function (devicetoken, title, message, data, done) {
    var notifications=data;
    if(data.type.indexOf('Order')===0){
        notifications={order:data.source.order};
    }
    else if(data.type.indexOf('Offer')===0){
        notifications={offer:data.source.offer};
    }
    request({
        url: config.fcm.url,
        method: 'POST',
        headers: {
            'Content-Type': ' application/json',
            'Authorization': 'key='+config.fcm.cloudmessagekey    //will get from FCM console
        },
        body: JSON.stringify(
            {
                'notification': {
                    title: title,
                    body: message
                },
                'data': notifications,
                'to': devicetoken  //will get from android POST
            }
        )
    }, function (error, response, body) {
        if (error) {
            done(error, response, body);
        }
        else if (response.statusCode >= 400) {
            done(new Error('HTTP Error: ' + response.statusCode + ' - ' + response.statusMessage + '\n'), response, body);
        }
        else {
            done(null, response, body);
        }
    });
};

