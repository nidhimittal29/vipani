'use strict';
var mongoose = require('mongoose'),
    Inventory = mongoose.model('Inventory'),
    ItemMaster = mongoose.model('ItemMaster'),
    StockMaster = mongoose.model('StockMaster'),
    ProductBrand = mongoose.model('ProductBrand'),
    errorHandler = require('../errors.server.controller'),
    Company = mongoose.model('Company'),
    dbUtil = require('./common.db.util'),
    businessUnitUtil = require('./common.businessunit.util'),
    commonUtil = require('./common.util'),
    globalUtil = require('./common.global.util'),
    BusinessUnit = mongoose.model('BusinessUnit'),
    UnitOfMeasure = mongoose.model('UnitOfMeasure'),
    logger = require('../../../lib/log').getLogger('INVENTORY', 'DEBUG'),
    _ = require('lodash'),
    _this=this,
    async = require('async');


/**
 * find a Category with id with populate fields
 */
/**
 * populateLevels 2 means populate only two levels with Item Masters ,Stock Master ,Company
 * populateLevels 3 means populate only three levels with Brand Master
 * populateLevels 4 means populate only four levels with Product Master
 */
function nestedProductPopulate(inventory,populateLevels,done) {
    Inventory.populate(inventory, [{
        path: 'itemMaster',
        model: 'ItemMaster'
    },{
        path: 'user.company',
        model: 'Company',
        select: 'name profileUrl profileImageURL addresses'
    }], function (nestedErr, populatedStockMasterInventory) {
        if (nestedErr) {
            done(nestedErr);
        } else {
            if( populateLevels>=3) {
                Inventory.populate(populatedStockMasterInventory, [{
                    path: 'itemMaster.productBrand',
                    model: 'ProductBrand'
                }], function (nestedBrandErr, populatedBrandInventory) {
                    if (nestedBrandErr) {
                        done(nestedBrandErr);
                    } else {
                        if( populateLevels===4) {
                            Inventory.populate(populatedBrandInventory, [{
                                path: 'itemMaster.productBrand.productCategory',
                                model: 'Category'
                            }], function (nestedCategoryErr, populatedBrandCategoryOffers) {
                                if (nestedCategoryErr) {
                                    done(nestedCategoryErr);
                                } else {
                                    done(populatedBrandCategoryOffers);
                                }
                            });
                        }else{
                            done(populatedBrandInventory);
                        }
                    }
                });
            }else{
                done(populatedStockMasterInventory);
            }
        }
    });
}

exports.findInventoryById = function(id,populateLevels,done){
    id=id.toString();
    if(populateLevels===-1){
        Inventory.findById(id).exec(function (inventoryError, inventory) {
            if (inventoryError) {
                done(inventoryError);
            }else if(!inventory){
                done(new Error('No Inventory with the Inventory Id:'+id));
            } else {
                done(inventory);
            }
        });
    }else if (populateLevels >= 1) {
        Inventory.findById(id).populate('user', 'displayName company').populate('stockMasters hsncode taxGroup unitOfMeasure productBrand').exec(function (err, inventory) {
            if (err) {
                //   logger.error('Error while loading the inventory details with inventory id- ' + id);
                done(err);
            }else if(!inventory){
                done(new Error('No Inventory with the Inventory Id:'+id));
            } else {
                if (populateLevels >=2) {
                    nestedProductPopulate(inventory, populateLevels, function (populateInventory) {
                        done(populateInventory);

                    });
                } else {
                    done(inventory);
                }
            }
        });
    }
};
/*
 * find Offer with populate tables
 * populateLevels -2 means -No population table
 * populateLevels -1 means populate for the old Apps
 * populateLevels 0 means populate with pagination
 * populateLevels 1 means populate only one level
 * populateLevels 2 means populate only two levels with Item Masters ,Stock Master ,Company
 * populateLevels 3 means populate only three levels with Brand Master
 * populateLevels 4 means populate only four levels with Product Master
 */
exports.findQueryByInventories =function(query,populateLevels,isFilterSummary,pageOptions, done) {
    //Load company and then the inventories for the default business unit if not business unit it specified.
    query.push({deleted:{$exists: true}});
    query.push({deleted:false});
    if(populateLevels===-3) {
        Inventory.find({$and:query},'-offerUnits -saleUnitPrice -buyUnitPrice -updateHistory -numberOfUnitsHistory -offerUnitsHistory -buyOfferUnitsHistory -moqAndPrice -croppedInventoryImageURL2 -croppedInventoryImageURL3 -croppedInventoryImageURL4 ').sort('-created').populate('user', 'displayName').populate('stockMasters hsncode taxGroup unitOfMeasure').exec(function (err, inventoriesObject) {
            done(err,inventoriesObject);

        });
    }else if(populateLevels===-2) {
        Inventory.find({$and:query}).exec(function (errInventory, product) {
            if(errInventory){
                done(errInventory);
            }else {
                done(product);
            }

        });
    }else if(populateLevels===-1) {
        Inventory.find({$and:query},'-offerUnits -saleUnitPrice -buyUnitPrice -updateHistory -numberOfUnitsHistory -offerUnitsHistory -buyOfferUnitsHistory -moqAndPrice -croppedInventoryImageURL2 -croppedInventoryImageURL3 -croppedInventoryImageURL4').sort('-created').populate('user', 'displayName').exec(function (err, inventoriesObject) {
            done(err,inventoriesObject);

        });
    }else if(populateLevels===0) {
        Inventory.count({$and:query},function(err,count){
            Inventory.find({$and:query},'-offerUnits -saleUnitPrice -buyUnitPrice -updateHistory -numberOfUnitsHistory -offerUnitsHistory -buyOfferUnitsHistory -moqAndPrice -croppedInventoryImageURL2 -croppedInventoryImageURL3 -croppedInventoryImageURL4 -inventoryImageURL2 -inventoryImageURL3 -inventoryImageURL4').skip(pageOptions.page > 0 ? ((pageOptions.page-1) * pageOptions.limit) : 0 ).limit(pageOptions.limit)
                .sort('-created').populate('user', 'displayName').exec(function (err, inventoriesObject) {
                if (err) {
                    done(err);
                } else {
                    if(isFilterSummary && (!pageOptions.page || pageOptions.page === 1)){
                        _this.findQueryByInventories(query,-1,null,null,function (err,inventory) {
                            if(inventory) {
                                _this.getInventorySummary(inventory,null, function (summaryData) {
                                    done({inventory: inventoriesObject, total_count: count,summaryData:summaryData});
                                });
                            }else {
                                done(null);
                            }
                        });
                    }else {
                        done({inventory: inventoriesObject, total_count: count});
                    }
                }
            });
        });
    }
};
function createItemMaster(itemMaster,productBrand,owner,done){
    itemMaster=new ItemMaster(itemMaster);
    itemMaster.productBrand=productBrand;
    itemMaster.user=owner.user;
    itemMaster.company=owner.user.company;
    itemMaster.businessUnit=owner.businessUnit;
    if (itemMaster.productBrand) {
        logger.debug('before failing:'+itemMaster.productBrand);
        logger.debug('before failing:'+JSON.stringify(itemMaster.productBrand));
        logger.debug('Started Item Master product using product Brand- ' + JSON.stringify(productBrand));
        itemMaster.save(function (err,saveInventory) {
            if (err) {
                done(err,null);
            } else {
                done(null,saveInventory);
            }
        });

    }
}


function findOrCreateItemMaster(inventory,productBrand,owner,done) {
    // we need to create Compound UOM's
    /**
     *  Find existing Item master for each product Brand
     */
    if(productBrand && productBrand._id &&inventory&&inventory.unitOfMeasure) {
        ItemMaster.findOne({
            'productBrand': productBrand._id,
            'unitOfMeasure': inventory.unitOfMeasure,
            'deleted': false
        }, function (itemMasterErr, itemMaster) {

            if (itemMasterErr) {
                logger.error(errorHandler.getErrorMessage(itemMasterErr));
                done(itemMasterErr, null);
            } else if (itemMaster) {
                logger.warn('Item master with product brand name - ' + productBrand.name + ', brand variety -' + productBrand.variety + ' and unit of measure -' + inventory.unitOfMeasure + ' already present');
                done(null, itemMaster);
            } else {
                logger.debug('Creating Item Master with product brand name - ' + productBrand.name + ', brand variety -' + productBrand.variety + ' and unit of measure -' + inventory.unitOfMeasure);
                createItemMaster(inventory, productBrand, owner, function (createItemMasterErr, createItemMaster) {
                    done(createItemMasterErr, createItemMaster);
                });
            }
        });
    }else{
        logger.error('No Item Master with the specified Value for Product Brand name'+JSON.stringify(productBrand)+' or unit of measure'+JSON.stringify(inventory.unitOfMeasure));
        done(new Error('No Item Master with the specified Value for Product Brand name or unit of measure'),null);
    }


}
function getUOMS (category,unitMeasure,isNew) {
    if(!category.unitOfMeasures || category.unitOfMeasures===null){
        category.unitOfMeasures=[];
    }
    if(isNew && unitMeasure) {
        category.unitOfMeasures.push(unitMeasure._id);
    }else {
        var oldUOM=category.unitOfMeasures.filter(function (eachUOM) {
            return eachUOM._id===unitMeasure._id;
        });
        if(oldUOM.length===0){
            category.unitOfMeasures.push(unitMeasure._id);
        }
    }
    return category;

}
function getUOMBrands(brand,unitMeasure,isNew) {
    if(!brand.unitOfMeasures || brand.unitOfMeasures===null){
        brand.unitOfMeasures=[];
    }
    if(isNew && unitMeasure) {
        brand.unitOfMeasures.push(unitMeasure._id);
    }else {
        var oldUOM=brand.unitOfMeasures.filter(function (eachUOM) {
            return eachUOM._id===unitMeasure._id;
        });
        if(oldUOM.length===0){
            brand.unitOfMeasures.push(unitMeasure._id);
        }
    }
    return brand;

}
function findOrCreateCompoundUOM(firstUnitOfMeasure,secondUnitOfMeasure,conversion,user,done) {
    UnitOfMeasure.findOne({
        'firstUnitOfMeasure': firstUnitOfMeasure._id,
        'secondUnitOfMeasure': secondUnitOfMeasure._id,
        'conversion': conversion,
        'type': 'Compound'
    }, function (unitOfMeasureErr, unitOfMeasure) {
        if (unitOfMeasureErr) {
            logger.error(errorHandler.getErrorMessage(unitOfMeasureErr));
            done(unitOfMeasureErr);
        } else if (unitOfMeasure) {
            done(null,unitOfMeasure);
        }else{
            unitOfMeasure = new UnitOfMeasure();
            var date = Date.now();
            unitOfMeasure.set('created', date);
            unitOfMeasure.set('lastUpdated', date);
            unitOfMeasure.user = user._id;
            unitOfMeasure.lastUpdatedUser = user._id;
            unitOfMeasure.name = firstUnitOfMeasure.symbol + ' Of ' + conversion + secondUnitOfMeasure.symbol;
            unitOfMeasure.description = firstUnitOfMeasure.symbol + ' Of ' + conversion + secondUnitOfMeasure.symbol;
            unitOfMeasure.symbol = firstUnitOfMeasure.symbol + ' (' + conversion + secondUnitOfMeasure.symbol + ')';
            unitOfMeasure.quantityType = secondUnitOfMeasure.quantityType;
            unitOfMeasure.firstUnitOfMeasure = firstUnitOfMeasure._id;
            unitOfMeasure.secondUnitOfMeasure = secondUnitOfMeasure._id;
            unitOfMeasure.conversion = conversion;
            unitOfMeasure.type = 'Compound';
            unitOfMeasure.save(function (unitOfMeasureErr) {
                if (unitOfMeasureErr) {
                    done(unitOfMeasureErr);
                } else {
                    done(null, unitOfMeasure);
                }
            });
        }
    });

}
function findOrCreateUOM(firstUOM, conversion, secondUOM, user,productBrand, done) {
    logger.debug('First UOM-' + firstUOM);
    logger.debug('Conversion-' + conversion);
    logger.debug('Second UOM-' + secondUOM);
    UnitOfMeasure.findOne({
        '_id': firstUOM,
        'type': 'Simple'
    }, function (firstUnitOfMeasureError, firstUnitOfMeasure) {
        if (firstUnitOfMeasureError) {
            logger.error(errorHandler.getErrorMessage(firstUnitOfMeasureError));
            done(firstUnitOfMeasureError);
        } else if (!firstUnitOfMeasure) {
            logger.error('No unit of measure with symbol -' + firstUOM + ', type - Simple');
            done(new Error('No unit of measure with symbol -' + firstUOM + ', type - Simple'));
        } else {
            UnitOfMeasure.findOne({
                '_id': secondUOM,
                'type': 'Simple'
            }, function (secondUnitOfMeasureError, secondUnitOfMeasure) {
                if (secondUnitOfMeasureError) {
                    logger.error(errorHandler.getErrorMessage(secondUnitOfMeasureError));
                    done(secondUnitOfMeasureError);
                } else if (secondUnitOfMeasure) {
                    findOrCreateCompoundUOM(firstUnitOfMeasure,secondUnitOfMeasure,conversion,user,function (compoundUOMErr,compoundUOM) {
                        if(compoundUOMErr){
                            done(compoundUOMErr,null);
                        }else if(compoundUOM) {
                            dbUtil.updateProductBrandUOM(productBrand._id, productBrand.productCategory._id, compoundUOM._id, function (errUOMSave, finalId) {
                                done(errUOMSave, compoundUOM);
                            });
                        }else{
                            done(new Error('No Converted Unit of Measure'),null);
                        }
                    });
                } else {
                    logger.error('No unit of measure with symbol -' + secondUOM + ', type - Simple');
                    done(new Error('No unit of measure with symbol -' + secondUOM + ', type - Simple'));
                }
            });
        }
    });

}

function getProductBrand(productBrandId,uom,user,done){
    var productBrand={};

    ProductBrand.findById(productBrandId).populate('user', 'displayName').populate('productCategory hsncode taxGroup').exec(function (errPopulateBrand, singleProductBrand) {
        if (errPopulateBrand) {
            done(errPopulateBrand, null);
        }else if(!singleProductBrand){
            done(new Error('No Brand With the specified Brand Name '+productBrandId), null);
        }else if(singleProductBrand && (!singleProductBrand.hsncode ||(singleProductBrand.hsncode && !singleProductBrand.hsncode._id))){
            logger.error('No Hsncode  specified at product : '+JSON.stringify(productBrand));
            done(new Error('No Hsncode  specified at product'), null);
        }else if(singleProductBrand && (!singleProductBrand.taxGroup ||(singleProductBrand.taxGroup && !singleProductBrand.taxGroup._id))){
            logger.error('No Hsncode specified at product : '+JSON.stringify(productBrand));
            done(new Error('No taxGroup specified at product'), null);
        }else {
            if(uom){
                findOrCreateUOM(uom.firstUnitOfMeasure,uom.conversion,uom.secondUnitOfMeasure,user,singleProductBrand,function (uomErr,unitOfMeasure) {
                    if(uomErr){
                        done(uomErr, null);
                    }else if(!unitOfMeasure) {
                        logger.error('Started inventory product using uom'+ JSON.stringify(uom));
                        done(new Error('No Proper UOM'),null);
                    }else{
                        ProductBrand.findById(productBrandId).populate('user', 'displayName').populate('productCategory hsncode taxGroup').exec(function (errPopulateBrands, singleProductUOMBrand) {
                            if (errPopulateBrands) {
                                done(errPopulateBrands, null);
                            } else {
                                ProductBrand.populate(singleProductBrand, [/*{ path: 'unitOfMeasure.firstUnitOfMeasure unitOfMeasure.secondUnitOfMeasure',
                model: 'UnitOfMeasure'},*/{
                                    path: 'productCategory.parent',
                                    model: 'Category'
                                }, {
                                    path: 'productCategory.grandParent',
                                    model: 'Category'
                                }], function (errorPopulateUOM, singleProductUOMBrand) {
                                    if (errorPopulateUOM) {
                                        done(errorPopulateUOM, null);
                                    } else {
                                        productBrand.brand = singleProductUOMBrand;
                                        productBrand.uom = unitOfMeasure._id;
                                        done(errorPopulateUOM, productBrand);
                                    }

                                });
                            }

                        });

                    }
                });


            }else {
                ProductBrand.populate(singleProductBrand, [/*{ path: 'unitOfMeasure.firstUnitOfMeasure unitOfMeasure.secondUnitOfMeasure',
                model: 'UnitOfMeasure'},*/{
                    path: 'productCategory.parent',
                    model: 'Category'
                }, {
                    path: 'productCategory.grandParent',
                    model: 'Category'
                }], function (errorPopulateUOM, singleProductBrandWithUOM) {
                    if (errorPopulateUOM) {
                        done(errorPopulateUOM, null);
                    } else {
                        productBrand.brand = singleProductBrandWithUOM;
                        done(errorPopulateUOM, productBrand);
                    }

                });
            }

        }
    });

}

function addStockMasterData(stockMaster,inventory,stockType,owner,isNewStock,done){
    stockMaster=new StockMaster(stockMaster);
    stockMaster.inventory=inventory._id;
    stockMaster.user=owner.user;
    stockMaster.stockOwner=(inventory.owner?inventory.owner:owner);
    stockMaster.currentOwner=(inventory.owner?inventory.owner:owner);
    if(stockType==='other') {
        stockMaster.stockIn.other = {
            count: stockMaster.currentBalance,
            comment: 'Created default stock Master as part of Inventory',
            updatedDate: new Date(),
            updatedUser: owner.user._id
        };
    }
    stockMaster.set('created', Date.now());
    stockMaster.save(function (saveStock) {
        if (saveStock) {
            done(saveStock,null);
        } else {
            done(null,stockMaster);
        }
    });
}
function saveStockMaster(stockMasters,inventory,owner,stockType,done){
    var updateInventoryCount=inventory.numberOfUnits >0 ?false:true;
    if(!stockMasters){
        done(new Error('No Stock Master '+inventory._id),null);
    } else if(stockMasters instanceof Array && stockMasters.length>0) {
        async.forEachSeries(stockMasters, function (eachStockMaster, callback) {
            if (eachStockMaster.batchNumber && eachStockMaster.batchNumber.length>0) {
                addStockMasterData(eachStockMaster, inventory, stockType, owner, updateInventoryCount, function (stockErr, stock) {
                    if (stockErr) {
                        logger.error('Failed to save the stock Master' +errorHandler.getErrorMessage(stockErr));
                        callback(stockErr);
                    } else {
                        callback();
                    }
                });
            }else {
                logger.error('No Proper Stock' + JSON.stringify(eachStockMaster));
                callback(new Error('No Proper Stock'));
            }

        }, function (err) {
            if (err) {
                done(err, null);
            } else {
                _this.findInventoryById(inventory._id, -1, function (updateInventory) {
                    if (updateInventory instanceof Error) {
                        logger.error('Failed to fetch inventory after creating the stock item master');
                        done(updateInventory,null);
                    } else{
                        done(null, updateInventory);
                    }
                });
            }
        });
    }else{
        addStockMasterData(stockMasters, inventory, stockType, owner,false,function (stockErr,stockMaster) {
            if(stockErr){
                done(stockErr,null);
            }else if(!stockMaster) {
                done(new Error('No proper Stock'),null);
            }else{
                _this.findInventoryById(inventory._id, -1, function (updateInventory) {
                    if (updateInventory instanceof Error) {
                        logger.error('Failed to fetch inventory after creating the stock item master');
                        done(updateInventory,null);
                    } else{
                        done(null, updateInventory);
                    }
                });
            }
        });

    }

}
function createUserItemMaster(inventory,itemMaster,productBrand,owner,stockType,done){
    logger.debug('Started inventory product using product Brand- ' + JSON.stringify(productBrand.productCategory));
    var buId = inventory.businessUnit;
    var stockMasters=inventory.stockMaster;
    if(!stockMasters || (stockMasters && stockMasters.length===0)){
        stockMasters=new StockMaster(inventory);
    }
    if(stockMasters && !(stockMasters instanceof Array)) {
        stockMasters.currentBalance=inventory.numberOfUnits;
    }
    inventory=new Inventory(inventory);
    inventory.productBrand=productBrand._id;
    inventory.taxGroup=productBrand.taxGroup;
    inventory.hsncode=productBrand.hsncode;
    inventory.productCategory=productBrand.productCategory._id;
    inventory.numberOfUnits=0;
    if(productBrand._id){
        inventory.brandName=productBrand.name;
        inventory.brandVariety=productBrand.variety;
        inventory.brandOwner=productBrand.brandOwner;
        inventory.manufacturers=productBrand.manufacturers;
        inventory.hsncode=productBrand.hsncode._id;
        inventory.hsncodeName=productBrand.hsncode.hsncode;
        inventory.taxGroup=productBrand.taxGroup._id;
        inventory.taxGroupName=productBrand.taxGroup.name;
        if(productBrand.productCategory){
            inventory.productName=productBrand.productCategory.name;
            inventory.productType=productBrand.productCategory.type;
            inventory.productCode=productBrand.productCategory.code;
            inventory.productDescription=productBrand.productCategory.destinations;
            if(productBrand.productCategory.parent){inventory.productSubCategory=productBrand.productCategory.parent._id;inventory.productSubCategoryName=productBrand.productCategory.parent.name;}
            if(productBrand.productCategory.grandParent){inventory.productMainCategory=productBrand.productCategory.grandParent._id;inventory.productMainCategoryName=productBrand.productCategory.grandParent.name;}
        }
    }
    inventory.set('lastUpdated', Date.now());
    inventory.set('created', Date.now());
    inventory.itemMaster=itemMaster._id;
    inventory.productBrand=productBrand._id;
    inventory.user=owner.user;
    logger.debug('Started inventory product using product Brand Name :'+inventory.productBrand);
    inventory.save(function (saveInventory) {
        if (saveInventory) {
            done(saveInventory,null);
        } else {
            saveStockMaster(stockMasters,inventory,owner,stockType,function(saveErr,saveRes){
                if(saveErr){
                    done(saveErr,null);
                }
                else {
                    done(null,saveRes);
                }
            });


        }
    });
}
function findOrCreateUserItemMaster(inventory,itemMaster,productBrand,owner,stockType,done) {

    if(itemMaster && itemMaster._id && inventory && inventory.unitOfMeasure && inventory.businessUnit) {
        Inventory.findOne({
            'itemMaster': itemMaster._id,
            'unitOfMeasure': inventory.unitOfMeasure,
            'businessUnit': inventory.businessUnit.toString(),
            'deleted': false
        }, function (itemMasterErr, eachInventory) {
            if (itemMasterErr) {
                logger.error(errorHandler.getErrorMessage(itemMasterErr));
                done(itemMasterErr, null);
            } else if (eachInventory) {
                logger.warn('Item master with product brand name - ' + productBrand.name + ', brand variety -' + productBrand.variety + ' and unit of measure -' + eachInventory.unitOfMeasure.symbol ? eachInventory.unitOfMeasure.symbol : eachInventory.unitOfMeasure + ' already present');
                done(null, eachInventory, true);
            } else {
                logger.debug('Creating User item Master with product brand name - ' + productBrand.name + ', brand variety -' + productBrand.variety + ' and unit of measure -' + inventory.unitOfMeasure);

                createUserItemMaster(inventory, itemMaster, productBrand, owner, stockType, function (ciErr, createInventory) {
                    if (ciErr) {
                        done(ciErr, createInventory, null);
                    } else {
                        done(null, createInventory, false);
                    }
                });
            }

        });
    }else {
        logger.error('Failed to fetch the inventory with the data '+JSON.stringify(inventory));
        done(new Error('No proper inventory details'),null,null);

    }

}
function findDefaultCompanyBusinessUnit(user,businessId,done){
    if(businessId){
        done(null,businessId);
    }else {
        Company.findOne(user.company, {
            businessUnits: {$elemMatch: {defaultBusinessUnit: true}}
        }).populate('employees.businessUnits.businessUnit').exec(function (companyErr, company) {
            if (companyErr) {
                logger.error('Failed to fetch company name as part of  Inventory creation');
                done(companyErr, null);
            } else {
                if (company.businessUnits.length > 0) {
                    done(null, company.businessUnits[0].businessUnit);
                } else {
                    done(new Error('No default Business Unit with the company'), null);
                }

            }
        });
    }
}
function createSingleInventory(inventory,productBrand,owner,isImport,stockType,isOld,done) {
    if(!inventory ||(inventory && !(inventory.unitOfMeasure || (inventory.uom  &&  inventory.addUOM)))){
        logger.error('No Unit measure specified at Inventory'+JSON.stringify(inventory));
        done(new Error('No Unit measure specified at Inventory'));
    }else {

        getProductBrand(productBrand, (inventory.addUOM ? inventory.uom : null), owner, function (errPopulateBrand, singleProductBrands) {

            if (errPopulateBrand) {
                /*return res.status(400).send({
                    message: errorHandler.getErrorMessage(errPopulateBrand)
                });*/
                done(errPopulateBrand, null, null);
            } else {
                var singleProductBrand = singleProductBrands.brand;
                if (inventory.addUOM) {
                    inventory.unitOfMeasure = singleProductBrands.uom;
                }
                logger.debug('At create single inventory:' + JSON.stringify(inventory));
                findOrCreateItemMaster(inventory, singleProductBrand, owner, function (itemMasterErr, itemMasterResponse) {
                    if (itemMasterErr) {
                        logger.error('Error while creating the item Master' + JSON.stringify(itemMasterErr));
                        done(itemMasterErr, null, null);
                    } else {
                        if (isImport) {
                            logger.info('mrp:' + itemMasterResponse.MRP);
                            logger.info('moqandmargin:' + itemMasterResponse.moqAndMargin);
                            logger.info('moqandbuymargin:' + itemMasterResponse.moqAndBuyMargin);
                            logger.info('uom:' + itemMasterResponse.unitOfMeasure);
                            inventory.MRP = itemMasterResponse.MRP;
                            inventory.moqAndMargin = itemMasterResponse.moqAndMargin;
                            inventory.movAndMargin = itemMasterResponse.movAndMargin;
                            inventory.moqAndBuyMargin = itemMasterResponse.moqAndBuyMargin;
                            inventory.movAndBuyMargin = itemMasterResponse.movAndBuyMargin;
                            inventory.unitOfMeasure = itemMasterResponse.unitOfMeasure;
                        }


                        /*inventory.businessUnit = foundBusinessUnit;*/
                        findOrCreateUserItemMaster(inventory, itemMasterResponse, singleProductBrand, owner, stockType, function (userItemMasterErr, userItemMasterResponse, isOld) {
                            if (userItemMasterErr) {
                                logger.error('Error while creating user the item Master' + JSON.stringify(userItemMasterErr));
                                done(userItemMasterErr, null, null);
                            } else if (!userItemMasterResponse) {
                                logger.error('Error while creating user the item Master' + JSON.stringify(inventory));
                                done(new Error('Fail to create Inventory'), null, null);
                            } else {
                                _this.findInventoryById(userItemMasterResponse._id, 1, function (inventoryR) {
                                    if (inventoryR instanceof Error) {
                                        logger.error('Error while fetching  inventory after creation- ' + singleProductBrand.name + ' Error Details:' + errorHandler.getErrorMessage(inventoryR));
                                        done(inventoryR, null, null);
                                    } else {
                                        done(null, inventoryR, isOld);
                                    }
                                });
                            }
                        });
                        /*     }
                         });*/
                    }
                });
            }
        });
    }
}
function updateInventoriesQuery(inventories,isNested,done) {
    var finalQuery=[];
    if(! inventories  || (inventories && inventories.length===0)){
        done (new Error('No inventories'),null);
    }else {
        async.forEach(inventories, function (eachInventory,callback) {
            if(isNested) {
                if (eachInventory.srcLine) {
                    finalQuery.push({inventory: eachInventory.inventory});
                } else {
                    finalQuery.push({inventory: eachInventory});
                }
            }else{
                if (eachInventory.srcLine) {
                    finalQuery.push({_id: eachInventory.inventory});
                } else {
                    finalQuery.push({_id: eachInventory});
                }
            }
            callback();
        },function (err) {
            done(err,finalQuery);
        });
    }
}

function matchedStockMaster(stockMaster,done) {
    if(stockMaster._id){
        StockMaster.findById(stockMaster._id).exec(function (stockErr, fetchUpdateStockMaster) {
            if (stockErr) {
                done(stockErr);
            } else if(fetchUpdateStockMaster) {
                done(null, fetchUpdateStockMaster);
            }else{
                done(null,stockMaster);
            }
        });
    }else{
        done(null,stockMaster);
    }
}
function mappedStockIn(inventories,stockIn,matchedStockMaster,done){
    async.forEachSeries(inventories, function (userItemMaster, callback) {
        async.forEachSeries(userItemMaster, function (stockMaster, stockCallback) {
            /*stockIn.push({stockMaster._id})*/
        });

    });

}
function mappedStockOutConvertSourceInventory(inventories,inventoryId){

    return inventories.filter(function (eachInventory) {
        return eachInventory.inventory.toString()===inventoryId.toString() ;
    });

}
function mappedStockOutConvertSourceInventoryStock(eachInventoryStock,eachStockMasterId, convertType){

    return eachInventoryStock.filter(function (eachStockMaster) {
        return eachStockMaster.convertType===convertType && eachStockMaster.stockMaster.toString()===eachStockMasterId.toString() ;
    });

}
function saveInventories(userItemMaster,done) {
    async.forEachSeries(userItemMaster.stockMasters,function(eachStockMaster,callback){
        eachStockMaster.save(function (saveStockMasterErr,stockMaster) {
            if(saveStockMasterErr){
                logger.error('Error while saving the user Item Master with Id'+eachStockMaster._id +' Error '+JSON.stringify(saveStockMasterErr));
            }
            callback(saveStockMasterErr,stockMaster);
        });

    },function (err) {
        if(err){
            done(err,null);
        }/*else if ( !(userItemMaster.inventory instanceof  Inventory)) {
            done(new Error('No inventory with the correct type'),null);
        }*/else{
            _this.findInventoryById(userItemMaster.inventory._id,-1,function (inventoryObject) {
                inventoryObject.numberOfUnits=inventoryObject.numberOfUnits-userItemMaster.inventory.numberOfUnits;

                inventoryObject.save(function (saveUserItemErr,saveUserItemMaster) {
                    if(saveUserItemErr){
                        done(saveUserItemErr,null);
                        logger.error('Error while saving the user Item Master with Id'+userItemMaster._id +' Error '+JSON.stringify(saveUserItemErr));
                    }else {
                        done(null, saveUserItemMaster);
                    }
                });
            });
        }

    });
}
function findStockMasterHistoryItem(historyInventory,foundConvertUpdateStockMaster,stockMaster,inventoryId,owner,isStockIn,convertType,done) {
    if(historyInventory && historyInventory.length>0 && inventoryId && inventoryId) {
        logger.debug('Get the Inventory Stock Master as part of conversion at the '+isStockIn?'stock in history':'stock out history');
        var matchedInventoryOut = mappedStockOutConvertSourceInventory(historyInventory, inventoryId);

        if (matchedInventoryOut.length > 0) {
            logger.debug('Found the Inventory Stock Master as part of conversion at the ' + isStockIn ? 'stock in history' : 'stock out history');
            var indexInventory = historyInventory.indexOf(matchedInventoryOut[0]);
            var matchedInventoryStockOut = mappedStockOutConvertSourceInventoryStock(historyInventory[indexInventory].stockMasters, foundConvertUpdateStockMaster._id);
            if (matchedInventoryStockOut){
                if (matchedInventoryStockOut.length > 0) {
                    var indexInventoryStock = historyInventory[indexInventory].stockMasters.indexOf(matchedInventoryStockOut[0]);
                    var eachMatchStockMaster = historyInventory[indexInventory].stockMasters[indexInventoryStock];
                    // We need to add the testcase for already existing stock with the same stock Id
                    if (eachMatchStockMaster) {
                        historyInventory[indexInventory].stockMasters[indexInventoryStock].count += stockMaster.currentBalance;
                        historyInventory[indexInventory].stockMasters[indexInventoryStock].updatedDate = new Date();
                        historyInventory[indexInventory].stockMasters[indexInventoryStock].updatedUser = owner.user._id.toString();
                        historyInventory[indexInventory].updatedDate = new Date();
                        historyInventory[indexInventory].updatedUser = owner.user._id.toString();
                        done(null, historyInventory);
                    } else {
                        done(null, historyInventory);
                    }

                } else {
                    historyInventory[indexInventory].stockMasters.push(
                        {
                            stockMaster: foundConvertUpdateStockMaster._id.toString(),
                            convertType:convertType,
                            count: stockMaster.currentBalance,
                            updatedDate: new Date(),
                            updatedUser: owner.user._id.toString(),
                            isNew: true
                        }
                    );
                    done(null, historyInventory);
                }
        }

        } else {

            historyInventory.push({
                inventory: inventoryId,
                stockMasters: [{
                    stockMaster: foundConvertUpdateStockMaster._id.toString(),
                    count: stockMaster.currentBalance,
                    convertType:convertType,
                    updatedDate: new Date(),
                    updatedUser: owner.user._id.toString()
                }],
                updatedDate: new Date(),
                updatedUser: owner.user._id.toString()
            });
            done(null, historyInventory);

        }
    }else{
        /* logger.error('No StockMaster at the inventory with the stockMaster data :'+JSON.stringify(stockMaster));
         done(new Error('No StockMaster at the inventory with the stockMaster data'),null);*/
        historyInventory=[{
            inventory: inventoryId.toString(),
            stockMasters: [{
                stockMaster: foundConvertUpdateStockMaster._id.toString(),
                count: stockMaster.currentBalance,
                convertType:convertType,
                updatedDate: new Date(),
                updatedUser: owner.user._id.toString()
            }],
            updatedDate: new Date(),
            updatedUser: owner.user._id.toString()
        }];
        done(null, historyInventory);
    }


}

function getStockMastersData(stockMasters,eachInventory,stockIn,foundConvertUpdateStockMaster,isStockIn,owner,convertType,done){
    if(stockMasters && stockMasters.length>0 ) {
        async.forEachSeries(stockMasters, function (stockMaster, stockCallback) {

            StockMaster.findById(stockMaster._id).exec(function (stockErr, fetchStockMaster) {
                if(stockErr){
                    stockCallback(stockErr);
                }else {
                    // we need to check the existing inventory stockOut at converted source
                    if(!fetchStockMaster){
                        logger.error('No Stock with the user item master with stock id: '+stockMaster._id);
                        done(new Error('No Source Inventory'),null);

                    }else {
                        if (!fetchStockMaster.stockOut.inventories) {
                            fetchStockMaster.stockOut.inventories = [];
                        }
                        logger.debug('Found Stock with stock id: '+stockMaster._id);
                        findStockMasterHistoryItem(fetchStockMaster.stockOut.inventories,foundConvertUpdateStockMaster, stockMaster, foundConvertUpdateStockMaster.inventory, owner,isStockIn,convertType, function (stockHistoryErr, stockHistory) {
                            if (stockHistoryErr) {
                                stockCallback(stockHistoryErr);
                            } else {
                                // foundUpdateStockMaster.currentBalance = foundUpdateStockMaster.currentBalance - stockIn.currentBalance;
                                logger.debug('Update Stock out history :'+JSON.stringify(stockHistory)+'at stock id:'+stockMaster._id);
                                fetchStockMaster.currentBalance=fetchStockMaster.currentBalance-stockMaster.currentBalance;
                                fetchStockMaster.stockOut.inventories = stockHistory;
                                eachInventory.stockMasters.push(fetchStockMaster);
                                if (isStockIn) {
                                    eachInventory.inventory.numberOfUnits = eachInventory.inventory.numberOfUnits + stockMaster.currentBalance;
                                }else {
                                    eachInventory.inventory.numberOfUnits = eachInventory.inventory.numberOfUnits - stockMaster.currentBalance;
                                }
                                fetchStockMaster.save(function (stockOutErr, saveStockOut) {
                                    if (stockOutErr) {
                                        logger.error('Fail to update the Stock out history :'+stockHistory+'at stock id:'+stockMaster._id);
                                        done(stockOutErr, null);
                                    } else {
                                        stockCallback();
                                    }
                                });
                            }

                        });
                    }

                }

            });


        },function (err) {
            if(err){
                done(err,null);
            }else{
                saveInventories(eachInventory,function (saveInventoryErr,inventory) {
                    if(saveInventoryErr){
                        done(saveInventoryErr,null);
                    }else{
                        done(null,inventory);
                    }

                });

            }

        });
    }else{
        logger.error('No Stock with the user item master :'+JSON.stringify(stockMasters));
        done(new Error('No Stock with the user item master'),null);
    }

}
function updateInventorySourceStockOut(inventories,stockIn,foundConvertUpdateStockMaster,owner,convertType,done){
    var updateStockMasters=[];
    async.forEachSeries(inventories, function (userItemMaster, callback) {
        userItemMaster.inventory.numberOfUnits=0;
        var eachInventory={inventory:userItemMaster.inventory,stockMasters:[],numberOfUnits:0};
        logger.debug('Get Source inventory stockOut history');
        getStockMastersData(userItemMaster.inventory.convertStockMasters,eachInventory,stockIn,foundConvertUpdateStockMaster,true,owner,convertType,function (eachStockErr,eachSourceInventory) {
            if(eachStockErr){
                callback(eachStockErr);
            }else{
                updateStockMasters.push(eachSourceInventory);
                callback();
            }

        });

    },function (err) {
        if(err){
            done(err,null);
        }else{
            done(null,updateStockMasters);
        }
    });

}

function updateStockInStockUpdate(inventories,stockIn,owner,convertType,done) {
    /*var updateStockMasters = [];*/
    var versionStockKey = stockIn.stockMasterVersionKey;
    async.forEachSeries(inventories, function (userItemMaster, callback) {
        async.forEachSeries(userItemMaster.inventory.convertStockMasters, function (eachStockMaster, stockCallback) {
            findStockMasterHistoryItem(stockIn.stockIn.inventories, eachStockMaster,eachStockMaster, userItemMaster.inventory._id, owner, true,convertType, function (stockHistoryErr, stockHistory) {
                if(stockHistoryErr){
                    stockCallback(stockHistoryErr,null);
                }else {
                    stockIn.stockIn.inventories = stockHistory;
                    stockCallback(null,stockIn);
                }

            });
            /*callback();*/
        },function (convertErr) {
            if(convertErr){
                callback(convertErr,null);
            }else{
                callback(null,stockIn);
            }
        });
    },function (err) {
        if(err){
            done(err,null);
        }else{
            stockIn.save(function (stockInErr) {
                if (stockInErr) {
                    done(stockInErr,null);
                } else {
                    done(null,stockIn);
                }
            });
        }
    });
}

exports.createInventoryItemMaster =function(inventory, productBrand, owner,isImport,stockType, done) {
    if(owner.businessUnit) {
        businessUnitUtil.findOneBusinessUnit([{_id:owner.businessUnit}],0,function(businessUnitError,fetchBusinessUnit) {
            if(businessUnitError){
                done(businessUnitError,null);
            }else {
              /*  owner=globalUtil.prepareOwnerByBusinessUnit(fetchBusinessUnit);*/
                var isOld = false;
                if (inventory instanceof Array) {
                    var inventories = [];
                    async.eachSeries(inventory, function (eachInventory, callback) {

                        var srcLine = eachInventory.srcLine;
                        if (!eachInventory.businessUnit && owner.businessUnit) {
                            eachInventory.businessUnit = owner.businessUnit;
                        }
                        if (!eachInventory.owner) {
                            eachInventory.owner = {};
                        }
                        eachInventory.owner=globalUtil.prepareOwnerByBusinessUnit(fetchBusinessUnit);
                        eachInventory.currentOwner=globalUtil.prepareOwnerByBusinessUnit(fetchBusinessUnit);
                        eachInventory.user = owner.user._id;
                        createSingleInventory(eachInventory, productBrand, owner, isImport, stockType, isOld, function (inventoryErr, finalInventory, isOld) {
                            if (inventoryErr) {
                                callback(inventoryErr);
                            } else {
                                if (isImport) {
                                    inventories.push({inventory: finalInventory, srcLine: srcLine});
                                }
                                else {
                                    if (!isOld)
                                        inventories.push(finalInventory);
                                }
                                callback();
                            }
                        });
                    }, function (err) {
                        if (err) {
                            done(err, null, isOld);
                        } else {
                            done(null, inventories, isOld);

                        }
                    });

                } else {
                    if (!inventory.businessUnit && owner.businessUnit) {
                        inventory.businessUnit = owner.businessUnit;
                    }
                    if (!inventory.owner) {
                        inventory.owner = {};
                    }
                    inventory.owner=globalUtil.prepareOwnerByBusinessUnit(fetchBusinessUnit);
                    inventory.currentOwner=globalUtil.prepareOwnerByBusinessUnit(fetchBusinessUnit);
                    inventory.user = owner.user._id;
                    createSingleInventory(inventory, productBrand, owner, isImport, stockType, isOld, function (inventoryCreateErr, userItemMasterResponse, isOld) {
                        if (inventoryCreateErr) {
                            done(inventoryCreateErr, userItemMasterResponse, isOld);
                        }
                        else {
                            if (isOld) {
                                done(inventoryCreateErr, userItemMasterResponse, isOld);
                            } else {
                                done(inventoryCreateErr, userItemMasterResponse, isOld);
                            }
                        }
                    });

                }
            }
        });
    }else{
        done(new Error('No Owner for inventory'), null, false);
    }

};
function updateStockMaster(stockMaster,inventory,stockIn,owner,done) {
    var isNew = false;
    /* if (stockMaster.isNew) {
         stockMaster.inventory = inventory._id;
         isNew = true;
     }*/

    if(stockMaster.isNew) {
        stockMaster._id= mongoose.Types.ObjectId();
        /* stockMaster = _.extend(stockMaster,'');*/
        businessUnitUtil.findOneBusinessUnit([{_id:owner.businessUnit}],0,function(businessUnitError,fetchBusinessUnit) {
            if(businessUnitError){
                done(businessUnitError, null, null);
            }else {
                inventory.owner=globalUtil.prepareOwnerByBusinessUnit(fetchBusinessUnit);
                addStockMasterData(stockMaster, inventory, '', owner, false, function (stockErr, createStockMaster) {
                    if (stockErr) {
                        done(stockErr, null, null);
                    } else {
                        _this.findInventoryById(inventory._id, 1, function (updateInventory) {
                            if (updateInventory instanceof Error) {
                                logger.error('Failed to fetch inventory after creating the stock item master');
                                done(updateInventory, null, null);
                            } else {
                                done(null, createStockMaster, updateInventory);
                            }
                        });
                    }
                });
            }
        });
    }else {
        var versionStockKey = stockMaster.stockMasterVersionKey;
        stockMaster = _.extend(stockMaster, {currentBalance: stockMaster.currentBalance + stockIn.currentBalance});
        stockMaster.stockMasterVersionKey = versionStockKey;
        stockMaster.save(function (err) {
            if (err) {
                done(err,null,null);
            } else {

                /* we need to add testcase for the following code.
                 we need to update the stock master change in the inventory after updating the number of unit.
                 */
                Inventory.update({_id: stockMaster.inventory}, {$inc: {numberOfUnits: stockIn.currentBalance}}).exec(function (inventoryStockUpdateErr, inventoryStockUpdate) {
                    if (inventoryStockUpdateErr) {
                        done(inventoryStockUpdateErr, null,null);
                    } else if (!inventoryStockUpdate) {
                        done(new Error('Fail to update the stock master at inventory for batch Number : '+stockMaster.batchNumber), null,null);
                    } else {
                        _this.findInventoryById(inventory._id, -1, function (updateInventory) {
                            if (updateInventory instanceof Error) {
                                done(updateInventory, stockMaster,null);
                            }else {
                                done(null, stockMaster, updateInventory);
                            }
                        });
                    }
                });

            }
        });
    }
}
exports.conversionInventory=function (inventories,inventory,productBrand,owner,convertType,done) {
    if(inventory.convertStockMasters && inventory.convertStockMasters.length>0) {
        var stockIn = new StockMaster(inventory.convertStockMasters[0]);
        stockIn.MRP=inventory.MRP;
        if (inventory._id && inventory._id!==null) {
            // Number of units update at inventory if there is an existing user Item master
            logger.debug('Use existing Inventory to convert the product - ' + JSON.stringify(inventory));
            // Still inventory is already exist then we need to allow user to update

            matchedStockMaster(stockIn, function (err, foundConvertUpdateStockMaster) {
                if (err) {
                    done(err);
                } else {
                    updateStockMaster(foundConvertUpdateStockMaster, inventory, stockIn, owner, function (stockInventoryUpdateErr, stockMaster, inventory) {
                        foundConvertUpdateStockMaster=stockMaster;
                        if (stockInventoryUpdateErr) {
                            logger.error('Error while setting stock to the Inventory');
                            done(stockInventoryUpdateErr, null);
                        } else {
                            stockIn.inventory=stockMaster.inventory;
                            updateInventorySourceStockOut(inventories, stockIn, foundConvertUpdateStockMaster, owner,convertType, function (errUpdateSourceInventory, updateSourceInventories) {
                                if (errUpdateSourceInventory) {
                                    logger.debug('Failed to update the stockMasters at the inventory Source - ' + JSON.stringify(errUpdateSourceInventory));
                                    done(errUpdateSourceInventory, null);
                                } else {
                                    updateStockInStockUpdate(inventories, foundConvertUpdateStockMaster, owner,convertType, function (stockInErr, stockIn) {
                                        done(stockInErr, inventory);

                                    });
                                }


                            });
                        }
                    });
                }
            });


        } else {
            // Create new Inventory  for the selected products.
            logger.debug('Create new Inventory  for the selected Products - ' + JSON.stringify(inventory));
            inventory.numberOfUnits = stockIn.currentBalance;
            inventory.stockMaster = [stockIn];
            _this.createInventoryItemMaster(inventory, productBrand, owner, false, 'inventory',function (inventoryErr,inventoryResponse,isOld) {
                if (inventoryErr instanceof Error) {
                    done(inventoryErr, null);
                } else if (isOld) {
                    done(new Error('Inventory already exist'), null);
                } else {
                    logger.debug('Update the stockOut at the Selected Inventories : - ' + JSON.stringify(inventories));
                    updateInventorySourceStockOut(inventories, stockIn, inventoryResponse.stockMasters[0], owner, convertType,function (errUpdateSourceInventory, updateSourceInventories) {
                        if (errUpdateSourceInventory) {
                            logger.error('Failed to update the stockMasters at the inventory Source - ' + JSON.stringify(errUpdateSourceInventory));
                            done(errUpdateSourceInventory, null);
                        } else {
                            logger.debug('Update stockIn at the converted Inventory : - ' + JSON.stringify(inventoryResponse));
                            updateStockInStockUpdate(inventories, inventoryResponse.stockMasters[0], owner,convertType, function (stockInErr, stockIn) {
                                done(stockInErr, inventoryResponse);

                            });
                        }
                    });


                }
            });




        }
    }else{
        logger.error('No Stock Master at conversion inventory '+JSON.stringify(inventory));
        done(new Error('No Stock Master at conversion inventory'), null);

    }

};
exports.createMassImport=function (inventories,owner,stockType,done) {
    var results=[];
    var srcLine;
    if(inventories && inventories.length>0) {
        async.forEachSeries(inventories, function (userItemMaster, callback) {
            var productBrand = userItemMaster.productBrand;
            var inventory = userItemMaster.inventory;
            var businessUnit;
            if (userItemMaster.businessUnit) {
                businessUnit = userItemMaster.businessUnit;
            }
            else {
                businessUnit = owner.businessUnit;
            }
            var isImport = userItemMaster.isImport;
            srcLine = userItemMaster.srcLine;
            if (productBrand) {
                /* ProductBrand.findById(productBrand).populate('user', 'displayName').populate('productCategory').exec(*/
                _this.createInventoryItemMaster(inventory, productBrand, owner, isImport, stockType, function (inventoryErr, inventoryResponse) {
                    if (inventoryErr && inventoryErr instanceof Error) {
                        callback(inventoryErr);
                        logger.error('Error while creating product in inventory - ' + productBrand + ' Error Details:' + errorHandler.getErrorMessage(inventoryErr));
                    } else if (inventoryResponse instanceof Array) {
                        results = results.concat(inventoryResponse);
                        callback();
                    } else {
                        if (inventoryResponse instanceof Inventory) {
                            logger.debug('Successfully created inventory products with product Name- ' + productBrand);
                            results.push(Inventory);
                            callback();
                        } else {
                            logger.error('Failed to created User inventory  with product Name- ' + productBrand);
                            callback();
                        }
                    }
                });


            } else {
                logger.error('No Brand for the inventory creation with data : ' + JSON.stringify(userItemMaster));
                callback(new Error('No Brand for the inventory creation'));
            }

        }, function (err) {
            /* if (err) {
                 logger.error(errorHandler.getErrorMessage(err));
                 done(err);
             } else {
                 done(null, results);
             }*/
            logger.debug('SRCLINE:' + srcLine);
            done(err, results)
            ;
        });
    }
    else{
        done(new Error('No inventories selected'));
    }
};
exports.getProductInventories= function (query,done) {
    Inventory.aggregate([
        {
            $match: query
        },
        {
            $group: {
                _id: { productCategory: '$productCategory', productBrand: '$productBrand' },
                inventoryDetails: {
                    $push: '$$ROOT'

                }
            }
        },
        {
            $group: {
                _id: '$_id.productCategory',
                productBrand: {
                    $push: {
                        productBrand: '$_id.productBrand',
                        inventories: '$inventoryDetails'
                    }
                }
            }
        }
    ]).exec(function (err,products) {
        if(err) done(err);
        else {

            //'productBrand.inventories.stockMasters productBrand.inventories.hsncode productBrand.inventories.taxGroup productBrand.inventories.unitOfMeasure '

            Inventory.populate( products,[{
                path: 'productBrand.inventories.hsncode',
                model: 'Hsncodes'
            },{
                path: 'productBrand.productBrand',
                model: 'ProductBrand'
            },{
                path: 'productBrand.inventories.taxGroup',
                model: 'TaxGroup'
            },{
                path: 'productBrand.inventories.unitOfMeasure',
                model: 'UnitOfMeasure'
            },{
                path: 'productBrand.inventories.stockMasters',
                model: 'StockMaster'
            }], function(errPopulate,results) {
                if (errPopulate) done(errPopulate);
                else {
                    done(null, results);
                }
            });


        }
    });
};

exports.batchInventoryUpdate=function(query,update,done){
    Inventory.update(
        query,
        {$set:update},
        {multi: true},
        function(err,updateInventory){
            done(err,updateInventory);

        }
    );
};

function searchUserItemMaster(businessUnit,results) {
    return results.filter(function (eachResult) {
        return eachResult.businessUnit.toString()===businessUnit.toString();

    });
}
function findInventoriesQuery(products,finalQuery,done){
    async.forEachSeries(products,function(createInventory,callback){
        var query={};
        if(createInventory.subCategory2 &&createInventory.subCategory2!==undefined && createInventory.subCategory2!==-1){
            query.productCategory=createInventory.subCategory2;
        }
        if(createInventory.subCategory1 && createInventory.subCategory1!==undefined && createInventory.subCategory1!==-1){
            query.productSubCategory=createInventory.subCategory1;
        }
        if(createInventory.mainCategory&& createInventory.mainCategory!==undefined && createInventory.mainCategory!==-1){
            query.productMainCategory=createInventory.mainCategory;
        }
        if(createInventory.productBrand && createInventory.productBrand!==-1){
            query.productBrand=createInventory.productBrand;
        }
        if(createInventory.unitOfMeasure && createInventory.unitOfMeasure!==-1){
            query.unitOfMeasure=createInventory.unitOfMeasure;
        }
        if(createInventory._id){
            query._id=createInventory._id;
        }
        finalQuery.push(query);
        callback();
    },function (err) {
        done(finalQuery);
    });

}
exports.searchItemMaster=function (createInventories,businessUnit,user,isItemMaster,isSource,done) {
    var query   =[];
    findInventoriesQuery(createInventories,query,function (query) {
        if(!createInventories[0]._id && !businessUnit){
            logger.error('No Business unit for product search');
            done(new Error('No Proper Business unit'), null, null);
        }else {

            var finalQuery = [{$or: query}];
            if(businessUnit){
                finalQuery.push({businessUnit:businessUnit});
            }
            _this.findQueryByInventories(finalQuery, -3, false, null, function (err, products) {
                if (err) {
                    done(err, null);
                } else if(createInventories[0]._id && createInventories.length < products.length) {
                    done(new Error('Not able to fetch the records'), null);
                } else {
                    if (products.length === 0) {
                        isItemMaster = true;
                        done(err, products, isItemMaster);
                    } else {
                        isItemMaster = false;
                        done(err, products, isItemMaster);
                    }

                }
            });
        }

    });
};
/**
 * Get Offer Inventories
 */
function getOfferInventoriesIds(products, isInventory, isBuyer, done) {
    var productIds = [];
    async.forEachSeries(products, function (product, callback) {
        if (isInventory) {
            if (product.inventory instanceof Inventory) {
                productIds.push(product.inventory._id);
            } else {
                logger.error('Error while fetching Inventory with value: :');
            }
        } else {
            var minMOQ = commonUtil.getMinQuantity(product, isBuyer);
            productIds.push({inventory: product, MOQ: minMOQ ? minMOQ.MOQ : 1, margin: minMOQ ? minMOQ.margin : 100});
        }
        callback();
    }, function (err) {
        if (err) {
            logger.error('Error while fetching products at offer :' + errorHandler.getErrorMessage(err));
            done(err, null);
        } else {
            done(null, productIds);
        }
    });
}
/**
 * Get Offer Item Masters
 */
function getOfferItemMastersIds(products, isInventory, isBuyer, done) {
    var productIds = [];
    async.forEachSeries(products, function (product, callback) {
        if (isInventory) {
            if (product.inventory instanceof Inventory) {
                productIds.push(product.inventory._id);
            } else {
                logger.error('Error while fetching Inventory with value: :');
            }
        } else {
            var minMOQ = commonUtil.getMinQuantity(product, isBuyer);
            productIds.push({inventory: product, MOQ: minMOQ ? minMOQ.MOQ : 1, margin: minMOQ ? minMOQ.margin : 100});
        }
        callback();
    }, function (err) {
        if (err) {
            logger.error('Error while fetching products at offer :' + errorHandler.getErrorMessage(err));
            done(err, null);
        } else {
            done(null, productIds);
        }
    });
}
/**
 * Get query for location field
 */
function getQuery(classificationCriteria, done) {
    var queryArray = [];
    if (classificationCriteria.criteriaType === 'Custom') {
        async.forEach(classificationCriteria.criteria, function (eachCriteria, callback) {
            if (eachCriteria.category || eachCriteria.productBrand) {

                if (eachCriteria.category) {
                    dbUtil.findCategoryById(eachCriteria.category, function (categoryErr, populateCategory) {
                        if (categoryErr) {
                            callback(categoryErr);
                        } else {
                            var eachInventory = {};
                            if (populateCategory) {
                                if (populateCategory.type === 'MainCategory') {
                                    eachInventory.productMainCategory = populateCategory._id;
                                } else if (populateCategory.type === 'SubCategory1') {
                                    eachInventory.productSubCategory = populateCategory._id;
                                } else if (populateCategory.type === 'SubCategory2') {
                                    eachInventory.productCategory = populateCategory._id;
                                }
                            }
                            if (eachCriteria.productBrand) {
                                eachInventory.productBrand = eachCriteria.productBrand;
                            }
                            if (eachCriteria.unitOfMeasure) {
                                eachInventory.unitOfMeasure = eachCriteria.unitOfMeasure;
                            }
                            queryArray.push(eachInventory);
                            callback();

                        }

                    });

                } else if (eachCriteria.brand) {
                    var eachInventory = {};
                    if (eachCriteria.productBrand) {
                        eachInventory.productBrand = eachCriteria.brand;
                    }
                    if (eachCriteria.unitOfMeasure) {
                        eachInventory.unitOfMeasure = eachCriteria.unitOfMeasure;
                    }
                    queryArray.push(eachInventory);
                    callback();
                }

            } else {
                callback();
            }

        }, function (err) {
            if (err) {
                done(err, null);
            } else {
                done(null, queryArray);
            }
        });
    } else {
        done(null, queryArray);
    }

}
/**
 * Get the Inventories for Match Criteria
 */
exports.offerCriteriaInventories=function (offer, req,user, done) {

    /*var query = {'owner.businessUnit': offer.owner.businessUnit};*/
    var query = {};
    if(offer.owner.businessUnit){
        query.businessUnit=offer.owner.businessUnit;
    }
    var pageOptions = {};
    if (req.param('page') && !isNaN(req.param('page'))) {
        pageOptions.page = parseInt(req.param('page'));
    }
    if (req.param('limit')) {
        pageOptions.limit = parseInt(req.param('limit'));
    }
    var queryArray = [];

    if (offer && (offer.offerType === 'Sell')) {
        getQuery(offer.inventoryItemsClassification.classificationCriteria,function (err, queryArray) {
            if (err) {
                done(err, null);
            } else {
                var finalQuery = null;
                if (queryArray.length > 0)
                    finalQuery = {$and: [query, {$or: queryArray}]};
                else finalQuery = query;

                /*if(offer.offerType==='Buy') {
                    finalQuery.businessUnit=user._id;
                }else if(offer.offerType==='Sell'){
                    finalQuery.businessUnit = offer.businessUnit;
                }*/
                if (queryArray.length > 0 || offer.inventoryItemsClassification.classificationCriteria.criteriaType === 'All') {
                    _this.findQueryByInventories([finalQuery], -1, false, null, function (inventoriesErr, inventories) {
                        if (inventoriesErr) {
                            done(inventoriesErr, null);
                        } else {
                            getOfferInventoriesIds(inventories, false, offer.offerType === 'Buy', function (err, finalInventories) {
                                done(err, finalInventories);
                            });
                        }

                    });
                } else {
                    done(null, null);
                }
            }
        });
    }else if(offer.offerType==='Buy'){
        getQuery(offer.inventoryItemsClassification.classificationCriteria,function (err, queryArray) {
            if (err) {
                done(err, null);
            } else {
                var finalQuery = null;
                if (queryArray.length > 0)
                    finalQuery = {$and: [query, {$or: queryArray}]};
                else finalQuery = query;
                //This needs to be changed based on user item master .
                _this.findQueryByInventories([finalQuery], -1, false, pageOptions, function (itemMasterErr, inventories) {
                    if (itemMasterErr) {
                        done(itemMasterErr, null);
                    } else {
                        getOfferInventoriesIds(inventories, false, offer.offerType === 'Buy', function (err, finalInventories) {
                            done(err, finalInventories);
                        });
                    }
                });
            }
        });
    }else {
        done(new Error('No matched criteria Products for Buy Offer'), null);
    }

};
function foundInventory(sourceInventories,targetInventory,done) {
    done(sourceInventories.filter(function (eachInventory) {
        return eachInventory.inventory.toString()===targetInventory;
    }));
}
function foundStockMaster(sourceStockMaster,stockMaster,done) {
    done(sourceStockMaster.filter(function (eachStockMaster) {
        return eachStockMaster.stockMaster.toString()===stockMaster._id.toString();
    }));

}
function foundMatchedStockMaster(sourceInventories,targetInventory,targetStockMasters,done){
    if(targetStockMasters[0]._id){
        var inventory=sourceInventories.filter(function (eachInventory) {
            return eachInventory.inventory.toString()===targetInventory;
        });
        if(inventory.length>0) {
            var sourceStockMaster = inventory[0].stockMasters.filter(function (eachStockMaster) {
                return eachStockMaster.stockMaster.toString() === targetStockMasters[0]._id.toString();
            });
            if(sourceStockMaster.length>0){
                return true;
            }else{
                return false;
            }
        }else{
            return false;
        }
    }else{
        return false;
    }
}
function validateDataBeforeConversion(soruceInventoryStockMasters,inventory,logger,done) {
    async.forEachSeries(soruceInventoryStockMasters, function (eachSourceInventory, inventoryCallback) {
        var inventoryTotalCount = 0;
        async.forEachSeries(eachSourceInventory.inventory.convertStockMasters, function (eachConvertStockMaster, stockCallback) {
            StockMaster.findOne({
                inventory: eachSourceInventory.inventory._id,
                _id: eachConvertStockMaster._id
            }).populate('inventory', 'businessUnit numberOfUnits').exec(function (err, stockMaster) {
                inventoryTotalCount = +eachConvertStockMaster.currentBalance;
                if (err) {
                    logger.error('No Sufficient Stock at source inventory data :' + JSON.stringify(eachSourceInventory) + err);
                    done(err, null);
                } else if (!stockMaster) {
                    logger.error('No Sufficient Stock at source inventory data :' + JSON.stringify(eachSourceInventory));
                    done(new Error('No Sufficient Stock at source inventory data'), null);
                } else if (stockMaster.currentBalance === 0 || stockMaster.inventory.numberOfUnits === 0 || stockMaster.currentBalance < eachConvertStockMaster.currentBalance || stockMaster.inventory.numberOfUnits < eachConvertStockMaster.currentBalance || stockMaster.inventory.numberOfUnits < inventoryTotalCount) {
                    logger.error('No Sufficient Stock at source inventory data :' + JSON.stringify(eachSourceInventory) + (stockMaster ? ' from the original source Inventory stock Master data :' + JSON.stringify(stockMaster) : ''));
                    done(new Error('No Sufficient Stock at source inventory data'), null);
                }else if (inventory && (inventory._id || inventory.businessUnit)) {
                    if(stockMaster.inventory.businessUnit && inventory.businessUnit!==stockMaster.inventory.businessUnit.toString()){
                        logger.error('Source and tareget inventories should belongs to same businessUnit' + JSON.stringify(eachSourceInventory) + (stockMaster ? ' from the original source Inventory stock Master data :' + JSON.stringify(stockMaster) : ''));
                        done(new Error('Conversion is  possible with in same business unit . Place order between business Units '), null);

                    }else if (inventory._id && stockMaster.inventory._id.toString() === inventory._id.toString()) {
                        logger.error('No Sufficient Stock at source inventory data :' + JSON.stringify(eachSourceInventory) + (stockMaster ? ' from the original source Inventory stock Master data :' + JSON.stringify(stockMaster) : ''));
                        done(new Error('Conversion is not possible between the same inventories'), null);
                    } else if (foundMatchedStockMaster(stockMaster.stockIn.inventories, inventory._id, inventory.convertStockMasters)) {
                        logger.error('There is already used this inventory as source inventory data :' + JSON.stringify(eachSourceInventory) + (stockMaster ? ' from the original source Inventory stock Master data :' + JSON.stringify(stockMaster) : ''));
                        done(new Error('Conversion is done from the same inventory with same batch Number. Try with the new Batch'), null);
                    } else {
                        stockCallback();
                    }
                } else {
                    stockCallback();
                }
            });

        }, function (stockError) {
            inventoryCallback(stockError, null);
        });
    }, function (inventoryError) {
        done(inventoryError, null);
    });

}
exports.getValidateConversionData=function(data,logger,done) {
    if(!data){
        done(new Error('No Convert Inventory'),null);
    }else if (!data.inventory) {
        logger.error('No Inventory at the conversion input:'+JSON.stringify(data));
        done(new Error('No Inventory at the conversion Data'),null);
    }else if(data.inventory && !data.inventory._id &&  !data.inventory.businessUnit){
        done(new Error('No Business Unit at the Convert Inventory'),null);
    }else if (!data.productBrand) {
        logger.error('No Product Brand at the conversion input:'+JSON.stringify(data));
        done(new Error('No Product Brand at the conversion Data'),null);
    }else if (!data.inventories || !(data.inventories instanceof Array) || (data.inventories instanceof Array  && data.inventories.length===0)) {
        logger.error('No Source inventory at the conversion input:'+JSON.stringify(data));
        done(new Error('No Source inventory at the conversion Data'),null);
    }else if (data.inventories.filter(function (eachInventory) {
            return !eachInventory.inventory || !eachInventory.inventory._id || !eachInventory.inventory.convertStockMasters  || !(eachInventory.inventory.convertStockMasters instanceof Array) || (eachInventory.inventory.convertStockMasters.filter(function (eachStockMaster) { return !eachStockMaster._id;}).length!==0);

        }).length!==0){
        logger.error('Incorrect Source Inventory format at the conversion input:'+JSON.stringify(data));
        done(new Error('Incorrect Source Inventory format at the conversion input'),null);

    }else{
        validateDataBeforeConversion(data.inventories,data.inventory,logger,function (inSufficentStock,proper) {
            if(inSufficentStock){
                done(inSufficentStock,data);
            }else{
                done(null,data);
            }

        });

    }
};
/*
exports.findInventoriesByBusinessUnit=function(query,populateLevels,isMobile,pageOptions, done){
    var results=[];
    BusinessUnit.findOne({_id:query._id}).populate({path:'inventories.inventory',match:{deleted:false},select:''}).skip(pageOptions.page > 0 ? ((pageOptions.page-1) * pageOptions.limit) : 0 ).limit(pageOptions.limit).exec(function(err,businessUnit){
        if(err){
            done(err,null);
        }
        else if(businessUnit==null){
            done(new Error('No business unit found'),null);
        }
        else {
            var retVal = {};
            retVal.inventories = [];
            if (businessUnit.inventories && businessUnit.inventories.length > 0) {
                businessUnit.inventories.forEach(function (inv) {
                    if(inv._id)
                        retVal.inventories.push(inv.inventory);
                });
            }
            retVal.total_count = retVal.inventories.length;
            retVal.businessUnit = businessUnit._id;
            done(err, retVal);
            /!*async.forEachSeries(businessUnit.inventories,function(inventory){
                logger.info(JSON.stringify(inventory));
            },function(err){
                if(err){
                    logger.error('Error occurred:'+err);
                    done(err,results)
                }
                else{
                    done(err,results);
                }
            });*!/
        }
    });

};
*/
exports.findInventoriesByBusinessUnit=function(query,populateLevels,isMobile,pageOptions, done){
    var results=[];
    var skip,pageNo,limit;
    pageNo=pageOptions.page;
    limit=pageOptions.limit;
    skip=pageNo > 0 ? ((pageNo-1) * limit) : 0;
    BusinessUnit.findOne({_id:query._id}).populate({path:'inventories.inventory',match:{deleted:false},select:''}).exec(function(err,businessUnit){
        if(err){
            done(err,null);
        }
        else if(businessUnit===null){
            done(new Error('No business unit found'),null);
        }
        else {
            var retVal = {};
            retVal.inventories = [];
            var inventoriesCount=businessUnit.inventories.length;
            if (businessUnit.inventories && businessUnit.inventories.length > 0) {
                businessUnit.inventories.forEach(function (inv) {
                    if(inv._id)
                        retVal.inventories.push(inv.inventory);
                });
            }
            var finalList;
            if(!pageNo){
                pageNo=1;
            }
            if(!limit){
                limit=10;
            }
            if(pageNo===1) {
                finalList = retVal.inventories.slice(0,pageNo * limit);
            }
            else{
                if(inventoriesCount<pageNo*limit){
                    finalList = retVal.inventories.slice((pageNo-1)*limit);
                }
                else {
                    finalList = retVal.inventories.slice((pageNo - 1) * limit, pageNo * limit);
                }
            }
            retVal.businessUnit = businessUnit._id;
            retVal.inventories=finalList;
            retVal.total_count=inventoriesCount;
            done(null,retVal);
        }
    });
};
exports.getInventorySummary=function (inventories,summaryFilters, done) {
    var outOfStock = [];
    var lowOnStock = [];
    var inactive = [];
    var fastMoving = [];
    var slowMoving= [];
    var newlyAdded = [];
    var expired=[];
    var inventoriesSummary = {};
    async.each(inventories, function (inventory, callback) {
        if(inventory.disabled && inventory.disabled === true){
            inactive.push(inventory);
        }
        if(inventory.expiryDate && inventory.expiryDate < new Date(Date.now())){
            expired.push(inventory);
        }
        if(inventory.numberOfUnits === 0){
            outOfStock.push(inventory);
        }
        if((inventory.packagingDate && inventory.packagingDate.getDate() === new Date(Date.now()).getDate()) || ((inventory.created && inventory.created.getDate() === new Date(Date.now()).getDate()))){
            newlyAdded.push(inventory);
        }
        if(inventory.numberOfUnits === 1){
            lowOnStock.push(inventory);
        }
        callback();
    }, function (eachErr) {
        if(!eachErr){
            inventoriesSummary.OutOfStock=(summaryFilters && summaryFilters.OutOfStock)?outOfStock:outOfStock.length;

            inventoriesSummary.LowOnStock=(summaryFilters && summaryFilters.LowOnStock)?lowOnStock:lowOnStock.length;

            inventoriesSummary.Inactive=(summaryFilters && summaryFilters.Inactive)?inactive:inactive.length;

            /*inventoriesSummary.push({
                'FastMoving': inventories?inventories.length:0
            });
            inventoriesSummary.push({
                'SlowMoving': inventories?inventories.length:0
            });*/
            inventoriesSummary.NewlyAdded=(summaryFilters && summaryFilters.NewlyAdded)?newlyAdded:newlyAdded.length;

            inventoriesSummary.Expired=(summaryFilters && summaryFilters.Expired)?expired:expired.length;
            inventoriesSummary.Count=inventories.length;

        }
    });
    done(inventoriesSummary);
};
