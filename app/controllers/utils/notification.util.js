'use strict';

/**
 * Module dependencies.
 */
var https = require('https'),
    logger = require('../../../lib/log').getLogger('NOTIFICATION', 'DEBUG'),
    sms = require('./sms.util'),
    fcm = require('./fcm.util'),
    nodemailer = require('nodemailer'),
    notificationUtil = require('./notification.util'),
    offerUtil = require('./common.offer.util'),
    orderUtil = require('./common.orders.util'),
    mongoose = require('mongoose'),
    moment = require('moment'),
    async = require('async'),
    User = mongoose.model('User'),
    Notification = mongoose.model('Notification'),
    Offer = mongoose.model('Offer'),
    Order = mongoose.model('Order'),
    Message = mongoose.model('Message'),
    Product = mongoose.model('Product'),
    config = require('../../../config/config'),
    commonUtil= require('./common.util');

/**
 * This utility method is used for sending the notifications.
 * @param app
 * @param notification
 * @param done
 */

function getInventorySizeMargin(offer){
    for(var i=0;i< offer.products.length;i++){
        offer.products[i].unitIndicativePrice=commonUtil.getQuantityPrice(offer.offerType==='Sell'?offer.products[i].inventory.moqAndSalePrice:offer.products[i].inventory.moqAndBuyPrice,offer.products[i].MOQ,offer.products[i].inventory.MRP);
        offer.products[i].margin=(offer.products[i].inventory.unitPrice-offer.products[i].unitIndicativePrice)*100/offer.products[i].inventory.unitPrice;
    }
    return offer.products;

}
exports.sendNotification = function (app, notification, done) {
    var notificationEmail;
    if(!config.sendNotification){
        done();
    }else {
        async.waterfall([
            function prepareNotification(done) {
                var displayName;

                if (notification.target.nVipaniUser) {
                    displayName = notification.target.nVipaniUser.displayName;
                } else if (notification.target.contact) {
                    displayName = notification.target.contact.displayName;
                }
                if (notification.source && notification.source.message && notification.type && notification.type === 'OrderComment' && notification.source.order) {
                    Message.findById(notification.source.message).populate('user', 'username email displayName serverUrl').exec(function (err, resultMessage) {
                        if (err) {
                            logger.error('Error while loading the message details with message id - ' + notification.source.message._id + ' of order comment message notification');
                            done(err);
                        } else {
                            Order.findById(notification.source.order).populate('buyer.contact buyer.nVipaniCompany buyer.businessUnit seller.contact seller.nVipaniCompany seller.businessUnit mediator.contact mediator.nVipaniCompany mediator.businessUnit', 'name displayName firstName lastName middleName addresses').populate('user', 'username email displayName serverUrl').exec(function (err, resultOrder) {
                                if (err) {
                                    logger.error('Error while loading the Order details with order id- ' + notification.source.order + ' of order comment message notification');
                                    done(err);
                                } else {
                                    notificationEmail = (resultOrder.user.email ? resultOrder.user.email : null);
                                    app.render('templates/order-comment', {
                                        name: displayName,
                                        appName: config.app.title,
                                        title: notification.title,
                                        userName: resultMessage.user.displayName,
                                        orderNumber: resultOrder.orderNumber,
                                        orderStatus: resultOrder.currentStatus,
                                        buyer: (resultOrder.buyer.contact.displayName?resultOrder.buyer.contact.displayName:(resultOrder.buyer.contact.firstName + ' ' + resultOrder.buyer.contact.lastName)),
                                        seller: (resultOrder.seller.contact.displayName?resultOrder.seller.contact.displayName:(resultOrder.seller.contact.firstName + ' ' + resultOrder.seller.contact.lastName)),
                                        orderType: resultOrder.orderType,
                                        totalAmount: resultOrder.totalAmount,
                                        paidAmount: resultOrder.paidAmount,
                                        baseUrl: resultOrder.user.serverUrl,
                                        url: '/#!/orders/' + notification.source.order+'/'+resultMessage._id,
                                        comment: resultMessage.body
                                    }, function (err, emailHTML) {
                                        var attachments = [];
                                        done(err, attachments, emailHTML);
                                    });
                                }
                            });
                        }
                    });
                } else if (notification.source && notification.source.message && notification.type && notification.type === 'OfferComment' && notification.source.offer) {
                    Message.findById(notification.source.message).populate('user', 'username email displayName serverUrl').exec(function (err, resultMessage) {
                        if (err) {
                            logger.error('Error while loading the message details with message id - ' + notification.source.message._id + ' of offer comment message notification');
                            done(err);
                        } else {
                            offerUtil.fetchOfferById(notification.source.offer, -2, function (errResult, resultOffer) {
                                if (errResult) {
                                    logger.error('Error while loading the offer details with offer id - ' + notification.source.offer._id + ' of offer comment message notification');
                                    done(errResult);
                                } else {
                                    notificationEmail = (resultOffer.user.email ? resultOffer.user.email : null);
                                    app.render('templates/offer-comment', {
                                        name: displayName,
                                        appName: config.app.title,
                                        title: notification.title,
                                        userName: resultMessage.user.displayName,
                                        offerName: resultOffer.name,
                                        offerType: resultOffer.offerType,
                                        offerNumber: resultOffer.offerNumber,
                                        offerValidFrom: resultOffer.validFrom?moment(resultOffer.validFrom).format('LL'):null,
                                        offerValidTill: resultOffer.validTill?moment(resultOffer.validTill).format('LL'):null,
                                        baseUrl: resultOffer.user.serverUrl,
                                        url: '/#!/offers/' + notification.source.offer+'/'+resultMessage._id,
                                        comment: resultMessage.body
                                    }, function (err, emailHTML) {
                                        var attachments = [];
                                        done(err, attachments, emailHTML);
                                    });
                                }
                            });
                        }
                    });
                } else if (notification.source && notification.source.offer) {
                    offerUtil.fetchOfferById(notification.source.offer, -3, function (errResult, resultOffer) {
                        if (errResult) {
                            logger.error('Error while loading the offer details with offer id - ' + notification.source.offer._id + ' of offer notification');
                            done(errResult);
                        } else {
                            /* Offer.populate(resultOffer, {
                                 path: 'products.inventory.product',
                                 model: Product,
                                 select: 'name category productImageURL1'
                             }, function (nestedErr, offer) {

                                 if (nestedErr) {
                                     done(nestedErr);
                                 } else {*/
                            logger.debug('email notification sending');
                            notificationEmail = (resultOffer.user.email ? resultOffer.user.email : null);
                            app.render('templates/offer-creation', {
                                name: displayName,
                                appName: config.app.title,
                                title: notification.title,
                                fromBunit: resultOffer.owner.businessUnit.name,
                                fromCompany: resultOffer.owner.company.name,
                                fromName: resultOffer.user.displayName ? resultOffer.user.displayName : resultOffer.user.username,
                                offerName: resultOffer.name,
                                offerType: resultOffer.offerType,
                                offerValidFrom: resultOffer.validFrom?moment(resultOffer.validFrom).format('LL'):null,
                                offerValidTill: resultOffer.validTill?moment(resultOffer.validTill).format('LL'):null,
                                baseUrl: resultOffer.user.serverUrl,
                                url: '/#!/offers/' + notification.source.offer,
                                products: getInventorySizeMargin(resultOffer)
                            }, function (err, emailHTML) {
                                var attachments = [];
                                done(err, attachments, emailHTML);
                            });

                            /*   }
                           });*/
                        }
                    });
                } else if (notification.source && notification.source.order) {
                    Order.findById(notification.source.order).populate('buyer.contact buyer.nVipaniCompany buyer.businessUnit seller.contact seller.nVipaniCompany seller.businessUnit mediator.contact mediator.nVipaniCompany mediator.businessUnit', 'name displayName firstName lastName middleName addresses').populate('products.inventory', '-offerUnits -saleUnitPrice -buyUnitPrice -updateHistory -numberOfUnitsHistory -offerUnitsHistory -buyOfferUnitsHistory -moqAndPrice -croppedInventoryImageURL2 -croppedInventoryImageURL3 -croppedInventoryImageURL4 -inventoryImageURL2 -inventoryImageURL3 -inventoryImageURL4').populate('user', 'username email displayName serverUrl').exec(function (err, resultOrder) {
                        if (err) {
                            logger.error('Error while loading the Order details with order id- ' + notification.source.order);
                            done(err);
                        } else {
                            Order.populate(resultOrder, {
                                path: 'products.inventory.product',
                                model: Product,
                                select: 'name category productImageURL1'
                            }, function (nestedErr, order) {
                                if (nestedErr) {
                                    done(nestedErr);
                                } else if (!order) {
                                    done(new Error('No Orders'));
                                } else {
                                    orderUtil.filePath(order,function (invoiceFilePath) {
                                        notificationEmail = (resultOrder.user.email ? resultOrder.user.email : null);
                                        app.render('templates/new-order-creation', {
                                            name: displayName,
                                            appName: config.app.title,
                                            title: notification.title,
                                            fromName: order.user.displayName ? order.user.displayName : order.user.username,
                                            orderNumber: order.orderNumber,
                                            orderStatus: order.currentStatus,
                                            buyer: (resultOrder.buyer.contact.displayName?resultOrder.buyer.contact.displayName:(resultOrder.buyer.contact.firstName + ' ' + resultOrder.buyer.contact.lastName)),
                                            seller: (resultOrder.seller.contact.displayName?resultOrder.seller.contact.displayName:(resultOrder.seller.contact.firstName + ' ' + resultOrder.seller.contact.lastName)),
                                            companyName: resultOrder.seller.nVipaniCompany?resultOrder.seller.nVipaniCompany.name:null,
                                            businessUnit: resultOrder.seller.businessUnit?resultOrder.seller.businessUnit.name:null,
                                            buyerCompanyName: resultOrder.buyer.nVipaniCompany?resultOrder.buyer.nVipaniCompany.name:resultOrder.buyer.contact.displayName,
                                            buyerBusinessUnit: resultOrder.buyer.businessUnit?resultOrder.buyer.businessUnit.name:null,
                                            shippingAddressLine: resultOrder.shippingAddress.addressLine,
                                            shippingAddressCity: resultOrder.shippingAddress.city,
                                            shippingAddressState: resultOrder.shippingAddress.state,
                                            shippingAddressCountry: resultOrder.shippingAddress.country,
                                            shippingAddressPincode: resultOrder.shippingAddress.pinCode,
                                            billingAddressLine: resultOrder.billingAddress.addressLine,
                                            billingAddressCity: resultOrder.billingAddress.city,
                                            billingAddressState: resultOrder.billingAddress.state,
                                            billingAddressCountry: resultOrder.billingAddress.country,
                                            billingAddressPincode: resultOrder.billingAddress.pinCode,
                                            orderType: order.orderType,
                                            totalAmount: order.totalAmount,
                                            paidAmount: order.paidAmount,
                                            baseUrl: order.user.serverUrl,
                                            url: '/#!/orders/' + notification.source.order,
                                            products: order.products
                                        }, function (err, emailHTML) {
                                            var attachments = [];
                                            if (invoiceFilePath) {
                                                attachments.push({
                                                    filename: order.orderNumber + '-Invoice.pdf',
                                                    path: invoiceFilePath
                                                });
                                            }
                                            done(err, attachments, emailHTML);
                                        });
                                    });

                                }
                            });

                        }
                    });
                } else {
                    app.render('templates/order-creation', {
                        name: displayName,
                        appName: config.app.title,
                        title: notification.title
                    }, function (err, emailHTML) {
                        var attachments = [];
                        done(err, attachments, emailHTML);
                    });
                }

            },
            function sendNotification(attachments, emailHTML, done) {
                var email;
                if (notification.target.nVipaniUser) {
                    email = notification.target.nVipaniUser.email;
                } else if (notification.target.contact && notification.target.contact.emails && notification.target.contact.emails.length > 0) {
                    email = notification.target.contact.emails[0].email;
                }
                if (email) {
                    var smtpTransport = nodemailer.createTransport(config.mailer.options);
                    var mailOptions = {
                        to: email,
                        from: config.mailer.from,
                        subject: notification.title,
                        html: emailHTML
                    };
                    if (notificationEmail) {
                        mailOptions.replyTo = notificationEmail;
                    }
                    if (attachments && attachments.length > 0) {
                        mailOptions.attachments = attachments;
                    }
                    smtpTransport.sendMail(mailOptions, function (smtpErr) {
                        if (smtpErr) {
                            logger.error('Error while sending an email to ' + email + ' with notification details.');
                            done(smtpErr);
                        } else {
                            logger.debug('An email has been sent to ' + email + ' with notification details.');
                            done();
                        }

                    });
                } else {
                    logger.debug('Email notification could not be sent as no email present for the target contact.');
                    done(new Error('Email notification could not be sent as no email present for the target contact.'));
                }
            }
        ], function (err) {
            if (err) {
                return done(err);
            } else {
                done();
            }
        });
    }
};
/**
 * Processes Email notification
 * @param app
 * @param emailNotification
 * @param done
 */

exports.processEmailNotification = function (app, emailNotification, done) {
    if(!config.sendNotification){
        done();
    }else if (emailNotification.target && ((emailNotification.target.contact && emailNotification.target.contact.emails && emailNotification.target.contact.emails.length > 0) || (emailNotification.target.nVipaniUser && emailNotification.target.nVipaniUser.email))) {
        var displayName;

        if (emailNotification.target.nVipaniUser) {
            displayName = emailNotification.target.nVipaniUser.displayName;
        } else if (emailNotification.target.contact) {
            displayName = emailNotification.target.contact.displayName;
        }
        logger.debug('Processing the notification with title - ' + emailNotification.title + ', target contact -' + displayName + ' and type-' + emailNotification.type);
        this.sendNotification(app, emailNotification, function (sendNotificationErr) {
            if (!sendNotificationErr) {
                logger.debug('Processed the notification with title - ' + emailNotification.title + ', target contact -' + displayName+ ' and type-' + emailNotification.type);
                emailNotification.processed = true;
                emailNotification.statusMessage = 'Successfully processed the notification with title - ' + emailNotification.title + ', target contact -' + displayName + ' and type-' + emailNotification.type;
                emailNotification.save(function (saveNotificationErr) {
                    if (saveNotificationErr) {
                        logger.error('Error while updating the notification with title - ' + emailNotification.title, saveNotificationErr);
                        done(saveNotificationErr);
                    } else {
                        done();
                    }

                });
            } else {
                //notification.processed = true;
                emailNotification.statusMessage = 'Failed in processing the notification with title - ' + emailNotification.title + ', target contact -' + displayName+ ' and type-' + emailNotification.type + '. Error - ' + sendNotificationErr;
                logger.error('Error in processing the notification with title - ' + emailNotification.title + ', target contact -' +displayName+ ' and type-' + emailNotification.type, sendNotificationErr);
                emailNotification.save(function (saveNotificationErr) {
                    if (saveNotificationErr) {
                        logger.error('Error while updating the notification with title - ' + emailNotification.title, saveNotificationErr);
                        done(saveNotificationErr);
                    } else {
                        done();
                    }
                });
            }
        });
    } else {
        //notification.processed = true;
        emailNotification.statusMessage = 'Error while processing the notification with title-' + emailNotification.title + ' as notification target contact email(s) are not present.';
        logger.error('Error while processing the notification with title-' + emailNotification.title + ' as notification target contact email(s) are not present.');
        emailNotification.save(function (saveNotificationErr) {
            if (saveNotificationErr) {
                logger.error('Error while updating the notification with title - ' + emailNotification.title, saveNotificationErr);
                done(saveNotificationErr);
            } else {
                done();
            }
        });
    }
};

/**
 * Processes SMS Notification
 * @param notification
 * @param done
 */

exports.processSMSNotification = function (notification, done) {
    if(!config.sendNotification){
        done();
    }else if (notification.target && ((notification.target.contact && notification.target.contact.phones && notification.target.contact.phones.length > 0) || (notification.target.nVipaniUser && notification.target.nVipaniUser.mobile))) {
        /*var firstName;
        var lastName;
        var mobile;
        if (notification.target.nVipaniUser) {
            firstName = notification.target.nVipaniUser.firstName;
            lastName = notification.target.nVipaniUser.lastName;
            mobile = notification.target.nVipaniUser.mobile;
        } else if (notification.target.contact) {
            firstName = notification.target.contact.firstName;
            lastName = notification.target.contact.lastName;
            mobile = notification.target.contact.phones[0].phoneNumber;
        }*/
        var displayName;
        var mobile;

        if (notification.target.nVipaniUser) {
            displayName = notification.target.nVipaniUser.displayName;
            mobile = notification.target.nVipaniUser.mobile;
        } else if (notification.target.contact) {
            displayName = notification.target.contact.displayName;
            mobile = notification.target.contact.phones[0].phoneNumber;
        }

        logger.debug('Processing the SMS notification with title - ' + notification.title + ', target contact -' + displayName+ ' and type-' + notification.type);
        sms.sendSMS(mobile, notification.shortMessage, function (smsNotificationErr, smsResponse) {
            if (smsNotificationErr) {
                logger.error('Error while processing the SMS notification with title-' + notification.title, smsNotificationErr);
                logger.error('SMS to ' + mobile + ', response-' + smsResponse);
                notification.smsStatusMessage = 'Error while processing the SMS notification with title-' + notification.title;
            } else {
                notification.smsStatusMessage = 'Successfully processed the SMS notification with title - ' + notification.title + ', target contact -' + displayName+ ' and type-' + notification.type;
                logger.debug(notification.smsStatusMessage);
                logger.debug('SMS to ' + mobile + ', response-' + smsResponse);
                notification.smsProcessed = true;
            }
            notification.save(function (notificationSaveErr) {
                if (notificationSaveErr) {
                    logger.error('Error while updating the notification with title - ' + notification.title, notificationSaveErr);
                    done(notificationSaveErr);
                } else {
                    done();
                }

            });

        });
    } else {
        //notification.processed = true;
        notification.smsStatusMessage = 'Error while processing the SMS notification with title-' + notification.title + ' as notification target contact phone(s) are not present.';
        logger.error('Error while processing the SMS notification with title-' + notification.title + ' as notification target contact phone(s) are not present.');
        notification.save(function (err) {
            if (err) {
                logger.error('Error while updating the notification with title - ' + notification.title, err);
                done(err);
            } else {
                done();
            }

        });
    }
};

/**
 * Processes FCM Push Notification
 * @param notification
 * @param done
 */

exports.processFCMNotification = function (notification, done) {
    if (notification.target && notification.target.nVipaniUser && notification.target.nVipaniUser.devices && notification.target.nVipaniUser.devices.length > 0) {
        /* var firstName = notification.target.nVipaniUser.firstName;
         var lastName = notification.target.nVipaniUser.lastName;*/
        var displayName = notification.target.nVipaniUser.displayName;
        var devices = notification.target.nVipaniUser.devices;

        logger.debug('Processing the FCM notification with title - ' + notification.title + ' and type-' + notification.type + ', target user with username -' + displayName + ' and name -' +displayName);

        async.forEach(devices, function (device, callback) {
            logger.debug('Processing the FCM notification with title - ' + notification.title + ' and type-' + notification.type + ', target user with username -' + notification.target.nVipaniUser.username + ' and name -' + displayName + ' and for the device with token -' + device.token);
            fcm.sendMessageToUser(device.token, notification.title, notification.title, notification, function (fcmNotificationError, fcmResponse, body) {
                if (fcmNotificationError) {
                    logger.error('Error while processing the FCM notification with title - ' + notification.title + ' and type-' + notification.type + ', target user with username -' + notification.target.nVipaniUser.username + ' and name -' + displayName + ' and for the device with token -' + device.token);
                    logger.error('FCM to ' + device.token + ', response-' + fcmResponse + ' and body-' + body);
                    notification.pushStatusMessage = 'Error while processing the FCM notification with title - ' + notification.title + ' and type-' + notification.type + ', target user with username -' + notification.target.nVipaniUser.username + ' and name -' + displayName+ ' and for the device with token -' + device.token;
                } else {
                    notification.pushStatusMessage = 'Successfully processed the FCM notification with title - ' + notification.title + ' and type-' + notification.type + ', target user with username -' + notification.target.nVipaniUser.username + ' and name -' + displayName+ ' and for the device with token -' + device.token;
                    logger.debug(notification.pushStatusMessage);
                    logger.debug('FCM to ' + device.token + ', response-' + fcmResponse + ' and body-' + body);
                    notification.pushProcessed = true;
                }
                notification.save(function (notificationSaveErr) {
                    if (notificationSaveErr) {
                        logger.error('Error while updating the notification with title - ' + notification.title, notificationSaveErr);
                        callback(notificationSaveErr);
                    } else {
                        callback();
                    }

                });

            });
        }, function (callbackError) {
            if (callbackError) {
                logger.error('Error while processing the FCM notification with title - ' + notification.title + ' and type-' + notification.type + ', target user with username -' + notification.target.nVipaniUser.username + ' and name -' + displayName, callbackError);
                done(callbackError);
            } else {
                done();
            }
        });
    } else {
        //notification.processed = true;
        notification.pushStatusMessage = 'Error while processing the FCM notification with title-' + notification.title + ' as notification target contact devices are not present.';
        logger.error('Error while processing the FCM notification with title-' + notification.title + ' as notification target contact devices are not present.');
        notification.save(function (err) {
            if (err) {
                logger.error('Error while updating the notification with title - ' + notification.title, err);
                done(err);
            } else {
                done();
            }

        });
    }
};

/**
 * Processes Notification
 * @param app
 * @param notification
 * @param done
 */
exports.processNotification = function (app, notification, done) {
    if(!config.sendNotification){
        done();
    }else{
        Notification.findById(notification._id).populate('target.contact target.nVipaniUser', 'firstName displayName lastName emails phones devices email mobile').exec(function (err, resultNotification) {
            if (!err) {
                notificationUtil.processEmailNotification(app, resultNotification, function (emailErr) {
                    if (emailErr) {
                        logger.error('Error while processing the email notification', emailErr);
                    }
                    if (!resultNotification.smsProcessed) {
                        notificationUtil.processSMSNotification(resultNotification, function (smsErr) {
                            if (smsErr) {
                                logger.error('Error while processing the sms notification', smsErr);
                            }
                            if (!resultNotification.pushProcessed) {
                                notificationUtil.processFCMNotification(resultNotification, function (fcmErr) {
                                    if (fcmErr) {
                                        done(fcmErr);
                                    } else {
                                        done();
                                    }

                                });
                            }
                        });
                    } else if (!resultNotification.pushProcessed) {
                        notificationUtil.processFCMNotification(resultNotification, function (fcmErr) {
                            if (fcmErr) {
                                done(fcmErr);
                            } else {
                                done();
                            }
                        });
                    }
                });
            } else {
                done(err);
            }

        });
    }
};

