'use strict';
var fs = require('fs'),
    mongoose = require('mongoose'),
    BusinessUnit = mongoose.model('BusinessUnit'),
    async = require('async');

/**
 * Module dependencies.
 */

function populateWithFindOneQuery(query,populateString,done) {
    query.push({deleted:{$exists: true}});
    query.push({deleted:false});
    var findQueryStatement=BusinessUnit.findOne({$and:query});
    if(populateString){
        findQueryStatement=findQueryStatement.populate(populateString);

    }
    findQueryStatement.exec(function(businessUnitError, fetchBusinessUnit) {
        if (businessUnitError) {
            done(businessUnitError, null);
        } else if (fetchBusinessUnit) {
            done(null, fetchBusinessUnit);
        } else {
            done(new Error('No Business unit'), null);
        }
    });
}
exports.findOneBusinessUnit=function (query,populatedLevel ,done) {
    if(populatedLevel===0) {
        populateWithFindOneQuery(query,['company'],function (err,businessUnit) {
            done(err,businessUnit);
        });

    } else if(populatedLevel === 1){
        populateWithFindOneQuery(query,['company','employees.user'],function (err,businessUnit) {
            done(err,businessUnit);
        });
    } else if(populatedLevel === 2){
        populateWithFindOneQuery(query,['company','employees.user','employees.userGroup'],function (err,businessUnit) {
            done(err,businessUnit);
        });
    } else{
        populateWithFindOneQuery(query,null,function (err,businessUnit) {
            done(err,businessUnit);
        });
    }
};
exports.findDefaultUserBusinessUnit=function(businessId,done){

        BusinessUnit.findById(businessId,function (unitErr, bUnit) {
            if (unitErr) {
                done(unitErr, null);
            } else {
                if (bUnit && bUnit.employees.length > 0) {
                    var employee=bUnit.employees.filter(function (eachemployee) {
                        return eachemployee.incharge===true;
                    });
                    if(employee.length>0) {
                        done(null, employee[0].user);
                    }else{
                        done(new Error('No Default Employee'),null);
                    }

                } else {
                    done(new Error('No default user with the Business Unit'), null);
                }

            }
        });
};
exports.findQuery=function(query,selectedQuery ,populateString,done){
    query.push({deleted:{$exists: true}});
    query.push({deleted:false});
    BusinessUnit.find({$and:query},selectedQuery).populate('company').exec(function (businessUnitError,businessUnits) {
        if (businessUnitError) {
            done(businessUnitError, null);
        } else if (businessUnits) {
            done(null, businessUnits);
        } else {
            done(new Error('No Business units'), null);
        }

    });
};
