'use strict';
var mongoose = require('mongoose'),
    BusinessSegments = mongoose.model('BusinessSegments'),
    async=require('async'),
    Category=mongoose.model('Category'),
    BusinessSegments=mongoose.model('BusinessSegments'),
    contactUtil = require('./common.contacts.util'),
    orderUtil=require('./common.orders.util'),
    _this=this,
    Offer = mongoose.model('Offer');
/**
 * populateLevels 2 means populate only two levels with Item Masters ,Stock Master ,Company
 * populateLevels 3 means populate only three levels with Brand Master
 * populateLevels 4 means populate only four levels with Product Master
 */
function nestedProductPopulate(offer,populateLevels,done) {
    Offer.populate(offer, [{
        path: 'products.inventory.itemMaster',
        model: 'ItemMaster'
    },{path: 'products.inventory.stockMasters',
        model: 'StockMaster'},{
        path: 'user.company',
        model: 'Company',
        select: 'name profileUrl profileImageURL addresses'
    },{
        path: 'products.inventory.unitOfMeasure',
        model: 'UnitOfMeasure'
    },{
        path: 'products.inventory.hsncode',
        model: 'Hsncodes'
    },{
        path: 'products.inventory.taxGroup',
        model: 'TaxGroup'
    },{
        path: 'applicablePaymentTerms.paymentTerm',
        model: 'PaymentTerm'
    },{
        path: 'applicablePaymentModes.paymentMode',
        model: 'PaymentMode'
    }], function (nestedErr, populatedStockMasterOffers) {
        if (nestedErr) {
            done(nestedErr);
        } else {
            if( populateLevels>=3) {
                Offer.populate(populatedStockMasterOffers, [{
                    path: 'products.inventory.itemMaster.productBrand',
                    model: 'ProductBrand'
                }], function (nestedBrandErr, populatedBrandOffers) {
                    if (nestedBrandErr) {
                        done(nestedBrandErr);
                    } else {
                        if( populateLevels===4) {
                            Offer.populate(populatedBrandOffers, [{
                                path: 'products.inventory.itemMaster.productBrand.productCategory',
                                model: 'Category'
                            }], function (nestedCategoryErr, populatedBrandCategoryOffers) {
                                if (nestedCategoryErr) {
                                    done(nestedCategoryErr);
                                } else {
                                    done(populatedBrandCategoryOffers);
                                }
                            });
                        }else{
                            done(populatedBrandOffers);
                        }
                    }
                });
            }else{
                done(populatedStockMasterOffers);
            }
        }
    });
}
function getNotNullProductsOffers(offers) {
    return offers.filter(function (eachOffer) {
        return (eachOffer.products.filter(function (eachProduct) {
            return eachProduct.inventory !== undefined && eachProduct.inventory && eachProduct.inventory._id;
        })).length>0;
    });
}
/*
 * find Offer with populate tables
 * populateLevels -2 means -No population table
 * populateLevels -1 means populate for the old Apps
 * populateLevels 0 means populate with pagination
 * populateLevels 1 means populate only one level
 * populateLevels 2 means populate only two levels with Item Masters ,Stock Master ,Company
 * populateLevels 3 means populate only three levels with Brand Master
 * populateLevels 4 means populate only four levels with Product Master
 */
function findQueryByOffers(query,populateLevels,isFilterSummary,pageOptions,offerType, done) {
    query.push({deleted:{$exists: true}});
    query.push({deleted:false});
    // query.push({deleted:{$exists: false}});
    if(populateLevels===-2) {
        Offer.find({$and:query}).exec(function (err, offers) {
            done(err,offers);
        });
    }else if(populateLevels===-3){
        Offer.find({$and:query},'-updateHistory -paymentOptions -sampleRequests -statusHistory -notificationsContacts').sort('-created').populate('user', 'displayName company').populate('toContacts.contacts.contact toContacts.groups.group user', 'firstName lastName middleName name company displayName').populate('products.inventory','-offerUnits -saleUnitPrice -buyUnitPrice -updateHistory -numberOfUnitsHistory -offerUnitsHistory -buyOfferUnitsHistory -moqAndPrice -croppedInventoryImageURL2 -croppedInventoryImageURL3 -croppedInventoryImageURL4 -inventoryImageURL2 -inventoryImageURL3 -inventoryImageURL4').exec(function (err, offers) {
            if (err) {
                done(err,null);
            } else {
                Offer.populate(offers, [/*{
                    path: 'products.inventory.product',
                    model: 'Product',
                    select: 'name productImageURL1 unitSize unitMeasure unitPrice numberOfUnits sampleNumber fssaiLicenseNumber description user'
                },*/{
                    path: 'owner.company',
                    model: 'Company',
                    select: 'name profileUrl profileImageURL addresses'
                }], function (nestedErr, populatedOffers) {
                    done(nestedErr,populatedOffers);

                });

            }
        });

    }else if(populateLevels===-1){
        Offer.find({$and:query},'-updateHistory -paymentOptions -products -sampleRequests -statusHistory -notificationsContacts').sort('-created').populate('user', 'displayName company').populate('toContacts.contacts.contact toContacts.groups.group user', 'firstName lastName middleName name company displayName').populate('products.inventory','-offerUnits -saleUnitPrice -buyUnitPrice -updateHistory -numberOfUnitsHistory -offerUnitsHistory -buyOfferUnitsHistory -moqAndPrice -croppedInventoryImageURL2 -croppedInventoryImageURL3 -croppedInventoryImageURL4 -inventoryImageURL2 -inventoryImageURL3 -inventoryImageURL4').exec(function (err, offers) {
            if (err) {
                done(err,null);
            } else {
                Offer.populate(offers, [/*{
                    path: 'products.inventory.product',
                    model: 'Product',
                    select: 'name productImageURL1 unitSize unitMeasure unitPrice numberOfUnits sampleNumber fssaiLicenseNumber description user'
                },*/{
                    path: 'owner.company',
                    model: 'Company',
                    select: 'name profileUrl profileImageURL addresses'
                }], function (nestedErr, populatedOffers) {
                    done(nestedErr,populatedOffers);

                });

            }
        });

    }else if(populateLevels===0){
        Offer.count({$and:query},function(err,count) {
            Offer.find({$and: query}, '-updateHistory -paymentOptions -products -sampleRequests -statusHistory -notificationsContacts').sort('-created').populate('owner.company').populate('user', 'displayName company').populate('toContacts.contacts.contact toContacts.groups.group user', 'firstName lastName middleName name company displayName').populate('products.inventory', '-offerUnits -saleUnitPrice -buyUnitPrice -updateHistory -numberOfUnitsHistory -offerUnitsHistory -buyOfferUnitsHistory -moqAndPrice -croppedInventoryImageURL2 -croppedInventoryImageURL3 -croppedInventoryImageURL4 -inventoryImageURL2 -inventoryImageURL3 -inventoryImageURL4').skip(pageOptions.page > 0 ? ((pageOptions.page - 1) * pageOptions.limit) : 0).limit(pageOptions.limit).exec(function (err, offers) {
                if (err) {
                    done(err,null);
                } else {
                    Offer.populate(offers, [{
                        path: 'owner.company',
                        model: 'Company',
                        select: 'name profileUrl profileImageURL addresses'
                    }], function (nestedErr, populatedOffers) {
                        if(isFilterSummary && (!pageOptions.page || pageOptions.page === 1)){
                            findQueryByOffers(query,-1,null,null,offerType,function (err,offers) {
                                if(offers) {
                                    _this.getOffersSummary(offers,null, offerType,function (summaryData) {
                                        done(err,{offer: populatedOffers, total_count: count,summaryData:summaryData});
                                    });
                                }else {
                                    done(err,null);
                                }
                            });
                        }else {
                            done(null,{offer: populatedOffers, total_count: count});
                        }
                    });

                }
            });
        });
    }else if(populateLevels===2){
        // public offers which are available for All Users
        Offer.find({isPublic:true,deleted:false,'offerStatus':'Opened'},'-updateHistory -paymentOptions -sampleRequests -statusHistory -notificationsContacts').populate('owner.company','name profileUrl profileImageURL addresses').sort('-created').populate({path:'products.inventory',match:query[0]}).exec(function (err, offers) {
            if(err){
                done(err,null);
            }else{
                done(null,getNotNullProductsOffers(offers));
            }
        });
    }else if(populateLevels===3){
        // public offers which are available for All Users with filter
        Offer.find({isPublic:query[0].isPublic,deleted:false,'offerStatus':'Opened','company':query[0].company},'-updateHistory -paymentOptions -sampleRequests -statusHistory -notificationsContacts').populate('owner.company','name profileUrl profileImageURL addresses').sort('-created').exec(function (err, offers) {
            if(err){
                done(err,null);
            }else{
                done(null,offers);
            }
        });
    }else if(populateLevels===4){
        // public offers which are available for All Users with filter
        Offer.aggregate([{$match:{'isPublic':query[0].isPublic,'deleted':false,'offerStatus':'Opened','company':query[0].company}},
            {
                $group: {
                    _id: { company: '$company'},
                    offerDetails: {
                        $push: '$$ROOT'

                    }
                }
            }]).exec(function(err,companyOffers) {
            done(err,companyOffers);

        });
        /* Offer.find({isPublic:true,deleted:false},'-updateHistory -paymentOptions -sampleRequests -statusHistory -notificationsContacts').sort('-created').populate({path:'products.inventory',match:query}).exec(function (err, offers) {
             if(err){
                 done(err,null);
             }else{
                 done(null,offers);
             }
         });*/
    }else{
        Offer.find({$and:query},'-updateHistory -paymentOptions -sampleRequests -statusHistory -notificationsContacts').sort('-created').populate('user', 'displayName company').populate('toContacts.contacts.contact toContacts.groups.group user', 'firstName lastName middleName name company displayName').populate('products.inventory','-offerUnits -saleUnitPrice -buyUnitPrice -updateHistory -numberOfUnitsHistory -offerUnitsHistory -buyOfferUnitsHistory -moqAndPrice -croppedInventoryImageURL2 -croppedInventoryImageURL3 -croppedInventoryImageURL4').exec(function (err, offers) {
            if (err) {
                done(err,null);
            } else {
                if(populateLevels >1) {
                    nestedProductPopulate(offers,populateLevels,function(populateOffers) {
                        if(populateOffers instanceof Error){
                            done(populateOffers,null);
                        }else {
                            done(null,populateOffers);
                        }

                    });
                }else{
                    Offer.populate(offers, {
                        path: 'user.company',
                        model: 'Company',
                        select: 'name profileUrl profileImageURL addresses'
                    }, function (companyNestedErr, companyPopulatedResultOffers) {
                        done(companyNestedErr,companyPopulatedResultOffers);

                    });
                }
            }
        });

    }
}
exports.findQueryByOffers=function(query,populateLevels,isFilterSummary,pageOptions,offerType, done){
    findQueryByOffers(query,populateLevels,isFilterSummary,pageOptions,offerType, function (err,offers) {
        done(err,offers);
    });


};
/**
 * fetch offer Id with populate Data
 * populateLevels -2 means populate for no population for Notification Util
 * populateLevels -1 means populate for no population for message
 * populateLevels 0 means populate for the old Apps
 * populateLevels 1 means populate only one level
 * populateLevels 2 means populate only two levels with Item Masters ,Stock Master ,Company
 * populateLevels 3 means populate only three levels with Brand Master
 * populateLevels 4 means populate only four levels with Product Master
 */

exports.fetchOfferById=function(offerId,populateLevels, done) {
    if (populateLevels <= -1) {
        Offer.findById(offerId).exec(function (err, resultOffer) {
            if(err) {
                done(err,null);
            }else{
                if (populateLevels === -2) {
                    Offer.populate(resultOffer, [{
                        path: 'user',
                        model: 'User',
                        select: 'email username displayName serverUrl'
                    }], function (nestedErr, populatedResultOffer) {
                        done(nestedErr,populatedResultOffer);
                    });
                }else if (populateLevels === -3) {
                    Offer.populate(resultOffer, [{
                        path: 'products.inventory',
                        model: 'Inventory',
                        select: '-offerUnits -saleUnitPrice -buyUnitPrice -updateHistory -numberOfUnitsHistory -offerUnitsHistory -buyOfferUnitsHistory -moqAndPrice -croppedInventoryImageURL2 -croppedInventoryImageURL3 -croppedInventoryImageURL4 -inventoryImageURL2 -inventoryImageURL3 -inventoryImageURL4'
                    },{
                        path: 'user',
                        model: 'User',
                        select: 'email username displayName serverUrl'
                    },{
                        path: 'owner.company',
                        model: 'Company',
                        select: 'name profileUrl'
                    },{
                        path: 'owner.businessUnit',
                        model: 'BusinessUnit',
                        select: 'name addresses emails phones'
                    }], function (nestedInventoryErr, populatedInventorResultOffer) {
                        /* if (nestedInventoryErr) done(nestedInventoryErr);
                         else */done(nestedInventoryErr,populatedInventorResultOffer);
                    });
                }else{
                    done(null,resultOffer);
                }
            }
        });
    }else if (populateLevels === 0) {
        Offer.findById(offerId).populate('toContacts.contacts.contact toContacts.groups.group notificationsContacts.nVipaniUser user updateHistory.modifiedBy', 'username firstName lastName middleName company name contactImageURL croppedContactImageURL groupClassification groupType groupImageURL croppedGroupImageURL contacts displayName').populate('products.inventory', '-updateHistory -numberOfUnitsHistory -offerUnitsHistory -buyOfferUnitsHistory').exec(function (err, resultOffer) {
            if (err) {
                // logger.error('Error while loading the offer details with offer id - ' + offerId);
                done(err);
            } else {
                Offer.populate(resultOffer, [{
                    path: 'products.inventory.product',
                    model: 'Product',
                    select: 'name productImageURL1'
                }, {
                    path: 'user.company',
                    model: 'Company',
                    select: 'name profileUrl profileImageURL addresses'
                }], function (nestedErr, populatedResultOffer) {
                    if (nestedErr) {
                        done(nestedErr,null);
                    }else {
                        done(nestedErr, populatedResultOffer);
                    }

                });
            }

        });
    } else if (populateLevels >= 1) {
        Offer.findById(offerId).populate('toContacts.contacts.contact toContacts.groups.group notificationsContacts.nVipaniUser user owner.company updateHistory.modifiedBy', 'username firstName lastName middleName company name contactImageURL croppedContactImageURL groupImageURL croppedGroupImageURL contacts groupType groupClassification displayName').populate('products.inventory', '-updateHistory -numberOfUnitsHistory -offerUnitsHistory -buyOfferUnitsHistory').populate('statusHistory.user','displayName profileImageURL').exec(function (err, resultOffer) {
            if (err) {
                //logger.error('Error while loading the offer details with offer id - ' + offerId);
                done(err);
            } else {
                if(populateLevels>1) {
                    nestedProductPopulate(resultOffer,populateLevels,function(populateOffer) {
                        if(populateOffer instanceof Error){
                            done(populateOffer,null);
                        }else {
                            done(null,populateOffer);
                        }

                    });
                }else{
                    Offer.populate(resultOffer, {
                        path: 'user.company',
                        model: 'Company',
                        select: 'name profileUrl profileImageURL addresses'
                    }, function (companyNestedErr, companyPopulatedResultOffer) {
                        if (companyNestedErr) {
                            done(companyNestedErr,companyPopulatedResultOffer);
                        }else {
                            done(companyNestedErr, companyPopulatedResultOffer);
                        }
                    });
                }

            }
        });
    }
};
exports.batchOfferUpdate=function(query,update,done){
    Offer.update(
        query,
        {$set:update},
        {multi: true},
        function(err,updateOffers){
            done(err,updateOffers);
        }
    );
};

function segmentCategories(segmentCategories,done) {
    var categories=[];
    async.forEachSeries(segmentCategories, function (eachCategory, categoryCallback) {
        if (eachCategory.category) {
            categories.push(eachCategory.category.toString());
        }
        categoryCallback();
    }, function (errCategory) {
        done(errCategory, categories);
    });

}
function findCategoriesBySegments(segments,done) {
    var categories=[];
    if(segments && segments.length>0) {
        async.forEachSeries(segments, function (eachSegment, segmentCallback) {
            segmentCategories(eachSegment.categories, function (err, eachSegmentCategories) {
                if (err) {
                    segmentCallback(err);
                } else {
                    categories.concat(eachSegmentCategories);
                    segmentCallback();
                }
            });

        }, function (err) {
            done(err, categories);
        });
    }else{
        done(null,categories);
    }

}
function getCategoriesQuery(segments,segment,filterCategory,done) {
    if(segment){
        segmentCategories(segment.categories, function (err, categories) {
            done(err, [{'productCategory': {$in: categories}}]);
        });
    }
    else if(filterCategory){
        Category.findById(filterCategory,function (fetchCategoryErr,fetchCategory) {
            if(fetchCategory.type==='MainCategory') {
                done(fetchCategoryErr,[{'productMainCategory':fetchCategory._id}]);
            }else if(fetchCategory.type==='SubCategory1'){
                done(fetchCategoryErr,[{'productSubCategory':fetchCategory._id}]);
            }else if(fetchCategory.type==='SubCategory2'){
                done(fetchCategoryErr,[{'productCategory':fetchCategory._id}]);
            }
        });

    }else {
        findCategoriesBySegments(segments,function (err,categories) {
            done(err, [{'productCategory':{$in:categories}}]);
        });

    }
}

// Populate company Segment Main Category, SubCategory,Product Category Related offers

exports.segmentsCategoriesOffers=function (company,segment,category,done) {
    getCategoriesQuery(company.segments,segment,category,function (err,categories) {
        if(err){
            done(err,null);
        }else{
            findQueryByOffers(categories,2,null,null,'own',function (errOffer,offers) {
                done(errOffer,offers);
            });
        }

    });

};
function getCompanyContactQuery(company,logger,done) {
    var contactQuery = [];
    logger.debug('Find company contacts for the offers with company Id :' + company);
    contactUtil.findQueryByContacts([{
        'company': company.toString(),
        'nVipaniCompany': {$exists: true, $nin: [null, company]}
    }], 2, false, null, function (queryErr, companyContacts) {
        if (queryErr) {
            done(queryErr, null);
        } else if (!companyContacts) {
            done('No Contacts', null);
        } else {
            async.forEachSeries(companyContacts, function (eachContact, callback) {
                if (eachContact.nVipaniCompany) {
                    contactQuery.push({
                        url: eachContact.nVipaniCompany.profileUrl,
                        name: eachContact.nVipaniCompany.name,
                        _id: eachContact.nVipaniCompany._id,
                        offers: []
                    });
                }
                callback();
            }, function (err) {
                done(null, contactQuery);
            });
        }


    });

}
// Populate company contact offers 
exports.companyContactsOffers=function(company,user,logger,done) {
    if(company===user.company._id) {
        getCompanyContactQuery(company, logger, function (err, contactQuery) {
            if (err) {
                done(err, null);
            } else if (contactQuery.length === 0) {
                logger.debug('No company contacts for the offers with company Id :' + company + JSON.stringify(contactQuery));
                done(null, []);
            } else if (contactQuery.length > 0) {
                logger.debug('found company contacts for the offers with company Id :' + company + JSON.stringify(contactQuery));
                findQueryByOffers([{
                    'company': contactQuery[0]._id,
                    isPublic: true
                }], 3, null,null,'own', function (errOffer, offers) {
                    contactQuery[0].offers = offers;
                    done(errOffer, contactQuery);
                });
            } else {
                done(new Error('No proper query'), null);
            }
        });
    }else{
        findQueryByOffers([{
            'company': company,
            isPublic: true
        }], 3, false, null, 'own',function (errOffer, offers) {
            var contactOffers=[{_id:company,offers:offers}];

            done(errOffer, contactOffers);
        });
    }

};

// Populate Company Segment Related offers
exports.companySegmentsOffers=function (company,done) {
    findCategoriesBySegments(company.segments,function (err,categories) {
        Category.find({_id:{$in:categories}},function (categoryErr,categories) {
            done(categoryErr,categories);
        });
    });
};
exports.isOfferProductCriteriaSpecified=function(offer) {
    return offer && offer.products && offer.products.length === 0 && offer.inventoryItemsClassification  && offer.inventoryItemsClassification.classificationType === 'Criteria' && (offer.inventoryItemsClassification.classificationCriteria.criteriaType === 'Custom' || offer.inventoryItemsClassification.classificationCriteria.criteriaType === 'All');
};
function offerProductInventoryQuery(products) {
    var query=[];
    products.forEach(function (eachProduct) {
        if(eachProduct.inventory)
            query.push({_id:eachProduct.inventory.toString()});
    });
    return query;
}
function findValidProducts(offer,inventoryUtil,done) {
    if (_this.isOfferProductCriteriaSpecified(offer)) {
        done(null, true);
    } else {
        /*   if(offer.offerType==='Buy'){
               done(new Error('Looking for what type of products'), false);
           }else*/ if (offer.products && offer.products instanceof Array) {
            if (offer.products.length ===0) {
                done(new Error('At least one product is required to create offer'), false);
            }else{
                var query=offerProductInventoryQuery(offer.products);
                if(query.length>0) {
                    inventoryUtil.findQueryByInventories([{
                        businessUnit: offer.businessUnit,
                        $or: query
                    }], -3, false, null, function (errProducts, finalProducts) {
                        if (errProducts) {
                            done(errProducts, false);
                        } else {
                            if (finalProducts.length === offer.products.length) {
                                done(null, false);
                            } else {
                                done(new Error('Invalid Products with Offer Business Unit'), false);
                            }
                        }
                    });
                }else{
                    done(new Error('Products are required'), false);
                }

            }
        }else{
            done(new Error('Products are required'), false);
        }
    }
}

exports.offerDataValidation=function (offer,inventoryUtil,logger,done) {
    if(!offer.name){
        logger.error('Offer Name is required to offer :'+JSON.stringify(offer));
        done(new Error('Offer Name is required to create offer'));
    }else if(!offer.offerType || !(offer.offerType==='Sell' || offer.offerType==='Buy')) {
        logger.error('Offer Type is required to offer :'+JSON.stringify(offer));
        done(new Error('Offer Type is required to create offer'));
    }else if(!(offer.products  || _this.isOfferProductCriteriaSpecified(offer))){
        logger.error('Products are required to place offer :'+JSON.stringify(offer));
        done(new Error('Products are required to create offer'));
    }else {
        findValidProducts(offer, inventoryUtil, function (productErr, finalProducts) {
            if (productErr) {
                done(productErr);
            } else {
                done(null);
            }
        });
    }


};
exports.getOrdersByOffer=function(offerId,user,done) {
    findQueryByOffers([{'owner.company': user.company, _id: offerId}], -2, null, null,'own', function (offersErr, offers) {
        if(offersErr){
            done(offersErr,null);
        }else if(offers.length ===0){
            done(new Error('No able to view the orders for this offer'+offerId),null);
        }else {
            var query = {$and: [{disabled: false}, {offer: {$exists: true}}, {offer: offerId}]};
            orderUtil.findQueryByOrders([query], 1, null,null, function (ordersError, orders) {
                done(ordersError, orders);
            });
        }
    });
};


exports.getOffersSummary=function (offers,summaryFilters,offerType,done) {
    var newOffers = []; // Opened
    var sellOffers = []; // offer type Sell
    var buyOffers = []; // offer type Buy
    var expiredOffers = []; // Valid till date over
    var draftOffers = []; // Drafted offers
    var offersSummary = {};
    var intraOffers=[];
    async.each(offers, function (offer, callback) {
        if(offer.offerStatus && (offer.offerStatus === 'Opened' || offer.offerStatus === 'Open')){
            newOffers.push(offer);
        }
        if(offer.offerType && offer.offerType === 'Sell'){
            sellOffers.push(offer);
        }
        if(offer.offerType && offer.offerType === 'Buy'){
            buyOffers.push(offer);
        }
        if(offer.offerStatus && offer.offerStatus === 'Expired'){
            expiredOffers.push(offer);
        }
        if (offer.offerStatus && offer.offerStatus === 'Drafted') {
            draftOffers.push(offer);
        }
        if(offer.isIntraCompany){
            intraOffers.push(offer);
        }

        callback();
    }, function (eachErr) {
        if(!eachErr){
            offersSummary.New=(summaryFilters&&summaryFilters.New)?newOffers:newOffers.length;
            offersSummary.Sell=(summaryFilters&&summaryFilters.Sell)?sellOffers:sellOffers.length;
            offersSummary.Buy=(summaryFilters&&summaryFilters.Buy)?buyOffers:buyOffers.length;
            offersSummary.Expired=(summaryFilters&&summaryFilters.Expired)?expiredOffers:expiredOffers.length;
            if(offerType==='own')
            offersSummary.Draft=(summaryFilters&&summaryFilters.Draft)?draftOffers:draftOffers.length;
            offersSummary.Intra=(summaryFilters&&summaryFilters.Intra)?intraOffers:intraOffers.length;
            offersSummary.Count=offers.length;
        }
    });
    done(offersSummary);
};
