'use strict';
var mongoose = require('mongoose'),
    async = require('async'),
    errorHandler = require('../errors.server.controller'),
    logger = require('../../../lib/log').getLogger('INVENTORY', 'DEBUG'),
    Groups = mongoose.model('Group'),
    BusinessUnit = mongoose.model('BusinessUnit'),
    PaymentTerms = mongoose.model('PaymentTerm'),
    offerUtil = require('./common.offer.util'),
    orderUtil=require('./common.orders.util'),
    _this=this,
    Contacts = mongoose.model('Contact');
/*
 Add global Code for Contacts
 */

/**
 * find a Contacts with specific query fields with pagination
 */
exports.findQueryByContacts=function(query,populateLevels,isFilterSummary,pageOptions, done) {
    /*query.push({deleted:{$exists: true}});*/
    query.push({deleted:false});
    if(populateLevels===-2) {
        Contacts.find({$and:query}).exec(function (errContact, contact) {
            done(errContact, contact);
        });
    }else if(populateLevels===-1) {
        Contacts.find({$and:query}).sort('-created').populate('user', 'displayName').exec(function (errContact, contact) {
            done(errContact, contact);
        });
    }else if(populateLevels===1) {
        Contacts.find({$and:query},'user nVipaniUser nVipaniCompany').sort('-created').populate('user nVipaniUser', 'displayName').exec(function (errContact, contact) {
            done(errContact, contact);
        });
    }else if(populateLevels===2) {
            Contacts.find({$and:query},'nVipaniCompany').populate('nVipaniCompany', 'name profileUrl').sort('-created').exec(function (errContact, contact) {
               done(errContact, contact);
            });

    }else if(populateLevels===0) {
        Contacts.count({$and:query},function(err,count){
            Contacts.find({$and:query}).skip(pageOptions.page > 0 ? ((pageOptions.page-1) * pageOptions.limit) : 0 ).limit(pageOptions.limit)
                .sort('-created').populate('user', 'displayName').exec(function (err, contacts) {
                if (err) {
                    done(err,null);
                } else {
                    if(isFilterSummary && (!pageOptions.page || pageOptions.page === 1)){
                        _this.findQueryByContacts(query,-1,null,null,function (err,listContacts) {
                            if(contacts) {
                                _this.getContactsSummary(listContacts,null, function (summaryData) {
                                    done(null,{contact: contacts, total_count: count,summaryData:summaryData});
                                });
                            }else {
                                done(err,null);
                            }
                        });
                    }else {
                        done(null,{contact: contacts, total_count: count});
                    }
                }
            });
        });
    }
};
/**
 * Get query for location field
 */
function getQuery(locations,done){
    var queryArray=[];

    async.forEach(locations, function (location, callback) {
        var regExp={'$regex': location.pinCode};
        var eachContact={};
        eachContact['addresses.pinCode'] = regExp;
        logger.debug(JSON.toString(eachContact));
        queryArray.push(eachContact);
        callback();
    },function (err) {
        if(err){
            done(err,null);
        }else{
            done(null,queryArray);
        }
    });

}

/**
 *
 * @param contacts
 * @param done
 */
function getAllContacts(contacts,isGroup,query,done){

    if(contacts && contacts.length>0) {
        async.forEachSeries(contacts, function (contact, callback) {
            var id= isGroup ? ((contact.contact instanceof Object)?(contact.contact._id?contact.contact._id:contact.contact.toString()):contact.contact): ((contact instanceof Object)? contact._id:contact);
            logger.debug('contact Id:'+JSON.stringify(contact));
            query.push({_id: isGroup ? ((contact.contact instanceof Object)?(contact.contact._id?contact.contact._id:contact.contact.toString()):contact.contact): ((contact instanceof Object)? contact._id:contact)});
            callback();
        }, function (err) {
            if (err) {
                done(err, null);
            } else {
                done(null, query);
            }
        });
    }else{
        done(null,query);
    }
}
/**
 *  Get All Group Contacts Query
 *
 * @param groups
 * @param queries
 * @param done
 */
function getGroupContacts(groups ,queries, done) {
    var queryArray=[];
    if(groups && groups.length>0) {
        async.forEach(groups, function (eachgroup, callback) {

            var group = eachgroup.group;

            Groups.findById(group).populate('contacts.contact').exec(function (err, group) {
                var query = {customerType: group.groupType};
                if (group.groupClassification.classificationType === 'Criteria') {
                    if (group.groupClassification.classificationCriteria.criteriaType === 'Custom') {
                        getQuery(group.groupClassification.classificationCriteria.location, function (err, queryArray) {
                            var finalQuery = null;
                            if (queryArray.length > 0)
                                finalQuery = {$and: [query, {$or: queryArray}]};
                            else finalQuery = query;

                            queries.push(finalQuery);
                            callback();

                        });
                    } else if (group.groupClassification.classificationCriteria.criteriaType === 'All') {
                        queries.push(query);
                        callback();
                    }
                } else if (group.groupClassification.classificationType === 'Custom') {
                    getAllContacts(group.contacts, true, queries, function (err, finalQuery) {
                        if (finalQuery && finalQuery.length > 0)
                            queries = queries.concat(finalQuery);
                        callback();
                    });
                }
            });
        }, function (err) {
            if (err) {
                done(err, null);
            } else {
                done(null, queries);
            }
        });

    }else{
        done(null,queries);
    }
}
/**
 * This api checks whether the contact is already added
 *
 * Returns matching contacts if contact has at least one phone matching or
 * at least one email matching and is the same type as the one being passed in.

 */
exports.matchContact = function(contact,user,done) {
    if (!contact) {
        done(new Error('Contact must be set to check whether contact is already added.'));
    }
    //RELAXED duplicate check for now.
    /*var phonesList = [];
    if(contact.phones && contact.phones.length>0){
        contact.phones.forEach(function(phone){
            phonesList.push(phone.phoneNumber);
        });
    }
    var emailsList = [];
    if(contact.emails && contact.emails.length>0){
        contact.emails.forEach(function(email){
            emailsList.push(email.email);
        });
    }
    if(phonesList.length>0 || emailsList.length>0) {
        var query = [];
        if (emailsList.length > 0) {
            query.push({'emails.email': {$in: emailsList}});
        }
        if (phonesList.length > 0) {
            query.push({'phones.phoneNumber': {$in: phonesList}});
        }
        Contacts.find({
            $and: [{'customerType': contact.customerType,'deleted':false},
                {$or: query}]
        }, function (contactFindErr, contacts) {
            if (contactFindErr) {
                done(contactFindErr);
            } else {
                done(null, contacts);
            }
        });

    }*/else{
        //this is an invalid case, shouldnt get here.
        done();
    }
};
function businessUnitInchargeQuery(units,done) {
    var bunitsQuery=[];
    units.forEach(function (eachUnit) {
        bunitsQuery.push({_id:eachUnit.businessUnit});
    });
    done(bunitsQuery);
}
function getInchargeUsers(employees) {
    if(employees instanceof Array && employees.length>0){
        return employees.filter(function (eachEmployee) { return eachEmployee.incharge===true;});
    }else{
        return [];
    }
}
/**
 *
 */
function getBusinessUnitInChargeContact(units,done){
    var finalQuery=[];
    businessUnitInchargeQuery(units,function (bunitsQuery) {
        BusinessUnit.find({$and:[{$or:bunitsQuery},{'employees.incharge':true}]},function (err,results){
            if(err){
                done(err,[]);
            }else{
                if(results instanceof Array && results.length>0) {
                    results.forEach(function (eachBusinessUnit) {
                        var inchargeUsers=getInchargeUsers(eachBusinessUnit.employees);
                        if(inchargeUsers && inchargeUsers.length===1) {
                            finalQuery.push({nVipaniUser:inchargeUsers[0].user});
                        }
                    });

                }
                done(null,finalQuery);
            }
        });

    });

}
/**
 * this api is used for offer contacts.
 * @param toContacts
 * @param query
 * @param done
 */
exports.findContactsQuery=function (toContacts,query,done) {

    /* query = gquery;*/
    if(toContacts.businessUnits && toContacts.businessUnits instanceof Array && toContacts.businessUnits.length>0){
        getBusinessUnitInChargeContact(toContacts.businessUnits,function (bError, bquery) {

            done(bError, [{$and:[{$or:bquery},{nVipaniRegContact:true}]}]);
        });
    }else {
        getAllContacts(toContacts.contacts, true, query, function (cError, cquery) {
            if (cError) {
                done(cError, null);
            } else {
                /* query = cquery;*/
                if (toContacts.groups && toContacts.groups.length > 0) {
                    getGroupContacts(toContacts.groups, cquery, function (gError, gquery) {
                        logger.debug(JSON.toString(gquery));
                        done(gError, gquery);
                    });
                } else {
                    done(null, cquery);
                }
            }
        });
    }

};
exports.batchContactUpdate=function(query,update,done){
    Contacts.update(
        query,
        {$set:update},
        {multi: true},
        function(err,updateContacts){
            done(err,updateContacts);
        }
    );
};

/*
 Add global Code for Groups
 */

/**
 * find a Groups with specific query fields with pagination
 */
exports.findQueryByGroups=function(query,populateLevels,isMobile,pageOptions, done) {
    /*query.push({deleted:{$exists: true}});*/
    query.push({deleted:false});
    if(populateLevels===-2) {
        Groups.find({$and:query}).exec(function (errGroup, group) {
            if(errGroup) done(errGroup,null);
            done(null,group);
        });
    }else if(populateLevels===-1) {
        Groups.find({$and:query}).sort('-created').populate('user', 'displayName').exec(function (errGroup, group) {
            if(errGroup) done(errGroup,null);
            done(null,group);
        });
    } else if(populateLevels===0) {
        if (pageOptions.page === 1) {
            Groups.count({$and: query}, function (err, count) {
                Groups.find({$and: query}).skip(pageOptions.page > 0 ? ((pageOptions.page - 1) * pageOptions.limit) : 0).limit(pageOptions.limit)
                    .sort('-created').populate('user', 'displayName').exec(function (err, groups) {
                    if (err) {
                        done(err, null);
                    } else {
                        if (isMobile) {
                            done(null, groups);
                        } else {
                            done(null, {group: groups, total_count: count});
                        }
                    }
                });
            });
        }else{
            Groups.count({$and: query}, function (err, count) {
                Groups.find({$and: query}).skip(pageOptions.page > 0 ? ((pageOptions.page - 1) * pageOptions.limit) : 0).limit(pageOptions.limit)
                    .sort('-created').populate('user', 'displayName').exec(function (err, groups) {
                    if (err) {
                        done(err, null);
                    } else {
                        if (isMobile) {
                            done(null, groups);
                        } else {
                            done(null, {group: groups, total_count: count});
                        }
                    }
                });
            });
        }
    }
};


exports.findByContactById=function(id,populateLevel,done) {
    Contacts.findById(id).populate('user', 'displayName').exec(function (err, contact) {
        if(err){
            done(err,null);
        }else{
            if(populateLevel>2){
                Contacts.populate(contact, [{
                    path: 'paymentTerm',
                    model: 'PaymentTerm'
                }], function (nestedPermentTermsErr, populatedPaymentTermsContact) {
                    if(nestedPermentTermsErr){
                        done(nestedPermentTermsErr,null);
                    }else{
                        if(populateLevel>3){
                            Contacts.populate(populatedPaymentTermsContact, [{
                                path: 'nVipaniUser',
                                model: 'User'
                            }], function (nestedPaymentTermnVipaniUserErr, populatedPaymentTermnVipaniUserContact) {
                                if(nestedPaymentTermnVipaniUserErr){
                                    done(nestedPaymentTermnVipaniUserErr,null);
                                }else{
                                    done(null,populatedPaymentTermnVipaniUserContact);
                                }
                            });
                        }else {
                            done(null, populatedPaymentTermsContact);
                        }
                    }
                });

            }else if(populateLevel>1){
                Contacts.populate(contact, [{
                    path: 'nVipaniUser',
                    model: 'User'
                }], function (nestednVipaniUserErr, populatednVipaniUserContact) {
                    if(nestednVipaniUserErr){
                        done(nestednVipaniUserErr,null);
                    }else{
                        done(null,populatednVipaniUserContact);
                    }
                });

            }else if(populateLevel>0){
                Contacts.populate(contact, [{
                    path: 'nVipaniUser',
                    model: 'User'
                    // select: field
                }], function (nestednVipaniUserErr, populatednVipaniUserContact) {
                    if(nestednVipaniUserErr){
                        done(nestednVipaniUserErr,null);
                    }else{
                        done(null,populatednVipaniUserContact);
                    }
                });

            }else {
                done(null, contact);
            }
        }

    });
};

exports.findPaymentTerm = function(ptName,done){
    PaymentTerms.findOne({name:ptName}).exec(function(ptErr,paymentTerm) {
        if (ptErr) {
            done(ptErr);
        }
        else if(!paymentTerm){
            done(new Error('PaymentTerm '+ptName +' was not found.'));
        }
        else {
            done(null, paymentTerm);
        }
    });
};
/**
 *
 * @param query
 * @param update
 * @param done
 */

exports.batchGroupUpdate=function(query,update,done){
    Groups.update(
        query,
        {$set:update},
        {multi: true},
        function(err,updateGroups){
            done(err,updateGroups);
        }
    );
};


function getPhoneQuery(phones,query,done) {
    async.forEach(phones,function (eachPhone) {
        query.push(eachPhone.phoneNumber);
    });
    done(query);
}
function getEmailQuery(emails,query,done) {
    async.forEach(emails,function (eachPhone) {
        query.push(eachPhone.email);
    });
    done(query);
}

exports.getContactPlayersQuery=function(contact,done) {
    var query=[];
    getPhoneQuery(contact.phones,query,function (contactQuery) {
        getEmailQuery(contact.emails,contactQuery,function (emailQuery) {
            done(emailQuery);
        });
    });
};
exports.getContactOffers=function (contactId,user,done) {
    _this.findQueryByContacts([{'company':user.company,_id:contactId}],-1,false,null,function (contactError,contact) {
        if(contactError){
            done(contactError,null);
        }else if(contact && contact.length===1){
            var query = {$and: [{disabled: false}, {'toContacts.contacts.contact': {$exists: true}}, {'toContacts.contacts.contact': contact[0]._id}]};
        offerUtil.findQueryByOffers([query],-1,null,null,'own',function (offersErr,offers) {
           done(offersErr,offers);
        });
        }else{
            done(new Error('No Data contact'),null);
        }

    });
    
};
exports.getContactOrders=function (contactId,user,done) {
    _this.findQueryByContacts([{'company':user.company,_id:contactId}],-1,false,null,function (contactError,contact) {
        if(contactError){
            done(contactError,null);
        }else if(contact && contact.length===1){
            var query = {$and: [{disabled: false},{$or:[{'buyer.contact': contact[0]._id}, {'seller.contact': contact[0]._id}]}]};
            orderUtil.findQueryByOrders([query],-1,null,null,function (offersErr,offers) {
                done(offersErr,offers);
            });
        }else{
            done(new Error('No Data contact'),null);
        }

    });


};
exports.getGroupOffers=function (groupId,user,done) {
    _this.findQueryByGroups([{'company':user.company,_id:groupId}],-1,false,null,function (groupError,groups) {
        if(groupError){
            done(groupError,null);
        }else if(groups && groups.length===1){
            var query = {$and: [{disabled: false}, {'toContacts.groups.group': {$exists: true}}, {'toContacts.groups.group': groups[0]._id}]};
            offerUtil.findQueryByOffers([query],-1,false,null,'own',function (offersErr,offers) {
                done(offersErr,offers);
            });
        }else{
            done(new Error('No Data Group'),null);
        }

    });

};

exports.getContactsSummary=function (contacts,summaryFilters,done) {
    var newlyAdded=[],inactive=[],contactSummary={};
    async.each(contacts,function (contact, callback) {
        if((!contact.nVipaniRegContact && contact.created && contact.created.getDate() === new Date(Date.now()).getDate())){
            newlyAdded.push(contact);
        }
        if(contact.disabled && contact.disabled === true){
            inactive.push(contact);
        }
        callback();
    },function (err) {
        if(!err) {
            contactSummary.NewlyAdded=(summaryFilters && summaryFilters.NewlyAdded)?newlyAdded:newlyAdded.length;
            contactSummary.Inactive=(summaryFilters && summaryFilters.Inactive)?inactive:inactive.length;
            contactSummary.Count=contacts.length;
        }
        done(contactSummary);
    });

};
exports.getUserContact=function (userId,done) {
    Contacts.findOne({nVipaniUser:userId,nVipaniRegContact:true},function (errContact,contacts) {
        if(errContact){
            done(errContact,null);
        }else if(contacts){
            done(null,contacts);
        }else{
            done(new Error('No contact found with the user'),null);
        }

    }) ;
};
