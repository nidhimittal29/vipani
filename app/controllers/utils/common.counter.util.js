'use strict';
var mongoose = require('mongoose'),
    async = require('async'),
    queryObject= {companyId:'companySeq.companies.company',businessUnitSequence:'companySeq.companies.businessUnitSeqNumber',customerContactSequence:'companySeq.companies.customerContactSeqNumber', supplierContactSeqNumber:'companySeq.companies.supplierContactSeqNumber',
        companySeqNumber:'companySeqNumber',orderSeqNumber:'companySeq.companies.businessUnits.orderSeqNumber',offerSeqNumber:'companySeq.companies.businessUnits.offerSeqNumber',invoiceSeqNumber:'companySeq.companies.businessUnits.invoiceSeqNumber',itemMasterSeqNumber:'companySeq.companies.businessUnits.itemMasterSeqNumber',
        userItemMasterSeqNumber:'companySeq.companies.businessUnits.itemMaster.userItemMasterSeqNumber',reportSeqNumber:'companySeq.companies.businessUnits.reportSeqNumber',batchSeqNumber:'companySeq.companies.businessUnits.itemMaster.inventories.inventory.batchSeqNumber'};




exports.getCounterQuery=function(field) {
    return queryObject[field];
};
exports.leftPadWithZeros = function (number, length) {
    var str = '' + number;
    while (str.length < length) {
        str = '0' + str;
    }

    return str;
};
exports.setCounterQuery=function(fieldValues,counter) {
    /* counter[queryObject[field]]=value;
     counter[queryObject[objectId]]=objectValue;*/
    for (var key in fieldValues) {
        var matchedFields=queryObject[key].split('.');
        if(matchedFields.length===1){
            counter[matchedFields[0]]=fieldValues[key];
        }else if(matchedFields.length===2){
            counter[matchedFields[0]][matchedFields[1]]=fieldValues[key];
        }else if(matchedFields.length===3){
            counter[matchedFields[0]][matchedFields[1]][matchedFields[2]]=fieldValues[key];
        }else if(matchedFields.length===4){
            counter[matchedFields[0]][matchedFields[1]][matchedFields[2]][matchedFields[3]]=fieldValues[key];
        }else if(matchedFields.length===5){
            counter[matchedFields[0]][matchedFields[1]][matchedFields[2]][matchedFields[3]][matchedFields[4]]=fieldValues[key];
        }
    }
    return counter;
};

function findOneAndUpdate(query,updateFields,arrayFilters,done) {
    mongoose.model('Counter').findOneAndUpdate(query, updateFields,
        arrayFilters
        , function (counterError, counter) {
        if(counterError){
            done(counterError,null);
        }else {
            if (counter && counter._id) {
                mongoose.model('Counter').findOne({_id: counter._id}, function (errorErr, finalCounter) {
                    done(errorErr, finalCounter);
                });
            } else {
                mongoose.model('Counter').find({},function (errorFind,finalObject) {
                    done(errorFind, finalObject[0]);
                })
            }
        }

        });
}
function getIndex(counter,key,id) {
    return counter.indexOf(counter.filter(function(eachObject){
        return id && eachObject[key].toString()===id.toString();
    })[0]);
}
exports.getNextBusinessUnitSequenceNumber=function (doc,done) {
    findOneAndUpdate({'companySeq.companies.company':doc.company}, {$inc: {'companySeq.companies.$[i].businessUnitSeqNumber': 1},$addToSet :   { "companySeq.companies.$[i].businessUnits" :{businessUnit:doc._id} }},{returnNewDocument:true,arrayFilters:[{'i.company': doc.company }]}, function (error, counter) {
        if (error) {
            done(error,null);
        }else if(counter){
            done(null,counter.companySeq.companies[getIndex(counter.companySeq.companies,'company',doc.company)].businessUnitSeqNumber);
        }else{
            done(new Error('No BusinessUnit Sequence Number'),null);
        }
    });

};
exports.getNextCompanySequenceNumber=function (doc,done) {

    mongoose.model('Counter').findOneAndUpdate({}, {$inc: {'companySeqNumber': 1}}, function (error, counter) {
        /*Counter.findOne({key:doc._id}, function (error, counter) {*/
        if (error) {
            done(error);

        }else if (counter === null) {
            var Counter= mongoose.model('Counter');
            var defaultCounter=new Counter();
            defaultCounter.companySeqNumber=1;
            defaultCounter.companySeq.companies.push({company:doc._id});
            defaultCounter.save(function (err) {
                if(err) {
                    done(err, defaultCounter);
                }else{
                    done(null,defaultCounter.companySeqNumber);
                }

            });
        } else {
            counter.companySeqNumber=+1;
            counter.companySeq.companies.push({'company':doc._id});
            counter.save(function (err) {
                if(err) {
                    done(err, counter);
                }else{
                    done(null,counter.companySeqNumber);
                }

            });
        }


    });


};
function getSupplierSequenceNumber(doc,done) {
    findOneAndUpdate({'companySeq.companies': {$elemMatch: {'company': doc.company}}}, {$inc: {'companySeq.companies.$[i].supplierContactSeqNumber': 1}}, {returnNewDocument:true,arrayFilters: [{'i.company': doc.company}]},function(error,counter){
        if (error) {
            done(error,null);
        }else if(counter){
            done(null,counter.companySeq.companies[getIndex(counter.companySeq.companies,'company',doc.company)].supplierContactSeqNumber);
        }else{
            done(new Error('No Supplier Sequence Number'),null);
        }
    });
}
function getCustomerSequenceNumber (doc,done) {
    findOneAndUpdate({'companySeq.companies': {$elemMatch: {'company': doc.company}}}, {$inc: {'companySeq.companies.$[i].customerContactSeqNumber': 1}}, {returnNewDocument:true,arrayFilters: [{'i.company': doc.company}]},function(error,counter){
        if (error) {
            done(error,null);
        }else if(counter){
            done(null,counter.companySeq.companies[getIndex(counter.companySeq.companies,'company',doc.company._id ?doc.company._id:doc.company)].customerContactSeqNumber);
        }else{
            done(new Error('No Customer Sequence Number'),null);
        }
    });
}
exports.getNextContactSeqenceNumber=function(doc,done){
    if(doc.customerType === 'Customer'){
        getCustomerSequenceNumber(doc,function (error,counter) {
            done(error,counter);
        });
    }else{
        getSupplierSequenceNumber(doc,function (error,counter) {
            done(error,counter);
        });
    }

};


exports.getNextItemMasterSequenceNumber=function (doc,done) {
    findOneAndUpdate({'companySeq.companies':{$elemMatch:{'company':doc.company,'businessUnits.businessUnit':doc.businessUnit}}},{$inc: {'companySeq.companies.$[i].businessUnits.$[j].itemMasterSeqNumber': 1},$addToSet : {'companySeq.companies.$[i].businessUnits.$[j].itemMasters' :{itemMaster:doc._id} }},{returnNewDocument:true,arrayFilters:[{'i.company': doc.company },{'j.businessUnit': doc.businessUnit}]},function(error,counter){
        if (error) {
            done(error,null);
        }else if(counter){
            var companyIndex=getIndex(counter.companySeq.companies,'company',doc.company);
            var businessUnitIndex=getIndex(counter.companySeq.companies[companyIndex].businessUnits,'businessUnit',doc.businessUnit);
            done(null,businessUnitIndex >0?counter.companySeq.companies[companyIndex].businessUnits[businessUnitIndex].itemMasterSeqNumber:1);
        }else{
            done(new Error('No Item Master Sequence Number'),null);
        }
    });
};
exports.getNextBatchSequenceNumber=function (doc,done) {
    findOneAndUpdate({'companySeq.companies':{$elemMatch:{'company':doc.currentOwner.company,'businessUnits.businessUnit':doc.currentOwner.businessUnit,'businessUnits.inventories.inventory':doc.inventory}}},{$inc: {'companySeq.companies.$[i].businessUnits.$[j].inventories.$[k].batchSeqNumber': 1}},{
        returnNewDocument:true, arrayFilters:[{'i.company': doc.currentOwner.company },{'j.businessUnit': doc.currentOwner.businessUnit},{'k.inventory': doc.inventory}]},function (error,counter) {
        if (error) {
            done(error,null);
        }else if(counter){
            var companyIndex=getIndex(counter.companySeq.companies,'company',doc.currentOwner.company);
            var businessUnitIndex=getIndex(counter.companySeq.companies[companyIndex].businessUnits,'businessUnit',doc.currentOwner.businessUnit);
            var inventoryIndex= getIndex(counter.companySeq.companies[companyIndex].businessUnits[businessUnitIndex].inventories,'inventory',doc.inventory);
            done(null,counter.companySeq.companies[companyIndex].businessUnits[businessUnitIndex].inventories[inventoryIndex].batchSeqNumber);
            /*done(null, companyIndex>=0 && businessUnitIndex>=0&& inventoryIndex >=0 ? counter.companySeq.companies[companyIndex].businessUnits[businessUnitIndex].inventories[inventoryIndex].batchSeqNumber:1);*/
        }else{
            done(new Error('No Batch Sequence Number'),null);
        }

    });
};
exports.getNextInventorySequenceNumber=function (doc,done) {
    findOneAndUpdate({'companySeq.companies':{$elemMatch:{'company':doc.currentOwner.company,'businessUnits.businessUnit':doc.currentOwner.businessUnit}}},{$inc: {'companySeq.companies.$[i].businessUnits.$[j].userItemMasterSeqNumber': 1},$addToSet : {'companySeq.companies.$[i].businessUnits.$[j].inventories' :{inventory:doc._id} }},{
        returnNewDocument:true,arrayFilters:[{'i.company': doc.currentOwner.company },{'j.businessUnit': doc.currentOwner.businessUnit}]},function (error,counter) {
        if (error) {
            done(error,null);
        }else if(counter){
            var companyIndex=getIndex(counter.companySeq.companies,'company',doc.currentOwner.company);
            var businessUnitIndex=getIndex(counter.companySeq.companies[companyIndex].businessUnits,'businessUnit',doc.currentOwner.businessUnit);
            done(null, counter.companySeq.companies[companyIndex].businessUnits[businessUnitIndex].userItemMasterSeqNumber);
        }else{
            done(new Error('No Batch Sequence Number'),null);
        }
    });

};

exports.getNextOrderSequenceNumber=function (doc,done) {
    findOneAndUpdate({'companySeq.companies':{$elemMatch:{'company':doc.owner.company,'businessUnits.businessUnit':doc.owner.businessUnit}}},{$inc: {'companySeq.companies.$[i].businessUnits.$[j].orderSeqNumber': 1}},{
        returnNewDocument:true, arrayFilters:[{'i.company': doc.owner.company._id },{'j.businessUnit': doc.owner.businessUnit}]},function (error,counter) {
        if (error) {
            done(error,null);
        }else if(counter){
            var companyIndex=getIndex(counter.companySeq.companies,'company',doc.owner.company._id);
            var businessUnitIndex=getIndex(counter.companySeq.companies[companyIndex].businessUnits,'businessUnit',doc.owner.businessUnit);
            done(null, counter.companySeq.companies[companyIndex].businessUnits[businessUnitIndex].orderSeqNumber);
        }else{
            done(new Error('No Order Sequence Number'),null);
        }
    });

};
exports.getNextOfferSequenceNumber=function (doc,done) {
    findOneAndUpdate({'companySeq.companies':{$elemMatch:{'company':doc.owner.company,'businessUnits.businessUnit':doc.owner.businessUnit}}},{$inc: {'companySeq.companies.$[i].businessUnits.$[j].offerSeqNumber': 1}},{
        returnNewDocument:true,arrayFilters:[{'i.company': doc.owner.company },{'j.businessUnit': doc.owner.businessUnit}]},function (error,counter) {
        if (error) {
            done(error,null);
        }else if(counter){
            var companyIndex=getIndex(counter.companySeq.companies,'company',doc.owner.company);
            var businessUnitIndex=getIndex(counter.companySeq.companies[companyIndex].businessUnits,'businessUnit',doc.owner.businessUnit);
            done(null, counter.companySeq.companies[companyIndex].businessUnits[businessUnitIndex].offerSeqNumber);
        }else{
            done(new Error('No Offer Sequence Number'),null);
        }
    });
};
exports.getNextReportSequenceNumber=function (doc,done) {

    findOneAndUpdate({'companySeq.companies':{$elemMatch:{'company':doc.owner.company,'businessUnits.businessUnit':doc.owner.businessUnit}}},{$inc: {'companySeq.companies.$[i].businessUnits.$[j].reportSeqNumber': 1}},{
        returnNewDocument:true, arrayFilters:[{'i.company': doc.owner.company },{'j.businessUnit': doc.owner.businessUnit}]},function (error,counter) {
        if (error) {
            done(error,null);
        }else if(counter){
            var companyIndex=getIndex(counter.companySeq.companies,'company',doc.owner.company);
            var businessUnitIndex=getIndex(counter.companySeq.companies[companyIndex].businessUnits,'businessUnit',doc.owner.businessUnit);
            done(null, counter.companySeq.companies[companyIndex].businessUnits[businessUnitIndex].reportSeqNumber);
        }else{
            done(new Error('No Report Sequence Number'),null);
        }
    });
};
exports.getNextInvoiceSequenceNumber=function (doc,done) {
    findOneAndUpdate({'companySeq.companies':{$elemMatch:{'company':doc.owner.company,'businessUnits.businessUnit':doc.owner.businessUnit}}},{$inc: {'companySeq.companies.$[i].businessUnits.$[j].invoiceSeqNumber': 1}},{
        returnNewDocument:true, arrayFilters:[{'i.company': doc.owner.company },{'j.businessUnit': doc.owner.businessUnit}]},function (error,counter) {
        if (error) {
            done(error,null);
        }else if(counter){
            var companyIndex=getIndex(counter.companySeq.companies,'company',doc.owner.company);
            var businessUnitIndex=getIndex(counter.companySeq.companies[companyIndex].businessUnits,'businessUnit',doc.owner.businessUnit);
            done(null, counter.companySeq.companies[companyIndex].businessUnits[businessUnitIndex].invoiceSeqNumber);
        }else{
            done(new Error('No Report Sequence Number'),null);
        }
    });
};

exports.getBusinessUnitCodeById=function (businessUnitId,done) {
    mongoose.model('BusinessUnit').findOne({'_id':businessUnitId}).populate('company','name code').exec(function (bError,businessUnit) {
        if (bError) {
            done(bError,null);
        } else if(businessUnit) {
            done(null,businessUnit);
        }else{
            done(new Error('No Business Unit'),null);
        }
    });
};
exports.getCompanyCodeById=function (Id,done) {
    mongoose.model('Company').findOne({'_id':Id},{code:1}).exec(function (bError,company) {
        if (bError) {
            done(bError,null);
        } else if(company) {
            done(null,company);
        }else{
            done(new Error('No Company'),null);
        }
    });
};
