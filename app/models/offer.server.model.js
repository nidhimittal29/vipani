'use strict';

/**
 * Module dependencies.
 */
var mongoose = require('mongoose'),
    Counter = mongoose.model('Counter'),
    Schema = mongoose.Schema;

var validateLength128 = function(property) {
    if(property) {
        return (property.length <= 128);
    }
    return true;

};

/**
 * Offer Schema
 */
var OfferSchema = new Schema({
    offerNumber: {
        type: String,
        default: ''
    },
    code:{
        type: String,
        default: '',
        trim: true
    },
    name: {
        type: String,
        default: '',
        required: 'Please fill Offer name',
        validate: [validateLength128, 'The offer name length should not exceed 128 characters.'],
        trim: true
    },
    validFrom: {
        type: Date,
        default: '',
        trim: true
    },
    validTill: {
        type: Date,
        default: '',
        trim: true
    },
    toContactsCriteria: {
        type: String,
        enum: ['All', 'Specific'],
        default: 'All'
    },
    toContacts:{
        contacts: [{contact: {
                type: Schema.ObjectId,
                ref: 'Contact'
            }}],
        groups:[{group: {
                type: Schema.ObjectId,
                ref: 'Group'
            }}],
        businessUnits:[{businessUnit:{
                type: Schema.ObjectId,
                ref: 'BusinessUnit'
            }}]
    },
    role: {
        type: String,
        enum: ['Seller', 'Buyer', 'Mediator'],
        default: 'Seller'
    },
    offerType: {
        type: String,
        enum: ['Sell', 'Buy'],
        default: 'Sell'
    },
    notificationsContacts:[{
        contact: {
            type: Schema.ObjectId,
            ref: 'Contact'
        },
        nVipaniUser: {
            type: Schema.ObjectId,
            ref: 'User'
        },
        nVipaniCompany:{
            type: Schema.ObjectId,
            ref: 'Company'
        },
        businessUnit:{
            type: Schema.ObjectId,
            ref: 'BusinessUnit'
        },
        viewed: {
            type: Boolean,
            default: false
        },
        quotationRequested: {
            type: Boolean,
            default: false
        }
    }],
    inventoryItemsClassification: {
        classificationType: {
            type: String,
            enum: ['Custom', 'Criteria'],
            default: 'Custom'
        },
        classificationCriteria: {
            criteriaType: {
                type: String,
                enum: ['All', 'Custom'],
                default: 'All'
            },

            criteria: [{
                category: {
                    type: Schema.ObjectId,
                    ref: 'Category'
                },
                productBrand: {
                    type: Schema.ObjectId,
                    ref: 'ProductBrand'
                },
                gradeDefinition: [{
                    attributeKey: {
                        type: String,
                        default: '',
                        trim: true
                    },
                    attributeValue: {
                        type: String,
                        default: '',
                        trim: true
                    }
                }],
                qualityDefinition: [{
                    attributeKey: {
                        type: String,
                        default: '',
                        trim: true
                    },
                    attributeValue: {
                        type: String,
                        default: '',
                        trim: true
                    }
                }],
                unitOfMeasure: {
                    type: Schema.ObjectId,
                    ref: 'UnitOfMeasure'
                }
            }]
        }
    },
    offerStatus: {
        type: String,
        enum: ['Drafted','Opened', 'Cancelled', 'Expired', 'Completed', 'Pending', 'Open', 'Closed'],
        default: 'Drafted',
        trim: true
    },
    statusComments: {
        type: String,
        default: ''
    },
    statusHistory: [{
        status: {
            type: String,
            enum: ['Drafted','Opened','Cancelled', 'Expired', 'Completed', 'Open', 'Closed'],
            default: 'Opened',
            trim: true
        },
        date: {
            type: Date,
            default: Date.now
        },
        user: {
            type: Schema.ObjectId,
            ref: 'User'
        },
        displayName: {
            type: String,
            default: ''
        },
        comments: {
            type: String,
            default: ''
        }
    }],
    definition: {
        locations: [{
            city: {
                type: String,
                default: '',
                trim: true
            },
            state: {
                type: String,
                default: '',
                trim: true
            },
            country: {
                type: String,
                default: '',
                trim: true
            },
            pinCode: {
                type: String,
                default: '',
                trim: true
            }
        }],
        discountType: {
            type: String,
            enum: ['Volume', 'Value'],
            default: 'Volume',
            trim: true
        },

        // TOQ - In Multiples or Above
        toq: {
            quantity: {
                type: Number,
                default: 0
            },
            unitOfMeasure: {
                type: Schema.ObjectId,
                ref: 'UnitOfMeasure'
            },
            inMultiples: {
                type: Boolean,
                default: false
            },
            above: {
                type: Boolean,
                default: false
            }
        },
        // TOV - In Multiples or Above
        tov: {
            value: {
                type: Number,
                default: 0
            },
            inMultiples: {
                type: Boolean,
                default: false
            },
            above: {
                type: Boolean,
                default: false
            }
        }
    },
    products :[{
        inventory: {
            type: Schema.ObjectId,
            ref: 'Inventory'
        },
        category: {
            type: Schema.ObjectId,
            ref: 'Category'
        },
        subCategory1: {
            type: Schema.ObjectId,
            ref: 'Category'
        },
        subCategory2: {
            type: Schema.ObjectId,
            ref: 'Category'
        },
        subCategory3: {
            type: Schema.ObjectId,
            ref: 'Category'
        },
        unitIndicativePrice: {
            type: Number,
            default: 0,
            trim: true
        },
        numberOfUnitsAvailable:{
            type: Number,
            default: 0,
            trim: true
        },
        MOQ:{
            type: Number,
            default: 0,
            trim: true
        },
        margin:{
            type: Number,
            default: 0,
            trim: true
        },
        moqAndSalePrice: [{
            MOQ: {
                type: Number,
                default: 0
            },
            price: {
                type: Number,
                default: 0
            },
            margin:{
                type: Number,
                default: 0
            }
        }],
        moqAndBuyPrice: [{
            MOQ: {
                type: Number,
                default: 0
            },
            price: {
                type: Number,
                default: 0
            },
            margin:{
                type: Number,
                default: 0
            }
        }],
        availableDate: {
            type: Date,
            default: '',
            trim: true
        },
        sampleAvailable: {
            type:Boolean,
            default:false
        },
        numberOfOfferUnitsHistory: [{
            order: {
                type: Schema.ObjectId,
                ref: 'Order'
            },
            changeType: {
                type: String,
                enum: ['OrderConfirmation', 'OrderReturn', 'OrderRejection', 'OrderCancellation'],
                trim: true
            },
            changeUnits: {
                type: Number,
                default: 0,
                trim: true
            }
        }]
    }],
    sampleRequests: [{
        product: {
            type: Schema.ObjectId,
            ref: 'Product'
        },
        sampleRequested: {
            type: Boolean,
            default: false
        },
        nVipaniUser: {
            type: Schema.ObjectId,
            ref: 'User'
        },
        sampleSent: {
            type: Boolean,
            default: false
        }
    }],
    applicablePaymentTerms: [{
        selected: {
            type: Boolean,
            default: true
        },
        paymentTerm:{
            type: Schema.ObjectId,
            ref: 'PaymentTerm'
        }}
    ],
    applicablePaymentModes: [{
        selected: {
            type: Boolean,
            default: true
        },
        paymentMode:{
            type: Schema.ObjectId,
            ref: 'PaymentMode'
        }}
    ],
    businessUnit: {
        type: Schema.ObjectId,
        ref: 'BusinessUnit'
    },
    company: {
        type: Schema.ObjectId,
        ref: 'Company'
    },
    owner: {
        // We need to use this fields for each offer owner details
        user: {
            type: Schema.ObjectId,
            ref: 'User'
        },
        businessUnit: {
            type: Schema.ObjectId,
            ref: 'BusinessUnit'
        },
        businessUnitCode:{
            type: String,
            default: '',
            trim: true
        },
        company: {
            type: Schema.ObjectId,
            ref: 'Company'
        },
        companyCode:{
            type: String,
            default: '',
            trim: true
        }
    },
    bankAccountDetails: {
        bankAccountNumber: {
            type: String,
            default: '',
            trim: true
        },
        accountType: {
            type: String,
            enum: ['Saving', 'Current'],
            default: 'Current'
        },
        bankName: {
            type: String,
            default: '',
            trim: true
        },
        bankBranchName: {
            type: String,
            default: '',
            trim: true
        },
        ifscCode: {
            type: String,
            default: '',
            trim: true
        }
    },
    viewed: [{
        viewedOn: {
            type: Date,
            default: Date.now
        },
        // This is for public offer, or if any non user views the offer.
        viewedByName: {
            type: String,
            default: '',
            trim: true
        },
        viewedBy: {
            type: Schema.ObjectId,
            ref: 'User'
        }
    }],
    isPublic: {
        type: Boolean,
        default: false
    },
    updateHistory: [{
        modifiedOn: {
            type: Date,
            default: Date.now
        },
        modifiedBy: {
            type: Schema.ObjectId,
            ref: 'User'
        }
    }],
    created: {
        type: Date,
        default: Date.now
    },
    user: {
        type: Schema.ObjectId,
        ref: 'User'
    },
    lastUpdated: {
        type: Date,
        default: Date.now
    },
    lastUpdatedUser: {
        type: Schema.ObjectId,
        ref: 'User'
    },
    disabled: {
        type: Boolean,
        required: 'Please set disabled flag',
        default: false
    },
    isIntraCompany:{
        type:Boolean,
        default:false
    },
    deleted: {
        type: Boolean,
        required: 'Please set deleted flag',
        default: false
    }
});
OfferSchema.set('versionKey', 'offersVersionKey');
mongoose.model('Offer', OfferSchema);
OfferSchema.pre('save', function (next) {
    var doc = this;
    doc.updateHistory.push({modifiedOn: Date.now(), modifiedBy: doc.user});
    if (this.isNew) {
        var counterUtil = require('../controllers/utils/common.counter.util');
        counterUtil.getBusinessUnitCodeById(doc.owner.businessUnit,function (bError,businessUnit) {
            if(bError){
                return next(bError);
            }else {
                counterUtil.getNextOfferSequenceNumber(doc, function (error, offerSeqNumber) {
                    if (error)
                        return next(error);
                    var date = new Date();
                    doc.created = date;
                    var dd = '' + date.getDate();
                    var mm = '' + (date.getMonth() + 1);
                    var yyyy = '' + date.getFullYear();

                    if (mm.length < 2) {
                        mm = '0' + mm;
                    }
                    if (dd.length < 2) {
                        dd = '0' + dd;
                    }
                    doc.owner.businessUnitCode=businessUnit.code;
                    doc.owner.companyCode=businessUnit.company.code;
                    /*doc.code = doc.owner.companyCode  +'-'+doc.owner.businessUnitCode+'-'+counterUtil.leftPadWithZeros(offerSeqNumber, 5);*/
                    doc.code = doc.owner.companyCode  +'-'+doc.owner.businessUnitCode+'-'+ dd + mm + yyyy + '-' + counterUtil.leftPadWithZeros(offerSeqNumber, 5);
                    doc.offerNumber =  doc.code;

                    next();
                    /* }*/


                });
            }
        });
    }else{
        next();
    }
});
OfferSchema.pre('remove', function(next){
    var pname=this.name;
    var id=this._id;
    var notification=this.model('Notification');
    this.model('Order').find(
        {'offer': id,'currentStatus':{$ne:'Drafted'}},
        function(error,orders){
            if(orders.length===0){
                notification.remove({'source.offer': id},
                    function (notificationError, notification) {
                        if(notificationError) { var title='Offer id '+id+' is used in notification';
                            var notificationErr = new Error(title);next(notificationErr);}
                        else next();
                    }
                );
            }else{
                var title='Offer '+pname+' is used in Orders';
                var err = new Error(title);
                next(err);
            }
        }
    );
});
