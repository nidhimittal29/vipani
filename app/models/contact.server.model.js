'use strict';

/**
 * Module dependencies.
 */
var mongoose = require('mongoose'),
    globalUtil = require('../controllers/utils/common.global.util'),
    async = require('async'),
    Schema = mongoose.Schema;


/**
 * A Validation function for checking the length
 */
var validateLength128 = function(property) {
    if(property) {
        return (property.length <= 128);
    }
    return true;

};

/**
 * A Validation function for checking the length
 */
var validateLength256 = function(property) {
    if(property) {
        return (property.length <= 256);
    }
    return true;
};

/**
 * A Validation function for checking the length
 */
var validatePhoneNumberLength = function(property) {
    if(property) {
        return (property.length <= 11 && property.length >=10);
    }
    return true;
};

/**
 * A Validation function for checking the length
 */
var validatePincodeLength = function(property) {
    if(property) {
        return (property.length === 6);
    }
    return true;
};

/**
 * A Validation function for checking the length
 */
var validateLength1024 = function(property) {
    if(property) {
        return (property.length <= 1024);
    }
    return true;
};


/**
 * Contact Schema
 */
var ContactSchema = new Schema({
    firstName: {
        type: String,
        default: '',
        /*required: 'Please fill Contact first name',*/
        validate: [validateLength128, 'The first name length should not exceed 128 characters.'],
        trim: true
    },
    code:{
        type: String,
        default: '',
        trim: true
    },
    middleName: {
        type: String,
        default: '',
        validate: [validateLength128, 'The last name length should not exceed 128 characters.'],
        trim: true
    },
    lastName: {
        type: String,
        default: '',
        validate: [validateLength128, 'The last name length should not exceed 128 characters.'],
        trim: true
    },
    displayName: {
        type: String,
        default: '',
        /*required: 'Please fill Contact Display name',*/
        validate: [validateLength128, 'The last name length should not exceed 128 characters.'],
        trim: true
    },
    companyName: {
        type: String,
        default: '',
        validate: [validateLength256, 'The company name length should not exceed 256 characters.'],
        trim: true
    },
    company: {
        type: Schema.ObjectId,
        ref: 'Company'
    },
    companyCode:{
        type: String,
        default: '',
        trim: true
    },
    customerType: {
        type: String,
        enum: ['Customer', 'Supplier', 'Other'],
        default: 'Customer'
    },
    phones:[{
        phoneNumber: {
            type: String,
            default: '',
            validate: [validatePhoneNumberLength, 'Phone Number length should be 10 to 11 digits.'],
            trim: true
        },
        phoneType: {
            type:String,
            enum:['Mobile', 'Home', 'Work','Other'],
            default:'Mobile'
        },
        primary: {
            type:Boolean,
            default:false
        }
    }],

    emails: [{
        email: {
            type:String,
            default:'',
            match: [/.+\@.+\..+/, 'Please fill a valid email address'],
            validate: [validateLength256, 'The email length should not exceed 256 characters.'],
            trim:true
        },
        emailType: {
            type:String,
            enum:['Personal', 'Work','Other'],
            default:'Personal'
        },
        primary: {
            type:Boolean,
            default:false
        }
    }],
    addresses:[{
        addressLine: {
            type: String,
            default: '',
            validate: [validateLength1024, 'The address line should not exceed 1024 characters.'],
            trim: true
        },
        city: {
            type: String,
            default: '',
            validate: [validateLength128, 'The city length should not exceed 128 characters.'],
            trim: true
        },
        state: {
            type: String,
            default: '',
            validate: [validateLength128, 'The state length should not exceed 128 characters.'],
            trim: true
        },
        country: {
            type: String,
            default: '',
            validate: [validateLength128, 'The country length should not exceed 128 characters.'],
            trim: true
        },
        pinCode: {
            type: String,
            default: '',
            validate: [validatePincodeLength, 'Pincode should be 6 digits.'],
            trim: true
        },
        addressType: {
            type:String,
            enum:['Billing','Receiving','Shipping','Invoice'],
            default:'Billing'
        },
        primary: {
            type:Boolean, default:false
        }
    }],
    contactImageURL: {
        type: String,
        default: 'modules/contacts/img/default.png'
    },
    croppedContactImageURL: {
        type: String,
        default: 'modules/contacts/img/default-resize-240-240.png'
    },
    gstinNumber: {
        type: String,
        default: '',
        trim: true
    },
    gstinNumberProof: {
        type: String,
        default: '',
        trim: true
    },
    panNumber: {
        type: String,
        default: '',
        trim: true
    },
    panNumberProof: {
        type: String,
        default: '',
        trim: true
    },
    paymentTerm: {
        type: Schema.ObjectId,
        ref: 'PaymentTerm'
    },
    bankAccountDetails: {
        bankAccountNumber: {
            type: String,
            default: '',
            trim: true
        },
        accountType: {
            type: String,
            enum: ['Saving', 'Current', 'OverDraft'],
            default: 'Current'
        },
        bankName: {
            type: String,
            default: '',
            trim: true
        },
        bankBranchName: {
            type: String,
            default: '',
            trim: true
        },
        ifscCode: {
            type: String,
            default: '',
            trim: true
        }
    },
    bankAccountDetailsProof: {
        type: String,
        default: '',
        trim: true
    },
    nVipaniCompany: {
        type: Schema.ObjectId,
        ref: 'Company'
    },
    nVipaniUser: {
        type: Schema.ObjectId,
        ref: 'User'
    },
    nVipaniRegContact :{
        type: Boolean,
        default: false
    },
    nVipaniBusinessContact:{
        type: Boolean,
        default: false
    },
    groups:[  {
        type: Schema.ObjectId,
        ref: 'Group'
    }
    ],
    lastUpdated: {
        type: Date,
        default: Date.now
    },
    lastUpdatedUser: {
        type: Schema.ObjectId,
        ref: 'User'
    },
    updateHistory: [{
        modifiedOn: {
            type: Date,
            default: Date.now
        },
        modifiedBy: {
            type: Schema.ObjectId,
            ref: 'User'
        }
    }],
    bbUser:{
        type: Boolean,
        default: false
    },
    /*businessUnit:{
	    type:Schema.ObjectId,
        ref: 'BusinessUnit'
    },*/
    created: {
        type: Date,
        default: Date.now
    },
    user: {
        type: Schema.ObjectId,
        ref: 'User'
    },
    disabled: {
        type: Boolean,
        required: 'Please set disabled flag',
        default: false
    },
    deleted: {
        type: Boolean,
        required: 'Please set deleted flag',
        default: false
    }
});


function getPhoneQuery(phones,query,done) {
    async.forEach(phones,function (eachPhone) {
        query.push(eachPhone.phoneNumber);
    });
    done(query);
}
function getEmailQuery(emails,query,done) {
    async.forEach(emails,function (eachPhone) {
        query.push(eachPhone.email);
    });
    done(query);
}

function getContactPlayersQuery(contact,done) {
    var query=[];
    getPhoneQuery(contact.phones,query,function (contactQuery) {
        getEmailQuery(contact.emails,contactQuery,function (emailQuery) {
            done(emailQuery);
        });
    });
}
function counterObject(doc,done) {
    var seqFieldName = (doc.type === 'Customer') ? 'customerContactSeqNumber' : 'supplierContactSeqNumber';
    if (doc.customerType === 'Customer') {
        mongoose.model('Counter').findOneAndUpdate({'companySeq.companies': {$elemMatch: {'company': doc.company}}}, {$inc: {'companySeq.companies.$[i].customerContactSeqNumber': 1}}, {
            arrayFilters: [{'i.company': doc.company}]
        }, function (counterError, counter) {
            done(counterError, counter);
        });
    } else {
        mongoose.model('Counter').findOneAndUpdate({'companySeq.companies': {$elemMatch: {'company': doc.company}}}, {$inc: {'companySeq.companies.$[i].supplierContactSeqNumber': 1}}, {
            arrayFilters: [{'i.company': doc.company}]
        }, function (counterError, counter) {
            done(counterError, counter);
        });

    }
}
ContactSchema.pre('save', function (next) {
    var doc = this;
    globalUtil.populateDisplayName(doc,function (name) {
        doc.displayName=name;
        if (doc.contactImageURL) {
            var fileName = doc.contactImageURL.substring(0, doc.contactImageURL.lastIndexOf('.'));

            var croppedContactImageURL = fileName + '-resize-240-240.png';

            if (doc.croppedContactImageURL !== croppedContactImageURL) {
                doc.croppedContactImageURL = croppedContactImageURL;
            }
        }
        doc.updateHistory.push({modifiedOn: doc.lastUpdated, modifiedBy: doc.user});
        var counterUtil = require('../controllers/utils/common.counter.util');
        counterUtil.getCompanyCodeById(doc.company,function (errorCompany,company) {
            if (errorCompany) {
                return next(errorCompany);
            } else {
                counterUtil.getNextContactSeqenceNumber(doc, function (errorCounter, contactSeqNumber) {
                    if (errorCounter) {
                        return next(errorCounter);
                    } else {
                        getContactPlayersQuery(doc, function (nvipaniUserQuery) {
                            doc.model('User').findOne({
                                username: {$in: nvipaniUserQuery},
                                status: 'Registered'
                            }, '-salt -password', function (fetchUserError, fetchedUser) {
                                if (fetchUserError) {
                                    next(fetchUserError);
                                } else {
                                    if (fetchedUser) {
                                        if (!doc.nVipaniUser || (doc.nVipaniUser && !doc.nVipaniRegContact)) {
                                            doc.nVipaniUser = fetchedUser;
                                            doc.nVipaniCompany = fetchedUser.company;
                                        }
                                    } else {
                                        doc.nVipaniUser = null;
                                        doc.nVipaniCompany = null;

                                    }
                                    doc.companyCode=company.code;
                                    doc.code =company.code+'-'+ globalUtil.leftPadWithZeros(contactSeqNumber, 5);
                                    next();
                                }

                            });
                        });
                    }

                });
            }

        });
    });

});

ContactSchema.set('versionKey', 'contactsVersionKey');
mongoose.model('Contact', ContactSchema);
