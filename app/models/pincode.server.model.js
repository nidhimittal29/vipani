'use strict';

/**
 * Module dependencies.
 */
var mongoose = require('mongoose'),
    Schema = mongoose.Schema;

var PincodesSchema = new Schema({
    state: {
        type: String,
        default: '',
        required: 'Please fill State name',
        trim: true
    },
    area:{
        type: String,
        default: '',
        required: 'Please fill State name',
        trim: true
    },
    code: {
        type: String,
        default: '',
        required: 'Please fill Pincode',
        trim: true
    },
    disabled: {
        type: Boolean,
        required: 'Please set disabled flag',
        default: false
    },
    deleted: {
        type: Boolean,
        required: 'Please set deleted flag',
        default: false
    }
});

mongoose.model('Pincodes', PincodesSchema);
