'use strict';

/**
 * Module dependencies.
 */
var mongoose = require('mongoose'),
	Schema = mongoose.Schema;


/**
 * A Validation function for checking the length
 */
var validateLength128 = function(property) {
	if(property) {
		return (property.length <= 128);
	}
	return true;

};
/**
 * A Validation function for checking the length
 */
var validatePincodeLength = function (property) {
    if (property) {
        return (property.length === 6);
    }
    return true;
};
/**
 * Group Schema
 */
var GroupSchema = new Schema({
	name: {
		type: String,
		default: '',
		required: 'Please fill Group name',
		validate: [validateLength128, 'The last name length should not exceed 128 characters.'],
		trim: true
    },
    code:{
        type: String,
        default: '',
        trim: true
    },
    groupType: {
        type: String,
        enum: ['Customer', 'Supplier', 'Other'],
        default: 'Customer'
    },
    contacts:[{
        contact: {
            type: Schema.ObjectId,
            ref: 'Contact'
        }
    }],
    groupImageURL: {
        type: String,
        default: 'modules/groups/img/default.png'
    },
    croppedGroupImageURL: {
        type: String,
        default: 'modules/groups/img/default-resize-240-240.png'
    },
    groupClassification: {
        classificationType: {
            type: String,
            enum: ['Custom', 'Criteria'],
            default: 'Custom'
        },
        classificationCriteria: {
            criteriaType: {
                type: String,
                enum: ['All', 'Custom'],
                default: 'All'
            },
            location: [{
                city: {
                    type: String,
                    default: '',
                    validate: [validateLength128, 'The city length should not exceed 128 characters.'],
                    trim: true
                },
                state: {
                    type: String,
                    default: '',
                    validate: [validateLength128, 'The state length should not exceed 128 characters.'],
                    trim: true
                },
                country: {
                    type: String,
                    default: '',
                    validate: [validateLength128, 'The country length should not exceed 128 characters.'],
                    trim: true
                },
                pinCode: {
                    type: String,
                    default: '',
                    validate: [validatePincodeLength, 'Pincode should be 6 digits.'],
                    trim: true
                }
            }]
        }
    },
	description: {
		type: String,
		default: '',
		trim: true
	},
	lastUpdated: {
		type: Date,
		default: Date.now
	},
	updateHistory: [{
		modifiedOn: {
			type: Date,
			default: Date.now
		},
		modifiedBy: {
			type: Schema.ObjectId,
			ref: 'User'
		}
	}],
	created: {
		type: Date,
		default: Date.now
	},
	user: {
		type: Schema.ObjectId,
		ref: 'User'
	},
    company: {
        type: Schema.ObjectId,
        ref: 'Company'
    },
    disabled: {
        type: Boolean,
        required: 'Please set disabled flag',
        default: false
    },
    deleted: {
        type: Boolean,
        required: 'Please set deleted flag',
        default: false
    }
});
GroupSchema.pre('remove', function(next){
	this.model('Contact').update(
			{groups: this._id.toString()},
			{$pull: {groups: this._id.toString()}},
			{multi: true},
			next
	);
});
GroupSchema.set('versionKey', 'groupsVersionKey');
/*GroupSchema.index({user:1, name:1}, { unique: true });*/
mongoose.model('Group', GroupSchema);
