'use strict';

/**
 * Module dependencies.
 */
var mongoose = require('mongoose'),
    Counter = mongoose.model('Counter'),
    Schema = mongoose.Schema;

/**
 * ItemMaster Schema
 */
var ItemMasterSchema = new Schema({
    productBrand: {
        type: Schema.ObjectId,
        ref: 'ProductBrand'
    },
    description: {
        type: String,
        default: '',
        trim: true
    },
    code:{
        type: String,
        default: '',
        trim: true
    },
    unitOfMeasure: {
        type: Schema.ObjectId,
        ref: 'UnitOfMeasure'
    },

    leastSalableQuantity: {
        type: Number,
        default: 1
    },
    MRP: {
        type: Number,
        default: 0
    },
    dealerPrice: {
        type: Number,
        default: 0
    },
    isBillable: {
        type: Boolean,
        default: true
    },
    isInventoryItem: {
        type: Boolean,
        default: true
    },
    isRegularItem: {
        type: Boolean,
        default: true
    },
    isImagePresent: {
        type: Boolean,
        default: true
    },
    isServiceItem: {
        type: Boolean,
        default: true
    },
    isConsigmentItem: {
        type: Boolean,
        default: true
    },
    isPriceAlterable: {
        type: Boolean,
        default: true
    },
    isStockCheckRequired: {
        type: Boolean,
        default: true
    },
    reorderLevel: {
        type: Number,
        default: 0
    },
    economicalOrderQuantity: {
        type: Number,
        default: 0
    },
    lastPurchasePrice: {
        type: Number,
        default: 0
    },
    currentCost: {
        type: Number,
        default: 0
    },
    moqAndMargin: [{
        MOQ: {
            type: Number,
            default: 0
        },
        margin: {
            type: Number,
            default: 0
        }
    }],
    movAndMargin: [{
        MOV: {
            type: Number,
            default: 0
        },
        margin: {
            type: Number,
            default: 0
        }
    }],
    moqAndBuyMargin: [{
        MOQ: {
            type: Number,
            default: 0
        },
        margin: {
            type: Number,
            default: 0
        }
    }],
    movAndBuyMargin: [{
        MOV: {
            type: Number,
            default: 0
        },
        margin: {
            type: Number,
            default: 0
        }
    }],
    SKU: {
        type: String,
        default: '',
        trim: true
    },
    barcode: {
        type: String,
        default: '',
        trim: true
    },
    qrcode: {
        type: String,
        default: '',
        trim: true
    },
    // Scanned Image of the barcode
    qrImage: {
        type: String,
        default: ''
    },

    inventoryImageURL1: {
        type: String,
        default: 'modules/inventories/img/profile/default.png'
    },
    inventoryImageURL2: {
        type: String,
        default: 'modules/inventories/img/profile/default.png'
    },
    inventoryImageURL3: {
        type: String,
        default: 'modules/inventories/img/profile/default.png'
    },
    inventoryImageURL4: {
        type: String,
        default: 'modules/inventories/img/profile/default.png'
    },
    croppedInventoryImageURL1: {
        type: String,
        default: 'modules/inventories/img/profile/default-resize-240-240.png'
    },
    croppedInventoryImageURL2: {
        type: String,
        default: 'modules/inventories/img/profile/default-resize-240-240.png'
    },
    croppedInventoryImageURL3: {
        type: String,
        default: 'modules/inventories/img/profile/default-resize-240-240.png'
    },
    croppedInventoryImageURL4: {
        type: String,
        default: 'modules/inventories/img/profile/default-resize-240-240.png'
    },

    created: {
        type: Date,
        default: Date.now
    },
    businessUnit:{
        type: Schema.ObjectId,
        ref: 'BusinessUnit'
    },
    company:{
        type: Schema.ObjectId,
        ref: 'Company'
    },
    businessUnitCode:{
        type: String,
        default: '',
        trim: true
    },
    companyCode:{
        type: String,
        default: '',
        trim: true
    },
    user: {
        type: Schema.ObjectId,
        ref: 'User'
    },
    lastUpdated: {
        type: Date,
        default: Date.now
    },
    lastUpdatedUser: {
        type: Schema.ObjectId,
        ref: 'User'
    },
    reviewed: {
        type: Number,
        enum: [0, 1],
        default: 1,
        trim: true
    },
    disabled: {
        type: Boolean,
        required: 'Please set disabled flag',
        default: false
    },
    deleted: {
        type: Boolean,
        required: 'Please set deleted flag',
        default: false
    }
});

ItemMasterSchema.set('versionKey', 'itemMasterVersionKey');
mongoose.model('ItemMaster', ItemMasterSchema);
ItemMasterSchema.pre('save', function (next) {
    var doc = this;
    if (this.isNew) {
        var counterUtil = require('../controllers/utils/common.counter.util');
        var globalUtil = require('../controllers/utils/common.global.util');
        counterUtil.getBusinessUnitCodeById(doc.businessUnit,function (bError,businessUnit) {
            if (bError) {
                 next(bError);
            }else {
                counterUtil.getNextItemMasterSequenceNumber(doc, function (error, itemMasterSeqNumber) {
                    if (error) {
                        next(error);
                    } else {
                        doc.businessUnitCode=businessUnit.code;
                        doc.companyCode=businessUnit.company.code;
                        doc.code = doc.companyCode +'-'+doc.businessUnitCode+'-'+globalUtil.leftPadWithZeros(itemMasterSeqNumber, 5);
                        next();
                    }

                });
            }
        });
    } else {
        next();
    }
});

