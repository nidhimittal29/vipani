'use strict';

/**
 * Module dependencies.
 */
var mongoose = require('mongoose'),
    Schema = mongoose.Schema;

/**
 * Todos Schema
 */
var TodoSchema = new Schema({
    title: {
        type: String,
        default: '',
        required: 'Please fill Todo name',
        trim: true
    },
    target: {
        type: Schema.ObjectId,
        ref: 'User'
    },
    source: {
        order: {
            type: Schema.ObjectId,
            ref: 'Order'
        },
        offer: {
            type: Schema.ObjectId,
            ref: 'Offer'
        }
    },
    type: {
        type: String,
        enum: ['OrderConfirmation', 'PaymentPending', 'OrderShipment', 'OrderDeliveryConfirmation', 'OrderDisputed', 'OrderDisputeResolution'],
        default: 'OrderConfirmation'
    },
    completed: {
        type: Boolean,
        default: false
    },
    statusMessage: {
        type: String,
        default: ''
    },
    dueDate: {
        type: Date,
        default: ''
    },
    created: {
        type: Date,
        default: Date.now
    },
    user: {
        type: Schema.ObjectId,
        ref: 'User'
    },
    lastUpdated: {
        type: Date,
        default: Date.now
    },
    disabled:{
        type: Boolean,
        required: 'Please set disabled flag',
        default: false
    },
    deleted: {
        type: Boolean,
        required: 'Please set deleted flag',
        default: false
    }
});

mongoose.model('Todo', TodoSchema);
