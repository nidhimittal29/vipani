'use strict';

/**
 * Module dependencies.
 */
var mongoose = require('mongoose'),
    Schema = mongoose.Schema;

/**
 * Counter Schema
 *
 * db.counters.insert({'_id': 'countersId', 'orderSeqNumber': 1, 'offerSeqNumber': 1, 'invoiceSeqNumber':1});
 *
 */
var CounterSchema = new Schema({

    companySeq: {
        companies:[{
            company: {
                type: Schema.ObjectId,
                ref: 'Company'
            },
            businessUnits: [{
                businessUnit: {
                    type: Schema.ObjectId,
                    ref: 'BusinessUnit'
                },
                disabled: {
                    type: Boolean,
                    required: 'Please set disabled flag',
                    default: false
                },
                deleted: {
                    type: Boolean,
                    required: 'Please set deleted flag',
                    default: false
                },
                orderSeqNumber: {
                    type: Number,
                    default: 0
                },
                offerSeqNumber: {
                    type: Number,
                    default: 0
                },
                invoiceSeqNumber: {
                    type: Number,
                    default: 0
                },
                itemMasterSeqNumber: {
                    type: Number,
                    default: 0
                },
                userItemMasterSeqNumber: {
                    type: Number,
                    default: 0
                },
                inventories: [{
                    inventory: {
                        type: Schema.ObjectId,
                        ref: 'Inventory'
                    },
                    batchSeqNumber: {
                        type: Number,
                        default: 0
                    }

                }],
                itemMasters: [{
                    itemMaster: {
                        type: Schema.ObjectId,
                        ref: 'ItemMaster'
                    }
                }],

                reportSeqNumber: {
                    type: Number,
                    default: 0
                }
            }],
            businessUnitSeqNumber: {
                type: Number,
                default: 0
            },
            customerContactSeqNumber: {
                type: Number,
                default: 0
            },
            supplierContactSeqNumber: {
                type: Number,
                default: 0
            },
            disabled: {
                type: Boolean,
                required: 'Please set disabled flag',
                default: false
            },
            deleted: {
                type: Boolean,
                required: 'Please set deleted flag',
                default: false
            }
        }]},
    companySeqNumber: {
        type: Number,
        default: 0
    },

    disabled: {
        type: Boolean,
        required: 'Please set disabled flag',
        default: false
    },
    deleted: {
        type: Boolean,
        required: 'Please set deleted flag',
        default: false
    }

});
CounterSchema.set('versionKey', 'counterVersionKey');
mongoose.model('Counter', CounterSchema);
