'use strict';

/**
 * Module dependencies.
 */
var mongoose = require('mongoose'),
    Counter = mongoose.model('Counter'),
    Schema = mongoose.Schema;

/**
 * Report Schema
 */
var ReportSchema = new Schema({
    reportNumber: {
        type: String,
        default: ''
    },
    reportType: {
        type: String,
        enum: ['Stock', 'Sales', 'Purchases', 'SalesAndPurchase', 'FastAndSlowMoving', 'Profit', 'ItemWiseProfit', 'Expenses', 'PaymentPending', 'SalesTax'],
        default: 'Stock',
        trim: true
    },

    //PDF, users/data/reports/Stock.pdf,
    //XLS, users/data/reports/Stock.xls
    //HTML, users/data/reports/Stock.html
    //XLSX, users/data/reports/Stock.xlsx
    //CSV, users/data/reports/Stock.csv
    reports: [{
        name: {
            type: String,
            default: '',
            required: 'Please fill Report name',
            trim: true
        },
        format: {
            type: String,
            enum: ['XLS', 'PDF', 'HTML', 'XLSX', 'CSV'],
            default: 'PDF',
            trim: true
        },
        location: {
            type: String,
            default: '',
            trim: true
        }
    }],

    reportPeriod: {
        type: String,
        enum: ['Daily', 'Monthly', 'Quarterly', 'HalfYearly', 'Yearly', 'Duration'],
        default: 'Daily',
        trim: true
    },
    disabled: {
        type: Boolean,
        required: 'Please set disabled flag',
        default: false
    },
    deleted: {
        type: Boolean,
        required: 'Please set deleted flag',
        default: false
    },
    startDate: {
        type: Date,
        default: ''
    },
    endDate: {
        type: Date,
        default: ''
    },
    created: {
        type: Date,
        default: Date.now
    },
    company: {
        type: Schema.ObjectId,
        ref: 'Company'
    },
    user: {
        type: Schema.ObjectId,
        ref: 'User'
    },
    lastUpdated: {
        type: Date,
        default: Date.now
    }
});

ReportSchema.pre('save', function (next) {
    var doc = this;
    if (this.isNew) {
        Counter.findOneAndUpdate({}, {$inc: {reportSeqNumber: 1}}, function (error, counter) {
            if (error)
                return next(error);
            var date = new Date();
            doc.created = date;
            var dd = '' + date.getDate();
            var mm = '' + (date.getMonth() + 1);
            var yyyy = '' + date.getFullYear();

            if (mm.length < 2) {
                mm = '0' + mm;
            }
            if (dd.length < 2) {
                dd = '0' + dd;
            }

            if (counter === null) {
                var defaultCounter = new Counter();
                defaultCounter.orderSeqNumber = 1;
                defaultCounter.offerSeqNumber = 1;
                defaultCounter.invoiceSeqNumber = 1;
                defaultCounter.productSeqNumber = 1;
                defaultCounter.productSampleSeqNumber = 1;
                defaultCounter.inventorySeqNumber = 1;
                defaultCounter.reportSeqNumber = 1;
                defaultCounter.save(function (err) {
                    if (err) {
                        return next(err);
                    }
                    var reportNumber = dd + mm + yyyy + '-' + (defaultCounter.reportSeqNumber);
                    doc.reportNumber = reportNumber;
                    next();
                });
            } else {
                var reportNumber = dd + mm + yyyy + '-' + counter.reportSeqNumber;
                doc.reportNumber = reportNumber;
                next();
            }


        });
    } else {
        next();
    }
});


ReportSchema.set('versionKey', 'reportsVersionKey');

mongoose.model('Report', ReportSchema);
