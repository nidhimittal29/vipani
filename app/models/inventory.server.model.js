'use strict';

/**
 * Module dependencies.
 */
var mongoose = require('mongoose'),
    Counter = mongoose.model('Counter'),
    globalUtil = require('../controllers/utils/common.global.util'),
    Schema = mongoose.Schema;

/**
 * Inventory Schema
 */
var InventorySchema = new Schema({
    //Deprecated fields
    product: {
        type: Schema.ObjectId,
        ref: 'Product'
    },
    code:{
        type: String,
        default: '',
        trim: true
    },
    itemMaster: {
        type: Schema.ObjectId,
        ref: 'ItemMaster'
    },
    businessUnit:{
        type: Schema.ObjectId,
        ref: 'BusinessUnit'
    },
    owner:{
        businessUnit:{
            type: Schema.ObjectId,
            ref: 'BusinessUnit'
        },
        businessUnitName:{
            type: String,
            default: '',
            trim: true
        },
        businessUnitCode:{
            type: String,
            default: '',
            trim: true
        },
        company:{
            type: Schema.ObjectId,
            ref: 'Company'
        },
        companyName:{
            type: String,
            default: '',
            trim: true
        },
        companyCode:{
            type: String,
            default: '',
            trim: true
        }
    },
    currentOwner:{
        businessUnit:{
            type: Schema.ObjectId,
            ref: 'BusinessUnit'
        },
        businessUnitName:{
            type: String,
            default: '',
            trim: true
        },
        businessUnitCode:{
            type: String,
            default: '',
            trim: true
        },
        company:{
            type: Schema.ObjectId,
            ref: 'Company'
        },
        companyName:{
            type: String,
            default: '',
            trim: true
        },
        companyCode:{
            type: String,
            default: '',
            trim: true
        }
    },
    // Product Number is unique identifier of each inventory item
    productNum: {
        type: String,
        default: '',
        trim: true
    },
    // Unit Size is a product which indicates how many unit measures are present in one unit.
    // For ex: if the unitMeasure is Kg, we can have 2kg pouch for which unit size is 2
    // If it is litre, then there could be 0.5 litre unit for which the unit size is 0.5
    // Deprecated
    unitSize: {
        type: Number,
        default: 0
    },
    // Deprecated
    unitMeasure: {
        type: String,
        enum: ['Kg', 'gram', 'litre', 'ml', 'count', 'tonne', 'Bag', 'Box', 'Carton', 'Bale', 'Candy', 'Bundle', 'Case', 'metre', 'dozen', 'inch', 'foot', 'Pack', 'Tray', 'Other'],
        default: 'Kg',
        trim: true
    },
    productCategory:{
        type: Schema.ObjectId,
        ref: 'Category'
    },
    productSubCategory:{
        type: Schema.ObjectId,
        ref: 'Category'
    },
    productMainCategory:{
        type: Schema.ObjectId,
        ref: 'Category'
    },
    productSubCategoryName:{
        type: String,
        default: '',
        trim: true
    },
    productMainCategoryName:{
        type: String,
        default: '',
        trim: true
    },
    productName: {
        type: String,
        default: '',
        trim: true
    },
    productType: {
        type: String,
        default: '',
        trim: true
    },
    productCode: {
        type: String,
        default: '',
        trim: true
    },
    productDescription: {
        type: String,
        default: '',
        trim: true
    },
    productBrand:{
        type: Schema.ObjectId,
        ref: 'ProductBrand'
    },
    brandName: {
        type: String,
        default: '',
        trim: true
    },
    brandVariety: {
        type: String,
        default: '',
        trim: true
    },
    brandOwner: {
        type: Schema.ObjectId,
        ref: 'Contact'
    },
    manufacturers: [{
        type: Schema.ObjectId,
        ref: 'Contact'
    }],
    hsncode: {
        type: Schema.ObjectId,
        ref: 'Hsncodes'
    },
    hsncodeName: {
        type: String,
        default: '',
        trim: true
    },
    taxGroup: {
        type: Schema.ObjectId,
        ref: 'TaxGroup'
    },
    taxGroupName:{
        type: String,
        default: '',
        trim: true
    },
    unitOfMeasure: {
        type: Schema.ObjectId,
        ref: 'UnitOfMeasure'
    },
    unitOfMeasureName:{
        type: String,
        default: '',
        trim: true
    },
    stockMasters:[{
        type: Schema.ObjectId,
        ref: 'StockMaster'
    }],
    leastSalableQuantity: {
        type: Number,
        default: 1
    },
    MRP: {
        type: Number,
        default: 0
    },
    dealerPrice: {
        type: Number,
        default: 0
    },
    isBillable: {
        type: Boolean,
        default: true
    },
    isInventoryItem: {
        type: Boolean,
        default: true
    },
    isRegularItem: {
        type: Boolean,
        default: true
    },
    isImagePresent: {
        type: Boolean,
        default: true
    },
    isServiceItem: {
        type: Boolean,
        default: true
    },
    isConsigmentItem: {
        type: Boolean,
        default: true
    },
    isPriceAlterable: {
        type: Boolean,
        default: true
    },
    isStockCheckRequired: {
        type: Boolean,
        default: true
    },
    reorderLevel: {
        type: Number,
        default: 0
    },
    economicalOrderQuantity: {
        type: Number,
        default: 0
    },
    lastPurchasePrice: {
        type: Number,
        default: 0
    },
    currentCost: {
        type: Number,
        default: 0
    },
    moqAndMargin: [{
        MOQ: {
            type: Number,
            default: 0
        },
        margin: {
            type: Number,
            default: 0
        }
    }],
    movAndMargin: [{
        MOV: {
            type: Number,
            default: 0
        },
        margin: {
            type: Number,
            default: 0
        }
    }],
    moqAndBuyMargin: [{
        MOQ: {
            type: Number,
            default: 0
        },
        margin: {
            type: Number,
            default: 0
        }
    }],
    movAndBuyMargin: [{
        MOV: {
            type: Number,
            default: 0
        },
        margin: {
            type: Number,
            default: 0
        }
    }],

    qrcode: {
        type: String,
        default: '',
        trim: true
    },
    // Scanned Image of the barcode
    qrImage: {
        type: String,
        default: ''
    },
    numberOfUnits: {
        type: Number,
        default: 0
    },
    numberOfUnitsHistory: [{
        order: {
            type: Schema.ObjectId,
            ref: 'Order'
        },
        changeType: {
            type: String,
            enum: ['OrderConfirmation', 'OrderReturn', 'OrderRejection', 'OrderCancellation', 'InventoryUpdate', 'OrderDelivered'],
            trim: true
        },
        changeUnits: {
            type: Number,
            default: 0,
            trim: true
        }
    }],
    maxUnits:{
        type: Number,
        default: 0
    },
    buyOfferUnits: {
        type: Number,
        default: 0
    },
    moqAndSalePrice: [{
        MOQ: {
            type: Number,
            default: 0
        },
        price: {
            type: Number,
            default: 0
        },
        margin:{
            type: Number,
            default: 0
        },
        company: {
            type: Schema.ObjectId,
            ref: 'Company'
        }
    }],
    moqAndBuyPrice: [{
        MOQ: {
            type: Number,
            default: 0
        },
        price: {
            type: Number,
            default: 0
        },
        margin:{
            type: Number,
            default: 0
        },
        company: {
            type: Schema.ObjectId,
            ref: 'Company'
        }
    }],
    SKU: {
        type: String,
        default: '',
        trim: true
    },
    barcode: {
        type: String,
        default: '',
        trim: true
    },
    batchNumber: {
        type: String,
        default: '',
        trim: true
    },
    // Scanned Image of the barcode
    barcodeImage: {
        type: String,
        default: ''
    },
    manufacturedDate: {
        type: Date
    },
    packagingDate: {
        type: Date
    },
    expiryDate: {
        type: Date
    },
    // Best before is number of days.
    bestBefore: {
        type: Number,
        default: 0
    },
    inventoryTestCertificate: {
        type: String,
        default: '',
        trim: true
    },
    isPublic: {
        type: Boolean,
        default: false
    },
    inventoryImageURL1: {
        type: String,
        default: 'modules/inventories/img/profile/default.png'
    },
    inventoryImageURL2: {
        type: String,
        default: 'modules/inventories/img/profile/default.png'
    },
    inventoryImageURL3: {
        type: String,
        default: 'modules/inventories/img/profile/default.png'
    },
    inventoryImageURL4: {
        type: String,
        default: 'modules/inventories/img/profile/default.png'
    },
    croppedInventoryImageURL1: {
        type: String,
        default: 'modules/inventories/img/profile/default-resize-240-240.png'
    },
    croppedInventoryImageURL2: {
        type: String,
        default: 'modules/inventories/img/profile/default-resize-240-240.png'
    },
    croppedInventoryImageURL3: {
        type: String,
        default: 'modules/inventories/img/profile/default-resize-240-240.png'
    },
    croppedInventoryImageURL4: {
        type: String,
        default: 'modules/inventories/img/profile/default-resize-240-240.png'
    },
    tax: {
        CGST: {
            type: Number,
            default: 0
        },
        SGST: {
            type: Number,
            default: 0
        },
        IGST: {
            type: Number,
            default: 0
        },
        VAT: {
            type: Number,
            default: 0
        },
        CST: {
            type: Number,
            default: 0
        },
        serviceTax: {
            type: Number,
            default: 0
        }
    },
    updateHistory: [{
        modifiedOn: {
            type: Date,
            default: Date.now
        },
        modifiedBy: {
            type: Schema.ObjectId,
            ref: 'User'
        }
    }],
    settings: {
        // {Company Code } - {Business Unit Code} - {Date, MMDDYY} - {Sequence - Five Digit} - {Shift, A, B, C}
        batchSeqNumber: {
            type: Number,
            default: 0
        }
    },
    created: {
        type: Date,
        default: Date.now
    },
    user: {
        type: Schema.ObjectId,
        ref: 'User'
    },
    lastUpdated: {
        type: Date,
        default: Date.now
    },
    lastUpdatedUser: {
        type: Schema.ObjectId,
        ref: 'User'
    },
    disabled: {
        type: Boolean,
        required: 'Please set disabled flag',
        default: false
    },
    deleted: {
        type: Boolean,
        required: 'Please set deleted flag',
        default: false
    }
});

InventorySchema.pre('save', function (next) {
    var doc = this;

    if (this.isNew) {
        if (!doc.numberOfUnits) {
            doc.numberOfUnits = parseInt(doc.numberOfUnits);

        } else{
            doc.numberOfUnits=0;
        }
        /*   Counter.findOneAndUpdate({}, {$inc: {inventorySeqNumber: 1}}, function (error, counter) {*/
        var counterUtil = require('../controllers/utils/common.counter.util');
        counterUtil.getBusinessUnitCodeById(doc.currentOwner.businessUnit,function (bError,businessUnit) {
            if (bError) {
                return next(bError);
            } else {
                counterUtil.getNextInventorySequenceNumber(doc, function (error, inventorySequenceNumber) {

                    if (error)
                        return next(error);
                    var date = new Date();
                    doc.created = date;
                    var dd = '' + date.getDate();
                    var mm = '' + (date.getMonth() + 1);
                    var yyyy = '' + date.getFullYear();

                    if (mm.length < 2) {
                        mm = '0' + mm;
                    }
                    if (dd.length < 2) {
                        dd = '0' + dd;
                    }

                    if (doc.inventoryImageURL1) {
                        var fileName1 = doc.inventoryImageURL1.substring(0, doc.inventoryImageURL1.lastIndexOf('.'));

                        doc.croppedInventoryImageURL1 = fileName1 + '-resize-240-240.png';
                    }

                    if (doc.inventoryImageURL2) {
                        var fileName2 = doc.inventoryImageURL2.substring(0, doc.inventoryImageURL2.lastIndexOf('.'));

                        doc.croppedInventoryImageURL2 = fileName2 + '-resize-240-240.png';
                    }

                    if (doc.inventoryImageURL3) {
                        var fileName3 = doc.inventoryImageURL3.substring(0, doc.inventoryImageURL3.lastIndexOf('.'));

                        doc.croppedInventoryImageURL3 = fileName3 + '-resize-240-240.png';
                    }

                    if (doc.inventoryImageURL4) {
                        var fileName4 = doc.inventoryImageURL4.substring(0, doc.inventoryImageURL4.lastIndexOf('.'));

                        doc.croppedInventoryImageURL4 = fileName4 + '-resize-240-240.png';
                    }

                    doc.model('UnitOfMeasure').findOne({
                        _id: doc.unitOfMeasure,
                        type: 'Compound'
                    }, 'name symbol conversion firstUnitOfMeasure secondUnitOfMeasure', function (errorFindUOM, unitOfMeasure) {

                        if (errorFindUOM) {
                            return next(errorFindUOM);
                        } else if (!(unitOfMeasure instanceof doc.model('UnitOfMeasure'))) return next(new Error('No UOM'));
                        else {
                            if (doc.businessUnit) {
                                doc.model('User').findOne({'_id': (doc.user && doc.user._id ? doc.user._id.toString() : doc.user)}, 'company', function (userErr, userCompany) {
                                    if (userErr) {
                                        return next(userErr);
                                    } else if (!userCompany) {
                                        return next('No user company for the inventory');
                                    } else {
                                        doc.model('BusinessUnit').update({
                                            _id: doc.businessUnit,
                                            company: userCompany.company.toString()
                                        }, {$addToSet: {inventories: {inventory: doc._id}}}).exec(function (businessUnitErr, businessUnitUpdate) {
                                            if (businessUnitErr) {
                                                return next(businessUnitErr);
                                            } else if (!businessUnitUpdate) {
                                                return next(Error('No Matched Business Unit in create inventory'));
                                            } else {
                                                doc.unitOfMeasureName = '' + unitOfMeasure.symbol;
                                                if (!doc.numberOfUnits) {
                                                    doc.numberOfUnits = 0;
                                                }
                                                doc.currentOwner.businessUnitCode=businessUnit.code;
                                                doc.currentOwner.companyCode=businessUnit.company.code;
                                                doc.code =doc.currentOwner.companyCode+'-'+doc.currentOwner.businessUnitCode+'-'+ globalUtil.leftPadWithZeros(inventorySequenceNumber, 5);
                                                next()
                                            }
                                        });
                                    }
                                });
                            } else {
                                return next(new Error('No Business unit information at create inventory '));
                            }
                        }

                    });
                });
            }
        });
    } else {
        if (doc.inventoryImageURL1) {
            var fileName1 = doc.inventoryImageURL1.substring(0, doc.inventoryImageURL1.lastIndexOf('.'));

            var croppedInventoryImageURL1 = fileName1 + '-resize-240-240.png';

            if (doc.croppedInventoryImageURL1 !== croppedInventoryImageURL1) {
                doc.croppedInventoryImageURL1 = croppedInventoryImageURL1;
            }
        }

        if (doc.inventoryImageURL2) {

            var fileName2 = doc.inventoryImageURL2.substring(0, doc.inventoryImageURL2.lastIndexOf('.'));

            var croppedInventoryImageURL2 = fileName2 + '-resize-240-240.png';

            if (doc.croppedInventoryImageURL2 !== croppedInventoryImageURL2) {
                doc.croppedInventoryImageURL2 = croppedInventoryImageURL2;
            }
        }

        if (doc.inventoryImageURL3) {
            var fileName3 = doc.inventoryImageURL3.substring(0, doc.inventoryImageURL3.lastIndexOf('.'));

            var croppedInventoryImageURL3 = fileName3 + '-resize-240-240.png';

            if (doc.croppedInventoryImageURL3 !== croppedInventoryImageURL3) {
                doc.croppedInventoryImageURL3 = croppedInventoryImageURL3;
            }
        }

        if (doc.inventoryImageURL4) {

            var fileName4 = doc.inventoryImageURL4.substring(0, doc.inventoryImageURL4.lastIndexOf('.'));

            var croppedInventoryImageURL4 = fileName4 + '-resize-240-240.png';

            if (doc.croppedInventoryImageURL4 !== croppedInventoryImageURL4) {
                doc.croppedInventoryImageURL4 = croppedInventoryImageURL4;
            }
        }
        next();
    }
});
InventorySchema.pre('remove', function(next){
    var pname=this.product && this.product.name?this.product.name:'';
    var id=this._id;
    var order= this.model('Order');
    var company= this.model('Company');
    this.model('Offer').find(
        {'products.inventory': id,'currentStatus':{$ne:'Drafted'}},
        function(error,offers){
            if(offers.length===0){

                order.find(
                    {'products.inventory': id},
                    function(error,orders){
                        if(orders.length===0){
                            company.update(
                                {},{$pull: { inventories: { inventory:id}}},function(cError,results){
                                    if (results === 1) {
                                        next();
                                    }else{
                                        var title = 'Product Name:"' + pname + '" is used in Company';
                                        var errC = new Error(title);
                                        next(errC);
                                    }
                                });

                        }else{
                            var title = 'Product Name:"' + pname + '" is used in Orders';
                            var err = new Error(title);
                            next(err);
                        }
                    }
                );
            }else{
                var title='Product Name:"'+pname+'" is used in Offers';
                var err = new Error(title);
                next(err);
            }
        }
    );

});
InventorySchema.set('versionKey', 'inventoriesVersionKey');
mongoose.model('Inventory', InventorySchema);
