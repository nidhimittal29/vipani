'use strict';

/**
 * Module dependencies.
 */
var mongoose = require('mongoose'),
	Counter = mongoose.model('Counter'),
	Category = mongoose.model('Category'),
	Schema = mongoose.Schema;

/**
 * Product Schema
 */
var ProductSchema = new Schema({
	name: {
		type: String,
		default: '',
		required: 'Please fill Product name',
		trim: true
	},
	// Product UID is uniquely generated ID. It is combination of category, subCategory1, subCategory2 codes and generated sequence number
	// This will be used to identify the product
	productUID: {
		type: String,
		default: '',
		trim: true
	},
	category: {
		type: Schema.ObjectId,
		ref: 'Category'
	},
	subCategory1: {
		type: Schema.ObjectId,
		ref: 'Category'
	},
	subCategory2: {
		type: Schema.ObjectId,
		ref: 'Category'
	},
	subCategory3: {
		type: Schema.ObjectId,
		ref: 'Category'
	},
	hsncode:{
        type: Schema.ObjectId,
        ref: 'Hsncodes'
	},
    hsncodes:{
        type: String,
        default: '',
        trim: true
    },
	// Keywords are array of attribute key value for the product.
	keywords: [{
		attributeKey: {
			type: String,
			default: '',
			trim: true
		},
		attributeValue: {
			type: String,
			default: '',
			trim: true
		}
	}],
	productImageURL1: {
		type: String,
		default: 'modules/products/img/profile/default.png'
	},
	productImageURL2: {
		type: String,
		default: 'modules/products/img/profile/default.png'
	},
	productImageURL3: {
		type: String,
		default: 'modules/products/img/profile/default.png'
	},
	productImageURL4: {
		type: String,
		default: 'modules/products/img/profile/default.png'
	},
    croppedProductImageURL1: {
        type: String,
		default: 'modules/products/img/profile/default-resize-240-240.png'
    },
    croppedProductImageURL2: {
        type: String,
		default: 'modules/products/img/profile/default-resize-240-240.png'
    },
    croppedProductImageURL3: {
        type: String,
		default: 'modules/products/img/profile/default-resize-240-240.png'
    },
    croppedProductImageURL4: {
        type: String,
		default: 'modules/products/img/profile/default-resize-240-240.png'
    },
	description: {
		type: String,
		default: '',
		trim: true
	},
	brandName: {
		type: String,
		default: '',
		trim: true
	},
	// Examples: Grade - A, Grade - B and Grade - C etc
	grade: {
		type: String,
		default: '',
		trim: true
	},
	// Examples "Superior", "Poor" and "Medium" etc
	quality: {
		type: String,
		default: '',
		trim: true
	},
    // Examples:
    // Size Vs 10m, 20m, 30m
    // Color Vs Blue, Black, White, Yellow
    // Fineness Vs Superior, Medium, Low
    gradeDefinition: [{
        attributeKey: {
            type: String,
            default: '',
            trim: true
        },
        attributeValue: {
            type: String,
            default: '',
            trim: true
        }
    }],
    // Examples
    // Quality - "Superior", "Poor" and "Medium" etc
    //
    qualityDefinition: [{
        attributeKey: {
            type: String,
            default: '',
            trim: true
        },
        attributeValue: {
            type: String,
            default: '',
            trim: true
        }
    }],
	sampleNumber: {
		type: String,
		default: '',
		trim: true
	},
	fssaiLicenseNumber: {
		type: String,
		default: '',
		trim: true
	},
	fssaiLicenseNumberProof: {
		type: String,
		default: '',
		trim: true
	},
	testCertificate: {
		type: String,
		default: '',
		trim: true
	},
	manufacturer: {
		type: Schema.ObjectId,
		ref: 'Contact'
	},
	updateHistory: [{
		modifiedOn: {
			type: Date,
			default: Date.now
		},
		modifiedBy: {
			type: Schema.ObjectId,
			ref: 'User'
		}
	}],
	created: {
		type: Date,
		default: Date.now
	},
	user: {
		type: Schema.ObjectId,
		ref: 'User'
	},
    lastUpdated: {
        type: Date,
        default: Date.now
    },
    disabled: {
        type: Boolean,
        required: 'Please set disabled flag',
        default: false
    },
    deleted: {
        type: Boolean,
        required: 'Please set deleted flag',
        default: false
    }
});

ProductSchema.pre('remove', function(next){
	var pname=this.name;
	var id=this._id;
	this.model('Offer').find(
		{'products.product': id},
		function(error,offers){
			if(offers.length===0){
				mongoose.model('Company').update(
					{'products.product': id},
					{ $pull: { 'products': {  product: id } } },
					{multi: true},function(companyErr, company) {
						if(companyErr){
							var title='Product '+pname+' is used in Company';
							var err = new Error(title);
							next(err);
						}else{
						next();
						}
					}
				);
			}else{
				var title='Product '+pname+' is used in Offers';
				var err = new Error(title);
				next(err);
			}
		}
	);
});

ProductSchema.pre('save', function (next) {
	var doc = this;
	if (this.isNew) {
        Counter.findOneAndUpdate({}, {
            $inc: {
                productSeqNumber: 1,
                productSampleSeqNumber: 1
            }
        }, function (error, counter) {
			if (error)
				return next(error);

			if (doc.productImageURL1) {
				var fileName1 = doc.productImageURL1.substring(0, doc.productImageURL1.lastIndexOf('.'));

				doc.croppedProductImageURL1 = fileName1 + '-resize-240-240.png';
			}

			if (doc.productImageURL2) {
				var fileName2 = doc.productImageURL2.substring(0, doc.productImageURL2.lastIndexOf('.'));

				doc.croppedProductImageURL2 = fileName2 + '-resize-240-240.png';
			}

			if (doc.productImageURL3) {
				var fileName3 = doc.productImageURL3.substring(0, doc.productImageURL3.lastIndexOf('.'));

				doc.croppedProductImageURL3 = fileName3 + '-resize-240-240.png';
			}

			if (doc.productImageURL4) {
				var fileName4 = doc.productImageURL4.substring(0, doc.productImageURL4.lastIndexOf('.'));

				doc.croppedProductImageURL4 = fileName4 + '-resize-240-240.png';
			}

			if (counter === null) {
				var defaultCounter = new Counter();
				defaultCounter.orderSeqNumber = 1;
				defaultCounter.offerSeqNumber = 1;
				defaultCounter.invoiceSeqNumber = 1;
				defaultCounter.productSeqNumber = 1;
				defaultCounter.productSampleSeqNumber = 1;
				defaultCounter.inventorySeqNumber = 1;
                defaultCounter.reportSeqNumber = 1;
				defaultCounter.save(function (err) {
					if (err) {
						return next(err);
					}

                    Category.findById(doc.subCategory2).exec(function (err, subCategory2) {
						if (err) return next(err);
                        if (!subCategory2) return next(new Error('Failed to load Category ' + doc.subCategory2));
                        var productUID = subCategory2.code + '-' + (defaultCounter.productSeqNumber);
                        var sampleNumber = 'SM-' + subCategory2.code + '-' + (defaultCounter.productSampleSeqNumber);
                        doc.productUID = productUID;
                        doc.sampleNumber = sampleNumber;
                        next();
					});

				});
			} else {
                Category.findById(doc.subCategory2).exec(function (err, subCategory2) {
					if (err) return next(err);
                    if (!subCategory2) return next(new Error('Failed to load Category ' + doc.subCategory2));
                    var productUID = subCategory2.code + '-' + (counter.productSeqNumber);
                    var sampleNumber = 'SM-' + subCategory2.code + '-' + (counter.productSampleSeqNumber);
                    doc.productUID = productUID;
                    doc.sampleNumber = sampleNumber;
                    next();
				});
			}
		});
	} else {
		if (doc.productImageURL1) {
			var fileName1 = doc.productImageURL1.substring(0, doc.productImageURL1.lastIndexOf('.'));

			var croppedProductImageURL1 = fileName1 + '-resize-240-240.png';

			if (doc.croppedProductImageURL1 !== croppedProductImageURL1) {
				doc.croppedProductImageURL1 = croppedProductImageURL1;
			}
		}

		if (doc.productImageURL2) {

			var fileName2 = doc.productImageURL2.substring(0, doc.productImageURL2.lastIndexOf('.'));

			var croppedProductImageURL2 = fileName2 + '-resize-240-240.png';

			if (doc.croppedProductImageURL2 !== croppedProductImageURL2) {
				doc.croppedProductImageURL2 = croppedProductImageURL2;
			}
		}

		if (doc.productImageURL3) {

			var fileName3 = doc.productImageURL3.substring(0, doc.productImageURL3.lastIndexOf('.'));

			var croppedProductImageURL3 = fileName3 + '-resize-240-240.png';

			if (doc.croppedProductImageURL3 !== croppedProductImageURL3) {
				doc.croppedProductImageURL3 = croppedProductImageURL3;
			}
		}

		if (doc.productImageURL4) {

			var fileName4 = doc.productImageURL4.substring(0, doc.productImageURL4.lastIndexOf('.'));

			var croppedProductImageURL4 = fileName4 + '-resize-240-240.png';

			if (doc.croppedProductImageURL4 !== croppedProductImageURL4) {
				doc.croppedProductImageURL4 = croppedProductImageURL4;
			}
		}
		next();
	}
});
ProductSchema.set('versionKey', 'productsVersionKey');
mongoose.model('Product', ProductSchema);
