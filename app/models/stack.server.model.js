'use strict';

/**
 * Module dependencies.
 */
var mongoose = require('mongoose'),
    Schema = mongoose.Schema;

var validateLength128 = function (property) {
    if (property) {
        return (property.length <= 128);
    }
    return true;

};


/**
 * A Validation function for checking the length
 */
var validatePhoneNumberLength = function (property) {
    if (property) {
        return (property.length <= 11 && property.length >= 10);
    }
    return true;
};

/**
 * A Validation function for checking the length
 */
var validatePincodeLength = function (property) {
    if (property) {
        return (property.length === 6);
    }
    return true;
};
/**
 * A Validation function for checking the length
 */
var validateLength1024 = function (property) {
    if (property) {
        return (property.length <= 1024);
    }
    return true;
};

/**
 * Stack Schema
 */
var StackSchema = new Schema({

    name: {
        type: String,
        default: '',
        required: 'Please fill Business Unit Name'
    },
    code: {
        type: String,
        default: '',
        trim: true
    },
    godown: {
        type: Schema.ObjectId,
        ref: 'Godown'
    },
    compartment: {
        type: Schema.ObjectId,
        ref: 'Compartment'
    },

    description: {
        type: String,
        default: '',
        trim: true
    },
    capacity: {
        numberOfUnits: {
            type: Number,
            default: 0,
            trim: true
        },
        unitOfMeasure: {
            type: Schema.ObjectId,
            ref: 'UnitOfMeasure'
        }
    },
    type: {
        type: String,
        enum: ['Cold', 'Wet', 'Dry', 'Other'],
        default: 'Owned'
    },

    // All measurements in feet
    measurements: {
        length: {
            numberOfUnits: {
                type: Number,
                default: 0,
                trim: true
            },
            unitOfMeasure: {
                type: Schema.ObjectId,
                ref: 'UnitOfMeasure'
            }
        },
        beadth: {
            numberOfUnits: {
                type: Number,
                default: 0,
                trim: true
            },
            unitOfMeasure: {
                type: Schema.ObjectId,
                ref: 'UnitOfMeasure'
            }
        },
        height: {
            numberOfUnits: {
                type: Number,
                default: 0,
                trim: true
            },
            unitOfMeasure: {
                type: Schema.ObjectId,
                ref: 'UnitOfMeasure'
            }
        }
    },

    disabled: {
        type: Boolean,
        required: 'Please set disabled flag',
        default: false
    },
    deleted: {
        type: Boolean,
        required: 'Please set deleted flag',
        default: false
    },
    updateHistory: [{
        modifiedOn: {
            type: Date,
            default: Date.now
        },
        modifiedBy: {
            type: Schema.ObjectId,
            ref: 'User'
        }
    }],
    created: {
        type: Date,
        default: Date.now
    },
    lastUpdated: {
        type: Date,
        default: Date.now
    },
    lastUpdatedUser: {
        type: Schema.ObjectId,
        ref: 'User'
    },
    user: {
        type: Schema.ObjectId,
        ref: 'User'
    }
});

StackSchema.set('versionKey', 'stackVersionKey');
mongoose.model('Stack', StackSchema);
