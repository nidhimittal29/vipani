'use strict';

/**
 * Module dependencies.
 */
var mongoose = require('mongoose'),
    Schema = mongoose.Schema;

var PaymentModeSchema = new Schema({
    name: {
        type: String,
        default: '',
        required: 'Please fill Payment Mode Name',
        trim: true
    },
    code:{
        type: String,
        default: '',
        trim: true
    },
    description:{
        type: String,
        trim:true,
        default:''
    },
    type: {
        type: String,
        enum: ['Cash', 'Cheque', 'DD', 'NEFT', 'RTGS', 'IMPS', 'UPI', 'Wallet', 'Online'],
        default: 'PayNow',
        trim: true
    },

    disabled: {
        type: Boolean,
        required: 'Please set disabled flag',
        default: false
    },
    deleted: {
        type: Boolean,
        required: 'Please set deleted flag',
        default: false
    },
    created: {
        type: Date,
        default: Date.now
    },
    user: {
        type: Schema.ObjectId,
        ref: 'User'
    },
    company: {
        type: Schema.ObjectId,
        ref: 'Company'
    },
    lastUpdated: {
        type: Date,
        default: Date.now
    },
    lastUpdatedUser: {
        type: Schema.ObjectId,
        ref: 'User'
    }
});
PaymentModeSchema.set('versionKey', 'paymentModeVersionKey');
mongoose.model('PaymentMode', PaymentModeSchema);
