'use strict';

/**
 * Module dependencies.
 */
var mongoose = require('mongoose'),
    globalUtil = require('../controllers/utils/common.global.util'),

    Schema = mongoose.Schema;

var validateLength128 = function (property) {
    if (property) {
        return (property.length <= 128);
    }
    return true;

};


/**
 * A Validation function for checking the length
 */
var validatePhoneNumberLength = function (property) {
    if (property) {
        return (property.length <= 11 && property.length >= 10);
    }
    return true;
};

/**
 * A Validation function for checking the length
 */
var validatePincodeLength = function (property) {
    if (property) {
        return (property.length === 6);
    }
    return true;
};
/**
 * A Validation function for checking the length
 */
var validateLength1024 = function (property) {
    if (property) {
        return (property.length <= 1024);
    }
    return true;
};

/**
 * BusinessUnit Schema
 */
var BusinessUnitSchema = new Schema({

    name: {
        type: String,
        default: '',
        required: 'Please fill Business Unit Name'
    },
    code: {
        type: String,
        default: '',
        trim: true
    },
    description: {
        type: String,
        default: '',
        trim: true
    },

    type: {
        type: String,
        enum: ['RegisteredOffice', 'Branch', 'Stockpoint', 'ManufacturingUnit', 'RetailOutlet', 'DistributionCentre', 'Warehouse', 'CorporateOffice'],
        default: 'RegisteredOffice'
    },
    emails: [{
        email: {
            type: String,
            default: '',
            match: [/.+\@.+\..+/, 'Please fill a valid email'],
            //validate: [validateLocalStrategyProperty, 'Please fill in your email'],
            trim: true
        },
        emailType: {
            type: String,
            enum: ['Personal', 'Work', 'Other'],
            default: 'Personal'
        },
        primary: {
            type: Boolean,
            default: false
        },
        isPublic: {
            type: Boolean,
            default: true
        }
    }],
    gpsLocation: {
        latitude: {
            type: Number,
            default: 0
        },
        longitude: {
            type: Number,
            default: false
        }
    },
    addresses: [{
        addressLine: {
            type: String,
            default: '',
            validate: [validateLength1024, 'The address line should not exceed 1024 characters.'],
            trim: true
        },
        city: {
            type: String,
            default: '',
            validate: [validateLength128, 'The city length should not exceed 128 characters.'],
            trim: true
        },
        state: {
            type: String,
            default: '',
            validate: [validateLength128, 'The state length should not exceed 128 characters.'],
            trim: true
        },
        country: {
            type: String,
            default: '',
            validate: [validateLength128, 'The country length should not exceed 128 characters.'],
            trim: true
        },
        pinCode: {
            type: String,
            default: '',
            validate: [validatePincodeLength, 'Pincode should be 6 digits.'],
            trim: true
        },
        addressType: {
            type: String,
            enum: ['Billing', 'Receiving', 'Shipping', 'Invoice'],
            default: 'Billing'
        },
        primary: {
            type: Boolean, default: false
        },
        isPublic: {
            type: Boolean,
            default: true
        }
    }],
    phones: [{
        phoneNumber: {
            type: String,
            default: '',
            validate: [validatePhoneNumberLength, 'Phone Number length should be 10 to 11 digits.'],
            trim: true
        },
        phoneType: {
            type: String,
            enum: ['Mobile', 'Office', 'Work', 'Other'],
            default: 'Mobile'
        },
        primary: {
            type: Boolean,
            default: false
        },
        isPublic: {
            type: Boolean,
            default: true
        }
    }],

    bankAccountDetails: {
        bankAccountNumber: {
            type: String,
            default: '',
            trim: true
        },
        accountType: {
            type: String,
            enum: ['Saving', 'Current'],
            default: 'Current'
        },
        bankName: {
            type: String,
            default: '',
            trim: true
        },
        bankBranchName: {
            type: String,
            default: '',
            trim: true
        },
        ifscCode: {
            type: String,
            default: '',
            trim: true
        }
    },
    bankAccountDetailsProof: {
        type: String,
        default: '',
        trim: true
    },
    tinNumber: {
        type: String,
        default: '',
        trim: true
    },
    tinNumberProof: {
        type: String,
        default: '',
        trim: true
    },
    panNumber: {
        type: String,
        default: '',
        trim: true
    },
    panNumberProof: {
        type: String,
        default: '',
        trim: true
    },
    gstinNumber: {
        type: String,
        default: '',
        trim: true
    },
    gstinNumberProof: {
        type: String,
        default: '',
        trim: true
    },
    warehouseDetails: {
        capacity: {
            numberOfUnits: {
                type: Number,
                default: 0,
                trim: true
            },
            unitOfMeasure: {
                type: Schema.ObjectId,
                ref: 'UnitOfMeasure'
            }
        },

        loadingCapacity: {
            numberOfUnits: {
                type: Number,
                default: 0,
                trim: true
            },
            unitOfMeasure: {
                type: Schema.ObjectId,
                ref: 'UnitOfMeasure'
            },
            durationInHours: {
                type: Number,
                default: 0,
                trim: true
            }
        },
        unLoadingCapacity: {
            numberOfUnits: {
                type: Number,
                default: 0,
                trim: true
            },
            unitOfMeasure: {
                type: Schema.ObjectId,
                ref: 'UnitOfMeasure'
            },
            durationInHours: {
                type: Number,
                default: 0,
                trim: true
            }
        },
        godowns: [{
            godown: {
                type: Schema.ObjectId,
                ref: 'Godown'
            }
        }]
    },


    inventories: [{
        godown: {
            type: Schema.ObjectId,
            ref: 'Godown'
        },
        compartment: {
            type: Schema.ObjectId,
            ref: 'Compartment'
        },
        stack: {
            type: Schema.ObjectId,
            ref: 'Stack'
        },
        inventory: {
            type: Schema.ObjectId,
            ref: 'Inventory'
        }
    }],
    company: {
        type: Schema.ObjectId,
        ref: 'Company'
    },
    employees: [{
        user: {
            type: Schema.ObjectId,
            ref: 'User'
        },
        incharge: {
            type: Boolean,
            default: false
        },
        userGroup: {
            type: Schema.ObjectId,
            ref: 'UserGroup'
        },
        fromDate:{
            type: Date,
            default: Date.now
        },
        tillNow:{
            type: Date,
            default: Date.now
        }
    }
    ],

    settings: {
        // {Company Code } - {Business Unit Code} - {Date, MMDDYY} - {Sequence - Five Digit)
        orderSeqNumber: {
            type: Number,
            default: 0
        },
        //  {Company Code } - {Business Unit Code} - {Date, MMDDYY} - {Sequence - Five Digit)
        offerSeqNumber: {
            type: Number,
            default: 0
        },
        //  {Company Code } - {Business Unit Code} - {Date, MMDDYY} - {Sequence - Five Digit)
        invoiceSeqNumber: {
            type: Number,
            default: 0
        },
        // {Company Code } - {Business Unit Code} - {Sequence - Five Digit}
        inventorySeqNumber: {
            type: Number,
            default: 0
        }
    },


    disabled: {
        type: Boolean,
        required: 'Please set disabled flag',
        default: false
    },
    deleted: {
        type: Boolean,
        required: 'Please set deleted flag',
        default: false
    },
    updateHistory: [{
        modifiedOn: {
            type: Date,
            default: Date.now
        },
        modifiedBy: {
            type: Schema.ObjectId,
            ref: 'User'
        }
    }],
    created: {
        type: Date,
        default: Date.now
    },
    lastUpdated: {
        type: Date,
        default: Date.now
    },
    lastUpdatedUser: {
        type: Schema.ObjectId,
        ref: 'User'
    },
    defaultBusinessUnit: {
        type: Boolean,
        default: false
    },
    user: {
        type: Schema.ObjectId,
        ref: 'User'
    }
});
function findDefaultInChangeEmployeeUser(businessUnits,done){
    return businessUnits.employees.filter(function(eachBusinessEmployee){
        return eachBusinessEmployee.incharge;
    });
}


BusinessUnitSchema.set('versionKey', 'businessUnitVersionKey');
mongoose.model('BusinessUnit', BusinessUnitSchema);
BusinessUnitSchema.pre('save', function (next) {
    var counterUtil = require('../controllers/utils/common.counter.util');
    var doc = this;
    var Company=this.model('Company');
    if (this.isNew && !this.defaultBusinessUnit) {
        var employee=findDefaultInChangeEmployeeUser(doc);

        counterUtil.getNextBusinessUnitSequenceNumber(doc, function(error,businessUnitSeqNumber) {

            if(error){
                return next(error);
            }else{
                if (employee.length > 0) {
                    Company.findOneAndUpdate({
                        _id: doc.company,
                        employees: {$elemMatch: {user: employee[0].user.toString()}}
                    }, {
                        $addToSet: {
                            'businessUnits': {
                                businessUnit: doc._id,
                                defaultBusinessUnit: false,
                                status: 'Active'
                            },
                            'employees.$.businessUnits': {
                                businessUnit: doc._id,
                                status: 'Active'
                            }

                        },
                        $inc: {'businessUnitSeqNumber': 1}
                    },{returnOriginal:false}, function (error, company) {
                        if (error) {
                            next(error);
                        } else {

                            var businessUnitCode =globalUtil.leftPadWithZeros(businessUnitSeqNumber, 5);
                            doc.code = businessUnitCode;
                            next();


                        }
                    });
                } else {

                    next(new Error('No Incharge for the Business Unit'));
                }
            }
        });
    }else{
        next();
    }

});
