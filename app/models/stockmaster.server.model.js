'use strict';

/**
 * Module dependencies.
 */
var mongoose = require('mongoose'),
    Counter = mongoose.model('Counter'),
    Schema = mongoose.Schema;

/**
 * Stockmaster Schema
 */
var StockMasterSchema = new Schema({
    inventory: {
        type: Schema.ObjectId,
        ref: 'Inventory'
    },
    code:{
        type: String,
        default: '',
        trim: true
    },
    batchNumber: {
        type: String,
        default: '',
        trim: true
    },
    serialNumber: {
        type: String,
        default: '',
        trim: true
    },
    location: {
        type: String,
        default: '',
        trim: true
    },
    currentOwner: {
        businessUnit:{
            type: Schema.ObjectId,
            ref: 'BusinessUnit'
        },
        businessUnitNumber: {
            type: String,
            default: '',
            trim: true
        },
        businessUnitName: {
            type: String,
            default: '',
            trim: true
        },
        company:{
            type: Schema.ObjectId,
            ref: 'Company'
        },
        companyName:{
            type: String,
            default: '',
            trim: true
        }
    },
    openingBalance: {
        type: Number,
        default: 0
    },
    openingBalanceValue: {
        type: Number,
        default: 0
    },
    yearOpeningBalance: {
        type: Number,
        default: 0
    },
    yearOpeningBalanceValue: {
        type: Number,
        default: 0
    },
    currentBalance: {
        type: Number,
        default: 0
    },
    lastPurchasePrice: {
        type: Number,
        default: 0
    },
    currentCost: {
        type: Number,
        default: 0
    },
    manufacturingDate: {
        type: Date,
        default: Date.now
    },
    packagingDate: {
        type: Date
    },
    expiryDate: {
        type: Date,
        default: Date.now
    },
    shelfLife: {
        type: Number,
        default: 0
    },
    bestBefore: {
        type: Number,
        default: 0
    },
    MRP: {
        type: Number,
        default: 0
    },

    stockOut: {
        other: [{
            comment: {
                type: String,
                default: ''
            },
            count: {
                type: Number,
                default: 0,
                trim: true
            },
            updatedDate: {
                type: Date,
                default: Date.now
            },
            updatedUser: {
                type: Schema.ObjectId,
                ref: 'User'
            }
        }],
        // If this batch, lot or stock item is converted from some other inventory batches
        inventories: [{
            inventory: {
                type: Schema.ObjectId,
                ref: 'Inventory'
            },
            stockMasters: [{
                stockMaster: {
                    type: Schema.ObjectId,
                    ref: 'StockMaster'
                },
                convertType:{
                    type: String,
                    enum: ['Manufactured', 'Repacked', 'Graded'],
                    default: 'Manufactured'
                },
                count: {
                    type: Number,
                    default: 0,
                    trim: true
                },
                updatedDate: {
                    type: Date,
                    default: Date.now
                },
                updatedUser: {
                    type: Schema.ObjectId,
                    ref: 'User'
                }
            }],
            updatedDate: {
                type: Date,
                default: Date.now
            },
            updatedUser: {
                type: Schema.ObjectId,
                ref: 'User'
            }

        }],
        // If this batch, lot or stock item is created when particular order is delivered.
        orders: [{
            order: {
                type: Schema.ObjectId,
                ref: 'Order'
            },
            count: {
                type: Number,
                default: 0,
                trim: true
            },
            updatedDate: {
                type: Date,
                default: Date.now
            },
            updatedUser: {
                type: Schema.ObjectId,
                ref: 'User'
            }
        }]
    },
    stockIn: {
        other: [{
            comment: {
                type: String,
                default: ''
            },
            count: {
                type: Number,
                default: 0,
                trim: true
            },
            updatedDate: {
                type: Date,
                default: Date.now
            },
            updatedUser: {
                type: Schema.ObjectId,
                ref: 'User'
            }
        }],
        // If this batch, lot or stock item is converted to some other inventory batches
        inventories: [{
            inventory: {
                type: Schema.ObjectId,
                ref: 'Inventory'
            },
            stockMasters: [{
                stockMaster: {
                    type: Schema.ObjectId,
                    ref: 'StockMaster'
                },
                convertType:{
                    type: String,
                    enum: ['Manufactured', 'Repacked', 'Graded'],
                    default: 'Manufactured'
                },
                count: {
                    type: Number,
                    default: 0,
                    trim: true
                },
                // Stock update date is required as it is primary object for inventory update.
                updatedDate: {
                    type: Date,
                    default: Date.now
                },
                updatedUser: {
                    type: Schema.ObjectId,
                    ref: 'User'
                }
            }],
            updatedDate: {
                type: Date,
                default: Date.now
            },
            updatedUser: {
                type: Schema.ObjectId,
                ref: 'User'
            }
        }],
        // For this batch, lot or stock item is quantity is deducted when particular order is confirmed for shipping this batch.
        orders: [{
            order: {
                type: Schema.ObjectId,
                ref: 'Order'
            },
            count: {
                type: Number,
                default: 0,
                trim: true
            },
            updatedDate: {
                type: Date,
                default: Date.now
            },
            updatedUser: {
                type: Schema.ObjectId,
                ref: 'User'
            }
        }]
    },

    certificate: {
        certificationBody: {
            contact: {
                type: Schema.ObjectId,
                ref: 'Contact'
            },
            nVipaniUser: {
                type: Schema.ObjectId,
                ref: 'User'
            }
        },
        /*
        TODO:
        certificateRef: {
                        type: Schema.ObjectId,
                        ref: 'Certificate'
                    }
                },*/
        certificateType: {
            type: String,
            enum: ['Farmer', 'Company', 'Products'],
            default: 'Company'
        },
        //Certificate can be PDF, PNG, JPEG
        certificateDocument: {
            type: String,
            default: 'modules/inventories/img/profile/default-certificate.png'
        },
        validity: {
            startDate: {
                type: Date,
                default: ''
            },
            endDate: {
                type: Date,
                default: ''
            }
        },
        certificateStatus: {
            type: String,
            enum: ['RequestSent', 'InProgress', 'Certified'],
            default: 'RequestSent'
        },
        // Examples:
        // Size Vs 10m, 20m, 30m
        // Color Vs Blue, Black, White, Yellow
        // Fineness Vs Superior, Medium, Low
        gradeDefinition: [{
            attributeKey: {
                type: String,
                default: '',
                trim: true
            },
            attributeValue: {
                type: String,
                default: '',
                trim: true
            }
        }],
        // Examples
        // Quality - "Superior", "Poor" and "Medium" etc
        //
        qualityDefinition: [{
            attributeKey: {
                type: String,
                default: '',
                trim: true
            },
            attributeValue: {
                type: String,
                default: '',
                trim: true
            }
        }]
    },
    created: {
        type: Date,
        default: Date.now
    },
    user: {
        type: Schema.ObjectId,
        ref: 'User'
    },
    stockOwner: {
        businessUnit:{
            type: Schema.ObjectId,
            ref: 'BusinessUnit'
        },
        businessUnitNumber: {
            type: String,
            default: '',
            trim: true
        },
        businessUnitName: {
            type: String,
            default: '',
            trim: true
        },
        company:{
            type: Schema.ObjectId,
            ref: 'Company'
        },
        companyName:{
            type: String,
            default: '',
            trim: true
        }
    },
    lastUpdated: {
        type: Date,
        default: Date.now
    },
    lastUpdatedUser: {
        type: Schema.ObjectId,
        ref: 'User'
    },
    reviewed: {
        type: Number,
        enum: [0, 1],
        default: 1,
        trim: true
    },
    disabled: {
        type: Boolean,
        required: 'Please set disabled flag',
        default: false
    },
    deleted: {
        type: Boolean,
        required: 'Please set deleted flag',
        default: false
    }
});

StockMasterSchema.set('versionKey', 'stockMasterVersionKey');
mongoose.model('StockMaster', StockMasterSchema);
StockMasterSchema.pre('save', function (next) {
    var doc = this;
    if(this.isNew){
        var counterUtil = require('../controllers/utils/common.counter.util');
        var globalUtil = require('../controllers/utils/common.global.util');
        counterUtil.getBusinessUnitCodeById(doc.currentOwner.businessUnit,function (bError,businessUnit) {
            if (bError) {
                return next(bError);
            }else {
                counterUtil.getNextBatchSequenceNumber(doc, function (error, batchSeqNumber) {
                    if (error) {
                        next(error);
                    } else {
                        if (doc.inventory) {
                            if (!doc.currentBalance) {
                                doc.currentBalance = 0;
                            }
                            doc.model('Inventory').update({_id: doc.inventory}, {
                                $addToSet: {stockMasters: doc._id},
                                $inc: {numberOfUnits: doc.currentBalance}
                            }).exec(function (inventoryStockErr, inventoryStockUpdate) {
                                if (inventoryStockErr) {
                                    next(inventoryStockErr);
                                } else if (!inventoryStockUpdate) {
                                    next(new Error('Stock is not added to inventory :'));
                                } else {
                                  /*  doc.code = globalUtil.leftPadWithZeros(batchSeqNumber, 5);*/
                                    doc.currentOwner.businessUnitCode=businessUnit.code;
                                    doc.currentOwner.companyCode=businessUnit.company.code;
                                    var date = new Date();
                                    doc.created = date;
                                    var dd = '' + date.getDate();
                                    var mm = '' + (date.getMonth() + 1);
                                    var yyyy = '' + date.getFullYear();

                                    if (mm.length < 2) {
                                        mm = '0' + mm;
                                    }
                                    if (dd.length < 2) {
                                        dd = '0' + dd;
                                    }
                                    doc.code =  doc.currentOwner.companyCode +'-'+doc.currentOwner.businessUnitCode+'-'+dd + mm + yyyy +'-'+globalUtil.leftPadWithZeros(batchSeqNumber, 5);
                                    next();
                                }
                            });
                        } else {
                            next(new Error('No Inventory to add stock Master'));
                        }
                    }
                });
            }
        });
    }else {
        next();
    }

});
