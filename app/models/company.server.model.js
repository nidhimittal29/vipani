'use strict';

/**
 * Module dependencies.
 */
var mongoose = require('mongoose'),
    BusinessUnit = mongoose.model('BusinessUnit'),
    globalUtil = require('../controllers/utils/common.global.util'),Counter,
    Schema = mongoose.Schema;


var validateLength128 = function (property) {
    if (property) {
        return (property.length <= 128);
    }
    return true;

};


/**
 * A Validation function for checking the length
 */
var validatePhoneNumberLength = function (property) {
    if (property) {
        return (property.length <= 11 && property.length >= 10);
    }
    return true;
};

/**
 * A Validation function for checking the length
 */
var validatePincodeLength = function (property) {
    if (property) {
        return (property.length === 6);
    }
    return true;
};
/**
 * A Validation function for checking the length
 */
var validateLength1024 = function (property) {
    if (property) {
        return (property.length <= 1024);
    }
    return true;
};
/**
 * Company Schema
 */
var CompanySchema = new Schema({
    name: {
        type: String,
        default: '',
        required: 'Please fill Company name',
        trim: true
    },
    code: {
        type: String,
        default: '',
        trim: true
    },
    businessUnitSeqNumber: {
        type: Number,
        default: 0
    },
    description: {
        type: String,
        default: '',
        trim: true
    },
    category: {
        //Seller
        seller: {
            type: Boolean,
            default: false
        },
        //Buyer
        buyer: {
            type: Boolean,
            default: false
        },
        //Mediator
        mediator: {
            type: Boolean,
            default: false
        }
    },
    registrationCategory:{
        type: Schema.ObjectId,
        ref: 'RegistrationCategories'

    },
    businessType: {
        type: String,
        enum: ['Trader','Farmer','Wholesaler','Distributor','Manufacturer','Retailer','Importer','Exporter']
    },
    emails: [{
        email: {
            type: String,
            default: '',
            match: [/.+\@.+\..+/, 'Please fill a valid email'],
            //validate: [validateLocalStrategyProperty, 'Please fill in your email'],
            trim: true
        },
        emailType: {
            type: String,
            enum: ['Personal', 'Work', 'Other'],
            default: 'Personal'
        },
        primary: {
            type: Boolean,
            default: false
        },
        isPublic: {
            type: Boolean,
            default: true
        }
    }],
    addresses: [{
        addressLine: {
            type: String,
            default: '',
            validate: [validateLength1024, 'The address line should not exceed 1024 characters.'],
            trim: true
        },
        city: {
            type: String,
            default: '',
            validate: [validateLength128, 'The city length should not exceed 128 characters.'],
            trim: true
        },
        state: {
            type: String,
            default: '',
            validate: [validateLength128, 'The state length should not exceed 128 characters.'],
            trim: true
        },
        country: {
            type: String,
            default: '',
            validate: [validateLength128, 'The country length should not exceed 128 characters.'],
            trim: true
        },
        pinCode: {
            type: String,
            default: '',
            validate: [validatePincodeLength, 'Pincode should be 6 digits.'],
            trim: true
        },
        addressType: {
            type: String,
            enum: ['Billing', 'Receiving', 'Shipping','Invoice'],
            default: 'Billing'
        },
        primary: {
            type: Boolean, default: false
        },
        isPublic: {
            type: Boolean,
            default: true
        }
    }],
    phones: [{
        phoneNumber: {
            type: String,
            default: '',
            validate: [validatePhoneNumberLength, 'Phone Number length should be 10 to 11 digits.'],
            trim: true
        },
        phoneType: {
            type: String,
            enum: ['Mobile', 'Office', 'Work', 'Other'],
            default: 'Mobile'
        },
        primary: {
            type: Boolean,
            default: false
        },
        isPublic: {
            type: Boolean,
            default: true
        }
    }],
    settings: {
        paymentModes: [
            {selected: {
                    type: Boolean,
                    default: true
                },
                paymentMode:{
                    type: Schema.ObjectId,
                    ref: 'PaymentMode'
                }}],
        paymentOptions: {
            //payOnDelivery
            payOnDelivery: {
                enabled: {
                    type: Boolean,
                    default: true
                },
                discount: {
                    type: Number,
                    default: 0,
                    trim: true
                }
            },
            //payNow
            payNow: {
                enabled: {
                    type: Boolean,
                    default: true
                },
                discount: {
                    type: Number,
                    default: 0,
                    trim: true
                }
            },
            lineOfCredit: {
                days: {
                    type: Number,
                    default: 30
                },
                discount: {
                    type: Number,
                    default: 0,
                    trim: true
                },
                enabled: {
                    type: Boolean,
                    default: true
                }
            },
            // Installments, Pay On Installments, Installments Pay
            //
            installments: {
                enabled: {
                    type: Boolean,
                    default: true
                },
                discount: {
                    type: Number,
                    default: 0,
                    trim: true
                },
                options: [
                    {
                        description: {
                            type: String,
                            default: 'Upfront Partial Payment'
                        },
                        days: {
                            type: Number,
                            default: 0
                        },
                        percentage: {
                            type: Number,
                            default: 40
                        }
                    }
                ]
            }
        },
        paymentTerms: [{
            paymentTerm: {
                type: Schema.ObjectId,
                ref: 'PaymentTerm'
            },
            selected:{
                type: Boolean,
                default: true
            }
        }],

        reports: {
            dailyStock: {
                enabled: {
                    type: Boolean,
                    default: true
                },
                format: [{
                    type: String,
                    enum: ['XLS', 'PDF', 'HTML', 'XLSX', 'CSV'],
                    default: 'PDF',
                    trim: true
                }]
            }
        }
    },
    bankAccountDetails:{
        bankAccountNumber: {
            type: String,
            default: '',
            trim: true
        },
        accountType: {
            type: String,
            enum: ['Saving', 'Current'],
            default: 'Current'
        },
        bankName: {
            type: String,
            default: '',
            trim: true
        },
        bankBranchName: {
            type: String,
            default: '',
            trim: true
        },
        ifscCode: {
            type: String,
            default: '',
            trim: true
        }
    },
    bankAccountDetailsProof:{
        type: String,
        default: '',
        trim: true
    },
    tinNumber: {
        type: String,
        default: '',
        trim: true
    },
    tinNumberProof: {
        type: String,
        default: '',
        trim: true
    },
    vatNumber: {
        type: String,
        default: '',
        trim: true
    },
    vatNumberProof: {
        type: String,
        default: '',
        trim: true
    },
    cstNumber: {
        type: String,
        default: '',
        trim: true
    },
    cstNumberProof: {
        type: String,
        default: '',
        trim: true
    },
    panNumber: {
        type: String,
        default: '',
        trim: true
    },
    panNumberProof: {
        type: String,
        default: '',
        trim: true
    },
    gstinNumber: {
        type: String,
        default: '',
        trim: true
    },
    gstinNumberProof: {
        type: String,
        default: '',
        trim: true
    },
    profileUrl: {
        type: String,
        unique: 'Profile URL should be unique',
        trim: true
    },
    profileImageURL: {
        type: String,
        default: 'modules/companies/img/profile/default.png'
    },
    croppedProfileImageURL: {
        type: String,
        default: 'modules/companies/img/profile/default-resize-240-240.png'
    },
//removed as this is never used.not relevant anymore.
    /* inventories: [{
         inventory: {
             type: Schema.ObjectId,
             ref: 'Inventory'
         }
     }],*/
    segments: [{
        segment: {
            type: Schema.ObjectId,
            ref: 'BusinessSegments'
        },
        categories: [{
            category: {
                type: Schema.ObjectId,
                ref: 'Category'
            },
            enabled:{
                type: Boolean,
                default: false
            }
        }]
    }],
    employees: [{
        user: {
            type: Schema.ObjectId,
            ref: 'User'
        },
        businessUnits: [{
            businessUnit:{
                type: Schema.ObjectId,
                ref: 'BusinessUnit'
            },
            status: {
                type: String,
                enum: ['Active', 'Inactive','Request'],
                default: 'Request'
            }

        }],
        statusToken:{
            type: String,
            default: '',
            trim: true
        },
        userGroup: {
            type: Schema.ObjectId,
            ref: 'UserGroup'
        },
        status: {
            type: String,
            enum: ['Active', 'Inactive','Request'],
            default: 'Request'
        },
        fromDate:{
            type: Date,
            default: Date.now
        },
        tillNow:{
            type: Date,
            default: Date.now
        }

    }
    ],
    mediators: [{
        user: {
            type: Schema.ObjectId,
            ref: 'User'
        },
        businessUnits: [{
            businessUnit:{
                type: Schema.ObjectId,
                ref: 'BusinessUnit'
            },
            status: {
                type: String,
                enum: ['Active', 'Inactive','Request'],
                default: 'Request'
            }
        }],
        status: {
            type: String,
            enum: ['Active', 'Inactive','Request'],
            default: 'Request'
        }
    }
    ],
    accountingFirms: [{
        user: {
            type: Schema.ObjectId,
            ref: 'User'
        },
        status: {
            type: String,
            enum: ['Active', 'Inactive','Request'],
            default: 'Request'
        }
    }
    ],
    certificationFirms: [{
        user: {
            type: Schema.ObjectId,
            ref: 'User'
        },
        status: {
            type: String,
            enum: ['Active', 'Inactive','Request'],
            default: 'Request'
        }
    }
    ],
    businessUnits: [{
        businessUnit: {
            type: Schema.ObjectId,
            ref: 'BusinessUnit'
        },
        defaultBusinessUnit: {
            type: Boolean,
            default: false
        },
        status: {
            type: String,
            enum: ['Active', 'Inactive','Request','Removed'],
            default: 'Request'
        }
    }
    ],
    status: {
        type: String,
        enum: ['Not Verified', 'Verified'],
        default: 'Not Verified'
    },
    disabled: {
        type: Boolean,
        required: 'Please set disabled flag',
        default: false
    },
    deleted: {
        type: Boolean,
        required: 'Please set deleted flag',
        default: false
    },
    updateHistory: [{
        modifiedOn: {
            type: Date,
            default: Date.now
        },
        modifiedBy: {
            type: Schema.ObjectId,
            ref: 'User'
        }
    }],
    created: {
        type: Date,
        default: Date.now
    },
    lastUpdated: {
        type: Date,
        default: Date.now
    },
    isPredefined:{
        type: Boolean,
        default: true
    },
    user: {
        type: Schema.ObjectId,
        ref: 'User'
    }
});
function getPaymentTerms(paymentTerm,paymentMode,user,done){
    paymentTerm.find({user:{$in:[null,user]}},'_id').exec(function (paymentTermErr, paymentTerms) {
        if(paymentTermErr){
            done(paymentTermErr);
        }else if(paymentTerms.length ===0){
            done(null,null,null);
        }else {
            paymentMode.find({user:{$in:[null,user]}},'_id').exec(function (paymentModeErr, paymentModes) {
                if(paymentModeErr){
                    done(paymentModeErr);
                }else if(paymentModes.length ===0){
                    done(null,paymentTerms,null);
                }else{
                    done(null,paymentTerms,paymentModes);
                }
            });
        }

    });
}

function paymentModeQuery(paymentModes,async,done) {
    var paymentModeQuery=[];
    if( paymentModes && paymentModes.length>0){
        async.forEachSeries(paymentModes, function (eachPaymentMode, callback) {
            paymentModeQuery.push({selected:true,paymentMode:eachPaymentMode._id});
            callback();
        }, function (error) {
            done(error,paymentModeQuery);
        });

    }else{
        done(new Error('No Payment modes'),null);
    }

}
function getPaymentTermsQuery(paymentTerm,paymentMode,user,done) {
    var async = require('async');
    getPaymentTerms(paymentTerm, paymentMode, user, function (err, paymentTerms, paymentModes) {
        if (!err && paymentTerms && paymentTerms.length > 0) {
            paymentModeQuery(paymentModes,async,function (paymentModeQueryErr,paymentModeQuery) {
                if(paymentModeQueryErr){
                    done(paymentModeQueryErr);
                }else {
                    var query=[];
                    async.forEachSeries(paymentTerms, function (eachPayment, callback) {

                        query.push({paymentTerm:eachPayment._id,selected:true});
                        callback();
                    }, function (err) {
                        done(null,query,paymentModeQuery);
                    });
                }
            });
        }
    });
}

function findOrCreateCounter(doc,done){
    Counter = mongoose.model('Counter');
    var seqUtil = require('../controllers/utils/common.counter.util');
    var key=seqUtil.getCounterQuery('companyId');
    Counter.findOneAndUpdate({}, {$inc: {'companySeq.companySeqNumber': 1}}, function (error, counter) {
        /*Counter.findOne({key:doc._id}, function (error, counter) {*/
        if (error) {
            done(error);

        }else if (counter === null) {
            var defaultCounter =seqUtil.setCounterQuery({companySeqNumber:1},new Counter());
            defaultCounter.companySeq.companies.push({company:doc._id});
            defaultCounter.save(function (err) {
                if (err) {
                    done(err);
                }else{
                    done(null,defaultCounter);
                }
            });
        } else {
            Counter.findOne({'companySeq.companies.company':doc._id}, function (oldCompanyError, fetchCounter) {
                if(oldCompanyError){
                    done(oldCompanyError,null);
                }else if(fetchCounter===null){

                    Counter.update( {}, {$inc: {'companySeqNumber': 1},$addToSet : { "companySeq.companies" :{company:doc._id} }}, function (updateCompanyError, updateCompanyCounter) {
                        if (updateCompanyError) {
                            done(updateCompanyError);
                        } else {
                            done(null, updateCompanyCounter);
                        }
                    });

                }else{
                    done(null, fetchCounter);
                }

            });
        }


    });

}




CompanySchema.pre('save', function (next) {
    var doc = this;
    if (this.isNew) {
        doc.settings.paymentTerms = [];

        /*doc.settings.paymentOptions.installments.options = [];
        doc.settings.paymentOptions.installments.options.push({
            description: 'Advance Payment',
            days: 0,
            percentage: 40
        });
        doc.settings.paymentOptions.installments.options.push({
            description: 'First Payment',
            days: 30,
            percentage: 40
        });
        doc.settings.paymentOptions.installments.options.push({description: 'Final Payment', days: 45, percentage: 20});
*/
        if (doc.profileImageURL) {

            var fileName = doc.profileImageURL.substring(0, doc.profileImageURL.lastIndexOf('.'));

            doc.croppedProfileImageURL = fileName + '-resize-240-240.png';
        }
        var seqUtil = require('../controllers/utils/common.counter.util');
        seqUtil.getNextCompanySequenceNumber(doc,function (counterError,companySeqNumber) {

            /*findOrCreateCounter(doc,function(counterError,defaultCounter) {*/
            if(counterError){
                return next(counterError);
            }else {
                /* BusinessUnit find*/
                var businessUnit = new BusinessUnit();
                businessUnit.name = 'RegisteredOffice (default)';
                mongoose.model('UserGroup').findOne({name: 'Admin'}).exec(function (userErr, userGroup) {
                    if (userErr) {
                        return next(userErr);
                    } else {
                        doc.code=globalUtil.leftPadWithZeros(companySeqNumber,5);
                        /* doc.businessUnitSeqNumber = 1;
                         var businessUnitCode = globalUtil.leftPadWithZeros(doc.businessUnitSeqNumber, 5);
                         doc.businessUnitSeqNumber = 2;
                         businessUnit.code = businessUnitCode;*/
                        businessUnit.emails = doc.emails;
                        businessUnit.phones = doc.phones;
                        businessUnit.company = doc._id;
                        businessUnit.employees = [];
                        businessUnit.defaultBusinessUnit=true;
                        businessUnit.employees.push({user: doc.user, incharge: true, userGroup: userGroup._id});
                        seqUtil.getNextBusinessUnitSequenceNumber(businessUnit,function (counterError,businessUnitSequenceNumber) {
                            if(counterError){
                                return next(counterError);
                            }else {
                                businessUnit.code = globalUtil.leftPadWithZeros(businessUnitSequenceNumber,5);
                                businessUnit.save(function (err) {
                                    if (err) {
                                        return next(err);
                                    } else {
                                        doc.businessUnits = [];
                                        doc.employees = [];

                                        // How to update the status as part of company employee.

                                        doc.employees.push({
                                            status: 'Active',
                                            user: doc.user,
                                            businessUnits: [{businessUnit: businessUnit._id}],
                                            userGroup: userGroup._id
                                        });
                                        doc.businessUnits.push({
                                            status: 'Active',
                                            businessUnit: businessUnit._id,
                                            defaultBusinessUnit: true
                                        });
                                        getPaymentTermsQuery(mongoose.model('PaymentTerm'), mongoose.model('PaymentMode'), doc.user, function (paymentTermErr, paymentTerms, paymentModes) {
                                            if (paymentTermErr) {
                                                return next(paymentTermErr);
                                            } else {
                                                if (paymentTerms) {
                                                    doc.settings.paymentTerms = paymentTerms;
                                                    doc.settings.paymentModes = paymentModes;
                                                }
                                                next();
                                            }

                                        });

                                    }
                                });
                            }
                        });
                    }
                });
            }
        });
    } else {
        if (doc.profileImageURL) {
            var fileName1 = doc.profileImageURL.substring(0, doc.profileImageURL.lastIndexOf('.'));

            var croppedProfileImageURL = fileName1 + '-resize-240-240.png';

            if (this.croppedProfileImageURL !== croppedProfileImageURL) {
                this.croppedProfileImageURL = croppedProfileImageURL;
            }
        }

        next();
    }
});
CompanySchema.set('versionKey', 'companiesVersionKey');
mongoose.model('Company', CompanySchema);
