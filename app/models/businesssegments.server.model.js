'use strict';

/**
 * Module dependencies.
 */
var mongoose = require('mongoose'),
    Schema = mongoose.Schema;

var BusinessSegmentsSchema = new Schema({
    name: {
        type: String,
        default: '',
        required: 'Please fill Segment name',
        trim: true
    },
    isSpecific:{
        type: Boolean,
        default: false
    },
    categories: [{
        category: {
            type: Schema.ObjectId,
            ref: 'Category'
        }
    }],
    segmentImageURL1: {
        type: String,
        default: 'modules/categories/img/profile/default.png'
    },
    croppedSegmentImageURL1: {
        type: String,
        default: 'modules/categories/img/profile/default.png'
    },
    created: {
        type: Date,
        default: Date.now
    },
    description:{
        type: String,
        default: '',
        trim: true
    },
    lastUpdated: {
        type: Date,
        default: Date.now
    },
    disabled: {
        type: Boolean,
        required: 'Please set disabled flag',
        default: false
    },
    deleted: {
        type: Boolean,
        required: 'Please set deleted flag',
        default: false
    }
});

mongoose.model('BusinessSegments', BusinessSegmentsSchema);
