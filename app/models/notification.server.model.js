'use strict';

/**
 * Module dependencies.
 */
var mongoose = require('mongoose'),
    Schema = mongoose.Schema;

/**
 * Notification Schema
 */
var NotificationSchema = new Schema({
    title: {
        type: String,
        default: '',
        required: 'Please fill Notification title',
        trim: true
    },
    code:{
        type: String,
        default: '',
        trim: true
    },
    target: {
        contact: {
            type: Schema.ObjectId,
            ref: 'Contact'
        },
        nVipaniUser: {
            type: Schema.ObjectId,
            ref: 'User'
        },
        nVipaniCompany:{
            type: Schema.ObjectId,
            ref: 'Company'
        },
        businessUnit:{
            type: Schema.ObjectId,
            ref: 'BusinessUnit'
        }
    },
    source: {
        message: {
            type: Schema.ObjectId,
            ref: 'Message'
        },
        order: {
            type: Schema.ObjectId,
            ref: 'Order'
        },
        offer: {
            type: Schema.ObjectId,
            ref: 'Offer'
        }
    },
    type: {
        type: String,
        enum: ['Message', 'Order', 'Offer', 'OrderComment', 'OrderStatus', 'OrderPaymentStatus', 'OfferComment', 'OfferStatus'],
        default: 'Message'
    },
    emailMessage: {
        type: String,
        default: ''
    },
    shortMessage: {
        type: String,
        default: ''
    },
    channel: [{
        type: String,
        enum: ['Email', 'SMS', 'Push'],
        default: 'Email'
    }],
    criticality: [{
        type: String,
        enum: ['High', 'Medium', 'Low'],
        default: 'Low'
    }],
    viewed: {
        type: Boolean,
        default: false
    },
    processed: {
        type: Boolean,
        default: false
    },
    smsProcessed: {
        type: Boolean,
        default: false
    },
    pushProcessed: {
        type: Boolean,
        default: false
    },
    statusMessage: {
        type: String,
        default: ''
    },
    smsStatusMessage: {
        type: String,
        default: ''
    },
    pushStatusMessage: {
        type: String,
        default: ''
    },
    dueDate: {
        type: Date,
        default: ''
    },
    created: {
        type: Date,
        default: Date.now
    },
    user: {
        type: Schema.ObjectId,
        ref: 'User'
    },
    lastUpdated: {
        type: Date,
        default: Date.now
    },
    disabled: {
        type: Boolean,
        required: 'Please set disabled flag',
        default: false
    },
    deleted: {
        type: Boolean,
        required: 'Please set deleted flag',
        default: false
    }
});

mongoose.model('Notification', NotificationSchema);
