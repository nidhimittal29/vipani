'use strict';

/**
 * Module dependencies.
 */
var mongoose = require('mongoose'),
    Schema = mongoose.Schema;

/**
 * Message Schema
 */
var MessageSchema = new Schema({
    body: {
        type: String,
        default: '',
        required: 'Please fill Message Body',
        trim: true
    },
    order: {
        type: Schema.ObjectId,
        ref: 'Order'
    },
    offer: {
        type: Schema.ObjectId,
        ref: 'Offer'
    },
    type: {
        type: String,
        enum: ['Message', 'Order', 'Offer'],
        default: 'Message'
    },
    parent: {
        type: Schema.ObjectId,
        ref: 'Message'
    },
    children:[ {
        type: Schema.ObjectId,
        ref: 'Message'
    }],
    lastUpdated: {
        type: Date,
        default: Date.now
    },
    created: {
        type: Date,
        default: Date.now
    },
    user: {
        type: Schema.ObjectId,
        ref: 'User'
    },
    disabled: {
        type: Boolean,
        required: 'Please set disabled flag',
        default: false
    },
    deleted: {
        type: Boolean,
        required: 'Please set deleted flag',
        default: false
    }
});

mongoose.model('Message', MessageSchema);
