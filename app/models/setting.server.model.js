'use strict';

/**
 * Module dependencies.
 */
var mongoose = require('mongoose'),
    Schema = mongoose.Schema;


/**
 * Settings Schema
 */
var SettingSchema = new Schema({
    contacts: {
        includeInactive: {
            type: Boolean,
            default: false
        },
        showPhoneNumber: {
            type: Boolean,
            default: false
        },
        nVipaniUserNotification: {
            type: Boolean,
            default: true
        },
        labels: [{
            name: {
                type: String,
                default: ''
            },
            value: {
                type: String,
                default: ''
            }
        }
        ]
    },
    inventory: {
        enableTracking: {
            type: Boolean,
            default: false
        },
        groupBy: {
            type: String,
            enum: ['Product', 'Brand'],
            default: 'Product'
        },
        showNonBillable: {
            type: Boolean,
            default: false
        },
        lowInventoryAlerts: {
            type: Boolean,
            default: false
        },
        expiryDateAlert: {
            type: Boolean,
            default: false
        },
        includeInactive: {
            type: Boolean,
            default: false
        },
        showSKU: {
            type: Boolean,
            default: false
        },
        labels: [{
            name: {
                type: String,
                default: ''
            },
            value: {
                type: String,
                default: ''
            }
        }
        ]
    },
    offer: {
        subscribeToBuyOffers: {
            type: Boolean,
            default: false
        },
        subscribeToSaleOffers: {
            type: Boolean,
            default: false
        },
        statusSequence: [{
            parent: {
                type: String,
                default: ''
            },
            children: [{
                type: String,
                default: ''
            }]
        }],
        labels: [{
            name: {
                type: String,
                default: ''
            },
            value: {
                type: String,
                default: ''
            }
        }
        ]
    },
    order: {
        enableQuickOrder: {
            type: Boolean,
            default: false
        },
        statusSequence: [{
            parent: {
                type: String,
                default: ''
            },
            children: [{
                type: String,
                default: ''
            }]
        }],
        labels: [{
            name: {
                type: String,
                default: ''
            },
            value: {
                type: String,
                default: ''
            }
        }
        ]
    },
    payment: {
        paymentModes: {
            cash: {
                type: Boolean,
                default: true
            },
            cheque: {
                type: Boolean,
                default: true
            },
            // NEFT
            neft: {
                type: Boolean,
                default: true
            },
            //RTGS
            rtgs: {
                type: Boolean,
                default: true
            },
            //IMPS
            imps: {
                type: Boolean,
                default: true
            },
            //UPI
            upi: {
                type: Boolean,
                default: true
            },
            //Wallet
            wallet: {
                type: Boolean,
                default: true
            },
            online: {
                enabled: {
                    type: Boolean,
                    default: false
                },
                paymentgateway: [{
                    // Citrus Pay, PayUMoney,
                    name: {
                        type: String,
                        default: ''
                    },
                    account: {
                        type: String,
                        default: ''
                    },
                    accessKey: {
                        type: String,
                        default: ''
                    },
                    secretKey: {
                        type: String,
                        default: ''
                    }
                }]
            }
        },
        roundOfDecimals: {
            type: Number,
            default: 2,
            trim: true
        },
        paymentTerms: [{
            paymentTerm: {
                type: Schema.ObjectId,
                ref: 'PaymentTerm'
            },
            preferred: {
                type: Boolean,
                default: false
            }
        }
        ],
        labels: [{
            name: {
                type: String,
                default: ''
            },
            value: {
                type: String,
                default: ''
            }
        }
        ]
    },
    company: {
        theme: {
            type: String,
            default: ''
        },
        labels: [{
            name: {
                type: String,
                default: ''
            },
            value: {
                type: String,
                default: ''
            }
        }
        ]
    },
    message: {
        sms: {
            offer: {
                type: String,
                default: ''
            },
            order: {
                type: String,
                default: ''
            },
            comment: {
                type: String,
                default: ''
            }
        },
        whatsapp: {
            offer: {
                type: String,
                default: ''
            },
            order: {
                type: String,
                default: ''
            },
            comment: {
                type: String,
                default: ''
            }
        },
        email: {
            offer: {
                subject: {
                    type: String,
                    default: ''
                },
                body: {
                    type: String,
                    default: ''
                }
            },
            order: {
                subject: {
                    type: String,
                    default: ''
                },
                body: {
                    type: String,
                    default: ''
                }
            },
            comment: {
                subject: {
                    type: String,
                    default: ''
                },
                body: {
                    type: String,
                    default: ''
                }
            }
        },
        labels: [{
            name: {
                type: String,
                default: ''
            },
            value: {
                type: String,
                default: ''
            }
        }
        ]
    },
    reports: {
        dailyStock: {
            enabled: {
                type: Boolean,
                default: true
            },
            format: [{
                type: String,
                enum: ['XLS', 'PDF', 'HTML', 'XLSX', 'CSV'],
                default: 'PDF',
                trim: true
            }]
        }
    },

    disabled: {
        type: Boolean,
        required: 'Please set disabled flag',
        default: false
    },
    deleted: {
        type: Boolean,
        required: 'Please set deleted flag',
        default: false
    },
    created: {
        type: Date,
        default: Date.now
    },
    lastUpdated: {
        type: Date,
        default: Date.now
    },
    lastUpdatedUser: {
        type: Schema.ObjectId,
        ref: 'User'
    },
    user: {
        type: Schema.ObjectId,
        ref: 'User'
    }
});


SettingSchema.set('versionKey', 'settingVersionKey');
mongoose.model('Setting', SettingSchema);
