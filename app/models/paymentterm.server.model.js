'use strict';

/**
 * Module dependencies.
 */
var mongoose = require('mongoose'),
    Schema = mongoose.Schema;

var PaymentTermSchema = new Schema({
    name: {
        type: String,
        default: '',
        required: 'Please fill Payment Term Name',
        trim: true
    },
    code:{
        type: String,
        default: '',
        trim: true
    },
    description:{
        type: String,
        trim:true,
        default:''
    },
    // Net Term - Net X Days, Net Term Discount - Y/Z Net X Days (Y Discount till Z days and the payment term is X Days),
    // Other types are self explanatory. We may use PayNow instead of PayInAdvance
    type: {
        type: String,
        enum: ['NetTerm', 'NetTermDiscount', 'PayInAdvance', 'PayNow', 'PayOnDelivery', 'Installments'],
        default: 'PayNow',
        trim: true
    },
    netTerm: {
        type: Number,
        default: 0
    },
    discountTerm: {
        type: Number,
        default: 0
    },
    termDiscount: {
        type: Number,
        default: 0
    },
    discount: {
        type: Number,
        default: 0
    },
    installments: [
        {
            description: {
                type: String,
                default: 'Upfront Partial Payment'
            },
            days: {
                type: Number,
                default: 0
            },
            percentage: {
                type: Number,
                default: 40
            }
        }
    ],
    disabled: {
        type: Boolean,
        required: 'Please set disabled flag',
        default: false
    },
    deleted: {
        type: Boolean,
        required: 'Please set deleted flag',
        default: false
    },
    created: {
        type: Date,
        default: Date.now
    },
    user: {
        type: Schema.ObjectId,
        ref: 'User'
    },
    company:{
        type: Schema.ObjectId,
        ref: 'Company'
    },
    lastUpdated: {
        type: Date,
        default: Date.now
    },
    lastUpdatedUser: {
        type: Schema.ObjectId,
        ref: 'User'
    }
});
PaymentTermSchema.set('versionKey', 'paymentTermVersionKey');
mongoose.model('PaymentTerm', PaymentTermSchema);
