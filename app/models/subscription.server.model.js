'use strict';

/**
 * Module dependencies.
 */
var mongoose = require('mongoose'),
    Schema = mongoose.Schema;

var SubscriptionSchema = new Schema({
    name: {
        type: String,
        default: '',
        required: 'Please fill Payment Term Name',
        trim: true
    },

    type: {
        type: String,
        enum: ['Free', 'Paid'],
        default: 'Free',
        trim: true
    },
    scope: {
        type: String,
        enum: ['User', 'Organization', 'Other'],
        default: 'Organization',
        trim: true
    },
    // The subscription
    termType: {
        type: String,
        enum: ['Monthly', 'Quarterly', 'Half Yearly', 'Yearly'],
        default: 'Monthly',
        trim: true
    },
    billingTermType: {
        type: String,
        enum: ['Monthly', 'Quarterly', 'Half Yearly', 'Yearly'],
        default: 'Monthly',
        trim: true
    },
    discountType: {
        type: String,
        enum: ['Monthly', 'Quarterly', 'Half Yearly', 'Yearly'],
        default: 'Monthly',
        trim: true
    },
    discount: {
        type: Number,
        default: 0
    },
    price: {
        type: Number,
        default: 0
    },
    netPrice: {
        type: Number,
        default: 0
    },
    freeTerm: {
        type: Number,
        default: 0
    },
    features: [
        {
            name: {
                type: String,
                default: ''
            },
            // If there is any limit for the feature
            // for this subscription then charge type will be set to 'Limited'
            chargeType: {
                type: String,
                enum: ['Limited', 'Unlimited'],
                default: 'Limited',
                trim: true
            },
            // Limit  - Total amount of the offer, orders etc.
            limit: {
                type: Number,
                default: 0
            },
            // Limit count - Number of Offers, Order and Products etc
            limitCount: {
                type: Number,
                default: 0
            }
        }
    ],
    disabled: {
        type: Boolean,
        required: 'Please set disabled flag',
        default: false
    },
    deleted: {
        type: Boolean,
        required: 'Please set deleted flag',
        default: false
    },
    created: {
        type: Date,
        default: Date.now
    },
    user: {
        type: Schema.ObjectId,
        ref: 'User'
    },
    lastUpdated: {
        type: Date,
        default: Date.now
    },
    lastUpdatedUser: {
        type: Schema.ObjectId,
        ref: 'User'
    }
});
SubscriptionSchema.set('versionKey', 'subscriptionVersionKey');
mongoose.model('Subscription', SubscriptionSchema);
