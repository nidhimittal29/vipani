'use strict';

/**
 * Module dependencies.
 */
var mongoose = require('mongoose'),
    Schema = mongoose.Schema;

var RegistrationCategories = new Schema({
    name: {
        type: String,
        default: '',
        required: 'Please fill Register Category name',
        trim: true
    },
    // 1- Radio Buttons - Sell, 2 - Checkboxes - Buy and Sell, 3-  Checkboxes - Buy, 4 - Radio Buttons - Buy and Sell,
    // 5 - Radio Buttons - Mediation, 6 - Checkboxes - Mediation
    // Manufacturer, 1, true
    // Distributor, 2, true
    // Retailer, 3, true
    // Commission Agent, 5, true
    // Trader, 2, true
    // Exporter, 2, true
    // Importer, 2, true
    // FPO/FPC, 4, true
    //Buy 1,
    // Sell 2,
    // Mediate 3,
    // Buy,Sell 4,
    //Buy, Mediate 5
    //Sell ,Mediate 6
    //Sell ,Buy ,Mediate 7

    //'Mediator', 'Accountant', 'CertificationAuthority', 'ServiceProvider'

    type: {
        type: Number
    },
    category:{
        type: Number
    },
    enabled:{
        type: Boolean,
        default: false
    },
    disabled: {
        type: Boolean,
        required: 'Please set disabled flag',
        default: false
    },
    deleted: {
        type: Boolean,
        required: 'Please set deleted flag',
        default: false
    },
    created: {
        type: Date,
        default: Date.now
    },
    isDefault:{
        type: Boolean,
        default: false
    },
    lastUpdated: {
        type: Date,
        default: Date.now
    },
    description:{
        type: String,
        default: '',
        trim: true
    },
    rImageURL: {
        type: String,
        default: 'modules/core/img/nvc-img/Retailer.png'
    },
    croppedRcImageURL: {
        type: String,
        default: 'modules/contacts/img/default.png'
    }
});

mongoose.model('RegistrationCategories', RegistrationCategories);
