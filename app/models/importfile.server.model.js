'use strict';

/**
 * Module dependencies.
 */
var mongoose = require('mongoose'),
    Schema = mongoose.Schema;

var ImportFileSchema = new Schema({
    // File location where it is stored
    fileName: {
        type: String,
        default: ''
    },
    filePath: {
        type: String,
        default: ''
    },
    importType: {
        type: String,
        enum: ['Contacts', 'InventoryItems', 'Invoices', 'PaymentsAndReceipts'],
        default: ''
    },
    // Header column names
    header: [{
        type: String,
        default: ''
    }],
    // All lines import data
    data: [{
        srcLine: {
            type: Number,
            default: ''
        },
        lineData: [{
            type: String,
            default: ''
        }]
    }
    ],
    created: {
        type: Date,
        default: Date.now
    },
    lastUpdated: {
        type: Date,
        default: Date.now
    },
    user: {
        type: Schema.ObjectId,
        ref: 'User'
    },
    lastUpdatedUser: {
        type: Schema.ObjectId,
        ref: 'User'
    },
    disabled: {
        type: Boolean,
        required: 'Please set disabled flag',
        default: false
    },
    deleted: {
        type: Boolean,
        required: 'Please set deleted flag',
        default: false
    }
});

mongoose.model('ImportFile', ImportFileSchema);
