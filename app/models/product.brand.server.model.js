'use strict';

/**
 * Module dependencies.
 */
var mongoose = require('mongoose'),
    Category = mongoose.model('Category'),
    Schema = mongoose.Schema;

/**
 * Product Brand Schema
 */
var ProductBrandSchema = new Schema({
    name: {
        type: String,
        default: '',
        required: 'Please fill Brand name',
        trim: true
    },
    variety: {
        type: String,
        default: '',
        trim: true
    },
    productCategory: {
        type: Schema.ObjectId,
        ref: 'Category'
    },

    brandOwner: {
        type: Schema.ObjectId,
        ref: 'Contact'
    },
    manufacturers: [{
        type: Schema.ObjectId,
        ref: 'Contact'
    }],
    hsncode: {
        type: Schema.ObjectId,
        ref: 'Hsncodes'
    },
    taxGroup: {
        type: Schema.ObjectId,
        ref: 'TaxGroup'
    },
    unitOfMeasures: [{
        type: Schema.ObjectId,
        ref: 'UnitOfMeasure'
    }],
    productBrandImageURL1: {
        type: String,
        default: 'modules/products/img/profile/default.png'
    },
    productBrandImageURL2: {
        type: String,
        default: 'modules/products/img/profile/default.png'
    },
    productBrandImageURL3: {
        type: String,
        default: 'modules/products/img/profile/default.png'
    },
    productBrandImageURL4: {
        type: String,
        default: 'modules/products/img/profile/default.png'
    },
    croppedProductBrandImageURL1: {
        type: String,
        default: 'modules/products/img/profile/default-resize-240-240.png'
    },
    croppedProductBrandImageURL2: {
        type: String,
        default: 'modules/products/img/profile/default-resize-240-240.png'
    },
    croppedProductBrandImageURL3: {
        type: String,
        default: 'modules/products/img/profile/default-resize-240-240.png'
    },
    croppedProductBrandImageURL4: {
        type: String,
        default: 'modules/products/img/profile/default-resize-240-240.png'
    },
    description: {
        type: String,
        default: '',
        trim: true
    },
    // Examples:
    // Size Vs 10m, 20m, 30m
    // Color Vs Blue, Black, White, Yellow
    // Fineness Vs Superior, Medium, Low
    gradeDefinition: [{
        attributeKey: {
            type: String,
            default: '',
            trim: true
        },
        attributeValue: {
            type: String,
            default: '',
            trim: true
        }
    }],
    // Examples
    // Quality - "Superior", "Poor" and "Medium" etc
    //
    qualityDefinition: [{
        attributeKey: {
            type: String,
            default: '',
            trim: true
        },
        attributeValue: {
            type: String,
            default: '',
            trim: true
        }
    }],
    // Keywords are array of attribute key value for the product.
    productBrandAttributes: [{
        attributeKey: {
            type: String,
            default: '',
            trim: true
        },
        attributeValue: {
            type: String,
            default: '',
            trim: true
        }
    }],
    fssaiLicenseNumber: {
        type: String,
        default: '',
        trim: true
    },
    fssaiLicenseNumberProof: {
        type: String,
        default: '',
        trim: true
    },
    testCertificate: {
        type: String,
        default: '',
        trim: true
    },

    created: {
        type: Date,
        default: Date.now
    },
    user: {
        type: Schema.ObjectId,
        ref: 'User'
    },
    lastUpdated: {
        type: Date,
        default: Date.now
    },
    lastUpdatedUser: {
        type: Schema.ObjectId,
        ref: 'User'
    },
    reviewed: {
        type: Number,
        enum: [0, 1],
        default: 1,
        trim: true
    },
    disabled: {
        type: Boolean,
        required: 'Please set disabled flag',
        default: false
    },
    deleted: {
        type: Boolean,
        required: 'Please set deleted flag',
        default: false
    }
});

ProductBrandSchema.set('versionKey', 'productBrandVersionKey');
mongoose.model('ProductBrand', ProductBrandSchema);
ProductBrandSchema.post('save', function(next) {
    /*if (error instanceof Error){
        next(error);
    }else {*/
        Category.findOneAndUpdate({_id: next.productCategory}, {$addToSet: {productBrands: next._id}}, {new: true}, function (errSubCategory, doc) {
            /*if (errSubCategory) {
                next(errSubCategory);
            } else {
                next();
            }*/
        });
   /* }*/

});
