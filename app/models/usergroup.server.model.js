'use strict';

/**
 * Module dependencies.
 */
var mongoose = require('mongoose'),
    Schema = mongoose.Schema;


/**
 * UserGroup Schema
 */
var UserGroupSchema = new Schema({

    name: {
        type: String,
        default: ''
    },
    acls: [{
        name: {
            type: String,
            default: ''
        },
        add: {
            type: Boolean,
            default: false
        },
        edit: {
            type: Boolean,
            default: false
        },
        view: {
            type: Boolean,
            default: false
        },
        delete: {
            type: Boolean,
            default: false
        },
        disabled: {
            type: Boolean,
            default: false
        }
    }],

    disabled: {
        type: Boolean,
        required: 'Please set disabled flag',
        default: false
    },
    deleted: {
        type: Boolean,
        required: 'Please set deleted flag',
        default: false
    },
    created: {
        type: Date,
        default: Date.now
    },
    lastUpdated: {
        type: Date,
        default: Date.now
    },
    lastUpdatedUser: {
        type: Schema.ObjectId,
        ref: 'User'
    },
    user: {
        type: Schema.ObjectId,
        ref: 'User'
    }
});


UserGroupSchema.set('versionKey', 'userGroupVersionKey');
mongoose.model('UserGroup', UserGroupSchema);
