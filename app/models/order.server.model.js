'use strict';

/**
 * Module dependencies.
 */
var mongoose = require('mongoose'),
    Counter = mongoose.model('Counter'),
    Schema = mongoose.Schema;

/**
 * A Validation function for checking the length
 */
var validateLength128 = function (property) {
    if (property) {
        return (property.length <= 128);
    }
    return true;

};

/**
 * A Validation function for checking the length
 */
var validatePincodeLength = function (property) {
    if (property) {
        return (property.length === 6);
    }
    return true;
};

/**
 * A Validation function for checking the length
 */
var validateLength1024 = function (property) {
    if (property) {
        return (property.length <= 1024);
    }
    return true;
};

/**
 * Order Schema
 */
var OrderSchema = new Schema({
    orderNumber: {
        type: String,
        default: ''
    },
    code:{
        type: String,
        default: '',
        trim: true
    },
    owner: {
        // We need to use this fields for each order owner details
        user: {
            type: Schema.ObjectId,
            ref: 'User'
        },
        businessUnit: {
            type: Schema.ObjectId,
            ref: 'BusinessUnit'
        },
        businessUnitCode:{
            type: String,
            default: '',
            trim: true
        },
        company: {
            type: Schema.ObjectId,
            ref: 'Company'
        },
        companyCode:{
            type: String,
            default: '',
            trim: true
        }
    },
    buyer: {
        contact: {
            type: Schema.ObjectId,
            ref: 'Contact'
        },
        nVipaniUser: {
            type: Schema.ObjectId,
            ref: 'User'
        },
        nVipaniCompany:{
            type: Schema.ObjectId,
            ref: 'Company'
        },
        businessUnit: {
            type: Schema.ObjectId,
            ref: 'BusinessUnit'
        }
    },
    seller: {
        contact: {
            type: Schema.ObjectId,
            ref: 'Contact'
        },
        nVipaniUser: {
            type: Schema.ObjectId,
            ref: 'User'
        },
        nVipaniCompany:{
            type: Schema.ObjectId,
            ref: 'Company'
        },
        businessUnit: {
            type: Schema.ObjectId,
            ref: 'BusinessUnit'
        }
    },
    billingAddress: {
        addressLine: {
            type: String,
            default: '',
            validate: [validateLength1024, 'The address line should not exceed 1024 characters.'],
            trim: true
        },
        city: {
            type: String,
            default: '',
            validate: [validateLength128, 'The city length should not exceed 128 characters.'],
            trim: true
        },
        state: {
            type: String,
            default: '',
            validate: [validateLength128, 'The state length should not exceed 128 characters.'],
            trim: true
        },
        country: {
            type: String,
            default: '',
            validate: [validateLength128, 'The country length should not exceed 128 characters.'],
            trim: true
        },
        pinCode: {
            type: String,
            default: '',
            validate: [validatePincodeLength, 'Pincode length should be 6'],
            trim: true
        },
        addressType: {
            type:String,
            enum:['Billing','Receiving','Shipping','Invoice'],
            default:'Billing'
        },
        gstin:{
            type: String,
            default: '',
            trim: true
        },
    },
    shippingAddress: {
        addressLine: {
            type: String,
            default: '',
            validate: [validateLength1024, 'The address line should not exceed 1024 characters.'],
            trim: true
        },
        city: {
            type: String,
            default: '',
            validate: [validateLength128, 'The city length should not exceed 128 characters.'],
            trim: true
        },
        state: {
            type: String,
            default: '',
            validate: [validateLength128, 'The state length should not exceed 128 characters.'],
            trim: true
        },
        country: {
            type: String,
            default: '',
            validate: [validateLength128, 'The country length should not exceed 128 characters.'],
            trim: true
        },
        pinCode: {
            type: String,
            default: '',
            validate: [validatePincodeLength, 'Pincode should be 6 digits.'],
            trim: true
        },
        addressType: {
            type:String,
            enum:['Billing','Receiving','Shipping','Invoice'],
            default:'Shipping'
        },
        gstin:{
            type: String,
            default: '',
            trim: true
        },
        sameAsBilling: {
            type:Boolean,
            default:true
        }
    },
    invoiceAddress: {
        addressLine: {
            type: String,
            default: '',
            validate: [validateLength1024, 'The address line should not exceed 1024 characters.'],
            trim: true
        },
        city: {
            type: String,
            default: '',
            validate: [validateLength128, 'The city length should not exceed 128 characters.'],
            trim: true
        },
        state: {
            type: String,
            default: '',
            validate: [validateLength128, 'The state length should not exceed 128 characters.'],
            trim: true
        },
        country: {
            type: String,
            default: '',
            validate: [validateLength128, 'The country length should not exceed 128 characters.'],
            trim: true
        },
        pinCode: {
            type: String,
            default: '',
            validate: [validatePincodeLength, 'Pincode should be 6 digits.'],
            trim: true
        },
        addressType: {
            type:String,
            enum:['Billing','Receiving','Shipping','Invoice'],
            default:'Invoice'
        }
    },
    statusComments: {
        type: String,
        default: ''
    },
    paymentComments: {
        type: String,
        default: ''
    },
    mediator: {
        contact: {
            type: Schema.ObjectId,
            ref: 'Contact'
        },
        nVipaniUser: {
            type: Schema.ObjectId,
            ref: 'User'
        },
        nVipaniCompany:{
            type: Schema.ObjectId,
            ref: 'Company'
        },
        businessUnit: {
            type: Schema.ObjectId,
            ref: 'BusinessUnit'
        }
    },
    orderType: {
        type: String,
        enum: ['Purchase', 'Sale'],
        default: 'Purchase'
    },
    orderScope: {
        type: String,
        enum: ['QuickOrder', 'Standard', 'Express'],
        default: 'Standard'
    },
    products :[{
        inventory: {
            type: Schema.ObjectId,
            ref: 'Inventory'
        },
        stockMasters: [{
            stockMaster: {
                type: Schema.ObjectId,
                ref: 'StockMaster'
            },
            batchNumber: {
                type: String,
                default: '',
                trim: true
            },
            unitPrice: {
                type: Number,
                default: 0,
                trim: true
            },
            unitMrpPrice: {
                type: Number,
                default: 0,
                trim: true
            },
            businessUnit:{
                type: Schema.ObjectId,
                ref: 'BusinessUnit'
            },
            numberOfUnits: {
                type: Number,
                default: 0,
                trim: true
            }
        }],
        MOQ:{
            type: Number,
            default: 0,
            trim: true
        },
        unitPrice: {
            type: Number,
            default: 0,
            trim: true
        },
        unitMrpPrice: {
            type: Number,
            default: 0,
            trim: true
        },
        margin: {
            type: Number,
            default: 0,
            trim: true
        },
        numberOfUnits:{
            type: Number,
            default: 0,
            trim: true
        },
        numberOfUnitsAvailable: {
            type: Number,
            default: 0,
            trim: true
        }
    }],
    currentStatus: {
        type: String,
        enum: ['Drafted', 'Placed','Accepted', 'Confirmed', 'Rejected', 'Cancelled', 'Pending', 'InProgress', 'Shipped', 'Delivered', 'Completed', 'Disputed', 'Resolved', 'Returned'],
        default: 'Drafted',
        trim: true
    },
    statusReason: [{
        // Rejection Reasons- 'OutofStock-Offer', 'OutofStock-Inventory', 'PaymentDefaulter'
        // Return Reasons- 'QualityMismatch', 'GoodsDamage'
        reason: {
            type: String,
            enum: ['OutofStock-Offer', 'OutofStock-Inventory', 'PaymentDefaulter', 'QualityMismatch', 'GoodsDamage'],
            default: '',
            trim: true
        },
        comment: {
            type: String,
            default: '',
            trim: true
        }
    }],
    statusHistory: [{
        status: {
            type: String,
            enum: ['Drafted', 'Placed','Accepted', 'Confirmed', 'Rejected', 'Cancelled', 'Pending', 'InProgress', 'Shipped', 'Delivered', 'Completed', 'Disputed', 'Resolved', 'Returned'],
            default: 'Drafted',
            trim: true
        },
        date: {
            type: Date,
            default: Date.now
        },
        user: {
            type: Schema.ObjectId,
            ref: 'User'
        },
        displayName: {
            type: String,
            default: ''
        },
        comments: {
            type: String,
            default: ''
        }
    }],
    applicablePaymentModes: [
        {selected: {
                type: Boolean,
                default: true
            },
            paymentMode:{
                type: Schema.ObjectId,
                ref: 'PaymentMode'
            }
        }
    ],
    applicablePaymentTerms: [{
        paymentTerm: {
            type: Schema.ObjectId,
            ref: 'PaymentTerm'
        },
        selected: {
            type: Boolean,
            default: false
        }
    }],
    applicablePaymentTerm:{
        paymentTerm:{
            type: Schema.ObjectId,
            ref: 'PaymentTerm'
        },
        name:{
            type: String,
            default: ''
        },
        type:{
            type: String,
            default: ''
        },
        paymentDetails: {
            description: {
                type: String,
                default: ''
            },
            netTerm: {
                type: Number,
                default: 0
            },
            discountTerm: {
                type: Number,
                default: 0
            },
            termDiscount: {
                type: Number,
                default: 0
            },
            discount: {
                type: Number,
                default: 0
            },
            paidByUser:{
                user: {
                    type: Schema.ObjectId,
                    ref: 'User'
                },
                contact: {
                    type: Schema.ObjectId,
                    ref: 'Contact'
                },
                businessUnit: {
                    type: Schema.ObjectId,
                    ref: 'BusinessUnit'
                },
                company: {
                    type: Schema.ObjectId,
                    ref: 'Company'
                }
            },
            paidToUser:{
                user: {
                    type: Schema.ObjectId,
                    ref: 'User'
                },
                contact: {
                    type: Schema.ObjectId,
                    ref: 'Contact'
                },
                businessUnit: {
                    type: Schema.ObjectId,
                    ref: 'BusinessUnit'
                },
                company: {
                    type: Schema.ObjectId,
                    ref: 'Company'
                }
            },
            paymentMode: {
                type: Schema.ObjectId,
                ref: 'PaymentMode'
            },
            status: {
                type: String,
                enum: ['YetToPay', 'Overdue', 'Paid','Partial'],
                default: 'YetToPay',
                trim: true
            },
            paymentResult: {
                type: String,
                enum: ['Success', 'Abort', 'Fail'],
                default: 'Success',
                trim: true
            },
            amountPaid: {
                type: Number,
                default: 0,
                trim: true
            },
            amount: {
                type: Number,
                default: 0,
                trim: true
            },
            dueDate: {
                type: Date,
                default: ''
            },
            discountTermDueDate: {
                type: Date,
                default: ''
            },
            paidDate: {
                type: Date,
                default: ''
            },
            invoiceNo: {
                type: String,
                default: '',
                trim: true
            },
            invoiceFile: {
                type: String,
                default: ''
            },
            invoiceDate: {
                type: Date,
                default: ''
            },
            referenceNumber: {
                type: String,
                default: ''
            },
            bankName: {
                type: String,
                default: ''
            },
            bankBranchName: {
                type: String,
                default: ''
            },
            comments: {
                type: String,
                default: ' '
            }
        },
        installmentPaymentDetails: [{
            description: {
                type: String,
                default: ''
            },
            // This field is for installments
            days: {
                type: Number,
                default: 0
            },
            // This field is for installments
            percentage: {
                type: Number,
                default: 0
            },
            paidByUser:{
                user: {
                    type: Schema.ObjectId,
                    ref: 'User'
                },
                contact: {
                    type: Schema.ObjectId,
                    ref: 'Contact'
                },
                businessUnit: {
                    type: Schema.ObjectId,
                    ref: 'BusinessUnit'
                },
                company: {
                    type: Schema.ObjectId,
                    ref: 'Company'
                }
            },
            paidToUser:{
                user: {
                    type: Schema.ObjectId,
                    ref: 'User'
                },
                contact: {
                    type: Schema.ObjectId,
                    ref: 'Contact'
                },
                businessUnit: {
                    type: Schema.ObjectId,
                    ref: 'BusinessUnit'
                },
                company: {
                    type: Schema.ObjectId,
                    ref: 'Company'
                }
            },
            paymentMode: {
                type: Schema.ObjectId,
                ref: 'PaymentMode'
            },
            status: {
                type: String,
                enum: ['YetToPay', 'Overdue', 'Paid','Partial'],
                default: 'YetToPay',
                trim: true
            },
            paymentResult: {
                type: String,
                enum: ['Success', 'Abort', 'Fail'],
                default: 'Success',
                trim: true
            },
            amountPaid: {
                type: Number,
                default: 0,
                trim: true
            },
            amount: {
                type: Number,
                default: 0,
                trim: true
            },
            dueDate: {
                type: Date,
                default: ''
            },
            paidDate: {
                type: Date,
                default: ''
            },
            invoiceNo: {
                type: String,
                default: '',
                trim: true
            },
            invoiceFile: {
                type: String,
                default: ''
            },
            invoiceDate: {
                type: Date,
                default: ''
            },
            comments: {
                type: String,
                default: ' '
            },
            referenceNumber: {
                type: String,
                default: ''
            },
            bankName: {
                type: String,
                default: ''
            },
            bankBranchName: {
                type: String,
                default: ''
            }
        }]
    },
    transport:{
        mode: {
            type: String,
            enum: ['Truck', 'Rail', 'Air', 'Ship', 'Other'],
            default: 'Truck',
            trim: true
        },
        amount: {
            type: Number,
            default: 0,
            trim: true
        },
        vehicleNumber:{
            type: String,
            default: ''
        },
        contact: {
            number: {
                type: String,
                default: ''
            },
            name: {
                type: String,
                default: ''
            }
        },
        courier: {
            // Air Way Bill - For Couriers
            awbNumber: {
                type: String,
                default: ''
            },
            name: {
                type: String,
                default: ''
            },
            trackurl: {
                type: String,
                default: ''
            }
        },
        parcel: {
            // Frieght Record Number - Railways
            fnrNumber: {
                type: String,
                default: ''
            },
            // Parcel Service
            consignmentNumber: {
                type: String,
                default: ''
            },
            name: {
                type: String,
                default: ''
            },
            trackurl: {
                type: String,
                default: ''
            }
        },
        type:{
            type: String,
            enum: ['Self', 'Courier','Parcel', 'Other'],
            default: 'Self',
            trim: true
        }
    },
    deliveryDate: {
        type: Date,
        default: ''
    },
    invoice: {
        number:{
            type: String,
            default: '',
            trim: true
        },
        invoiceDate: {
            type: Date,
            default: ''
        },
        vatNo: {
            type: String,
            default: '',
            trim: true
        },
        tinNo: {
            type: String,
            default: '',
            trim: true
        },
        cstNo: {
            type: String,
            default: '',
            trim: true
        },
        fglNo: {
            type: String,
            default: '',
            trim: true
        },
        wayBillNo: {
            type: String,
            default: '',
            trim: true
        },
        permitNo: {
            type: String,
            default: '',
            trim: true
        },
        vehicleNo: {
            type: String,
            default: '',
            trim: true
        }
    },
    paidAmount: {
        type: Number,
        default:0,
        trim: true
    },
    totalAmount: {
        type: Number,
        default: 0,
        trim: true
    },
    offer: {
        type: Schema.ObjectId,
        ref: 'Offer'
    },
    lastUpdated: {
        type: Date,
        default: Date.now
    },
    updateHistory: [{
        modifiedOn: {
            type: Date,
            default: Date.now
        },
        modifiedBy: {
            type: Schema.ObjectId,
            ref: 'User'
        }
    }],
    created: {
        type: Date,
        default: Date.now
    },
    disabled: {
        type: Boolean,
        required: 'Please set disabled flag',
        default: false
    },
    deleted: {
        type: Boolean,
        required: 'Please set deleted flag',
        default: false
    },
    user: {
        type: Schema.ObjectId,
        ref: 'User'
    },
    isInterState:{
        type:Boolean,
        default:false
    },
    isIntraStock:{
        type:Boolean,
        default:false
    }

});

OrderSchema.pre('save', function (next) {
    var doc = this;
    if (this.isNew) {
        var counterUtil = require('../controllers/utils/common.counter.util');
        var globalUtil = require('../controllers/utils/common.global.util');
        counterUtil.getBusinessUnitCodeById(doc.owner.businessUnit,function (bError,businessUnit) {
            if(bError){
                return next(bError);
            }else {
                counterUtil.getNextOrderSequenceNumber(doc, function (error, orderSeqNumber) {
                    if (error)
                        return next(error);
                    else {
                        var date = new Date();
                        doc.created = date;
                        var dd = '' + date.getDate();
                        var mm = '' + (date.getMonth() + 1);
                        var yyyy = '' + date.getFullYear();

                        if (mm.length < 2) {
                            mm = '0' + mm;
                        }
                        if (dd.length < 2) {
                            dd = '0' + dd;
                        }
                        /*    if(counter===null){
                                var defaultCounter = new Counter();
                                defaultCounter.orderSeqNumber=1;
                                defaultCounter.offerSeqNumber=1;
                                defaultCounter.invoiceSeqNumber=1;
                                defaultCounter.productSeqNumber = 1;
                                defaultCounter.productSampleSeqNumber = 1;
                                defaultCounter.inventorySeqNumber = 1;
                                defaultCounter.reportSeqNumber = 1;
                                defaultCounter.save(function (err) {
                                    if(err){
                                        return next(err);
                                    }
                                    var orderNumber = dd + mm + yyyy + '-' +(defaultCounter.orderSeqNumber);
                                    doc.orderNumber = orderNumber;
                                    next();
                                });
                            }else {*/
                        doc.owner.businessUnitCode=businessUnit.code;
                        doc.owner.companyCode=businessUnit.company.code;
                        doc.code=   doc.owner.companyCode +'-'+doc.owner.businessUnitCode +'-'+dd + mm + yyyy + '-' + globalUtil.leftPadWithZeros(orderSeqNumber, 5);

                        doc.orderNumber = doc.code;
                        next();
                        /* }*/
                    }

                });
            }
        });
    }else{
        next();
    }

});



OrderSchema.pre('remove', function(next){
    var pname=this.name;
    var id=this._id;
    var notification=this.model('Notification');
    this.model('Todo').remove(
        {'source.order': id},
        function(todoError,orders){
            if(todoError){
                var title='Order id '+id+' is used in todo';
                var todoErr = new Error(title);
                next(todoErr);
            }else {
                notification.remove({'source.order': id},
                    function (notificationError, notification) {
                        if(notificationError) {
                            var title='Order id '+id+' is used in notification';
                            var notificationErr = new Error(title);next(notificationErr);}
                        else next();
                    }
                );
            }
        }
    );
});
OrderSchema.set('versionKey', 'ordersVersionKey');
mongoose.model('Order', OrderSchema);
