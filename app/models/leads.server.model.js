'use strict';

/**
 * Module dependencies.
 */
var mongoose = require('mongoose'),
    Schema = mongoose.Schema;


var validateLength128 = function (property) {
    if (property) {
        return (property.length <= 128);
    }
    return true;

};


/**
 * A Validation function for checking the length
 */
var validatePhoneNumberLength = function (property) {
    if (property) {
        return (property.length <= 11 && property.length >= 10);
    }
    return true;
};

/**
 * A Validation function for checking the length
 */
var validatePincodeLength = function (property) {
    if (property) {
        return (property.length === 6);
    }
    return true;
};
/**
 * A Validation function for checking the length
 */
var validateLength1024 = function (property) {
    if (property) {
        return (property.length <= 1024);
    }
    return true;
};
/**
 * Leads Schema
 */
var LeadsSchema = new Schema({
    name: {
        type: String,
        default: '',
        required: 'Please fill name',
        trim: true
    },
    description: {
        type: String,
        default: '',
        trim: true
    },
    email:  { type: String,
        default: '',
        match: [/.+\@.+\..+/, 'Please fill a valid email'],
        //validate: [validateLocalStrategyProperty, 'Please fill in your email'],
        trim: true
    },
    sent: {
        type: Boolean,
        default: false
    },
    created: {
        type: Date,
        default: Date.now
    },
    lastUpdated: {
        type: Date,
        default: Date.now
    },
    disabled: {
        type: Boolean,
        required: 'Please set disabled flag',
        default: false
    },
    deleted: {
        type: Boolean,
        required: 'Please set deleted flag',
        default: false
    }

});

LeadsSchema.set('versionKey', 'leadsVersionKey');
mongoose.model('Leads', LeadsSchema);
