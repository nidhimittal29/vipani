
# nVipani Source Code Build Status

Welcome to the build status of nVipani Source Code
Versions: Display Versions Information here that are used in this project - http://shields.io/


Live demo
Access the app at  http://localhost:3000

## Integration with Build Tools
We are integrating the build tools into repo to improve the health of the code base.

### Build Status
CodeShip Status - ( https://codeship.com/projects/123937/status?branch=master)

[![Build Status](https://travis-ci.org/ndbabu/nvipani.svg?branch=master)](https://travis-ci.org/ndbabu/nvipani)


### Code Coverage
[![Coverage Status](https://img.shields.io/coveralls/ndbabu/nvipani.svg?style=flat)](https://coveralls.io/r/ndbabu/nvipani?branch=master)


### Static Analysis


### Code Claimate Metrics
Code Climate <a href="https://codeclimate.com/repos/567a85f4bd3f3b1a7a0010e2/feed"><img src="https://codeclimate.com/repos/567a85f4bd3f3b1a7a0010e2/badges/b236fc1047c4efac1187/gpa.svg" /></a>
Test Coverage <a href="https://codeclimate.com/repos/567a85f4bd3f3b1a7a0010e2/coverage"><img src="https://codeclimate.com/repos/567a85f4bd3f3b1a7a0010e2/badges/b236fc1047c4efac1187/coverage.svg" /></a>
Issues Count <a href="https://codeclimate.com/repos/567a85f4bd3f3b1a7a0010e2/feed"><img src="https://codeclimate.com/repos/567a85f4bd3f3b1a7a0010e2/badges/b236fc1047c4efac1187/issue_count.svg" /></a>

### KLOC Metrics

### Dependency Versions
Versions: Display Versions Information here that are used in this project - http://shields.io/

### Dependencies
[![Dependency Status](https://img.shields.io/david/ndbabu/nvipani.svg?style=flat)](https://david-dm.org/ndbabu/nvipani)
[![devDependency Status](https://img.shields.io/david/dev/ndbabu/nvipani.svg?style=flat)](https://david-dm.org/ndbabu/nvipani#info=devDependencies)

[![Dependency Status](https://david-dm.org/ndbabu/nvipani.svg?theme=shields.io)](https://david-dm.org/ndbabu/nvipani)
[![devDependency Status](https://david-dm.org/ndbabu/nvipani/dev-status.svg?theme=shields.io)](https://david-dm.org/ndbabu/nvipani#info=devDependencies)
