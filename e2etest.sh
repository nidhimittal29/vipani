echo "by default: all test suite is executed.  To run specific test suite,usage: ./e2etest.sh registration,products"
if [ $# -eq 1 ]
then
    grunt test:e2e --suite=$1
else
    grunt test:e2e --suite=all
fi
